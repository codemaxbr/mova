/*
Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 1.6.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v1.6/admin/
    ----------------------------
        APPS CONTENT TABLE
    ----------------------------
    
    <!-- ======== GLOBAL SCRIPT SETTING ======== -->
    01. Handle Scrollbar
    
    02. Handle Sidebar - Menu
    03. Handle Sidebar - Mobile View Toggle
    04. Handle Sidebar - Minify / Expand
    05. Handle Page Load - Fade in
    06. Handle Panel - Remove / Reload / Collapse / Expand
    07. Handle Panel - Draggable
    08. Handle Tooltip & Popover Activation
    09. Handle Scroll to Top Button Activation
    
    <!-- ======== Added in V1.2 ======== -->
    10. Handle Theme & Page Structure Configuration
    11. Handle Theme Panel Expand
    12. Handle After Page Load Add Class Function - added in V1.2
    
    <!-- ======== Added in V1.5 ======== -->
    13. Handle Save Panel Position Function - added in V1.5
    14. Handle Draggable Panel Local Storage Function - added in V1.5
    15. Handle Reset Local Storage - added in V1.5
    16. Handle Ajax Page Load - added in V1.5
    17. Handle Ajax Page Load Url - added in V1.5
    18. Handle Ajax Sidebar Toggle Content - added in V1.5
    19. Handle Url Hash Change - added in V1.5
    20. Handle Pace Page Loading Plugins - added in V1.5
    
    <!-- ======== Added in V1.6 ======== -->
    21. Handle IE Full Height Page Compatibility - added in V1.6
    22. Handle Unlimited Nav Tabs - added in V1.6
	
    <!-- ======== APPLICATION SETTING ======== -->
    Application Controller
*/


/* 01. Handle Scrollbar
------------------------------------------------ */
var varGlobal = {};
    varGlobal.unitData;
    varGlobal.userID;
    varGlobal.userName;
    varGlobal.timeReload = 60000;
    varGlobal.controlGeoCode = false;
    varGlobal.activePanel = 1;
    varGlobal.filaGeoCode = [];
    varGlobal.widgetsInfo;
    varGlobal.perms;
    varGlobal.widgetCookieName = 'panelWidgets';
    varGlobal.widgetCookie = 'panelWidgets';
    varGlobal.colorOn = '#00acac';
    varGlobal.colorStat = '#f59c1a';
    varGlobal.colorOff = '#d9534f';
    varGlobal.marker = [];
        varGlobal.marker.Size = 'large';
        varGlobal.marker.color = '#242a30';
        varGlobal.marker.carSymbol = 'car';
    varGlobal.lastWidgetInfo = [];
        varGlobal.lastWidgetInfo.widgetName = '';
        varGlobal.lastWidgetInfo.size;
        varGlobal.lastWidgetInfo.height;
    varGlobal.imgRelLogo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKwAAAAyCAYAAADBXTDrAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADfVJREFUeNrsXQtwFdUZPnsTQDtTUUR8gER5KyQBdXhIiKTysJCLrQqDEGxxQEQRtQMFQTqlKka0OrQBSuOUKSTUEp1BQuwYQGoSKE9jLqAE5ZGAILYCsWN5JTk937nZZXfvObtn770hybj/zM7k7r275z97vvOd7//PORuNUkribcX7P6dVp/9Lqk5/Zzmf2rE9aXt1GzKi9x0a8c23KCwxHjfJLd1JS788Tkq++CoCpBKjKR1vIOndO5L0bh3J2Hv6+gD2Tcm0aBl255HDNOefn5LVOz6P2YmkdteQrAF3kJcfHOYD17f4Arby66/oE/kbGZsej7szkAszhvZlwB3uA9e32AH74rqN9JV/7FAGX2qnG4zPZ89dIKHj/1a+NjdruC8VfIsOsDsPH6ZTGas6AS6FgTOY3IXp0k6OQRUYGvcpDB0m60OHSA0DsswmDbiTrHr8IR+0vqkDFhH/uNwNUmABVPNHDSA9b+oYFbByS3fQPKaDZRIDHaFiwTQftL65AxZgeiKv2AGoA6MGqqhjoCxRliEM2id90PomB6wMrNCXa6cGGy2X+sw7G2jOlnIftL6pAxaadcSSdyNkAAfNbxofNLklO+js9z6OKB/6+OPZk33Q/oAtIDo5Lnc9A8t59hc1jpRO7a8IWGFT0wdoxc8+wti8tcWHki+OkRfXFVO/2XzAGjYmJ59WffudgFmnX1Fm69+lCwPt2Ijzr3ywnY8AftP5koAHPiOXFFh+kHT9NeToq8812TCcW7I9QkvHowNdKv8b1U7sJYHDZcY52q4zoTcnk7remaTNjb2F979waj9N2L+BaCf3Eu10tXG+vksaobckk1b9Ho35Wem+8TLO1UT4h7Ja3zbYsZxL2/5EE/as4X/X3T2BtLo3fqNjbdE8iudGr25L6obNdfXl4tGt/Pf2Z4breX26svr0GhVxj9r3nqYB9hx4OWnT+W8sgE1duIzac60fPjeWBVh3NqluBOsXVnxpOffnSSOZdBioRQOGxMK5RDtT7fi7unsmktphL1iAW5efRRN25zteR6/rzK/zChDeEcqWkYQ9+RaQRlMOAJW46VXrOfbbxNGLYm7H+j+mM/CVWgH5bJkQtLUfvcbr5PasdfDWDX7K8FFWjgFYMNm01R9afjAj4y7yhwnBZhHkXDczm5qDMDD/keznPflW93aQs6Oq4SHWZmZzZmu1aoISkC4z7hASeKZEU2WshK3LPN3fDNxL45ZbGIr+LonaQYK6aIvOxtSWFw98QFuvGB3pfxoD2sNLNa+kIAXu3RNJIgN6RDms0xkaNn/7ZxHpq+YCVtjiR4ZaPkNnF+wqF2pZDCXkeY0faDwMSfWvp3oCK9dLDECtCthQxBrJK5jADihThbHAhtGAlfvIQAH/wGbGyfM1wrqgY8TSBokbs8U+mMrDs+edOwqw6n6KwGoJuipPfkVLDrICwLYNx4yMfs1KbGP4T2r3Y4uPedv3C7WbucK8QZekkcCJ0JWPaFmZYHUpWBmg7cNe1GBijAbJwu/LNLjIwOKx6GqZr/W3pxmSyQlsccsS5G3fF/HFSz8fydl15ppCGpjyW3r7nDcpgN2UoM0a2Nvy2a5reYU+K2pWHQ2sjqFUqAXj3Imgr9FhMXTK2CtalkWHkEkfrvcha1z0fdwAG274y/nOYGrXBl37L5rz0W5+rurbGrJ0yx7S9ICllgM+2jWdF5156TEWSb9FNRyXxi7nGkpJ2zJQXJi7j1+LYKBOwmqioTQcZZc635/pwovTigzfuH/MV4DDEVgb5hq6Uvg9kx8I8DyxK+sEsiG+dvhcLn/sQZ4oiDU/a/7cWP3gp8oztwA2dOwbCw4yU7rxL5du/sRynv+uCa3nzZ20lI4dLD6VHbQumqlTfAB4gAiKzGkoRNwITJyux3eXniji0ayeQUCEnDClUJMxGxpUBwnPBjgMzfALPiCIsad64GvCxDzeQepvSZHr7rXTSR3zRVYPN3Cp/h4dHodM2/LfMD/RseG3PeWH+qGeqK9Th7c8y+J9n1E7a6X3uJV/GTp+ynIegVhTW5j9L/tUceyU5XuACIByBB1jYTxAadSeKW8AfCfLOwLEMobXh3+e5pEEWLXBbEe/jIZm5QdmV2gy0KKDaGeqeJpIJh1UWVaFXWWjBUAIP2U5bYtPrMO7jR68bqHj30RkB8Bkoh9npnZrcsCm3NrB8tnuv96gTqB1681OOVS3/CpSMkLmO7E33DB78qU+Jf5kjqesDMAgq2PC1uWOo40qy7qxa0LZcjkpMBB60uCss+KejnWu+d95C8GmdupgYrPuxvkUdn7qfYOaPM2V1K6tfUCQspCMYchVbRvNP8x2OeUxRewKUHltXLfRIMCCPT0ZHy3LurGrHlQKrx23PLpshMt1DXlYceu/P3OSNj/zXrJ4bAb5dOGMZpGT7d+1q2ZHbPG+/c1mbYGTFAkcKhOzlaJ+kzG+qEx0DICNy5QoWVYP4ETsCv0pkwJgV9FUq4pBPjiNgAG3Ifelhx7QZv00w1/SFwfTzorZqv7O0THdVzaMQsdyueHAsphUETJo0Twq09o6C8q+r4uhA3LAd0lzAKwpEW8czcQqTx6nv8j9O52Zt87qVDP11xWwp6s9s7JSA9+c7MyUDsFgYuELEg28TJrF0IMoXZfHW3LVO8iqCIY1p67Q+5xmahrbBr68kqzetpfkbN5NAFyfo6M3p5SbfWLDiV1l97lSJtCw1FSZMulMTWMbWJUHhLb0VVivWv0d0ad3i5AssTKpV6kRoXVlLGvLo6qwq65VhXa+Jrb6nJbXJ9D5emvUXWFiWJ2aZcFCY1oIABX0I3tWg7Qg3pUN3YH9sU0nBw6VKg3NKizrhV2xik18v9jwEjgivz6QaguyAAhox3APSoqLA/Ew3c/Sg8dsQeKNLQewEm0my80qpYGw2FuQegKb2yc4VFhWlV1hskwAJkliGZUDDqvqAv27dotIExWWHzRSDKhgvFYUebHwrNplnyYOCjd2SWWV5by9wzVnk+1GaJRFKZJI3YllsSBHlieWXSfLCMiCOTdDzOS01JJr2GDfHpaTpQerIxyKdS2lV5sfTCNJTK7gWPHL0WREcm8NzB+yTcWmNUwjtxSTTT8iJ4pEvad75WdJE/vYFuNVy0qnWAc/Jd0yJEvJ8aWVDcsdlUcLbOtxWbPMATuENbpGqXFsKK80ZEF9Q8VjWUsZjYH5j/x+Dj+mDg0PbXlbQxY/cejftfRonYOpYLp1IbZT6oexoWw5n57Yl/oQzPYUKDptrXHqAPBPNcsEQkT9lbIEs0bdH+HQsk27wjqF6SA8ANB0U6a4YEs377J8Dvbr0eLSS2Aq2dI/fYjHLgkR2yLNiIal8651XJ6oT5s6SRO3OXszu7qCzaEDgDHhL/wWTQWjnqiv6toGIw+bda915c/qbSGDZc3zxl6HraiDCVYOAgr984J3P2hIc122zL4tD7AclA8vla604pqWDfOcbbDFhzU22BR/Y+eE23YaMLjKtKgbqFXY1dwBnFZa8W0vzO822X14nVAf1At/o55ettOYAJtsmT2q+f4cmfPORiMa1FkBBTQ2aHF/lBNo2KbM2XXTTot/2IQ4pYXJAUsdH1ujlJdFY6sGvQCN6s5YtKkby6qwq8GkWGnl0AntWjnaPWwGYIcn99GG9EyyfFnItGzBjt3UzgqNCVodrPqed9jP3loZwa7zgumkJZu+ble1kVXAqrKWVpVlVdnVAqbZFUprWmMxy9TsismZxJ7imraykFSeOEbtrABQxVvTmoU3ykJjvlG0mRaWH7D4hAU6UzLc2VU6E6OyDUZwbbxnqvSF2LE0Mt8BMXa5Z7C6sawXdrUzLTRtLM/KifktgO15y60aZy4TZmu+P0/GLy0QsoIuqGNlWySZsYMUOkdvADidt7WCyZLiiJmtFZPHKN1XtPSOM7fCaiLRQmzZ4mw7CIRlOoCSb3uZVkRUAyHzPbG9JJa3uohYFp01lpduYCH6xWe3Eq8dEe2C7T/YuiQiDLwhRvj2wn7z36Shamu+M6XzjaT8lV9dfguK7aUU+gsQkAZze3WNJe/GdKqu0fhLIRqYFWCd9pfCiGteGz+CzBp9v/LDRGSNJDZPjLP71gZfVV6rqb/gQger+WURbmUmbMrmHdprmcarkPBqH5N+Db/WJ4VPh2JbdTxfPYTgFv7y3CkDjcrrh7zKPEy3Yo2AdjJk6Fd0UF6vLmkRr4fCc0DGhJMi3nLDWBvBnRCwkACDFuYSu25M6XwTY7cg6d+tu6Y7wqNWwVtG8HDrG9Y18inJhgXFWBgR4O+0sgYSCOr0jXNhsK6P8CtrcCr565MT/bW5P2CTvtB455df0AdeXx0B2rY/uoosZiw3JSNNM/cgEXBVgwWkYtCLUBaAimDPbugsZob3zQesMmhhyCgAuDrb6kMhVh7xN9WdqRICmA8DgmHt7S1l9NdMr4rK8sHqmxJgDdAuXiUEEix4Vy/y9LD+ZHhKclSAeqNoE122cQep+s9Z4fdcBkzP8sHqmxpgdU07PmctCVV/Lf1NUvtrOesO6XUbX/JnZl6zbQztpViMXVZZRQo/OeBY7muPjmQBlv/fEX3zCFjdFhRsoIve/9hTAQBwNWNPGYOKjAd3j4+Rgt43H7DKBrYFaBHJx9vA0vMevM8S0PnmW0yAtQMXs1AyfeuFhaFVp2QM8YHqW+MA1mwF23dRaNKSA0cdda5uSI2lM5CmMc2LoA0zbH5T+HbFACvKLMhYN9psgm++wf4vwACwaw/iLA1MawAAAABJRU5ErkJggg==';

var default_content = '<div class="p-t-40 p-b-40 text-center f-s-20 content"><i class="fa fa-warning fa-lg text-muted m-r-5"></i> <span class="f-w-600 text-inverse">Error 404! Page not found.</span></div>';

var handleSlimScroll = function() {
    "use strict";
    $('[data-scrollbar=true]').each( function() {
        generateSlimScroll($(this));
    });
};
var generateSlimScroll = function(element) {
    var dataHeight = $(element).attr('data-height');
        dataHeight = (!dataHeight) ? $(element).height() : dataHeight;
    
    var scrollBarOption = {
        height: dataHeight, 
        alwaysVisible: true
    };
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        scrollBarOption.wheelStep = 3;
        scrollBarOption.touchScrollStep = 100;
    }
    $(element).slimScroll(scrollBarOption);
};
/* 02. Handle Sidebar - Menu
------------------------------------------------ */
var handleSidebarMenu = function() {
    "use strict";
    $('.sidebar .nav > .has-sub > a').die('click').die('click').live('click', function() {
        var target = $(this).next('.sub-menu');
        var otherMenu = '.sidebar .nav > li.has-sub > .sub-menu';
    
        if ($('.page-sidebar-minified').length === 0) {
            $(otherMenu).not(target).slideUp(250, function() {
                $(this).closest('li').removeClass('expand');
            });
            $(target).slideToggle(250, function() {
                var targetLi = $(this).closest('li');
                if ($(targetLi).hasClass('expand')) {
                    $(targetLi).removeClass('expand');
                } else {
                    $(targetLi).addClass('expand');
                }
            });
        }
    });
    $('.sidebar .nav > .has-sub .sub-menu li.has-sub > a').die('click').die('click').live('click', function() {
        if ($('.page-sidebar-minified').length === 0) {
            var target = $(this).next('.sub-menu');
            $(target).slideToggle(250);
        }
    });
};

/**
* function to load a given css file 
*/ 
var loadCSS = function(href) {
    var cssLink = $("<link rel='stylesheet' type='text/css' href='"+href+"'>");
    $("head").append(cssLink); 
};

/**
* function to load a given js file 
*/ 
var loadJS = function(src) {
    var jsLink = $("<script type='text/javascript' src='"+src+"'>");
    $("head").append(jsLink); 
}; 

var versionDashboard = function(){
    $('[rel="stylesheet"]').each(function(i, index){
        var css = $(index).attr('href');
        //$(index).attr('href', css + '?version=1.0');
        loadCSS(css + '?version=1.0');
    });
};

var postExterno = function(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();

    form.remove();
};
/* 03. Handle Sidebar - Mobile View Toggle
------------------------------------------------ */
var handleMobileSidebarToggle = function() {
    var sidebarProgress = false;
    $('.sidebar').on('click touchstart', function(e) {
        if ($(e.target).closest('.sidebar').length !== 0) {
            sidebarProgress = true;
        } else {
            sidebarProgress = false;
            e.stopPropagation();
        }
    });
    
    $(document).on('click touchstart', function(e) {
        if ($(e.target).closest('.sidebar').length === 0) {
            sidebarProgress = false;
        }
        if (!e.isPropagationStopped() && sidebarProgress !== true) {
            if ($('#page-container').hasClass('page-sidebar-toggled')) {
                sidebarProgress = true;
                $('#page-container').removeClass('page-sidebar-toggled');
            }
            if ($(window).width() < 979) {
                if ($('#page-container').hasClass('page-with-two-sidebar')) {
                    sidebarProgress = true;
                    $('#page-container').removeClass('page-right-sidebar-toggled');
                }
            }
        }
    });
    
    $('[data-click=right-sidebar-toggled]').die('click').die('click').live('click', function(e) {
        e.stopPropagation();
        var targetContainer = '#page-container';
        var targetClass = 'page-right-sidebar-collapsed';
            targetClass = ($(window).width() < 979) ? 'page-right-sidebar-toggled' : targetClass;
        if ($(targetContainer).hasClass(targetClass)) {
            $(targetContainer).removeClass(targetClass);
        } else if (sidebarProgress !== true) {
            $(targetContainer).addClass(targetClass);
        } else {
            sidebarProgress = false;
        }
        if ($(window).width() < 480) {
            $('#page-container').removeClass('page-sidebar-toggled');
        }
    });
    
    $('[data-click=sidebar-toggled]').die('click').die('click').live('click', function(e) {
        e.stopPropagation();
        var sidebarClass = 'page-sidebar-toggled';
        var targetContainer = '#page-container';

        if ($(targetContainer).hasClass(sidebarClass)) {
            $(targetContainer).removeClass(sidebarClass);
        } else if (sidebarProgress !== true) {
            $(targetContainer).addClass(sidebarClass);
        } else {
            sidebarProgress = false;
        }
        if ($(window).width() < 480) {
            $('#page-container').removeClass('page-right-sidebar-toggled');
        }
    });
};
/* 04. Handle Sidebar - Minify / Expand
------------------------------------------------ */
var handleSidebarMinify = function() {
    $('[data-click=sidebar-minify]').die('click').die('click').live('click', function(e) {
        e.preventDefault();
        var sidebarClass = 'page-sidebar-minified';
        var targetContainer = '#page-container';
        if ($(targetContainer).hasClass(sidebarClass)) {
            $(targetContainer).removeClass(sidebarClass);
            if ($(targetContainer).hasClass('page-sidebar-fixed')) {
                generateSlimScroll($('#sidebar [data-scrollbar="true"]'));
            }
        } else {
            $(targetContainer).addClass(sidebarClass);
            if ($(targetContainer).hasClass('page-sidebar-fixed')) {
                $('#sidebar [data-scrollbar="true"]').slimScroll({destroy: true});
                $('#sidebar [data-scrollbar="true"]').removeAttr('style');
            }
            // firefox bugfix
            $('#sidebar [data-scrollbar=true]').trigger('mouseover');
        }
        $(window).trigger('resize');
    });
};
/* 05. Handle Page Load - Fade in
------------------------------------------------ */
var handlePageContentView = function() {
    "use strict";
    $.when($('#page-loader').addClass('hide')).done(function() {
        $('#page-container').addClass('in');
    });
};
/* 06. Handle Panel - Remove / Reload / Collapse / Expand
------------------------------------------------ */
var deleteVariables = function (widgetName){
    //variaveis de mapa
    if((widgetName == 'widgetsmapbox') || (widgetName == undefined)){    
        delete varMapBox;  
        delete MapBox;
    }
    //variaveis de relatório
    if((widgetName == 'widgetsreport') || (widgetName == undefined)){  
        delete varReport;  
        delete widgetsReport;
    }
    if((widgetName == 'widgetsreporthidro') || (widgetName == undefined)){  
        delete varReporthidro;  
        delete widgetsReportHidro;
    }
    if((widgetName == 'widgetsreportstop') || (widgetName == undefined)){  
        delete varReportstop;  
        delete widgetsReportStop;
    }
    if((widgetName == 'widgetsreportkm') || (widgetName == undefined)){  
        delete varReportkm;  
        delete widgetsReportkm;
    }
    //variaveis de widgets
    if((widgetName == 'widgetsaccount') || (widgetName == undefined)){  
        delete varAccount;  
        delete WidgetsAccount;
    }
    if((widgetName == 'widgetsdevice') || (widgetName == undefined)){  
        delete varDevices;  
        delete widgetsDevice;
    }
    if((widgetName == 'widgetstable') || (widgetName == undefined)){  
        delete varTables;  
        delete widgetsTable;
        delete widgetsTableAccountType;
        delete widgetsTableWidgets;
        delete widgetsTableManufacture;
        delete widgetsTableModel;
    }
    if((widgetName == 'widgetsunit') || (widgetName == undefined)){  
        delete varUnit;  
        delete widgetsUnit;
    }
    if((widgetName == 'widgetsuser') || (widgetName == undefined)){  
        delete varUser;  
        delete widgetsUser;
    }
    if((widgetName == 'widgetspoi') || (widgetName == undefined)){ 
        delete varPOI;  
        delete widgetspoi;
    }
    if((widgetName == 'widgetscerca') || (widgetName == undefined)){  
        delete varCerca;  
        delete widgetscerca;
    }
    if((widgetName == 'widgetsalarm') || (widgetName == undefined)){  
        delete varAlarm;  
        delete widgetsalarm;
    }
    if((widgetName == 'widgetsalarmmonitor') || (widgetName == undefined)){  
        delete varAlarmmonitor;  
        delete widgetsalarmmonitor;
    }
    if((widgetName == 'widgetsgroups') || (widgetName == undefined)){  
        delete varGroups;  
        delete widgetsgroups;
    }
    //VELLEDA
    if((widgetName == 'widgetsgroups') || (widgetName == undefined)){  
        delete varChecklistvelleda;  
        delete widgetschecklistvelleda;
    }
    if((widgetName == 'widgetsgroups') || (widgetName == undefined)){  
        delete varReportchecklistvelleda;  
        delete widgetsreportchecklistvelleda;
    }
}
var handlePanelAction = function() {
    "use strict";    
    // sound
    $('[data-click=panel-sound]').live('hover', function() {
        $(this).tooltip({
            title: 'Habilitar / Desabilitar Som',
            placement: 'bottom',
            trigger: 'hover',
            container: 'body'
        });
        $(this).tooltip('show');
    });
    $('[data-click=panel-sound]').die('click').live('click', function(e) {
        if($(this).find('i').hasClass('fa-bell')){
            $(this).find('i').removeClass('fa-bell');
            $(this).find('i').addClass('fa-bell-slash');
        }else{
            $(this).find('i').addClass('fa-bell');
            $(this).find('i').removeClass('fa-bell-slash');
        }
    });
    
    // remove
    $('[data-click=panel-remove]').live('hover', function() {
        $(this).tooltip({
            title: 'Remove',
            placement: 'bottom',
            trigger: 'hover',
            container: 'body'
        });
        $(this).tooltip('show');
    });
    $('[data-click=panel-remove]').die('click').live('click', function(e) {
        e.preventDefault();
        var widgetName = $(this).parent().parent().parent().attr('id');

        //controla o tour quando o usuário remove o widget de mapa
        if(($('#step-8').length > 0) && (widgetName == 'widgetsmapbox')){
            $('#step-8').find('[data-role=next]').click();
        }

        $(this).tooltip('destroy');
        $(this).closest('.panel').remove();
        $('#'+widgetName).empty().remove();

        handleDivFullScr();
        //removendo todas as variaveis do widget
        deleteVariables(widgetName);
        //Apaga cookie
        window.setTimeout(function() {
            handleCookieWidget();
        }, 1000); 
    });
    
    // collapse
    $('[data-click=panel-collapse]').live('hover', function() {
        $(this).tooltip({
            title: 'Minimizar / Maximizar',
            placement: 'bottom',
            trigger: 'hover',
            container: 'body'
        });
        $(this).tooltip('show');
    });
    $('[data-click=panel-collapse]').die('click').live('click', function(e) {
        e.preventDefault();
        var widgetName = $(this).parent().parent().parent().attr('id');

        //controla o tour quando o usuário remove o widget de mapa
        if(($('#step-8').length > 0) && (widgetName == 'widgetsmapbox')){
            $('#step-8').find('[data-role=next]').click();
        }

        $(this).closest('.panel').find('.panel-body').css('min-height', '0px');
        $(this).closest('.panel').find('.panel-body').slideToggle('slow', function() {
            $(this).closest('.panel').find('.panel-body').css('min-height', '269px'); //min-height: 308px;
        });

        if(widgetName == 'widgetsmapbox'){
            window.setTimeout(function() {
                if(typeof(varMapBox.mapInstance) == 'object'){
                    varMapBox.mapInstance.invalidateSize();
                }
            }, 500); 
        }
    });
    
    // reload
    $('[data-click=panel-reload]').live('hover', function() {
        $(this).tooltip({
            title: 'Recarregar',
            placement: 'bottom',
            trigger: 'hover',
            container: 'body'
        });
        $(this).tooltip('show');
    });
    $('[data-click=panel-reload]').die('click').live('click', function(e) {
        e.preventDefault();
        var target = $(this).closest('.panel');
        if (!$(target).hasClass('panel-loading')) {
            var targetBody = $(target).find('.panel-body');
            var spinnerHtml = '<div class="panel-loader"><span class="spinner-small"></span></div>';
            $(target).addClass('panel-loading');
            $(targetBody).prepend(spinnerHtml);

            //setTimeout(function() {
            //    $(target).removeClass('panel-loading');
            //    $(target).find('.panel-loader').remove();
            //}, 2000);
            eval('load_'+target[0].id+'_Page()');
            //RECARREGA OBJETO GLOBAL
            handleLoadGlobalVar();
        }
    });
    
    // expand
    $('[data-click=panel-expand]').live('hover', function() {
        $(this).tooltip({
            title: 'Expandir / Comprimir',
            placement: 'bottom',
            trigger: 'hover',
            container: 'body'
        });
        $(this).tooltip('show');
    });
    $('[data-click=panel-expand]').die('click').live('click', function(e) {       
        e.preventDefault();
        var widgetName = $(this).parent().parent().parent().attr('id');

        //controla o tour quando o usuário remove o widget de mapa
        if(($('#step-8').length > 0) && (widgetName == 'widgetsmapbox')){
            $('#step-8').find('[data-role=next]').click();
        }

        var widgetHeight = $(window).height();
        var target = $(this).closest('.panel');
        var targetParent = target.parent();

        if((varGlobal.lastWidgetInfo.widgetName != '') && (varGlobal.lastWidgetInfo.widgetName != widgetName)){
            var widgetObj = $('#'+varGlobal.lastWidgetInfo.widgetName).closest('.panel');
            widgetObj.removeClass('full-Scr');
            //widgetObj.find('.panel-body').height(varGlobal.lastWidgetInfo.height);
            widgetObj.find('.panel-body').css('height', '');
            if(varGlobal.lastWidgetInfo.widgetName == 'widgetsmapbox'){
                if(typeof(varMapBox.mapInstance) == 'object'){
                    varMapBox.widgetObj.find('#map-content').css('height', varGlobal.lastWidgetInfo.height - 10);
                    window.setTimeout(function() {
                        varMapBox.mapInstance.invalidateSize();
                    }, 500); 
                }
            }
            widgetObj.detach().prependTo($('.dashboard').find('.row').find('.col-md-'+varGlobal.lastWidgetInfo.size+'.dashboard-grid'));            
        }
        if(target.hasClass('full-Scr')){ // VOLTA PRA COL ORIGINAL
            target.removeClass('full-Scr');
            //target.find('.panel-body').height(varGlobal.lastWidgetInfo.height);
            target.find('.panel-body').css('height', '');
            if(widgetName == 'widgetsmapbox'){
                if(typeof(varMapBox.mapInstance) == 'object'){
                    varMapBox.widgetObj.find('#map-content').css('height', varGlobal.lastWidgetInfo.height - 10);
                    window.setTimeout(function() {
                        varMapBox.mapInstance.invalidateSize();
                    }, 500); 
                }
            }
            if($('.slimScrollDiv').length == 0){ try { $('.sidebar-minify-btn').trigger('click'); } catch(err) {} }
            target.detach().prependTo($('.dashboard').find('.row').find('.col-md-'+varGlobal.lastWidgetInfo.size+'.dashboard-grid'));

            //guarda o nome do widget
            varGlobal.lastWidgetInfo.widgetName = '';
        }else{ //JOGA PRA COL 12
            //guarda o nome do widget
            varGlobal.lastWidgetInfo.widgetName = widgetName;
            //guarda o numero de colunas do widget
            if(targetParent.attr('class').indexOf('col-md-12') >= 0){
                varGlobal.lastWidgetInfo.size = 12;
            }else if(targetParent.attr('class').indexOf('col-md-8') >= 0){
                varGlobal.lastWidgetInfo.size = 8;
            }else{
                varGlobal.lastWidgetInfo.size = 4;
            }
            //guarda a altura do widget
            varGlobal.lastWidgetInfo.height = target.find('.panel-body').height(); 

            target.addClass('full-Scr');
            target.find('.panel-body').height(widgetHeight - 40);
            if(widgetName == 'widgetsmapbox'){
                if(typeof(varMapBox.mapInstance) == 'object'){
                    varMapBox.widgetObj.find('#map-content').css('height', widgetHeight - 50);
                    window.setTimeout(function() {
                        varMapBox.mapInstance.invalidateSize();
                    }, 500); 
                }
            }
            if($('.slimScrollDiv').length > 0){ try { $('.sidebar-minify-btn').trigger('click'); } catch(err) {} }
            target.detach().prependTo($('.dashboard').find('.row').find('.col-md-12.dashboard-FullScr'));
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        handleDivFullScr(); 
        handleWidgetNumRows(target);     
       
        //gerenciar cookies
        window.setTimeout(function() {
            handleCookieWidget();
        }, 1000); 
    });
};
var handleWidgetNumRows = function (panelWidget) {
    //var numRows = Math.round(panelWidget.height() / 28) - 5;
    var widgetID = panelWidget.attr('id');
    widgetID = widgetID.replace('widgets', '');
    widgetID = widgetID.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
    widgetID = 'var'+widgetID;
    varWidget = eval(widgetID); //recupera a variavel global do widget

    if(panelWidget.hasClass('full-Scr')){
        if(typeof(varWidget) == 'object'){
            if(typeof(varWidget.rowSizeMax) == 'number'){
                var numRows = Math.round(panelWidget.height() / varWidget.rowSizeMax) - varWidget.rowAdjust;
                varWidget.api.forEach(function(entry) {
                    entry.context[0]._iDisplayLength = numRows;
                    entry.draw(false);
                }); 
            }
        }
    }else{
        if(typeof(varWidget) == 'object'){
            if(typeof(varWidget.rowSizeMin) == 'number'){
                varWidget.api.forEach(function(entry) {
                    entry.context[0]._iDisplayLength = varWidget.rowSizeMin;
                    entry.draw(false);
                }); 
            }
        }
    }
}
var handleDivFullScr = function() {
    if($.trim($('.dashboard').find('.row').find('.col-md-12.dashboard-FullScr').html()).length == 0){
        $('.dashboard').find('.row').find('.col-md-12.dashboard-FullScr').addClass('hidden');
    }else{
        $('.dashboard').find('.row').find('.col-md-12.dashboard-FullScr').removeClass('hidden');
    }
}
/* 07. Handle Panel - Draggable
------------------------------------------------ */
var handleDraggablePanel = function() {
    "use strict";

    var target = $('.panel').parent('[class*=col]');
    var targetHandle = '.panel-heading';
    var connectedTarget = '.row > [class*=col]';
    
    $(target).sortable({
        handle: targetHandle,
        connectWith: connectedTarget,
        stop: function(event, ui) {
            ui.item.find('.panel-title').append('<i class="fa fa-refresh fa-spin m-l-5" data-id="title-spinner"></i>');
            handleSavePanelPosition(ui.item);

            console.log('trocou..' + ui);
            //controla o tour quando o usuário move o widget de lugar
            if(($('#step-7').length > 0) && (ui.item.attr('id') == 'widgetsmapbox')){
                if($('#widgetsmapbox').length > 0){
                    $('#step-7').find('[data-role=next]').click();
                }
            }
        }
    });
};
/* 08. Handle Tooltip & Popover Activation
------------------------------------------------ */
var handelTooltipPopoverActivation = function() {
    "use strict";
    $('[data-toggle=tooltip]').tooltip();
    $('[data-toggle=popover]').popover();
};
/* 09. Handle Scroll to Top Button Activation
------------------------------------------------ */
var handleScrollToTopButton = function() {
    "use strict";
    $(document).scroll( function() {
        var totalScroll = $(document).scrollTop();

        if (totalScroll >= 200) {
            $('[data-click=scroll-top]').addClass('in');
        } else {
            $('[data-click=scroll-top]').removeClass('in');
        }
    });

    $('[data-click=scroll-top]').die('click').die('click').live('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("body").offset().top
        }, 500);
    });
};
/* 10. Handle Theme & Page Structure Configuration - added in V1.2
------------------------------------------------ */
var handleThemePageStructureControl = function() {
    // COOKIE - Theme File Setting
    if ($.cookie && $.cookie('theme')) {
        if ($('.theme-list').length !== 0) {
            $('.theme-list [data-theme]').closest('li').removeClass('active');
            $('.theme-list [data-theme="'+ $.cookie('theme') +'"]').closest('li').addClass('active');
        }
        var cssFileSrc = 'assets/css/theme/' + $.cookie('theme') + '.css';
        $('#theme').attr('href', cssFileSrc);
    }
    
    // COOKIE - Sidebar Styling Setting
    if ($.cookie && $.cookie('sidebar-styling')) {
        if ($('.sidebar').length !== 0 && $.cookie('sidebar-styling') == 'grid') {
            $('.sidebar').addClass('sidebar-grid');
            $('[name=sidebar-styling] option[value="2"]').prop('selected', true);
        }
    }
    
    // COOKIE - Header Setting
    if ($.cookie && $.cookie('header-styling')) {
        if ($('.header').length !== 0 && $.cookie('header-styling') == 'navbar-inverse') {
            $('.header').addClass('navbar-inverse');
            $('[name=header-styling] option[value="2"]').prop('selected', true);
        }
    }
    
    // COOKIE - Gradient Setting
    if ($.cookie && $.cookie('content-gradient')) {
        if ($('#page-container').length !== 0 && $.cookie('content-gradient') == 'enabled') {
            $('#page-container').addClass('gradient-enabled');
            $('[name=content-gradient] option[value="2"]').prop('selected', true);
        }
    }
    
    // COOKIE - Content Styling Setting
    if ($.cookie && $.cookie('content-styling')) {
        if ($('body').length !== 0 && $.cookie('content-styling') == 'black') {
            $('body').addClass('flat-black');
            $('[name=content-styling] option[value="2"]').prop('selected', true);
        }
    }
    
    // THEME - theme selection
    $('.theme-list [data-theme]').die('click').live('click', function() {
        var cssFileSrc = 'assets/css/theme/' + $(this).attr('data-theme') + '.css';
        $('#theme').attr('href', cssFileSrc);
        $('.theme-list [data-theme]').not(this).closest('li').removeClass('active');
        $(this).closest('li').addClass('active');
        $.cookie('theme', $(this).attr('data-theme'));
    });
    
    // HEADER - inverse or default
    $('.theme-panel [name=header-styling]').live('change', function() {
        var targetClassAdd = ($(this).val() == 1) ? 'navbar-default' : 'navbar-inverse';
        var targetClassRemove = ($(this).val() == 1) ? 'navbar-inverse' : 'navbar-default';
        $('#header').removeClass(targetClassRemove).addClass(targetClassAdd);
        $.cookie('header-styling',targetClassAdd);
    });
    
    // SIDEBAR - grid or default
    $('.theme-panel [name=sidebar-styling]').live('change', function() {
        if ($(this).val() == 2) {
            $('#sidebar').addClass('sidebar-grid');
            $.cookie('sidebar-styling', 'grid');
        } else {
            $('#sidebar').removeClass('sidebar-grid');
            $.cookie('sidebar-styling', 'default');
        }
    });
    
    // CONTENT - gradient enabled or disabled
    $('.theme-panel [name=content-gradient]').live('change', function() {
        if ($(this).val() == 2) {
            $('#page-container').addClass('gradient-enabled');
            $.cookie('content-gradient', 'enabled');
        } else {
            $('#page-container').removeClass('gradient-enabled');
            $.cookie('content-gradient', 'disabled');
        }
    });
    
    // CONTENT - default or black
    $('.theme-panel [name=content-styling]').live('change', function() {
        if ($(this).val() == 2) {
            $('body').addClass('flat-black');
            $.cookie('content-styling', 'black');
        } else {
            $('body').removeClass('flat-black');
            $.cookie('content-styling', 'default');
        }
    });
    
    // SIDEBAR - fixed or default
    $('.theme-panel [name=sidebar-fixed]').live('change', function() {
        if ($(this).val() == 1) {
            if ($('.theme-panel [name=header-fixed]').val() == 2) {
                alert('Default Header with Fixed Sidebar option is not supported. Proceed with Fixed Header with Fixed Sidebar.');
                $('.theme-panel [name=header-fixed] option[value="1"]').prop('selected', true);
                $('#header').addClass('navbar-fixed-top');
                $('#page-container').addClass('page-header-fixed');
            }
            $('#page-container').addClass('page-sidebar-fixed');
            if (!$('#page-container').hasClass('page-sidebar-minified')) {
                generateSlimScroll($('.sidebar [data-scrollbar="true"]'));
            }
        } else {
            $('#page-container').removeClass('page-sidebar-fixed');
            if ($('.sidebar .slimScrollDiv').length !== 0) {
                if ($(window).width() <= 979) {
                    $('.sidebar').each(function() {
                        if (!($('#page-container').hasClass('page-with-two-sidebar') && $(this).hasClass('sidebar-right'))) {
                            $(this).find('.slimScrollBar').remove();
                            $(this).find('.slimScrollRail').remove();
                            $(this).find('[data-scrollbar="true"]').removeAttr('style');
                            var targetElement = $(this).find('[data-scrollbar="true"]').parent();
                            var targetHtml = $(targetElement).html();
                            $(targetElement).replaceWith(targetHtml);
                        }
                    });
                } else if ($(window).width() > 979) {
                    $('.sidebar [data-scrollbar="true"]').slimScroll({destroy: true});
                    $('.sidebar [data-scrollbar="true"]').removeAttr('style');
                }
            }
            if ($('#page-container .sidebar-bg').length === 0) {
                $('#page-container').append('<div class="sidebar-bg"></div>');
            }
        }
    });
    
    // HEADER - fixed or default
    $('.theme-panel [name=header-fixed]').live('change', function() {
        if ($(this).val() == 1) {
            $('#header').addClass('navbar-fixed-top');
            $('#page-container').addClass('page-header-fixed');
            $.cookie('header-fixed', true);
        } else {
            if ($('.theme-panel [name=sidebar-fixed]').val() == 1) {
                alert('Default Header with Fixed Sidebar option is not supported. Proceed with Default Header with Default Sidebar.');
                $('.theme-panel [name=sidebar-fixed] option[value="2"]').prop('selected', true);
                $('#page-container').removeClass('page-sidebar-fixed');
                if ($('#page-container .sidebar-bg').length === 0) {
                    $('#page-container').append('<div class="sidebar-bg"></div>');
                }
            }
            $('#header').removeClass('navbar-fixed-top');
            $('#page-container').removeClass('page-header-fixed');
            $.cookie('header-fixed', false);
        }
    });
};
/* 11. Handle Theme Panel Expand - added in V1.2
------------------------------------------------ */
var handleThemePanelExpand = function() {
    $('[data-click="theme-panel-expand"]').die('click').live('click', function() {
        var targetContainer = '.theme-panel';
        var targetClass = 'active';
        if ($(targetContainer).hasClass(targetClass)) {
            $(targetContainer).removeClass(targetClass);
        } else {
            $(targetContainer).addClass(targetClass);
        }
    });
};
/* 12. Handle After Page Load Add Class Function - added in V1.2
------------------------------------------------ */
var handleAfterPageLoadAddClass = function() {
    if ($('[data-pageload-addclass]').length !== 0) {
        $(window).load(function() {
            $('[data-pageload-addclass]').each(function() {
                var targetClass = $(this).attr('data-pageload-addclass');
                $(this).addClass(targetClass);
            });
        });
    }
};
/* 13. Handle Save Panel Position Function - added in V1.5
------------------------------------------------ */
var handleSavePanelPosition = function(element) {
    "use strict";
    if ($('.ui-sortable').length !== 0) {
        var newValue = [];
        var index = 0;
        var size = '8';
        var listWidgetsCookie = [];
        $.when($('.ui-sortable').each(function() {
            var panelSortableElement = $(this).find('[data-sortable-id]');
            if($(panelSortableElement.context).hasClass('col-md-4')){
                size = '4';
            }else if($(panelSortableElement.context).hasClass('col-md-8')){
                size = '8';
            }else if($(panelSortableElement.context).hasClass('col-md-12')){
                size = '12';
            }

            if (panelSortableElement.length !== 0) {
                var columnValue = [];
                $(panelSortableElement).each(function() {
                    var targetSortId = $(this).attr('data-sortable-id');                    
                    var widget = $(this).attr('id');
                    if(typeof(widget) != 'undefined'){
                        var path = widget.replace('widgets', '');
                        listWidgetsCookie.unshift('#widgets/'+path+'/'+path+'.html|'+$(this).attr('id')+'|'+size);
                    }
                    columnValue.push({id: targetSortId});
                });
                newValue.push(columnValue);
            } else {
                newValue.push([]);
            }
            index++;
        })).done(function() {
            var targetPage = window.location.href;
                targetPage = targetPage.split('?');
                targetPage = targetPage[0];
            localStorage.setItem(targetPage, JSON.stringify(newValue));
            $(element).find('[data-id="title-spinner"]').delay(500).fadeOut(500, function() {
                $(this).remove();
            });
            if(typeof(varMapBox) == 'object'){
                window.setTimeout(function() {
                    if(typeof(varMapBox.mapInstance) == 'object'){
                        varMapBox.mapInstance.invalidateSize();
                    }
                }, 500); 
            }            
            handleDivFullScr();
            var cookie = listWidgetsCookie.join();
            var day;
            if(cookie == ''){
                day = -1;
            }
            handleSaveCookieWidget(varGlobal.widgetCookie, cookie, day);
        });
    }
};
/* 14. Handle Draggable Panel Local Storage Function - added in V1.5
------------------------------------------------ */
var handleLocalStorage = function() {
    "use strict";
    if (typeof(Storage) !== 'undefined') {
        var targetPage = window.location.href;
            targetPage = targetPage.split('?');
            targetPage = targetPage[0];
        var panelPositionData = localStorage.getItem(targetPage);
        
        if (panelPositionData) {
            panelPositionData = JSON.parse(panelPositionData);
            var i = 0;
            $('.panel').parent('[class*="col-"]').each(function() {
                var storageData = panelPositionData[i]; 
                var targetColumn = $(this);
                if(storageData != undefined){
                    $.each(storageData, function(index, data) {
                        var targetId = '[data-sortable-id="'+ data.id +'"]';
                        if ($(targetId).length !== 0) {
                            var targetHtml = $(targetId).clone();
                            $(targetId).remove();
                            $(targetColumn).append(targetHtml);
                        }
                    });
                }
                i++;
            });
        }
    } else {
        alert('Your browser is not supported with the local storage'); 
    }
};
/* 15. Handle Reset Local Storage - added in V1.5
------------------------------------------------ */
var handleResetLocalStorage = function() {
    "use strict";
    $('[data-click=reset-local-storage]').die('click').live('click', function(e) {
        e.preventDefault();
        
        var targetModalHtml = ''+
        '<div class="modal fade" data-modal-id="reset-local-storage-confirmation">'+
        '    <div class="modal-dialog">'+
        '        <div class="modal-content">'+
        '            <div class="modal-header">'+
        '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
        '                <h4 class="modal-title"><i class="fa fa-refresh m-r-5"></i> Reset Local Storage Confirmation</h4>'+
        '            </div>'+
        '            <div class="modal-body">'+
        '                <div class="alert alert-info m-b-0">Would you like to RESET all your saved widgets and clear Local Storage?</div>'+
        '            </div>'+
        '            <div class="modal-footer">'+
        '                <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal"><i class="fa fa-close"></i> No</a>'+
        '                <a href="javascript:;" class="btn btn-sm btn-inverse" data-click="confirm-reset-local-storage"><i class="fa fa-check"></i> Yes</a>'+
        '            </div>'+
        '        </div>'+
        '    </div>'+
        '</div>';
        
        $('body').append(targetModalHtml);
        $('[data-modal-id="reset-local-storage-confirmation"]').modal('show');
    });
    $('[data-modal-id="reset-local-storage-confirmation"]').live('hidden.bs.modal', function() {
        $('[data-modal-id="reset-local-storage-confirmation"]').remove();
    });
    $('[data-click=confirm-reset-local-storage]').die('click').live('click', function(e) {
        e.preventDefault();
        var localStorageName = window.location.href;
            localStorageName = localStorageName.split('?');
            localStorageName = localStorageName[0];
        localStorage.removeItem(localStorageName);
        window.location.href = document.URL;
        location.reload();
    });
};
/* 16. Handle Ajax Page Load - added in V1.5
------------------------------------------------ */
var handleLoadPage = function(hash) {
    Pace.restart();
    var targetUrl = hash.replace('#','');
    $('.jvectormap-label, .jvector-label, .AutoFill_border ,#gritter-notice-wrapper, .ui-autocomplete, .colorpicker, .FixedHeader_Header, .FixedHeader_Cloned .lightboxOverlay, .lightbox').remove();
    $.ajax({
        async: true,
        type: 'POST',
        url: targetUrl,	//with the page number as a parameter
        dataType: 'html',	//expect html to be returned
        success: function(data) {
            $('#ajax-content').html(data);
            $('html, body').animate({
                scrollTop: $("body").offset().top
            }, 250);
        },
        error: function(error) {
            $('#ajax-content').html(default_content);  
        }
    });  
};

var handleLoadCookieWidget = function() {
    var cookies = document.cookie;
    if(cookies != undefined && cookies.length > 0) {
        var c = cookies.split(';');
        c.forEach(function(entry) {
            if(entry.indexOf(varGlobal.widgetCookie) >= 0){
                //panelWidgets=#widgets/mapbox/mapbox.html|widgetsmapbox|4@#widgets/unitmonitor/unitmonitor.html|widgetsunitmonitor|8
                var widgets = entry.replace(varGlobal.widgetCookie+"=", "");
                widgets = widgets.split(',');
                widgets.forEach(function(c) {
                    var w = c.split('|');
                    var hash = w[0];
                    var widget = w[1];
                    var type = w[2];

                    handleLoadWidget(hash, widget, type, false);
                });
            }
        });
    }
    handleDivFullScr();
}

var handleCookieWidget = function() {
    "use strict";
    if ($('.ui-sortable').length !== 0) {
        var size = '8';
        var listWidgetsCookie = [];
        //$.when($('.ui-sortable').each(function() {
        $.when($('.dashboard-grid').each(function() {
            var panelSortableElement = $(this).find('[data-sortable-id]');
            if($(panelSortableElement.context).hasClass('col-md-4')){
                size = '4';
            }else if($(panelSortableElement.context).hasClass('col-md-8')){
                size = '8';
            }else if($(panelSortableElement.context).hasClass('col-md-12')){
                size = '12';
            }
            if (panelSortableElement.length !== 0) {
                $(panelSortableElement).each(function() {                  
                    var widget = $(this).attr('id');
                    if(typeof(widget) != 'undefined'){
                        var path = widget.replace('widgets', '');
                        listWidgetsCookie.unshift('#widgets/'+path+'/'+path+'.html|'+$(this).attr('id')+'|'+size);
                    }
                });
            }
        })).done(function() { 
            //SALVANDO WIDGET EM FULL SCR EM SUA POSIÇÃO ORIGINAL
            var widgetfullScr = $('.dashboard-FullScr').find('[data-sortable-id]');
            var widget = widgetfullScr.attr('id');
            if(typeof(widget) != 'undefined'){
                var path = widget.replace('widgets', '');
                listWidgetsCookie.unshift('#widgets/'+path+'/'+path+'.html|'+widgetfullScr.attr('id')+'|'+varGlobal.lastWidgetInfo.size);
            }

            handleDivFullScr();          
            var cookie = listWidgetsCookie.join();
            var day;
            if(cookie == ''){
                day = -1;
            }
            handleSaveCookieWidget(varGlobal.widgetCookie, cookie, day);
        });
    }
};

var handleSaveCookieWidget = function (cookieName, cookie, days){
    //remove o cookie caso ele já exista 
    //####################################
    var dateToRemoveCookie = new Date();
    dateToRemoveCookie.setTime(dateToRemoveCookie.getTime()+(-1*24*60*60*1000));
    expiresToRemoveCookie = "; expires=" + dateToRemoveCookie.toUTCString();

    document.cookie = cookieName +'='+ cookie + expiresToRemoveCookie + ";";
    document.cookie = cookieName +'='+ cookie + expiresToRemoveCookie + "; path=/";
    //####################################

    var expires = '';
    if(typeof(days) == 'number'){
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();            
    }
    
    document.cookie = cookieName +'='+ cookie + expires;
}

var handleLoadWidget = function(hash, widgetName, type, hasCookie) {
    //Não permite que mais de um widget igual, seja aberto;
    if($('#'+widgetName).length > 0){ return false; }
    var targetUrl = hash.replace('#','');
    $.ajax({
        async: true,
        type: 'POST',
        url: targetUrl, //with the page number as a parameter
        dataType: 'html',   //expect html to be returned
        success: function(content) {
            //Não permite que mais de um widget igual, seja aberto;
            if($('#'+widgetName).length > 0){ return false; }

            var firstWidgetObj;
            var rowSmall =  $('.dashboard').find('.row').find('.col-md-4.dashboard-grid');
            var rowMedium = $('.dashboard').find('.row').find('.col-md-8.dashboard-grid');  
            var rowLarge = $('.dashboard').find('.row').find('.col-md-12.dashboard-grid');        

            if(type == '4'){
                firstWidgetObj = rowSmall.find('.panel')[0];
                if(typeof(rowSmall.find('.panel')[0]) == 'undefined'){
                    firstWidgetObj = rowSmall;
                    $(firstWidgetObj).append(content);
                }else{
                    firstWidgetObj = rowSmall.find('.panel')[0];
                    $(firstWidgetObj).before(content);
                }
            }else if(type == '8'){
                firstWidgetObj = rowMedium.find('.panel')[0];
                if(typeof(rowMedium.find('.panel')[0]) == 'undefined'){
                    firstWidgetObj = rowMedium;
                    $(firstWidgetObj).append(content);
                }else{
                    firstWidgetObj = rowMedium.find('.panel')[0];
                    $(firstWidgetObj).before(content);
                }
            }else if(type == '12'){
                firstWidgetObj = rowLarge.find('.panel')[0];
                if(typeof(rowLarge.find('.panel')[0]) == 'undefined'){
                    firstWidgetObj = rowLarge;
                    $(firstWidgetObj).append(content);
                }else{
                    firstWidgetObj = rowLarge.find('.panel')[0];
                    $(firstWidgetObj).before(content);
                }
            } 

            handleDivFullScr();
            if(typeof(hasCookie) == 'undefined'){
                window.setTimeout(function() {
                    handleCookieWidget();
                }, 1000);
            }           
        },
        error: function(error) {
            //cria mensagem de erro no widget
            createMsgBox('error', error.responseText);  
        }
    });
}

var handleAccessLevel = function(widgetName) {
    var template = widgetName.replace('widgets', '');
    var id = 0;
    //na base de dados as permissões são 1 para leitura e 2 para escrita
    var accessLevel = 1;
    
    //buscando id do widget
    $.each(varGlobal.widgetsInfo, function(i, item) {
        if(item.template == template){
            id = item.id;
        }
    });
    //buscando o nivel de acesso do usuário
    $.each(varGlobal.perms, function(i, item) {
        if(item.widget_id == id){
            accessLevel = item.access_level;
        }
    });
    return accessLevel;
}

var handleLoadUserInfo = function() {
    $.ajax({
        async: true,
        type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'widget/dashboard/'},
        dataType: 'json',
        success: function(data) {
            //carrega variaveis de ambiente
            varGlobal.widgetsInfo = data.infos.widgetsInfo;
            varGlobal.perms = data.userinfo.perms;
            //Carrega dados do usuário
            var userData = data.userinfo.user;
            var imgAccount = '   <img src="assets/skin/1/img/logo_small.png" alt="" />';
            var userMenu = '<div class="image">'+imgAccount +'</div>'+
                                userData.name+' <b class="caret"></b>'+
                                '<small>'+userData.name_account+'</small>';
            // Carrega style personalizado para o cliente
            if(userData.skin == 1){
                handleLoadMyStyle(userData.account_id);  
                imgAccount = '   <img src="assets/skin/'+userData.account_id+'/img/logo_small.png" alt="" />';
                varGlobal.imgRelLogo = userData.imglogo;

                userMenu = '<div class="image">'+imgAccount +'</div>'+
                                userData.name+' <b class="caret"></b>'+
                                '<small>'+userData.name_account+'</small>';
            }


            $('#sidebar').find('.nav .nav-profile .info').html(userMenu);

            varGlobal.userID = userData.id;
            varGlobal.userName = userData.name;
            varGlobal.widgetCookie = varGlobal.widgetCookieName + '_' + varGlobal.userID + '_' + varGlobal.activePanel;
            $('#my_id').val(userData.id);
            $('#my_name').val(userData.name);
            $('#my_login').val(userData.login);

            //Carrega menu do usuário
            var category_name = '';
            var leftMenu = '';
            //leftMenu += '<li class="has-sub">';
            //leftMenu += '   <a href="javascript:;">';
            //leftMenu += '       <b class="caret pull-right"></b>';
            //leftMenu += '       <i class="fa fa-laptop"></i>';
            //leftMenu += '       <span>Dashboard</span>';
            //leftMenu += '   </a>';
            //leftMenu += '   <ul class="sub-menu">';
            //leftMenu += '       <li class="active"><a href="#grid/dashboard_1.html" data-toggle="ajax">Painel Completo</a></li>';
            //leftMenu += '       <li class="active"><a href="#grid/dashboard_2.html" data-toggle="ajax">Painel 8 x 4</a></li>';
            //leftMenu += '   </ul>';
            //leftMenu += '</li>';
            leftMenu += '<li class="has-sub active">';
            leftMenu += '   <a href="javascript:;">';
            leftMenu += '       <b class="caret pull-right"></b>';
            leftMenu += '       <i class="fa fa-align-left"></i> ';
            leftMenu += '       <span>Widgets</span>';
            leftMenu += '   </a>';
            leftMenu += '   <ul class="sub-menu">';

            $($(data.infos.widgetsInfo)).each(function(i, row){
                if(category_name != row.category_name){             
                    if(category_name != ''){
                        leftMenu += '           </ul>';
                        leftMenu += '       </li>';
                    }
                    category_name = row.category_name;
                    leftMenu += '       <li class="has-sub">';
                    leftMenu += '           <a href="javascript:;" id="'+handlePrepareString(row.category_name).toLowerCase()+'">';
                    leftMenu += '               <b class="caret pull-right"></b>';
                    leftMenu += '               <span class="fa-stack fa-lg icon">';
                    leftMenu += '                   <i class="fa fa-circle fa-stack-2x"></i>';
                    leftMenu += '                   <i class="fa '+row.category_icon+' fa-stack-1x fa-inverse"></i>';
                    leftMenu += '               </span>';
                    leftMenu += '               '+row.category_name;
                    leftMenu += '           </a>';
                    leftMenu += '           <ul class="sub-menu">'; 
                }
                                                
                leftMenu += '                   <li id="'+row.template+'"><a href="javascript:;" onClick="handleLoadWidget($(this).attr(\'id\'), \'widgets'+row.template+'\', \''+row.size+'\');" id="#widgets/'+row.template+'/'+row.template+'.html">';
                leftMenu += '                       <span class="fa-stack fa-lg icon">';
                leftMenu += '                           <i class="fa fa-circle fa-stack-2x"></i>';
                leftMenu += '                           <i class="fa '+row.icon+' fa-stack-1x fa-inverse"></i>';
                leftMenu += '                       </span>';
                leftMenu += '                   </i>'+row.name+'</a></li>';           
            });

            leftMenu += '           </ul>';
            leftMenu += '       </li>';
            leftMenu += '   </ul>';
            leftMenu += '</li>';
            leftMenu += '<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>';

            handleLoadleftMenu(leftMenu, true);            
        },
        error: function(error) {
            //cria mensagem de erro no widget
            createMsgBox('error', error.responseText);  
        }
    });
}

var handleLoadleftMenu = function(leftMenu, fade){
    if(fade){
        $('span.load-unit').fadeOut("slow", function() {
            // Animation complete.
            $('span.load-unit').html('Carregando dados das Unidades...');
            $('span.load-unit').fadeIn("slow", function(){});
        });        
    }
    if(typeof(varGlobal.unitData) == 'object'){
        $('#sidebar').find('.nav.left-menu').html(leftMenu);
        //carrega widgets gravados em cookie
        handleLoadCookieWidget();
        //BEGIN TOUR
        handleStartTour();
    }else{
        window.setTimeout(function() {
            handleLoadleftMenu(leftMenu, false)
        }, 1000);       
    }
}

var handleStartTour = function (){    
    //BEGIN TOUR
    var tour = new Tour({
        orphan: true,
        steps: [
            {
                title: "Tour Sistema Mova",
                content: "Olá "+varGlobal.userName+", bem-vindo ao sistema Mova.<br><br>Esse Tour irá lhe mostrar como usar o novo sistema mova e algumas de suas funcionalidades.<br>Nossa equipe preparou em um novo sistema para você, com muitas melhorias e novas funcionalidades.<br>A qualquer momento você pode interromper nosso tour clicando em cancelar.<br><br> Vamos seguir em frente?<br><br>"
            },
            {
                element: "#operacao",
                title: "Barra de Ferramentas",
                content: "Do lado esquerdo, fica localizada a barra de ferramentas. É nela que você vai encontrar o menu principal e terá acesso a todas as funcionalidades do sistema.<BR><BR>As ferramentas estão agrupadas em categorias, para expandi-las, basta clicar no nome da categoria."
            },
            {
                element: ".nav-profile",
                title: "Editar Perfil",
                content: "No alto, aparece o seu nome, e clicando nele você pode editar o seu perfil de usuário e inclusive alterar o seu nome, seu login e MUDAR SUA SENHA."
            },
            {
                element: ".sidebar-minify-btn",
                title: "Minimizar Barra de Ferramentas",
                content: "Você pode minimizar a barra de ferramentas, assim sobra mais espaço na área de trabalho.<br><br>Minimize a barra para prosseguir com o tour."
            },
            {
                element: ".sidebar-minify-btn",
                title: "Maximizar Barra de Ferramentas",
                placement: "bottom",
                content: "Mesmo com a barra lateral fechada, você consegue ter acesso a todas as ferramentas do sistema, passando o mouse sobre o ícone do menu.<br><br>Para abrir novamente a barra, clique na seta"
            },
            {
                element: "#operacao",
                title: "Abrir Widgets",
                content: "Agora que você já conhece a barra de ferramentas, vamos dar uma olhada nas ferramentas disponíveis. Clique na categoria OPERAÇÃO, encontre e abra o MAPA clicando sobre ele."
            },
            {
                element: "#unitmonitor",
                title: "Operação",
                content: "Agora vamos abrir também o MONITOR DE UNIDADES."
            },
            {
                element: "#widgetsmapbox .panel-heading",
                placement: "bottom",
                title: "Organizando Dashboard",
                content: "Sempre que você abrir uma ferramenta, ela se posicionará na parte de cima da área de trabalho.<BR><BR>Os formulários se posicionarão do lado direito, enquanto o mapa, o monitor de unidade e os relatórios ocuparão a coluna do lado esquerdo.<BR><BR>Você sempre poderá reposicionar as janelas clicando e arrastando para outra posição.<BR><BR>Vamos tentar? Clique e arraste o MAPA para cima, colocando-o no lugar do MONITOR DE UNIDADES"
            },
            {
                element: "#widgetsmapbox .panel-heading-btn",
                placement: "left",
                title: "Controles dos Widgets",
                content: "Na parte superior de cada janela, você encontra os controles para expandir, minimizar e fechar a janela.<BR><BR>Algumas janelas também possuem um menu de ações. Explore-os para conhecer as opções de cada ferramenta."
            },
            {
                title: "Fim do Tour",
                content: "Agora você já sabe como operar o sistema Mova.<BR><BR>A qualquer momento você pode ver esse tour novamente acessando a categoria Ajuda do menu principal.<BR><BR>Você encontrará também outros tutoriais dentro das ferramentas.<BR><BR>E sempre que precisar nossa equipe de suporte estará a sua disposição."
            }
        ]
    });
    // Initialize the tour
    tour.init();
    // Start the tour
    tour.start();
    tour.goTo(0);

    $('#step-0 [data-role=next]').addClass('p-10 p-l-10 p-r-10 btn btn-primary m-r-2 btn-sm');
    $('#step-0 [data-role=next]').html('Ok, vamos lá!');
    $('#step-0 [data-role=end]').addClass('p-10 p-l-10 p-r-10 btn btn-default m-r-2 btn-sm');
    $('#step-0 [data-role=end]').html('Agora não.');

    $('#sidebar').find('[data-click=sidebar-minify]').on('click', function (){
        if($('#step-3').length > 0){
            $('#step-3').find('[data-role=next]').click();
        }
        if($('#step-4').length > 0){
            window.setTimeout(function() {
                if($('#operacao').length > 0){
                    $('#step-4').find('[data-role=next]').click();
                }
            }, 500);
        }
    });
    $('#sidebar').find('#mapbox').on('click', function (){
        if($('#step-5').length > 0){
            if($('#unitmonitor').length > 0){
                $('#step-5').find('[data-role=next]').click();
            }
        }
    });
    $('#sidebar').find('#unitmonitor').on('click', function (){
        if($('#step-6').length > 0){
            window.setTimeout(function() {
                if($('#widgetsunitmonitor').length > 0){
                    $('#step-6').find('[data-role=next]').click();
                }
            }, 500);
        }
    });
}

var handleSidebarObjectVisibility = function(){    
    //controle de visibilidade do menu do mapa quando sidebar estiver retraida
    //serve para evitar que o menu do mapa fique sobre o menu do sidebar
    $('div#sidebar li.has-sub').die('mouseover').live('mouseover', function() {
        if($('.page-sidebar-fixed').hasClass('page-sidebar-minified')){
            if(typeof(varMapBox) == 'object'){
                varMapBox.widgetObj.find('.leaflet-top.leaflet-left').addClass('hidden');                               
            }                           
        }
    });
    $('div#sidebar li.has-sub').die('mouseout').live('mouseout', function() {
        if(typeof(varMapBox) == 'object'){
            varMapBox.widgetObj.find('.leaflet-top.leaflet-left').removeClass('hidden');      
        }               
    });
}

var handlePrepareString = function(palavra) { 
    com_acento = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ'; 
    sem_acento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC'; 
    nova=''; 
    for(i=0;i<palavra.length;i++) { 
        if (com_acento.search(palavra.substr(i,1))>=0) { 
            nova+=sem_acento.substr(com_acento.search(palavra.substr(i,1)),1); 
        } 
        else { 
            nova+=palavra.substr(i,1); 
        } 
    } 
    return nova; 
}

var handleLoadGlobalVar = function(){
    $.ajax({
        async: true,
        type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'map/loadData/'},
        dataType: 'json',
        cache: false,
        success: function(data) {
            varGlobal.unitData = data;
            window.setTimeout(function() {
                if(typeof(MapBox) == 'object'){
                    //o mapa só pode ser atualizado quando o balão da unidade não estiver aberto
                    if(varMapBox.widgetObj.find('#custom-bubble').length == 0){
                        MapBox.ReloadLayers();                        
                    }
                }
                if(typeof(widgetsUnitMonitor) == 'object'){
                    widgetsUnitMonitor.reloadUnitMonitor(varGlobal.unitData);
                }
                handleLoadGlobalVar();
            }, varGlobal.timeReload); 
        },
        error: function(error) {
            //cria mensagem de erro no widget
            createMsgBox('error', error.responseText);  
        }
    });
}
/* 17. Handle Ajax Page Load Url - added in V1.5
------------------------------------------------ */
var handleCheckPageLoadUrl = function(hash) {
    hash = (hash) ? hash : '#grid/dashboard_2.html';
    
    if (hash === '') {
        $('#ajax-content').html(default_content);
    } else {
        $('.sidebar [href="'+hash+'"][data-toggle=ajax]').trigger('click');
        handleLoadPage(hash);
    }
};
/* 18. Handle Ajax Sidebar Toggle Content - added in V1.5
------------------------------------------------ */
var handleSidebarAjaxClick = function() {
    $('.sidebar [data-toggle=ajax]').die('click').die('click').live('click', function() {
        var targetLi = $(this).closest('li');
        var targetParentLi = $(this).parents();
        $('.sidebar li').not(targetLi).not(targetParentLi).removeClass('active');
        $(targetLi).addClass('active');
        $(targetParentLi).addClass('active');
    });
};
/* 19. Handle Url Hash Change - added in V1.5
------------------------------------------------ */
var handleHashChange = function() {
    $(window).hashchange(function() {
        handleLoadPage(window.location.hash);
    });
};
/* 20. Handle Pace Page Loading Plugins - added in V1.5
------------------------------------------------ */
var handlePaceLoadingPlugins = function() {
    Pace.on('hide', function(){
        $('.pace').addClass('hide');
    });
};
/* 21. Handle IE Full Height Page Compatibility - added in V1.6
------------------------------------------------ */
var handleIEFullHeightContent = function() {
    var userAgent = window.navigator.userAgent;
    var msie = userAgent.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $('.vertical-box-row [data-scrollbar="true"][data-height="100%"]').each(function() {
            var targetRow = $(this).closest('.vertical-box-row');
            var targetHeight = $(targetRow).height();
            $(targetRow).find('.vertical-box-cell').height(targetHeight);
        });
    }
};
/* 22. Handle Unlimited Nav Tabs - added in V1.6
------------------------------------------------ */
var handleUnlimitedTabsRender = function() {
    
    // function handle tab overflow scroll width 
    function handleTabOverflowScrollWidth(obj, animationSpeed) {
        var marginLeft = parseInt($(obj).css('margin-left'));  
        var viewWidth = $(obj).width();
        var prevWidth = $(obj).find('li.active').width();
        var speed = (animationSpeed > -1) ? animationSpeed : 150;
        var fullWidth = 0;

        $(obj).find('li.active').prevAll().each(function() {
            prevWidth += $(this).width();
        });

        $(obj).find('li').each(function() {
            fullWidth += $(this).width();
        });

        if (prevWidth >= viewWidth) {
            var finalScrollWidth = prevWidth - viewWidth;
            if (fullWidth != prevWidth) {
                finalScrollWidth += 40;
            }
            $(obj).find('.nav.nav-tabs').animate({ marginLeft: '-' + finalScrollWidth + 'px'}, speed);
        }

        if (prevWidth != fullWidth && fullWidth >= viewWidth) {
            $(obj).addClass('overflow-right');
        } else {
            $(obj).removeClass('overflow-right');
        }

        if (prevWidth >= viewWidth && fullWidth >= viewWidth) {
            $(obj).addClass('overflow-left');
        } else {
            $(obj).removeClass('overflow-left');
        }
    }
    
    // function handle tab button action - next / prev
    function handleTabButtonAction(element, direction) {
        var obj = $(element).closest('.tab-overflow');
        var marginLeft = parseInt($(obj).find('.nav.nav-tabs').css('margin-left'));  
        var containerWidth = $(obj).width();
        var totalWidth = 0;
        var finalScrollWidth = 0;

        $(obj).find('li').each(function() {
            if (!$(this).hasClass('next-button') && !$(this).hasClass('prev-button')) {
                totalWidth += $(this).width();
            }
        });
    
        switch (direction) {
            case 'next':
                var widthLeft = totalWidth + marginLeft - containerWidth;
                if (widthLeft <= containerWidth) {
                    finalScrollWidth = widthLeft - marginLeft;
                    setTimeout(function() {
                        $(obj).removeClass('overflow-right');
                    }, 150);
                } else {
                    finalScrollWidth = containerWidth - marginLeft - 80;
                }

                if (finalScrollWidth != 0) {
                    $(obj).find('.nav.nav-tabs').animate({ marginLeft: '-' + finalScrollWidth + 'px'}, 150, function() {
                        $(obj).addClass('overflow-left');
                    });
                }
                break;
            case 'prev':
                var widthLeft = -marginLeft;
            
                if (widthLeft <= containerWidth) {
                    $(obj).removeClass('overflow-left');
                    finalScrollWidth = 0;
                } else {
                    finalScrollWidth = widthLeft - containerWidth + 80;
                }
                $(obj).find('.nav.nav-tabs').animate({ marginLeft: '-' + finalScrollWidth + 'px'}, 150, function() {
                    $(obj).addClass('overflow-right');
                });
                break;
        }
    }

    // handle page load active tab focus
    function handlePageLoadTabFocus() {
        $('.tab-overflow').each(function() {
            var targetWidth = $(this).width();
            var targetInnerWidth = 0;
            var targetTab = $(this);
            var scrollWidth = targetWidth;

            $(targetTab).find('li').each(function() {
                var targetLi = $(this);
                targetInnerWidth += $(targetLi).width();
    
                if ($(targetLi).hasClass('active') && targetInnerWidth > targetWidth) {
                    scrollWidth -= targetInnerWidth;
                }
            });

            handleTabOverflowScrollWidth(this, 0);
        });
    }
    
    // handle tab next button click action
    $('[data-click="next-tab"]').die('click').live('click', function(e) {
        e.preventDefault();
        handleTabButtonAction(this,'next');
    });
    
    // handle tab prev button click action
    $('[data-click="prev-tab"]').die('click').live('click', function(e) {
        e.preventDefault();
        handleTabButtonAction(this,'prev');

    });
    
    // handle unlimited tabs responsive setting
    $(window).resize(function() {
        $('.tab-overflow .nav.nav-tabs').removeAttr('style');
        handlePageLoadTabFocus();
    });
    
    handlePageLoadTabFocus();
};

var handleLoadMyStyle = function(account_id) {    
    $('head').append('<link href="assets/skin/'+account_id+'/css/my_style.css" rel="stylesheet" />');
}
/* Application Controller
------------------------------------------------ */
var App = function () {
    "use strict";
    
    return {
        //main function
        init: function () {
            // draggable panel & local storage
            handleDraggablePanel();
            handleLocalStorage();
            handleResetLocalStorage();
        
            // slimscroll
            handleSlimScroll();

            // version Dashboard
            versionDashboard();
            
            // panel
            handlePanelAction();
            
            // tooltip
            handelTooltipPopoverActivation();
            
            // page load
            handlePageContentView();
            handleAfterPageLoadAddClass();
            handlePaceLoadingPlugins();
            
            // scroll to top
            handleScrollToTopButton();
            
            // sidebar
            handleSidebarMenu();
            handleMobileSidebarToggle();
            handleSidebarMinify();
            handleSidebarObjectVisibility();
        
            // theme configuration
            handleThemePageStructureControl();
            handleThemePanelExpand();
            
            // ajax
            handleSidebarAjaxClick();
            handleCheckPageLoadUrl(window.location.hash);
            handleHashChange();
            
            // IE Compatibility
            handleIEFullHeightContent();
            
            // unlimited nav tabs
            handleUnlimitedTabsRender();

            //Carrega dados do usuário assim como seu menu
            handleLoadUserInfo();
            handleLoadGlobalVar();
            
            // ajax cache setup
            $.ajaxSetup({
                cache: true
            });
        },
        setPageTitle: function(pageTitle) {
            document.title = pageTitle;
        },
        restartGlobalFunction: function(panel_id) {
            handleDraggablePanel();
            handleLocalStorage();
            handleSlimScroll();
            handlePanelAction();
            handelTooltipPopoverActivation();
            handleAfterPageLoadAddClass();
            handleUnlimitedTabsRender();
            //carregando cookies 
            deleteVariables();
            varGlobal.activePanel = panel_id;
            varGlobal.widgetCookie = varGlobal.widgetCookieName + '_' + varGlobal.userID + '_' + varGlobal.activePanel;
            window.setTimeout(function() {
                if(typeof(varGlobal.unitData) == 'object'){
                    handleLoadCookieWidget();
                }
            }, 1000); 
        }
    };
}();