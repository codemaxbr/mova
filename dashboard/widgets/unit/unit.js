var varUnit = {};
	varUnit.widgetName 	= 'widgetsunit';
	varUnit.varName 	= 'varUnit';
	varUnit.widgetObj 	= $('#'+varUnit.widgetName);
	varUnit.action 		= 'add';
	varUnit.deviceId 	= 0;
	varUnit.deviceRecord = [];
	varUnit.driverManager = 1;
	varUnit.tableUnitAssocCad;
	varUnit.primaryDevice;

	//VARIAVEIS PARA O COMPONENTE DE ASSOCIAÇÃO DE UNIDADES
	//tabelas do componente de associação de unidades
	varUnit.tableUnit;
	varUnit.tableGroup;
	varUnit.tableUnitAssoc;
	varUnit.tableUnitAssocCad;
	//listas do componente de associação de unidades
	varUnit.Groups = [];
	varUnit.Units = [];
	varUnit.UnitsAssocOri = [];
	varUnit.UnitsAssoc = [];
	//lista do componente de associação de unidades
	varUnit.UnitsSelected = [];
	//FIM DA VARIAVEIS DE ASOCIÇÃO

var widgetsUnit = {	
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	loadEditForm: function (data) {
		var form = varUnit.widgetObj.find('form');
		var driverUnit;

		if(data[0].driver == undefined){
			driverUnit = {data: []};
		}else{
			driverUnit = JSON.parse(data[0].driver);
		}

		//limpa formulário
		widgetsUnit.reset();
		// muda o form para edição
		varUnit.action = 'edit';

		varUnit.widgetObj.find(".btn-group button").attr("disabled",true);
		varUnit.deviceRecord = [];
		varUnit.widgetObj.find(".list-devices").html('');	
		
		form.find("#label").mask("aaa-9999");
		
		form.find("#id").val($(data)[0].id);
		form.find("#account").val($(data)[0].account_id);
		form.find('#label').val($(data)[0].label);
		form.find('#label2').val($(data)[0].label2);
		form.find("#os").val($(data)[0].os_num);
		form.find("#os_id").val($(data)[0].os_id);
		form.find("#cli_fat").val($(data)[0].cli_fat_id);
		if($(data)[0].dst == true) {
			form.find("#dst").attr('checked', true);
		} else {
			form.find("#dst").attr('checked', false);
		}
		
		form.find('#group option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == $(data)[0].group_id) {
				$(this).attr('selected', true);
				widgetsUnit.loadSubGroup(value, "#subgroup", $(data)[0].subgroup_id);
			}

		});
		
		form.find('#unit_type option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == $(data)[0].unit_type_id) {
				$(this).attr('selected', true);
				//Passando id da categoria para selecionar ao renderizar os '<options>'.
				widgetsUnit.loadCategory(value,"#unit_category", $(data)[0].unit_category_id);
			}
		});	

		form.find('#product option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == $(data)[0].intranet_product_id)
				$(this).attr('selected', true);

		});

		var timezone = $("#timezone option");

		$(timezone).each(function(i, obj) {
			if ($(obj).val() == $(data)[0].timezone) {
				$(obj).attr("selected", true);
			}
		});

		if( $(data)[0].devices.length > 0){
			if ($(data)[0].devices[0].length > 0) {
				deviceId = 0;
				varUnit.widgetObj.find("#registered-devices").html('');
				$((data)[0].devices[0]).each(function(i, o) {
					var deviceLabel = '<label class="f-s-11 m-r-3 label label-info device-list">'+
					'	<span class="device-list-label">'+o.identifier+'</span>'+
					'	<input type="hidden" name="device[]" value="'+o.id+'"></input>'+
					'	<a href="javascript:;" class="remove-device m-l-10 pull-right btn btn-xs btn-icon btn-square btn-inverse">'+
					'		<i class="fa fa-times"></i>'+
					'	</a>'+
					'</label>';
					varUnit.widgetObj.find(".list-devices").append(deviceLabel);
				});
				widgetsUnit.handleList();

				widgetsUnit.verifyPrimary(varUnit.widgetObj.find('.label-info'));
			}
		}

		form.find('#status option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == $(data)[0].status) {
				$(this).attr('selected', true);
			}
		});	


		if((driverUnit.data.length > 0) && (varUnit.driverManager == 2)){
			varUnit.widgetObj.find('#assoc-driver-manager').attr('checked', 'checked');
    		varUnit.widgetObj.find('.assoc-validation').css('display', 'block');
		}

		if(varUnit.driverManager != 1){
			varUnit.UnitsAssocOri = [];
			$.each(driverUnit.data, function(index, val) {
				varUnit.UnitsAssocOri.push({
					id: val.id,
					name: val.name,
					description: val.description,
					group_id: val.group_id,
					group_name: val.group_name,
					memory_position: val.memory_position
				});
			});
			widgetsUnit.buildTableUnitAssoc(driverUnit.data, varUnit.widgetObj.find('#create-table-drivers-unit'));
		}

		openForm(varUnit.widgetName, varUnit.action);
	},
	buildUnitType: function (data){
		varUnit.widgetObj.find('#unit_type').html('<option value="">Selecione um tipo</option>');
		var template = 	'{{#typeinfo}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/typeinfo}}';
		var select = Mustache.to_html(template, data);
		varUnit.widgetObj.find('#unit_type').append(select);
	},
	buildGroup: function (data){
		varUnit.widgetObj.find('#group').html('<option value="">Selecione um grupo</option>');
		var template = 	'{{#groupinfo}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/groupinfo}}';
		var select = Mustache.to_html(template, data);
		varUnit.widgetObj.find('#group').append(select);
	},
	buildProduct: function (data){
		varUnit.widgetObj.find('#product').html('<option value="">Selecione um produto</option>');
		var template = 	'{{#productinfo}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/productinfo}}';
		var select = Mustache.to_html(template, data);
		varUnit.widgetObj.find('#product').append(select);
	},
	reset: function (){
		var form = varUnit.widgetObj.find('form');
		varUnit.action = 'add'; 
		form[0].reset();  

		//widgetsUnit.unloadForm();
		// Removendo options
		form.find('#unit_type').prop('selectedIndex', 0);
		form.find('#group').prop('selectedIndex', 0);
		form.find('#product').prop('selectedIndex', 0);
		form.find("#unit_category").html("<option>Selecione um tipo</option>");
		form.find("#subgroup").html("<option>Selecione um grupo</option>");
		form.find('#timezone').prop('selectedIndex', 2);
		form.find("#dst").attr('checked', false);
		form.find(".list-devices").html('');
		form.find('#assoc-driver-manager').removeAttr('checked');
		// Removendo inputs extras
		var devices = form.find("input[name='device[]']");
		$(devices).each( function(idx,obj) {
			if ($(obj).attr("id") != 'device')
				$(obj).remove();
		});

		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');
	},
	delete: function () {
	    var data = { unit_id: varUnit.widgetObj.find("#id").val(), unit_status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'unit/deleteunit',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varUnit.widgetName);
			},
			success : function(data) {
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varUnit.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varUnit.widgetObj.find('#table-list-unit').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varUnit.widgetName);
				widgetsUnit.reset();
				loadingWidget('#'+varUnit.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varUnit.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varUnit.widgetName+' .panel-body');			
			}
		});
		return false;
	},
	getById : function (data) {
	    $.ajax({
			async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'unit/createEditForm/',
				data : JSON.parse(data)
			},
			dataType : 'json',
			success : function(data) {
		        widgetsUnit.loadEditForm(data, varUnit.widgetName);
	        	loadingWidget('#'+varUnit.widgetName, true);
			},
			error : function(error) {
	        	createMsgBox('error', error.responseText, '#'+varUnit.widgetName+' .panel-body');
	        	loadingWidget('#'+varUnit.widgetName, true);
			}
		});
	},
	buildTable: function (){	
		varUnit.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-unit" width="100%"></table>' );
		//Pega os dados da tabela
		var table_unit = varUnit.widgetObj.find('#table-list-unit').dataTable({
			ajax : {
				"url" : 'widgets/routing.php',
				"type" : "POST",
				"data" : { url : 'unit/listTrackedUnit' },
				error: function (error) {
		        	loadingWidget('#'+varUnit.widgetName, true);
					createMsgBox('error', error.responseText, '#'+varUnit.widgetName+' .panel-body');
				}
			},
			paginate : true,
			bLengthChange : false,
			bFilter : true,
			bInfo : true,
			"pagingType" : "full",
		    "fnDrawCallback": function( oSettings ) {
                varUnit.widgetObj.find('.20_p').css('width', '20%');
                varUnit.widgetObj.find('.30_p').css('width', '30%');
		    },
			"columns" : [
					{ "title" : "#", "data" : "id", "class" : "hidden" }, 
					{ 
						"title" : "Placa", 
						"data" : "label" ,
		            	"className": "20_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
					}, 
					{ 
						"title" : "Descrição", 
						"data" : "label2" ,
		            	"className": "30_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
					},
					{ 
						"title" : "Grupo", 
						"data" : "group_name" ,
		            	"className": "30_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
					}, 
					{ 
						"title" : '<div class="nowrap ellipsis" title="Identificador">Identificador</div>', 
						"data" : "identifier" ,
		            	"className": "20_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
					}
			],
			"language" : {
				"emptyTable" : "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
				"infoEmpty" : "Mostrando 0 de 0 de um total de 0 registros",
				"infoFiltered" : "(filtrado de um total de  _MAX_ registros)",
				"infoPostFix" : "",
				"thousands" : ".",
				"lengthMenu" : "Mostre _MENU_ registros",
				"loadingRecords" : "Carregando...",
				"processing" : "Processando...",
				"search" : "",
				"searchPlaceholder" : "Procure por unidades...",
				"zeroRecords" : "Nenhum registro encontrado com esta pesquisa",
				"paginate" : {
					"first" : "<<",
					"last" : ">>",
					"next" : ">",
					"previous" : "<"
				}
			},
			"initComplete" : function(settings, json) {
				if(json.data.length > 0){
                    varUnit.widgetObj.find('.20_p').css('width', '20%');
                	varUnit.widgetObj.find('.30_p').css('width', '30%');
					// CLICANDO NA CONTA PARA EDITÁ-LA
			    	varUnit.widgetObj.find('#table-list-unit tbody').delegate('tr', 'click', function() {
						// Pegando id da conta
						var data = $(this).children('td').html();
			    	    loadingWidget('#'+varUnit.widgetName);	
			    	    widgetsUnit.getById(data);
						
						return false;
					});
				}			
			}
		});
	},
	buildTableUnitAssoc : function (data, tableObj){
		if(typeof(varUnit) != 'object'){return false;}
		tableObj.html( '<table class="table table-striped table-bordered" id="table-list-unit-drivers" width="100%"></table>' );
		//Pega os dados da tabela
		varUnit.tableUnitAssocCad = varUnit.widgetObj.find('#table-list-unit-drivers').DataTable({
		    "data": data,
		    paginate: true,
		    bLengthChange: false,
		    bFilter: false,
		    bInfo: true,
		    "pagingType": "full",
		    "fnDrawCallback": function( oSettings ) {
    			if(typeof(varUnit) != 'object'){return false;}
                varUnit.widgetObj.find('.30_p').css('width', '30%');
                varUnit.widgetObj.find('.60_p').css('width', '60%');
                varUnit.widgetObj.find('.10_p').css('width', '10%');
		    },
		    "columns": 
		    	[
		            { "title": "#", "data": "id", "class": 'hidden' },
		            { "title": "Motorista", "data": "name", "orderable": false, "className": "30_p" },
		            { "title": "Grupo", "data": "group_name", "orderable": false, "className": "60_p" },
		            { "title": "", "data": "status", "orderable": false, "className": "10_p text-center"}
	          	],
          	"columnDefs": 
                [
                    {
                        "targets": [1],
		            	"render": function ( data, type, full, meta ) {
		            		return '<div class="nowrap ellipsis" title="'+data+'">'+data+'</div>';
						}
                    },
                    {
                        "targets": [2],
		            	"render": function ( data, type, full, meta ) {
		            		if((data == null) || (data == '')){
		            			return '';
		            		}else{
		            			return '<div class="nowrap ellipsis" title="'+data+'">'+data+'</div>';
		            		}
						}
                    },
                    {
                        "targets": [3],
		            	"render": function ( data, type, full, meta ) {
		            		if(data == 1){
								return '<div title="Motorista pendente de envio"><i class="fa fa-warning color-stat" style="font-size: 14px;"></i></div>';
		            		}else if(data == 2){
								return '<div title="Motorista enviado"><i class="fa fa-check-circle color-on" style="font-size: 15px;"></div>';
		            		}else{
		            			return '<div title="Não foi possivel enviar o motorista"><i class="fa fa-minus-circle color-off" style="font-size: 15px;"></div>';
		            		}
						}
                    }
                ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por Unidade...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
    			if(typeof(varUnit) != 'object'){return false;}
                varUnit.widgetObj.find('.30_p').css('width', '30%');
                varUnit.widgetObj.find('.60_p').css('width', '60%');
                varUnit.widgetObj.find('.10_p').css('width', '10%');
		  	}
		});
		return false; 	
	},
	loadSubGroup: function (group, target, subgroupid) {
		$.ajax({
			async : true,
			type : "POST",
			data : {
				url : 'unit/listSubGroup',
				data : group
			},
			url : 'widgets/routing.php',
			dataType : "json",
			beforeSend: function(){
				varUnit.widgetObj.find(target).html($('<option></option>').val('').html('Carregando...'));				
			},
			success : function(json) {
				varUnit.widgetObj.find(target).children().remove();
				$(json.subgroupinfo).each( function(index, obj) {
					varUnit.widgetObj.find(target).append($('<option></option>').val(obj.id).html(obj.name));
				});

				varUnit.widgetObj.find(target).val(subgroupid);
			},
			error : function(xhr, ajaxOptions, thrownError) {
				varUnit.widgetObj.find(target).children().remove();
			}
		});
	},
	//Ultimo parametro (categoryId) usado somente no form.edit
	loadCategory: function (type, target, categoryId) {
		$.ajax({
			async : true,
			type : "POST",
			data : {
				url : 'unit/listCategory',
				data : type
			},
			url : 'widgets/routing.php',
			dataType : "json",
			beforeSend: function(){
				varUnit.widgetObj.find(target).html($('<option></option>').val('').html('Carregando...'));				
			},
			success : function(json) {
				varUnit.widgetObj.find(target).children().remove();
				$(json).each( function(index, obj) {
					varUnit.widgetObj.find(target).append($('<option></option>').val(obj.id).html(obj.name));
				});
				varUnit.widgetObj.find(target).val(categoryId);
			},
			error : function(xhr, ajaxOptions, thrownError) {
				varUnit.widgetObj.find(target).children().remove();
			}
		});
	},
	getType: function (category, target, child) {
		$.ajax({
			async : true,
			type : "POST",
			data : {
				url : 'unit/loadTypeByCategory',
				data : category
			},
			url : 'widgets/routing.php',
			dataType : "json",
			beforeSend: function(){
				varUnit.widgetObj.find(target).html($('<option></option>').val('').html('Carregando...'));				
			},
			success : function(json) {
				varUnit.widgetObj.find(target+' option').each(function(i, obj) {
					var value = $(this).attr('value');
					if (value == json[0].id) {
						$(this).attr('selected', true);
					}
				});
				widgetsUnit.loadCategory(json[0].id,child);
			},
			error : function(xhr, ajaxOptions, thrownError) {
				varUnit.widgetObj.find(target).children().remove();
			}
		});
	},
	contains: function (a, obj) {
	   var tam = a.length;
	   
	   for(var i=0; i < tam; i++){
	       if (a[i] === obj) {
	           return true;
	       }
	   }   
	    return false;
	},
	clone: function (target, label) {
		var lastDevice = varUnit.deviceId;
		var newDevice = $(target).clone();
		varUnit.deviceId++;
		varUnit.widgetObj.find(newDevice).attr("id", label + varUnit.deviceId);
		varUnit.widgetObj.find(newDevice).val("");
		if (lastDevice > 0) {
			varUnit.widgetObj.find(newDevice).insertAfter(target + lastDevice);
		} else {
			varUnit.widgetObj.find(newDevice).insertAfter(target);
		}
	},
	removeClone: function (target) {
		var lastDevice = varUnit.deviceId;
		var device = document.getElementById(target+ lastDevice);
		varUnit.widgetObj.find(device).remove();
		if (varUnit.deviceId > 0) {
			--varUnit.deviceId;
		}
	},
	searchOs: function (target, type) {
		$.ajax({
			async : true,
			type : "POST",
			data : {
				url : 'unit/loadOS',
				data : varUnit.widgetObj.find("#os").val()
			},
			url : 'widgets/routing.php',
			dataType : "json",
			beforeSend: function () {
				varUnit.widgetObj.find(target).removeClass("btn-primary");
				varUnit.widgetObj.find(target).addClass("btn-info");
				varUnit.widgetObj.find(target).html('Aguarde');
	        },
			success : function(json) {
				varUnit.widgetObj.find("#os").val($(json)[0].num_os);
				varUnit.widgetObj.find("#os_id").val($(json)[0].id);
				varUnit.widgetObj.find("#label").val($(json)[0].placa);
				varUnit.widgetObj.find("#cli_fat").val($(json)[0].clifatid);
				varUnit.widgetObj.find("#display_client").html($(json)[0].name);
				varUnit.widgetObj.find("#product option").each(function(i, o) {
					if ($(o).val() == $(json)[0].product) {
						$(o).attr("selected", true);
					}
				});
				
				varUnit.widgetObj.find(target).removeClass("btn-info");
				varUnit.widgetObj.find(target).addClass("btn-primary");
				varUnit.widgetObj.find(target).html('<i class=" fa fa-1x fa-search "></i> Buscar');
				varUnit.widgetObj.find("#os").removeClass('parsley-error');
				varUnit.widgetObj.find('#parsley-search-device').html('');			
			},
			error : function(xhr, ajaxOptions, thrownError) {
				varUnit.widgetObj.find(target).removeClass("btn-info");
				varUnit.widgetObj.find(target).addClass("btn-primary");
				varUnit.widgetObj.find(target).html('<i class=" fa fa-1x fa-search "></i> Buscar');
				varUnit.widgetObj.find("#os").addClass('parsley-error');
	        	varUnit.widgetObj.find('#parsley-search-device').html("OS não encontrada.");
				varUnit.widgetObj.find("#os").val('');
				varUnit.widgetObj.find("#os_id").val('');
				varUnit.widgetObj.find("#label").val('');
				varUnit.widgetObj.find("#cli_fat").val('');
				varUnit.widgetObj.find("#display_client").html('');
			}
		});
	},
	handleInclude: function (target, button, appendMessage, list) {
		varUnit.widgetObj.find(button).html("Aguarde");
		var device,deviceExists,newDevice = varUnit.widgetObj.find(target).val();

		if(varUnit.widgetObj.find(target).val().length > 0) {
			if(varUnit.deviceRecord.length > 0) {
				deviceExists = widgetsUnit.contains(varUnit.deviceRecord, newDevice);
				if(deviceExists === true) {
					varUnit.widgetObj.find(target).addClass("parsley-error");
					varUnit.widgetObj.find(appendMessage).html('Dispositivo já adicionado.');
					varUnit.widgetObj.find(button).html('<i class=" fa fa-1x fa-plus-circle "></i> Incluir');
					return false;
				} else {
					if(varUnit.widgetObj.find(target).hasClass("parsley-error")) {
						varUnit.widgetObj.find(target).removeClass("parsley-error");
						varUnit.widgetObj.find(appendMessage).html('');
					}
					varUnit.deviceRecord.push(newDevice);
				}
			} else {
				varUnit.deviceRecord.push(newDevice);
			}
			device = widgetsUnit.checkDevice(newDevice, target, button, appendMessage, list);
		} else {
			varUnit.widgetObj.find(target).addClass("parsley-error");
			varUnit.widgetObj.find(appendMessage).html('Preencha o campo corretamente.');
			varUnit.widgetObj.find(button).html('<i class=" fa fa-1x fa-plus-circle "></i> Incluir');
		}
	},
	handleList: function () {
		varUnit.widgetObj.find(".remove-device").click(function(e) {
			e.preventDefault();
			$('<label class="m-l-3 label label-info" id="search-device-response">Verificando. Aguarde.</label>').insertAfter(varUnit.widgetObj.find(".list-devices").prev().prev());
			
			var deviceId = $(this).prev("input").val();
			var device = $(this);

			var data = {
				unit_id: varUnit.widgetObj.find("#id").val(),	
				device_id: deviceId
				};

			data = JSON.stringify(data);
			$.ajax({
				async : true,
				type : "POST",
				data : {
					url : 'unit/releaseTrackedUnitDevice',
					data : JSON.parse(data)
				},
				url : 'widgets/routing.php',
				dataType : "json",
				success : function(json) {
					device.parent().remove();
					createMsgBox('success', json, '#'+varUnit.widgetName+' .panel-body');

					varUnit.widgetObj.find("#search-device-response").remove();				 
				},
				error : function(erro) {
					createMsgBox('error', error.responseText, '#'+varUnit.widgetName+' .panel-body');
					isRegistered = false;
					return isRegistered;
				}
			});
		});
	},
	checkDevice: function (device, target, button, appendMessage, list) {
		var isRegistered;
		$.ajax({
			async : true,
			type : "POST",
			data : {
				url : 'unit/checkDevice',
				data : device
			},
			url : 'widgets/routing.php',
			dataType : "json",
			beforeSend: function(){
				
			},
			success : function(data) {
				widgetsUnit.handleDevices(data, device, target, button, appendMessage, list);
			},
			error : function(xhr, ajaxOptions, thrownError) {			
				isRegistered = true;
				return isRegistered;
			}
		});	
	},
	handleDevices: function (data, newDevice, target, button, appendMessage, list) {
		var device_id = null;
		var tracked_id = null;

		if(data == false) {
			varUnit.widgetObj.find(target).addClass("parsley-error");
			varUnit.widgetObj.find(appendMessage).html('Dispositivo não cadastrado.');
			varUnit.widgetObj.find(button).html('<i class=" fa fa-1x fa-plus-circle "></i> Incluir');
			varUnit.widgetObj.find(target).val('');
		} else {
			device_id = $(data)[0].device_id;
			tracked_id = $(data)[0].tracked_id;		

			if(tracked_id == null) {
				var deviceLabel = '<label class="f-s-11 m-r-3 label label-info device-list">'+
				'	<span class="device-list-label">'+newDevice+'</span>'+
				'	<input type="hidden" name="device[]" value="'+device_id+'-'+newDevice+'"></input>'+
				'	<a href="javascript:;" class="remove-device m-l-10 pull-right btn btn-xs btn-icon btn-square btn-inverse">'+
				'		<i class="fa fa-times"></i>'+
				'	</a>'+
				'</label>';
	 			varUnit.widgetObj.find(list).append(deviceLabel);
				varUnit.widgetObj.find(button).html('<i class=" fa fa-1x fa-plus-circle "></i> Incluir');
				varUnit.widgetObj.find(target).val('');	

				widgetsUnit.verifyPrimary(varUnit.widgetObj.find(list).find('.label-info'));

			} else {
				varUnit.widgetObj.find(target).addClass("parsley-error");
				varUnit.widgetObj.find(appendMessage).html('O dispositivo já está associado a uma unidade.');
				varUnit.widgetObj.find(button).html('<i class=" fa fa-1x fa-plus-circle "></i> Incluir');
				varUnit.widgetObj.find(target).val('');
			}
		}
	},
	unloadForm: function () {
		varUnit.widgetObj.find(".list-devices").html('');
		varUnit.widgetObj.find("#unit_category").html("<option>Selecione um tipo</option>");
		varUnit.widgetObj.find("#subgroup").html("<option>Selecione um grupo</option>");
		
		varUnit.widgetObj.find("#product option").each(function(i, obj) {
			$(this).attr("selected", false);
		});
		
		var product = varUnit.widgetObj.find("#product option")[0];
		$(product).attr("selected", true);
		varUnit.widgetObj.find("#display_client").html('');
		varUnit.deviceRecord = [];
	},
	verifyPrimary : function (obj) {
		obj.each( function( i ) {
			if( i == 0 ){
				if(!$(this).hasClass('device-list-primary')){
					$(this).addClass('device-list-primary');
					$(this).append('<span class="device-list-label primary"> (primário)</span>')					
				}							
			}
		});
	},
	sendDriver : function () {	
		var data = [];
		var id = varUnit.widgetObj.find("#id").val();

		$.each(varUnit.UnitsAssocOri, function(index, val) {
			data.push({
				type: 'drivers',
				id: id,
				unit_id: id,
				driver_id: val.id,
				memory_position: val.memory_position
			});
		});

		data = JSON.stringify(data);

		$.ajax({
			async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'drivers/resendDriverUnit/',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
	        	loadingWidget('#'+varUnit.widgetName);
			},
			success : function(data) {
	        	loadingWidget('#'+varUnit.widgetName, true);       	        
    	        //Criando mensagem de retorno
    	        createMsgBox('success', data.msg, '#'+varUnit.widgetName+' .panel-body');

			    var info = {
			    	widgetName: varUnit.widgetName,
			    	widgetVar: varUnit.varName
			    }
			    handleAssocSelect.reloadTableUnitAssoc(info, JSON.parse(data.data));
			},
			error : function(error) {
	        	loadingWidget('#'+varUnit.widgetName, true);
	        	//Criando mensagem de retorno
	        	createMsgBox('error', error.responseText, '#'+varUnit.widgetName+' .panel-body');	

			}
		});
		return false;
	}
};

function load_widgetsunit_Page(){
	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'unit/index/'},
        dataType: 'json',
        beforeSend: function () {
        	closeForm(varUnit.widgetName);
			widgetsUnit.reset();	
			varUnit.widgetObj.find('.btn').attr('disabled', 'disabled');
            varUnit.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	widgetsUnit.buildTable();
        	widgetsUnit.buildUnitType(JSON.parse(data.typeinfo));
        	widgetsUnit.buildGroup(JSON.parse(data.groupinfo));
        	widgetsUnit.buildProduct(JSON.parse(data.productinfo));
        	//SET ID account user
        	varUnit.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	varUnit.driverManager = data.userinfo.user.driver_manager;
        	if(varUnit.driverManager == 2){
        		varUnit.widgetObj.find('.assoc-validation').css('display', 'none');
        		varUnit.widgetObj.find('.assoc-validation-check').css('display', 'block');
        	}else if(varUnit.driverManager == 3){
        		varUnit.widgetObj.find('.assoc-validation').css('display', 'block');
        		varUnit.widgetObj.find('.assoc-validation-check').css('display', 'none');
        	}else{
        		varUnit.widgetObj.find('.assoc-validation').css('display', 'none');
        		varUnit.widgetObj.find('.assoc-validation-check').css('display', 'none');
        	}
        	//liberando menu
        	varUnit.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varUnit.widgetName, true); 

        	//Carregando comportamento do widget
        },
        error: function(error) {
        	loadingWidget('#'+varUnit.widgetName, true);
            varUnit.widgetObj.find('.btn-danger').removeAttr("disabled");
			createMsgBox('error', error.responseText, '#'+varUnit.widgetName+' .panel-body');
        }
    });
}

$(document).ready(function() {
	load_widgetsunit_Page();

    //INICIALIZANDO COMPONENTE DE ASSOCIAÇÃO DE UNIDADES
    var info = {
    	widgetName: varUnit.widgetName,
    	btnSaveName: 'Associar e Enviar',
    	widgetVar: 'varUnit',
    	title: 'Associação de motoristas',
    	titletab: 'Motoristas',
    	type: 'drivers',
    	assocUrl: 'drivers/assocDriverUnit/'
    }
    handleAssocSelect.init(info);
    //FIM DO COMPONETE DE ASSOCIAÇÃO DE UNIDADES

	varUnit.widgetObj.find('.openForm').die("click").live("click", function(){
		widgetsUnit.reset();
		varUnit.action = 'add'; 
		openForm(varUnit.widgetName, 'add');
	});
	//CHAMADA PARA EXCLUSÃO DE Devices 
	varUnit.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		openConfirm(varUnit.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir Unidade", "Deseja remover a unidade selecionada?", "Sim", "Não");
	});
	varUnit.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		widgetsUnit.delete();
		closeConfirm(varUnit.widgetName);
	});
	varUnit.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varUnit.widgetName);
	});
	//CHAMADA PARA REENVIAR MOTORISTAS PARA UNIDADE
	varUnit.widgetObj.find('.resend').die("click").live("click", function(){
		openConfirm(varUnit.widgetName, "<i class='fa fa-2x fa-rss' style='vertical-align: middle;'></i> Reenviar Motoristas", "Deseja reenviar todos os motoristas para essa unidade?", "Sim", "Não", "resend_yes", "resend_no");
	});
	varUnit.widgetObj.find('.resend_yes').die("click").live("click", function(){
		widgetsUnit.sendDriver();
		closeConfirm(varUnit.widgetName);
	});
	varUnit.widgetObj.find('.resend_no').die("click").live("click", function(){
		closeConfirm(varUnit.widgetName);
	});
	//Resetando árvores ao clicar em cancelar/fechar
	varUnit.widgetObj.find('form').find('.cancel').die('click').live('click', function() {
		closeForm(varUnit.widgetName);
		widgetsUnit.reset();		
	});

	varUnit.widgetObj.find("#search_os").die("click").live("click", function(){
		widgetsUnit.searchOs("#search_os",'');
		var form = varUnit.widgetObj.find('form');
		form[0].reset();
		
		varUnit.widgetObj.find("#unit_category").html("<option>Selecione um tipo</option>");
		varUnit.widgetObj.find("#subgroup").html("<option>Selecione um grupo</option>");		
	});

	varUnit.widgetObj.find("#label").mask("aaa-9999");
	
	varUnit.widgetObj.find("#device-register").die("click").live("click", function(){
		widgetsUnit.handleInclude("#device", "#device-register", "#parsley-device", ".list-devices");
	});
	
	varUnit.widgetObj.find("#unit_type").change(function() {
		widgetsUnit.loadCategory($(this).val(), "#unit_category");
	});

	varUnit.widgetObj.find("#group").change(function() {
		widgetsUnit.loadSubGroup($(this).val(), "#subgroup", 0);
	});

	varUnit.widgetObj.find(".remove-device").die('click').live('click', function() {
		var device = $(this);		
		var delOfArr = device.parent().text();
		
		device.parent().remove();

		widgetsUnit.verifyPrimary(varUnit.widgetObj.find('.label-info'));
		
		varUnit.deviceRecord = $.grep(varUnit.deviceRecord, function( value, index ) {
			return value != delOfArr;
		});
	});

	varUnit.widgetObj.find('#assoc-driver-manager').die("click").live("click", function(){
		if($(this).attr('checked') == 'checked'){
			$(this).attr('checked', 'checked');
    		varUnit.widgetObj.find('.assoc-validation').css('display', 'block');
    	}else{
			$(this).removeAttr('checked');
    		varUnit.widgetObj.find('.assoc-validation').css('display', 'none');
    	}
	});
	
	// Criando Unidade
	varUnit.widgetObj.find('form').submit( function(e) {
		e.preventDefault();
		if ($(this).parsley().isValid()) {
			
			var form = $(this);
			var devices = form.find("input[name='device[]']");
			var deviceIds = [];

			devices.each(function(index, obj) {
				objAux = $(obj).val();
				deviceIds.push(objAux.split("-")[0]);
			});
			
			var os_num, os_id, cli_fat_id;
			if(form.find("#os").val() == '') {
				os_num = 0;
			} else {
				os_num = form.find("#os").val();
			}
			
			if(form.find("#os_id").val() == '') {
				os_id = 0;
			} else {
				os_id = form.find("#os_id").val();
			}
			
			if(form.find("#cli_fat").val() == '') {
				cli_fat_id = 0;
			} else {
				cli_fat_id = form.find("#cli_fat").val();
			}
			
			var data = {
				tracked_unit: $(this).find("#id").val(),
				account : form.find("#account_id").val(),
				label : form.find('#label').val(),
				label2 : form.find('#label2').val(),
				type : form.find('#type').val(),
				category : parseInt(form.find('#unit_category').val()),
				group : form.find('#group').val(),
				subgroup : form.find('#subgroup').val(),
				product : form.find('#product').val(),
				osnum : os_num,
				osid : os_id,
				clifatid : cli_fat_id,
				timezone : form.find("#timezone").val(),
				status: form.find("#status").val(),
				unit_type:  form.find("#unit_type").val(),
				device : deviceIds,
				dst: form.find("#dst").is(':checked')
			};

			data = JSON.stringify(data);

			$.ajax({
				async: true,
				type : 'POST',
				url : 'widgets/routing.php',
				data : {
					url : 'unit/'+varUnit.action+'/',
					data : JSON.parse(data)
				},
				dataType : 'json',
				beforeSend : function() {
    	        	loadingWidget('#'+varUnit.widgetName);
				},
				success : function(data) {
    	        	loadingWidget('#'+varUnit.widgetName, true);       	        
	    	        //Criando mensagem de retorno
	    	        createMsgBox('success', data, '#'+varUnit.widgetName+' .panel-body');
	    	        // atualiza tabela que lista as contas
	    	        varUnit.widgetObj.find('#table-list-unit').DataTable().ajax.reload(null, false);	    	            	
	    	        //reseta o form	    
					closeForm(varUnit.widgetName);
	    	        widgetsUnit.reset(); 
				},
				error : function(error) {
    	        	loadingWidget('#'+varUnit.widgetName, true);
    	        	//Criando mensagem de retorno
    	        	createMsgBox('error', error.responseText, '#'+varUnit.widgetName+' .panel-body');	

				}
			});
			return false;
		}
	});	
});
