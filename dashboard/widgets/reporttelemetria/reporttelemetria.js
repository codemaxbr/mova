var varReporttelemetria = {};
    varReporttelemetria.widgetName    = 'widgetsreporttelemetria';
    varReporttelemetria.widgetObj     = $('#'+varReporttelemetria.widgetName);
    varReporttelemetria.numEventsG    = 0;
    varReporttelemetria.numEvents     = 0;
    varReporttelemetria.numSearchs    = 0;
    varReporttelemetria.backupElem    = '';
    varReporttelemetria.tabResults    = 0;
    varReporttelemetria.table         = [];
    varReporttelemetria.api           = [];
    varReporttelemetria.dataFilterArr = [];
    varReporttelemetria.processingSearchLimit = 1000;
    varReporttelemetria.callMap;

    varReporttelemetria.rowSizeMax = 28;
    varReporttelemetria.rowSizeMin = 10;
    varReporttelemetria.rowAdjust = 5;

var widgetsReportTelemetria = {
    reset: function(){
        varReporttelemetria.widgetObj.find('form').each(function() { 
            this.reset() 
        });
    },
    createJSTrees: function(json){
        varReporttelemetria.widgetObj.find('.eventTree').jstree({
            "checkbox" : {
                "keep_selected_style" : false
            },
            "core": {
                "data" : json,
                "themes":{ "icons": false }
            },
            "plugins" : [ "checkbox"]
        });
    },
    exportCSV: function(json){
        var json_data = JSON.stringify(json);
        var url_relatorio = '../dashboard-api/class/export/csv/relatorioTelemetria.php';

        postExterno(url_relatorio, {dados: json_data});
    },
    buildGroup: function (data){
        varReporttelemetria.widgetObj.find('#group').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varReporttelemetria.widgetObj.find('#group').append(select);
    },
    buildGroup_motoristas: function (data){
        varReporttelemetria.widgetObj.find('#group_motoristas').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varReporttelemetria.widgetObj.find('#group_motoristas').append(select);
    },
    buildSubGroup: function (data){
        varReporttelemetria.widgetObj.find('#subgroup').html('<option value="0" selected>Todos</option>');
        var template =  '{{#subgroupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/subgroupinfo}}';
        var select = Mustache.to_html(template, data);
        varReporttelemetria.widgetObj.find('#subgroup').append(select);
    },
    processingSearch: function (idTab, dataFilter){
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reporttelemetria/search/', data: dataFilter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }

                varReporttelemetria.widgetObj.find('#table-reporttelemetria-list-result-'+idTab+'_info').append('<span class="sp-load-'+idTab+'" title="Carregando consulta..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }
                //para de processar caso a tab seja fechada
                if(varReporttelemetria.table[idTab] == null ){ return false; }

                $(json).each(function(i, rows) {                
                    varReporttelemetria.table[idTab].row.add({
                        "label": rows.label,
                        "motorista": rows.motorista, 
                        "odometro": rows.odometro, 
                        "rpm": rows.rpm, 
                        "sensor1": rows.sensor1, 
                        "sensor2": rows.sensor2, 
                        "data": rows.data,  
                        "hora_min": rows.hora_min, 
                        "full_address": rows.full_address, 
                        "devst_id": rows.devst_id, 
                        "longitude": rows.longitude, 
                        "latitude": rows.latitude, 
                        "velocidade": rows.velocidade, 
                        "event": rows.event, 
                        "full_date": rows.full_date                        
                    });
                }); 
                varReporttelemetria.table[idTab].draw(false);
                //reinicia o geocode caso haja algum registro para ser carregado
                buildRevGeoFromTable(varReporttelemetria.widgetName);

                //verifica se ainda existem registros para serem buscados
                if( json.length >= varReporttelemetria.processingSearchLimit ){
                    dataFilter.last_id = json[json.length - 1].devst_id;

                    window.setTimeout(function() {
                        widgetsReportTelemetria.processingSearch(idTab, dataFilter); 
                    }, 50);
                }    
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText, '#'+varReporttelemetria.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }
                varReporttelemetria.widgetObj.find('.sp-load-'+idTab).remove();
            }
        });
    },
    print: function (type, id){
        var filter = JSON.parse(varReporttelemetria.dataFilterArr[id]);
        //removendo limit para retornar todos os dados quando for´exportação
        filter.limit = 0;
        filter.last_id = 0;

        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reporttelemetria/search/', data: filter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }
                varReporttelemetria.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
                varReporttelemetria.widgetObj.find('.actionMenu .fristAction').append('<span class="sp-load" title="Carregando exportação..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }
                switch (type) { 
                    case 'print': 
                        var content = '<style>';
                        content += '    table { border: 1px solid #999; border-bottom: none; width: 100%; }';
                        content += '    thead { border: 5px solid #000; }';
                        content += '    tbody td { border: 1px solid #ccc; }';
                        content += '</style>';        
                        content += '<table><thead"><th>Placa</th><th>Motorista</th><th>Data</th><th>Local</th><th>Velocidade</th><th>Odômetro</th><th>RPM</th><th>Evento</th><th>Farol</th><th>Limpador</th></thead><tbody>';
                        
                        $(json).each(function(i, row) {
                            if(row.rpm !== null){ row.rpm; }else{ row.rpm = 0;}
                            if(row.motorista !== null){ row.motorista; }else{ row.motorista = '';}
                            if(row.sensor1 !== false){
                                row.sensor1 = 'Ligado';

                            }else{
                                row.sensor1 = 'Desligado';
                            }

                            if(row.sensor2 !== false){
                                row.sensor2 = 'Ligado';

                            }else{
                                row.sensor2 = 'Desligado';
                            }
                            content += '<tr><td>'+row.label+'</td><td>'+row.motorista+'</td><td>'+row.data_report+' às '+row.hora_min+'</td><td>'+row.full_address+'</td><td>'+row.velocidade+' km/h</td><td>'+row.odometro+' km/h</td><td>'+row.rpm+'</td><td>'+row.event+'</td><td>'+row.sensor1+'</td><td>'+row.sensor2+'</td></tr>';
                        });            
                        content += '</tbody></table>';

                        var w = window.open("");

                        w.document.write(content);
                        w.print();
                        w.close();

                        break;
                    case 'PDF': 
                        var columns = ["Placa", "Motorista", "Data", "Endereço", "Velocidade", "Odômetro", "RPM", "Evento", "Farol", "Limpador"];
                        var rows = [];
                        $(json).each(function(i, row) {
                            if(row.rpm !== null){ row.rpm; }else{ row.rpm = 0;}
                            if(row.motorista !== null){ row.motorista; }else{ row.motorista = '';}

                            if(row.sensor1 !== false){
                                row.sensor1 = 'Ligado';

                            }else{
                                row.sensor1 = 'Desligado';
                            }

                            if(row.sensor2 !== false){
                                row.sensor2 = 'Ligado';

                            }else{
                                row.sensor2 = 'Desligado';
                            }

                            

                            rows.push([row.label, row.motorista, row.data_report+' às '+row.hora_min, row.full_address, row.velocidade+' km/h', row.odometro+' km', row.rpm, row.event, row.sensor1, row.sensor2]);
                            //var canvasSensor1 = row.sensor1;
                            //var canvasSensor2 = row.sensor2;
                        });
                        var canvasON = '<canvas id="sensor1"  class="fa fa-circle fa-1x color-on" style="margin-left: 4px;" title="Entrada on"></canvas>';
                        var canvasOFF = '<canvas id="sensor1" class="fa fa-circle fa-1x color-off-2" style="margin-left: 4px;" title="Entrada off"></canvas>';
                        var doc = new jsPDF('l', 'pt');
                        var dataCurrente = new Date();
                        var year = dataCurrente.getFullYear();
                        var month = dataCurrente.getMonth() + 1;
                        var day = dataCurrente.getDate();



                        doc.autoTable(columns, rows, {
                            theme: 'striped', // 'striped', 'grid' or 'plain'
                            headerStyles: {
                                fillColor: [0, 147, 147],
                                color: [255, 255, 255]
                            },
                            bodyStyles: {
                                overflow: 'linebreak', // visible, hidden, ellipsize or linebreak
                            },
                            margin: {
                                top: 100
                            },
                            beforePageContent: function(data) {
                                var imgData = varGlobal.imgRelLogo;
                                doc.addImage(imgData, 'PNG', 40, 30);
                                //var imgSensor1 = canvasON.toDataURL("image/jpeg", 1.0);
                                //var imgSensor2 = canvasOFF.toDataURL("image/jpeg", 1.0);
                                //doc.addImage(imgSensor1, 'JPEG', 40, 30);
                                //doc.addImage(imgSensor2, 'JPEG', 40, 30);
                                doc.text("Relatório Telemetria", 40, 90);
                            }
                        });
                        doc.save("Relatório_telemetria_"+year+"-"+month+"-"+day+".pdf");

                        break;
                    case 'CSV': widgetsReportTelemetria.exportCSV(json); break;
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReporttelemetria.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }
                varReporttelemetria.widgetObj.find('.dropdown-toggle').removeAttr("disabled"); 
                varReporttelemetria.widgetObj.find('.sp-load').remove(); 
            }
        });            
    },
    openMap: function (lat, lng, drawMark){
        if(typeof(varMapBox) != 'object'){
            handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 4);

            clearTimeout(varReporttelemetria.callMap);
            varReporttelemetria.callMap = window.setTimeout(function() {
                widgetsReportTelemetria.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }
        if(typeof(varMapBox.mapInstance) != 'object'){
            clearTimeout(varReporttelemetria.callMap);
            varReporttelemetria.callMap = window.setTimeout(function() {
                widgetsReportTelemetria.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }

        MapBox.centerUnit(lat, lng, false, null, drawMark);
    }
};

function load_widgetsReportTelemetria_Page(){
    $.ajax({
        async: true,
        type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'reporttelemetria/index/'},
        dataType: 'json',
        beforeSend: function () {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReporttelemetria) != 'object'){ return false; }

            varReporttelemetria.widgetObj.find('.btn').attr('disabled', 'disabled');
            varReporttelemetria.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReporttelemetria) != 'object'){ return false; }
            widgetsReportTelemetria.createJSTrees(JSON.parse(data.events));
            widgetsReportTelemetria.buildGroup(JSON.parse(data.groupinfo));
            widgetsReportTelemetria.buildGroup_motoristas(JSON.parse(data.groupinfo));
            widgetsReportTelemetria.buildSubGroup(JSON.parse(data.subgroupinfo));

            varReporttelemetria.widgetObj.find('form').find('#start_day').val(data.date);
            varReporttelemetria.widgetObj.find('form').find('#end_day').val(data.date);
            varReporttelemetria.widgetObj.find('form').find('#end_hour').val(data.hour);

            varReporttelemetria.widgetObj.find('form').removeClass('hidden');
            varReporttelemetria.widgetObj.find('.load-reporttelemetria').addClass('hidden');
            //liberando menu
            varReporttelemetria.widgetObj.find('.btn').removeAttr("disabled");
            loadingWidget('#'+varReporttelemetria.widgetName, true); 
            varReporttelemetria.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
        },
        error: function(error) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReporttelemetria) != 'object'){ return false; }

            createMsgBox('error', error.responseText, '#'+varReporttelemetria.widgetName+' .panel-body');
        },
        complete: function() {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReporttelemetria) != 'object'){ return false; }
            
            varReporttelemetria.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
    load_widgetsReportTelemetria_Page();

    varReporttelemetria.widgetObj.find('input.range').ionRangeSlider({
        min: 0,
        max: 300,
        type: 'single',
        postfix: "km/h",
        prettify: false,
        hasGrid: true,
        from: 0,
        to: 300,
        step: 1,
        grid_margin: false,
    });

    varReporttelemetria.widgetObj.find('.clockpicker').clockpicker({
        autoclose: true,
        'default': 'now'
    }); 

    varReporttelemetria.widgetObj.find('.pluginDate').datepicker({ 
        autoclose: true, 
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        todayHighlight: true,
    });

    varReporttelemetria.widgetObj.find('select#group_motoristas').on('change', function() {
        var select = 'select#motoristas';
        var data = 
        {
            group_id: $(this).val()
        };
        varReporttelemetria.numEventsG++;
        data = JSON.stringify(data);

        var html = '';
        var selected = 'selected';
        if($(this).val() == 0) {
            html += '<option value="0" selected>Todos</option>';
            selected  = '';
        }
        
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'drivers/list/', data:JSON.parse(data) },
            beforeSend: function () {
                varReporttelemetria.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }

                try {
                    var motoristas = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReporttelemetria.widgetName+' .panel-body');              
                    varReporttelemetria.widgetObj.find(select).html('');
                    varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                html = '';
                html+='<option value="0" selected>Todos</option>';
                $.each($(motoristas.data), function(i, val) {
                    html+='<option value="'+val.id+'" '+selected+'>'+val.name+'</option>';
                });

                varReporttelemetria.numEventsG--;
                if(varReporttelemetria.numEventsG == 0) {
                    varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                    varReporttelemetria.widgetObj.find(select).html(html);
                    varReporttelemetria.widgetObj.find('select#motoristas').change();
                }
            },
            error: function(error) {
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varReporttelemetria.widgetName+' .panel-body');                 
                varReporttelemetria.widgetObj.find(select).html(error.responseText);
                varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
            }
        });     
    }); // Fim Combo Motoristas

    varReporttelemetria.widgetObj.find('select#group').on('change', function() {
        var select = 'select#subgroup';
        var data = 
        {
            groupIds: $(this).val()
        };
        varReporttelemetria.numEventsG++;
        data = JSON.stringify(data);

        var html = '';
        var selected = 'selected';
        if($(this).val() == 0) {
            html += '<option value="0" selected>Todos</option>';
            selected  = '';
        }
        
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'client/listSubGroups/', data:JSON.parse(data) },
            beforeSend: function () {
                varReporttelemetria.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
                varReporttelemetria.widgetObj.find('select#unit').addClass('select-loader').html('<option class="spinner-small select"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }

                try {
                    var subgroups = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReporttelemetria.widgetName+' .panel-body');              
                    varReporttelemetria.widgetObj.find(select).html('');
                    varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                
                $(subgroups).each(function(i, val) {
                    html+='<option value="'+val.id+'" '+selected+'>'+val.name+'</option>';
                });

                varReporttelemetria.numEventsG--;
                if(varReporttelemetria.numEventsG == 0) {
                    varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                    varReporttelemetria.widgetObj.find(select).html(html);
                    varReporttelemetria.widgetObj.find('select#subgroup').change();
                }
            },
            error: function(error) {
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varReporttelemetria.widgetName+' .panel-body');                 
                varReporttelemetria.widgetObj.find(select).html(error.responseText);
                varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
            }
        });     
    });
    
    varReporttelemetria.widgetObj.find('select#subgroup').on('change', function() {
        var select = 'select#unit';

        var html = '';      
        if($(this).val() == '0') {
            html += '<option value="0">Todos</option>';
        }
        var data = 
        {
            subGroupIds: $(this).val()
        };
        varReporttelemetria.numEvents++;
        
        data = JSON.stringify(data);
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'unit/listTrackedUnit/', data:JSON.parse(data) },
            beforeSend: function () {
                varReporttelemetria.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporttelemetria) != 'object'){ return false; }
                
                try {
                    var unities = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReporttelemetria.widgetName+' .panel-body');              
                    varReporttelemetria.widgetObj.find(select).html('');
                    varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                html = '<option value="0" selected>Todos</option>';
                $(unities).each(function(i, val) {
                    if(val.label != undefined){
                        html += '<option value="'+val.id+'">'+val.label+'</option>';                            
                    }
                });
                
                varReporttelemetria.numEvents--;
                if(varReporttelemetria.numEvents == 0) {
                    varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                    varReporttelemetria.widgetObj.find(select).html(html);
                }                   
            },
            error: function(error) {
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varReporttelemetria.widgetName+' .panel-body');                 
                varReporttelemetria.widgetObj.find(select).html(error.responseText);
                varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
            }
        });
    });

    varReporttelemetria.widgetObj.find('input#searchUnits').on('keyup', function(e) {
        e.preventDefault();
        if(varReporttelemetria.backupElem == '') {
            varReporttelemetria.backupElem = varReporttelemetria.widgetObj.find('select#unit').html();
        }
        if($(this).val().length > 2) {
            var select = 'select#unit';
            var html = '';
            var data = 
            {
                unit: $(this).val()
            };
            data = JSON.stringify(data);
            varReporttelemetria.numSearchs++;
             $.ajax({
                async : true,
                type : "POST",
                data: { url: 'unit/searchUnit', data: JSON.parse(data) },
                url : 'widgets/routing.php',
                dataType : "json",
                beforeSend: function () {
                    varReporttelemetria.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
                },
                success : function(json) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReporttelemetria) != 'object'){ return false; }
                
                    varReporttelemetria.numSearchs--;
                    if(json.length <= 0) {
                        if(varReporttelemetria.numSearchs == 0) {
                            varReporttelemetria.widgetObj.find(select).html('');
                            varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                            return false;
                        }
                    }
                    html+='<option value="0" selected>Todos</option>';
                    $(json).each(function(i, val) {
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    });
                    if(varReporttelemetria.numSearchs == 0) {
                        varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                        varReporttelemetria.widgetObj.find(select).html(html);
                    }
                },
                error : function(error) {
                    varReporttelemetria.numSearchs--;
                    if(varReporttelemetria.numSearchs == 0) {
                        createMsgBox('error', error.responseText, "#"+varReporttelemetria.widgetName+" .panel-body");
                        varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                    }
                }
            });
        } else if($(this).val().length <= 2) {
            if(varReporttelemetria.backupElem) {
                varReporttelemetria.widgetObj.find('select#unit').html(varReporttelemetria.backupElem);
                varReporttelemetria.backupElem = '';
            }
        } else {
            varReporttelemetria.backupElem = '';
            return false;       
        }
    });

    varReporttelemetria.widgetObj.find('input#searchDriver').on('keyup', function(e) {
        e.preventDefault();
        if($(this).val().length > 2) {
            var select = 'select#motoristas';
            var html = '';
            var data = 
            {
                name: $(this).val()
            };
            data = JSON.stringify(data);
            varReporttelemetria.numSearchs++;
             $.ajax({
                async : true,
                type : "POST",
                data: { url: 'drivers/searchDriver', data: JSON.parse(data) },
                url : 'widgets/routing.php',
                dataType : "json",
                beforeSend: function () {
                    varReporttelemetria.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
                },
                success : function(json) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReporttelemetria) != 'object'){ return false; }
                
                    varReporttelemetria.numSearchs--;
                    if(json.length <= 0) {
                        if(varReporttelemetria.numSearchs == 0) {
                            varReporttelemetria.widgetObj.find(select).html('');
                            varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                            return false;
                        }
                    }
                    html+='<option value="0" selected>Todos</option>';
                    $(json).each(function(i, val) {
                        html+='<option value="'+val.id+'">'+val.name+'</option>';
                    });
                    if(varReporttelemetria.numSearchs == 0) {
                        varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                        varReporttelemetria.widgetObj.find(select).html(html);
                    }
                },
                error : function(error) {
                    varReporttelemetria.numSearchs--;
                    if(varReporttelemetria.numSearchs == 0) {
                        createMsgBox('error', error.responseText, "#"+varReporttelemetria.widgetName+" .panel-body");
                        varReporttelemetria.widgetObj.find(select).removeClass('select-loader');
                    }
                }
            });
        } 
    });

    varReporttelemetria.widgetObj.find('.closeReportTab').die('click').live('click', function (){
        var id = $(this).attr('id');
        varReporttelemetria.widgetObj.find('#tab-reporttelemetria-result-'+id).empty().remove();
        varReporttelemetria.widgetObj.find('#result-reporttelemetria-'+id).empty().remove();
        varReporttelemetria.widgetObj.find('#search-reporttelemetria').trigger('click');
        window.setTimeout(function() {
            varReporttelemetria.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled'); 
        }, 50);  
        varReporttelemetria.dataFilterArr[id] = null;
        varReporttelemetria.table[id]         = null; 
    });

    varReporttelemetria.widgetObj.find('#unit').die('click').live('click', function (){
        var id = $(this).val();
        var label = $(this).find('option:selected').text();        
        var labelArr = label.match(/.{1,8}/g); //divide a string a cada 8 caracteres
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';

        if(id.length > 1){
            for (i = 0; i < id.length; i++) { 
                var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id[i]+'" title="Click para remover esse unidade">'+labelArr[i]+'</div>';
                var content = varReporttelemetria.widgetObj.find('.selectedUnitList').html();
                if((content.indexOf(div) < 0) && (div != divTodos)){
                    if(content == divTodos){
                        varReporttelemetria.widgetObj.find('.selectedUnitList').html(div);
                    }else{
                        varReporttelemetria.widgetObj.find('.selectedUnitList').append(div);
                    }
                }
            }
        }else{  
            var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id+'" title="Click para remover esse unidade">'+label+'</div>';
            var content = varReporttelemetria.widgetObj.find('.selectedUnitList').html();
            if((content.indexOf(div) < 0) && (div != divTodos)){
                if(content == divTodos){
                    varReporttelemetria.widgetObj.find('.selectedUnitList').html(div);
                }else{
                    varReporttelemetria.widgetObj.find('.selectedUnitList').append(div);
                }  
            }        
        }
    });

    varReporttelemetria.widgetObj.find('#motoristas').die('click').live('click', function (){
        var id = $(this).val();
        var label = $(this).find('option:selected').text();        
        var labelArr = label.match(/.{1,8}/g); //divide a string a cada 8 caracteres
        var divTodos = '<div class="selectedMotorista bg-color-primary no-select" id="0" title="Click para remover esse motorista">Todos</div>';

        if(id.length > 1){
            for (i = 0; i < id.length; i++) { 
                var div = '<div class="selectedMotorista bg-color-primary no-select" id="'+id[i]+'" title="Click para remover esse motorista">'+labelArr[i]+'</div>';
                var content = varReporttelemetria.widgetObj.find('.selectedMotoristasList').html();
                if((content.indexOf(div) < 0) && (div != divTodos)){
                    if(content == divTodos){
                        varReporttelemetria.widgetObj.find('.selectedMotoristasList').html(div);
                    }else{
                        varReporttelemetria.widgetObj.find('.selectedMotoristasList').append(div);
                    }
                }
            }
        }else{  
            var div = '<div class="selectedMotorista bg-color-primary no-select" id="'+id+'" title="Click para remover esse motorista">'+label+'</div>';
            var content = varReporttelemetria.widgetObj.find('.selectedMotoristasList').html();
            if((content.indexOf(div) < 0) && (div != divTodos)){
                if(content == divTodos){
                    varReporttelemetria.widgetObj.find('.selectedMotoristasList').html(div);
                }else{
                    varReporttelemetria.widgetObj.find('.selectedMotoristasList').append(div);
                }  
            }        
        }
    });

    varReporttelemetria.widgetObj.find('.selectedUnit').die('click').live('click', function (){
        $(this).remove();
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';
        var content = varReporttelemetria.widgetObj.find('.selectedUnitList').html();

        if(content == ''){
            varReporttelemetria.widgetObj.find('.selectedUnitList').html(divTodos);
        }
    });

    varReporttelemetria.widgetObj.find('.selectedMotorista').die('click').live('click', function (){
        $(this).remove();
        var divTodos = '<div class="selectedMotorista bg-color-primary no-select" id="0" title="Click para remover esse motorista">Todos</div>';
        var content = varReporttelemetria.widgetObj.find('.selectedMotoristasList').html();

        if(content == ''){
            varReporttelemetria.widgetObj.find('.selectedMotoristasList').html(divTodos);
        }
    });

    varReporttelemetria.widgetObj.find('#search-reporttelemetria').die('click').live('click', function (){
        varReporttelemetria.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');     
    }); 

    varReporttelemetria.widgetObj.find('form').submit(function(e) {
        e.preventDefault();
        if ( $(this).parsley().isValid() ) {  
            var selectedElmsIds = [];
            var selectedElms = $(this).find('.eventTree').jstree("get_selected", true);
            
            selectedElmsIds.push(0);
            $.each(selectedElms, function() {
                if(this.children.length == 0)
                {
                    selectedElmsIds.push(this.id);
                }
            });            
            //Pegando velocidade inicial e final
            var speed = $(this).find('#speed').val();
            
            var unitIds = [];
            var motoristaIds = [];

            varReporttelemetria.widgetObj.find(".selectedUnitList > div").each( function(index, value) {
                unitIds.push($(this).attr('id'));
            });

            varReporttelemetria.widgetObj.find(".selectedMotoristasList > div").each( function(index, value) {
                motoristaIds.push($(this).attr('id'));
            });
            
            var dataFilter = 
                {
                    last_id: 0,
                    start_day: $(this).find('#start_day').val(),
                    end_day: $(this).find('#end_day').val(),
                    start_hour: $(this).find('#start_hour').val(),
                    end_hour: $(this).find('#end_hour').val(),
                    events: selectedElmsIds,
                    group:  $(this).find('select#group').val(),
                    subgroup:  $(this).find('select#subgroup').val(),
                    group_motoristas: $(this).find('select#group_motoristas').val(),
                    units: unitIds,
                    motoristas: motoristaIds,
                    speed: speed,
                    limit: varReporttelemetria.processingSearchLimit
                };
            dataFilter = JSON.stringify(dataFilter);
            
            $.ajax({
                async: true,
                type: 'POST',
                url: 'widgets/routing.php',
                data: { url: 'reporttelemetria/search/', data: JSON.parse(dataFilter) },
                dataType: 'json',
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReporttelemetria) != 'object'){ return false; }

                    loadingWidget('#'+varReporttelemetria.widgetName);
                },
                success: function(data) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReporttelemetria) != 'object'){ return false; }
                
                    varReporttelemetria.dataFilterArr.push(dataFilter);
                    loadingWidget('#'+varReporttelemetria.widgetName, true);

                    varReporttelemetria.widgetObj.find('.tab-content').append('<div class="col-md-12 tab-pane" id="tab-reporttelemetria-result-'+varReporttelemetria.tabResults+'"></div>');
                    varReporttelemetria.widgetObj.find('#widgetsReportTelemetria-nav').append('<li class=""><a href="#tab-reporttelemetria-result-'+varReporttelemetria.tabResults+'"  id="result-reporttelemetria-'+varReporttelemetria.tabResults+'" data-toggle="tab">Resultado <i class="fa fa-times closeReportTab" id="'+varReporttelemetria.tabResults+'"></i></a></li>');

                    varReporttelemetria.widgetObj.find('#result-reporttelemetria-'+varReporttelemetria.tabResults).die('click').live('click', function (){
                        var id = $(this).attr('id');
                        id = id.replace('result-reporttelemetria-', '');

                        var dropDownMenu = '<li><a href="javascript:;" onClick="widgetsReportTelemetria.print(\'print\', '+id+')" class="a_print">Imprimir</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportTelemetria.print(\'PDF\', '+id+')" class="a_pdf">Exportar PDF</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportTelemetria.print(\'CSV\', '+id+')" class="a_csv">Exportar CSV</a></li>';

                        varReporttelemetria.widgetObj.find('#widgetsReportTelemetria-dropdown').html(dropDownMenu);   
                        varReporttelemetria.widgetObj.find('.dropdown-toggle').removeAttr("disabled");                 
                    }); 
                    var numRows = varReporttelemetria.rowSizeMin;
                    var panelWidget = varReporttelemetria.widgetObj.closest('.panel');
                    if(panelWidget.hasClass('full-Scr')){
                        numRows = Math.round(panelWidget.height() / varReporttelemetria.rowSizeMax) - varReporttelemetria.rowAdjust;
                    }
                    varReporttelemetria.widgetObj.find('#tab-reporttelemetria-result-'+varReporttelemetria.tabResults).html( '<table class="table table-striped table-bordered" data-page-length="'+numRows+'" id="table-reporttelemetria-list-result-'+varReporttelemetria.tabResults+'"></table>' );
                    varReporttelemetria.table.push(null);
                    varReporttelemetria.table[varReporttelemetria.tabResults] = varReporttelemetria.widgetObj.find('#table-reporttelemetria-list-result-'+varReporttelemetria.tabResults).DataTable({
                        "data": data,
                        paginate: true,
                        bLengthChange: false,
                        bFilter: false,
                        bInfo: true,
                        "ordering": true,
                        "pagingType": "full",
                        "fnDrawCallback": function( oSettings ) {
                            window.setTimeout(function() {
                                buildRevGeoFromTable(varReporttelemetria.widgetName);
                            }, 500);
                        },
                        "columns": 
                            [
                                
                                { "title": "<div class='labelHeader nowrap ellipsis' title='Placa'>Placa</div>", "data": "label", "className": "larguraPlaca" },
                                { "title": "<div class='labelHeader nowrap ellipsis' title='Motorista'>Motorista</div>", "data": "motorista", "className": "20_p" },
                                { "title": "<div class='labelHeader' title='Data'>Data</div>", "data": "data", "className": "larguraData" },
                                { "title": "<div class='labelHeader nowrap ellipsis' title='Local'>Local</div>", "data": "full_address", "className": "40_p" },
                                { "title": "<div class='labelHeader nowrap ellipsis' title='Velocidade'>Velocidade</div>", "data": "velocidade", "className": "15_p" },
                                { "title": "<div class='labelHeader nowrap ellipsis' title='Odômetro'>Odômetro</div>", "data": "odometro", "className": "15_p" },
                                { "title": "<div class='labelHeader nowrap ellipsis' title='RPM'>RPM</div>", "data": "rpm", "className": "10_p" },
                                { "title": "<div class='labelHeader nowrap ellipsis' title='Evento'>Evento</div>", "data": "event", "className": "20_p" },
                                { "title": "<div class='labelHeader nowrap ellipsis' title='Farol'>Farol</div>", "data": "", "className": "10_p" },
                                { "title": "<div class='labelHeader nowrap ellipsis' title='Limpador'>Limpador</div>", "data": "", "className": "10_p" },
                                { "title": "Date Order", "data": "full_date", "visible": false }
                            ],
                        "columnDefs": 
                            [
                                
                                { //Placa
                                    "targets": [0],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" long="'+full.longitude+'" lat="'+full.latitude+'">' + data + '</div>';
                                    }
                                },
                                { // Motorista
                                    "targets": [1],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        if(data !== null){ data; }else{ data = '';}
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                    }
                                },
                                { // Data
                                    "targets": [2],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + full.data + ' às ' + full.hora_min + '">' + full.data + ' às ' + full.hora_min + '</div>';
                                    }
                                },
                                { // Local
                                    "targets": [3],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        //verifica se este endereco ja existe no banco
                                        if(data != '') {
                                            //se sim ja retorna o endereco
                                            return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                        } else {
                                            //se nao faz o reversegeo para capturar o endereco e salvar no banco
                                            return "<div class='mapAddress nowrap ellipsis' hist='"+full.devst_id+"' long='"+full.longitude+"' lat='"+full.latitude+"'>Carregando...</div>" ;
                                        }
                                    }
                                },
                                { // Velocidade
                                    "targets": [4],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + ' km/h</div>';
                                    }
                                },
                                { // Odometro
                                    "targets": [5],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + ' km</div>';
                                    }
                                },
                                { //RPM
                                    "targets": [6],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        if(data !== null){ data; }else{ data = 0;}
                                        return '<div class="nowrap ellipsis" title="' + data + '">' +  data  + '</div>';
                                    }
                                },
                                { // Evento
                                    "targets": [7],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                    }
                                },
                                { // Farol
                                    "targets": [8],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        if(full.sensor1 !== false){
                                            full.sensor1 = '<i class="fa fa-circle fa-1x color-on" style="margin-left: 4px;" title="Ligado"></i>';

                                        }else{
                                            full.sensor1 = '<i class="fa fa-circle fa-1x color-off-2" style="margin-left: 4px;" title="Desligado"></i>';
                                        }

                                        return '<center><span title="Farol">' + full.sensor1 + '</span></center>';
                                    }
                                },
                                { // Limpador
                                    "targets": [9],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        if(full.sensor2 !== false){
                                            full.sensor2 = '<i class="fa fa-circle fa-1x color-on" style="margin-left: 4px;" title="Ligado"></i>';

                                        }else{
                                            full.sensor2 = '<i class="fa fa-circle fa-1x color-off-2" style="margin-left: 4px;" title="Desligado"></i>';
                                        }

                                        return '<center><span title="Limpador">' + full.sensor2 + '</span></center>';
                                    }
                                }
                            ],
                        "language" : {
                            "emptyTable":     "Ainda não há registros",
                            "info" : "Total de _TOTAL_ registros",
                            "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
                            "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
                            "infoPostFix":    "",
                            "thousands":      ".",
                            "lengthMenu":     "Mostre _MENU_ registros",
                            "loadingRecords": "Carregando...",
                            "processing":     "Processando...",
                            "search":         "",
                            "searchPlaceholder": "Procure por unidade...",
                            "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
                            "paginate": {
                                "first":      "<<",
                                "last":       ">>",
                                "next":       ">",
                                "previous":   "<"
                            }
                        },
                        "order": [[ 5, 'desc' ], [ 0, 'asc' ], [ 1, 'desc' ]],
                        "initComplete": function(settings) {
                            if( data.length > 0){
                                varReporttelemetria.api.push(this.api());

                                varReporttelemetria.widgetObj.find('.larguraData').css('width', '120px');
                                varReporttelemetria.widgetObj.find('.larguraPlaca').css('width', '80px');
                                varReporttelemetria.widgetObj.find('.10_p').css('width', '10%');
                                varReporttelemetria.widgetObj.find('.15_p').css('width', '15%');
                                varReporttelemetria.widgetObj.find('.20_p').css('width', '20%');
                                varReporttelemetria.widgetObj.find('.30_p').css('width', '30%');
                                varReporttelemetria.widgetObj.find('.40_p').css('width', '40%');

                                dataFilter = JSON.parse(dataFilter);
                                dataFilter.last_id = data[data.length - 1].devst_id;
                                if( data.length >= varReporttelemetria.processingSearchLimit ){
                                    widgetsReportTelemetria.processingSearch(varReporttelemetria.tabResults, dataFilter);
                                } 
                                
                                varReporttelemetria.widgetObj.find('#table-reporttelemetria-list-result-'+varReporttelemetria.tabResults+' tbody tr').die("click").live("click", function(){
                                    var label = $(this).children('td').html();
                                    var lat = $(label).attr('lat');
                                    var lng = $(label).attr('long');
                                    label = $(label).html();

                                    var drawMark = [];
                                        drawMark.size = varGlobal.marker.Size;
                                        drawMark.symbol = varGlobal.marker.carSymbol;
                                        drawMark.color = varGlobal.marker.color;

                                    widgetsReportTelemetria.openMap(lat, lng, drawMark);
                                }); 
                            }
                        }
                    });              
                    //reseta o form   
                    varReporttelemetria.widgetObj.find('#result-reporttelemetria-'+varReporttelemetria.tabResults).trigger('click');  
                    varReporttelemetria.tabResults++;
                    //Report.reset(); 
                },
                error: function(error) {
                    loadingWidget('#'+varReporttelemetria.widgetName, true);
                    //Criando mensagem de retorno
                    createMsgBox('error', error.responseText, '#'+varReporttelemetria.widgetName+' .panel-body'); 
                }
            });
            return false;
        }
    }); 
});


