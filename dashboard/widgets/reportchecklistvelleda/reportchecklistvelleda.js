var varReportchecklistvelleda = {};
    varReportchecklistvelleda.widgetName   = 'widgetsreportchecklistvelleda';
    varReportchecklistvelleda.widgetObj    = $('#'+varReportchecklistvelleda.widgetName);
    varReportchecklistvelleda.numEventsG   = 0;
    varReportchecklistvelleda.numEvents    = 0;
    varReportchecklistvelleda.numSearchs   = 0;
    varReportchecklistvelleda.backupElem   = '';
    varReportchecklistvelleda.tabResults   = 0;
    varReportchecklistvelleda.table        = [];
    varReportchecklistvelleda.api           = [];
    varReportchecklistvelleda.dataFilterArr = [];
    varReportchecklistvelleda.processingSearchLimit = 1000;
    varReportchecklistvelleda.callMap;

    varReportchecklistvelleda.rowSizeMax = 28;
    varReportchecklistvelleda.rowSizeMin = 10;
    varReportchecklistvelleda.rowAdjust = 5;

    varReportchecklistvelleda.id = 0;
    varReportchecklistvelleda.user_id = 0;

var widgetsreportchecklistvelleda = {    
    buildGroup: function (data){
        varReportchecklistvelleda.widgetObj.find('#group').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varReportchecklistvelleda.widgetObj.find('#group').append(select);
    },
    processingSearch: function (idTab, dataFilter){
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportchecklistvelleda/search/', data: dataFilter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportchecklistvelleda) != 'object'){ return false; }

                varReportchecklistvelleda.widgetObj.find('#table-list-result-checklistvelleda-'+idTab+'_info').append('<span class="sp-load-'+idTab+'" title="Carregando consulta..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
                //para de processar caso a tab seja fechada
                if(varReportchecklistvelleda.table[idTab] == null ){ return false; }
                
                $(json).each(function(i, rows) {                
                    varReportchecklistvelleda.table[idTab].row.add({
                        "label": rows.label, 
                        "data_inicio": rows.data_inicio,  
                        "data_report_inicio": rows.data_report_inicio,
                        "hora_min_inicio": rows.hora_min_inicio, 
                        "data_fim": rows.data_fim,  
                        "data_report_fim": rows.data_report_fim,
                        "hora_min_fim": rows.hora_min_fim,  
                        "longitude": rows.longitude, 
                        "latitude": rows.latitude, 
                        "full_address": rows.full_address, 
                        "devst_id": rows.id, 
                        "poi_name": rows.poi_name, 
                        "qtd": rows.qtd, 
                        "total_time": rows.total_time, 
                        "full_date_begin": rows.full_date_begin
                    });
                }); 
                varReportchecklistvelleda.table[idTab].draw(false);
                //reinicia o geocode caso haja algum registro para ser carregado
                buildRevGeoFromTable(varReportchecklistvelleda.widgetName);
                //verifica se ainda existem registros para serem buscados
                if( json.length >= varReportchecklistvelleda.processingSearchLimit ){
                    dataFilter.limit += varReportchecklistvelleda.processingSearchLimit;

                    window.setTimeout(function() {
                        widgetsreportchecklistvelleda.processingSearch(idTab, dataFilter); 
                    }, 50);
                }    
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReportchecklistvelleda.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
                varReportchecklistvelleda.widgetObj.find('.sp-load-'+idTab).remove();
            }
        });
    },
    print: function (type, id){
        var filter = JSON.parse(varReportchecklistvelleda.dataFilterArr[id]);
        //removendo limit para retornar todos os dados quando for´exportação
        filter.limit = null;

        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportchecklistvelleda/search/', data: filter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
                varReportchecklistvelleda.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
                varReportchecklistvelleda.widgetObj.find('.actionMenu .fristAction').append('<span class="sp-load" title="Carregando exportação..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
                switch (type) { 
                    case 'print': 
                        var content = '<style>';
                        content += '    table { border: 1px solid #999; border-bottom: none; width: 100%; }';
                        content += '    thead { border: 5px solid #000; }';
                        content += '    tbody td { border: 1px solid #ccc; }';
                        content += '</style>';        
                        content += '<table><thead"><th>Placa</th><th>Data</th><th>Início</th><th>Fim</th><th>Tempo Total</th><th>Local</th><th>Quantidade</th></thead><tbody>';
                        
                        $(json).each(function(i, row) {
                            content += '<tr><td>'+row.label+'</td><td>'+row.data_report_inicio+'</td><td>'+row.hora_min_inicio+'</td><td>'+row.hora_min_fim+'</td><td>'+row.total_time+' min.</td>';
                            content += '<td>'+row.full_address+'</td><td>'+row.qtd+' L</td></tr>';
                        });            
                        content += '</tbody></table>';

                        var w = window.open("");

                        w.document.write(content);
                        w.print();
                        w.close();

                        break;
                    case 'PDF': 
                        var columns = ["Placa", "Data", "Início", "Fim", "Tempo", "Local", "Qtd."];
                        var rows = [];
                        $(json).each(function(i, row) {
                            rows.push([row.label, row.data_report_inicio, row.hora_min_inicio, row.hora_min_fim, row.total_time+' min.', row.full_address, row.qtd+' L']);
                        });   
                        var doc = new jsPDF('l', 'pt');
                        var dataCurrente = new Date();
                        var year = dataCurrente.getFullYear();
                        var month = dataCurrente.getMonth() + 1;
                        var day = dataCurrente.getDate();

                        doc.autoTable(columns, rows, {
                            theme: 'striped', // 'striped', 'grid' or 'plain'
                            headerStyles: {
                                fillColor: [0, 147, 147],
                                color: [255, 255, 255]
                            },
                            bodyStyles: {
                                overflow: 'linebreak', // visible, hidden, ellipsize or linebreak
                            },
                            margin: {
                                top: 100
                            },
                            beforePageContent: function(data) {
                                var imgData = varGlobal.imgRelLogo;
                                doc.addImage(imgData, 'PNG', 40, 30);
                                doc.text("Relatório de Descargas", 40, 90);
                            }
                        });
                        doc.save("Relatorio_de_Descargas"+year+"-"+month+"-"+day+".pdf");

                        break;
                    case 'CSV': 
                        var content = 'Placa;Data;Início;Fim;Tempo Total;Local;Quantidade\r\n';
                        
                        $(json).each(function(i, row) {
                            content += row.label+';'+row.data_report_inicio+';'+row.hora_min_inicio+';'+row.hora_min_fim+';'+row.total_time+' min.;'+row.full_address+';'+row.qtd+' L\r\n';
                        });           

                        var a         = document.createElement('a');
                        a.href        = 'data:attachment/csv,' +  escape(content);
                        a.target      = '_blank';
                        a.download    = 'Relatório_de_Descarga.csv';

                        document.body.appendChild(a);
                        a.click();

                        break;     
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReportchecklistvelleda.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
                varReportchecklistvelleda.widgetObj.find('.dropdown-toggle').removeAttr("disabled"); 
                varReportchecklistvelleda.widgetObj.find('.sp-load').remove(); 
            }
        }); 
    }
};

function load_widgetsreportchecklistvelleda_Page(){
    $.ajax({
        type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'reportchecklistvelleda/index/'},
        dataType: 'json',
        beforeSend: function () {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
            varReportchecklistvelleda.widgetObj.find('.btn').attr('disabled', 'disabled');
            varReportchecklistvelleda.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportchecklistvelleda) != 'object'){ return false; }

            widgetsreportchecklistvelleda.buildGroup(JSON.parse(data.groupinfo));

            varReportchecklistvelleda.widgetObj.find('form').find('#start_dataatual').val(data.date);
            varReportchecklistvelleda.widgetObj.find('form').find('#end_dataatual').val(data.date);

            varReportchecklistvelleda.widgetObj.find('form').removeClass('hidden');
            varReportchecklistvelleda.widgetObj.find('.load-report').addClass('hidden');
            //liberando menu
            varReportchecklistvelleda.widgetObj.find('.btn').removeAttr("disabled");
            loadingWidget('#'+varReportchecklistvelleda.widgetName, true); 
            varReportchecklistvelleda.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');

        },
        error: function(error) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
            createMsgBox('error', error.responseText, '#'+varReportchecklistvelleda.widgetName+' .panel-body');
        },
        complete: function() {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
            varReportchecklistvelleda.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
    load_widgetsreportchecklistvelleda_Page();
  
    //Datepicker
    varReportchecklistvelleda.widgetObj.find('.pluginDate').datepicker({ 
        autoclose: true, 
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        todayHighlight: true,
    });

    varReportchecklistvelleda.widgetObj.find('.closeReportTab').die('click').live('click', function (){
        var id = $(this).attr('id');
        varReportchecklistvelleda.widgetObj.find('#tab-result-checklistvelleda-'+id).empty().remove();
        varReportchecklistvelleda.widgetObj.find('#result-checklistvelleda-'+id).empty().remove();
        varReportchecklistvelleda.widgetObj.find('#search-checklistvelleda').trigger('click');
        window.setTimeout(function() {
            varReportchecklistvelleda.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled'); 
        }, 50);  
        varReportchecklistvelleda.dataFilterArr[id] = null;
        varReportchecklistvelleda.table[id]         = null; 
    });  

    varReportchecklistvelleda.widgetObj.find('#search').die('click').live('click', function (){
        varReportchecklistvelleda.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');     
    }); 

    varReportchecklistvelleda.widgetObj.find('form').submit(function(e) {
        e.preventDefault();
        if ( $(this).parsley().isValid() ) {       
            
            var unitIds = [];

            varReportchecklistvelleda.widgetObj.find(".selectedUnitList > div").each( function(index, value) {
                unitIds.push($(this).attr('id'));
            });
            
            var dataFilter = 
                {
                    start_dataatual: $(this).find('#start_dataatual').val(),
                    end_dataatual: $(this).find('#end_dataatual').val(),
                    group_id: $(this).find('#group_id').val(),
                    limit: 0
                };
            dataFilter = JSON.stringify(dataFilter);
            
            $.ajax({
                async: true,
                type: 'POST',
                url: 'widgets/routing.php',
                data: { url: 'reportchecklistvelleda/search/', data: JSON.parse(dataFilter) },
                dataType: 'json',
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
                    loadingWidget('#'+varReportchecklistvelleda.widgetName);
                },
                success: function(data) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
                    varReportchecklistvelleda.dataFilterArr.push(dataFilter);
                    loadingWidget('#'+varReportchecklistvelleda.widgetName, true); 

                    varReportchecklistvelleda.widgetObj.find('.tab-content').append('<div class="col-md-12 tab-pane" id="tab-result-checklistvelleda-'+varReportchecklistvelleda.tabResults+'"></div>');
                    varReportchecklistvelleda.widgetObj.find('#widgetsreportchecklistvelleda-nav').append('<li class=""><a href="#tab-result-checklistvelleda-'+varReportchecklistvelleda.tabResults+'"  id="result-checklistvelleda-'+varReportchecklistvelleda.tabResults+'" data-toggle="tab">Resultado <i class="fa fa-times closeReportTab" id="'+varReportchecklistvelleda.tabResults+'"></i></a></li>');

                    varReportchecklistvelleda.widgetObj.find('#result-checklistvelleda-'+varReportchecklistvelleda.tabResults).die('click').live('click', function (){
                        var id = $(this).attr('id');
                        id = id.replace('result-checklistvelleda-', '');

                        var dropDownMenu = '<li><a href="javascript:;" onClick="widgetsreportchecklistvelleda.print(\'print\', '+id+')">Imprimir</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsreportchecklistvelleda.print(\'PDF\', '+id+')">Exportar PDF</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsreportchecklistvelleda.print(\'CSV\', '+id+')">Exportar CSV</a></li>';

                        varReportchecklistvelleda.widgetObj.find('#widgetsreportchecklistvelleda-dropdown').html(dropDownMenu);   
                        varReportchecklistvelleda.widgetObj.find('.dropdown-toggle').removeAttr("disabled");                 
                    }); 
                    var numRows = varReportchecklistvelleda.rowSizeMin;
                    var panelWidget = varReportchecklistvelleda.widgetObj.closest('.panel');
                    if(panelWidget.hasClass('full-Scr')){
                        numRows = Math.round(panelWidget.height() / varReportchecklistvelleda.rowSizeMax) - varReportchecklistvelleda.rowAdjust;
                    }
                    varReportchecklistvelleda.widgetObj.find('#tab-result-checklistvelleda-'+varReportchecklistvelleda.tabResults).html( '<table class="table table-striped table-bordered" data-page-length="'+numRows+'" id="table-list-result-checklistvelleda-'+varReportchecklistvelleda.tabResults+'"></table>' );
                    varReportchecklistvelleda.table.push(null);
                    varReportchecklistvelleda.table[varReportchecklistvelleda.tabResults] = varReportchecklistvelleda.widgetObj.find('#table-list-result-checklistvelleda-'+varReportchecklistvelleda.tabResults).DataTable({
                        "data": data,
                        paginate: true,
                        bLengthChange: false,
                        bFilter: false,
                        bInfo: true,
                        "ordering": true,
                        "pagingType": "full",
                        "fnDrawCallback": function( oSettings ) {
                            varReportchecklistvelleda.widgetObj.find('.25_p').css('width', '25%');                            
                        },
                        "columns": 
                            [
                                { "title": "#", "data": "id", "class": 'hidden' },
                                { "title": "<div class='labelHeader'>dataatual</div>", "data": "dataatual", "className": "25_p" },
                                { "title": "<div class='labelHeader'>placacavalo</div>", "data": "placacavalo", "className": "25_p" },
                                { "title": "<div class='labelHeader'>origem</div>", "data": "origem", "className": "25_p" },
                                { "title": "<div class='labelHeader'>Posto</div>", "data": "posto", "className": "25_p" }
                            ],
                        "columnDefs": 
                            [
                                {
                                    "targets": [0],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" user_id="' + full.user_id + '">' + data + '</div>';
                                    }
                                },
                                {
                                    "targets": [1],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + full.dataatual_data + '">' + full.dataatual_data + '</div>';
                                    }
                                },
                                {
                                    "targets": [2],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                    }
                                },
                                {
                                    "targets": [3],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                    }
                                },
                                {
                                    "targets": [4],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                    }
                                }
                            ],
                        "language" : {
                            "emptyTable":     "Ainda não há registros",
                            "info" : "Total de _TOTAL_ registros",
                            "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
                            "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
                            "infoPostFix":    "",
                            "thousands":      ".",
                            "lengthMenu":     "Mostre _MENU_ registros",
                            "loadingRecords": "Carregando...",
                            "processing":     "Processando...",
                            "search":         "",
                            "searchPlaceholder": "Procure por checklist...",
                            "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
                            "paginate": {
                                "first":      "<<",
                                "last":       ">>",
                                "next":       ">",
                                "previous":   "<"
                            }
                        },
                        "initComplete": function(settings) {
                            if( data.length > 0){
                                varReportchecklistvelleda.api.push(this.api())
                                varReportchecklistvelleda.widgetObj.find('.25_p').css('width', '25%');  

                                dataFilter = JSON.parse(dataFilter);
                                dataFilter.limit += varReportchecklistvelleda.processingSearchLimit;
                                if( data.length >= varReportchecklistvelleda.processingSearchLimit ){
                                    //widgetsreportchecklistvelleda.processingSearch(varReportchecklistvelleda.tabResults, dataFilter);
                                } 
                                
                                varReportchecklistvelleda.widgetObj.find('#table-list-result-checklistvelleda-'+varReportchecklistvelleda.tabResults+' tbody tr').die("click").live("click", function(){ 
                                    var label = $(this).children('td').html();
                                    varReportchecklistvelleda.user_id = $(label).attr('user_id');
                                    varReportchecklistvelleda.id = $(label).html();
                                    if($('#widgetschecklistvelleda').length > 0){
                                        varChecklistvelleda.widgetObj.find('[data-click=panel-remove]').trigger('click');
                                    }
                                    window.setTimeout(function() {
                                        handleLoadWidget('#widgets/checklistvelleda/checklistvelleda.html', 'widgetschecklistvelleda', 4);
                                    }, 50);
                                }); 
                            }
                        }
                    });              
                    //reseta o form   
                    varReportchecklistvelleda.widgetObj.find('#result-checklistvelleda-'+varReportchecklistvelleda.tabResults).trigger('click');  
                    varReportchecklistvelleda.tabResults++;
                    //resetReportAction(); 
                },
                error: function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportchecklistvelleda) != 'object'){ return false; }
                    loadingWidget('#'+varReportchecklistvelleda.widgetName, true);
                    //Criando mensagem de retorno
                    createMsgBox('error', error.responseText, '#'+varReportchecklistvelleda.widgetName+' .panel-body'); 
                }
            });
            return false;
        }
    }); 

});


