var varTables = {};
	varTables.widgetName 	= 'widgetstable';
	varTables.widgetObj 	= $('#'+varTables.widgetName);

	varTables.widgetAccountTypeName = 'tab-account-type';
	varTables.widgetAccountTypeObj = varTables.widgetObj.find('#'+varTables.widgetAccountTypeName);

	varTables.widgetWidgetsName = 'tab-widgets';
	varTables.widgetWidgetsObj = varTables.widgetObj.find('#'+varTables.widgetWidgetsName);

	varTables.widgetManufacturerName = 'tab-manuf';
	varTables.widgetManufacturerObj = varTables.widgetObj.find('#'+varTables.widgetManufacturerName);

	varTables.widgetModelName = 'tab-model';
	varTables.widgetModelObj = varTables.widgetObj.find('#'+varTables.widgetModelName);

	varTables.action = 'add';

	varTables.tableType = "account";
	varTables.tabNameToClose;

var widgetsTable = {
	reset: function (){
		varTables.action = 'add'; 

		varTables.widgetObj.find('form').each(function() { this.reset() });

		varTables.widgetObj.find('#category').prop('selectedIndex', 0);
		varTables.widgetObj.find('#size').prop('selectedIndex', 0);
		varTables.widgetObj.find('#manufacturer').prop('selectedIndex', 0);

		varTables.widgetObj.find('input').removeClass('parsley-success');
		varTables.widgetObj.find('select').removeClass('parsley-success');
	},
	loadCategory: function (data){
		varTables.widgetObj.find('#category').html('<option value="">Selecione uma categoria</option>');
		var template = 	'{{#widgetsCategory}}'+
						'	<option value="{{parent_id}}">{{name}}</option>'+
						'{{/widgetsCategory}}';
		var select = Mustache.to_html(template, data);
		varTables.widgetObj.find('#category').append(select);
	},
	loadManufactures: function (data){
		varTables.widgetObj.find('#manufacturer').html('<option value="">Selecione um fabricante</option>');
		var template = 	'{{#manufactures}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/manufactures}}';
		var select = Mustache.to_html(template, data);
		varTables.widgetObj.find('#manufacturer').append(select);
	},
	reloadManufactures: function () {
		var backupOptions = $('#widgetstable').find('form').find('select.manufacturer').html();	
		var deviceManuf = $('#widgetsdevice').find('form').find('select.selectManuf');
		
		$.ajax({
		  	async: true,
	        type: 'POST',
	        url: 'widgets/routing.php',
	        data: { url: 'device/listManufacturer/' },
	        dataType: 'json',
	        beforeSend: function () {
	        	$('#widgetstable').find('select.manufacturer').html('<option>Atualizando...</option>');
	        	if(deviceManuf) {
	            	deviceManuf.html('<option>Atualizando...</option>'); //atualizando widget device       		
	        	}
	        },
	        success: function(data) {
	        	var html = '';
	        	
	        	$(data).each(function(i, v) {
	        		html += '<option value="'+v.id+'">'+v.name+'</option>';
	        	});
	        	
	        	$('#widgetstable').find('select.manufacturer').html(html);
	        	if(deviceManuf) {
	            	deviceManuf.html(html); //atualizando widget device
	            	//verifica se no widget device esta aberto form edit de algum dispositivo
	            	//caso esteja ele atualiza o select de modelo tambem
	        		if($('#widgetsdevice form.edit.animated')) {
	        			loadModel($("#edit_manufacturer").val(), "#edit_model");
	        		}
	        	}
	        },
	        error: function(error) {
	        	//cria mensagem de sucesso no widget
	        	createMsgBox('error', error.responseText, "#widgetstable .panel-body");
	        	$('#widgetstable').find('select.manufacturer').html(backupOptions);
	        }
	    });
	},
	reloadAccountType: function () {
		var account = $("#widgetsaccount");
		var localOpt = $('#widgetsaccount select.selAccTypes');
		var backupOptions = localOpt.html();

		if(account.length > 0) {
			$.ajax({
				type : 'POST',
				url : 'widgets/routing.php',
				data : { url : 'account/listAccountTypes/' },
		        beforeSend: function () {
		        	localOpt.html('<option>Atualizando...</option>');
		        },
				success: function(data) {
		        	var html = '';
		        	var attOptions = JSON.parse(data);
		        	
		        	$(attOptions).each(function(i, v) {
		        		html += '<option value="'+v.id+'">'+v.name+'</option>';
		        	});
		        	
		        	localOpt.html(html);
				},
				error: function() {
					console.log('Erro ao recarregar tipos de conta.');
					localOpt.html(backupOptions);
				}
			});
		}
	}
};
var widgetsTableAccountType = {
	buildTable: function (dataList) {
		var accountTypeList = JSON.parse(dataList.accountTypeList);
		widgetsTable.loadCategory(JSON.parse(dataList.widgetsCategory));
		
		varTables.widgetAccountTypeObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-types-account"></table>' );
		var table_account_types = varTables.widgetAccountTypeObj.find('#table-list-types-account').dataTable({
		    //"data": accountTypeList.data,
		    ajax: {
			      "url": 'widgets/routing.php',
			      "type": "POST",
			      "data": { url: 'account/listAccountTypes/', data: true }
			    },
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "columns": 
		    	[
		            { "title": "#", "data": "id", 'class': 'hidden', 'searchable': false },
		            { 
		            	"title": "Tipo", 
		            	"data": "name" ,
		            	"className": "100_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            }
		          ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por tipos de conta...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },	    
		    "initComplete": function(settings, json) {
		    	widgetsTableWidgets.buildTable(dataList);
		    	//CLICANDO NA CONTA PARA EDITÁ-LAcontrol
		    	if(accountTypeList.data.length > 0){
                    varTables.widgetObj.find('.100_p').css('width', '100%');

		    		varTables.widgetObj.find('#table-list-types-account').find('tbody').delegate('tr', 'click', function() {
		    			//Pegando id da conta
			    		var data = $(this).children('td').html();
			    	    loadingWidget('#'+varTables.widgetName);			
		    		    $.ajax({
		    		    	async: true,
		    		    	type: 'POST',
		    		        url: 'widgets/routing.php',
		    		        data: { url: 'account/createEditFormAccountType/', 
		    		        data: JSON.parse(data) },
		    		        dataType: 'json',
		    		        success: function(data) {
			    		        widgetsTableAccountType.loadEditForm(data, varTables.widgetAccountTypeName);
			    	        	loadingWidget('#'+varTables.widgetName, true);
		    		        },
		    		        error: function(error) {
			    	        	createMsgBox('error', error.responseText, '#'+varTables.widgetName+' .panel-body');
			    	        	loadingWidget('#'+varTables.widgetName, true);
		    		        }
		    		    });
		    		    return false;
		    		});  
		    	}
		    		 	
		      },
		});
	},
	delete: function () {
	    var data = { id: varTables.widgetAccountTypeObj.find("#id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	        async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'account/deleteaccountType',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varTables.widgetName);
			},
			success : function(data) {
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varTables.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varTables.widgetObj.find('#table-list-types-account').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varTables.widgetAccountTypeName);
				widgetsTable.reset();
				loadingWidget('#'+varTables.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varTables.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varTables.widgetName+' .panel-body');			
			}
		});
		return false;
	},
	loadEditForm: function (data) {	
		var form = varTables.widgetAccountTypeObj.find('form');
		//limpa formulário
		widgetsTable.reset();
		// muda o form para edição
		varTables.action = 'edit';
		
		form.removeClass('hidden').addClass('animated fadeIn');	
		form.find('input.name').focus();
		form.find('h3 > span').html(data.name);
		form.find('input.id').val(data.id);
		form.find('input.name').val(data.name);

		openForm(varTables.widgetAccountTypeName, varTables.action);
	}
};
var widgetsTableWidgets = {
	buildTable: function (dataList){
		var widgetList = JSON.parse(dataList.widgetList);
		
		//CRIANDO TABELA PARA WIDGETS
		varTables.widgetWidgetsObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-widgets"></table>' );
		var table_widgets = varTables.widgetWidgetsObj.find('#table-list-widgets').dataTable({
		    ajax: {
			      "async": true,
			      "url": 'widgets/routing.php',
			      "type": "POST",
			      "data": { url: 'widget/list/' }
			    },
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    //Carregando os ícones na tabela
		    "createdRow": function ( row, data, index ) {
		    	$('td', row).eq(1).addClass('text-center').html('<i class="fa fa-1x '+data.icon+'"></i>');
	        },	    
		    "columns": 
		    	[
		            { "title": "#", "data": "id", 'class': 'hidden', 'searchable': false },
		            { 
		            	"title": "Ícone", 
		            	"data": "icon" ,
		            	"className": "10_p"
		            },
		            { 
		            	"title": "Widget", 
		            	"data": "name" ,
		            	"className": "45_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            },
		            { 
		            	"title": "Categoria", 
		            	"data": "cat" ,
		            	"className": "45_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            },
		          ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por widgets...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },	    
		    "initComplete": function(settings, json) {
		    	widgetsTableManufacture.buildTable(dataList);
		    	//CLICANDO NA CONTA PARA EDITÁ-LA
		    	if(widgetList.data.length > 0){
                    varTables.widgetObj.find('.10_p').css('width', '10%');
                    varTables.widgetObj.find('.45_p').css('width', '45%');

		    		varTables.widgetObj.find('#table-list-widgets').find('tbody').delegate('tr', 'click', function() {
		    			//Pegando id da conta
			    		var data = $(this).children('td').html();
			    	    loadingWidget('#'+varTables.widgetName);	
		    		    $.ajax({
		    		    	async: true,
		    		    	type: 'POST',
		    		        url: 'widgets/routing.php',
		    		        data: { url: 'widget/createEditForm/', data: JSON.parse(data) },
		    		        dataType: 'json',
		    		        success: function(data) {
			    		        widgetsTableWidgets.loadEditForm(data, varTables.widgetName);
			    	        	loadingWidget('#'+varTables.widgetName, true);
		    		        },
		    		        error: function(error) {
			    	        	createMsgBox('error', error.responseText, '#'+varTables.widgetName+' .panel-body');
			    	        	loadingWidget('#'+varTables.widgetName, true);
		    		        }
		    		    });
		    		    return false;
		    		});
		    	}
		      }
		});
	},
	delete: function () {
	    var data = { id: varTables.widgetWidgetsObj.find("#id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'widget/deleteWidgets',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varTables.widgetName);
			},
			success : function(data) {
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varTables.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varTables.widgetObj.find('#table-list-widgets').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varTables.widgetWidgetsName);
				widgetsTable.reset();
				loadingWidget('#'+varTables.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varTables.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varTables.widgetName+' .panel-body');			
			}
		});
		return false;
	},
	loadEditForm: function (data) {
		var form = varTables.widgetWidgetsObj.find('form');
		//limpa formulário
		widgetsTable.reset();
		// muda o form para edição
		varTables.action = 'edit';

		form.removeClass('hidden').addClass('animated fadeIn');	
		form.find('input.name').focus();
		form.find('h3 > span').html(data.name);

		form.find('input.id').val(data.id);
		form.find('input.name').val(data.name);
		form.find('input.template').val(data.template);	
		form.find('.selected-icon').children().attr('class', data.icon);

		form.find('select#size option').each(function(index, value){
		      var value = $(this).attr('value');
		      if(value == data.size)
			      $(this).attr('selected', true);
		});

		form.find('select.category option').each(function(index, value){
		      var value = $(this).attr('value');
		      if(value == data.widget_category_id)
			      $(this).attr('selected', true);
		});

		openForm(varTables.widgetWidgetsName, varTables.action);
	}
};
var widgetsTableManufacture = {
	buildTable: function (dataList){
		var manufacturer = dataList.manuf;
		widgetsTable.loadManufactures(JSON.parse(manufacturer));
		
		//CRIANDO TABELA PARA FABRICANTES
		varTables.widgetManufacturerObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-manuf"></table>' );
		var table_widgets = varTables.widgetManufacturerObj.find('#table-list-manuf').dataTable({
		    ajax: {
			      "async": true,
			      "url": 'widgets/routing.php',
			      "type": "POST",
			      "data": { url: 'device/listDataTableManufacturer/' }
			    },
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "columns": 
		    	[
		            { "title": "#", "data": "id", 'class': 'hidden', 'searchable': false },
		            { 
		            	"title": "Nome", 
		            	"data": "name" ,
		            	"className": "100_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            }
		          ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por fabricantes...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },	    
		    "initComplete": function(settings, json) {
		    	widgetsTableModel.buildTable(dataList);
		    	//CLICANDO NA CONTA PARA EDITÁ-LA
		    	if(manufacturer.length > 0){
                    varTables.widgetObj.find('.100_p').css('width', '100%');

		    		varTables.widgetObj.find('#table-list-manuf').find('tbody').delegate('tr', 'click', function() {
		    			//Pegando id da conta
		    			var data = $(this).children('td').html();
			    	    loadingWidget('#'+varTables.widgetName);	
		    		    $.ajax({
		    		    	async: true,
		    		    	type: 'POST',
		    		        url: 'widgets/routing.php',
		    		        data: { url: 'device/getManufacturer/', data: JSON.parse(data) },
		    		        dataType: 'json',
		    		        success: function(data) {
			    		        widgetsTableManufacture.loadEditForm(data, varTables.widgetName);
			    	        	loadingWidget('#'+varTables.widgetName, true);
		    		        },
		    		        error: function(error) {
			    	        	createMsgBox('error', error.responseText, '#'+varTables.widgetName+' .panel-body');
			    	        	loadingWidget('#'+varTables.widgetName, true);
		    		        }
		    		    });
		    		    return false;
		    		});
		    	}
		      }
		});	
	},
	delete: function () {
	    var data = { id: varTables.widgetManufacturerObj.find("#id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'device/deleteManufacturer',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varTables.widgetName);
			},
			success : function(data) {
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varTables.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varTables.widgetObj.find('#table-list-manuf').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varTables.widgetManufacturerName);
				widgetsTable.reset();
				loadingWidget('#'+varTables.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varTables.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varTables.widgetName+' .panel-body');			
			}
		});
		return false;
	},
	loadEditForm: function (data) {	
		var form = varTables.widgetManufacturerObj.find('form');
		//limpa formulário
		widgetsTable.reset();
		// muda o form para edição
		varTables.action = 'edit';

		form.removeClass('hidden').addClass('animated fadeIn');
		
		form.find('input.id').val(data.id);
		form.find('input.name').val(data.name);

		openForm(varTables.widgetManufacturerName, varTables.action);
	}
};
var widgetsTableModel = {
	buildTable: function (dataList){
		var modelList = JSON.parse(dataList.listModel);
		
		//CRIANDO TABELA PARA MODELOS
		varTables.widgetModelObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-model"></table>' );
		var table_widgets = varTables.widgetModelObj.find('#table-list-model').dataTable({
		    ajax: {
			      "async": true,
			      "url": 'widgets/routing.php',
			      "type": "POST",
			      "data": { url: 'device/listDataTableModel/' }
			    },
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "columns": 
		    	[
		            { "title": "#", "data": "id", 'class': 'hidden', 'searchable': false },
		            { 
		            	"title": "Modelo", 
		            	"data": "name" ,
		            	"className": "50_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            },
		            { 
		            	"title": "Fabricante", 
		            	"data": "manuf_name" ,
		            	"className": "50_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            },
	            ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por modelos...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },	    
		    "initComplete": function(settings, json) {
		    	//CLICANDO NA CONTA PARA EDITÁ-LA
		    	if(modelList.data.length > 0){
                    varTables.widgetObj.find('.50_p').css('width', '50%');

		    		varTables.widgetObj.find('#table-list-model').find('tbody').delegate('tr', 'click', function() {
		    			//Pegando id da conta
		    			var data = $(this).children('td').html();
			    	    loadingWidget('#'+varTables.widgetName);	
		    		    $.ajax({
		    		    	async: true,
		    		    	type: 'POST',
		    		        url: 'widgets/routing.php',
		    		        data: { url: 'device/getModel/', data: JSON.parse(data) },
		    		        dataType: 'json',
		    		        success: function(data) {
			    		        widgetsTableModel.loadEditForm(data);
			    	        	loadingWidget('#'+varTables.widgetName, true);
		    		        },
		    		        error: function(error) {
			    	        	createMsgBox('error', error.responseText, '#'+varTables.widgetName+' .panel-body');
			    	        	loadingWidget('#'+varTables.widgetName, true);
		    		        }
		    		    });
		    		    return false;
		    		});
		    	}
		      }
		});
	},
	delete: function () {
	    var data = { id: varTables.widgetModelObj.find("#id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'device/deleteModel',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varTables.widgetName);
			},
			success : function(data) {
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varTables.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varTables.widgetObj.find('#table-list-model').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varTables.widgetModelName);
				widgetsTable.reset();
				loadingWidget('#'+varTables.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varTables.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varTables.widgetName+' .panel-body');			
			}
		});
		return false;
	},
	loadEditForm: function (data) {	
		var form = varTables.widgetModelObj.find('form');
		//limpa formulário
		widgetsTable.reset();
		// muda o form para edição
		varTables.action = 'edit';
		
		form.removeClass('hidden').addClass('animated fadeIn');	
		form.find('input.id').val(data.id);
		form.find('input.name').val(data.name);
		form.find('#qtd_driver').val(data.qtd_driver);
		form.find('select#manufacturer option').each(function(index, value){
		      var value = $(this).attr('value');
		      if(value == data.manuf_id)
			      $(this).attr('selected', true);
		});

		openForm(varTables.widgetModelName, varTables.action);
	}
};

function load_widgetsdevice_Page(){
	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'widget/index/'},
        dataType: 'json',
        beforeSend: function () {
        	closeForm(varTables.widgetName);
			widgetsTable.reset();	
			varTables.widgetObj.find('.btn').attr('disabled', 'disabled');
            varTables.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	widgetsTableAccountType.buildTable(data);
        	selectIcons('#'+varTables.widgetWidgetsName+' form', data.listIcons, 9);
        	//SET ID account user
        	//varTables.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	//liberando menu
        	varTables.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varTables.widgetName, true); 

        	//Carregando comportamento do widget
        },
        error: function(error) {
        	loadingWidget('#'+varTables.widgetName, true);
            varTables.widgetObj.find('.btn-danger').removeAttr("disabled");
			createMsgBox('error', error.responseText, '#'+varTables.widgetName+' .panel-body');
        }
    });
}

$(document).ready(function() {
	load_widgetsdevice_Page();

	//mask nos campos de telefone
	varTables.widgetModelObj.find('#qtd_driver').mask('?999999');

	varTables.widgetObj.find('.openForm').die("click").live("click", function(){
		var widgetTable = $(this).attr('class');
		widgetsTable.reset();
		varTables.action = 'add';
		widgetTable = widgetTable.replace('openForm td-', '');
		switch (widgetTable) { 
			case 'account': 
				openForm(varTables.widgetAccountTypeName, 'add');
				varTables.widgetObj.find('#account_type').trigger('click');
				break;
			case 'widget': 
				openForm(varTables.widgetWidgetsName, 'add');
				varTables.widgetObj.find('#widgets').trigger('click');
				break;
			case 'manufacture': 
				openForm(varTables.widgetManufacturerName, 'add');
				varTables.widgetObj.find('#manuf').trigger('click');
				break;		
			case 'model': 
				openForm(varTables.widgetModelName, 'add');
				varTables.widgetObj.find('#model').trigger('click');
				break;		
		}
	});

	//CHAMADA PARA EXCLUSÃO DE ACCOUNT 
	varTables.widgetAccountTypeObj.find(".confirm_delete").die("click").live("click", function(){

		varTables.tableType = "account";
		openConfirm(varTables.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir tipo de Conta", "Deseja remover o tipo de conta selecionado?", "Sim", "Não");

	});
	varTables.widgetWidgetsObj.find(".confirm_delete").die("click").live("click", function(){

		varTables.tableType = "widget";
		openConfirm(varTables.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir Widget", "Deseja remover o widget selecionado?", "Sim", "Não");

	});
	varTables.widgetManufacturerObj.find(".confirm_delete").die("click").live("click", function(){

		varTables.tableType = "manuf";
		openConfirm(varTables.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir fabricante", "Deseja remover o fabricante selecionado?", "Sim", "Não");

	});
	varTables.widgetModelObj.find(".confirm_delete").die("click").live("click", function(){

		varTables.tableType = "model";
		openConfirm(varTables.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir modelo", "Deseja remover o modelo selecionado?", "Sim", "Não");

	});

	varTables.widgetObj.find(".confirm_yes").die("click").live("click", function(){
		
		switch (varTables.tableType) { 
			case 'account': 
				widgetsTableAccountType.delete();
				closeConfirm(varTables.widgetName);
				break;
			case 'widget': 
				widgetsTableWidgets.delete();
				closeConfirm(varTables.widgetName);
				break;
			case 'manuf': 
				widgetsTableManufacture.delete();
				closeConfirm(varTables.widgetName);
				break;		
			case 'model': 
				widgetsTableModel.delete();
				closeConfirm(varTables.widgetName);
				break;		
		}
	});

	varTables.widgetObj.find(".confirm_no").die("click").live("click", function(){
		closeConfirm(varTables.widgetName);
	});

	varTables.widgetObj.find('.cancel').die('click').live('click', function() {
		varTables.tabNameToClose = $(this).parent().parent().parent().attr('id'); 
		closeForm(varTables.widgetName + ' #' + varTables.tabNameToClose);
		widgetsTable.reset();		
	});	
});

// FORM TIPO DE CONTA
varTables.widgetAccountTypeObj.find('form').submit(function(e) {
	e.preventDefault();
	if ( $(this).parsley().isValid() ) {  
		var data = 
			{
				id: $(this).find('.id').val(),
				name: $(this).find('.name').val()
			};

		data = JSON.stringify(data);
		
		$.ajax({
  	  		async: true,
	        type: 'POST',
	        url: 'widgets/routing.php',
	        data: { url: 'account/'+varTables.action+'Type/', data: JSON.parse(data) },
	        dataType: 'json',
	        beforeSend: function () {
	        	loadingWidget('#'+varTables.widgetName);
	        },
	        success: function(data) {
	        	loadingWidget('#'+varTables.widgetName, true);       	        
    	        //Criando mensagem de retorno
    	        createMsgBox('success', data, '#'+varTables.widgetName+' .panel-body');
    	        // atualiza tabela que lista as contas
    	        varTables.widgetObj.find('#table-list-types-account').DataTable().ajax.reload(null, false);	    	            	
    	        //reseta o form	    
				closeForm(varTables.widgetAccountTypeName);
    	        widgetsTable.reset(); 
	        },
	        error: function(error) {
	        	loadingWidget('#'+varTables.widgetName, true);
	        	//Criando mensagem de retorno
	        	createMsgBox('error', error.responseText, '#'+varTables.widgetName+' .panel-body');	
	        }
	    });
		return false;
	}
});
// FORM WIDGET
varTables.widgetWidgetsObj.find('form').submit(function(e) {
    e.preventDefault();
    if ( $(this).parsley().isValid() ) {
        
	    var data = 
			{
				id: $(this).find('.id').val(),
				name: $(this).find('.name').val(),
				category: parseInt($(this).find('.category').val()),
				icon: $(this).find('.selected-icon').children('i').attr('class'),
				template: $(this).find('.template').val().toLowerCase(),
				size: $(this).find('#size').val()
			};
	    
	    data = JSON.stringify(data);
	    
        $.ajax({
    	  	async: true,
	        type: 'POST',
	        url: 'widgets/routing.php',
            data: { url: 'widget/'+varTables.action+'/', data: JSON.parse(data) },
	        dataType: 'json',
	        beforeSend: function () {
	        	loadingWidget('#'+varTables.widgetName);
	        },
	        success: function(data) {
	        	loadingWidget('#'+varTables.widgetName, true);       	        
    	        //Criando mensagem de retorno
    	        createMsgBox('success', data, '#'+varTables.widgetName+' .panel-body');
    	        // atualiza tabela que lista as contas
    	        varTables.widgetObj.find('#table-list-widgets').DataTable().ajax.reload(null, false);	    	            	
    	        //reseta o form	    
				closeForm(varTables.widgetWidgetsName);
    	        widgetsTable.reset(); 
	        },
	        error: function(error) {
	        	loadingWidget('#'+varTables.widgetName, true);
	        	//Criando mensagem de retorno
	        	createMsgBox('error', error.responseText, '#'+varTables.widgetName+' .panel-body');	
	        }
	    });
		return false;
    }
});
// FORM Manufacturer
varTables.widgetManufacturerObj.find('form').submit(function(e) {
	e.preventDefault();
	if ( $(this).parsley().isValid() ) {
	    var data = 
			{
				id: $(this).find('.id').val(),
				name: $(this).find('.name').val(),
			};
	    
	    data = JSON.stringify(data);
	    
	    $.ajax({
			asyc: true,
	        type: 'POST',
	        url: 'widgets/routing.php',
	        data: { url: 'device/'+varTables.action+'Manufacturer/', data: JSON.parse(data) },
	        dataType: 'json',
	        beforeSend: function () {
	        	loadingWidget('#'+varTables.widgetName);
	        },
	        success: function(data) {
	        	loadingWidget('#'+varTables.widgetName, true);       	        
    	        //Criando mensagem de retorno
    	        createMsgBox('success', data, '#'+varTables.widgetName+' .panel-body');
    	        // atualiza tabela que lista as contas
    	        varTables.widgetObj.find('#table-list-manuf').DataTable().ajax.reload(null, false);	    	            	
    	        //reseta o form	    
				closeForm(varTables.widgetManufacturerName);
    	        widgetsTable.reset(); 
	        },
	        error: function(error) {
	        	loadingWidget('#'+varTables.widgetName, true);
	        	//Criando mensagem de retorno
	        	createMsgBox('error', error.responseText, '#'+varTables.widgetName+' .panel-body');	
	        }
	    });
		return false;
	}
});
// FORM MODEL
varTables.widgetModelObj.find('form').submit(function(e) {
	e.preventDefault();
	if ( $(this).parsley().isValid() ) {
    
	    var data = 
			{
				id: $(this).find('.id').val(),
				name: $(this).find('.name').val(),
				manuf: $(this).find('.manufacturer').val(),
				qtd_driver: $(this).find('#qtd_driver').val()
			};
	    
	    data = JSON.stringify(data);
	    
	    $.ajax({
		  	async: true,
	        type: 'POST',
	        url: 'widgets/routing.php',
	        data: { url: 'device/'+varTables.action+'Model/', data: JSON.parse(data) },
	        dataType: 'json',
	        beforeSend: function () {
	        	loadingWidget('#'+varTables.widgetName);
	        },
	        success: function(data) {
	        	loadingWidget('#'+varTables.widgetName, true);       	        
    	        //Criando mensagem de retorno
    	        createMsgBox('success', data, '#'+varTables.widgetName+' .panel-body');
    	        // atualiza tabela que lista as contas
    	        varTables.widgetObj.find('#table-list-model').DataTable().ajax.reload(null, false);	    	            	
    	        //reseta o form	    
				closeForm(varTables.widgetModelName);
    	        widgetsTable.reset(); 
	        },
	        error: function(error) {
	        	loadingWidget('#'+varTables.widgetName, true);
	        	//Criando mensagem de retorno
	        	createMsgBox('error', error.responseText, '#'+varTables.widgetName+' .panel-body');	
	        }
	    });
		return false;
	}
});
