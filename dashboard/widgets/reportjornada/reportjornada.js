var varReportjornada = {};
    varReportjornada.widgetName    = 'widgetsreportjornada';
    varReportjornada.widgetObj     = $('#'+varReportjornada.widgetName);
    varReportjornada.numEventsG    = 0;
    varReportjornada.numEvents     = 0;
    varReportjornada.numSearchs    = 0;
    varReportjornada.backupElem    = '';
    varReportjornada.tabResults    = 0;
    varReportjornada.table         = [];
    varReportjornada.api           = [];
    varReportjornada.dataFilterArr = [];
    varReportjornada.limitIncrement = 1000;

    varReportjornada.rowSizeMax = 160;
    varReportjornada.rowSizeMin = 4;
    varReportjornada.rowAdjust = 1;

var widgetsReportJornada = {
    buildGroup: function (data){
        varReportjornada.widgetObj.find('#group').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varReportjornada.widgetObj.find('#group').append(select);
    },

    exportCSV: function(json){
        var json_data = JSON.stringify(json);
        var url_relatorio = '../dashboard-api/class/export/csv/relatorioJornada.php';

        postExterno(url_relatorio, {dados: json_data});
    },

    exportPDF: function(json){
        var json_data = JSON.stringify(json);
        var url_relatorio = '../dashboard-api/class/export/pdf/relatorioJornada.php';

        postExterno(url_relatorio, {dados: json_data});
    },
    formatHora: function(hora) {

        var string = String(hora);
        var retorno = string.split(":");

        var hour = retorno[0];
        var min = retorno[1];

        if(hour < 1){
            return min +'min';
        }else{
            return hour+"H e "+min +"min";
        }
    },
    buildSubGroup: function (data){
        varReportjornada.widgetObj.find('#subgroup').html('<option value="0" selected>Todos</option>');
        var template =  '{{#subgroupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/subgroupinfo}}';
        var select = Mustache.to_html(template, data);
        varReportjornada.widgetObj.find('#subgroup').append(select);
    },
    processingSearch: function (idTab, dataFilter){
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportjornada/search/', data: dataFilter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                varReportjornada.widgetObj.find('#table-list-result-jornada-'+idTab+'_info').append('<span class="sp-load-'+idTab+'" title="Carregando consulta..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                //para de processar caso a tab seja fechada
                if(varReportjornada.table[idTab] == null ){ return false; }
                
                $(json).each(function(i, rows) {                
                    varReportjornada.table[idTab].row.add({
                        "label": rows.label, 
                        "data_inicio": rows.data_inicio,  
                        "data_report_inicio": rows.data_report_inicio,
                        "hora_min_inicio": rows.hora_min_inicio, 
                        "data_fim": rows.data_fim,  
                        "data_report_fim": rows.data_report_fim,
                        "hora_min_fim": rows.hora_min_fim,  
                        "initial_longitude": rows.initial_longitude, 
                        "initial_latitude": rows.initial_latitude, 
                        "last_longitude": rows.last_longitude,
                        "initial_poi_name": rows.initial_poi_name,
                        "last_poi_name": rows.last_poi_name,
                        "last_latitude": rows.last_longitude, 
                        "initial_history_id": rows.initial_history_id,
                        "last_history_id": rows.last_history_id, 
                        "total_jornada": rows.total_jornada,
                        "initial_address": rows.initial_address,
                        "last_address": rows.last_address, 
                        //"initial_poi_name": rows.initial_poi_name, 
                        //"initial_area_name": rows.initial_area_name, 
                        "total_time": rows.total_time, 
                        "full_date_begin": rows.full_date_begin
                    });
                }); 
                varReportjornada.table[idTab].draw(false);
                //reinicia o geocode caso haja algum registro para ser carregado
                buildRevGeoFromTable(varReportjornada.widgetName);
                //verifica se ainda existem registros para serem buscados
                if( json.length >= varReportjornada.limitIncrement ){
                    dataFilter.limit += varReportjornada.limitIncrement;

                    window.setTimeout(function() {
                        varReportjornada.processingSearch(idTab, dataFilter); 
                    }, 50);
                }    
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReportjornada.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                varReportjornada.widgetObj.find('.sp-load-'+idTab).remove();
            }
        });
    },
    print: function (type, id){
        var filter = JSON.parse(varReportjornada.dataFilterArr[id]);
        //removendo limit para retornar todos os dados quando for´exportação
        filter.limit = 0;
        filter.last_id = 0;

        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportjornada/search/', data: filter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                varReportjornada.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
                varReportjornada.widgetObj.find('.actionMenu .fristAction').append('<span class="sp-load" title="Carregando exportação..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                var horaFim;
                var speedFim;

                switch (type) { 
                    case 'print': 
                        var content = '<style>';
                        content += '    table { border: 1px solid #999; border-bottom: none; width: 100%; }';
                        content += '    thead { border: 5px solid #000; }';
                        content += '    tbody td { border: 1px solid #ccc; background: #eee; }';
                        content += '</style>';             
                        content += '<table><tbody>';                        
                        
                        $(json).each(function(i, row) {

                            content += '<tr>';
                            content += '    <td>'+row.label+'</td>';
                            content += '</tr>';
                            content += '<tr>';
                            content += '    <td>';

                            content += '    <table class="table table-bordered tableDetalhes">';
                            content += '        <thead>';
                            content += '            <tr>';
                            content += '                <th>Data</th>';
                            content += '                <th class="endInicio col-md-4">Inicio</th>';
                            content += '                <th class="endFim col-md-4">Fim</th>';
                            content += '                <th>Total</th>';
                            content += '            </tr>';
                            content += '        </thead>';
                            content += '        <tbody>';
                            
                            $(row.itens).each(function(index, val){
                            //////////////////////////////////////////////////

                            if(val.poi_inicio == null || val.poi_fim == null){
                                var poi_inicio = "";
                                var poi_fim = "";
                            }else{
                                var poi_inicio = val.poi_inicio;
                                var poi_fim = val.poi_fim;
                            }

                            if(val.initial_address == null || val.last_address == null){
                                var initial_address = "";
                                var last_address = "";
                            }else{
                                var initial_address = val.initial_address;
                                var last_address = val.last_address;
                            }


                            content += '            <tr>';
                            content += '                <td>'+val.data_relatorio+'</td>';
                            content += '                <td class="POIinicio">';
                            content += '                    Hora: '+val.hora_inicio+'<br />';
                            content += '                    Endereço: '+initial_address+'<br />';
                            content += '                    POI: '+poi_inicio+'<br />';

                            content +- '                </td>';
                            content += '                <td class="POIfim">';
                            content += '                    Hora: '+val.hora_fim+'<br />';
                            content += '                    Endereço: '+last_address+'<br />';
                            content += '                    POI: '+poi_fim+'<br />';

                            content +- '                </td>';
                            content += '                <td>'+ widgetsReportJornada.formatHora(val.total_jornada) +'</td>';
                            content += '            </tr>';

                            //////////////////////////////////////////////////
                            });

                            content += '        </tbody>';
                            content += '    </table>';

                            content += '</td>';
                            content += '</tr>';

                        });            
                        content += '</tbody></table>';

                        var w = window.open("");

                        w.document.write(content);
                        w.print();
                        w.close();

                        break;

                    case 'PDF': widgetsReportJornada.exportPDF(json); break;
                    case 'CSV': widgetsReportJornada.exportCSV(json); break;     
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReportjornada.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                varReportjornada.widgetObj.find('.dropdown-toggle').removeAttr("disabled"); 
                varReportjornada.widgetObj.find('.sp-load').remove(); 
            }
        }); 
    },
    openMap: function (lat, lng, drawMark){
        if(typeof(varMapBox) != 'object'){
            handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 4);

            clearTimeout(varReportjornada.callMap);
            varReportjornada.callMap = window.setTimeout(function() {
                widgetsReportJornada.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }
        if(typeof(varMapBox.mapInstance) != 'object'){
            clearTimeout(varReportjornada.callMap);
            varReportjornada.callMap = window.setTimeout(function() {
                widgetsReportJornada.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }

        MapBox.centerUnit(lat, lng, false, null, drawMark);
    }
};

function load_widgetsreportjornada_Page(){
    $.ajax({
        type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'reportjornada/index/'},
        dataType: 'json',
        beforeSend: function () {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportjornada) != 'object'){ return false; }
            varReportjornada.widgetObj.find('.btn').attr('disabled', 'disabled');
            varReportjornada.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportjornada) != 'object'){ return false; }

            widgetsReportJornada.buildGroup(JSON.parse(data.groupinfo))
            widgetsReportJornada.buildSubGroup(JSON.parse(data.subgroupinfo))

            varReportjornada.widgetObj.find('form').find('#start_day').val(data.date);
            varReportjornada.widgetObj.find('form').find('#end_day').val(data.date);
            varReportjornada.widgetObj.find('form').find('#end_hour').val(data.hour);

            varReportjornada.widgetObj.find('form').removeClass('hidden');
            varReportjornada.widgetObj.find('.load-report').addClass('hidden');
            //liberando menu
            varReportjornada.widgetObj.find('.btn').removeAttr("disabled");
            loadingWidget('#'+varReportjornada.widgetName, true); 
            varReportjornada.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');

        },
        error: function(error) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportjornada) != 'object'){ return false; }
            createMsgBox('error', error.responseText, '#'+varReportjornada.widgetName+' .panel-body');
        },
        complete: function() {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportjornada) != 'object'){ return false; }
            varReportjornada.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
    load_widgetsreportjornada_Page();

    if(varReportjornada.widgetObj.parent().hasClass('col-md-4')){
        varReportjornada.widgetObj.find('.larguraEndereco').css('width', '60px');
    }else{
        varReportjornada.widgetObj.find('.larguraEndereco').css('width', '200px');
    }

    //Plugin Clockpicker
    varReportjornada.widgetObj.find('.clockpicker').clockpicker({
        autoclose: true,
        'default': 'now'
    });    
    //Datepicker
    varReportjornada.widgetObj.find('.pluginDate').datepicker({ 
        autoclose: true, 
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        todayHighlight: true,
    });

    varReportjornada.widgetObj.find('select#group').on('change', function() {
        var select = 'select#subgroup';
        var data = 
        {
            groupIds: $(this).val()
        };
        varReportjornada.numEventsG++;
        data = JSON.stringify(data);

        var html = '';
        var selected = 'selected';
        if($(this).val() == 0) {
            html += '<option value="0" selected>Todos</option>';
            selected  = '';
        }
        
        $.ajax({
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'client/listSubGroups/', data:JSON.parse(data) },
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                varReportjornada.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                varReportjornada.widgetObj.find('select#unit').addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                try {
                    var subgroups = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReportjornada.widgetName+' .panel-body');              
                    varReportjornada.widgetObj.find(select).html('');
                    varReportjornada.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                
                $(subgroups).each(function(i, val) {
                    html+='<option value="'+val.id+'" '+selected+'>'+val.name+'</option>';
                });

                varReportjornada.numEventsG--;
                if(varReportjornada.numEventsG == 0) {
                    varReportjornada.widgetObj.find(select).removeClass('select-loader');
                    varReportjornada.widgetObj.find(select).html(html);
                    varReportjornada.widgetObj.find('select#subgroup').change();
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                createMsgBox('error', error.responseText,'#'+varReportjornada.widgetName+' .panel-body');                 
                varReportjornada.widgetObj.find(select).html(error.responseText);
                varReportjornada.widgetObj.find(select).removeClass('select-loader');
            }
        });     
    });
    
    varReportjornada.widgetObj.find('select#subgroup').on('change', function() {
        var select = 'select#unit';

        var html = '';      
        if($(this).val() == '0') {
            html += '<option value="0">Todos</option>';
        }
        var data = 
        {
            subGroupIds: $(this).val()
        };
        varReportjornada.numEvents++;
        
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'unit/listTrackedUnit/', data:JSON.parse(data) },
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                varReportjornada.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                try {
                    var unities = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReportjornada.widgetName+' .panel-body');              
                    varReportjornada.widgetObj.find(select).html('');
                    varReportjornada.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                html = '<option value="0" selected>Todos</option>';
                $(unities).each(function(i, val) {
                    if(val.label != undefined){
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    }   
                });
                
                varReportjornada.numEvents--;
                if(varReportjornada.numEvents == 0) {
                    varReportjornada.widgetObj.find(select).removeClass('select-loader');
                    varReportjornada.widgetObj.find(select).html(html);
                }                   
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportjornada) != 'object'){ return false; }
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varReportjornada.widgetName+' .panel-body');                 
                varReportjornada.widgetObj.find(select).html(error.responseText);
                varReportjornada.widgetObj.find(select).removeClass('select-loader');
            }
        });
    });

    varReportjornada.widgetObj.find('input#searchUnits').on('keyup', function(e) {
        e.preventDefault();
        if(varReportjornada.backupElem == '') {
            varReportjornada.backupElem = varReportjornada.widgetObj.find('select#unit').html();
        }
        if($(this).val().length > 2) {
            var select = 'select#unit';
            var html = '';
            var data = 
            {
                unit: $(this).val()
            };
            data = JSON.stringify(data);
            varReportjornada.numSearchs++;
             $.ajax({
                async : true,
                type : "POST",
                data: { url: 'unit/searchUnit', data: JSON.parse(data) },
                url : 'widgets/routing.php',
                dataType : "json",
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportjornada) != 'object'){ return false; }
                    varReportjornada.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                },
                success : function(json) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportjornada) != 'object'){ return false; }
                    varReportjornada.numSearchs--;
                    if(json.length <= 0) {
                        if(varReportjornada.numSearchs == 0) {
                            varReportjornada.widgetObj.find(select).html('');
                            varReportjornada.widgetObj.find(select).removeClass('select-loader');
                            return false;
                        }
                    }
                    html+='<option value="0" selected>Todos</option>';
                    $(json).each(function(i, val) {
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    });
                    if(varReportjornada.numSearchs == 0) {
                        varReportjornada.widgetObj.find(select).removeClass('select-loader');
                        varReportjornada.widgetObj.find(select).html(html);
                    }
                },
                error : function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportjornada) != 'object'){ return false; }
                    varReportjornada.numSearchs--;
                    if(varReportjornada.numSearchs == 0) {
                        createMsgBox('error', error.responseText, "#"+varReportjornada.widgetName+" .panel-body");
                        varReportjornada.widgetObj.find(select).removeClass('select-loader');
                    }
                }
            });
        } else if($(this).val().length <= 2) {
            if(varReportjornada.backupElem) {
                varReportjornada.widgetObj.find('select#unit').html(varReportjornada.backupElem);
                varReportjornada.backupElem = '';
            }
        } else {
            varReportjornada.backupElem = '';
            return false;       
        }
    });

    varReportjornada.widgetObj.find('#unit').die('click').live('click', function (){
        var id = $(this).val();
        var label = $(this).find('option:selected').text();        
        var labelArr = label.match(/.{1,8}/g); //divide a string a cada 8 caracteres
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';

        if(id.length > 1){
            for (i = 0; i < id.length; i++) { 
                var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id[i]+'" title="Click para remover esse unidade">'+labelArr[i]+'</div>';
                var content = varReportjornada.widgetObj.find('.selectedUnitList').html();
                if(divTodos == div){
                    varReportjornada.widgetObj.find('.selectedUnitList').html(divTodos);
                    return false;
                }
                if((content.indexOf(div) < 0) && (div != divTodos)){
                    if(content == divTodos){
                        varReportjornada.widgetObj.find('.selectedUnitList').html(div);
                    }else{
                        varReportjornada.widgetObj.find('.selectedUnitList').append(div);
                    }
                }
            }
        }else{  
            var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id+'" title="Click para remover esse unidade">'+label+'</div>';
            var content = varReportjornada.widgetObj.find('.selectedUnitList').html();
            if(divTodos == div){
                varReportjornada.widgetObj.find('.selectedUnitList').html(divTodos);
                return false;
            }
            if((content.indexOf(div) < 0) && (div != divTodos)){
                if(content == divTodos){
                    varReportjornada.widgetObj.find('.selectedUnitList').html(div);
                }else{
                    varReportjornada.widgetObj.find('.selectedUnitList').append(div);
                }  
            }        
        }
    }); 

    varReportjornada.widgetObj.find('.selectedUnit').die('click').live('click', function (){
        $(this).remove();
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';
        var content = varReportjornada.widgetObj.find('.selectedUnitList').html();

        if(content == ''){
            varReportjornada.widgetObj.find('.selectedUnitList').html(divTodos);
        }
    }); 

    varReportjornada.widgetObj.find('.closeReportTab').die('click').live('click', function (){
        var id = $(this).attr('id');
        varReportjornada.widgetObj.find('#tab-result-jornada-'+id).empty().remove();
        varReportjornada.widgetObj.find('#result-jornada-'+id).empty().remove();
        varReportjornada.widgetObj.find('#search-jornada').trigger('click');
        window.setTimeout(function() {
            varReportjornada.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled'); 
        }, 50);  
        varReportjornada.dataFilterArr[id] = null;
        varReportjornada.table[id]         = null; 
    });  

    varReportjornada.widgetObj.find('#search').die('click').live('click', function (){
        varReportjornada.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');     
    }); 

    // FORM TIPO DE CONTA
    varReportjornada.widgetObj.find('form').submit(function(e) {
        e.preventDefault();
        if ( $(this).parsley().isValid() ) {       
            
            var unitIds = [];

            varReportjornada.widgetObj.find(".selectedUnitList > div").each( function(index, value) {
                unitIds.push($(this).attr('id'));
            });
            
            var dataFilter = 
                {
                    last_id: 0,
                    start_day: $(this).find('#start_day').val(),
                    end_day: $(this).find('#end_day').val(),
                    start_hour: $(this).find('#start_hour').val(),
                    end_hour: $(this).find('#end_hour').val(),
                    group:  $(this).find('select#group').val(),
                    subgroup:  $(this).find('select#subgroup').val(),
                    units: unitIds,
                    limit: 0
                };
            dataFilter = JSON.stringify(dataFilter);
            
            $.ajax({
                async: true,
                type: 'POST',
                url: 'widgets/routing.php',
                data: { url: 'reportjornada/search/', data: JSON.parse(dataFilter) },
                dataType: 'json',
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportjornada) != 'object'){ return false; }
                    loadingWidget('#'+varReportjornada.widgetName);
                },
                
                success: function(data) {

                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportjornada) != 'object'){ return false; }
                    varReportjornada.dataFilterArr.push(dataFilter);
                    loadingWidget('#'+varReportjornada.widgetName, true); 

                    varReportjornada.widgetObj.find('.tab-content').append('<div class="col-md-12 tab-pane" id="tab-result-jornada-'+varReportjornada.tabResults+'"></div>');
                    varReportjornada.widgetObj.find('#widgetsreportjornada-nav').append('<li class=""><a href="#tab-result-jornada-'+varReportjornada.tabResults+'"  id="result-jornada-'+varReportjornada.tabResults+'" data-toggle="tab">Resultado <i class="fa fa-times closeReportTab" id="'+varReportjornada.tabResults+'"></i></a></li>');

                    varReportjornada.widgetObj.find('#result-jornada-'+varReportjornada.tabResults).die('click').live('click', function (){
                        var id = $(this).attr('id');
                        id = id.replace('result-jornada-', '');

                        var dropDownMenu = '<li><a href="javascript:;" onClick="widgetsReportJornada.print(\'print\', '+id+')">Imprimir</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportJornada.print(\'PDF\', '+id+')">Exportar PDF</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportJornada.print(\'CSV\', '+id+')">Exportar CSV</a></li>';

                        varReportjornada.widgetObj.find('#widgetsreportjornada-dropdown').html(dropDownMenu);   
                        varReportjornada.widgetObj.find('.dropdown-toggle').removeAttr("disabled");                 
                    }); 
                    var numRows = varReportjornada.rowSizeMin;
                    var panelWidget = varReportjornada.widgetObj.closest('.panel');
                    if(panelWidget.hasClass('full-Scr')){
                        numRows = Math.round(panelWidget.height() / varReportjornada.rowSizeMax) - varReportjornada.rowAdjust;
                    }
                    varReportjornada.widgetObj.find('#tab-result-jornada-'+varReportjornada.tabResults).html( '<table class="table table-striped table-bordered tableJornada" data-page-length="'+numRows+'" id="table-list-result-jornada-'+varReportjornada.tabResults+'"></table>' );
                    varReportjornada.table.push(null);

                    //varReportjornada.table[varReportjornada.tabResults] = varReportjornada.widgetObj.find('#table-list-result-jornada-'+varReportjornada.tabResults).html(data);

                    var oTable = varReportjornada.table[varReportjornada.tabResults] = varReportjornada.widgetObj.find('#table-list-result-jornada-'+varReportjornada.tabResults).DataTable({
                        "data": data,
                        paginate: true,
                        bLengthChange: false,
                        bFilter: false,
                        bInfo: true,
                        buttons: true,
                        "ordering": true,
                        "pagingType": "full",
                        "fnDrawCallback": function( oSettings ) {
                            //console.log(oSettings);
                            
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;                            

                            varReportjornada.widgetObj.find('#table-list-result-jornada-'+varReportjornada.tabResults+' thead').hide();
                             varReportjornada.widgetObj.find('#table-list-result-jornada-'+varReportjornada.tabResults+' table.tableDetalhes thead').show();
                            
                            window.setTimeout(function() {
                                buildRevGeoFromTable(varReportjornada.widgetName);
                            }, 500);
                        },
                        "columns": 
                            [
                                
                                { "title": "Placa", "data": "label", "className": "100_p"}
                                
                            ],
                        
                        "columnDefs": 
                            [
                                {
                                    "targets": [0],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {

                                        var content;
                                        content  = '<div class="col-md-12 col-sm-12 m-b-12" style="padding:0 !important;">';
                                        content += '        <div class="groupTable col-md-12">'+full.label+'</div>';

                                        content += '        <div class="col-md-12 noPadding">';
                                        content += '            <div class="col-md-2 labelCol">Data</div>';
                                        content += '            <div class="col-md-4 labelCol">Inicio</div>';
                                        content += '            <div class="col-md-4 labelCol">Fim</div>';
                                        content += '            <div class="col-md-2 labelCol">Total</div>';
                                        content += '        </div>';
                                        
                                        $(full.itens).each(function(index, val){
                                        //////////////////////////////////////////////////

                                        if(val.poi_inicio == null || val.poi_fim == null){
                                            var poi_inicio = "";
                                            var poi_fim = "";
                                        }else{
                                            var poi_inicio = val.poi_inicio;
                                            var poi_fim = val.poi_fim;
                                        }

                                        if(val.fim_long == null || val.fim_lat == null){
                                            var fim_long = "";
                                            var fim_lat = "";
                                        }else{
                                            var fim_long = val.fim_long;
                                            var fim_lat = val.fim_lat;
                                        }

                                        if(val.hora_fim == null || val.total_jornada == null){
                                            var hora_fim = "";
                                            var total_jornada = "";
                                        }else{
                                            var hora_fim = val.hora_fim;
                                            var total_jornada = widgetsReportJornada.formatHora(val.total_jornada);
                                        }

                                        if(val.last_history_id == null){
                                            last_hist_id = 0;
                                        }else{
                                            last_hist_id = val.last_history_id;
                                        }

                                        content += '        <div class="col-md-12 rowLabel noPadding">';
                                        content += '            <div class="col-md-2 labelRow">';
                                        content += '                <div class="nowrap ellipsis text-center" style="margin-top:27px;">'+val.data_relatorio+'</div>';
                                        content += '            </div>'
                                        content += '            <div class="col-md-4 labelRow">';
                                        content += '                Hora: '+val.hora_inicio+'<br />';
                                        if(val.initial_address == ''){
                                            content += '            Endereço: <div class="mapAddress nowrap ellipsis" hist="'+val.initial_history_id+'" long="'+val.inicio_long+'" lat="'+val.inicio_lat+'">Carregando...</div>';
                                        }else{
                                            content += '            Endereço: <div class=" nowrap ellipsis" title="'+val.initial_address+'">'+val.initial_address+'</div>';
                                        }
                                        content += '                POI: '+poi_inicio+'<br />';

                                        content += '             </div>';
                                        content += '             <div class="col-md-4 labelRow">';
                                        content += '                Hora: '+hora_fim+'<br />';
                                        if(fim_long == ''){
                                            content += '            Endereço: <br />';
                                        }else{
                                            content += '            Endereço: <div class="mapAddress nowrap ellipsis" hist="'+last_hist_id+'" long="'+val.fim_long+'" lat="'+val.fim_lat+'">Carregando...</div>';
                                        }
                                        content += '                POI: '+poi_fim+'<br />';
                                        content += '             </div>';
                                        content += '             <div class="col-md-2 labelRow">'
                                        content += '                <div class="nowrap ellipsis text-center" style="margin-top:27px;">'+ total_jornada +'</div>';
                                        content += '             </div>';
                                        content += '        </div>';

                                        });

                                        return content;
                                        //return full.itens;
                                    }
                                }
                            ],
                        "language" : {
                            "emptyTable":     "Ainda não há registros",
                            "info" : "Total de _TOTAL_ registros",
                            "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
                            "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
                            "infoPostFix":    "",
                            "thousands":      ".",
                            "lengthMenu":     "Mostre _MENU_ registros",
                            "loadingRecords": "Carregando...",
                            "processing":     "Processando...",
                            "search":         "",
                            "searchPlaceholder": "Procure por unidade...",
                            "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
                            "paginate": {
                                "first":      "<<",
                                "last":       ">>",
                                "next":       ">",
                                "previous":   "<"
                            }
                        },
                        //"order": [[ 0, 'desc' ], [ 0, 'asc' ], [ 1, 'desc' ]],
                        "initComplete": function(settings) {
                            if( data.length > 0){
                                varReportjornada.api.push(this.api());

                                dataFilter = JSON.parse(dataFilter);
                                dataFilter.last_id = data[data.length - 1].id;
                                if( data.length >= varReportjornada.limitIncrement ){
                                    widgetsReportJornada.processingSearch(varReportjornada.tabResults, dataFilter);
                                } 
                                
                                varReportjornada.widgetObj.find('#table-list-result-jornada-'+varReportjornada.tabResults+' tbody tr').die("click").live("click", function(){          
                                    var label = $(this).children('td').html();
                                    var lat = $(label).attr('lat');
                                    var lng = $(label).attr('long');
                                    label = $(label).html();

                                    var drawMark = [];
                                        drawMark.size = varGlobal.marker.Size;
                                        drawMark.symbol = varGlobal.marker.carSymbol;
                                        drawMark.color = varGlobal.marker.color;

                                    widgetsReportJornada.openMap(lat, lng, drawMark);
                                }); 
                            }
                        }
                    });

                    //reseta o form   
                    varReportjornada.widgetObj.find('#result-jornada-'+varReportjornada.tabResults).trigger('click');  
                    varReportjornada.tabResults++;
                    //resetReportAction(); 
                },
                error: function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportjornada) != 'object'){ return false; }
                    loadingWidget('#'+varReportjornada.widgetName, true);
                    //Criando mensagem de retorno
                    createMsgBox('error', error.responseText, '#'+varReportjornada.widgetName+' .panel-body'); 
                }
            });
            return false;
        }
    }); 
});

