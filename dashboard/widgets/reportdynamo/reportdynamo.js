var varDynamo = {};
    varDynamo.widgetName    = 'widgetsreportdynamo';
    varDynamo.widgetObj     = $('#'+varDynamo.widgetName);
    varDynamo.numEventsG    = 0;
    varDynamo.numEvents     = 0;
    varDynamo.numSearchs    = 0;
    varDynamo.backupElem    = '';
    varDynamo.tabResults    = 0;
    varDynamo.table         = [];
    varDynamo.api           = [];
    varDynamo.dataFilterArr = [];
    varDynamo.processingSearchLimit = 1000;
    varDynamo.callMap;
    varDynamo.unitInfo;

var widgetsReportDynamo = {
    reset: function(){
        varDynamo.widgetObj.find('form').each(function() { 
            this.reset() 
        });
        
        form.find('input').removeClass('parsley-success');
        form.find('select').removeClass('parsley-success');
    },
    createJSTrees: function(json){
        varDynamo.widgetObj.find('.eventTree').jstree({
            "checkbox" : {
                "keep_selected_style" : false
            },
            "core": {
                "data" : json,
                "themes":{ "icons": false }
            },
            "plugins" : [ "checkbox"]
        });
    },
    buildGroup: function (data){
        varDynamo.widgetObj.find('#group').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varDynamo.widgetObj.find('#group').append(select);
    },
    buildSubGroup: function (data){
        varDynamo.widgetObj.find('#subgroup').html('<option value="0" selected>Todos</option>');
        var template =  '{{#subgroupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/subgroupinfo}}';
        var select = Mustache.to_html(template, data);
        varDynamo.widgetObj.find('#subgroup').append(select);
    },
    processingSearch: function (idTab, dataFilter){
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportdynamo/search/', data: dataFilter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varDynamo) != 'object'){ return false; }

                varDynamo.widgetObj.find('#table-list-result-'+idTab+'_info').append('<span class="sp-load-'+idTab+'" title="Carregando consulta..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                var data = json;
                // var data = json.rows;
                // var metadata = json.metadata;
                //para de processar caso não exista widget seja fechado.
                if(typeof(varDynamo) != 'object'){ return false; }
                //para de processar caso a tab seja fechada
                if(varDynamo.table[idTab] == null ){ return false; }

                $(data).each(function(i, rows) {                
                    varDynamo.table[idTab].row.add({
                        "label": rows.label, 
                        "data": rows.data,  
                        "hora_min": rows.hora_min, 
                        "full_address": rows.full_address, 
                        "devst_id": rows.devst_id, 
                        "longitude": rows.longitude, 
                        "latitude": rows.latitude, 
                        "velocidade": rows.velocidade, 
                        "event": rows.event, 
                        "full_date": rows.full_date                        
                    });
                }); 
                varDynamo.table[idTab].draw(false);
                //reinicia o geocode caso haja algum registro para ser carregado
                buildRevGeoFromTable(varDynamo.widgetName);

                // //verifica se ainda existem registros para serem buscados
                // if( data.length >= varDynamo.processingSearchLimit ){
                //     dataFilter.last_id = metadata.last_id;

                //     window.setTimeout(function() {
                //         widgetsReportDynamo.processingSearch(idTab, dataFilter); 
                //     }, 50);
                // }    
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varDynamo) != 'object'){ return false; }
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText, '#'+varDynamo.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varDynamo) != 'object'){ return false; }
                varDynamo.widgetObj.find('.sp-load-'+idTab).remove();
            }
        });
    },
    print: function (type, id){
        var filter = JSON.parse(varDynamo.dataFilterArr[id]);
        //removendo limit para retornar todos os dados quando for´exportação
        filter.limit = null;
        filter.last_id = 0;

        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportdynamo/search/', data: filter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varDynamo) != 'object'){ return false; }
                varDynamo.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
                varDynamo.widgetObj.find('.actionMenu .fristAction').append('<span class="sp-load" title="Carregando exportação..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                var data = json.rows;
                //para de processar caso não exista widget seja fechado.
                if(typeof(varDynamo) != 'object'){ return false; }
                switch (type) { 
                    case 'print': 
                        var content = '<style>';
                        content += '    table { border: 1px solid #999; border-bottom: none; width: 100%; }';
                        content += '    thead { border: 5px solid #000; }';
                        content += '    tbody td { border: 1px solid #ccc; }';
                        content += '</style>';        
                        content += '<table><thead"><th>Placa</th><th>Data</th><th>Local</th><th>Velocidade</th><th>Evento</th></thead><tbody>';
                        
                        $(data).each(function(i, row) {
                            content += '<tr><td>'+row.label+'</td><td>'+row.data_report+' às '+row.hora_min+'</td><td>'+row.full_address+'</td><td>'+row.velocidade+' km/h</td><td>'+row.event+'</td></tr>';
                        });            
                        content += '</tbody></table>';

                        var w = window.open("");

                        w.document.write(content);
                        w.print();
                        w.close();

                        break;
                    case 'PDF': 
                        var columns = ["Placa", "Data", "Endereço", "Velocidade", "Evento"];
                        var rows = [];
                        $(data).each(function(i, row) {
                            rows.push([row.label, row.data_report+' às '+row.hora_min, row.full_address, row.velocidade+' km/h', row.event]);
                        });   
                        var doc = new jsPDF('l', 'pt');
                        var dataCurrente = new Date();
                        var year = dataCurrente.getFullYear();
                        var month = dataCurrente.getMonth() + 1;
                        var day = dataCurrente.getDate();

                        doc.autoTable(columns, rows, {
                            theme: 'striped', // 'striped', 'grid' or 'plain'
                            headerStyles: {
                                fillColor: [0, 147, 147],
                                color: [255, 255, 255]
                            },
                            bodyStyles: {
                                overflow: 'linebreak', // visible, hidden, ellipsize or linebreak
                            },
                            margin: {
                                top: 100
                            },
                            beforePageContent: function(data) {
                                var imgData = varGlobal.imgRelLogo;
                                doc.addImage(imgData, 'PNG', 40, 30);
                                doc.text("Histórico", 40, 90);
                            }
                        });
                        doc.save("Histórico_"+year+"-"+month+"-"+day+".pdf");

                        break;
                    case 'CSV': 
                        var content = 'Placa;Data;Hora;Endereço;Velocidade;Evento\r\n';
                        
                        $(data).each(function(i, row) {
                            content += row.label+';'+row.data_report+';'+row.hora_min+';'+row.full_address+';'+row.velocidade+' km/h;'+row.event+'\r\n';
                        });            

                        var a         = document.createElement('a');
                        a.href        = 'data:attachment/csv,' +  escape(content);
                        a.target      = '_blank';
                        a.download    = 'Relatório_Histórico.csv';

                        document.body.appendChild(a);
                        a.click();

                        break;     
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varDynamo) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varDynamo.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varDynamo) != 'object'){ return false; }
                varDynamo.widgetObj.find('.dropdown-toggle').removeAttr("disabled"); 
                varDynamo.widgetObj.find('.sp-load').remove(); 
            }
        });            
    },
    openMap: function (lat, lng, drawMark){
        if(typeof(varMapBox) != 'object'){
            handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 4);

            clearTimeout(varDynamo.callMap);
            varDynamo.callMap = window.setTimeout(function() {
                widgetsReportDynamo.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }
        if(typeof(varMapBox.mapInstance) != 'object'){
            clearTimeout(varDynamo.callMap);
            varDynamo.callMap = window.setTimeout(function() {
                widgetsReportDynamo.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }

        MapBox.centerUnit(lat, lng, false, null, drawMark);
    }
};

function load_widgetsreportdynamo_Page(){
    $.ajax({
        async: true,
        type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'reportdynamo/index/'},
        dataType: 'json',
        beforeSend: function () {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varDynamo) != 'object'){ return false; }

            varDynamo.widgetObj.find('.btn').attr('disabled', 'disabled');
            varDynamo.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varDynamo) != 'object'){ return false; }
            widgetsReportDynamo.createJSTrees(JSON.parse(data.events));
            widgetsReportDynamo.buildGroup(JSON.parse(data.groupinfo));
            widgetsReportDynamo.buildSubGroup(JSON.parse(data.subgroupinfo));

            varDynamo.unitInfo = JSON.parse(data.unitinfo); // guarda a lista com todas as unidades do usuário

            varDynamo.widgetObj.find('form').find('#start_day').val(data.date);
            varDynamo.widgetObj.find('form').find('#end_day').val(data.date);
            varDynamo.widgetObj.find('form').find('#end_hour').val(data.hour);

            varDynamo.widgetObj.find('form').removeClass('hidden');
            varDynamo.widgetObj.find('.load-report').addClass('hidden');
            //liberando menu
            varDynamo.widgetObj.find('.btn').removeAttr("disabled");
            loadingWidget('#'+varDynamo.widgetName, true); 
            varDynamo.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
        },
        error: function(error) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varDynamo) != 'object'){ return false; }

            createMsgBox('error', error.responseText, '#'+varDynamo.widgetName+' .panel-body');
        },
        complete: function() {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varDynamo) != 'object'){ return false; }
            
            varDynamo.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
    load_widgetsreportdynamo_Page();

    varDynamo.widgetObj.find('input.range').ionRangeSlider({
        min: 0,
        max: 300,
        type: 'single',
        postfix: "km/h",
        prettify: false,
        hasGrid: true,
        from: 0,
        to: 300,
        step: 1,
        grid_margin: false,
    });

    varDynamo.widgetObj.find('.clockpicker').clockpicker({
        autoclose: true,
        'default': 'now'
    }); 

    varDynamo.widgetObj.find('.pluginDate').datepicker({ 
        autoclose: true, 
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        todayHighlight: true,
    });

    varDynamo.widgetObj.find('select#group').on('change', function() {
        var select = 'select#subgroup';
        var data = 
        {
            groupIds: $(this).val()
        };
        varDynamo.numEventsG++;
        data = JSON.stringify(data);

        var html = '';
        var selected = 'selected';
        if($(this).val() == 0) {
            html += '<option value="0" selected>Todos</option>';
            selected  = '';
        }
        
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'client/listSubGroups/', data:JSON.parse(data) },
            beforeSend: function () {
                varDynamo.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
                varDynamo.widgetObj.find('select#unit').addClass('select-loader').html('<option class="spinner-small select"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varDynamo) != 'object'){ return false; }

                try {
                    var subgroups = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varDynamo.widgetName+' .panel-body');              
                    varDynamo.widgetObj.find(select).html('');
                    varDynamo.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                
                $(subgroups).each(function(i, val) {
                    html+='<option value="'+val.id+'" '+selected+'>'+val.name+'</option>';
                });

                varDynamo.numEventsG--;
                if(varDynamo.numEventsG == 0) {
                    varDynamo.widgetObj.find(select).removeClass('select-loader');
                    varDynamo.widgetObj.find(select).html(html);
                    varDynamo.widgetObj.find('select#subgroup').change();
                }
            },
            error: function(error) {
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varDynamo.widgetName+' .panel-body');                 
                varDynamo.widgetObj.find(select).html(error.responseText);
                varDynamo.widgetObj.find(select).removeClass('select-loader');
            }
        });     
    });
    
    varDynamo.widgetObj.find('select#subgroup').on('change', function() {
        var select = 'select#unit';

        var html = '';      
        if($(this).val() == '0') {
            html += '<option value="0">Todos</option>';
        }
        var data = 
        {
            subGroupIds: $(this).val()
        };
        varDynamo.numEvents++;
        
        data = JSON.stringify(data);
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'unit/listTrackedUnit/', data:JSON.parse(data) },
            beforeSend: function () {
                varDynamo.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varDynamo) != 'object'){ return false; }
                
                try {
                    var unities = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varDynamo.widgetName+' .panel-body');              
                    varDynamo.widgetObj.find(select).html('');
                    varDynamo.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                html = '<option value="0" selected>Todos</option>';
                $(unities).each(function(i, val) {
                    if(val.label != undefined){
                        html += '<option value="'+val.id+'">'+val.label+'</option>';                            
                    }
                });
                
                varDynamo.numEvents--;
                if(varDynamo.numEvents == 0) {
                    varDynamo.widgetObj.find(select).removeClass('select-loader');
                    varDynamo.widgetObj.find(select).html(html);
                }                   
            },
            error: function(error) {
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varDynamo.widgetName+' .panel-body');                 
                varDynamo.widgetObj.find(select).html(error.responseText);
                varDynamo.widgetObj.find(select).removeClass('select-loader');
            }
        });
    });

    varDynamo.widgetObj.find('input#searchUnits').on('keyup', function(e) {
        e.preventDefault();
        if(varDynamo.backupElem == '') {
            varDynamo.backupElem = varDynamo.widgetObj.find('select#unit').html();
        }
        if($(this).val().length > 2) {
            var select = 'select#unit';
            var html = '';
            var data = 
            {
                unit: $(this).val()
            };
            data = JSON.stringify(data);
            varDynamo.numSearchs++;
             $.ajax({
                async : true,
                type : "POST",
                data: { url: 'unit/searchUnit', data: JSON.parse(data) },
                url : 'widgets/routing.php',
                dataType : "json",
                beforeSend: function () {
                    varDynamo.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
                },
                success : function(json) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varDynamo) != 'object'){ return false; }
                
                    varDynamo.numSearchs--;
                    if(json.length <= 0) {
                        if(varDynamo.numSearchs == 0) {
                            varDynamo.widgetObj.find(select).html('');
                            varDynamo.widgetObj.find(select).removeClass('select-loader');
                            return false;
                        }
                    }
                    html+='<option value="0" selected>Todos</option>';
                    $(json).each(function(i, val) {
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    });
                    if(varDynamo.numSearchs == 0) {
                        varDynamo.widgetObj.find(select).removeClass('select-loader');
                        varDynamo.widgetObj.find(select).html(html);
                    }
                },
                error : function(error) {
                    varDynamo.numSearchs--;
                    if(varDynamo.numSearchs == 0) {
                        createMsgBox('error', error.responseText, "#"+varDynamo.widgetName+" .panel-body");
                        varDynamo.widgetObj.find(select).removeClass('select-loader');
                    }
                }
            });
        } else if($(this).val().length <= 2) {
            if(varDynamo.backupElem) {
                varDynamo.widgetObj.find('select#unit').html(varDynamo.backupElem);
                varDynamo.backupElem = '';
            }
        } else {
            varDynamo.backupElem = '';
            return false;       
        }
    });

    varDynamo.widgetObj.find('.closeReportTab').die('click').live('click', function (){
        var id = $(this).attr('id');
        varDynamo.widgetObj.find('#tab-result-'+id).empty().remove();
        varDynamo.widgetObj.find('#result-'+id).empty().remove();
        varDynamo.widgetObj.find('#search').trigger('click');
        window.setTimeout(function() {
            varDynamo.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled'); 
        }, 50);  
        varDynamo.dataFilterArr[id] = null;
        varDynamo.table[id]         = null; 
    });

    varDynamo.widgetObj.find('#unit').die('click').live('click', function (){
        var id = $(this).val();
        var label = $(this).find('option:selected').text();        
        var labelArr = label.match(/.{1,8}/g); //divide a string a cada 8 caracteres
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';

        if(id.length > 1){
            for (i = 0; i < id.length; i++) { 
                var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id[i]+'" title="Click para remover esse unidade">'+labelArr[i]+'</div>';
                var content = varDynamo.widgetObj.find('.selectedUnitList').html();
                if(divTodos == div){
                    varDynamo.widgetObj.find('.selectedUnitList').html(divTodos);
                    return false;
                }
                if((content.indexOf(div) < 0) && (div != divTodos)){
                    if(content == divTodos){
                        varDynamo.widgetObj.find('.selectedUnitList').html(div);
                    }else{
                        varDynamo.widgetObj.find('.selectedUnitList').append(div);
                    }
                }
            }
        }else{  
            var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id+'" title="Click para remover esse unidade">'+label+'</div>';
            var content = varDynamo.widgetObj.find('.selectedUnitList').html();
            if(divTodos == div){
                varDynamo.widgetObj.find('.selectedUnitList').html(divTodos);
                return false;
            }
            if((content.indexOf(div) < 0) && (div != divTodos)){
                if(content == divTodos){
                    varDynamo.widgetObj.find('.selectedUnitList').html(div);
                }else{
                    varDynamo.widgetObj.find('.selectedUnitList').append(div);
                }  
            }        
        }

    }); 

    varDynamo.widgetObj.find('.selectedUnit').die('click').live('click', function (){
        $(this).remove();
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';
        var content = varDynamo.widgetObj.find('.selectedUnitList').html();

        if(content == ''){
            varDynamo.widgetObj.find('.selectedUnitList').html(divTodos);
        }
    });  

    varDynamo.widgetObj.find('#search').die('click').live('click', function (){
        varDynamo.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');     
    }); 

    varDynamo.widgetObj.find('form').submit(function(e) {
        e.preventDefault();
        if ( $(this).parsley().isValid() ) {  
            var selectedElmsIds = [];
            var selectedElms = $(this).find('.eventTree').jstree("get_selected", true);
            
            selectedElmsIds.push(0);
            $.each(selectedElms, function() {
                if(this.children.length == 0)
                {
                    selectedElmsIds.push(this.id);
                }
            });            
            //Pegando velocidade inicial e final
            var speed = $(this).find('#speed').val();
            speed = speed.split(';');

            var start_km = parseInt(speed[0]);
            var end_km = parseInt(speed[1]);
            
            var unitIds = [];
            var allUnit = false; // controla se o usuário selecionou todas as viaturas ou algumas
            varDynamo.widgetObj.find(".selectedUnitList > div").each( function(index, value) {
                unitIds.push($(this).attr('id'));
                if($(this).attr('id') == '0'){
                    allUnit = true;
                }else{
                    allUnit = false;
                }
            });
            // Caso usuário não tenha selecionado nenhuma unidade
            // O frontend envia a lista de todas as unidades para o backand
            if(allUnit){
                unitIds = [];
                $.each(varDynamo.unitInfo.data, function(index, val) {
                    unitIds.push(val.id);
                });
            }
            
            var dataFilter = 
                {
                    last_id: 0,
                    start_day: $(this).find('#start_day').val(),
                    end_day: $(this).find('#end_day').val(),
                    start_hour: $(this).find('#start_hour').val(),
                    end_hour: $(this).find('#end_hour').val(),
                    events: selectedElmsIds,
                    group:  $(this).find('select#group').val(),
                    subgroup:  $(this).find('select#subgroup').val(),
                    units: unitIds,
                    start_km: start_km,
                    end_km: end_km,
                    limit: varDynamo.processingSearchLimit
                };

            dataFilter = JSON.stringify(dataFilter);
            
            $.ajax({
                async: true,
                type: 'POST',
                url: 'widgets/routing.php',
                data: { url: 'reportdynamo/search/', data: JSON.parse(dataFilter) },
                dataType: 'json',
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varDynamo) != 'object'){ return false; }

                    loadingWidget('#'+varDynamo.widgetName);
                },
                success: function(json) {
                    var data = json;
                    // var data = json.rows;
                    // var metadata = json.metadata;
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varDynamo) != 'object'){ return false; }
                
                    varDynamo.dataFilterArr.push(dataFilter);
                    loadingWidget('#'+varDynamo.widgetName, true); 

                    varDynamo.widgetObj.find('.tab-content').append('<div class="col-md-12 tab-pane" id="tab-result-'+varDynamo.tabResults+'"></div>');
                    varDynamo.widgetObj.find('#widgetsreportdynamo-nav').append('<li class=""><a href="#tab-result-'+varDynamo.tabResults+'"  id="result-'+varDynamo.tabResults+'" data-toggle="tab">Resultado <i class="fa fa-times closeReportTab" id="'+varDynamo.tabResults+'"></i></a></li>');

                    varDynamo.widgetObj.find('#result-'+varDynamo.tabResults).die('click').live('click', function (){
                        var id = $(this).attr('id');
                        id = id.replace('result-', '');

                        var dropDownMenu = '<li><a href="javascript:;" onClick="widgetsReportDynamo.print(\'print\', '+id+')" class="a_print">Imprimir</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportDynamo.print(\'PDF\', '+id+')" class="a_pdf">Exportar PDF</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportDynamo.print(\'CSV\', '+id+')" class="a_csv">Exportar CSV</a></li>';

                        varDynamo.widgetObj.find('#widgetsreportdynamo-dropdown').html(dropDownMenu);   
                        varDynamo.widgetObj.find('.dropdown-toggle').removeAttr("disabled");                 
                    }); 
                    var numRows = 10;
                    var panelWidget = varDynamo.widgetObj.closest('.panel');
                    if(panelWidget.hasClass('full-Scr')){
                        numRows = Math.round(panelWidget.height() / 28) - 5;
                    }
                    varDynamo.widgetObj.find('#tab-result-'+varDynamo.tabResults).html( '<table class="table table-striped table-bordered" data-page-length="'+numRows+'" id="table-list-result-'+varDynamo.tabResults+'"></table>' );
                    varDynamo.table.push(null);
                    varDynamo.table[varDynamo.tabResults] = varDynamo.widgetObj.find('#table-list-result-'+varDynamo.tabResults).DataTable({
                        "data": data,
                        paginate: true,
                        bLengthChange: false,
                        bFilter: false,
                        bInfo: true,
                        "ordering": true,
                        "pagingType": "full",
                        "fnDrawCallback": function( oSettings ) {
                            window.setTimeout(function() {
                                buildRevGeoFromTable(varDynamo.widgetName);
                            }, 500);
                        },
                        "columns": 
                            [
                                { "title": "<div class='labelHeader nowrap ellipsis'>Placa</div>", "data": "label", "className": "larguraPlaca" },
                                { "title": "<div class='labelHeader nowrap ellipsis'>Data</div>", "data": "data", "className": "larguraData" },
                                { "title": "<div class='labelHeader nowrap ellipsis'>Local</div>", "data": "full_address", "className": "40_p" },
                                { "title": "<div class='labelHeader nowrap ellipsis'>Velocidade</div>", "data": "velocidade", "className": "10_p" },
                                { "title": "<div class='labelHeader nowrap ellipsis'>Evento</div>", "data": "event", "className": "20_p" },
                                { "title": "Date Order", "data": "full_date", "visible": false }
                            ],
                        "columnDefs": 
                            [
                                {
                                    "targets": [0],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" long="'+full.longitude+'" lat="'+full.latitude+'">' + data + '</div>';
                                    }
                                },
                                {
                                    "targets": [1],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + full.data + ' às ' + full.hora_min + '">' + full.data + ' às ' + full.hora_min + '</div>';
                                    }
                                },
                                {
                                    "targets": [2],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        //verifica se este endereco ja existe no banco
                                        if(data != '') {
                                            //se sim ja retorna o endereco
                                            return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                        } else {
                                            //se nao faz o reversegeo para capturar o endereco e salvar no banco
                                            return "<div class='mapAddress nowrap ellipsis' hist='"+full.devst_id+"' long='"+full.longitude+"' lat='"+full.latitude+"'>Carregando...</div>" ;
                                        }
                                    }
                                },
                                {
                                    "targets": [3],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + ' km/h</div>';
                                    }
                                },
                                {
                                    "targets": [4],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                    }
                                }
                            ],
                        "language" : {
                            "emptyTable":     "Ainda não há registros",
                            "info" : "Total de _TOTAL_ registros",
                            "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
                            "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
                            "infoPostFix":    "",
                            "thousands":      ".",
                            "lengthMenu":     "Mostre _MENU_ registros",
                            "loadingRecords": "Carregando...",
                            "processing":     "Processando...",
                            "search":         "",
                            "searchPlaceholder": "Procure por unidade...",
                            "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
                            "paginate": {
                                "first":      "<<",
                                "last":       ">>",
                                "next":       ">",
                                "previous":   "<"
                            }
                        },
                        "order": [[ 5, 'desc' ], [ 0, 'asc' ], [ 1, 'desc' ]],
                        "initComplete": function(settings) {
                            if( data.length > 0){
                                varDynamo.api.push(this.api());

                                varDynamo.widgetObj.find('.larguraData').css('width', '120px');
                                varDynamo.widgetObj.find('.larguraPlaca').css('width', '80px');
                                varDynamo.widgetObj.find('.10_p').css('width', '10%');
                                varDynamo.widgetObj.find('.20_p').css('width', '20%');
                                varDynamo.widgetObj.find('.30_p').css('width', '30%');
                                varDynamo.widgetObj.find('.40_p').css('width', '40%');

                                dataFilter = JSON.parse(dataFilter);
                                // dataFilter.last_id = metadata.last_id;
                                // if( data.length >= varDynamo.processingSearchLimit ){
                                //     widgetsReportDynamo.processingSearch(varDynamo.tabResults, dataFilter);
                                // } 
                                
                                varDynamo.widgetObj.find('#table-list-result-'+varDynamo.tabResults+' tbody tr').die("click").live("click", function(){              
                                    var label = $(this).children('td').html();
                                    var lat = $(label).attr('lat');
                                    var lng = $(label).attr('long');
                                    label = $(label).html();

                                    var drawMark = [];
                                        drawMark.size = varGlobal.marker.Size;
                                        drawMark.symbol = varGlobal.marker.carSymbol;
                                        drawMark.color = varGlobal.marker.color;

                                    widgetsReportDynamo.openMap(lat, lng, drawMark);
                                }); 
                            }
                        }
                    });              
                    //reseta o form   
                    varDynamo.widgetObj.find('#result-'+varDynamo.tabResults).trigger('click');  
                    varDynamo.tabResults++;
                    //Report.reset(); 
                },
                error: function(error) {
                    loadingWidget('#'+varDynamo.widgetName, true);
                    //Criando mensagem de retorno
                    createMsgBox('error', error.responseText, '#'+varDynamo.widgetName+' .panel-body'); 
                }
            });
            return false;
        }
    }); 
});


