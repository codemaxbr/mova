var varReportkm = {};
    varReportkm.widgetName   = 'widgetsreportkm';
    varReportkm.widgetObj    = $('#'+varReportkm.widgetName);
    varReportkm.numEventsG   = 0;
    varReportkm.numEvents    = 0;
    varReportkm.numSearchs   = 0;
    varReportkm.backupElem   = '';
    varReportkm.tabResults   = 0;
    varReportkm.table        = [];
    varReportkm.api           = [];
    varReportkm.dataFilterArr = [];
    varReportkm.processingSearchLimit = 1000;
    varReportkm.callMap;

    varReportkm.rowSizeMax = 80;
    varReportkm.rowSizeMin = 5;
    varReportkm.rowAdjust = 1;

var widgetsreportkm = {    
    buildGroup: function (data){
        varReportkm.widgetObj.find('#group').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varReportkm.widgetObj.find('#group').append(select);
    },
    buildSubGroup: function (data){
        varReportkm.widgetObj.find('#subgroup').html('<option value="0" selected>Todos</option>');
        var template =  '{{#subgroupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/subgroupinfo}}';
        var select = Mustache.to_html(template, data);
        varReportkm.widgetObj.find('#subgroup').append(select);
    },
    processingSearch: function (idTab, dataFilter){
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportkm/search/', data: dataFilter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }

                varReportkm.widgetObj.find('#table-list-result-km-'+idTab+'_info').append('<span class="sp-load-'+idTab+'" title="Carregando consulta..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                //para de processar caso a tab seja fechada
                if(varReportkm.table[idTab] == null ){ return false; }

                //transformando linha em coluna
                var newData = widgetsreportkm.lineToColumn(json);
                
                $(newData).each(function(i, rows) {                
                    varReportkm.table[idTab].row.add({
                        "label": rows.label, 
                        "dom": rows.dom + ' Km',  
                        "seg": rows.seg + ' Km',
                        "ter": rows.ter + ' Km', 
                        "qua": rows.qua + ' Km',  
                        "qui": rows.qui + ' Km',
                        "sex": rows.sex + ' Km',  
                        "sab": rows.sab + ' Km', 
                        "kmTotal": rows.kmTotal + ' Km'
                    });
                }); 
                varReportkm.table[idTab].draw(false);
                //reinicia o geocode caso haja algum registro para ser carregado
                buildRevGeoFromTable(varReportkm.widgetName);
                //verifica se ainda existem registros para serem buscados
                if( json.length >= varReportkm.processingSearchLimit ){
                    dataFilter.limit += varReportkm.processingSearchLimit;

                    window.setTimeout(function() {
                        widgetsreportkm.processingSearch(idTab, dataFilter); 
                    }, 50);
                }    
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReportkm.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                varReportkm.widgetObj.find('.sp-load-'+idTab).remove();
            }
        });
    },
    print: function (type, id){
        var filter = JSON.parse(varReportkm.dataFilterArr[id]);
        //removendo limit para retornar todos os dados quando for´exportação
        filter.limit = null;

        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportkm/search/', data: filter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                varReportkm.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
                varReportkm.widgetObj.find('.actionMenu .fristAction').append('<span class="sp-load" title="Carregando exportação..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //transformando linha em coluna
                var newData = widgetsreportkm.lineToColumn(json);
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                switch (type) { 
                    case 'print': 
                        var content = '<style>';
                        content += '    table { border: 1px solid #999; border-bottom: none; width: 100%; }';
                        content += '    thead { border: 5px solid #000; }';
                        content += '    tbody td { border: 1px solid #ccc; }';
                        content += '</style>';        
                        content += '<table><tbody>';
                        
                        $(newData).each(function(i, row) {
                            content += '<tr><td colspan="8">'+row.label+'</td></tr>';
                            content += '<tr>';
                            content += '    <td>Domingo</td>';
                            content += '    <td>Segunda</td>';
                            content += '    <td>Terça</td>';
                            content += '    <td>Quarta</td>';
                            content += '    <td>Quinta</td>';
                            content += '    <td>Sexta</td>';
                            content += '    <td>Sabado</td>';
                            content += '    <td>Total</td>';
                            content += '</tr>';
                            content += '<tr>';
                            content += '    <td>'+row.dom+'</td>';
                            content += '    <td>'+row.seg+'</td>';
                            content += '    <td>'+row.ter+'</td>';
                            content += '    <td>'+row.qua+'</td>';
                            content += '    <td>'+row.qui+'</td>';
                            content += '    <td>'+row.sex+'</td>';
                            content += '    <td>'+row.sab+'</td>';
                            content += '    <td>'+row.kmTotal+'</td>';
                            content += '</tr>';
                        });            
                        content += '</tbody></table>';

                        var w = window.open("");

                        w.document.write(content);
                        w.print();
                        w.close();

                        break;
                    case 'PDF': 
                        var doc = new jsPDF('l', 'pt');
                        doc.setFont("helvetica");
                        doc.setFontType("bold");
                        doc.setFontSize(10);
                        doc.setTextColor(0,0,0);
                        doc.setFillColor(175, 185, 189);

                        var dataCurrente = new Date();
                        var year = dataCurrente.getFullYear();
                        var month = dataCurrente.getMonth() + 1;
                        var day = dataCurrente.getDate();

                        var columns = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sabado", "Total"];
                        var rows = [];
                        $(newData).each(function(i, row) {
                            rows[0] = [row.dom, row.seg, row.ter, row.qua, row.qui, row.sex, row.sab, row.kmTotal];
                            if(i == 0){
                                doc.rect(40, 100, 762, 20, 'F'); // empty red square
                                doc.text(row.label, 50, 114); // header com a placa da unidade
                                doc.autoTable(columns, rows, {
                                    theme: 'plain', // 'striped', 'grid' or 'plain'
                                    headerStyles: {
                                        fillColor: [193, 204, 209],
                                        color: [0, 0, 0]
                                    },
                                    bodyStyles: {
                                        overflow: 'linebreak', // visible, hidden, ellipsize or linebreak
                                    },
                                    margin: {
                                        top: 120
                                    },
                                    beforePageContent: function(data) {
                                        var imgData = varGlobal.imgRelLogo;
                                        doc.addImage(imgData, 'PNG', 40, 30);
                                        doc.text("Relatório de Km por semana", 40, 90);
                                    }
                                });
                            }else{        
                                doc.setTextColor(0,0,0); 
                                doc.setFillColor(175, 185, 189); 
                                doc.rect(40, doc.autoTableEndPosY(), 762, 20, 'F'); // empty red square                      
                                doc.text(row.label, 50, doc.autoTableEndPosY() + 15); // header com a placa da unidade
                                doc.autoTable(columns, rows, {
                                    startY: doc.autoTableEndPosY() + 20,
                                    theme: 'plain', // 'striped', 'grid' or 'plain'
                                    headerStyles: {
                                        fillColor: [193, 204, 209],
                                        color: [0, 0, 0]
                                    },
                                    bodyStyles: {
                                        overflow: 'linebreak', // visible, hidden, ellipsize or linebreak
                                    }
                                });
                            }
                        });   


                        doc.save("Relatorio_de_km_por_semana_"+year+"-"+month+"-"+day+".pdf");

                        break;
                    case 'CSV': 
                        var content = '';
                        
                        content += 'Placa;Domingo;Segunda;Terça;Quarta;Quinta;Sexta;Sabado;Total\r\n';
                        $(newData).each(function(i, row) {
                            content += row.label+';'+row.dom+';'+row.seg+';'+row.ter+';'+row.qua+';'+row.qui+';'+row.sex+';'+row.sab+';'+row.kmTotal+'\r\n';
                        });

                        var a         = document.createElement('a');
                        a.href        = 'data:attachment/csv,' +  escape(content);
                        a.target      = '_blank';
                        a.download    = 'Relatório_de_km_por_semana.csv';

                        document.body.appendChild(a);
                        a.click();

                        break;     
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReportkm.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                varReportkm.widgetObj.find('.dropdown-toggle').removeAttr("disabled"); 
                varReportkm.widgetObj.find('.sp-load').remove(); 
            }
        }); 
    },
    openMap: function (lat, lng, drawMark){
        if(typeof(varMapBox) != 'object'){
            handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 4);

            clearTimeout(varReportkm.callMap);
            varReportkm.callMap = window.setTimeout(function() {
                widgetsreportkm.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }
        if(typeof(varMapBox.mapInstance) != 'object'){
            clearTimeout(varReportkm.callMap);
            varReportkm.callMap = window.setTimeout(function() {
                widgetsreportkm.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }

        MapBox.centerUnit(lat, lng, false, null, drawMark);
    },
    lineToColumn: function(data){
        var lastLabel = '';
        var newData = [];
        var weekday = [0, 0, 0, 0, 0, 0, 0];
        var kmTotal = 0;
        $.each(data, function(index, val) {   

            if(val.label != lastLabel && lastLabel != ''){

                newData.push({
                    label: lastLabel,
                    dom: weekday[0] + ' km',
                    seg: weekday[1] + ' km',
                    ter: weekday[2] + ' km',
                    qua: weekday[3] + ' km',
                    qui: weekday[4] + ' km',
                    sex: weekday[5] + ' km',
                    sab: weekday[6] + ' km',
                    kmTotal: kmTotal + ' km'
                });

                weekday = [0, 0, 0, 0, 0, 0, 0];
                kmTotal = 0;
            }

            lastLabel = val.label;
            weekday[parseInt(val.weekday) - 1] = val.km;
            kmTotal += val.km;
        });
        //recupera os dados do ultimo registro
        newData.push({
            label: lastLabel,
            dom: weekday[0] + ' km',
            seg: weekday[1] + ' km',
            ter: weekday[2] + ' km',
            qua: weekday[3] + ' km',
            qui: weekday[4] + ' km',
            sex: weekday[5] + ' km',
            sab: weekday[6] + ' km',
            kmTotal: kmTotal + ' km'
        });

        return newData;
    }
};

function load_widgetsreportkm_Page(){
    $.ajax({
        type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'reportkm/index/'},
        dataType: 'json',
        beforeSend: function () {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportkm) != 'object'){ return false; }
            varReportkm.widgetObj.find('.btn').attr('disabled', 'disabled');
            varReportkm.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportkm) != 'object'){ return false; }

            widgetsreportkm.buildGroup(JSON.parse(data.groupinfo))
            widgetsreportkm.buildSubGroup(JSON.parse(data.subgroupinfo))

            varReportkm.widgetObj.find('form').find('#start_day').val(data.date);
            varReportkm.widgetObj.find('form').find('#end_day').val(data.date);
            varReportkm.widgetObj.find('form').find('#end_hour').val(data.hour);

            varReportkm.widgetObj.find('form').removeClass('hidden');
            varReportkm.widgetObj.find('.load-report').addClass('hidden');
            //liberando menu
            varReportkm.widgetObj.find('.btn').removeAttr("disabled");
            loadingWidget('#'+varReportkm.widgetName, true); 
            varReportkm.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');

        },
        error: function(error) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportkm) != 'object'){ return false; }
            createMsgBox('error', error.responseText, '#'+varReportkm.widgetName+' .panel-body');
        },
        complete: function() {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportkm) != 'object'){ return false; }
            varReportkm.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
    load_widgetsreportkm_Page();
  
    //Datepicker
    varReportkm.widgetObj.find('.pluginDateWeek').datepicker({ 
        autoclose: true, 
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        showWeek: true, 
        selectWeek: true
    }).on('changeDate', function(e) {
        var value = $(this).val();
        var firstDate = moment(value, "DD/MM/YYYY").day(0).format("DD/MM/YYYY");
        var lastDate =  moment(value, "DD/MM/YYYY").day(6).format("DD/MM/YYYY");

        window.setTimeout(function() {
            varReportkm.widgetObj.find('#cday').val('De ' + firstDate + ' até ' + lastDate);
        }, 50);

        varReportkm.widgetObj.find('#start_day').val(firstDate);
        varReportkm.widgetObj.find('#end_day').val(lastDate);
        $('.datepicker-days').find('table').find('tbody').find('tr').die('mouseover');
    });

    varReportkm.widgetObj.find('.pluginDateWeek').die('click').live('click', function (){
        $('.datepicker-days').find('table').find('tbody').find('tr').die('mouseover').live('mouseover', function() {
            var tr = $(this);
            var parent = tr.parent();

            parent.find('tr').removeClass('bg-color-primary'); 
            tr.addClass('bg-color-primary');     
        });
    });  
    $('.datepicker-days').find('table').die('mouseleave').live('mouseleave', function() {
        $('.datepicker-days').find('table').find('tbody').find('tr').die('mouseover');     
    }); 

    varReportkm.widgetObj.find('select#group').on('change', function() {
        var select = 'select#subgroup';
        var data = 
        {
            groupIds: $(this).val()
        };
        varReportkm.numEventsG++;
        data = JSON.stringify(data);

        var html = '';
        var selected = 'selected';
        if($(this).val() == 0) {
            html += '<option value="0" selected>Todos</option>';
            selected  = '';
        }
        
        $.ajax({
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'client/listSubGroups/', data:JSON.parse(data) },
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                varReportkm.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                varReportkm.widgetObj.find('select#unit').addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                try {
                    var subgroups = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReportkm.widgetName+' .panel-body');              
                    varReportkm.widgetObj.find(select).html('');
                    varReportkm.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                
                $(subgroups).each(function(i, val) {
                    html+='<option value="'+val.id+'" '+selected+'>'+val.name+'</option>';
                });

                varReportkm.numEventsG--;
                if(varReportkm.numEventsG == 0) {
                    varReportkm.widgetObj.find(select).removeClass('select-loader');
                    varReportkm.widgetObj.find(select).html(html);
                    varReportkm.widgetObj.find('select#subgroup').change();
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                createMsgBox('error', error.responseText,'#'+varReportkm.widgetName+' .panel-body');                 
                varReportkm.widgetObj.find(select).html(error.responseText);
                varReportkm.widgetObj.find(select).removeClass('select-loader');
            }
        });     
    });
    
    varReportkm.widgetObj.find('select#subgroup').on('change', function() {
        var select = 'select#unit';

        var html = '';      
        if($(this).val() == '0') {
            html += '<option value="0">Todos</option>';
        }
        var data = 
        {
            subGroupIds: $(this).val()
        };
        varReportkm.numEvents++;
        
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'unit/listTrackedUnit/', data:JSON.parse(data) },
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                varReportkm.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                try {
                    var unities = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReportkm.widgetName+' .panel-body');              
                    varReportkm.widgetObj.find(select).html('');
                    varReportkm.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                html = '<option value="0" selected>Todos</option>';
                $(unities).each(function(i, val) {
                    if(val.label != undefined){
                        html += '<option value="'+val.id+'">'+val.label+'</option>';
                    }
                });
                
                varReportkm.numEvents--;
                if(varReportkm.numEvents == 0) {
                    varReportkm.widgetObj.find(select).removeClass('select-loader');
                    varReportkm.widgetObj.find(select).html(html);
                }                   
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportkm) != 'object'){ return false; }
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varReportkm.widgetName+' .panel-body');                 
                varReportkm.widgetObj.find(select).html(error.responseText);
                varReportkm.widgetObj.find(select).removeClass('select-loader');
            }
        });
    });

    varReportkm.widgetObj.find('input#searchUnits').on('keyup', function(e) {
        e.preventDefault();
        if(varReportkm.backupElem == '') {
            varReportkm.backupElem = varReportkm.widgetObj.find('select#unit').html();
        }
        if($(this).val().length > 2) {
            var select = 'select#unit';
            var html = '';
            var data = 
            {
                unit: $(this).val()
            };
            data = JSON.stringify(data);
            varReportkm.numSearchs++;
             $.ajax({
                async : true,
                type : "POST",
                data: { url: 'unit/searchUnit', data: JSON.parse(data) },
                url : 'widgets/routing.php',
                dataType : "json",
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportkm) != 'object'){ return false; }
                    varReportkm.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                },
                success : function(json) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportkm) != 'object'){ return false; }
                    varReportkm.numSearchs--;
                    if(json.length <= 0) {
                        if(varReportkm.numSearchs == 0) {
                            varReportkm.widgetObj.find(select).html('');
                            varReportkm.widgetObj.find(select).removeClass('select-loader');
                            return false;
                        }
                    }
                    html+='<option value="0" selected>Todos</option>';
                    $(json).each(function(i, val) {
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    });
                    if(varReportkm.numSearchs == 0) {
                        varReportkm.widgetObj.find(select).removeClass('select-loader');
                        varReportkm.widgetObj.find(select).html(html);
                    }
                },
                error : function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportkm) != 'object'){ return false; }
                    varReportkm.numSearchs--;
                    if(varReportkm.numSearchs == 0) {
                        createMsgBox('error', error.responseText, "#"+varReportkm.widgetName+" .panel-body");
                        varReportkm.widgetObj.find(select).removeClass('select-loader');
                    }
                }
            });
        } else if($(this).val().length <= 2) {
            if(varReportkm.backupElem) {
                varReportkm.widgetObj.find('select#unit').html(varReportkm.backupElem);
                varReportkm.backupElem = '';
            }
        } else {
            varReportkm.backupElem = '';
            return false;       
        }
    });

    varReportkm.widgetObj.find('#unit').die('click').live('click', function (){
        var id = $(this).val();
        var label = $(this).find('option:selected').text();        
        var labelArr = label.match(/.{1,8}/g); //divide a string a cada 8 caracteres
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';

        if(id.length > 1){
            for (i = 0; i < id.length; i++) { 
                var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id[i]+'" title="Click para remover esse unidade">'+labelArr[i]+'</div>';
                var content = varReportkm.widgetObj.find('.selectedUnitList').html();
                if(divTodos == div){
                    varReportkm.widgetObj.find('.selectedUnitList').html(divTodos);
                    return false;
                }
                if((content.indexOf(div) < 0) && (div != divTodos)){
                    if(content == divTodos){
                        varReportkm.widgetObj.find('.selectedUnitList').html(div);
                    }else{
                        varReportkm.widgetObj.find('.selectedUnitList').append(div);
                    }
                }
            }
        }else{  
            var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id+'" title="Click para remover esse unidade">'+label+'</div>';
            var content = varReportkm.widgetObj.find('.selectedUnitList').html();
            if(divTodos == div){
                varReportkm.widgetObj.find('.selectedUnitList').html(divTodos);
                return false;
            }
            if((content.indexOf(div) < 0) && (div != divTodos)){
                if(content == divTodos){
                    varReportkm.widgetObj.find('.selectedUnitList').html(div);
                }else{
                    varReportkm.widgetObj.find('.selectedUnitList').append(div);
                }  
            }        
        }
    }); 

    varReportkm.widgetObj.find('.selectedUnit').die('click').live('click', function (){
        $(this).remove();
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';
        var content = varReportkm.widgetObj.find('.selectedUnitList').html();

        if(content == ''){
            varReportkm.widgetObj.find('.selectedUnitList').html(divTodos);
        }
    }); 

    varReportkm.widgetObj.find('.closeReportTab').die('click').live('click', function (){
        var id = $(this).attr('id');
        varReportkm.widgetObj.find('#tab-result-km-'+id).empty().remove();
        varReportkm.widgetObj.find('#result-km-'+id).empty().remove();
        varReportkm.widgetObj.find('#search-km').trigger('click');
        window.setTimeout(function() {
            varReportkm.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled'); 
        }, 50);  
        varReportkm.dataFilterArr[id] = null;
        varReportkm.table[id]         = null; 
    });  

    varReportkm.widgetObj.find('#search').die('click').live('click', function (){
        varReportkm.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');     
    }); 
    
    varReportkm.widgetObj.find('form').submit(function(e) {
        e.preventDefault();
        if ( $(this).parsley().isValid() ) {       
            
            var unitIds = [];

            varReportkm.widgetObj.find(".selectedUnitList > div").each( function(index, value) {
                unitIds.push($(this).attr('id'));
            });
            
            var dataFilter = 
                {
                    start_day: $(this).find('#start_day').val(),
                    end_day: $(this).find('#end_day').val(),
                    units: unitIds,
                    limit: 0
                };

            dataFilter = JSON.stringify(dataFilter);
            
            $.ajax({
                async: true,
                type: 'POST',
                url: 'widgets/routing.php',
                data: { url: 'reportkm/search/', data: JSON.parse(dataFilter) },
                dataType: 'json',
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportkm) != 'object'){ return false; }
                    loadingWidget('#'+varReportkm.widgetName);
                },
                success: function(data) {
                    //transformando linha em coluna
                    var newData = widgetsreportkm.lineToColumn(data);

                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportkm) != 'object'){ return false; }
                    varReportkm.dataFilterArr.push(dataFilter);
                    loadingWidget('#'+varReportkm.widgetName, true); 

                    varReportkm.widgetObj.find('.tab-content').append('<div class="col-md-12 tab-pane" id="tab-result-km-'+varReportkm.tabResults+'"></div>');
                    varReportkm.widgetObj.find('#widgetsreportkm-nav').append('<li class=""><a href="#tab-result-km-'+varReportkm.tabResults+'"  id="result-km-'+varReportkm.tabResults+'" data-toggle="tab">Resultado <i class="fa fa-times closeReportTab" id="'+varReportkm.tabResults+'"></i></a></li>');

                    varReportkm.widgetObj.find('#result-km-'+varReportkm.tabResults).die('click').live('click', function (){
                        var id = $(this).attr('id');
                        id = id.replace('result-km-', '');

                        var dropDownMenu = '<li><a href="javascript:;" onClick="widgetsreportkm.print(\'print\', '+id+')">Imprimir</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsreportkm.print(\'PDF\', '+id+')">Exportar PDF</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsreportkm.print(\'CSV\', '+id+')">Exportar CSV</a></li>';

                        varReportkm.widgetObj.find('#widgetsreportkm-dropdown').html(dropDownMenu);   
                        varReportkm.widgetObj.find('.dropdown-toggle').removeAttr("disabled");                 
                    }); 
                    var numRows = varReportkm.rowSizeMin;
                    var panelWidget = varReportkm.widgetObj.closest('.panel');
                    if(panelWidget.hasClass('full-Scr')){
                        numRows = Math.round(panelWidget.height() / varReportkm.rowSizeMax) - varReportkm.rowAdjust;
                    }
                    varReportkm.widgetObj.find('#tab-result-km-'+varReportkm.tabResults).html( '<table class="table table-striped table-bordered" data-page-length="'+numRows+'" id="table-list-result-km-'+varReportkm.tabResults+'"></table>' );
                    varReportkm.table.push(null);
                    varReportkm.table[varReportkm.tabResults] = varReportkm.widgetObj.find('#table-list-result-km-'+varReportkm.tabResults).DataTable({
                        "data": newData,
                        paginate: true,
                        bLengthChange: false,
                        bFilter: false,
                        bInfo: true,
                        "ordering": false,
                        "pagingType": "full",
                        "fnDrawCallback": function( oSettings ) {
                            varReportkm.widgetObj.find('.100_p').css('width', '100%');
                        },
                        "columns": 
                            [
                                { "title": "none", "data": "label", "className": "100_p" }
                            ],
                        "columnDefs": 
                            [
                                {
                                    "targets": [0],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        var content = ' <div class="dv-tr nowrap ellipsis" title="'+full.label+'" style="background: #AFB9BD; color: #000;"><b>'+full.label+'</b></div>';

                                        content += '<div class="dv-tr dv-tr-header" style="background: #C1CCD1;">';
                                        content += '    <div class="dv-td nowrap ellipsis" title="Domingo">Domingo</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="Segunda">Segunda</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="Terça">Terça</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="Quarta">Quarta</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="Quinta">Quinta</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="Sexta">Sexta</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="Sabado">Sabado</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="Total">Total</div>';
                                        content += '</div>';

                                        content += '<div class="dv-tr">';
                                        content += '    <div class="dv-td nowrap ellipsis" title="'+full.dom+'">'+full.dom+'</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="'+full.seg+'">'+full.seg+'</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="'+full.ter+'">'+full.ter+'</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="'+full.qua+'">'+full.qua+'</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="'+full.qui+'">'+full.qui+'</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="'+full.sex+'">'+full.sex+'</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="'+full.sab+'">'+full.sab+'</div>';
                                        content += '    <div class="dv-td nowrap ellipsis" title="'+full.kmTotal+'">'+full.kmTotal+'</div>';
                                        content += '</div>';
                                        
                                        return content;
                                    }
                                }
                            ],
                        "language" : {
                            "emptyTable":     "Ainda não há registros",
                            "info" : "Total de _TOTAL_ registros",
                            "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
                            "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
                            "infoPostFix":    "",
                            "thousands":      ".",
                            "lengthMenu":     "Mostre _MENU_ registros",
                            "loadingRecords": "Carregando...",
                            "processing":     "Processando...",
                            "search":         "",
                            "searchPlaceholder": "Procure por unidade...",
                            "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
                            "paginate": {
                                "first":      "<<",
                                "last":       ">>",
                                "next":       ">",
                                "previous":   "<"
                            }
                        },
                        "initComplete": function(settings) {
                            if( data.length > 0){
                                varReportkm.api.push(this.api());
                                varReportkm.widgetObj.find('.100_p').css('width', '100%');

                                dataFilter = JSON.parse(dataFilter);
                                dataFilter.limit += varReportkm.processingSearchLimit;
                                if( data.length >= varReportkm.processingSearchLimit ){
                                    widgetsreportkm.processingSearch(varReportkm.tabResults, dataFilter);
                                } 
                            }
                        }
                    });              
                    //reseta o form   
                    varReportkm.widgetObj.find('#result-km-'+varReportkm.tabResults).trigger('click');  
                    varReportkm.tabResults++;
                    //resetReportAction(); 
                },
                error: function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportkm) != 'object'){ return false; }
                    loadingWidget('#'+varReportkm.widgetName, true);
                    //Criando mensagem de retorno
                    createMsgBox('error', error.responseText, '#'+varReportkm.widgetName+' .panel-body'); 
                }
            });
            return false;
        }
    }); 

});


