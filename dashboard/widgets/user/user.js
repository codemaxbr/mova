var varUser = {};
	varUser.widgetName 	= 'widgetsuser';
	varUser.widgetObj 	= $('#'+varUser.widgetName);
	varUser.action 		= 'add';
	varUser.tree 		= varUser.widgetObj.find('.groupTree');
	varUser.client 		= varUser.widgetObj.find('.groupTree').jstree('get_json');

var widgetsUser = {	
	createJSTree: function (json, widget, tree, icons) {
		var three_state = true;
		var cascade = "";
		if(tree == '.permTree') {
			three_state = false;
			cascade = "down"
		}
		varUser.widgetObj.find(tree).jstree({
			"checkbox" : {
				"keep_selected_style" : false,
				"three_state": three_state,
				cascade: cascade
		    },
		    "core": {
		    	"data" : json,
		        "themes":{
		            "icons":icons
		        }
		    },
		    // Adicionando checkboxes para cada permissão
		    "plugins" : [ "checkbox"]
	    });
	},
	reset: function (){
		var form = varUser.widgetObj.find('form');

		varUser.action = 'add'; 

		form[0].reset();  
	    form.find('.permTree').jstree('uncheck_all');    
		form.find('.permTree').jstree('close_all');	
	    form.find('.groupTree').jstree('uncheck_all');    
		form.find('.groupTree').jstree('close_all');	

		form.find('#user_mobile').removeAttr('checked');
		form.find('#user_web').removeAttr('checked');
		
		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');
		//form.find('.accordion').accordion({ collapsible: true, active: false });
	},
	// combo de tipos de contas
	buildDay: function (data){
		var template = 	'{{#days}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/days}}';
		var select = Mustache.to_html(template, data);
		varUser.widgetObj.find('#day_start').append(select);
		varUser.widgetObj.find('#day_end').append(select);
	},
	buildTable: function (user) {
		//Cria a tabela dinamicamente
		varUser.widgetObj.find('#create-table').html('<table class="table table-striped table-bordered" id="table-list-user" width="100%"></table>');
		//Pega os dados da tabela
		var table_user = varUser.widgetObj.find('#table-list-user').dataTable({
		    ajax: {
		        "url": 'widgets/routing.php',
		        "type": "POST",
		        "data": { url: 'user/list' },
		  		error: function (error) { 
		        	loadingWidget('#'+varUser.widgetName, true); 
					createMsgBox('error', error.responseText, '#'+varUser.widgetName+' .panel-body');
		  		}
		      },
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "columns": 
		    	[	    	 	
		            { 
		            	"title": "ID",  
		            	"data": "id",
		            	"className": "10_p"
		            },
		            { 
		            	"title": "Nome", 
		            	"data": "name" ,
		            	"className": "45_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            },
		            { 
		            	"title": "Login", 
		            	"data": "login" ,
		            	"className": "45_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            }
		          ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por usuários...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
		    	//CLICANDO NA CONTA PARA EDITÁ-LA
		    	if(user.data.length > 0) {
                    varUser.widgetObj.find('.10_p').css('width', '10%');
                    varUser.widgetObj.find('.45_p').css('width', '45%');

			    	varUser.widgetObj.find('#table-list-user tbody').delegate('tr', 'click', function() {
			    		//Pegando id da conta
			    		var data = $(this).children('td').html();
			    	    loadingWidget('#'+varUser.widgetName);
			    	    $.ajax({
			    	    	async: true,
				        	type: 'POST',
			    	        url: 'widgets/routing.php',
			    	        data: { url: 'user/getUser/', 
			    	        data: JSON.parse(data) },
			    	        dataType: 'json',
			    	        success: function(data) {
			    		        widgetsUser.loadEditForm(data, varUser.widgetName);
			    	        	loadingWidget('#'+varUser.widgetName, true);
			    	        },
			    	        error: function(error) {
			    	        	createMsgBox('error', error.responseText, '#'+varUser.widgetName+' .panel-body');
			    	        	loadingWidget('#'+varUser.widgetName, true);
			    	        }
			    	    });
			    	    return false;
			    	});
		    	}	    	
		    }
		});
	},
	delete: function () {
	    var data = { id: varUser.widgetObj.find('form').find('input#id').val(), status: 3 };    
	    data = JSON.stringify(data);      
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'user/deleteuser',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varUser.widgetName);
			},
			success : function(data) {
				// cria mensagem de sucesso no widget
				createMsgBox('success', data, '#'+varUser.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varUser.widgetObj.find('#table-list-user').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varUser.widgetName);
				widgetsUser.reset();
				loadingWidget('#'+varUser.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varUser.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error', error.responseText, '#'+varUser.widgetName+' .panel-body');			
			}
		});
		return false;
	},
	checkRestr: function (index) {
		var form = index.parents('form');
		var checked = index.attr('checked');
		var input = index.attr('id');
		
		if(checked === 'checked') {
			var inputs = form.find('.'+input);
			inputs.attr('disabled', false);
			inputs.parent('div').fadeIn("slow");
		} else {
			var inputs = form.find('.'+input);
			inputs.val('');
			inputs.attr('disabled', true);
			inputs.parent('div').fadeOut("slow");
		}
	},
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	loadEditForm: function (data, widget) {
		var form = varUser.widgetObj.find('form');
		//limpa formulário
		widgetsUser.reset();
		// muda o form para edição
		varUser.action = 'edit';

		varUser.widgetObj.find('.btn-group button').attr("disabled", true);
		form.find('.permTree').jstree('uncheck_all');
		form.find('.groupTree').jstree('uncheck_all');

		//Checando se os valores não estão nulos
		if((data.user.hour_start != null) || (data.user.hour_end != null)) {
			form.find('#check_hour').attr('checked', true);
		} else {
			form.find('#check_hour').attr('checked', false);		
		}
		if(data.user.end_access != null) {
			form.find('#check_end_access').attr('checked', true);
			//Formatando saída de data para client-side
			var date = data.user.end_access.split('-');
			data.user.end_access = date[2]+'/'+date[1]+'/'+date[0];
		} else {
			form.find('#check_end_access').attr('checked', false);		
		}
		if((data.user.day_start != null) || (data.user.day_end !=null)) {
			form.find('#check_day').attr('checked', true);
		} else {
			form.find('#check_day').attr('checked', false);		
		}
		form.find('input[type="checkbox"]').each(function(index, value){
			widgetsUser.checkRestr($(this));
		});

		form.find('#user_mobile').removeAttr('checked');
		form.find('#user_web').removeAttr('checked');
		if(data.user.user_mobile == 1){
			form.find('#user_mobile').attr('checked', 'checked');
		}
		if(data.user.user_web == 1){
			form.find('#user_web').attr('checked', 'checked');
		}
		
		//Abrindo accordion de restricoes caso ele tenha alguma restrição
		if( (data.user.hour_start != null) || (data.user.hour_end != null) 
			|| (data.user.end_access != null) || (data.user.day_start != null) || (data.user.day_end !=null) ) {
			form.find('.accordion.rest').accordion({ active: 0 });
		} else {
			form.find('.accordion.rest').accordion({ active: 1 });
		}
		
		//Deixando aberto demais accordions(permissoes grupos)
		form.find('.accordion.perm').accordion({ active: 0 });
		form.find('.accordion.group').accordion({ active: 0 });
		form.find('.permTree').jstree('open_all');
		form.find('.groupTree').jstree('open_all');
		
		form.removeClass('hidden').addClass('animated fadeIn');
		
		form.find('input#name').focus();
		
		form.find('input#id').val(data.user.id);
		form.find('input#name').val(data.user.name);
		form.find('input#login').val(data.user.login);
		form.find('input#hour_start').val(data.user.hour_start);
		form.find('input#hour_end').val(data.user.hour_end);
		form.find('input#end_access').val(data.user.end_access);
		form.find('input#end_status').val(data.user.status);

		form.find('select#account option').each(function(index, value){
		      var value = $(this).attr('value');
		      if(value == data.user.account_id)
			      $(this).attr('selected', true);
		});

		form.find('select#day_start option').each(function(index, value){
		      var value = $(this).attr('value');
		      if(value == data.user.day_start)
			      $(this).attr('selected', true);
		});

		form.find('select#day_end option').each(function(index, value){
		      var value = $(this).attr('value');
		      if(value == data.user.day_end)
			      $(this).attr('selected', true);
		});
		
		form.find('.permTree li').each(function(index, value) {
				var check = $(this);
				var id = check.attr('id');
			$.each(data.perms, function(i, val) {
					if(id == val.widget_id) {
						form.find('.permTree').jstree('check_node', check);
						if(val.access_level == 2) {
							var write = $(check).find('li').attr('id');
							form.find('.permTree').jstree('check_node', write);					
						} else {
							var write = $(check).find('li').attr('id');
							form.find('.permTree').jstree('uncheck_node', write);		
						}
					}
			});
		});

		form.find('.groupTree li li').each(function(index, value) {
				var check = $(this);
				var id = check.attr('id');
			$.each(data.groups, function(i, val) {
				if(id == val.subgroup_id)
					form.find('.groupTree').jstree('check_node', check);
		      });
		});
		
		openForm(varUser.widgetName, varUser.action);
	}
};

function load_widgetsuser_Page(){
	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'user/index/'},
        dataType: 'json',
        beforeSend: function () {
        	closeForm(varUser.widgetName);
			widgetsUser.reset();	
			varUser.widgetObj.find('.btn').attr('disabled', 'disabled');
            varUser.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	widgetsUser.buildTable(JSON.parse(data.userList));
        	widgetsUser.buildDay(data.days);
        	widgetsUser.createJSTree(JSON.parse(data.widgetList), '#'+varUser.widgetName, '.permTree', true);
        	widgetsUser.createJSTree(JSON.parse(data.groupList), '#'+varUser.widgetName, '.groupTree', false);
        	//SET ID account user
        	varUser.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	//liberando menu
        	varUser.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varUser.widgetName, true); 

			//Carregando comportamento do widget
		    varUser.widgetObj.find('.clockpicker').clockpicker({
		        autoclose: true,
		        'default': 'now'
		    });
			
			//Ao marcar permissão de escrita, marca automaticamente leitura
			//Para isso parametro cascade do plugin jstree precisa estar com valor down
			varUser.widgetObj.find('.permTree').on('select_node.jstree', function (e, data) {
				var tree = this;
				if(data.node.children.length == 0) {
					var node = varUser.widgetObj.find('.permTree').jstree('get_node', data.node.parent);
						$(tree).jstree('check_node', node);
				}
			});
			
			varUser.widgetObj.find('form input[type="checkbox"]').on('click', function(){
				widgetsUser.checkRestr($(this));	
			});

			//Accordion
			$(function() {
				varUser.widgetObj.find('.accordion').accordion({
					active: false,
					header: 'h6',
					collapsible: true,
					heightStyle: "content"
				});
			});

			//Datepicker
			varUser.widgetObj.find('.pluginDate').datepicker({ 
				autoclose: true, 
				language: 'pt-BR',
				format: 'dd/mm/yyyy',
			});
        },
        error: function(error) {
        	loadingWidget('#'+varUser.widgetName, true);
            varUser.widgetObj.find('.btn-danger').removeAttr("disabled");
			createMsgBox('error', error.responseText, '#'+varUser.widgetName+' .panel-body');
        }
    });
}

$(document).ready( function () {
	load_widgetsuser_Page();

	varUser.widgetObj.find('.openForm').die("click").live("click", function(){
		widgetsUser.reset();
		varUser.action = 'add';
		openForm(varUser.widgetName, 'add');
	});
	//CHAMADA PARA EXCLUSÃO DE USUARIOS 
	varUser.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		openConfirm(varUser.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir Usuário", "Deseja remover o usuário selecionado?", "Sim", "Não");
	});
	varUser.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		widgetsUser.delete();
		closeConfirm(varUser.widgetName);
	});
	varUser.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varUser.widgetName);
	});
	//Resetando árvores ao clicar em cancelar/fechar
	varUser.widgetObj.find('.cancel').die('click').live('click', function() {
		closeForm(varUser.widgetName);
		widgetsUser.reset();		
	});
	// CRIANDO CONTA
    varUser.widgetObj.find('form').submit(function(e) { 
        e.preventDefault();
        if ($(this).parsley().isValid()) {
        	var form = this;
        	var selectedElmsIds = [];
        	var selectedElms = $(this).find('.permTree').jstree("get_selected", true);
        	if($(selectedElms).length > 0){
        		$.each(selectedElms, function() {
        			//Pegando permissão de escrita/write
        			var children = $(form).find('.permTree').jstree("get_node", this.children[0]);
        			children = children.state;
        			for(var key in children) {
        				var iWrite = children['selected'];
        			}
        			if(this.children.length !== 0) {
	        			if(isNaN(this.id) !== true) {
	        				if(iWrite === true) {
	    						selectedElmsIds.push({widget_id: this.id, access_level: 2});
	        				} else {
	        					selectedElmsIds.push({widget_id: this.id, access_level: 1});
	        				}
	        			}
        			}
    			});
        	} else {
    	        createMsgBox('error', 'Deve selecionar no mínimo 1 acesso.', '#'+varUser.widgetName+' .panel-body');
            	$(this).find('.permTree').addClass('permError');
            	$(this).find('#parsley-perm-tree-add').html("Campo Obrigatório");
            	return false;
        	}

        	var groupsObj = [];
        	var groups = $(form).find('.groupTree').jstree('get_selected', true);
        	if($(groups).length <= 0) {
    	        createMsgBox('error', 'Selecione ao menos 1 grupo.', '#'+varUser.widgetName+' .panel-body');
            	return false;
        	}
    		$.each(groups, function() {
    			if(this.children.length == 0) {
	    			var parent = $(form).find('.groupTree').jstree('get_node', this.parent);
	    			groupsObj.push({ subgroup: this.id, group: parent.id });
    			}
    		});
        	
        	//Verifica se está resetando password
        	if ($(this).find('#reset_password').is(":checked"))
        	{
        		var resetPass = 1;
        	} else {
        		var resetPass = 0;
        	}
        	
			var data = 
				{
					id: $(this).find('#id').val(),
					name: $(this).find('#name').val(),
					login: $(this).find('#login').val(),
					reset_password: resetPass,
					account_id: parseInt($(this).find('#account_id').val()),
					hour_start: $(this).find('#hour_start').val(),
					hour_end: $(this).find('#hour_end').val(),
					day_start: $(this).find('#day_start').val(),
					day_end: $(this).find('#day_end').val(),
					end_access: $(this).find('#end_access').val(),
					user_web: $(this).find('#user_web').attr('checked') == 'checked' ? 1 : 0,
					user_mobile: $(this).find('#user_mobile').attr('checked') == 'checked' ? 1 : 0,
					perms: selectedElmsIds,
					groups: groupsObj
				};
				
			data = JSON.stringify(data);
			
            $.ajax({
            	async: true,
    	        type: 'POST',
    	        url: 'widgets/routing.php',
                data: { url: 'user/'+varUser.action+'/', data: JSON.parse(data) },
    	        dataType: 'json',
    	        beforeSend: function () {
    	        	//Criando botão loading e excluindo buttons ('salvar' e 'cancelar')
    	        	loadingWidget('#'+varUser.widgetName);
    	        },
    	        success: function(data) {
    	        	loadingWidget('#'+varUser.widgetName, true);       	        
	    	        //Criando mensagem de retorno
	    	        createMsgBox('success', data, '#'+varUser.widgetName+' .panel-body');
	    	        // atualiza tabela que lista as contas
	    	        varUser.widgetObj.find('#table-list-user').DataTable().ajax.reload(null, false);	    	            	
	    	        //reseta o form	    
					closeForm(varUser.widgetName);
	    	        widgetsUser.reset(); 
    	        },
    	        error: function(error) {
    	        	loadingWidget('#'+varUser.widgetName, true);
    	        	//Criando mensagem de retorno
    	        	createMsgBox('error', error.responseText, '#'+varUser.widgetName+' .panel-body');
	            }
    	    });
    		return false;
        }
    });    
});
