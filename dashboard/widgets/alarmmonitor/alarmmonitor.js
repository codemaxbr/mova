var varAlarmmonitor = {};
	varAlarmmonitor.widgetName 	= 'widgetsalarmmonitor';
	varAlarmmonitor.widgetObj 	= $('#'+varAlarmmonitor.widgetName);
	varAlarmmonitor.callMap;
	varAlarmmonitor.id
	varAlarmmonitor.accessLevel 	= 1;
	varAlarmmonitor.table;
	varAlarmmonitor.api;
	varAlarmmonitor.data;
	varAlarmmonitor.reload;
	varAlarmmonitor.sound;
	varAlarmmonitor.reloadSound
	varAlarmmonitor.playSound = true;
	varAlarmmonitor.dataFilter = JSON.stringify({ group_id: null, level: null });

var widgetsalarmmonitor = {
	buildGroup: function (data){
    	if(typeof(varAlarmmonitor) != 'object'){return false;}
		varAlarmmonitor.widgetObj.find('#group').html('<option value="0">Todos</option>');
		var template = 	'{{#groupinfo}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/groupinfo}}';
		var select = Mustache.to_html(template, data);
		varAlarmmonitor.widgetObj.find('#group').append(select);
	},
	reset: function (){
    	if(typeof(varAlarmmonitor) != 'object'){return false;}
		var form = varAlarmmonitor.widgetObj.find('form');

		form[0].reset();  
		form.find('#group').prop('selectedIndex', 0);
		form.find('#level').val('0');
		varAlarmmonitor.widgetObj.find('.btn-level').removeClass('selectdLevel');	
		
		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');
	},
	loadDataAlarmmonitor : function () {
		$.ajax({
	        async: true,
	        type: 'POST',
	        url: 'widgets/routing.php',
	        data: { 
	        	url: 'alarm/listViolation/', 
	        	data: JSON.parse(varAlarmmonitor.dataFilter) 
	        },
	        dataType: 'json',
	        cache: false,
	        success: function(data) {
        		if(typeof(varAlarmmonitor) != 'object'){return false;}

        		var soundON = false;
        		varAlarmmonitor.data = data.data;
				$.each(varAlarmmonitor.data, function( key, value ) {
					if(value.user_view == 0){
						soundON = true;
					}
				});
        		if(soundON){
		 			widgetsalarmmonitor.playSound();
		 		}else{
		 			clearTimeout(varAlarmmonitor.reloadSound);
		 		}

        		if(varAlarmmonitor.widgetObj.find('#table-list-alarmmonitor').length == 0){
	            	widgetsalarmmonitor.buildTable(varAlarmmonitor.data);
        		}else{
        			widgetsalarmmonitor.reloadTable(varAlarmmonitor.data);
        		}
        		clearTimeout(varAlarmmonitor.reload);
	            varAlarmmonitor.reload = window.setTimeout(function() {
	                widgetsalarmmonitor.loadDataAlarmmonitor();
	            }, varGlobal.timeReload);
	        },
	        error: function(error) {
	            //cria mensagem de erro no widget
				createMsgBox('error', error.responseText, '#'+varAlarmmonitor.widgetName+' .panel-body'); 
	        }
	    });
	},
    reloadTable: function (data){
		//removendo rows
		if(typeof(varAlarmmonitor.table) == 'object'){
        	varAlarmmonitor.table.clear();
        }
		$(data).each(function(i, row) {
            varAlarmmonitor.table.row.add({
        		'alarm_id': row.alarm_id,
        		'alarm_name': row.alarm_name,
        		'description': row.description,
        		'email_notification': row.email_notification,
        		'level': row.level,
        		'group_id': row.group_id,
        		'group_name': row.group_name,
        		'label': row.label,
        		'violation_id': row.violation_id,
        		'initial_time': row.initial_time,
        		'user_view': row.user_view,
        		'hist_id': row.hist_id,
        		'data': row.data,
        		'full_date': row.full_date,
        		'hora': row.hora,
        		'lat': row.lat,
        		'lng': row.lng,
        		'full_address': row.full_address
        	});
        });  
        varAlarmmonitor.table.draw(false);              
    },
    openMap: function (lat, lng, drawMark){
        if(typeof(varMapBox) != 'object'){
            handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 8);

            clearTimeout(widgetsalarmmonitor.callMap);
            widgetsalarmmonitor.callMap = window.setTimeout(function() {
                widgetsalarmmonitor.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }
        if(typeof(varMapBox.mapInstance) != 'object'){
            clearTimeout(widgetsalarmmonitor.callMap);
            widgetsalarmmonitor.callMap = window.setTimeout(function() {
                widgetsalarmmonitor.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }

        MapBox.centerUnit(lat, lng, false, null, drawMark);
    },
    delete : function (row){
		var data = { id: varAlarmmonitor.id };    
	    data = JSON.stringify(data); 
		$.ajax({
	        async: true,
	        type: 'POST',
	        url: 'widgets/routing.php',
			data : {
				url : 'alarm/deleteAlarmViolated/',
				data : JSON.parse(data)
			},
	        dataType: 'json',
	        cache: false,
			beforeSend : function() {
				loadingWidget('#'+varAlarmmonitor.widgetName);
			},
	        success: function(data) {
    			if(typeof(varAlarmmonitor) != 'object'){return false;}
				loadingWidget('#'+varAlarmmonitor.widgetName, true);
	            createMsgBox('success', data.responseText, '#'+varAlarmmonitor.widgetName+' .panel-body'); 
    			widgetsalarmmonitor.loadDataAlarmmonitor();
	        },
	        error: function(error) {
    			if(typeof(varAlarmmonitor) != 'object'){return false;}
				loadingWidget('#'+varAlarmmonitor.widgetName, true);
	            //cria mensagem de erro no widget
	            createMsgBox('error', error.responseText, '#'+varAlarmmonitor.widgetName+' .panel-body'); 
	        }
	    });
    },
    playSound : function () {
    	if(typeof(varAlarmmonitor) != 'object'){return false;}
	 	clearTimeout(varAlarmmonitor.reloadSound);
	    varAlarmmonitor.reloadSound = window.setTimeout(function() {
	        if(varAlarmmonitor.playSound){
	        	varAlarmmonitor.sound.play();
	        	widgetsalarmmonitor.playSound();
	        }
	    }, 500);
    },
	buildTable: function (alarm){
    	if(typeof(varAlarmmonitor) != 'object'){return false;}

		var pageSize = $(window).height();
		var numRows = Math.round(pageSize / 73);
		numRows -= 1;

		varAlarmmonitor.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" data-page-length="'+numRows+'" id="table-list-alarmmonitor" width="100%"></table>' );
		//Pega os dados da tabela
		varAlarmmonitor.table = varAlarmmonitor.widgetObj.find('#table-list-alarmmonitor').DataTable({
		    "data": alarm,
		    paginate: true,
		    bLengthChange: false,
		    bFilter: false,
		    bInfo: true,
		    "pagingType": "full",
		    "fnDrawCallback": function( oSettings ) {
                varAlarmmonitor.widgetObj.find('.45_p').css('width', '45px');
                if(!varAlarmmonitor.widgetObj.find('#table-list-alarmmonitor tbody tr').hasClass('trInfo')){
	    			varAlarmmonitor.widgetObj.find('#table-list-alarmmonitor tbody tr').addClass('trInfo');
                }

	    		varAlarmmonitor.widgetObj.find('#table-list-alarmmonitor tbody tr td > div.white').css('color', '#ffffff');
	    		varAlarmmonitor.widgetObj.find('#table-list-alarmmonitor tbody tr td > div.level_1').parent().addClass('bg-color-on');
	    		varAlarmmonitor.widgetObj.find('#table-list-alarmmonitor tbody tr td > div.level_2').parent().addClass('bg-color-stat');
	    		varAlarmmonitor.widgetObj.find('#table-list-alarmmonitor tbody tr td > div.level_3').parent().addClass('bg-color-off');
		    },
		    "columns": 
		    	[
		            { "title": "#", "data": "violation_id", "class": 'hidden' },
		            { "title": "Grupo", "data": "group_name", "class": 'hidden'},
		            { "title": "Alarme", "data": "alarm_name", "className": "45_p"},
		            { "title": "Alarme", "data": "alarm_name"}
	          	],
          	"columnDefs": 
                [
                    {
                        "targets": [2],
		            	"render": function ( data, type, full, meta ) {
		            		var styleColor;
		            		switch(full.level) {
							    case 1:
									styleColor = ' color-on ';
							        break;
							    case 2:
									styleColor = ' color-stat ';
							        break;
							    case 3:
									styleColor = ' color-off ';
							        break;
							}
							var content;
							content  = '<div class="col-md-12 col-sm-12 m-b-12 alarmRow open-trInfo level_'+full.level+'" id="'+full.violation_id+'">';
							content += '	<span class="fa-stack fa-lg">';
		                    content += '		<i class="fa fa-circle fa-stack-2x"></i>';
		                    content += '		<i class="fa fa-warning fa-stack-1x '+styleColor+'"></i>';
		                    content += '	</span>';
							content += '</div>'

							return content;
						}
                    },
                    {
                        "targets": [3],
		            	"render": function ( data, type, full, meta ) {
		            		var styleColor;
		            		switch(full.level) {
							    case 1:
									styleColor = ' color-on ';
							        break;
							    case 2:
									styleColor = ' color-stat ';
							        break;
							    case 3:
									styleColor = ' color-off ';
							        break;
							}
							var levelCss = ' level_'+full.level+' ';
							var levelCheck = ' alarm-checked ';
							if(full.user_view == 1){
								levelCss = '';
								levelCheck = ' alarm-btn-disabled ';
							}

							var content;
							content  = '<div class="col-md-12 col-sm-12 m-b-12 alarmRow '+levelCss+'" >';
							content += '	<div class="col-md-12 col-sm-12 m-b-12 open-trInfo" id="'+full.violation_id+'">';
		                    content += '		<div class="nowrap ellipsis text-left" title="' + data + '"><span class="alarm-title">' + full.label + ' - ' + data + '</span><br>' + full.group_name + '</div>';
		                    content += '	</div>';
							content += '	<div class="col-md-12 col-sm-12 m-b-12">';
							content += '		<div class="col-md-8 col-sm-8 m-b-8 text-left open-trInfo" id="'+full.violation_id+'">';
		                    content += '			<div class="nowrap ellipsis text-left" title="' + full.data + ' às ' + full.hora + '">' + full.data + ' às ' + full.hora + '</div>'
		                    content += '		</div>';
							content += '		<div class="col-md-4 col-sm-4 m-b-4 text-center">';
		                    content += '			<i class="btn-icon fa fa-globe fa-1x alarm-icon alarm-map" title="Ver no mapa" level="'+full.level+'" lat="'+full.lat+'" lng="'+full.lng+'"></i>';
		                    content += '			<i class="btn-icon fa fa-check fa-1x alarm-icon '+levelCheck+'" title="Marcar como lida" id="'+full.violation_id+'"></i>';
		                    content += '			<i class="btn-icon fa fa-trash fa-1x alarm-icon confirm_delete title="Remover Violação de Alarme" id="'+full.violation_id+'"></i>';
		                    content += '		</div>';
		                    content += '	</div>';
							content += '</div>'

							content += '<div class="col-md-12 col-sm-12 m-b-12 alarmRow info info_'+full.violation_id+'" >';
							content += '	<div class="col-md-12 col-sm-12 m-b-12">';
							content += '		<div class="nowrap ellipsis">Local: </div>';
		                    if(full.full_address != '') {
	                            //se sim ja retorna o endereco
	                            content += '	<div class="nowrap ellipsis" title="'+full.full_address+'">' + full.full_address + '</div>';
	                        } else {
	                            //se nao faz o reversegeo para capturar o endereco e salvar no banco
	                            content += "	<div class='nowrap ellipsis mapAddress' hist='"+full.hist_id+"' long='"+full.lng+"' lat='"+full.lat+"'>Carregando...</div>";
	                        }
		                    content += '	</div>';
							content += '	<div class="col-md-12 col-sm-12 m-b-12">';
							content += '		<div class="nowrap ellipsis">Descrição: </div>';
							content += '		<div class="nowrap ellipsis">'+full.description+'</div>';
		                    content += '	</div>';
							content += '</div>'

							return content;
						}
                    }
                ],
		    "language" : {
		    	"emptyTable":     "Nenhum alarme no momento",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por Alarmes...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings) {
    			if(typeof(varAlarmmonitor) != 'object'){return false;}
    			varAlarmmonitor.api = this.api();
    			varAlarmmonitor.widgetObj.find('#table-list-alarmmonitor').find('thead').css('display', 'none');

		    	//CLICANDO NA CONTA PARA EDITÃ�-LA
		    	if(alarm.length > 0){
		    		varAlarmmonitor.widgetObj.find('.open-trInfo').die("click").live("click", function(){
			    		//Pegando id
	    				var infoName = 'info_' + $(this).attr('id');
						var parent = $(this).parent();
						while(!parent.hasClass('trInfo')){
							parent = parent.parent();
						}
						console.log(parent);
						varAlarmmonitor.widgetObj.find('.info.open').toggle( function() {
					    	varAlarmmonitor.widgetObj.find('.info.open').removeClass('open');
					  	});
						if(parent.hasClass('test-open')){
							parent.removeClass("test-open");
						}else{
							varAlarmmonitor.widgetObj.find('#table-list-alarmmonitor tbody tr').removeClass("test-open");
							parent.addClass("test-open");

							varAlarmmonitor.widgetObj.find('.'+infoName).toggle( function() {
						    	varAlarmmonitor.widgetObj.find('.'+infoName).addClass('open');
						    	if( varAlarmmonitor.widgetObj.find('.'+infoName+' .mapAddress').length == 1 ){
									getAddressByCoord(varAlarmmonitor.widgetObj.find('.'+infoName+' .mapAddress'));
								}
						  	});
						}
	    		    	return false;
	    		    });
				}  	
		  	}
		});
	}
};

function load_widgetsalarmmonitor_Page(){
	if(typeof(varAlarmmonitor) != 'object'){return false;}
	varAlarmmonitor.accessLevel = handleAccessLevel(varAlarmmonitor.widgetName);
	if(varAlarmmonitor.accessLevel == 0){
		varAlarmmonitor.widgetObj.find('.openForm').addClass('hidden');
	}

	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'alarm/indexViolation/'},
        dataType: 'json',
        beforeSend: function () {
    		if(typeof(varAlarmmonitor) != 'object'){return false;}
        	closeForm(varAlarmmonitor.widgetName);
			widgetsalarmmonitor.reset();	
			varAlarmmonitor.widgetObj.find('.btn').attr('disabled', 'disabled');
            varAlarmmonitor.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	if(typeof(varAlarmmonitor) != 'object'){return false;}
        	widgetsalarmmonitor.loadDataAlarmmonitor();        	
        	widgetsalarmmonitor.buildGroup(JSON.parse(data.groupinfo));

        	//SET ID account user
        	varAlarmmonitor.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	//liberando menu
        	varAlarmmonitor.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varAlarmmonitor.widgetName, true); 
        },
        error: function(error) {
    		if(typeof(varAlarmmonitor) != 'object'){return false;}
        	loadingWidget('#'+varAlarmmonitor.widgetName, true);
			createMsgBox('error', error.responseText, '#'+varAlarmmonitor.widgetName+' .panel-body');
        },
        complete: function() {
    		if(typeof(varAlarmmonitor) != 'object'){return false;}
            varAlarmmonitor.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
	varAlarmmonitor.sound = varAlarmmonitor.widgetObj.find('#audio').get(0);
	load_widgetsalarmmonitor_Page();

 	varAlarmmonitor.widgetObj.find('.play').die("click").live("click", function(){ 		
 		if(varAlarmmonitor.playSound){
 			clearTimeout(varAlarmmonitor.reloadSound);
 			varAlarmmonitor.playSound = false;
 		}else{
 			varAlarmmonitor.playSound = true;
 			widgetsalarmmonitor.playSound();
 		} 		
 	});

    if(typeof(varAlarmmonitor) != 'object'){return false;}
			
	varAlarmmonitor.widgetObj.find('.openForm').die("click").live("click", function(){
		widgetsalarmmonitor.reset();
		varAlarmmonitor.action = 'add';
		openForm(varAlarmmonitor.widgetName, 'add');
	});

	varAlarmmonitor.widgetObj.find('form').find('.cancel').die('click').live('click', function() {
		closeForm(varAlarmmonitor.widgetName);
		widgetsalarmmonitor.reset();		
	});	

	varAlarmmonitor.widgetObj.find('.visualize-box').find('.cancel').die('click').live('click', function() {
		closeVisualize(varAlarmmonitor.widgetName);
	});	

	varAlarmmonitor.widgetObj.find('.btn-level').die("click").live("click", function(){	
		varAlarmmonitor.widgetObj.find('.btn-level').removeClass('selectdLevel');
		$(this).addClass('selectdLevel');		
		varAlarmmonitor.widgetObj.find('#level').val($(this).attr('id'));		
	});

	varAlarmmonitor.widgetObj.find('.alarm-map').die("click").live("click", function(e){
		e.preventDefault();	
		var lat = $(this).attr('lat');		
		var lng = $(this).attr('lng');		
		var level = $(this).attr('level');		
		var drawMark = [];

        drawMark.size = varGlobal.marker.Size;
        drawMark.symbol = varGlobal.marker.carSymbol;
		switch(level) {
		    case '1':
       			drawMark.color = varGlobal.colorOn;
		        break;
		    case '2':
       			drawMark.color = varGlobal.colorStat;
		        break;
		    case '3':
       			drawMark.color = varGlobal.colorOff;
		        break;
		}

        widgetsalarmmonitor.openMap(lat, lng, drawMark);
	});

	varAlarmmonitor.widgetObj.find('.alarm-checked').die("click").live("click", function(){	
		var row = $(this);
		var parent = $(this).parent().parent().parent();
		var parentTd = $(this).parent().parent().parent().parent();
		var id = $(this).attr('id');
		var data = { id: id };    
	    data = JSON.stringify(data); 
		$.ajax({
	        async: true,
	        type: 'POST',
	        url: 'widgets/routing.php',
			data : {
				url : 'alarm/setViewUser/',
				data : JSON.parse(data)
			},
	        dataType: 'json',
	        cache: false,
			beforeSend : function() {
				//loadingWidget('#'+varAlarmmonitor.widgetName);
				row.removeClass('fa-check');
				row.addClass('fa-spinner fa-spin');
			},
	        success: function(data) {
    			if(typeof(varAlarmmonitor) != 'object'){return false;}

				row.addClass('alarm-btn-disabled');
				row.removeClass('alarm-checked');

				parent.removeClass('level_1');	
				parent.removeClass('level_2');	
				parent.removeClass('level_3');
				parentTd.removeClass('bg-color-on');	
				parentTd.removeClass('bg-color-stat');	
				parentTd.removeClass('bg-color-off'); 

				//loadingWidget('#'+varAlarmmonitor.widgetName, true);
				row.removeClass('fa-spinner fa-spin');
				row.addClass('fa-check');
	        },
	        error: function(error) {
				//loadingWidget('#'+varAlarmmonitor.widgetName, true);
				row.removeClass('fa-spinner fa-spin');
				row.addClass('fa-check');
	            //cria mensagem de erro no widget
	            createMsgBox('error', error.responseText, '#'+varAlarmmonitor.widgetName+' .panel-body'); 
	        }
	    });
	});

	varAlarmmonitor.widgetObj.find('.btn-level').die("click").live("click", function(){
		if($(this).hasClass('selectdLevel')){
			$(this).removeClass('selectdLevel')
		}else{
			$(this).addClass('selectdLevel')
		}
	});

	//CHAMADA PARA EXCLUSÃO DE pois 
	varAlarmmonitor.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		varAlarmmonitor.id = $(this).attr('id');
		openConfirm(varAlarmmonitor.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir Violação de Alarme", "Deseja remover a violação selecionado?", "Sim", "Não");
	});
	varAlarmmonitor.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		widgetsalarmmonitor.delete();
		closeConfirm(varAlarmmonitor.widgetName);
	});
	varAlarmmonitor.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varAlarmmonitor.widgetName);
	});

	varAlarmmonitor.widgetObj.find('form').submit(function(e) { 
		e.preventDefault();
		var form = $(this);    
	    if ( form.parsley().isValid() ) {   

			loadingWidget('#'+varAlarmmonitor.widgetName);
			
			if(form.find('.btn-success.btn-level').hasClass('selectdLevel')){			
				form.find("#level").val('1');
			}
			if(form.find('.btn-warning.btn-level').hasClass('selectdLevel')){		
				if(form.find("#level").val() == 0){
					form.find("#level").val('2');
				}else{
					form.find("#level").val('1,2');
				}	
			}
			if(form.find('.btn-danger.btn-level').hasClass('selectdLevel')){			
				if(form.find("#level").val() == 0){
					form.find("#level").val('3');
				}else{
					form.find("#level").val(form.find("#level").val()+',3');
				}
			}

		    var data = 
				{
					group_id: form.find("#group").val(),
					level: form.find("#level").val()
				};

		    varAlarmmonitor.dataFilter = JSON.stringify(data);		    
	        //atualizando alarmes
	        widgetsalarmmonitor.loadDataAlarmmonitor();

	        window.setTimeout( function() {
		        //reseta o form	    
				loadingWidget('#'+varAlarmmonitor.widgetName, true);
				closeForm(varAlarmmonitor.widgetName);
		        widgetsalarmmonitor.reset();
		    }, 2000);

			return false;
	    }
	});
});



