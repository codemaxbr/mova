var varAlarm = {};
	varAlarm.widgetName = 'widgetsalarm';
	varAlarm.widgetObj = $('#'+varAlarm.widgetName);
	varAlarm.action = 'add';
	varAlarm.accessLevel = 1;

	varAlarm.parameter;
	varAlarm.parameterType = [];
	varAlarm.parameterInput = [];
	varAlarm.parameterInputPlaceholder	= [];
	varAlarm.parameterOption = [];
	varAlarm.parameterCerca;
	varAlarm.parameterEvent;
	varAlarm.typePar = [];

	//VARIAVEIS PARA O COMPONENTE DE ASSOCIAÇÃO DE UNIDADES
	//tabelas do componente de associação de unidades
	varAlarm.tableUnit;
	varAlarm.tableGroup;
	varAlarm.tableUnitAssoc;
	varAlarm.tableUnitAssocCad;
	//listas do componente de associação de unidades
	varAlarm.Groups = [];
	varAlarm.Units = [];
	varAlarm.UnitsAssocOri = [];
	varAlarm.UnitsAssoc = [];
	//lista do componente de associação de unidades
	varAlarm.UnitsSelected = [];
	//FIM DA VARIAVEIS DE ASOCIÇÃO

var widgetsalarm = {
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	loadEditForm: function (dataAlarm) {
		//verifica se o widget ainda está aberto
    	if(typeof(varAlarm) != 'object'){return false;}
    	var data = dataAlarm[0];
    	var alarmUnitAssoc = JSON.parse(dataAlarm[1]);

    	if(varAlarm.accessLevel == 1){
			var divContent = varAlarm.widgetObj.find('.visualize-box');
			var content;

			divContent.find('.list-alarm-view').html('<label>Disparar alarme quando</label><br>');
			$.each(data, function(index, val) {
				divContent.find('.lbl_name').html(val.name);
				divContent.find('.lbl_group').html(val.group_name);

				var operator;
				if(val.operators.indexOf(',') >= 0){
					ArrOperators = val.operators.split(',');
					ArrOperatorsName = val.operators_name.split(',');
					operator = ArrOperatorsName[ArrOperators.indexOf(val.operator)];
				}else{
					operator = val.operators_name;
				}

				if(val.parameter_id == 14){
					content = '<div class="col-md-12 col-sm-12 m-b-12 visualize-content" style="margin-top: 7px;">'+val.parameter_name + ' <span class="operador">' + operator + '</span> ' + val.cerca_name +'</div>';	
				}else if(val.parameter_id == 15){
					content = '<div class="col-md-12 col-sm-12 m-b-12 visualize-content" style="margin-top: 7px;">'+val.parameter_name + ' <span class="operador">' + operator + '</span> ' + val.event_name +'</div>';	
				}else{
					if(val.input == 'text'){
						content = '<div class="col-md-12 col-sm-12 m-b-12 visualize-content" style="margin-top: 7px;">'+val.parameter_name + ' <span class="operador">' + operator + '</span> ' + val.value +'</div>';							
					}else{
						ArrOptions = val.option_name.split(',');
						var value = ArrOptions[val.value];
						content = '<div class="col-md-12 col-sm-12 m-b-12 visualize-content" style="margin-top: 7px;">'+val.parameter_name + ' <span class="operador">' + operator + '</span> ' + value +'</div>';	
					}				
				}
				divContent.find('.list-alarm-view').append(content);

				if(val.level == 1){
					divContent.find('.lbl_level').html('<button type="button" class="p-10 btn btn-success m-r-2" id="1">Baixo</button>');
				}else if(val.level == 2){
					divContent.find('.lbl_level').html('<button type="button" class="p-10 btn btn-warning m-r-2" id="2">Médio</button>');
				}else{
					divContent.find('.lbl_level').html('<button type="button" class="p-10 btn btn-danger m-r-2" id="3">Alto</button>');
				}

				divContent.find('.lbl_description').html(val.description);

				divContent.find('.lbl_note_alarme').html('<button type="button" class="p-10 btn btn-danger m-r-2" id="3">Não</button>');
				divContent.find('.lbl_send_mail').html('<button type="button" class="p-10 btn btn-danger m-r-2" id="3">Não</button>');
				divContent.find('.field-email-view').css('display', 'none');
				if(val.notif_monitor){ 
					divContent.find('.lbl_note_alarme').html('<button type="button" class="p-10 btn btn-success m-r-2" id="1">Sim</button>');
				}
				if(val.notif_email){ 
					divContent.find('.lbl_send_mail').html('<button type="button" class="p-10 btn btn-success m-r-2" id="1">Sim</button>');
					divContent.find('.lbl_time_send_mail').html(minutesToHMS(val.time_send_mail * 60));
					divContent.find('.lbl_email_notification').html(val.email_notification);
					divContent.find('.field-email-view').css('display', 'block');
				}
	    	});

			widgetsalarm.buildTableUnitAssoc(alarmUnitAssoc.data, varAlarm.widgetObj.find('#create-table-alarm-unit-view'));

			openVisualize(varAlarm.widgetName);
			return false;
		}

		var form = varAlarm.widgetObj.find('form');
		//limpa formulário
		widgetsalarm.reset();
		varAlarm.widgetObj.find('.list-alarm').html('<label>Disparar alarme quando</label><br>');
		// muda o form para edição
		varAlarm.action = 'edit';

		var alarm_parameter;
		$.each(data, function(index, val) {
			form.find('#id').val(val.id);
			form.find('#name').val(val.name);
			form.find('#description').val(val.description);
			form.find('#email_notification').val(val.email_notification);
			varAlarm.widgetObj.find('.field-email').addClass('hidden');

			form.find('#level').val(val.level);
			form.find('.btn-level').removeClass('selectdLevel');	
			form.find('[id='+val.level+']').addClass('selectdLevel');		

			form.find('#time_send_mail option').each(function(i, obj) {
				var value = $(this).attr('value');
				if (value == val.time_send_mail) {
					$(this).prop('selected', true);
				}
			});		

			form.find('#group option').each(function(i, obj) {
				var value = $(this).attr('value');
				if (value == val.group_id) {
					$(this).prop('selected', true);
				}
			});

			form.find("#sendEmail").removeAttr('checked');
			form.find("#showInMonitor").removeAttr('checked');
			if(val.notif_email){ 
				form.find("#sendEmail").attr('checked', 'checked'); 
				varAlarm.widgetObj.find('.field-email').removeClass('hidden');
			}
			if(val.notif_monitor){ 
				form.find("#showInMonitor").attr('checked', 'checked'); 
			}

			widgetsalarm.handleAddAlarmParameter();

			alarm_parameter = form.find('.alarm_parameter');
			if(alarm_parameter.length == 1){
				alarm_parameter.val(val.parameter_id);
			}else{				
				alarm_parameter = $(alarm_parameter[index]);
				alarm_parameter.val(val.parameter_id);
			}
			//CARREGANDO COMBOS DINAMICAMENTE
			widgetsalarm.handleLoadAlarmParameter(alarm_parameter);

			alarm_parameter_type = form.find('.alarm_parameter_type');
			alarm_option = form.find('.alarm_option');
			if(alarm_parameter_type.length == 1){
				alarm_parameter_type.val(val.operator);
				alarm_option.val(val.value);
			}else{				
				alarm_parameter_type = $(alarm_parameter_type[index]);
				alarm_parameter_type.val(val.operator);
				alarm_option = $(alarm_option[index]);
				alarm_option.val(val.value);
			}
		}); 

		varAlarm.UnitsAssocOri = [];
		$.each(alarmUnitAssoc.data, function(index, val) {
			varAlarm.UnitsAssocOri.push({
				id: val.id,
				name: val.name,
				description: val.description,
				group_id: val.group_id
			});
		});
		widgetsalarm.buildTableUnitAssoc(alarmUnitAssoc.data, varAlarm.widgetObj.find('#create-table-alarm-unit'));

		openForm(varAlarm.widgetName, varAlarm.action);
	},
	buildGroup: function (data){
    	if(typeof(varAlarm) != 'object'){return false;}
		varAlarm.widgetObj.find('#group').html('<option value="">Selecione um grupo</option>');
		var template = 	'{{#groupinfo}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/groupinfo}}';
		var select = Mustache.to_html(template, data);
		varAlarm.widgetObj.find('#group').append(select);
	},
	reset: function (){
    	if(typeof(varAlarm) != 'object'){return false;}
		var form = varAlarm.widgetObj.find('form');

		varAlarm.action = 'add'; 

		form[0].reset();  
		form.find('#group').prop('selectedIndex', 0);
		form.find('#description').val('');
		form.find('#email_notification').val('');
		form.find('#level').val(1);
		varAlarm.widgetObj.find('.btn-level').removeClass('selectdLevel');
		varAlarm.widgetObj.find('.default-lvl').addClass('selectdLevel');
		varAlarm.widgetObj.find('.list-alarm').html('<label>Disparar alarme quando</label><br>');

		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');
		
		widgetsalarm.handleAddAlarmParameter();		
	},
	buildParameterList: function (parameter){
		varAlarm.parameter = '<select class="form-control input-sm alarm_parameter" required="required">';
		varAlarm.parameter += '	<option value="">Selecione</option>';
		varAlarm.parameterType.push(null);
		varAlarm.parameterInput.push(null);
		varAlarm.parameterInputPlaceholder.push(null);
		varAlarm.parameterOption.push(null);

		$.each(parameter.data, function(index, val) {
			varAlarm.parameter += '	<option value="'+val.parameter_id+'">'+val.parameter_name+'</option>';
			varAlarm.parameterInput.push(val.input);
			varAlarm.parameterInputPlaceholder.push(val.placeholder);

			var operators = val.operators.split(',');
			var operatorsName = val.operators_name.split(',');
			var c = '<select class="form-control input-sm alarm_parameter_type" required="required">';
			$.each(operators, function(index, o) {
				c += '	<option value="'+o+'">'+operatorsName[index]+'</option>';
			});
			c += '</select>';
			varAlarm.parameterType.push(c);

			var optionID = val.option_id.split(',');
			var optionName = val.option_name.split(',');
			var cp = '<select class="form-control input-sm alarm_option" required="required">';
			$.each(optionID, function(index, op) {
				cp += '	<option value="'+op+'">'+optionName[index]+'</option>';
			});
			cp += '</select>';
			varAlarm.parameterOption.push(cp);

		}); 
		varAlarm.parameter += '</select>';
	},
	buildCercaList: function (cerca){
		varAlarm.parameterCerca = '<select class="form-control input-sm alarm_option" required="required">';
		$.each(cerca.data, function(index, val) {
			varAlarm.parameterCerca += '	<option value="'+val.cerca_id+'">'+val.cerca_name+'</option>';
		}); 
		varAlarm.parameterCerca += '</select>';
	},
	buildEventList: function (event){
		varAlarm.parameterEvent = '<select class="form-control input-sm alarm_option" required="required">';
		$.each(event.data, function(index, val) {
			varAlarm.parameterEvent += '	<option value="'+val.event_id+'">'+val.event_name+'</option>';
		}); 
		varAlarm.parameterEvent += '</select>';
	},
	delete: function () {
    	if(typeof(varAlarm) != 'object'){return false;}
	    var data = { id: varAlarm.widgetObj.find('form').find("#id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'alarm/delete',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varAlarm.widgetName);
			},
			success : function(data) {
    			if(typeof(varAlarm) != 'object'){return false;}
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varAlarm.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varAlarm.widgetObj.find('#table-list-alarm').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varAlarm.widgetName);
				widgetsalarm.reset();				
				loadingWidget('#'+varAlarm.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varAlarm.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varAlarm.widgetName+' .panel-body');		
			}
		});
		return false;
	},
	handleAddAlarmParameter : function () {
		var basicContent = '<div class="col-md-12 col-sm-12 item-alarm">';
		basicContent += 	varAlarm.parameter;
		basicContent += '</div>';
		varAlarm.widgetObj.find('.list-alarm').append(basicContent);
	},
	handleRemoveAlarmParameter : function () {
		var obj = varAlarm.widgetObj.find('.item-alarm');
		if(obj.length > 1){
			var lastObj = obj[obj.length-1];
			var index = varAlarm.typePar.indexOf(15);
			if($(lastObj).find('.alarm_parameter').val() == 15){
				varAlarm.typePar.splice(index, 1);
			}
			obj[obj.length-1].remove();
		}
	},
	handleLoadAlarmParameter : function (obj){
		var parent = obj.parent();
		while(!parent.hasClass('item-alarm')){
			parent = parent.parent();
		}
		var id = obj.val();
		var basicContent; 

		if( (id == 15) && ($.inArray('15', varAlarm.typePar) >= 0) ){
			createMsgBox('error', 'Só pode haver um item do tipo Evento', '#'+varAlarm.widgetName+' .panel-body');
			id = '';
		}

		if(id == ''){
			basicContent  = '<div class="col-md-12 col-sm-12 item-alarm-selected">';
			basicContent += 	varAlarm.parameter;
			basicContent += '</div>';
		}else{
			clockpicker = '';
			if(varAlarm.parameterInputPlaceholder[id] == 'hour'){
				clockpicker = ' clockpicker ';
			}

			basicContent  = '<div class="col-md-4 col-sm-4 item-alarm-selected">';
			basicContent += 	varAlarm.parameter;
			basicContent += '</div>';
			basicContent += '<div class="col-md-4 col-sm-4 item-alarm-selected" style="padding-left: 5px !important;">';
			basicContent += 	varAlarm.parameterType[id];
			basicContent += '</div>';
			basicContent += '<div class="col-md-4 col-sm-4 item-alarm-selected '+clockpicker+'" style="padding-left: 5px !important;">';
			if(varAlarm.parameterInput[id] == 'text'){
				if(varAlarm.parameterInputPlaceholder[id] == 'hour'){
					basicContent += '	<input type="text" class="form-control input-sm alarm_option" required="required" value="00:00"/>';
				}else{
					basicContent += '	<input type="text" class="form-control input-sm alarm_option" required="required" placeholder="'+varAlarm.parameterInputPlaceholder[id]+'"/>';
				}
			}else if(varAlarm.parameterInput[id] == 'combo'){
				if(id == 14){
					basicContent += varAlarm.parameterCerca;
				}else if(id == 15){
					basicContent += varAlarm.parameterEvent;
				}else{
					basicContent += varAlarm.parameterOption[id];
				}
			}
			basicContent += '</div>';			
		}

		if(id == 15){
			basicContent = '<span class="informer-text">Só pode haver um item do tipo Evento.</span><br>' + basicContent;
		}
		parent.html(basicContent);
		parent.find('.alarm_parameter').val(id);
		if(id == 15){
			parent.addClass('event-item');	
		}else{
			parent.removeClass('event-item');	
		}

		varAlarm.typePar = [];
		varAlarm.widgetObj.find('.alarm_parameter').each(function( index ) {
			varAlarm.typePar.push($(this).val());
		});

		if(varAlarm.parameterInputPlaceholder[id] == 'hour'){
			varAlarm.widgetObj.find('.clockpicker').clockpicker({
		        autoclose: true,
		        'default': 'now'
		    });
		}
	},
	buildTable: function (){
    	if(typeof(varAlarm) != 'object'){return false;}
		varAlarm.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-alarm" width="100%"></table>' );
		//Pega os dados da tabela
		var table_alarm = varAlarm.widgetObj.find('#table-list-alarm').dataTable({
		    ajax: {
		        "url": 'widgets/routing.php',
		        "type": "POST",
		        "data": { url: 'alarm/list/'},
		  		error: function (error) {  
    				if(typeof(varAlarm) != 'object'){return false;}
		        	loadingWidget('#'+varAlarm.widgetName, true);
					createMsgBox('error', error.responseText, '#'+varAlarm.widgetName+' .panel-body');
		  		}
	      	},
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "fnDrawCallback": function( oSettings ) {
                varAlarm.widgetObj.find('.20_p').css('width', '20%');
                varAlarm.widgetObj.find('.40_p').css('width', '40%');

	    		varAlarm.widgetObj.find('#table-list-alarm tbody tr td > div.white').css('color', '#ffffff');
	    		varAlarm.widgetObj.find('#table-list-alarm tbody tr td > div.level_1').parent().addClass('bg-color-on');
	    		varAlarm.widgetObj.find('#table-list-alarm tbody tr td > div.level_2').parent().addClass('bg-color-stat');
	    		varAlarm.widgetObj.find('#table-list-alarm tbody tr td > div.level_3').parent().addClass('bg-color-off');
		    },
		    "columns": 
		    	[
		            { "title": "#", "data": "alarm_id", "class": 'hidden' },
		            { "title": "Alarme", "data": "alarm_name", "className": "40_p" },
		            { "title": "Grupo", "data": "group_name", "className": "40_p" },
		            { "title": "Nível", "data": "level", "className": "20_p" }
	          	],
          	"columnDefs": 
                [
                    {
                        "targets": [1],
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
                    },
                    {
                        "targets": [2], 
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
                    },
                    {
                        "targets": [3],
                        "orderable": false,
                        "render": function (data, type, full, meta) {
                        	switch(data) {
							    case 1:
									nameLevel = 'Baixo';
							        break;
							    case 2:
									nameLevel = 'Médio';
							        break;
							    case 3:
									nameLevel = 'Alto';
							        break;
							}
							return '<div class="nowrap ellipsis level_'+data+' white" title="' + nameLevel + '">' + nameLevel + '</div>';
                        }
                    }
                ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por Alarmes...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
    			if(typeof(varAlarm) != 'object'){return false;}
                varAlarm.widgetObj.find('.20_p').css('width', '20%');
                varAlarm.widgetObj.find('.40_p').css('width', '40%');
		    	//CLICANDO NA CONTA PARA EDITÃ�-LA
		    	if(json.data.length > 0){

		    		varAlarm.widgetObj.find('#table-list-alarm tbody tr').die("click").live("click", function(){
			    		//Pegando id
	    				var data = $(this).children('td').html();
			    	    loadingWidget('#'+varAlarm.widgetName);	
			    	    widgetsalarm.getById(data);

	    		    	return false;
	    		    });
				}  	
		  	}
		});
	},
	getById : function (data) {			
	    $.ajax({
	    	async: true,
	    	type: 'POST',
	        url: 'widgets/routing.php',
	        data: { 
	        	url: 'alarm/getById/', 
	        	data: JSON.parse(data) 
	        },
	        dataType: 'json',
	        success: function(data) {
		        widgetsalarm.loadEditForm(data.data);
	        	loadingWidget('#'+varAlarm.widgetName, true);
	        },
	        error: function(error) {
	        	createMsgBox('error', error.responseText, '#'+varAlarm.widgetName+' .panel-body');
	        	loadingWidget('#'+varAlarm.widgetName, true);
	        }
	    });
	},
	buildTableUnitAssoc : function (data, tableObj){
		if(typeof(varAlarm) != 'object'){return false;}
		tableObj.html( '<table class="table table-striped table-bordered" id="table-list-alarm-unit" width="100%"></table>' );
		//Pega os dados da tabela
		varAlarm.tableUnitAssocCad = varAlarm.widgetObj.find('#table-list-alarm-unit').DataTable({
		    "data": data,
		    paginate: true,
		    bLengthChange: false,
		    bFilter: false,
		    bInfo: true,
		    "pagingType": "full",
		    "fnDrawCallback": function( oSettings ) {
    			if(typeof(varAlarm) != 'object'){return false;}
                varAlarm.widgetObj.find('.30_p').css('width', '30%');
                varAlarm.widgetObj.find('.70_p').css('width', '70%');
		    },
		    "columns": 
		    	[
		            { "title": "#", "data": "id", "class": 'hidden' },
		            { "title": "Unidade", "data": "name", "orderable": false, "className": "30_p" },
		            { "title": "Descrição", "data": "description", "orderable": false, "className": "70_p" },
		            { "title": "", "data": "status", "orderable": false, "className": "10_p", "class": 'hidden' }
	          	],
          	"columnDefs": 
                [
                    {
                        "targets": [1],
		            	"render": function ( data, type, full, meta ) {
		            		return '<div class="nowrap ellipsis" title="'+data+'">'+data+'</div>';
						}
                    },
                    {
                        "targets": [2],
		            	"render": function ( data, type, full, meta ) {
		            		return '<div class="nowrap ellipsis" title="'+data+'">'+data+'</div>';
						}
                    },
                    {
                        "targets": [3],
		            	"render": function ( data, type, full, meta ) {
		            		if(data == 0){
								return '<div title="Alarme pendente de envio"><i class="fa fa-warning color-stat" style="font-size: 14px;"></i></div>';
		            		}else{
								return '<div title="Alarme enviado"><i class="fa fa-check-circle color-on" style="font-size: 15px;"></div>';
		            		}
						}
                    }
                ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por Unidade...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
    			if(typeof(varAlarm) != 'object'){return false;}
                varAlarm.widgetObj.find('.30_p').css('width', '30%');
                varAlarm.widgetObj.find('.70_p').css('width', '70%');
		  	}
		});
		return false; 	
	}
};

function load_widgetsalarm_Page(){
	if(typeof(varAlarm) != 'object'){return false;}
	varAlarm.accessLevel = handleAccessLevel(varAlarm.widgetName);
	if(varAlarm.accessLevel == 1){
		varAlarm.widgetObj.find('.openForm').addClass('hidden');
	}

	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'alarm/index/'},
        dataType: 'json',
        beforeSend: function () {
    		if(typeof(varAlarm) != 'object'){return false;}
        	closeForm(varAlarm.widgetName);
			widgetsalarm.reset();	
			varAlarm.widgetObj.find('.btn').attr('disabled', 'disabled');
            varAlarm.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	if(typeof(varAlarm) != 'object'){return false;}
        	widgetsalarm.buildTable();
        	widgetsalarm.buildGroup(JSON.parse(data.groupinfo));
        	widgetsalarm.buildParameterList(JSON.parse(data.parameterInfo));
        	widgetsalarm.buildCercaList(JSON.parse(data.cercaInfo));
        	widgetsalarm.buildEventList(JSON.parse(data.eventInfo));

			widgetsalarm.reset();	
        	//SET ID account user
        	varAlarm.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	//liberando menu
        	varAlarm.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varAlarm.widgetName, true); 
        },
        error: function(error) {
    		if(typeof(varAlarm) != 'object'){return false;}
        	loadingWidget('#'+varAlarm.widgetName, true);
			createMsgBox('error', error.responseText, '#'+varAlarm.widgetName+' .panel-body');
        },
        complete: function() {
    		if(typeof(varAlarm) != 'object'){return false;}
            varAlarm.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
	load_widgetsalarm_Page();

    if(typeof(varAlarm) != 'object'){return false;}

    //INICIALIZANDO COMPONENTE DE ASSOCIAÇÃO DE UNIDADES
    //inicializando componente de associação de unidades
    var info = {
    	widgetName: varAlarm.widgetName,
    	btnSaveName: 'Associar',
    	widgetVar: 'varAlarm',
    	title: 'Associação de unidades',
    	titletab: 'Unidades',
    	type: 'units',
    	assocUrl: 'alarm/assocAlarmUnit/'
    }
    handleAssocSelect.init(info);
    //FIM DO COMPONETE DE ASSOCIAÇÃO DE UNIDADES

	//ABRIR FORM PARA CADASTRO
	varAlarm.widgetObj.find('.openForm').die("click").live("click", function(){
		widgetsalarm.reset();
		varAlarm.action = 'add';
		openForm(varAlarm.widgetName, 'add');
	});
	//CHAMADA PARA EXCLUSÃO DE ALARME 
	varAlarm.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		openConfirm(varAlarm.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir Alarme", "Deseja remover o Alarme selecionado?", "Sim", "Não");
	});
	varAlarm.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		widgetsalarm.delete();
		closeConfirm(varAlarm.widgetName);
	});
	varAlarm.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varAlarm.widgetName);
	});

	varAlarm.widgetObj.find('form').find('.cancel').die('click').live('click', function() {
		closeForm(varAlarm.widgetName);
		widgetsalarm.reset();		
	});	

	varAlarm.widgetObj.find('.visualize-box').find('.cancel').die('click').live('click', function() {
		closeVisualize(varAlarm.widgetName);
	});	

	varAlarm.widgetObj.find('.btn-level').die("click").live("click", function(){	
		varAlarm.widgetObj.find('.btn-level').removeClass('selectdLevel');
		$(this).addClass('selectdLevel');		
		varAlarm.widgetObj.find('#level').val($(this).attr('id'));		
	});

	varAlarm.widgetObj.find('#sendEmail').die("click").live("click", function(){	
		if($(this).attr('checked') == 'checked'){
			varAlarm.widgetObj.find('.field-email').removeClass('hidden');
		}else{
			varAlarm.widgetObj.find('.field-email').addClass('hidden');
		}	
	});

	varAlarm.widgetObj.find('.plus-alarm').die("click").live("click", function(){	
		widgetsalarm.handleAddAlarmParameter();
	});

	varAlarm.widgetObj.find('.minus-alarm').die("click").live("click", function(){	
		widgetsalarm.handleRemoveAlarmParameter();
	});

	varAlarm.widgetObj.find('.alarm_parameter').die("change").live("change", function(){
		widgetsalarm.handleLoadAlarmParameter($(this));
	});

	varAlarm.widgetObj.find('form').submit(function(e) { 
		e.preventDefault();
	    if ( $(this).parsley().isValid() ) {
			var form = $(this); 

			var alarm_parameter = form.find('.alarm_parameter');
			var alarm_parameter_val = [];
			alarm_parameter.each(function() {
				alarm_parameter_val.push($(this).val());
			});
			var alarm_parameter_type = form.find('.alarm_parameter_type');
			var alarm_parameter_type_val = [];
			alarm_parameter_type.each(function() {
				alarm_parameter_type_val.push($(this).val());
			});
			var alarm_option = form.find('.alarm_option');
			var alarm_option_val = [];
			alarm_option.each(function() {
				alarm_option_val.push($(this).val());
			});

		    var data = 
				{
					id: $(this).find("#id").val(),
					name: $(this).find("#name").val(),
					group_id: $(this).find("#group").val(),
					account_id: $(this).find("#account_id").val(),
					level: $(this).find("#level").val(),
					description: $(this).find("#description").val(),
					notif_email: $(this).find("#sendEmail").attr('checked') == 'checked' ? true : false,
					notif_monitor: $(this).find("#showInMonitor").attr('checked') == 'checked' ? true : false,
					time_send_mail: $(this).find("#sendEmail").attr('checked') == 'checked' ? $(this).find("#time_send_mail").val() : 0,
					email_notification: $(this).find("#email_notification").val(),
					alarm_parameter_val: alarm_parameter_val,
					alarm_parameter_type_val: alarm_parameter_type_val,
					alarm_option_val: alarm_option_val
				};

		    data = JSON.stringify(data);
		    
	        $.ajax({
	        	async: true,
				type : 'POST',
				url : 'widgets/routing.php',
				data : {
					url : 'alarm/'+varAlarm.action+'/',
					data : JSON.parse(data)
				},
				dataType : 'json',
				beforeSend : function() {
    				if(typeof(varAlarm) != 'object'){return false;}
    	        	loadingWidget('#'+varAlarm.widgetName);
				},
				success : function(data) {
					var id = data.data[0];
					var msg = data.data[1];

        			if(typeof(varAlarm) != 'object'){return false;}   	        
	    	        //Criando mensagem de retorno
	    	        createMsgBox('success', msg, '#'+varAlarm.widgetName+' .panel-body');
	    	        // atualiza tabela que lista as contas
	    	        varAlarm.widgetObj.find('#table-list-alarm').DataTable().ajax.reload(null, false);

	    	        if(varAlarm.action == 'edit'){ 
		    	        //reseta o form	    
						closeForm(varAlarm.widgetName);
		    	        widgetsalarm.reset();
	        			loadingWidget('#'+varAlarm.widgetName, true);   
	    	        }else{
	    	        	widgetsalarm.getById(id);
	    	        	varAlarm.widgetObj.find('.edit').removeClass('hidden')
	    	        	varAlarm.widgetObj.find('.add').addClass('hidden');
	    	        }
				},
				error : function(error) {
    				if(typeof(varAlarm) != 'object'){return false;}
    	        	loadingWidget('#'+varAlarm.widgetName, true);
    	        	createMsgBox('error', error.responseText, '#'+varAlarm.widgetName+' .panel-body');		
				}
			});
			return false;
	    }
	});
});



