var varDevices = {};
	varDevices.widgetName 	= 'widgetsdevice';
	varDevices.widgetObj 	= $('#'+varDevices.widgetName);
	varDevices.action 		= 'add';

var widgetsDevice = {
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	loadEditForm: function (data, widget) {
		var form = varDevices.widgetObj.find('form');
		//limpa formulário
		widgetsDevice.reset();
		// muda o form para edição
		varDevices.action = 'edit';

		form.find('#device_id').val($(data)[0].device_id);
		form.find("#identifier").val($(data)[0].identifier);
		form.find("#asset").val($(data)[0].asset);
		
		form.find('#manufacturer option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == $(data)[0].manufacturer_id) {
				$(this).prop('selected', true);
				widgetsDevice.loadModel(value, "#model", $(data)[0].model_id);
			}

		});

		openForm(varDevices.widgetName, varDevices.action);
	},
	buildManufactures: function (data){
		varDevices.widgetObj.find('#manufacturer').html('<option value="">Selecione um fabricante</option>');
		var template = 	'{{#manufactures}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/manufactures}}';
		var select = Mustache.to_html(template, data);
		varDevices.widgetObj.find('#manufacturer').append(select);
	},
	reset: function (){
		var form = varDevices.widgetObj.find('form');

		varDevices.action = 'add'; 

		form[0].reset();  
		form.find('#manufacturer').prop('selectedIndex', 0);
		form.find('#model').html('<option value="">Selecione um modelo</option>');
		form.find('#model').prop('selectedIndex', 0);
		form.find('#asset').removeClass('parsley-error');
		form.find('#parsley-asset').html('');
		
		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');

		widgetsDevice.disabledSaveAndDelete(false);	
		widgetsDevice.disabledFields(false);
	},
	loadModel: function (manufacturer, target, model) {
		$.ajax({
			async : true,
			type : "POST",
			data : {
				url : 'device/listModel',
				data : manufacturer
			},
			url : 'widgets/routing.php',
			dataType : "json",
			beforeSend: function(){
				varDevices.widgetObj.find(target).html($('<option></option>').val('').html('Carregando...'));				
			},
			success : function(json) {

				varDevices.widgetObj.find(target).children().remove();
				$(json).each(function(index, obj) {
					
					varDevices.widgetObj.find(target).append($('<option></option>').val(obj.id).html(obj.name));

					if (model == obj.id) {
						$(target).children(obj.id).prop('selected', true);
					}

				});
				widgetsDevice.disabledSaveAndDelete(false);	
			},
			error : function(xhr, ajaxOptions, thrownError) {
				widgetsDevice.disabledSaveAndDelete(false);	
				varDevices.widgetObj.find(target).children().remove();
			}
		});
	},
	loadDeviceFromAsset: function (target, asset, form_type) {
		$.ajax({
			async : true,
			type : "POST",
			data : {
				url : 'device/listAssetByTag',
				data : asset
			},
			url : 'widgets/routing.php',
			dataType : "json",
			beforeSend: function(){
				varDevices.widgetObj.find(target).removeClass("btn-primary");
				varDevices.widgetObj.find(target).addClass("btn-info");
				varDevices.widgetObj.find(target).html('Aguarde');		

				widgetsDevice.disabledSaveAndDelete(true);		
			},
			success : function(data) {

				var form = varDevices.widgetObj.find('form');
				form.find('.btn-group button').attr("disabled",true);
				form.find('#identifier').val($(data)[0].identifier);
				
				form.find('#manufacturer option').each(function(i, obj) {
					var value = $(this).attr('value');
					if (value == $(data)[0].manufacturer_id) {
						$(this).prop('selected', true);
						widgetsDevice.loadModel(value, "#model", $(data)[0].model_id);
					}

				});		

				form.find(target).removeClass("btn-info");
				form.find(target).addClass("btn-primary");
				form.find(target).html('<i class=" fa fa-1x fa-search "></i> Buscar');
				form.find("#asset").removeClass('parsley-error');
				form.find('#parsley-asset').html('');

	        	// liberando ou bloqueando campos
				widgetsDevice.disabledFields(true);

			},
			error : function(error) {
				varDevices.widgetObj.find(target).removeClass("btn-info");
				varDevices.widgetObj.find(target).addClass("btn-primary");
				varDevices.widgetObj.find(target).html('<i class=" fa fa-1x fa-search "></i> Buscar');
				varDevices.widgetObj.find('#asset').addClass('parsley-error');
	        	varDevices.widgetObj.find('#parsley-asset').html("Patrimônio não encontrado.");
	        	// liberando ou bloqueando campos
				widgetsDevice.disabledSaveAndDelete(false);	
				widgetsDevice.disabledFields(false);
			}
		});
	},
	delete: function () {
	    var data = { id: varDevices.widgetObj.find('form').find("#device_id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'device/deletedevice',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varDevices.widgetName);
			},
			success : function(data) {
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varDevices.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varDevices.widgetObj.find('#table-list-device').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varDevices.widgetName);
				widgetsDevice.reset();
				loadingWidget('#'+varDevices.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varDevices.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varDevices.widgetName+' .panel-body');		
			}
		});
		return false;
	},
	disabledFields: function (value){
		varDevices.widgetObj.find("#identifier").attr('disabled', value);
		varDevices.widgetObj.find("#manufacturer").attr('disabled', value);
		varDevices.widgetObj.find("#model").attr('disabled', value);
	},
	disabledAndResetForm: function (){
		widgetsDevice.disabledFields(false);

		// Resetando form
		varDevices.widgetObj.find("#manufacturer").prop('selectedIndex', 0);
		varDevices.widgetObj.find("#model").html("<option value=''>Selecione um modelo</option>");
	},
	disabledSaveAndDelete: function (type){
		varDevices.widgetObj.find('.save').attr('disabled', type);
		varDevices.widgetObj.find('.delete').attr('disabled', type);
		varDevices.widgetObj.find('.cancel').attr('disabled', type);
	},
	unloadForm: function (){
		varDevices.widgetObj.find("#model").html("<option>Selecione um modelo</option>");
			
		varDevices.widgetObj.find("#manufacturer option").each(function(i, obj) {
			$(this).attr("selected",false);
		});
		
		var manufacturer = varDevices.widgetObj.find("#manufacturer option")[0];
		$(manufacturer).attr("selected",true);
	},
	buildTable: function (device){
		varDevices.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-device" width="100%"></table>' );
		//Pega os dados da tabela
		var table_devices = varDevices.widgetObj.find('#table-list-device').dataTable({
		    ajax: {
		        "url": 'widgets/routing.php',
		        "type": "POST",
		        "data": { url: 'device/listDevices/'},
		  		error: function (error) {  
		        	loadingWidget('#'+varDevices.widgetName, true);
					createMsgBox('error', error.responseText, '#'+varDevices.widgetName+' .panel-body');
		  		}
		      },
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "columns": 
		    	[
		            { "title": "#", "data": "device_id", "class": 'hidden' },
		            { 
		            	"title": "Identificador", 
		            	"data": "identifier",
		            	"className": "40_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            },
		            { 
		            	"title": "Modelo", 
		            	"data": "model_name" ,
		            	"className": "60_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            },
		            { 
		            	"title": "Recurso", 
		            	"data": "asset", 
		            	"class": 'hidden' 
		            },
		            { 
		            	"title" : "ID", 
		            	"data": "internal_id", 
		            	"class": 'hidden' 
		            },
		            { 
		            	"title": "Conta", 
		            	"data": "account_name", 
		            	"class": 'hidden' 
		            }
		          ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por dispositivos...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
                varDevices.widgetObj.find('.40_p').css('width', '40%');
                varDevices.widgetObj.find('.60_p').css('width', '60%');
		    	//CLICANDO NA CONTA PARA EDITÃ�-LA
		    	if(json.data.length > 0){

			    	varDevices.widgetObj.find('#table-list-device tbody').delegate('tr', 'click', function() {
			    		//Pegando id da conta
			    		var data = $(this).children('td').html();
			    	    loadingWidget('#'+varDevices.widgetName);		
		    		    $.ajax({
		    		    	async: true,
		    		    	type: 'POST',
		    		        url: 'widgets/routing.php',
		    		        data: { 
		    		        	url: 'device/createEditFormDevice/', 
		    		        	data: JSON.parse(data) 
		    		        },
		    		        dataType: 'json',
		    		        success: function(data) {
			    		        widgetsDevice.loadEditForm(data, varDevices.widgetName);
			    	        	loadingWidget('#'+varDevices.widgetName, true);
		    		        },
		    		        error: function(error) {
			    	        	createMsgBox('error', error.responseText, '#'+varDevices.widgetName+' .panel-body');
			    	        	loadingWidget('#'+varDevices.widgetName, true);
		    		        }
		    		    });
		    		    return false;
		    		}); 
				}  	
		  	}
		});
	}
};

function load_widgetsdevice_Page(){
	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'device/index/'},
        dataType: 'json',
        beforeSend: function () {
        	closeForm(varDevices.widgetName);
			widgetsDevice.reset();	
			varDevices.widgetObj.find('.btn').attr('disabled', 'disabled');
            varDevices.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	widgetsDevice.buildTable(data.deviceList);
        	widgetsDevice.buildManufactures(JSON.parse(data.manufacturerinfo));
        	//SET ID account user
        	varDevices.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	//liberando menu
        	varDevices.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varDevices.widgetName, true); 

        	//Carregando comportamento do widget
        },
        error: function(error) {
        	loadingWidget('#'+varDevices.widgetName, true);
            varDevices.widgetObj.find('.btn-danger').removeAttr("disabled");
			createMsgBox('error', error.responseText, '#'+varDevices.widgetName+' .panel-body');
        }
    });
}

$(document).ready(function() {
	load_widgetsdevice_Page();

	varDevices.widgetObj.find('.openForm').die("click").live("click", function(){
		widgetsDevice.reset();
		varDevices.action = 'add';
		openForm(varDevices.widgetName, 'add');
	});
	//CHAMADA PARA EXCLUSÃO DE Devices 
	varDevices.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		openConfirm(varDevices.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir Dispositivo", "Deseja remover o dispositivo selecionado?", "Sim", "Não");
	});
	varDevices.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		widgetsDevice.delete();
		closeConfirm(varDevices.widgetName);
	});
	varDevices.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varDevices.widgetName);
	});
	//Resetando árvores ao clicar em cancelar/fechar
	varDevices.widgetObj.find('.cancel').die('click').live('click', function() {
		closeForm(varDevices.widgetName);
		widgetsDevice.reset();		
	});

	varDevices.widgetObj.find('#asset').keypress(function() {
	    widgetsDevice.disabledSaveAndDelete(true);
	});

	varDevices.widgetObj.find('#asset').change(function() {		
		widgetsDevice.loadDeviceFromAsset("#search_asset", $(this).val(), varDevices.action);
	});	

	varDevices.widgetObj.find('#manufacturer').change(function() {
		widgetsDevice.loadModel($(this).val(), "#model");
	});	
	
	varDevices.widgetObj.find('#search_asset').die('click').live('click', function() {
		widgetsDevice.loadDeviceFromAsset("#search_asset", $(this).val(), varDevices.action);
	});	

	varDevices.widgetObj.find('form').submit(function(e) { 
		e.preventDefault();
	    if ( $(this).parsley().isValid() ) {
			var form = $(this);        
		    var data = 
				{
					device_id: form.find("#device_id").val(),
					model: form.find("#model").val(),
					asset: form.find("#asset").val(),
					identifier: form.find("#identifier").val(),
					internal_id: form.find("#model").val() + form.find("#identifier").val(),
					account_id: form.find("#account_id").val()
				};
		    
		    data = JSON.stringify(data);
		    
	        $.ajax({
	        	async: true,
				type : 'POST',
				url : 'widgets/routing.php',
				data : {
					url : 'device/'+varDevices.action+'/',
					data : JSON.parse(data)
				},
				dataType : 'json',
				beforeSend : function() {
    	        	loadingWidget('#'+varDevices.widgetName);
				},
				success : function(data) {
    	        	loadingWidget('#'+varDevices.widgetName, true);       	        
	    	        //Criando mensagem de retorno
	    	        createMsgBox('success', data, '#'+varDevices.widgetName+' .panel-body');
	    	        // atualiza tabela que lista as contas
	    	        varDevices.widgetObj.find('#table-list-device').DataTable().ajax.reload(null, false);	    	            	
	    	        //reseta o form	    
					closeForm(varDevices.widgetName);
	    	        widgetsDevice.reset(); 
				},
				error : function(error) {
    	        	loadingWidget('#'+varDevices.widgetName, true);
    	        	//Criando mensagem de retorno
    	        	createMsgBox('error', error.responseText, '#'+varDevices.widgetName+' .panel-body');		
				}
			});
			return false;
	    }
	});
});



