var varMapBox = {};
	varMapBox.widgetName 		= 'widgetsmapbox';
	varMapBox.widgetObj 		= $('#'+varMapBox.widgetName);
	varMapBox.mapInstance;
	varMapBox.DefaultLatitude 	= -10.992611;
	varMapBox.DefaultLongitude 	= -49.475099;
	varMapBox.DefaultZoomIni 	= 4;
	varMapBox.DefaultZoomGoto 	= 14;
	varMapBox.mapKey 			= 'pk.eyJ1IjoibW92YW1hcHMiLCJhIjoiZTIwM2MzNTBkMzBlMDI3YThmNzE4MmViMDhmZjgwZGUifQ.7cJw2Mx5rHo2sx2jFPDQhQ';
	//varMapBox.mapStyle 			= 'movamaps.p0kmi9kl';
	varMapBox.mapStyle 			= 'movamaps.928255e7';

	varMapBox.mapPathLayer;
	varMapBox.mapPathPOILayer;
	varMapBox.lastMarker;
	varMapBox.pathMarkerList	= [];

	varMapBox.unitClusterLayer;
	varMapBox.UnitGroupDataListShow 	= [];
	varMapBox.UnitSubGroupDataListShow	= [];
	varMapBox.unitsInfoBox		= [];
	varMapBox.unitLabelSelected;
	//variaveis de POI
	varMapBox.POILayer;	
	varMapBox.POIDataList;	
	varMapBox.POIDataListShow 	= [];	
	varMapBox.markerPOISelected;
	varMapBox.markerPOI;
	//Variaveis de cerca
	varMapBox.cercaLayer;	
	varMapBox.cercaDataList;	
	varMapBox.cercaDataListShow 	= [];
	varMapBox.drawControlCerca;
	varMapBox.featureGroupCerca;
	varMapBox.drawPolyId 		= 0;
	varMapBox.listaIdCerca;

	varMapBox.sideMenuSize 		= 200;

var MapBox = {
  	load: function(){
    	varMapBox.widgetObj.find('.btn-danger').removeAttr("disabled");
    	varMapBox.widgetObj.find('.mapstyle').attr("disabled", "disabled");
		if(typeof(varMapBox.mapInstance) == 'object'){
			varMapBox.mapInstance.remove();
	    }

		$('head').append('<link rel="stylesheet "href="https://api.tiles.mapbox.com/mapbox.js/v2.2.1/mapbox.css" type="text/css" />');
		$('head').append('<link href="https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css" rel="stylesheet" />');
		$('head').append('<link rel="stylesheet "href="https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css" type="text/css" />');   
		$('head').append('<link href="assets/plugins/Leaflet.draw/dist/leaflet.draw.css" rel="stylesheet" />');      
		$.getScript('https://api.tiles.mapbox.com/mapbox.js/v2.2.1/mapbox.js').done(function( script, textStatus ) {
			$.getScript('https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js').done(function( script, textStatus ) {
				$.getScript('assets/plugins/Leaflet.draw/dist/leaflet.draw.js', function(){
					L.mapbox.accessToken = varMapBox.mapKey;
					varMapBox.mapInstance = L.mapbox.map('map', varMapBox.mapStyle).setView([varMapBox.DefaultLatitude, varMapBox.DefaultLongitude], varMapBox.DefaultZoomIni);
		            //liberando menu
					var mapHeight = $(window).height();					
					if(varMapBox.widgetObj.hasClass('full-Scr')){
						varMapBox.widgetObj.find('#map-content').css('height', mapHeight - 50);
					}else{
						varMapBox.widgetObj.find('#map-content').css('height', mapHeight - 408);
					}

		            varMapBox.widgetObj.find('.btn').removeAttr("disabled");
		            varMapBox.widgetObj.find('#loadingProgressG').addClass('hidden');
		            varMapBox.widgetObj.find('#map-content').removeClass('hidden');
		            varMapBox.mapInstance.invalidateSize();
	            	loadingWidget('#'+varMapBox.widgetName, true); 

				    window.setTimeout(function() {
				    	MapBox.LoadLayers();
    					varMapBox.widgetObj.find('.mapstyle').removeAttr("disabled");
    					varMapBox.widgetObj.find('.removePath').trigger('click');
				    }, 1000); 
				});
			});
		});
	},
	LoadLayers: function(){
		if(typeof(varMapBox.mapInstance) == 'object'){
			MapBox.loadClusteredUnitsLayer();
			MapBox.loadPOILayer();
			MapBox.loadCercaLayer();	

	    	window.setTimeout(function() {
				MapBox.handleMapSidebar();
		    }, 1000); 	
		}else{
		    window.setTimeout(function() {
		    	MapBox.LoadLayers()
		    }, 1000); 		
		}
	},
	ReloadLayers: function(){
		if(typeof(varMapBox.mapInstance) == 'object'){
			MapBox.loadClusteredUnitsLayer();
			//remove todos os marcadores genericos do mapa
			if(typeof(varMapBox.genericUnitLayer) == 'object'){
	  			varMapBox.mapInstance.removeLayer(varMapBox.genericUnitLayer);
	  		} 	
			//remove todos os POIs marcadores do mapa
	  		MapBox.getMarkerPOI(0, 0, '');
		}else{
		    window.setTimeout(function() {
		    	MapBox.LoadLayers()
		    }, 1000); 		
		}
	},
	loadClusteredUnitsLayer: function(){
		if(typeof(varMapBox) != 'object') { return false; }  
		if(typeof(varMapBox.unitClusterLayer) == 'object') {
  			varMapBox.mapInstance.removeLayer(varMapBox.unitClusterLayer);
      		varMapBox.unitClusterLayer = undefined;
		}
		if(varMapBox.widgetObj.find('#unit').attr('checked') != 'checked'){return false;}	

		varMapBox.unitClusterLayer = new L.MarkerClusterGroup({
			showCoverageOnHover: false,
			removeOutsideVisibleBounds: true, 
			animateAddingMarkers: true, 
			disableClusteringAtZoom: 14, 
			maxClusterRadius: 40
		});

		$(varGlobal.unitData.features).each(function(i,o){
			if((varMapBox.UnitGroupDataListShow.indexOf('group-unit-'+o.properties.group_id) == -1) && (varMapBox.UnitSubGroupDataListShow.indexOf('subgroup-unit-'+o.properties.subgroup_id) == -1)){
				var objClone;
				var marker = L.marker(new L.LatLng(o.geometry.coordinates[1],o.geometry.coordinates[0]), {
					icon: L.divIcon({
						iconAnchor: [15,27],
						className: 'unit '+o.properties.label,
						html: o.properties.iconhtml
					}),
					properties : {
						title : o.properties.label,
						'data' : o.properties
					}
		        });
				marker.on('click', MapBox.setUpBubble);
				objClone = $.extend(true, {},  o);
				varMapBox.unitClusterLayer.addLayer(marker);
				varMapBox.unitsInfoBox.push(objClone);
			}
		});
		varMapBox.mapInstance.addLayer(varMapBox.unitClusterLayer);

		varMapBox.mapInstance.on('zoomend', function() {
			MapBox.highlightMapBox(1, varMapBox.unitLabelSelected, false);
			varMapBox.mapInstance.invalidateSize();
		});
	},
	//controle de POI
	//#######################################
	loadPOILayer: function(){
		$.ajax({
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'poi/list/',				
			},
			dataType : 'json',
			success : function(json) {
				varMapBox.POIDataList = json;
				
				MapBox.drawPOILayer();
			},			
			error : function(error) {
	            //Criando mensagem de retorno
	        	createMsgBox('error', error.responseText,'#'+varMapBox.widgetName+' .panel-body');  
			}
		});
	},
	drawPOILayer: function (){
		if(varMapBox.POIDataList.data.length <= 0){return false;}
		
		if(typeof(varMapBox) != 'object') { return false; }
  		if(typeof(varMapBox.POILayer) == 'object') {
      		varMapBox.mapInstance.removeLayer(varMapBox.POILayer);
      		varMapBox.POILayer = undefined;
  		} 	
  		if(varMapBox.widgetObj.find('#poi').attr('checked') != 'checked'){return false;}

		//varMapBox.POILayer = new L.mapbox.featureLayer();
		varMapBox.POILayer = new L.MarkerClusterGroup({
			showCoverageOnHover: false,
			removeOutsideVisibleBounds: true, 
			animateAddingMarkers: true, 
			disableClusteringAtZoom: 14, 
			maxClusterRadius: 40,
			iconCreateFunction: function(cluster) {
				return new L.DivIcon({
					//iconSize: [20, 20],
					html: '<div class="dv-cluster-poi">' + cluster.getChildCount() + '</div>'
				});
			}
		});
		
		$(varMapBox.POIDataList.data).each(function(i,o){
			if(varMapBox.POIDataListShow.indexOf('group-poi-'+o.group_id) == -1){
				var marker = L.marker(new L.LatLng(o.latitude,o.longitude), {
					icon: L.divIcon({
						iconAnchor: [15,15],
						//className: 'poi '+o.poi_id+' group-poi-'+o.group_id,
						html: o.iconhtmlstack
					}),
					properties : {
						title : o.poi_name,
						'data' : o
					}
		        });
				//marker.on('click', setUpBubble);
				var content = '<b>'+ o.poi_name+'<\/b><br>' + o.description;
				marker.bindPopup(content,{
			        closeButton: false,
			        minWidth: 250,
			        className: 'popupPOIclass'
			    });
				varMapBox.POILayer.addLayer(marker);				
			}
		});
		varMapBox.mapInstance.addLayer(varMapBox.POILayer);
	},
	addMarkerPOI: function (lat, lng){
    	if(typeof(varPOI) != 'object'){return false;}
		var form = varPOI.widgetObj.find('form');
		var color = form.find('.color').val();
		var icon = form.find('.selected-icon').children('i').attr('class');
		//remove marcador caso exista
		MapBox.removeMarkerPOI();

		varMapBox.markerPOI = L.marker(new L.LatLng(lat, lng), {
		    icon: L.divIcon({
				iconAnchor: [15,15],
		        html: MapBox.getStackMarkerIcon(color, icon)
		    }),
		    draggable: true
		}).addTo(varMapBox.mapInstance);

		varMapBox.markerPOI._icon.style.border = '2px solid rgb(245, 156, 26)';
		varMapBox.markerPOI._icon.style.width = '36px';
		varMapBox.markerPOI._icon.style.height = '36px';
		varMapBox.markerPOI._icon.style.borderRadius = '25px';
		varMapBox.markerPOI._icon.style.backgroundColor = '#fff';

		varMapBox.markerPOI.on('dragend', function(e){
			var m = varMapBox.markerPOI.getLatLng();
		    var lat = m.lat;
		    var lng = m.lng;
		    //carrega lat e long nos campos de cadastro do POI
		    form.find('#lat').val(lat);
		    form.find('#long').val(lng);
		});
	},
	disabledMarker: function (){
    	if(typeof(varPOI) == 'object'){
			varPOI.widgetObj.find('form').find('.call_map').removeClass('color-stat');
		}
		if(typeof(varMapBox) == 'object'){
			if(typeof(varMapBox.markerPOI) == 'object'){
				varMapBox.markerPOI.dragging.disable();
				varMapBox.markerPOI._icon.style.border = '';
				varMapBox.markerPOI._icon.style.backgroundColor = '';
				varMapBox.markerPOI._icon.style.width = '0px';
				varMapBox.markerPOI._icon.style.height = '0px';		
				
				MapBox.removeMarkerPOI();
				varMapBox.markerPOI = undefined;	
			}
			
			if(typeof(varMapBox.markerPOISelected) == 'object'){
				varMapBox.markerPOISelected._icon.style.display = '';
				varMapBox.markerPOISelected._icon.style.border = '';
				varMapBox.markerPOISelected._icon.style.backgroundColor = '';
				varMapBox.markerPOISelected._icon.style.width = '0px';
				varMapBox.markerPOISelected._icon.style.height = '0px';
			}
		}
	},
	removeMarkerPOI: function (){
		if(typeof(varMapBox.markerPOI) == 'object'){
			varMapBox.mapInstance.removeLayer(varMapBox.markerPOI);
			varMapBox.markerPOI = undefined;
		}
	},
	getMarkerPOI: function (id, z, display){
		var maxZindex = 0;
		var title;

		if(typeof(varMapBox.POILayer) != 'object'){return false;}

		if(z == 0){
			maxZindex = 0;
		}else{
			varMapBox.unitClusterLayer.eachLayer(function(marker) { 
				if(marker._icon != null){
					if(marker._icon.style.zIndex > maxZindex){
						maxZindex = marker._icon.style.zIndex;
					}
				}
			});
		}
		varMapBox.POILayer.eachLayer(function(marker) { 
			if(typeof(marker.options.properties) == 'object'){
				
				markerId = marker.options.properties.data.poi_id;

				if(marker._icon != null){
					marker._icon.style.display = '';
					marker._icon.style.border = '';
					marker._icon.style.backgroundColor = '';
					marker._icon.style.width = '0px';
					marker._icon.style.height = '0px';	

					if (markerId == id){
						varMapBox.markerPOISelected = marker;
						marker._icon.style.display = display;
						marker._icon.style.zIndex = maxZindex + 2;
						marker._icon.style.border = '2px solid rgb(245, 156, 26)';
						marker._icon.style.width = '36px';
						marker._icon.style.height = '36px';
						marker._icon.style.borderRadius = '25px';
						marker._icon.style.backgroundColor = '#fff';
					}
				}
			}
		});
		return true;
	},
	getStackMarkerIcon: function (color, icon){
		//monta stack do POI no mapa
		return 	'<span class="fa-stack fa-lg">'+
				'	<i class="fa fa-circle fa-stack-2x" style="color: ' + color + ';"></i>'+
				'	<i class="fa ' + icon + ' fa-stack-1x"></i>'+
				'</span>';
	},
	reloadMarkerActive: function (){
    	if(typeof(varPOI) != 'object'){return false;}
		var form = varPOI.widgetObj.find('form');
		//alterar marcador no mapa conforme alterações de formulário
		if(typeof(varMapBox.markerPOI) == 'object'){
			var m = varMapBox.markerPOI.getLatLng();
		    var lat = m.lat;
		    var lng = m.lng;

			MapBox.addMarkerPOI(lat, lng);
		}
	},
	activePOIControls: function (){
		if(typeof(varPOI) == 'object'){
			varPOI.widgetObj.find('[data-click=panel-remove]').die("click").live("click", function(){
				if(typeof(MapBox) == 'object'){
					varMapBox.widgetObj.find('.leaflet-container').css('cursor', '');
					MapBox.disabledMarker(); 
					if(typeof(MapBox.mapInstance) == 'object'){
						MapBox.mapInstance.off('click');
					}
				}
			});
		}
	},
	//Controle de CErca	
	//#######################################
	loadCercaLayer: function(){
		$.ajax({
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'cerca/list/',				
			},
			dataType : 'json',
			success : function(json) {
				varMapBox.cercaDataList = json;
				
				MapBox.drawCercaLayer();
			},			
			error : function(error) {
				varMapBox.cercaLayer = L.featureGroup().addTo(varMapBox.mapInstance);
	        	createMsgBox('error', error.responseText,'#'+varMapBox.widgetName+' .panel-body');  
			}
		});
	},
	drawCercaLayer: function(){
		if(varMapBox.cercaDataList.data.length <= 0){return false;}

		if(typeof(varMapBox) != 'object') { return false; }
  		if(typeof(varMapBox.cercaLayer) == 'object') {
      		varMapBox.mapInstance.removeLayer(varMapBox.cercaLayer);
      		varMapBox.cercaLayer = undefined;
  		} 
		if(varMapBox.widgetObj.find('#cerca').attr('checked') != 'checked'){return false;}

		varMapBox.cercaLayer = L.featureGroup().addTo(varMapBox.mapInstance);
		varMapBox.listaIdCerca = [];
        var bounds = [];
        var polyline_options;
		
		$(varMapBox.cercaDataList.data).each(function(i,o){			
			if(varMapBox.cercaDataListShow.indexOf('group-cerca-'+o.group_id) == -1){
				switch(o.polygroup_type) {
				    case 1:
						var circle_options = {
							color: o.color,     // Stroke color
							opacity: 1,         // Stroke opacity
							weight: 1,          // Stroke weight
							fillColor: o.color, // Fill color
							fillOpacity: 0.5,    // Fill opacity
							className: 'cerca '+o.cerca_id+' group-cerca-'+o.group_id
						};

						MapBox.drawCerca(1, o.cerca_id, 0, o.latitude, o.longitude, o.radius, circle_options, o.cerca_name, o.description);		

				        break;
				    case 2:

						polyline_options = {
							color: o.color,     // Stroke color
							opacity: 1,         // Stroke opacity
							weight: 1,          // Stroke weight
							fill: true,			// Fill polygon
							fillColor: o.color, // Fill color
							fillOpacity: 0.5,    // Fill opacity
							className: 'cerca '+o.cerca_id+' group-cerca-'+o.group_id
						};

						bounds = [];
						var points = o.polygon;
						var coords;
						points = points.split('|');

						points.forEach(function(p) {
							coords = p.split(',');
							bounds.push([coords[0], coords[1]]);
						});

				        MapBox.drawCerca(2, o.cerca_id, bounds, 0, 0, 0, polyline_options, o.cerca_name, o.description);									
		        				
						//bounds.push([o.lat, o.lng]);
				        break;
				    case 3:

						polyline_options = {
							color: o.color,     // Stroke color
							opacity: 1,         // Stroke opacity
							weight: 1,          // Stroke weight
							fill: true,			// Fill polygon
							fillColor: o.color, // Fill color
							fillOpacity: 0.5,    // Fill opacity
							className: 'cerca '+o.cerca_id+' group-cerca-'+o.group_id
						};

						bounds = [];
						var points = o.polygon;
						var coords;
						points = points.split('|');
						
						points.forEach(function(p) {
							coords = p.split(',');
							bounds.push( L.latLng(coords[0], coords[1]) );
						});
						
				        MapBox.drawCerca(3, o.cerca_id, bounds, 0, 0, 0, polyline_options, o.cerca_name, o.description);

						//bounds.push( L.latLng(o.lat, o.lng) );
				}
			}
		});
		var i = 0;
		varMapBox.cercaLayer.eachLayer(function(cerca) { 
			varMapBox.listaIdCerca[i][1] = cerca._leaflet_id;	
			i++;
		});
	},	
	drawCerca: function(type, id, bounds, lat, lng, radius, style, name, desc) {
		var objDraw;
		var content;

		switch(type) {
		    case 1:
				objDraw = L.circle([lat, lng], radius, style).addTo(varMapBox.cercaLayer);
		        break;
		    case 2:
	   		 	objDraw = L.rectangle(bounds, style).addTo(varMapBox.cercaLayer);
		        break;
		    case 3:
	   		 	objDraw = L.polygon(bounds, style).addTo(varMapBox.cercaLayer);   		 	
		}
		
		varMapBox.listaIdCerca.push([id, 0]);

		content = '<b>'+ name + '<\/b><br>' + desc;
		objDraw.bindPopup(content, {
	        closeButton: false,
	        minWidth: 250,
	        className: 'popupPOIclass'
	    });
	},
	loadDrawControl: function(){
  		if(typeof(varMapBox.featureGroupCerca) == 'object') {
      		varMapBox.mapInstance.removeLayer(varMapBox.featureGroupCerca);
      		varMapBox.featureGroupCerca = undefined;
  		}
		if(typeof(varMapBox.drawControlCerca) == 'object') {
			varMapBox.mapInstance.removeControl(varMapBox.drawControlCerca);
			varMapBox.drawControlCerca = undefined;			
		}

		varMapBox.featureGroupCerca = L.featureGroup().addTo(varMapBox.mapInstance);

		varMapBox.drawControlCerca = new L.Control.Draw({
			edit: {
				featureGroup: varMapBox.featureGroupCerca
			},
			draw: {
				polygon: true,
				polyline: false,
				rectangle: true,
				circle: true,
				marker: false
			}
		}).addTo(varMapBox.mapInstance);

		varMapBox.mapInstance.off('draw:created');
		varMapBox.mapInstance.on('draw:created', function(e) {
			varMapBox.featureGroupCerca.addLayer(e.layer);
			varMapBox.drawPolyId = e.layer._leaflet_id;
			
			if(typeof(varCerca) == 'object') {
				widgetscerca.changeDrawColor(varCerca.widgetObj.find('.color').val());
			}
		});
	},
	disabledDrawControl: function (){
		if(typeof(varMapBox) != 'object'){ return false; }
		//remove poligono temporario
		if(typeof(varMapBox.featureGroupCerca) == 'object') {
			varMapBox.mapInstance.removeLayer(varMapBox.featureGroupCerca);
			varMapBox.featureGroupCerca = undefined;
		}
		//remove os controles
		if(typeof(varMapBox.drawControlCerca) == 'object') {
			varMapBox.mapInstance.removeControl(varMapBox.drawControlCerca);
			varMapBox.drawControlCerca = undefined;			
		}
		//libera o marker sobre o mapa				
		if(typeof(varMapBox) == 'object') { 
			MapBox.loadCercaLayer();
		}		
		if(typeof(varCerca) != 'object'){ return false; }
		//retorna a cerca caso as alterações não sejam salvas
		varCerca.widgetObj.find('form').find('.call_map').removeClass('color-stat');
	},
	activeCercaControls: function (){
		if(typeof(varCerca) == 'object'){
			//interação com cadastro de POI
			//Essa interação precisa estar aqui para funcionar. 
			varCerca.widgetObj.find('[data-click=panel-remove]').die("click").live("click", function(){
				if(typeof(MapBox) == 'object'){
					MapBox.disabledDrawControl();
				}
			});
		}
	},
	//#######################################
	centerUnit: function(lat, lng, mark, label, drawMark, zoom){
		if(mark){
			if(zoom == undefined){
	  			varMapBox.mapInstance.setView([lat,lng + 0.01], varMapBox.DefaultZoomGoto);
			}else{
	  			varMapBox.mapInstance.setView([lat,lng + 0.01]);
			}
    		window.setTimeout(function() {

				MapBox.highlightMapBox(1, label, true);

    		}, 500); 
		}else{
			if(zoom == undefined){
	  			varMapBox.mapInstance.setView([lat, lng], varMapBox.DefaultZoomGoto);
			}else{
	  			varMapBox.mapInstance.setView([lat, lng]);
			}	
		}

		if(drawMark != null){
			if(typeof(varMapBox.genericUnitLayer) == 'object'){
	  			varMapBox.mapInstance.removeLayer(varMapBox.genericUnitLayer);
	  		} 	

			varMapBox.genericUnitLayer = new L.MarkerClusterGroup({showCoverageOnHover: false,removeOutsideVisibleBounds: true, animateAddingMarkers:true});
			var marker = L.marker(new L.LatLng(lat, lng), {
			    icon: L.mapbox.marker.icon({
			        'marker-size': drawMark.size,
			        'marker-symbol': drawMark.symbol,
			        'marker-color': drawMark.color
			    })
			});

			varMapBox.genericUnitLayer.addLayer(marker);		
			varMapBox.mapInstance.addLayer(varMapBox.genericUnitLayer);
		}
  	},
	highlightMapBox: function(z, label, bubble) {
		if(typeof(varMapBox.unitClusterLayer) != 'object') {return false;}
		//global
		varMapBox.unitLabelSelected = label;	
		
		var maxZindex = 0;
		var title;
		if(z == 0){
			maxZindex = 0;
		}else{
			varMapBox.unitClusterLayer.eachLayer(function(marker) { 
				if(marker._icon != null){
					if(marker._icon.style.zIndex > maxZindex){
						maxZindex = marker._icon.style.zIndex;
					}
				}
			});
		}
		varMapBox.unitClusterLayer.eachLayer(function(marker) { 
			if(typeof(marker.options.properties) == 'object'){
				
				title = marker.options.properties.title;

				if(marker._icon != null){
					marker._icon.style.border = '';
					marker._icon.style.paddingTop = '';
					marker._icon.style.backgroundColor = '';
					marker._icon.style.width = '0px';
					marker._icon.style.height = '0px';	

					if (title == label){
						marker._icon.style.zIndex = maxZindex + 2;
						marker._icon.style.border = '2px solid rgb(245, 156, 26)';
						marker._icon.style.width = '45px';
						marker._icon.style.height = '45px';
						marker._icon.style.paddingTop = '8px';
						marker._icon.style.paddingLeft = '3px';
						marker._icon.style.borderRadius = '25px';
						marker._icon.style.backgroundColor = '#fff';
						//open bubble
						if(bubble){
							MapBox.setUpBubble(marker, true);
						}
					}
				}
			}
		});
	},
	setUpBubble: function(e, type) {
		$.get("templates/map-bubble.html", function( template ) {
		 	if(type){
			  var marker = e;
			}else{
			  var marker = e.target;
			}

		  	varMapBox.lastMarker = marker;
		  	var properties =  marker.options.properties;
		  	var data = properties.data;

		  	if(typeof(data.clock) == 'undefined'){
				data.clock =  data.local_time.substr(11,8);
				data.date = dateToBr(data.local_time);
				data.local_time = dateToString(data.local_time);  
		  	}
		  	data.time_on_edit = minutesToHMS(data.time_on * 60);
		  	data.time_stop_edit = minutesToHMS(data.time_stop * 60);
		  	data.time_stop_ign_edit = minutesToHMS(data.time_stop_ign * 60);
		  	//offset: left, top
		  	marker.bindPopup(template, {
	            className: 'custom-bubble',
	            offset: [245, 75]
		    });

			marker.openPopup();
			doBubble(data);

			var fullAddress = varMapBox.widgetObj.find('#custom-bubble').find('.mapAddress').html();
			if(fullAddress == ''){
				varMapBox.widgetObj.find('#custom-bubble').find('.mapAddress').html('Carregando...')
				getAddressByCoord(varMapBox.widgetObj.find('#custom-bubble').find('.mapAddress'));
			}

		  	//remove marcação da unidade quando popup do for fechado
			varMapBox.widgetObj.find('.leaflet-popup-close-button').on('click', function(){
				MapBox.highlightMapBox(0, 'false', false);
			});
			varMapBox.mapInstance.on('click', function(){
				MapBox.highlightMapBox(0, 'false', false);
			});
			// navegação no menu de abas laterais do popup das unidades
			varMapBox.widgetObj.find('.bubble-menu-item').on('click', function(){
				var contentAux = $(this).attr('id');

				if($(this).hasClass('path')){
					varMapBox.widgetObj.find('.title.path').css('display', '');
				}else{
					varMapBox.widgetObj.find('.title.path').css('display', 'none');
				}

				varMapBox.widgetObj.find('.bubble-menu-item').each( function () {
					$(this).removeClass('bg-color-on');
				});
				varMapBox.widgetObj.find(this).addClass('bg-color-on');

				varMapBox.widgetObj.find('.bubble-content-item').each( function () {
					$(this).css('display', 'none');
				});
				varMapBox.widgetObj.find('.'+contentAux).css('display', 'block');				
			});

			MapBox.highlightMapBox(1, data.label, false);
			
			varMapBox.widgetObj.find('.clockpicker').clockpicker({
		        autoclose: true,
		        'default': 'now'
		        //, afterDone: function() {
		        //	console.log($("#start_hour").is(":focus"))
		        //	if ($("#start_hour").is(":focus")) {
				//        varMapBox.widgetObj.find('#end_day').focus();
			    //	}
                //}
		    });

		    varMapBox.widgetObj.find('.pluginDate').datepicker({ 
		        autoclose: true, 
		        language: 'pt-BR',
		        format: 'dd/mm/yyyy',
		    }).on('hide', function(e) {

		    	if($(this).attr('id') == 'start_day'){
			        varMapBox.widgetObj.find('#start_hour').focus();
		    		window.setTimeout(function() {
			    		varMapBox.widgetObj.find('#start_hour').trigger('click');
				    }, 50);
		    	}else if($(this).attr('id') == 'end_day'){
			        varMapBox.widgetObj.find('#end_hour').focus();
		    		window.setTimeout(function() {
			    		varMapBox.widgetObj.find('#end_hour').trigger('click');
				    }, 50);
		    	}

		    });

		    varMapBox.widgetObj.find('#periodo').on('change', function () {
		    	if($(this).val() == 5){
		    		varMapBox.widgetObj.find('.filterPeriodo').removeClass('hidden');
		    		varMapBox.widgetObj.find('#start_day').focus();
		    	}else{
	    			varMapBox.widgetObj.find('.filterPeriodo').addClass('hidden');
		    	}
		    }); 

		   	varMapBox.widgetObj.find('form').submit(function(e) { 
				e.preventDefault();

				var date = new Date();

				switch(varMapBox.widgetObj.find('#periodo').val()) {
				    case "1":
						varMapBox.widgetObj.find('#end_day').val(	getDateDif(date, "DD/MM/YYYY", 0, "-")	);
		                varMapBox.widgetObj.find('#end_hour').val(	getDateDif(date, "HH24:MM", 0, "-")		);
						varMapBox.widgetObj.find('#start_day').val(	getDateDif(date, "DD/MM/YYYY", 0, "-")	);
		                varMapBox.widgetObj.find('#start_hour').val(getDateDif(date, "HH24:MM", 1, "-")		);
				        break;
				    case "2":
						varMapBox.widgetObj.find('#end_day').val(	getDateDif(date, "DD/MM/YYYY", 0, "-")	);
		                varMapBox.widgetObj.find('#end_hour').val(	getDateDif(date, "HH24:MM", 0, "-")		);
						varMapBox.widgetObj.find('#start_day').val(	getDateDif(date, "DD/MM/YYYY", 0, "-")	);
		                varMapBox.widgetObj.find('#start_hour').val(getDateDif(date, "HH24:MM", 3, "-")		);
				        break;
				    case "3":
						varMapBox.widgetObj.find('#end_day').val(	getDateDif(date, "DD/MM/YYYY", 0, "-")	);
		                varMapBox.widgetObj.find('#end_hour').val(	getDateDif(date, "HH24:MM", 0, "-")		);
						varMapBox.widgetObj.find('#start_day').val(	getDateDif(date, "DD/MM/YYYY", 0, "-")	);
		                varMapBox.widgetObj.find('#start_hour').val(getDateDif(date, "HH24:MM", 6, "-")		);
				        break;
				    case "4":
						varMapBox.widgetObj.find('#end_day').val(	getDateDif(date, "DD/MM/YYYY", 0, "-")	);
		                varMapBox.widgetObj.find('#end_hour').val(	getDateDif(date, "HH24:MM", 0, "-")		);
						varMapBox.widgetObj.find('#start_day').val(	getDateDif(date, "DD/MM/YYYY", 1, "-")	);
		                varMapBox.widgetObj.find('#start_hour').val(getDateDif(date, "HH24:MM", 0, "-")		);
				        break;
				}

		    	var dataFilter = 
                {
                    unit_id: varMapBox.widgetObj.find('#unit_id').val(),
                    start_day: varMapBox.widgetObj.find('#start_day').val(),
                    end_day: varMapBox.widgetObj.find('#end_day').val(),
                    start_hour: varMapBox.widgetObj.find('#start_hour').val(),
                    end_hour: varMapBox.widgetObj.find('#end_hour').val()
                };
                
                //dd/mm/yyyy
				date1 = new Date(dataFilter.start_day.substring(6, 10) +'/'+ dataFilter.start_day.substring(3, 5) +'/'+ dataFilter.start_day.substring(0, 2) +' '+ dataFilter.start_hour);
				date2 = new Date(dataFilter.end_day.substring(6, 10) +'/'+ dataFilter.end_day.substring(3, 5) +'/'+ dataFilter.end_day.substring(0, 2) +' '+ dataFilter.end_hour);
				
				var diferenca = Math.abs(date1 - date2); //diferença em milésimos e positivo
				var dia = 1000*60*60*24; // milésimos de segundo correspondente a um dia
				//var total = Math.round(diferenca/dia); //valor total de dias arredondado 
				var total = (diferenca / dia) * 24; //valor total de dias arredondado 

				if(total > 24){
		            //Criando mensagem de retorno
		        	createMsgBox('warning', 'Intervalo de busca não pode ser superior a um (1) dia.', '#'+varMapBox.widgetName+' #custom-bubble .bubble-content');  
					return false;
				}

            	dataFilter = JSON.stringify(dataFilter);

				MapBox.handleGetPath(dataFilter);
			});
		});
	},
	handleMapSidebar : function() {
	    "use strict";
	    varMapBox.widgetObj.find('.mapSidebar').removeClass('hidden');
	    varMapBox.widgetObj.find('.mapSidebar .nav > .has-sub > a').die('click').die('click').live('click', function() {
	        var target = $(this).next('.sub-menu');
        	var otherMenu = '.mapSidebar .nav > li.has-sub > .sub-menu';

            $(otherMenu).not(target).slideUp(250, function() {
                $(this).closest('li').removeClass('expand');
            });

            $(target).slideToggle(250, function() {
                var targetLi = $(this).closest('li');
                if ($(targetLi).hasClass('expand')) {
                    $(targetLi).removeClass('expand');
                } else {
                    $(targetLi).addClass('expand');
                }
            });
	    });
	    varMapBox.widgetObj.find('.mapSidebar .nav > .has-sub .sub-menu li.has-sub > a').die('click').die('click').live('click', function() {
            var target = $(this).next('.sub-menu');
            $(target).slideToggle(250);
	    });
	},
	loadGroupUnit : function(data) {
		var content = 	'<ul class="sub-menu">';
        content 	+=	'	<li class="has-sub">';
	    $(data.children).each(function(i, group){

	    	content += '<input type="checkbox" name="group-unit" id="group-unit" class="group-unit" value="group-unit-'+group.id+'" checked>';
	    	content += '<a href="javascript:;">';
            content += '    <b class="caret pull-right"></b>';
            content += '    <b>'+group.text+'</b>';
            content += '</a>';
            content += '<ul class="sub-menu">';

		    $(group.children).each(function(i, subgroup){
		    	content += '<li>';
                content += '	<i class="fa fa-circle color-icon" style="color: '+subgroup.color+';"></i>';
                content += '    <input type="checkbox" name="subgroup-unit" id="subgroup-unit" class="subgroup-unit subgroup-group-unit-'+group.id+'" value="subgroup-unit-'+subgroup.id+'" checked>';
                content += '	<a href="javascript:;">'+subgroup.text+'</a>';
                content += '</i>';
			});

            content += '</ul>';

		});
        content += '</i>';
        content += '</ul>';

		varMapBox.widgetObj.find('li.has-sub.groupUnit').append(content);

		varMapBox.widgetObj.find('.group-unit').on('click', function (){
			MapBox.handleUnitLayerVisibility($(this), 'group');
		});

		varMapBox.widgetObj.find('.subgroup-unit').on('click', function (){
			MapBox.handleUnitLayerVisibility($(this), 'subgroup');
		});
	},
	loadGroupCerca : function(data) {
		var content = 	'<ul class="sub-menu">';

	    $(data.children).each(function(i, group){

	    	content += '<li>';
            content += '    <input type="checkbox" name="group-cerca" id="group-cerca" class="group-cerca" value="group-cerca-'+group.id+'" checked><a href="javascript:;"><b>'+group.text+'</b></a>';
            content += '</i>';

		});
        content += '</ul>';

		varMapBox.widgetObj.find('li.has-sub.groupCerca').append(content);

		varMapBox.widgetObj.find('.group-cerca').on('click', function (){
			MapBox.handleCercaLayerVisibility($(this));
		});
	},
	loadGroupPOI : function(data) {
		var content = 	'<ul class="sub-menu">';

	    $(data.children).each(function(i, group){

	    	content += '<li>';
            content += '    <input type="checkbox" name="group-poi" id="group-poi" class="group-poi" value="group-poi-'+group.id+'" checked><a href="javascript:;"><b>'+group.text+'</b></a>';
            content += '</i>';

		});
        content += '</ul>';

		varMapBox.widgetObj.find('li.has-sub.groupPOI').append(content);

		varMapBox.widgetObj.find('.group-poi').on('click', function (){
			MapBox.handlePOILayerVisibility($(this));
		});
	},
	handleCercaLayerVisibility : function (obj){
		// var objName = '.'+obj.val();
		// if(obj.attr('checked') == 'checked'){
		// 	varMapBox.widgetObj.find(objName).css('display', '');
		// }else{
		// 	varMapBox.widgetObj.find(objName).css('display', 'none');
		// }
		var objName = obj.val();
		if(obj.attr('checked') == 'checked'){
			var index = varMapBox.cercaDataListShow.indexOf(objName);
			if (index > -1) {
				varMapBox.cercaDataListShow.splice(index, 1);
			}
		}else{
			varMapBox.cercaDataListShow.push(objName);
		}

		MapBox.drawCercaLayer();
	},
	handlePOILayerVisibility : function (obj){
		var objName = obj.val();
		if(obj.attr('checked') == 'checked'){
			var index = varMapBox.POIDataListShow.indexOf(objName);
			if (index > -1) {
				varMapBox.POIDataListShow.splice(index, 1);
			}
		}else{
			varMapBox.POIDataListShow.push(objName);
		}

		MapBox.drawPOILayer();
	},
	handleUnitLayerVisibility : function (obj, type){
		var objName = obj.val();
		var objNameSub = '.subgroup-' + obj.val();
		if(obj.attr('checked') == 'checked'){
			var index;			
			switch(type) {
			    case 'group':
			    	index = varMapBox.UnitGroupDataListShow.indexOf(objName);
			    	if (index > -1) {
						varMapBox.UnitGroupDataListShow.splice(index, 1);
						varMapBox.widgetObj.find(objNameSub).attr('checked', 'checked');
					}
			        break;
			    case 'subgroup':
			    	index = varMapBox.UnitSubGroupDataListShow.indexOf(objName);
			    	if (index > -1) {
						varMapBox.UnitSubGroupDataListShow.splice(index, 1);
					}
			}
		}else{
			switch(type) {
			    case 'group':
					varMapBox.UnitGroupDataListShow.push(objName);
					varMapBox.widgetObj.find(objNameSub).removeAttr('checked');
			        break;
			    case 'subgroup':
					varMapBox.UnitSubGroupDataListShow.push(objName);
			}
		}

		MapBox.loadClusteredUnitsLayer();
	},
	handleGetPath : function (dataFilter){
		$.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'map/getMapPath/', data: JSON.parse(dataFilter) },
            dataType: 'json',
            beforeSend: function () {
            	loadingWidgetSpec('#'+varMapBox.widgetName + ' #custom-bubble .bubble-content');
            },
            success: function(data) {
            	loadingWidgetSpec('#'+varMapBox.widgetName + ' #custom-bubble .bubble-content', true);
                //para de processar caso não exista widget seja fechado.
                if(typeof(varMapBox) != 'object'){ return false; }
		  		if(typeof(varMapBox.mapPathLayer) == 'object') {
		      		varMapBox.mapInstance.removeLayer(varMapBox.mapPathLayer);
		      		varMapBox.mapInstance.removeLayer(varMapBox.mapPathPOILayer);
		  		}

		  		varMapBox.mapPathPOILayer = L.mapbox.featureLayer().addTo(varMapBox.mapInstance);
		  		var iconhtmlstack;
		  		var direction = 'fm-arrow';
		  		var markerPathSize = 22;
		  		var markerPathColor = '#F16430';
		  		var walking = true; //evita criar marcadores quando a unidade está parada
		  		var secondaryPOICount = 0;
		  		var secondaryPOIMax = Math.round(data.length / 20);
		  		var secondaryPOIControler;
                // Create array of lat,lon points.
				var polyline_options = {
				    color: '#FF2400'
				};
				var line_points = [];
				var markerId = 0;
				//var line_pointsLngLat = [];
				varMapBox.pathMarkerList = [];
				$(data).each(function(i, point){
					if((walking) || (point.velocidade > 0)){
						line_points.push([parseFloat(point.latitude), parseFloat(point.longitude)]);
						//line_pointsLngLat.push([parseFloat(point.longitude), parseFloat(point.latitude)]);

						//if(point.velocidade == 0){
						if(point.data_inicio != null){
							direction = 'fm-stop';
							walking = false;
		  					markerPathSize = 30;
		  					markerPathColor = '#F16430';
						}else{
							direction = 'fm-arrow' + getUnitIconRotation(point.direction);
							walking = true;
		  					markerPathSize = 22;
		  					markerPathColor = '#00ACAC';
						}

						if((direction == 'fm-stop') && (point.data_inicio == null)){
							//não desenha parada não consolidada
						}else{
							iconhtmlstack = '<span class="fm-stack fm-lg">';
							iconhtmlstack += '	<i class="fm fm-circle-o fm-stack-1x" style="color: '+markerPathColor+'; font-size: '+markerPathSize+'px;"></i>';
							iconhtmlstack += '	<i class="fm fm-circle-b fm-stack-1x" style="font-size: '+markerPathSize+'px; color: #fff;"></i>';
							iconhtmlstack += '	<i class="fm '+direction+' fm-stack-1x" style="font-size: '+markerPathSize+'px; color: #fff;"></i>';
							iconhtmlstack += '</span>';
							//fm-stop fm-arrow fm-rotate-135
							secondaryPOIControler = ' secondaryPathPOI ';
							if((secondaryPOIMax == secondaryPOICount) || (markerId == 0)  || (markerId == data.length) ){
								secondaryPOIControler = '';
								secondaryPOICount = 0;
							}
							//pontos de parada não são escondidos de acordo com o nivel de zoom
							if(point.data_inicio != null){
								secondaryPOIControler = '';
							}
							//montando ponto da rota
							var marker = L.marker(new L.LatLng(point.latitude,point.longitude), {
								icon: L.divIcon({
									iconAnchor: [15,15],
									className: 'unit_poi_marker'+secondaryPOIControler,
									html: iconhtmlstack
								}),
								properties : {
									title : point.poi_name,
									'data' : point
								}
					        });

							var content = "<b>"+ point.label+"<\/b><br>";
							content += "<b>Data:<\/b> "+point.data+" às "+point.hour+"<br>";
							content += "<b>Velocidade:<\/b> "+point.velocidade+" km/h<br>";
							content += "<b>Local<\/b>";
							if(point.full_address != '') {
	                            //se sim ja retorna o endereco
	                            content += '<div>' + point.full_address + '</div>';
	                        } else {
	                            //se nao faz o reversegeo para capturar o endereco e salvar no banco
	                            content += "<div class='mapAddress' hist='"+point.devst_id+"' long='"+point.longitude+"' lat='"+point.latitude+"'>Carregando...</div>";
	                        }
	                        //se o ponto for do tipo parada, verifica dados de consolidação da parada
	                        if(direction == 'fm-stop'){  
					        	var contentStop = 'Nenhum dado encontrado.';
					        	var dt_fim = "--";
					        	var tt = "--";

					        	if(point.data_inicio != null){
					        		contentStop = '';
						        	//atualiza tabela de origem
						        	contentStop += "<b>Início da parada:</b> "+point.data_inicio+" às "+point.hora_min_inicio+"<br>";
						        	if(point.data_fim != null){
						        		dt_fim = point.data_fim+" às "+point.hora_min_fim;
						        	}
						        	contentStop += "<b>Final da parada:</b> "+dt_fim+"<br>";
						        	if(point.total_time != ''){
						        		tt = minutesToHMS(point.total_time);
						        	}
						        	contentStop += "<b>Tempo parado:</b> "+tt;
						        }	        
	                        	content += "<div class='conStopData border'>"+contentStop+"</div>";
	                        }
	                        content += "<a href='javascript:;' onClick='MapBox.navegatePath("+markerId+", \"prev\");' >Anterior</a>";
	                        content += "<a href='javascript:;' onClick='MapBox.navegatePath("+markerId+", \"next\");' style='margin-left: 10px;'>Próximo</a>";
							marker.bindPopup(content,{
						        closeButton: false,
						        minWidth: 250,
						        className: 'popupPOIclass'
						    });

						    varMapBox.pathMarkerList.push(marker);
						    markerId++;
						    secondaryPOICount++;
							varMapBox.mapPathPOILayer.addLayer(marker);
						}

					}
					if(point.velocidade == 0){
						walking = false;
					}else{
						walking = true;
					}
				});

				varMapBox.widgetObj.find('.unit_poi_marker').on('click', function (){
					if( varMapBox.widgetObj.find('.mapAddress').length == 1 ){
						getAddressByCoord(varMapBox.widgetObj.find('.mapAddress'));
					}
				});

				varMapBox.mapInstance.addLayer(varMapBox.mapPathPOILayer);
				varMapBox.widgetObj.find('.removePath').removeClass('hidden');
				
				/*
				console.log(line_pointsLngLat);
				var line_pointsStr = line_pointsLngLat.join(';');
				$.ajax({ 
                	url: 'https://api.mapbox.com/v4/directions/mapbox.driving/'+line_pointsStr+'.json?alternatives=false&instructions=html&geometry=polyline&steps=true&&access_token='+varMapBox.mapKey,
	            }).done(function(rota){
	            	console.log(rota);
	                line_points = [];
	                var lat;
	                var lng;
	                //$(rota.routes[0].steps).each(function(i, steps){
	                $(rota.waypoints).each(function(i, steps){
	                	//lat = steps.maneuver.location.coordinates[0];
	                	//lng = steps.maneuver.location.coordinates[1];
	                	lat = steps.geometry.coordinates[1];
	                	lng = steps.geometry.coordinates[0];
	                	line_points.push([lat, lng])
	                });

	                varMapBox.mapPathLayer = L.polyline(line_points, polyline_options).addTo(varMapBox.mapInstance);
					varMapBox.lastMarker.closePopup(); //fechando o popup da unidade
					MapBox.highlightMapBox(); //fechando o popup da unidade
					varMapBox.mapInstance.fitBounds(varMapBox.mapPathLayer.getBounds());
            	});
				*/

				varMapBox.mapPathLayer = L.polyline(line_points, polyline_options).addTo(varMapBox.mapInstance);
				varMapBox.lastMarker.closePopup(); //fechando o popup da unidade
				MapBox.highlightMapBox(); //fechando o popup da unidade
				varMapBox.mapInstance.fitBounds(varMapBox.mapPathLayer.getBounds());

				if(varMapBox.mapInstance.getZoom() >= 15){
					varMapBox.widgetObj.find('.secondaryPathPOI').removeClass('hidden');
				}else{
					varMapBox.widgetObj.find('.secondaryPathPOI').addClass('hidden');
				}
				varMapBox.mapInstance.on('zoomend', function() {
					if(varMapBox.mapInstance.getZoom() >= 15){
						varMapBox.widgetObj.find('.secondaryPathPOI').removeClass('hidden');
					}else{
						varMapBox.widgetObj.find('.secondaryPathPOI').addClass('hidden');
					}
				});	
        	},
            error: function(error) {
                loadingWidgetSpec('#'+varMapBox.widgetName + ' #custom-bubble .bubble-content', true);
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText, '#'+varMapBox.widgetName+' #custom-bubble .bubble-content');
            }
        });
        return false;
	},
	navegatePath : function (current, nav){
		var id = current;
		if(nav == 'next'){
			id++;
		}else{
			id--;
		}

		if(varMapBox.pathMarkerList[id] == undefined){ return false; }

		MapBox.centerUnit(varMapBox.pathMarkerList[id]._latlng.lat, varMapBox.pathMarkerList[id]._latlng.lng, false, undefined, undefined, 'none');
		varMapBox.pathMarkerList[id].openPopup();
		if( varMapBox.widgetObj.find('.mapAddress').length == 1 ){
			getAddressByCoord(varMapBox.widgetObj.find('.mapAddress'));
		}
	}
};

function load_widgetsmapbox_Page(){
	$.ajax({
		type : 'POST',
		url : 'widgets/routing.php',
		data : {
			url : 'map/loadGroups/',				
		},
		dataType : 'json',
		success : function(json) {
			MapBox.load();
			MapBox.activePOIControls();
			MapBox.activeCercaControls();

			MapBox.loadGroupUnit(JSON.parse(json.groupinfo));
			MapBox.loadGroupCerca(JSON.parse(json.groupinfo));
			MapBox.loadGroupPOI(JSON.parse(json.groupinfo));

			varMapBox.widgetObj.find('.mapstyle').on('click', function (){
				varMapBox.mapStyle 			= $(this).val();
				var coords = varMapBox.mapInstance.getCenter();				
				varMapBox.DefaultLatitude 	= coords.lat;
				varMapBox.DefaultLongitude 	= coords.lng;
				varMapBox.DefaultZoomIni 	= varMapBox.mapInstance._zoom;

				MapBox.load();
			});	

			varMapBox.widgetObj.find('.removePath').on('click', function (){
				if(typeof(varMapBox) != 'object'){ return false; }
		  		if(typeof(varMapBox.mapPathLayer) == 'object') {
		      		varMapBox.mapInstance.removeLayer(varMapBox.mapPathLayer);
		      		varMapBox.mapInstance.removeLayer(varMapBox.mapPathPOILayer);
		  		}
		  		$(this).addClass('hidden');
			});

			varMapBox.widgetObj.find('.primary-checkbox').on('click', function (){
				var type = $(this).val();
				var childrenName = '.group-' + type;
				var childrenNameSubGroup = '.subgroup-' + type;
				if($(this).attr('checked') == 'checked'){
					varMapBox.widgetObj.find(childrenName).attr('checked', 'checked');
					varMapBox.widgetObj.find(childrenNameSubGroup).attr('checked', 'checked');
				}else{
					varMapBox.widgetObj.find(childrenName).removeAttr('checked');
					varMapBox.widgetObj.find(childrenNameSubGroup).removeAttr('checked');
					switch(type) {
					    case 'unit':
							varMapBox.UnitGroupDataListShow = [];
							varMapBox.UnitSubGroupDataListShow = [];
					        break;
					    case 'poi':
							varMapBox.POIDataListShow = [];
					    case 'cerca':
							varMapBox.cercaDataListShow = [];
					}
				}
				switch(type) {
				    case 'unit':
				        MapBox.loadClusteredUnitsLayer();
				        break;
				    case 'poi':
				        MapBox.drawPOILayer();
				        break;
				    case 'cerca':
				    	MapBox.drawCercaLayer();
						//MapBox.handleCercaLayerVisibility($(this));
				}
			});	
		},			
		error : function(error) {
            //Criando mensagem de retorno
        	createMsgBox('error', error.responseText,'#'+varMapBox.widgetName+' .panel-body');  
		}
	});
}

$(document).ready(function() {
	load_widgetsmapbox_Page();	

	varMapBox.widgetObj.find("#openmapSidebar").click(function () {
		if(varMapBox.widgetObj.find("#mapSidebar").width() == 0){
		    varMapBox.widgetObj.find("#mapSidebar").animate({
		    	width: varMapBox.sideMenuSize
		    }).promise().done(function () {
		    	varMapBox.widgetObj.find('#mapSidebarContent').removeClass('hidden');
		    	varMapBox.widgetObj.find('#mapSidebar').addClass('mapSidebarPadding');
		    });
		}else{
		    varMapBox.widgetObj.find("#mapSidebar").animate({
		    	width: 0
		    }).promise().done(function () {
		    	varMapBox.widgetObj.find('#mapSidebarContent').addClass('hidden');
		    	varMapBox.widgetObj.find('#mapSidebar').removeClass('mapSidebarPadding');
		    });				
		}
	});
});
