// Google Analytics
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71274760-1', 'auto');
  ga('send', 'pageview');


/**
 * Mostra loading
 * @param widget string, passando o nome do widget que está envolvido
 * @param end boolean, informa se está finalizando o loading, padrão: false
 */
function loadingWidget(widget, end) {
	end = end || false;
	if(end==false)
	{
	    if (!$(widget).hasClass('panel-loading')) {
	        var targetBody = $(widget).find('.panel-body');
	        var spinnerHtml = '<div class="panel-loader"><span class="spinner-small"></span></div>';
	        $(widget).addClass('panel-loading');
	        $(targetBody).prepend(spinnerHtml);
			$(widget).find('.btn').attr('disabled', 'disabled');
	    }
	} else {
		$(widget).removeClass('panel-loading');
		$(widget).find('.panel-loader').remove();
		$(widget).find('.btn').removeAttr("disabled");
	}
    $(widget).find('.btn-danger').removeAttr("disabled");
}
function loadingWidgetSpec(widget, end) {
	end = end || false;
	if(end==false)
	{
	    if (!$(widget).hasClass('panel-loading')) {
	        var targetBody = $(widget);
	        var spinnerHtml = '<div class="panel-loader"><span class="spinner-small"></span></div>';
	        $(widget).addClass('panel-loading');
	        $(targetBody).prepend(spinnerHtml);
			$(widget).find('.btn').attr('disabled', 'disabled');
	    }
	} else {
		$(widget).removeClass('panel-loading');
		$(widget).find('.panel-loader').remove();
		$(widget).find('.btn').removeAttr("disabled");
	}
    $(widget).find('.btn-danger').removeAttr("disabled");
}
/**
 * Funcao que cria o spinner (loader) ao submitar um formulário
 * @param widget string nome do widget ('account')
 * @param form string class do form a ser editado ('add')
 * @param start boolean true (está iniciando/abrindo o spinner) or false (está fechando o spinner)
 */
function waitResponse(widget, start) {
	//Se parametro start não for definido será true
	if(start!=false)
	{
		start = true;
	}	
	if(start === true) {
	//Criando botão loading e excluindo buttons ('salvar' e 'cancelar')
		 $('#'+widget).find('form').find('button').hide();
		 $('#'+widget).find('form').find('.action-buttons').prepend('<div class="p-10 m-t-10 loader"><span class="spinner-small" style="position: inherit; display: inline-block;"></span></div>');
		 $('#'+widget).find('form').find('.link-delete').hide();
	} else {
	//Excluindo botão loading e voltando buttons ('salvar' e 'cancelar')
		 $('#'+widget).find('form').find('.loader').remove();
		 $('#'+widget).find('form').find('button').show();
		 $('#'+widget).find('form').find('.link-delete').show();
	}
    $('#'+widget).find('.btn-danger').removeAttr("disabled");
}
//CRIANDO DIV PARA MENSAGEM DE SUCESSO / ERRO / INFORMAÇÃO / ATENÇÃO
function createMsgBox(type, msg, parent){ 
	// Sai do sistema se não ouver sessão ativa para o usuário
	if(msg == '-0904'){
		window.location = "../login/logout.php";
	}
    var elem = "<div class='alert hidden m-b-15 MsgBox' id='alertMsg'>" +
    			"	<strong class='title'></strong>"+
    			"	<span class='close' data-dismiss='alert'>&times;</span>"+
				"	<div class='text' style='display: inline-block;'></div>"+
				"</div>";
    type = type.toLowerCase();
	var title = '';

	//Vê o tipo da mensagem
	if(type == 'success'){
		title = 'Sucesso! ';
	} else if(type == 'error'){
		title = 'Erro! ';
		type = 'danger';
	} else if(type == 'warning'){
		title = 'Atenção! ';
	} else if(type == 'info'){
		title = 'Informe! ';
	} else {
		type = '';
	}

	$(parent).append(elem);
	var alert = $(parent).find("#alertMsg");
	var alertClass = 'alert-'+type;
	alert.addClass('animated fadeInDown');
	alert.addClass(alertClass);
	alert.removeClass('hidden');
	alert.find('.title').html(title);
	alert.find('.text').html(msg);	
	
    // $('html, body').animate({
    //     scrollTop: alert.offset().top - 40
    // }, 500);
	
	//Mensagem terá duração de 5 segundos
	setTimeout(function() {
		alert.removeClass(alertClass);
		alert.removeClass('animated fadeInDown');
		alert.addClass('animated fadeOutDown');
		alert.find('.text').html('');
    	//Limpando a mensagem
        setTimeout(function(){
        	alert.remove();
        }, 800);
  	}, 5000);
}
//Adiciona cor de fundo ao elemento panel-body (container do conteudo), quando um form do widget e aberto
function widgetFadeOut(widget) {
	$(widget+" .panel-body").append('<div class="ui-widget-overlay ui-front" style="z-index: 900; position: absolute;"></div>');
}
// Remove cor de fundo do elemento panel-body (container do conteudo), quando um form do widget e fechado
function widgetFadeIn(widget) {
	$(widget+" .panel-body").find('div.ui-widget-overlay').remove();
}
//Botão cancelar dentro do form
//Ao clicar pega o form que é pai do botao e realiza a função unmakeForm para fecha-lo 
function loadButtonControl(){
	
    $(':button').bind('click', function(){
        var widget, form = $(this).closest('form');
        
        if($(this).hasClass('cancel')){
            widget = $(this).closest('.panel');
            widget = $(widget).attr("id");
            widget = "#"+widget;
            
            widgetFadeIn(widget);   
            unmakeForm(widget);
        } 
    });
}

function customMenu(node) {
	var form = this.element.context;
	var items = {
		createRoot: {
			label: "Criar Grupo",
			icon: "fa fa-1x fa-folder-o",
			action: function (data) {
				var inst = $.jstree.reference(data.reference),
					obj = inst.get_node(data.reference);
				inst.create_node('#', {}, "last", function (new_node) {
					new_node.data = {file: false}; //Referenciando nó como pai(grupo)
					new_node.text= 'Grupo'; //Nomeando nó de grupo
					new_node.state.opened = true; //Deixando nó aberto
					setTimeout(function () { inst.edit(new_node); },0);
					//Criando automaticamente um subgrupo
					inst.create_node(new_node, {}, "last", function (new_node) {
						new_node.data = {file: true};
						new_node.text= 'Subgrupo';
					});
				});
			}
		},
		createItem: {
			label: "Criar Subgrupo",
			icon: "fa fa-1x fa-folder-open-o",
			action: function (data) {
				var inst = $.jstree.reference(data.reference),
					obj = inst.get_node(data.reference);
				inst.create_node(obj, {}, "last", function (new_node) {
					new_node.data = {file: true}; //Referenciando nó como filho(subgrupo)
					new_node.text= 'Subgrupo';
					setTimeout(function () { inst.edit(new_node); },0);
				});
			}
		},
		renameItem: {
			label: "Renomear",
			icon: "fa fa-1x fa-edit",
			action: function (data) {
				var inst = $.jstree.reference(data.reference),
					obj = inst.get_node(data.reference);
				inst.edit(obj);
			}
		},
		deleteItem: {
			label: "Deletar",
			icon: "fa fa-1x fa-times",
			action: function (data) {
				var inst = $.jstree.reference(data.reference),
					obj = inst.get_node(data.reference);
				
				//Verifica em que form está o click e se o grupo clicado foi gerado na hora ou está no banco
				if(($(form).closest('form').hasClass('add')) || (!obj.original.id) ) {
	        		//Deletando grupo da árvore
					if(inst.is_selected(obj)) {
						inst.delete_node(inst.get_selected());
					}
					else {
						inst.delete_node(obj);
					}
				} else {					
					//Verifica se é subgroup
					if(obj.data.file == true) {
			    		var param = 'subgroup';
			    	} else {
			    		var param = 'group';
			    	}
					
					var data = 
						{
							id: obj.id,
							group: param
						};
					
					data = JSON.stringify(data);
					
					var spinner = '<div class="panel-loader" style="position: absolute;width: 100%;height: 100%;background-color: white;"><span class="spinner-small" style="top: 80px;"></span></div>';
					
			    	$.ajax({
				        type : "POST",
				        data: { url: 'client/checkDep', data: obj.id },
				        url : 'widgets/routing.php',
		    	        dataType: 'text',
		    	        beforeSend: function() {
		    	        	$(form).prepend(spinner)
		    	        },
				        success : function(resp) {
				        	if(resp == '0') {
				        		if(confirm('Este grupo há unidades existentes ativas. Deseja realmente excluir? Deletando fará com que delete estas unidades.')) {
					        		$.ajax({
								        type : "POST",
								        data: { url: 'client/delete', data: JSON.parse(data) },
								        url : 'widgets/routing.php',
								        success : function(resp) {
								        		//Deletando grupo da árvore
												if(inst.is_selected(obj)) {
													inst.delete_node(inst.get_selected());
												}
												else {
													inst.delete_node(obj);
												}
							    	        	createMsgBox('success', 'Grupo deletado com sucesso.', '#widgetsaccount .panel-body'); //Mensagem de sucesso
												$(form).find('.panel-loader').remove();

								    	        reloadGroupTree(); //atualizando lista de grupos nos widgets abertos que necessitarem
							        	},
								        error : function(xhr, ajaxOptions, thrownError) {
							    	        createMsgBox('error', 'Erro ao tentar excluir Grupo.', '#widgetsaccount .panel-body'); //Mensagem de erro
											$(form).find('.panel-loader').remove();
								        }
							    	});
				        		}
				        	} else {
				        		$.ajax({
							        type : "POST",
							        data: { url: 'client/delete', data: JSON.parse(data) },
							        url : 'widgets/routing.php',
							        success : function(resp) {
							        		//Deletando grupo da árvore
											if(inst.is_selected(obj)) {
												inst.delete_node(inst.get_selected());
											}
											else {
												inst.delete_node(obj);
											}
						    	        	createMsgBox('success', 'Grupo deletado com sucesso.', '#widgetsaccount .panel-body'); //Mensagem de sucesso
											$(form).find('.panel-loader').remove();

							    	        reloadGroupTree(); //atualizando lista de grupos nos widgets abertos que necessitarem
						        	},
							        error : function(xhr, ajaxOptions, thrownError) {
						    	        createMsgBox('error', 'Erro ao tentar excluir Grupo.', '#widgetsaccount .panel-body'); //Mensagem de erro
										$(form).find('.panel-loader').remove();
							        }
						    	});
				        	}
			        	},
				        error : function(xhr, ajaxOptions, thrownError) {
			    	        createMsgBox('error', 'Erro ao tentar excluir Grupo.', '#widgetsaccount .panel-body'); //Mensagem de erro
				        }
			    	});
				}			    	
			}
		}
	};
	
	//Verifica se existe irmão de grupos para validar delete.
	var masterParent = $(form).jstree('get_node', node.parent);
	var masterbrothers = masterParent.children.length;
	if(masterbrothers < 2) {
		delete items.deleteItem;
	}
	
	//Verifica se nó é submenu
	if (node.data.file) {
		//Caso seja exclui a possibilidade de criar "Sub-Sub-Grupo"
		delete items.createItem;
		delete items.createRoot;
		
		//Verifica se o submenu tem irmãos
		//Caso não tenha impossibilita a exclusão do subgrupo
		var parent = $(form).jstree('get_node', node.parent);
		var brothers = parent.children.length;
		if(brothers < 2) {
			delete items.deleteItem;
		}
	}
	
	return items;
}
function reloadGroupTree() {
	var unit = $("#widgetsunit select.selectGroup");
	var user = $("#widgetsuser .groupTree");
	
	if(user.length > 0) {
		$.ajax({
			type : 'POST',
			url : 'widgets/routing.php',
			data : { url : 'client/listTreeAccLog/' },
			async: true,
			success: function(tree) {
				user.jstree('destroy');
				createJSTree(JSON.parse(tree), '#widgetsuser', '.groupTree', false);
			}
		});
	}
	
	if(unit.length > 0) {
		var backupOptions = unit.html();
		$.ajax({
			type : 'POST',
			url : 'widgets/routing.php',
			data : { url : 'unit/listGroup/' },
			async: true,
			beforeSend: function() {
				unit.html('<option>Atualizando...</option>');
			},
			success: function(data) {
	        	var html = '';
	        	var attOptions = JSON.parse(data);
	        	
	        	$(attOptions).each(function(i, v) {
	        		html += '<option value="'+v.id+'">'+v.name+'</option>';
	        	});
	        	
	        	unit.html(html);
            	//verifica se no widget unit esta aberto form edit de alguma unidade
            	//caso esteja ele atualiza o select do subgrupo tambem
        		if($('#widgetsunit form.edit.animated')) {
        			loadSubGroup($("#edit_group").val(), "#edit_subgroup");
        		}
			},
			error: function() {
				unit.html(backupOptions);
			}
		});
	}
}
//Recebe o form que é para ser fechado
function unmakeForm(widgetName) { 
	var form = $('#'+widgetName).find('form');
	form.closest(".panel").find(".btn-group button").removeAttr("disabled")
    //Reseta o form (Limpa os erros/acertos)
    //form.parsley().reset();
	
    //Reseta o form (Limpa os campos)
    form[0].reset();
    
    //Adiciona efeitos animados ao fecha-lo
    form.removeClass('animated fadeIn').addClass('animated fadeOut');
    //Remove a class permError da arvore de permissoes
    form.find('.permTree').removeClass('permError');
    //Remove a mensagem de erro caso ela tenha sido adicionada esvaziando o elemento
    form.find('.parsley-required').html('');
    

	form.find('.permTree').jstree('uncheck_all');
	form.find('.permTree').jstree('close_all');
	form.find('.accordion').accordion({ collapsible: true, active: false });
    
    //8ms de espera para realmente escondê-lo (hidden)
    //Apaga a class animated fadeOut para poder funcionar a animação novamente
    setTimeout(function(){
    	//form.addClass('hidden');
    	//form.removeClass('animated fadeOut');
    	closeForm(widgetName);
    }, 800);
    return false;
}

function openModalWidget(widget, msg, btnYes, btnNo) {
	var content = 	"<div name='confirm_shadow' id='confirm_shadow' class='confirm_shadow'>" +	
					"</div>" +
					"<div name='confirm' id='confirm' class='confirm veiculosAuthModal'>" +
					"	<form class='veiculosAuthModal'>" +
					"		<div class='col-md-12 col-sm-12 m-b-3'>" +
					"			<label class='confirm-msg'>" + msg + "</label>" +
					"		</div>"	+
					"		<div class='form-group col-md-12 col-sm-12 col-xs-12 text-right p-t-5 action-buttons'>" +
					"			<button type='button' class='p-10 btn btn-white m-r-2 btn-sm salvarVeiculos'>" + btnYes + "</button>" +
					"			<button type='button' class='p-10 btn btn-primary m-r-2 btn-sm confirm_no'>" + btnNo + "</button>" +
					"		</div>" +
					"	</form>" +
					"</div>"; 
    $('#'+widget).find('.panel-body').prepend(content);
    $('#duallistbox').DualListBox({
    	'json': false,
    	'title': "teste"
    });
}
// CONTROLE DE MSGbOX DE CONFIRMAÇÃO DE AÇÃO
function openConfirm(widget, title, msg, btnYesLabel, btnNoLabel, btnYes, btnNo) {
	var yes = "confirm_yes";
	var no = "confirm_no";

	if((btnYes != undefined) && (btnYes != '')){
		yes = btnYes;
		no = btnNo;
	}
	var content = 	"<div name='confirm_shadow' id='confirm_shadow' class='confirm_shadow'>" +	
					"</div>" +
					"<div name='confirm' id='confirm' class='confirm'>" +
					"	<form>" +
					"		<div class='col-md-12 col-sm-12 m-b-3'>" +
					"			<h5><label class='confirm-title'>" + title + "</label></h5>" +
					"		</div>"	+
					"		<div class='col-md-12 col-sm-12 m-b-3'>" +
					"			<label class='confirm-line'></label>" +
					"		</div>"	+
					"		<div class='col-md-12 col-sm-12 m-b-3'>" +
					"			<label class='confirm-msg'>" + msg + "</label>" +
					"		</div>"	+
					"		<div class='form-group col-md-12 col-sm-12 col-xs-12 text-right p-t-5 action-buttons'>" +
					"			<button type='button' class='p-10 btn btn-white m-r-2 btn-sm " + yes + "'>" + btnYesLabel + "</button>" +
					"			<button type='button' class='p-10 btn btn-primary m-r-2 btn-sm " + no + "'>" + btnNoLabel + "</button>" +
					"		</div>" +
					"	</form>" +
					"</div>"; 

	var confirmShadowH = $('#'+widget).find('.panel-body').height();
    $('#'+widget).find('.panel-body').prepend(content);	
    $('#'+widget).find('.panel-body').find('.confirm_shadow').height(confirmShadowH);
    $('html, body').animate({
        scrollTop: $('#'+widget).find("#confirm").offset().top - 40
    }, 500);
}
// METODO PARA FECHAMENTO DA DIV DE CONFIRMAÇÃO.
function closeConfirm(widget) {
	
	$('#'+widget).find('.confirm_shadow').remove();
	$('#'+widget).find('.confirm').remove();
    
    return false;	
}
/**
 * Manipula as abas de acao definindo a ativa
 * de acordo com a tab selecionada pelo usuario
 * @param event evt
 */
function changeActiveMenu(evt) {
	//Id da tab
	var widget = evt.target.id;
	//Id do elemento pai mais externo (widget)
	var parent = evt.target.parentElement.parentElement.id;
	
	//Substitui parte do id do elemento mais externo por parte do id do menu de acoes
	parent = parent.replace("-nav","-dropdown li");
	
	disableTabs(widget,"#"+parent);
}	
/**
 * 
 * Verifica entre os elementos do menu de acoes o equivalente a tab selecionada
 * e desativa todos os outros
 * @param active, tab ativa selecionada pelo usuario
 * @param parent, representa todo o widget
 */
function disableTabs(active,parent){
	
	active = "menu_"+active;
	
	$(parent).each(function(id,obj){
		
		child = $(this).children().attr("id");

		if(child !== active){
			$("#"+child).css("display","none");
		}
		
		$("#"+active).css("display","block");		
	});
}

function selectIcons(form, iconsArray, qtd) {
	
	var icons = [];
	iconsArray = JSON.parse(iconsArray);
	$.each(iconsArray, function(index, val) {
		icons.push(val.icon);
	});        	
	$(form).find('select.icon').fontIconPicker({
		hasSearch: false,
		emptyIcon: false,
		source: icons,
		theme: 'fip-bootstrap',
		iconsPerPage: qtd,
	});
}
//Recebe o widget aonde abrirá o form
function openForm(widgetName, action){ 
	var widgetObj = $('#'+widgetName);

	widgetObj.find('form').removeClass('hidden');
    widgetObj.find('form').removeClass('animated fadeOut');
	widgetObj.find('#create-table').addClass('hidden');
	widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
	//configurando formulário de acordo com a ação do cadastro
	if(action == 'add'){
		widgetObj.find('.edit').addClass('hidden');
		widgetObj.find('.add').removeClass('hidden');
		widgetObj.find('#id').val('');
	}else{
		widgetObj.find('.add').addClass('hidden');
		widgetObj.find('.edit').removeClass('hidden');		
	}
}
//Recebe o widget aonde abrirá o form
function closeForm(widgetName){ 
	var widgetObj = $('#'+widgetName);

	widgetObj.find('form').addClass('hidden');
    widgetObj.find('form').addClass('animated fadeOut');
	widgetObj.find('#create-table').removeClass('hidden');
	widgetObj.find('.dropdown-toggle').removeAttr("disabled");
}
//Recebe o widget aonde abrirá o form
function openVisualize(widgetName){ 
	var widgetObj = $('#'+widgetName);

	widgetObj.find('.visualize-box').removeClass('hidden');
    widgetObj.find('.visualize-box').removeClass('animated fadeOut');
	widgetObj.find('#create-table').addClass('hidden');
	widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
}
//Recebe o widget aonde abrirá o form
function closeVisualize(widgetName){ 
	var widgetObj = $('#'+widgetName);

	widgetObj.find('.visualize-box').addClass('hidden');
    widgetObj.find('.visualize-box').addClass('animated fadeOut');
	widgetObj.find('#create-table').removeClass('hidden');
	widgetObj.find('.dropdown-toggle').removeAttr("disabled");
}

function destroyTree(tree) {
	tree.jstree('destroy');
}

function getAddressByCoord(obj){
	var hist = obj.attr('hist');
    var lng = obj.attr('long');
    var lat = obj.attr('lat');

    $.ajax({
        url: "https://api.mapbox.com/geocoding/v5/mapbox.places/"+lng+","+lat+".json?access_token=pk.eyJ1IjoibW92YW1hcHMiLCJhIjoiZTIwM2MzNTBkMzBlMDI3YThmNzE4MmViMDhmZjgwZGUifQ.7cJw2Mx5rHo2sx2jFPDQhQ",
    }).done(function(local){
        var unitdata = {id: '',street: '',streetnumber: '',features : '', fulladdress: ''};

        if(local.features.length > 0){
            unitdata.address = local.features[0].place_name;
            
            if(typeof(local.features[0].context) != "undefined"){
                unitdata.features =  local.features[0].context;
            }else{
                unitdata.features = '';
            }
            unitdata.street = local.features[0].text;
            unitdata.fulladdress =  local.features[0].place_name;
            
            if(typeof(local.features[0].address) != 'undefined'){
                 unitdata.streetnumber = local.features[0].address;
            }
	        //salva em base o endereço 
	        unitdata.id = hist;
	        saveAddress(unitdata, null);
        }else{
            unitdata.address = '';
            unitdata.fulladdress = 'Endereço não encontrado.';
            unitdata.streetnumber = '';
            unitdata.features = '';
        }
        //atualiza tabela de origem
        obj.html(unitdata.fulladdress);
        obj.attr('title', unitdata.fulladdress);
        obj.removeClass('mapAddress');
        obj.removeAttr('hist');
        obj.removeAttr('long');
        obj.removeAttr('lat');
    });
}
//Salva o endereco de uma unidade passado pelo rev. geocoding
function getConStopData(obj){	
	var hist = obj.attr('hist');

 	$.ajax({
        async: true,
	    type : 'POST',
		url : 'widgets/routing.php',
		data : {
				url : 'reportstop/getConStopByHistId/',
				data: hist
		},
        beforeSend: function () {
        	//NADA
        },
        success: function(data) {
        	var dt = JSON.parse(data);   
        	var content = 'Nenhum dado encontrado.';
        	var dt_fim = "--";
        	var tt = "--";

        	if(dt.length > 0){
        		content = '';
	        	//atualiza tabela de origem
	        	content += "<b>Início da parada:</b> "+dt[0].data_inicio+" às "+dt[0].hora_min_inicio+"<br>";
	        	if(dt[0].data_fim != null){
	        		dt_fim = dt[0].data_fim+" às "+dt[0].hora_min_fim;
	        	}
	        	content += "<b>Final da parada:</b> "+dt_fim+"<br>";
	        	if(dt[0].total_time != ''){
	        		tt = minutesToHMS(dt[0].total_time);
	        	}
	        	content += "<b>Tempo parado:</b> "+tt;
	        }	        
	        obj.html(content);
        },
        error: function(jqXHR, textStatus, errorThrown) {
        	
        }
 	});
}

function minutesToHMS(value){
	//value deve estar em seg
	if(value == 0){ return "--"; }
	var second = Math.floor(value % 60);
    var minutes = Math.floor(value / 60 % 60);
    var hours = Math.floor(value / 60 / 60 % 24);
    var result = '';

    if (hours > 0){
        result = hours+" h";
    }
    if (minutes > 0){
        if (result != ''){
            result += " ";
        }
        result += minutes + " min";
    }
    if (second > 0){
        if (result != ''){
            result += " ";
        }
        result += second + " seg";
    }
    return result;
}

function buildRevGeoFromTable(widgetName){
	if( $.inArray(widgetName, varGlobal.filaGeoCode) < 0 ) {
		varGlobal.filaGeoCode.push(widgetName);
	}
	if(varGlobal.controlGeoCode){ 
		return false;
	}
    var hist;
    var lng;
    var lat;
    var unitdata = {id: '',street: '',streetnumber: '',features : '', fulladdress: ''};
    //pega todas as tds com a classse mapAddress
    var td = $('#'+widgetName).find('td').find('div.mapAddress');
    //Remove a classe mapAddress da primeira td
    $(td[0]).removeClass('mapAddress'); 
    //verifica se existe alguma td com a classe mapAddress
    if(td.length <= 0){
    	var index = varGlobal.filaGeoCode.indexOf(widgetName);
		if (index > -1) {
		    varGlobal.filaGeoCode.splice(index, 1);
		}
		if(varGlobal.filaGeoCode.length > 0){
			//chama a função de geo code para oproximo elemento do vetor que tenha pedido 
			buildRevGeoFromTable(varGlobal.filaGeoCode[0]);
		}
        return false;
    }
    //MARCA que o geocode já esta sendo executado por um widgets
    varGlobal.controlGeoCode = true;

    hist = $(td[0]).attr('hist');
    lng = $(td[0]).attr('long');
    lat = $(td[0]).attr('lat');

    $.ajax({
        url: "https://api.mapbox.com/geocoding/v5/mapbox.places/"+lng+","+lat+".json?access_token=pk.eyJ1IjoibW92YW1hcHMiLCJhIjoiZTIwM2MzNTBkMzBlMDI3YThmNzE4MmViMDhmZjgwZGUifQ.7cJw2Mx5rHo2sx2jFPDQhQ",
    }).done(function(local){
         
        if(local.features.length > 0){
            unitdata.address = local.features[0].place_name;
            
            if(typeof(local.features[0].context) != "undefined"){
                unitdata.features =  local.features[0].context;
            }else{
                unitdata.features = '';
            }
            unitdata.street = local.features[0].text;
            unitdata.fulladdress =  local.features[0].place_name;
            
            if(typeof(local.features[0].address) != 'undefined'){
                 unitdata.streetnumber = local.features[0].address;
            }
            
        }else{
            unitdata.address = '';
            unitdata.fulladdress = '';
            unitdata.streetnumber = '';
            unitdata.features = '';
        }
        //atualiza tabela de origem
        $(td[0]).html(unitdata.fulladdress);
        $(td[0]).attr('title', unitdata.fulladdress);
        if((widgetName == 'widgetsunitmonitor') && (typeof(varUnitMonitor) == 'object')){
        	varUnitMonitor.api.draw(false);
        }
        
        //salva em base o endereço 
        unitdata.id = hist;
        saveAddress(unitdata, widgetName);
        //segue para a proxima td da tabela de origem.
        //buildRevGeoFromTable(widgetName);
    });
}
//Salva o endereco de uma unidade passado pelo rev. geocoding
function saveAddress(unit, widgetName){	
	data = JSON.stringify(unit);
 	$.ajax({
        async: true,
	    type : 'POST',
		url : 'widgets/routing.php',
		data : {
				url : 'map/saveAddress/',
				data: JSON.parse(data)
		},
        beforeSend: function () {
        	//NADA
        },
        success: function(data) {  
        	//segue para a proxima td da tabela de origem de widgetName diferente de null.
        	if(widgetName != null){ 
	        	varGlobal.controlGeoCode = false;
	        	buildRevGeoFromTable(widgetName);
	        }
        },
        error: function(jqXHR, textStatus, errorThrown) {
        	varGlobal.controlGeoCode = false;
        }
 	});
}

function dateToBr(sqlDate) {
	var sDate = sqlDate.split(" ");
	return sDate[0].substr(8,2)+'/'+sDate[0].substr(5,2)+'/'+sDate[0].substr(0,4);
}
function dateToEn(sqlDate) {
	var sDate = sqlDate.split(" ");
	return sDate[0].substr(6,4)+'-'+sDate[0].substr(3,2)+'-'+sDate[0].substr(0,2);
}

function dateToString(uDate) {
	var today = new Date();
	var dd = today.getDate();
	var stringDate;
	var sDate = uDate.split(" ");
	
	var uDay = sDate[0].substr(8,2);
	
	if(uDay == dd){
		stringDate = "Hoje às "+sDate[1].substr(0,8);
	} else {
		var diff = uDay - dd;
		
		if(diff == 1){
			stringDate = "Ontem às "+sDate[1].substr(0,8);
		}else{
			stringDate = "Em "+sDate[0].substr(8,2)+'-'+sDate[0].substr(5,2)+'-'+sDate[0].substr(0,4)+' às '+sDate[1].substr(0,8);
		}
		
	}	
	
	return stringDate;
}

function doBubble(data) {
	   var temp = $("#custom-bubble").html();

       Mustache.parse(temp);
       var html = Mustache.render(temp, data);
       $("#custom-bubble").html(html);
       
       $("#custom-bubble .tab-overflow > ul >li > a").click(function() {
    	    if($(this).attr("data-type") == 'history'){
    	    	 var spinnerHtml = '<div class="panel-loader"><div class="spinner-small"></div></div>';
    	    	 
    	    	$("#custom-bubble .history-nav-body").html('');
    	    	$("#custom-bubble").append(spinnerHtml);
    	    	getUnitHistory(data);
    	    } else {
    	    	$("#custom-bubble").find('.panel-loader').remove();
    	    }
       });
}

function selectIcons(form,iconsArray, qtd) {
	
	var icons = [];
	iconsArray = JSON.parse(iconsArray);
	$.each(iconsArray, function(index, val) {
		icons.push(val.icon);
	});        	
	$(form+' select.icon').fontIconPicker({
		hasSearch: false,
		emptyIcon: false,
		source: icons,
		theme: 'fip-bootstrap',
		iconsPerPage: qtd,
	});
}

function getDateDif(date, type, num, op){
	var d; //dias
	var me; //meses
	var y; //anos
	var h; //horas
	var mi; //minutos

	switch(type) {
	    case "DD/MM/YYYY":

			if(op == "+"){
				date.setDate(date.getDate() + num);
			}else{
				date.setDate(date.getDate() - num);
			}

			d 	= date.getDate();
			d 	= ("0" + d).slice(-2);
			me 	= date.getMonth()+1;
			me 	= ("0" + me).slice(-2);
			y 	= date.getFullYear();

			return d+'/'+me+'/'+y;

	        break;
	    case "HH24:MM":

			if(op == "+"){
				date.setHours(date.getHours() + num);
			}else{
				date.setHours(date.getHours() - num);
			}

			h 	= date.getHours();
			h 	= ("0" + h).slice(-2);
			mi 	= date.getMinutes();
			mi 	= ("0" + mi).slice(-2);

			return h+':'+mi;
			
	        break;
	    default:
			return '';
	        break;
	}
}

function getUnitIconRotation(direction){	
	switch(direction) {
	    case 1:
			return ' fm-rotate-45 ';
	        break;
	    case 2:
			return' fm-rotate-90 ';
	        break;
	    case 3:
			return' fm-rotate-135 ';
	        break;
	    case 4:
			return' fm-rotate-180 ';
	        break;
	    case 5:
			return' fm-rotate-225 ';
	        break;
	    case 6:
			return' fm-rotate-270 ';
	        break;
	    case 7:
			return' fm-rotate-315 ';
	        break;
	    default:
			return '';
	        break;
	}
}

var handleAssocSelect = {
	init : function (info){
		var widgetName = info.widgetName;
		var widgetVarString = info.widgetVar;
		var widgetObj = $('#' + info.widgetName);
		var widgetVar = eval(info.widgetVar);
		var assocUrl = info.assocUrl;
		var title = info.title;
		var titletab = info.titletab;
		var type = info.type;
		var content;

		content  = '<div class="col-md-12 col-sm-12 header-text">';
		content += '	<label>'+title+'</label>';
		content += '</div>';

		content += '<div class="col-md-5 col-sm-5 text-center" style="padding: 0px !important;">';
		content += '	<div class="col-md-12 col-sm-12 text-center" style="padding-left: 0px !important; padding-right: 0px !important;">';
		content += '		<div class="col-md-6 col-sm-6 text-center assoc-aba selected">';
		content += '			'+titletab;
		content += '		</div>';
		content += '		<div class="col-md-6 col-sm-6 text-center assoc-aba">';
		content += '			Grupos';
		content += '		</div>';
		content += '	</div>';

		content += '	<div class="col-md-12 col-sm-12 text-center assoc-item assoc-aba-item" style="padding: 0px !important;">';
		content += '		<div id="create-table-assoc-unit" class="table-assoc"></div>';
		content += '	</div>';
		content += '	<div class="col-md-12 col-sm-12 text-center assoc-item assoc-aba-item hidden" style="padding: 0px !important;">';
		content += '		<div id="create-table-assoc-group" class="table-assoc"></div>';
		content += '	</div>';
		content += '</div>';

		content += '<div class="col-md-2 col-sm-2 control-assoc text-center" style="padding: 0px !important;">';
		content += '	<div class="col-md-12 col-sm-12 text-center" style="height: 80px;">';
		content += '	</div>';
		content += '	<div class="col-md-12 col-sm-12 text-center">';
		content += '		<button type="button" class="btn btn-primary btn-default btn-sm assoc-right" title="Associar os itens selecinados."><i class="fa fa-1x fa-angle-right"></i></button>';
		content += '	</div>';
		content += '	<div class="col-md-12 col-sm-12 text-center">';
		content += '		<button type="button" class="btn btn-primary btn-default btn-sm assoc-db-right" title="Associar todos os itens disponíveis."><i class="fa fa-1x fa-angle-double-right"></i></button>';
		content += '	</div>';
		content += '	<div class="col-md-12 col-sm-12 text-center">';
		content += '		<button type="button" class="btn btn-primary btn-default btn-sm assoc-left" title="Desassociar os itens selecinados."><i class="fa fa-1x fa-angle-left"></i></button>';
		content += '	</div>';
		content += '	<div class="col-md-12 col-sm-12 text-center">';
		content += '		<button type="button" class="btn btn-primary btn-default btn-sm assoc-db-left" title="Desassociar todos os itens associados."><i class="fa fa-1x fa-angle-double-left"></i></button>';
		content += '	</div>';
		content += '</div>';

		content += '<div class="col-md-5 col-sm-5 text-center" style="padding: 0px !important;">';
		content += '	<div class="col-md-12 col-sm-12 text-center" style="height: 37px; padding: 0px !important;">';
		content += '	</div>';
		content += '	<div class="col-md-12 col-sm-12 text-center" style="padding: 0px !important;">';
		content += '		<div id="create-table-assoc-unit-assoc" class="table-assoc"></div>';
		content += '	</div>';
		content += '</div>';

		content += '<div class="form-group col-md-12 col-sm-12 col-xs-12 text-center p-t-5 action-buttons">';
		content += '	<button type="button" class="p-10 btn btn-primary m-r-2 btn-sm save-send"><i class=" fa fa-1x fa-check "></i> '+info.btnSaveName+'</button>';
		content += '	<button type="button" class="p-10 btn btn-danger m-r-2 btn-sm cancel-send"><i class=" fa fa-1x fa-times "></i> Cancelar</button>';
		content += '</div>';

		widgetObj.find('.assoc-unit-div').html(content);

	    widgetObj.find('.assoc-left').die("click").live("click", function(){
			var id;
	    	var removeUnit;
	    	var removeUnitAssoc;

	    	$.each(widgetVar.UnitsSelected, function(index, val) {
	            id = val.id;
	    		removeUnit = -1;
	    		removeUnitAssoc = -1;

				$.each(widgetVar.Units, function(index, val) {
		            if(val.id == id){
		            	removeUnit = index;
		            }
		        });
				$.each(widgetVar.UnitsAssoc, function(index, val) {
		            if(val.id == id){
		            	removeUnitAssoc = index;
		            }
		        });

				if(removeUnit >= 0){ widgetVar.Units.splice(removeUnit, 1); }
				if(removeUnitAssoc >= 0){ widgetVar.UnitsAssoc.splice(removeUnitAssoc, 1); }

	            widgetVar.Units.push({ id: parseInt(val.id), name: val.name, group_id: parseInt(val.group_id) });
	        });

			handleAssocSelect.reloadTableAssoc(info.widgetVar);
		});

	    widgetObj.find('.assoc-right').die("click").live("click", function(){
	    	var id;
	    	var removeUnit;
	    	var removeUnitAssoc;

	    	$.each(widgetVar.UnitsSelected, function(index, val) {
	            id = val.id;
	    		removeUnit = -1;
	    		removeUnitAssoc = -1;

				$.each(widgetVar.Units, function(index, val) {
		            if(val.id == id){
		            	removeUnit = index;
		            }
		        });
				$.each(widgetVar.UnitsAssoc, function(index, val) {
		            if(val.id == id){
		            	removeUnitAssoc = index;
		            }
		        });

				if(removeUnit >= 0){ widgetVar.Units.splice(removeUnit, 1); }
				if(removeUnitAssoc >= 0){ widgetVar.UnitsAssoc.splice(removeUnitAssoc, 1); }

	            widgetVar.UnitsAssoc.push({ id: parseInt(val.id), name: val.name, group_id: parseInt(val.group_id) });
	        });

			handleAssocSelect.reloadTableAssoc(info.widgetVar);
		});
				
	    widgetObj.find('.assoc-db-left').die("click").live("click", function(){
	    	var id;
	    	var have;
	    	$.each(widgetVar.UnitsAssoc, function(index, val) {
	            id = val.id;
	    		have = false;
				$.each(widgetVar.Units, function(index, val) {
		            if(val.id == id){
		            	have = true;
		            }
		        });
		        if(!have){
	            	widgetVar.Units.push({ id: parseInt(val.id), name: val.name, group_id: parseInt(val.group_id) });
		        }
	        });
	    	widgetVar.UnitsAssoc = [];

			handleAssocSelect.reloadTableAssoc(info.widgetVar);
		});	

	    widgetObj.find('.assoc-db-right').die("click").live("click", function(){
	    	var id;
	    	var have;
	    	$.each(widgetVar.Units, function(index, val) {
	            id = val.id;
	    		have = false;
				$.each(widgetVar.UnitsAssoc, function(index, val) {
		            if(val.id == id){
		            	have = true;
		            }
		        });
		        if(!have){
	            	widgetVar.UnitsAssoc.push({ id: parseInt(val.id), name: val.name, group_id: parseInt(val.group_id) });
	            }
	        });
	    	widgetVar.Units = [];

			handleAssocSelect.reloadTableAssoc(info.widgetVar);
		});	

		widgetObj.find('.assoc-aba').die("click").live("click", function(){
			var abas = widgetObj.find('.assoc-aba');
			var itens = widgetObj.find('.assoc-aba-item');

	    	if(!$(this).hasClass('selected')){
	    		abas.removeClass('selected');
	    		$(this).addClass('selected');
		    	widgetVar.UnitsSelected = [];
				widgetObj.find('.table-assoc tbody tr').removeClass('assoc-selected');

		    	$.each(itens, function(index, val) {
					if($(this).hasClass('hidden')){
						$(this).removeClass('hidden');
					}else{
						$(this).addClass('hidden');
					}
				});
	    	}    		
		});	

		widgetObj.find('.assoc-unit').die("click").live("click", function(){
			var info = {
		    	widgetName: widgetName,
		    	widgetVar: widgetVarString,
		    	title: title,
		    	titletab: titletab,
		    	type: type
		    }
			handleAssocSelect.loadListByGroup(info);
		});

		widgetObj.find('.cancel-send').die("click").live("click", function(){
	    	widgetVar.UnitsSelected = [];
			widgetObj.find('.table-assoc tbody tr').removeClass('assoc-selected');
			widgetObj.find('#confirm_shadow').remove()
			widgetObj.find('.assoc-unit-div').addClass('hidden');
		});

		widgetObj.find('.save-send').die("click").live("click", function(){
			var info = {
		    	widgetName: widgetName,
		    	widgetVar: widgetVarString
		    }

	    	loadingWidget('#'+widgetName);

			var unitAssoc = [];
			$.each(widgetVar.UnitsAssoc, function(index, val) {
	            unitAssoc.push(val.id);
	        });

			var unitRemove = [];
			$.each(widgetVar.Units, function(index, val) {
	            unitRemove.push(val.id);
	        });

		    var data = 
				{
					id: widgetObj.find('#id').val(),
					unitAssoc: unitAssoc,
					unitRemove: unitRemove,
					type: type
				};
				
		    data = JSON.stringify(data);
	    	widgetVar.UnitsSelected = [];

	    	$.ajax({
	        	async: true,
				type : 'POST',
				url : 'widgets/routing.php',
				data : {
					url : assocUrl,
					data : JSON.parse(data)
				},
				dataType : 'json',
				beforeSend : function() {
					if(typeof(widgetVar) != 'object'){return false;}
		        	loadingWidget('#'+widgetName);
	        		
					widgetObj.find('.table-assoc tbody tr').removeClass('assoc-selected');
					widgetObj.find('#confirm_shadow').remove()
					widgetObj.find('.assoc-unit-div').addClass('hidden');
				},
				success : function(data) {
	    			if(typeof(widgetVar) != 'object'){return false;}
				    handleAssocSelect.reloadTableUnitAssoc(info, JSON.parse(data));
				},
				error : function(error) {
					if(typeof(widgetVar) != 'object'){return false;}
		        	loadingWidget('#'+widgetName, true);
		        	createMsgBox('error', error.responseText, '#'+widgetName+' .panel-body');		
				}
			});
		});
	},
	loadListByGroup : function (info) {	
		var widgetName = info.widgetName;
		var widgetVarString = info.widgetVar;
		var widgetObj = $('#' + info.widgetName);
		var widgetVar = eval(info.widgetVar);
		var title = info.title;
		var titletab = info.titletab;
		var type = info.type;

		$.ajax({
	        async: true,
	        type: 'POST',
	        url: 'widgets/routing.php',
	        data: { url : 'unit/listByGroup/' ,
					data : type
			},
	        dataType: 'json',
	        cache: false,
			beforeSend : function() {
				if(typeof(widgetVar) != 'object'){return false;}
	        	loadingWidget('#'+widgetName);
	        	widgetVar.UnitsAssoc = [];
	        	widgetVar.Units = [];
	        	widgetVar.Groups = [];
			},
	        success: function(data) {
    			if(typeof(widgetVar) != 'object'){return false;}
    			var id;
    			var newId;

				$.each(widgetVar.UnitsAssocOri, function(index, val) {
					widgetVar.UnitsAssoc.push({
						id: parseInt(val.id),
						name: val.name,
						group_id: parseInt(val.group_id)
					});
				});

        		$.each(data.children, function(index, val) {
					widgetVar.Groups.push({
						id: parseInt(val.g_id),
						name: val.text,
						group_id: 0
					});

	        		$.each(val.children, function(index, val) {
	        			id = val.id;
	        			newId = true;

	        			$.each(widgetVar.UnitsAssoc, function(index, val) {
							if(val.id == id){
								newId = false;
							}
						});

						if(newId){
							widgetVar.Units.push({
								id: parseInt(val.id),
								name: val.text,
								group_id: parseInt(val.group_id)
							});															
						}
					});
				});

				//inicializando componente de associação de unidades
			    var info = {
			    	widgetName: widgetName,
			    	widgetVar: widgetVarString,
			    	data: widgetVar.UnitsAssoc,
			    	title: titletab,
			    	type: type,
			    	table: widgetObj.find('#create-table-assoc-unit-assoc'),
			    	list: 'table-list-assoc-unit-assoc',
			    	titleSearch: titletab+' Associadas...'
			    }
        		widgetVar.tableUnitAssoc = handleAssocSelect.buildTableAssoc(info);
			    var info2 = {
			    	widgetName: widgetName,
			    	widgetVar: widgetVarString,
			    	data: widgetVar.Units,
			    	title: titletab,
			    	type: type,
			    	table: widgetObj.find('#create-table-assoc-unit'),
			    	list: 'table-list-assoc-unit',
			    	titleSearch: titletab+' Disponíveis...'
			    }
        		widgetVar.tableUnit = handleAssocSelect.buildTableAssoc(info2);
			    var info3 = {
			    	widgetName: widgetName,
			    	widgetVar: widgetVarString,
			    	data: widgetVar.Groups,
			    	title: 'Grupo',
			    	type: type,
			    	table: widgetObj.find('#create-table-assoc-group'),
			    	list: 'table-list-assoc-group',
			    	titleSearch: 'Grupos Disponíveis...'
			    }
        		widgetVar.tableGroup = handleAssocSelect.buildTableAssoc(info3);

	        	loadingWidget('#'+widgetName, true);  
        		widgetObj.find('.panel-body').prepend("<div name='confirm_shadow' id='confirm_shadow' class='confirm_shadow'></div>");
        		widgetObj.find('.panel-body').find('#confirm_shadow').css('height', widgetObj.find('.panel-body').height());       		
        		widgetObj.find('.assoc-unit-div').removeClass('hidden');
	        },
	        error: function(error) {
    			if(typeof(widgetVar) != 'object'){return false;}
	            //cria mensagem de erro no widget
				createMsgBox('error', error.responseText, '#'+widgetName+' .panel-body'); 
	        }
	    });
		return false;
	},
	buildTableAssoc : function (info){
		var widgetObj = $('#' + info.widgetName);
		var widgetVar = eval(info.widgetVar);
		var title = info.title;
		var objTable = info.table;
		var list = info.list;
		var data = info.data;

		objTable.html( '<table class="table table-striped table-bordered" id="'+list+'" width="100%"></table>' );
		//Pega os dados da tabela
		var table = widgetObj.find('#'+list).DataTable({
		    "data": data,
		    "scrollY": "200px",
	        "scrollCollapse": true,
	        "paging": false,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: false,
		    "pagingType": "full",
		    "fnDrawCallback": function( oSettings ) {
    			if(typeof(widgetVar) != 'object'){return false;}
                widgetObj.find('.100_p').css('width', '100%');
		    },
		    "columns": 
		    	[
		            { "title": "#", "data": "id", "class": 'hidden' },
		            { 
	            		"title": title, "data": "name", "orderable": false, "className": "100_p" 
		    			//, "render": function ( data, type, full, meta ) {
						// 	return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						// } 
					},
		            { "title": "group_id", "data": "group_id", "class": 'hidden' },
	          	],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": info.titleSearch,
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
    			if(typeof(widgetVar) != 'object'){return false;}
		    	//CLICANDO NA CONTA PARA EDITÃ�-LA
	    		widgetObj.find('#'+list+' tbody tr').die("click").live("click", function(){
	    			if($(this).children('td').html() == 'Ainda não há registros'){ return false; }
		    		//Pegando id
    				var id = $(this).children('td').html();
    				id = parseInt(id);
    				var name = $(this).children('td')[1];
    				name = $(name).html();
    				var group_id = $(this).children('td')[2];
    				group_id = $(group_id).html();
    				group_id = parseInt(group_id);
    				var remove;
    				var removeArr = [];
    				var corrFactor = 0;

					if($(this).hasClass('assoc-selected')){
						$(this).removeClass('assoc-selected');
						if(['Unidades', 'Motoristas'].indexOf(title) >= 0){
							$.each(widgetVar.UnitsSelected, function(index, val) {
					            if(parseInt(val.id) == id){
					            	remove = index;
					            }
					        });
							if(remove >= 0){
								widgetVar.UnitsSelected.splice(remove, 1);
							}
						}else{
							$.each(widgetVar.UnitsSelected, function(index, val) {
					            if(parseInt(val.group_id) == id){
					            	removeArr.push(index);
					            }
					        });
							$.each(removeArr, function(index, val) {
								widgetVar.UnitsSelected.splice(val + corrFactor, 1);
								corrFactor--;
					        });
						}
    				}else{
						$(this).addClass('assoc-selected');
						if(['Unidades', 'Motoristas'].indexOf(title) >= 0){
							widgetVar.UnitsSelected.push({ id: parseInt(id), name: name, group_id: parseInt(group_id) });
						}else{
							$.each(widgetVar.Units, function(index, val) {
					            if(parseInt(val.group_id) == id){
					            	widgetVar.UnitsSelected.push({ id: parseInt(val.id), name: val.name, group_id: parseInt(id) });
					            }
					        });
					        if(widgetVar.UnitsSelected.length == 0){
					        	$.each(widgetVar.UnitsAssoc, function(index, val) {
						            if(parseInt(val.group_id) == id){
						            	widgetVar.UnitsSelected.push({ id: parseInt(val.id), name: val.name, group_id: parseInt(id) });
						            }
						        });
					        }
						}
    				}	
				});
		  	}
		});
		return table; 	
	},
    reloadTableAssoc: function (info){
		var widgetVar = eval(info);
		//removendo rows
		if(typeof(widgetVar.tableUnit) == 'object'){
        	widgetVar.tableUnit.clear();
        }

		$(widgetVar.Units).each(function(i, row) {
            widgetVar.tableUnit.row.add({
        		'id': parseInt(row.id),
        		'name': row.name,
        		'group_id': parseInt(row.group_id)
        	});
        });  
		//removendo rows
		if(typeof(widgetVar.tableUnitAssoc) == 'object'){
        	widgetVar.tableUnitAssoc.clear();
        }

		$(widgetVar.UnitsAssoc).each(function(i, row) {
            widgetVar.tableUnitAssoc.row.add({
        		'id': parseInt(row.id),
        		'name': row.name,
        		'group_id': parseInt(row.group_id)
        	});
        });  

    	widgetVar.UnitsSelected = [];
		widgetVar.widgetObj.find('.table-assoc tbody tr').removeClass('assoc-selected');
        widgetVar.tableUnitAssoc.draw(false);
        widgetVar.tableUnit.draw(false);
    },
    reloadTableUnitAssoc: function (info, data){ 
		var widgetName = info.widgetName;
		var widgetObj = $('#' + info.widgetName);
		var widgetVar = eval(info.widgetVar);
    	
    	widgetVar.UnitsAssocOri = [];
		//removendo rows
		if(typeof(widgetVar.tableUnitAssocCad) == 'object'){
        	widgetVar.tableUnitAssocCad.clear();
        }
		$(data.data).each(function(i, row) {
            widgetVar.tableUnitAssocCad.row.add({
        		'id': parseInt(row.id),
        		'name': row.name,
        		'group_id': parseInt(row.group_id),
        		'group_name': row.group_name,
        		'description': row.description,
        		'status': parseInt(row.status)
        	});

			widgetVar.UnitsAssocOri.push({
				id: parseInt(row.id),
				name: row.name,
				description: row.description,
				group_id: parseInt(row.group_id),
				group_name: row.group_name,
				memory_position: parseInt(row.memory_position)
			});
        });  

    	loadingWidget('#'+widgetName, true);
    	widgetVar.UnitsSelected = [];
		widgetObj.find('.table-assoc tbody tr').removeClass('assoc-selected');
        widgetVar.tableUnitAssocCad.draw(false);              
    }
};

/**
 *
 *
 */ 
$('a.i-check').die("click").live("click", function(){
	if($(this).find('i').hasClass('fa-toggle-off')){
		$(this).find('i').removeClass('fa-toggle-off');
		$(this).find('i').addClass('fa-toggle-on');
	}else{
		$(this).find('i').removeClass('fa-toggle-on');
		$(this).find('i').addClass('fa-toggle-off');
	}
});