var varCerca = {};
	varCerca.widgetName 	= 'widgetscerca';
	varCerca.widgetObj 		= $('#'+varCerca.widgetName);
	varCerca.action 		= 'add';
	varCerca.callMap;
	varCerca.cerca_polygroupType 	= 0;
	varCerca.lastClickedPolygonType = 1;
	varCerca.slideSpeed;
	varCerca.color;
	varCerca.accessLevel = 1;

var widgetscerca = {
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	loadEditForm: function (data) {
		if(typeof(varCerca) != 'object'){return false;}

    	if(varCerca.accessLevel == 1){
			var divContent = varCerca.widgetObj.find('.visualize-box');
			var typeCerca;
			switch(data.polygroup_type) {
			    case '1':
			        typeCerca = 'Circular';
			        break;
			    case '2':
			        typeCerca = 'Quadrada';
			        break;
			    default:
			        typeCerca = 'Polígonal';
			}

			divContent.find('.lbl_local').html(data.name);
			divContent.find('.lbl_type').html(typeCerca);
			divContent.find('.lbl_speed').html(data.speed + 'km/h');
			divContent.find('.lbl_color').html("<i class='fa fa-circle' style='color: "+data.color+"; font-size: 15px;'></i>");
			divContent.find('.lbl_group').html(data.group_name);
			divContent.find('.lbl_description').html(data.description);

			openVisualize(varCerca.widgetName);
			return false;
    	}

		var form = varCerca.widgetObj.find('form');
		//limpa formulário
		widgetscerca.reset();
		// muda o form para edição
		varCerca.action = 'edit';
	
		form.find('.call_map').removeClass('third-color-font');

		form.find('#id').val(data.cerca_id);
		form.find('#name').val(data.cerca_name);

		form.find('.color').val(data.color);
		form.find('.color').css('background-color', data.color);
		if(typeof(varCerca.color) == 'object'){
			$(varCerca.color).colorPicker({
				customBG: data.color
			});
		}

		form.find('#speed').val(data.speed);
		if(typeof(varCerca.slideSpeed) == 'object'){
			varCerca.slideSpeed.update({
			    from: data.speed
			});
		}

		form.find('#description').val(data.description);

		form.find('#group option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == data.group_id) {
				$(this).prop('selected', true);
			}
		});
		form.find('#polygroup_type option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == data.polygroup_type) {
				$(this).prop('selected', true);
			}
		});
		//caso mapa esteja aberto, junto a edição do CERCA executar um "ir para" o CERCA no mapa. 
		widgetscerca.loadDrawControl(varCerca.widgetObj.find('.call_map'));
		varCerca.widgetObj.find('.btn-group button').attr('disabled', true);

		openForm(varCerca.widgetName, varCerca.action);
	},
	buildGroup: function (data){
		varCerca.widgetObj.find('#group').html('<option value="">Selecione um grupo</option>');
		var template = 	'{{#groupinfo}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/groupinfo}}';
		var select = Mustache.to_html(template, data);
		varCerca.widgetObj.find('#group').append(select);
	},
	reset: function (){
		if(typeof(varCerca) != 'object'){return false;}
		var form = varCerca.widgetObj.find('form');

		varCerca.action = 'add'; 

		form[0].reset();  
		form.find('#group').prop('selectedIndex', 0);
		form.find('#description').val('');
		form.find('.color').val('#808080');
		form.find('.color').css('background-color', '#808080');
		if(typeof(varCerca.color) == 'object'){
			$(varCerca.color).colorPicker({
				customBG: '#808080'
			});
		}	
		form.find('#speed').val(0);		
		if(typeof(varCerca.slideSpeed) == 'object'){
			varCerca.slideSpeed.update({
			    from: 0
			});
		}
		
		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');
	},
	delete: function () {
		if(typeof(varCerca) != 'object'){return false;}
	    var data = { id: varCerca.widgetObj.find('form').find("#id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'cerca/delete',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varCerca.widgetName);
			},
			success : function(data) {
				if(typeof(varCerca) != 'object'){return false;}
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varCerca.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varCerca.widgetObj.find('#table-list-cerca').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varCerca.widgetName);
				widgetscerca.cancelForm();

				if(typeof(MapBox) == 'object'){
					MapBox.disabledDrawControl();
				}					
				loadingWidget('#'+varCerca.widgetName, true);
			},
			error : function(error) {
				if(typeof(varCerca) != 'object'){return false;}
	        	loadingWidget('#'+varCerca.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varCerca.widgetName+' .panel-body');		
			}
		});
		return false;
	},
	cancelForm: function (){
		if(typeof(varCerca) != 'object'){ return false; }

		clearTimeout(varCerca.callMap);	
		closeForm(varCerca.widgetName);
		widgetscerca.reset();
		if(typeof(MapBox) == 'object'){
			MapBox.disabledDrawControl();
		}	
	},
	changeDrawColor: function (color){
		if(typeof(varMapBox) != 'object'){ return false; }		
		if(typeof(varMapBox.featureGroupCerca) != 'object') { return false; }
		if(typeof(varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]) != 'object'){ return false; }

		varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId].options.color = color;
		varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId].options.fill = true;
		varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId].options.fillColor = color;
		varMapBox.mapInstance.getContainer().querySelector('.leaflet-draw-edit-edit').click();	 
		varMapBox.mapInstance.getContainer().querySelector('ul.leaflet-draw-actions.leaflet-draw-actions-top li a').click();
	},
	getIdCercaById: function (id){
		varMapBox.listaIdCerca.forEach(function(entry) {
		    if(entry[0] == id){
		    	retorno = entry[1];
		    }
		});
		return retorno;
	},
	loadDrawControl: function (obj){
		if(typeof(varCerca) != 'object'){return false;}
		//verifica se o botão está ativado 
		// se estiver, remove o mesmo, assim como o menu de controle de 'draw'
		if(obj.hasClass('color-stat')){
			//altera cor do botão para indicar que estáativo
			obj.removeClass('color-stat');
			if(typeof(MapBox) == 'object'){
				//MapBox.disabledDrawControl();
			}
			return false;
		}

		//altera cor do botão para indicar que estáativo
		obj.addClass('color-stat');
		//verifica de existe uma instancia de mapa e se o widgets do mapa está aberto e se o widget de POI tambem existe
		if(typeof(varMapBox) == 'object'){
			if((typeof(varMapBox.cercaLayer) != 'object') || (varCerca.action != 'edit') || (typeof(varMapBox.mapInstance) != 'object')){
				//para dar tempo do mapa abrir, espera alguns segundos para chamar a função novamente
				clearTimeout(varCerca.callMap);
				varCerca.callMap = window.setTimeout(function() {
					obj.removeClass('color-stat');
			        widgetscerca.loadDrawControl(obj);
			    }, 2000); 
			    return false;
			}
			clearTimeout(varCerca.callMap);
			MapBox.loadDrawControl();

			varMapBox.widgetObj.find('.leaflet-draw-draw-circle').on("click", function(event){	
				varCerca.lastClickedPolygonType = 1;
				widgetscerca.removeDrawPolygon(varCerca.lastClickedPolygonType);	
				return true;
			});

			varMapBox.widgetObj.find('.leaflet-draw-draw-rectangle').on("click", function(){	
				varCerca.lastClickedPolygonType = 2;
				widgetscerca.removeDrawPolygon(varCerca.lastClickedPolygonType);
				return true;
			});

			varMapBox.widgetObj.find('.leaflet-draw-draw-polygon').on("click", function(){	
				varCerca.lastClickedPolygonType = 3;
				widgetscerca.removeDrawPolygon(varCerca.lastClickedPolygonType);
				return true;
			});

			varMapBox.widgetObj.find('.leaflet-draw-edit-remove').on("click", function(){	
				widgetscerca.removeDrawPolygon(varCerca.lastClickedPolygonType);			

				switch(varCerca.lastClickedPolygonType) {
				    case 1:
						varMapBox.mapInstance.getContainer().querySelector('.leaflet-draw-draw-circle').click();
				        break;
				    case 2:
						varMapBox.mapInstance.getContainer().querySelector('.leaflet-draw-draw-rectangle').click();
				        break;
				    case 3:
						varMapBox.mapInstance.getContainer().querySelector('.leaflet-draw-draw-polygon').click();
				}	
			});

			varMapBox.widgetObj.find('[data-click=panel-remove]').on("click", function(){
				widgetscerca.cancelForm(varCerca.action);     
		    });
			
			if(varCerca.action == 'edit'){	
				window.setTimeout(function() {
					widgetscerca.getPolygon();
			    }, 1000);		
			}

			varMapBox.widgetObj.find('.leaflet-draw-edit-edit').removeClass('leaflet-disabled');
			varMapBox.widgetObj.find('.leaflet-draw-edit-remove').removeClass('leaflet-disabled');

		}else{
			//caso o mapa não esteja aberto, chama o mapa
			handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 8);
			//para dar tempo do mapa abrir, espera alguns segundos para chamar a função novamente
			clearTimeout(varCerca.callMap);
			varCerca.callMap = window.setTimeout(function() {
				obj.removeClass('color-stat');
		        widgetscerca.loadDrawControl(obj);
		    }, 2000); 
		}
	},
	getPolygon: function (){
		if(typeof(varMapBox.featureGroupCerca) != 'object'){
			window.setTimeout(function() {
				widgetscerca.getPolygon();
		    }, 1000);
			return false;
		}
		varMapBox.featureGroupCerca._layers[1] = varMapBox.cercaLayer._layers[ widgetscerca.getIdCercaById(varCerca.widgetObj.find('form').find("#id").val()) ];
		varMapBox.featureGroupCerca._layers[1].options.weight = 4;
		varMapBox.mapInstance.getContainer().querySelector('.leaflet-draw-edit-edit').click();
		varMapBox.drawPolyId = 1;
		//caso mapa esteja aberto, junto a edição do CERCA executar um "ir para" o CERCA no mapa. 
		varMapBox.mapInstance.fitBounds(varMapBox.featureGroupCerca.getBounds());
	},
	fitBoundsCerca: function (id){
		if(typeof(varCerca) != 'object'){return false;}
		//verifica de existe uma instancia de mapa e se o widgets do mapa está aberto e se o widget de POI tambem existe
		if(typeof(varMapBox) == 'object'){
			clearTimeout(varCerca.callMap);
			if(typeof(varMapBox.cercaLayer) != 'object'){
				clearTimeout(varCerca.callMap);
				varCerca.callMap = window.setTimeout(function() {
			        widgetscerca.fitBoundsCerca(id);
			    }, 1000);
			    return false;
			}
			var cercaFitBounds = varMapBox.cercaLayer._layers[widgetscerca.getIdCercaById(id)];
			varMapBox.mapInstance.fitBounds(cercaFitBounds.getBounds());
		}else{
			//caso o mapa não esteja aberto, chama o mapa
			handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 8);
			//para dar tempo do mapa abrir, espera alguns segundos para chamar a função novamente
			clearTimeout(varCerca.callMap);
			varCerca.callMap = window.setTimeout(function() {
		        widgetscerca.fitBoundsCerca(id);
		    }, 2000); 
		}
	},
	removeDrawPolygon: function (type){	
		if(typeof(varMapBox.featureGroupCerca) == 'object') { 
			varMapBox.featureGroupCerca.removeLayer(varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]);
		}
		if(typeof(varMapBox.CercaClusterLayer) == 'object') { 
			varMapBox.CercaClusterLayer.removeLayer(varMapBox.CercaClusterLayer._layers[widgetscerca.getIdCercaById(varCerca.widgetObj.find('form').find("#id").val())]);
		}

		varCerca.widgetObj.find('#polygroup_type option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == type) {
				$(this).prop('selected', true);
			}
		});

		return true;
	},
	DisabledButtons: function (){
		if(typeof(varCerca) != 'object'){return false;}
		varCerca.widgetObj.find('.call_map').hide();
		varMapBox.widgetObj.find('.leaflet-draw.leaflet-control').hide();
	},
	EnabledButtons: function (){
		if(typeof(varCerca) != 'object'){return false;}
		varCerca.widgetObj.find('.call_map').show();
		varMapBox.widgetObj.find('.leaflet-draw.leaflet-control').show();
	},
	buildTable: function (){
		if(typeof(varCerca) != 'object'){return false;}
		varCerca.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-cerca" width="100%"></table>' );
		//Pega os dados da tabela
		var table_pois = varCerca.widgetObj.find('#table-list-cerca').dataTable({
		    ajax: {
		        "url": 'widgets/routing.php',
		        "type": "POST",
		        "data": { url: 'cerca/list/'},
		  		error: function (error) {  
		  			if(typeof(varCerca) != 'object'){return false;}
		        	loadingWidget('#'+varCerca.widgetName, true);
					createMsgBox('error', error.responseText, '#'+varCerca.widgetName+' .panel-body');
		  		}
		      },
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "columns": 
		    	[
		            { "title": "#", "data": "cerca_id", "class": 'hidden' },
		            { 
		            	"title": "Cerca", 
		            	"data": "cerca_name", 
		            	"className": "goToEdit 30_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
					},
		            { 
		            	"title": "Grupo", 
		            	"data": "group_name", 
		            	"className": "goToEdit 30_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            },
		            { 
		            	"title": "Velocidade", 
		            	"data": "speed", 
		            	"className": "text-center goToEdit 20_p", 
		            	"render": function ( data, type, full, meta ) { 
							return '<div class="nowrap ellipsis" title="' + data + ' km/h">' + data + ' km/h</div>';
		            	}
		            },
		            { 	
		            	"title": "Cor", 
		            	"data": "color", 
		            	"bSortable": false, 
		            	"className": "text-center goToEdit no-select 10_p", 
		            	"render": function ( data, type, full, meta ) {
							return "<i class='fa fa-circle' style='color: "+data+"; font-size: 15px;'></i>";
						}
					},
		            { 
		            	"title": "&nbsp;", 
		            	"data": null, 
		            	"bSortable": false, 
		            	"defaultContent": "<i class='fa fa-globe primary-color-font' style='font-size: 15px;' title='Ver cerca no mapa'></i>", 
		            	"className": "text-center no-select goToMap 10_p"
		            }
	          	],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por dispositivos...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
		    	if(typeof(varCerca) != 'object'){return false;}
                varCerca.widgetObj.find('.10_p').css('width', '10%');
                varCerca.widgetObj.find('.20_p').css('width', '20%');
                varCerca.widgetObj.find('.30_p').css('width', '30%');
		    	//CLICANDO NA CONTA PARA EDITÃ�-LA
		    	if(json.data.length > 0){

			    	varCerca.widgetObj.find('#table-list-cerca tbody tr').find('td.goToEdit').die("click").live("click", function(){
			    		//Pegando id da conta
			    		var parent = $(this).parents('tr');
	    				var data = parent.children('td').html();
			    	    loadingWidget('#'+varCerca.widgetName);		
		    		    $.ajax({
		    		    	async: true,
		    		    	type: 'POST',
		    		        url: 'widgets/routing.php',
		    		        data: { 
		    		        	url: 'cerca/getById/', 
		    		        	data: JSON.parse(data) 
		    		        },
		    		        dataType: 'json',
		    		        success: function(data) {
			    		        widgetscerca.loadEditForm(data);
			    	        	loadingWidget('#'+varCerca.widgetName, true);
		    		        },
		    		        error: function(error) {
			    	        	createMsgBox('error', error.responseText, '#'+varCerca.widgetName+' .panel-body');
			    	        	loadingWidget('#'+varCerca.widgetName, true);
		    		        }
		    		    });
		    		    return false;
		    		}); 

		    		varCerca.widgetObj.find('#table-list-cerca tbody tr').find('td.goToMap').die("click").live("click", function(){
						var parent = $(this).parents('tr');
		    			var data = parent.children('td').html();
		    			widgetscerca.fitBoundsCerca(data)
		    		    return false;
		    		}); 
				}  	
		  	}
		});
	}
};

function load_widgetscerca_Page(){
	if(typeof(varCerca) != 'object'){return false;}
	varCerca.accessLevel = handleAccessLevel(varCerca.widgetName);
	if(varCerca.accessLevel == 1){
		varCerca.widgetObj.find('.openForm').addClass('hidden');
	}

	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'cerca/index/'},
        dataType: 'json',
        beforeSend: function () {
        	if(typeof(varCerca) != 'object'){return false;}
        	closeForm(varCerca.widgetName);
			widgetscerca.reset();	
			varCerca.widgetObj.find('.btn').attr('disabled', 'disabled');
            varCerca.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	if(typeof(varCerca) != 'object'){return false;}
        	widgetscerca.buildTable();
        	widgetscerca.buildGroup(JSON.parse(data.groupinfo));

        	varCerca.color = varCerca.widgetObj.find("form").find('.color').colorPicker({
				opacity: false,
				doRender: true,
				customBG: '#808080',
				renderCallback: function($elm, toggled) {
					if(typeof(varCerca) != 'object'){return false;}
					var color = '#'+this.color.colors.HEX;
					varCerca.widgetObj.find("form").find('.color').val(color);
					widgetscerca.changeDrawColor(color);
				}
			});

			varCerca.widgetObj.find('form').find('#speed').ionRangeSlider({
				min: 0,
		        max: 300,
		        type: "single",
		        postfix: "km/h",
		        from: 0,
		        to: 300
		    });
		    varCerca.slideSpeed = varCerca.widgetObj.find("form").find('#speed').data("ionRangeSlider");

			if(typeof(MapBox) == 'object'){
				MapBox.activeCercaControls();
			}

        	//SET ID account user
        	varCerca.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	//liberando menu
        	varCerca.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varCerca.widgetName, true); 
        },
        error: function(error) {
        	if(typeof(varCerca) != 'object'){return false;}
        	loadingWidget('#'+varCerca.widgetName, true);
            varCerca.widgetObj.find('.btn-danger').removeAttr("disabled");
			createMsgBox('error', error.responseText, '#'+varCerca.widgetName+' .panel-body');
        }
    });
}

$(document).ready(function() {
	load_widgetscerca_Page();

	varCerca.widgetObj.find('.openForm').die("click").live("click", function(){
		widgetscerca.reset();
		varCerca.action = 'add';
		openForm(varCerca.widgetName, 'add');
	});
	//CHAMADA PARA EXCLUSÃO DE pois 
	varCerca.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		openConfirm(varCerca.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir Cerca", "Deseja remover a Cerca selecionada?", "Sim", "Não");
	});
	varCerca.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		widgetscerca.delete();
		closeConfirm(varCerca.widgetName);
	});
	varCerca.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varCerca.widgetName);
	});
	//Resetando árvores ao clicar em cancelar/fechar
	varCerca.widgetObj.find('form').find('.cancel').die('click').live('click', function() {
		widgetscerca.cancelForm();
	});	

	varCerca.widgetObj.find('.visualize-box').find('.cancel').die('click').live('click', function() {
		closeVisualize(varCerca.widgetName);
	});	

	varCerca.widgetObj.find('.call_map').die("click").live("click", function(){	
		widgetscerca.loadDrawControl($(this));	
	});

	varCerca.widgetObj.find('form').submit(function(e) { 
		e.preventDefault();
		if(typeof(varCerca) != 'object'){return false;}
	    if ( $(this).parsley().isValid() ) {
			var form = this; 
			varCerca.cerca_polygroupType = $(this).find("#polygroup_type").val();
			
			if((typeof(varMapBox.featureGroupCerca) != 'object') && (varCerca.action = 'edit')){
				// cria mensagem de erro no widget
				createMsgBox('info','Ative a edição de cerca, para que o sistema identifique a cerca a ser editada.', '#'+varCerca.widgetName+' .panel-body');
				return false;
			}
			
			if(typeof(varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]) != 'object'){
				// cria mensagem de erro no widget
				createMsgBox('info','É preciso desenhar uma cerca.', '#'+varCerca.widgetName+' .panel-body');
				return false;
			}

			switch(varCerca.cerca_polygroupType) {
			    case '1':
					var cerca_lat = varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlng.lat;
					var cerca_lng = varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlng.lng;				
					var cerca_coords = [cerca_lat, cerca_lng];
					var cerca_radius = parseInt(varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._mRadius);

			        break;
			    case '2':
					var cerca_coords = [];
			        for(var i=0 ; i < varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs.length ; i++){
			        	cerca_coords.push( [varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs[i].lat, varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs[i].lng] );
			        }
		        	cerca_coords.push( [varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs[0].lat, varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs[0].lng] );
					var cerca_radius = 0;

			        break;
			    default:
					var cerca_coords = [];
					if(varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs.length > 31){
						// validando numero maximo de pontos em um poligono
						createMsgBox('info','Sua cerca possui '+varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs.length+' pontos, e o máximo suportado pelo sistema é de 31 pontos.', '#'+varCerca.widgetName+' .panel-body');
						return false
					}
			        for(var i=0 ; i < varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs.length ; i++){
			        	cerca_coords.push( [varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs[i].lat, varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs[i].lng] );
			        }
		        	cerca_coords.push( [varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs[0].lat, varMapBox.featureGroupCerca._layers[varMapBox.drawPolyId]._latlngs[0].lng] );
					var cerca_radius = 0;
			}      

		    var data = 
				{
					id: $(this).find("#id").val(),
					name: $(this).find("#name").val(),
					group_id: $(this).find("#group").val(),
					account_id: $(this).find("#account_id").val(),
					polygroup_type: $(this).find("#polygroup_type").val(),
					color: $(this).find(".color").val(),
					speed: $(this).find("#speed").val(),
					description: $(this).find("#description").val(),
					cerca_coords: [cerca_coords],
					cerca_radius: cerca_radius
				};
		    
		    data = JSON.stringify(data);
		    
	        $.ajax({
	        	async: true,
				type : 'POST',
				url : 'widgets/routing.php',
				data : {
					url : 'cerca/'+varCerca.action+'/',
					data : JSON.parse(data)
				},
				dataType : 'json',
				beforeSend : function() {
					if(typeof(varCerca) != 'object'){return false;}
    	        	loadingWidget('#'+varCerca.widgetName);
				},
				success : function(data) {
					if(typeof(varCerca) != 'object'){return false;}
    	        	loadingWidget('#'+varCerca.widgetName, true);       	        
	    	        //Criando mensagem de retorno
	    	        createMsgBox('success', data, '#'+varCerca.widgetName+' .panel-body');
	    	        // atualiza tabela que lista as contas
	    	        varCerca.widgetObj.find('#table-list-cerca').DataTable().ajax.reload(null, false);	    	            	
	    	        //reseta o form	    
					closeForm(varCerca.widgetName);
					widgetscerca.cancelForm();
					//liberar o marker sobre o mapa
					if(typeof(MapBox) == 'object'){
						MapBox.disabledDrawControl();
					}					
				},
				error : function(error) {
					if(typeof(varCerca) != 'object'){return false;}
    	        	loadingWidget('#'+varCerca.widgetName, true);
    	        	//Criando mensagem de retorno
    	        	createMsgBox('error', error.responseText, '#'+varCerca.widgetName+' .panel-body');		
				}
			});
			return false;
	    }
	});
});



