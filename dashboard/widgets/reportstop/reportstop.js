var varReportstop = {};
    varReportstop.widgetName    = 'widgetsreportstop';
    varReportstop.widgetObj     = $('#'+varReportstop.widgetName);
    varReportstop.numEventsG    = 0;
    varReportstop.numEvents     = 0;
    varReportstop.numSearchs    = 0;
    varReportstop.backupElem    = '';
    varReportstop.tabResults    = 0;
    varReportstop.table         = [];
    varReportstop.api           = [];
    varReportstop.dataFilterArr = [];
    varReportstop.processingSearchLimit = 1000;

    varReportstop.rowSizeMax = 28;
    varReportstop.rowSizeMin = 10;
    varReportstop.rowAdjust = 5;

var widgetsReportStop = {
    buildGroup: function (data){
        varReportstop.widgetObj.find('#group').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varReportstop.widgetObj.find('#group').append(select);
    },
    buildSubGroup: function (data){
        varReportstop.widgetObj.find('#subgroup').html('<option value="0" selected>Todos</option>');
        var template =  '{{#subgroupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/subgroupinfo}}';
        var select = Mustache.to_html(template, data);
        varReportstop.widgetObj.find('#subgroup').append(select);
    },
    processingSearch: function (idTab, dataFilter){
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportstop/search/', data: dataFilter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                varReportstop.widgetObj.find('#table-list-result-stop-'+idTab+'_info').append('<span class="sp-load-'+idTab+'" title="Carregando consulta..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                //para de processar caso a tab seja fechada
                if(varReportstop.table[idTab] == null ){ return false; }
                
                $(json).each(function(i, rows) {                
                    varReportstop.table[idTab].row.add({
                        "label": rows.label, 
                        "data_inicio": rows.data_inicio,  
                        "data_report_inicio": rows.data_report_inicio,
                        "hora_min_inicio": rows.hora_min_inicio, 
                        "data_fim": rows.data_fim,  
                        "data_report_fim": rows.data_report_fim,
                        "hora_min_fim": rows.hora_min_fim,  
                        "initial_longitude": rows.initial_longitude, 
                        "initial_latitude": rows.initial_latitude, 
                        "initial_full_address": rows.initial_full_address, 
                        "devst_id": rows.devst_id, 
                        "initial_poi_name": rows.initial_poi_name, 
                        "initial_area_name": rows.initial_area_name, 
                        "total_time": rows.total_time, 
                        "full_date_begin": rows.full_date_begin
                    });
                }); 
                varReportstop.table[idTab].draw(false);
                //reinicia o geocode caso haja algum registro para ser carregado
                buildRevGeoFromTable(varReportstop.widgetName);
                //verifica se ainda existem registros para serem buscados
                if( json.length >= varReportstop.processingSearchLimit ){
                    dataFilter.limit += varReportstop.processingSearchLimit;

                    window.setTimeout(function() {
                        widgetsReportStop.processingSearch(idTab, dataFilter); 
                    }, 50);
                }    
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReportstop.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                varReportstop.widgetObj.find('.sp-load-'+idTab).remove();
            }
        });
    },
    print: function (type, id){
        var filter = JSON.parse(varReportstop.dataFilterArr[id]);
        //removendo limit para retornar todos os dados quando for´exportação
        filter.limit = null;

        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportstop/search/', data: filter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                varReportstop.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
                varReportstop.widgetObj.find('.actionMenu .fristAction').append('<span class="sp-load" title="Carregando exportação..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                var horaFim;
                var speedFim;

                switch (type) { 
                    case 'print': 
                        var content = '<style>';
                        content += '    table { border: 1px solid #999; border-bottom: none; width: 100%; }';
                        content += '    thead { border: 5px solid #000; }';
                        content += '    tbody td { border: 1px solid #ccc; }';
                        content += '</style>';             
                        content += '<table><thead"><th>Placa</th><th>Hora Inicial</th><th>Hora Final</th><th>Tempo Total</th><th>Local</th><th>POI</th><th>Cerca</th></thead><tbody>';
                        
                        $(json).each(function(i, row) {
                            horaFim = row.data_report_fim+' às '+row.hora_min_fim;
                            if(row.data_report_fim == null){
                                horaFim = '--';
                            }
                            speedFim = minutesToHMS(row.total_time * 60);
                            if(row.total_time == 0){
                                speedFim = '--';
                            }
                            content += '<tr><td>'+row.label+'</td><td>'+row.data_report_inicio+' às '+row.hora_min_inicio+'</td><td>'+horaFim+'</td><td>'+speedFim+'</td>';
                            content += '<td>'+row.initial_full_address+'</td><td>'+row.initial_poi_name+'</td><td>'+row.initial_area_name+'</td></tr>';
                        });            
                        content += '</tbody></table>';

                        var w = window.open("");

                        w.document.write(content);
                        w.print();
                        w.close();

                        break;
                    case 'PDF': 
                        var columns = ["Placa", "Hora Inicial", "Hora Final", "Tempo", "Local", "POI", "Cerca"];
                        var rows = [];
                        $(json).each(function(i, row) {
                            horaFim = row.data_report_fim+' às '+row.hora_min_fim;
                            if(row.data_report_fim == null){
                                horaFim = '--';
                            }
                            speedFim = minutesToHMS(row.total_time * 60);
                            if(row.total_time == 0){
                                speedFim = '--';
                            }
                            rows.push([row.label, row.data_report_inicio+' às '+row.hora_min_inicio, horaFim, speedFim, row.initial_full_address, row.initial_poi_name, row.initial_area_name]);
                        });   
                        var doc = new jsPDF('l', 'pt');
                        var dataCurrente = new Date();
                        var year = dataCurrente.getFullYear();
                        var month = dataCurrente.getMonth() + 1;
                        var day = dataCurrente.getDate();

                        doc.autoTable(columns, rows, {
                            theme: 'striped', // 'striped', 'grid' or 'plain'
                            headerStyles: {
                                fillColor: [0, 147, 147],
                                color: [255, 255, 255]
                            },
                            bodyStyles: {
                                overflow: 'linebreak', // visible, hidden, ellipsize or linebreak
                            },
                            margin: {
                                top: 100
                            },
                            beforePageContent: function(data) {
                                var imgData = varGlobal.imgRelLogo;
                                doc.addImage(imgData, 'PNG', 40, 30);
                                doc.text("Histórico", 40, 90);
                            }
                        });
                        doc.save("Histórico_"+year+"-"+month+"-"+day+".pdf");

                        break;
                    case 'CSV': 
                        var content = 'Placa;Hora Inicial;Hora Final;Tempo Total;Local;POI;Cerca\r\n';
                        
                        $(json).each(function(i, row) {
                            horaFim = row.data_report_fim+' às '+row.hora_min_fim;
                            if(row.data_report_fim == null){
                                horaFim = '--';
                            }
                            speedFim = minutesToHMS(row.total_time * 60);
                            if(row.total_time == 0){
                                speedFim = '--';
                            }
                            content += row.label+';'+row.data_report_inicio+' às '+row.hora_min_inicio+';'+horaFim+';'+speedFim+';'+row.initial_full_address+';'+row.initial_poi_name+';'+row.initial_area_name+'\r\n';
                        });           

                        var a         = document.createElement('a');
                        a.href        = 'data:attachment/csv,' +  escape(content);
                        a.target      = '_blank';
                        a.download    = 'Relatório_de_Paradas.csv';

                        document.body.appendChild(a);
                        a.click();

                        break;     
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReportstop.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                varReportstop.widgetObj.find('.dropdown-toggle').removeAttr("disabled"); 
                varReportstop.widgetObj.find('.sp-load').remove(); 
            }
        }); 
    },
    openMap: function (lat, lng, drawMark){
        if(typeof(varMapBox) != 'object'){
            handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 4);

            clearTimeout(varReportstop.callMap);
            varReportstop.callMap = window.setTimeout(function() {
                widgetsReportStop.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }
        if(typeof(varMapBox.mapInstance) != 'object'){
            clearTimeout(varReportstop.callMap);
            varReportstop.callMap = window.setTimeout(function() {
                widgetsReportStop.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }

        MapBox.centerUnit(lat, lng, false, null, drawMark);
    }
};

function load_widgetsreportstop_Page(){
    $.ajax({
        type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'reportstop/index/'},
        dataType: 'json',
        beforeSend: function () {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportstop) != 'object'){ return false; }
            varReportstop.widgetObj.find('.btn').attr('disabled', 'disabled');
            varReportstop.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportstop) != 'object'){ return false; }

            widgetsReportStop.buildGroup(JSON.parse(data.groupinfo))
            widgetsReportStop.buildSubGroup(JSON.parse(data.subgroupinfo))

            varReportstop.widgetObj.find('form').find('#start_day').val(data.date);
            varReportstop.widgetObj.find('form').find('#end_day').val(data.date);
            varReportstop.widgetObj.find('form').find('#end_hour').val(data.hour);

            varReportstop.widgetObj.find('form').removeClass('hidden');
            varReportstop.widgetObj.find('.load-report').addClass('hidden');
            //liberando menu
            varReportstop.widgetObj.find('.btn').removeAttr("disabled");
            loadingWidget('#'+varReportstop.widgetName, true); 
            varReportstop.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');

        },
        error: function(error) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportstop) != 'object'){ return false; }
            createMsgBox('error', error.responseText, '#'+varReportstop.widgetName+' .panel-body');
        },
        complete: function() {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportstop) != 'object'){ return false; }
            varReportstop.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
    load_widgetsreportstop_Page();

    //Plugin Clockpicker
    varReportstop.widgetObj.find('.clockpicker').clockpicker({
        autoclose: true,
        'default': 'now'
    });    
    //Datepicker
    varReportstop.widgetObj.find('.pluginDate').datepicker({ 
        autoclose: true, 
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        todayHighlight: true,
    });

    varReportstop.widgetObj.find('select#group').on('change', function() {
        var select = 'select#subgroup';
        var data = 
        {
            groupIds: $(this).val()
        };
        varReportstop.numEventsG++;
        data = JSON.stringify(data);

        var html = '';
        var selected = 'selected';
        if($(this).val() == 0) {
            html += '<option value="0" selected>Todos</option>';
            selected  = '';
        }
        
        $.ajax({
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'client/listSubGroups/', data:JSON.parse(data) },
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                varReportstop.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                varReportstop.widgetObj.find('select#unit').addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                try {
                    var subgroups = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReportstop.widgetName+' .panel-body');              
                    varReportstop.widgetObj.find(select).html('');
                    varReportstop.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                
                $(subgroups).each(function(i, val) {
                    html+='<option value="'+val.id+'" '+selected+'>'+val.name+'</option>';
                });

                varReportstop.numEventsG--;
                if(varReportstop.numEventsG == 0) {
                    varReportstop.widgetObj.find(select).removeClass('select-loader');
                    varReportstop.widgetObj.find(select).html(html);
                    varReportstop.widgetObj.find('select#subgroup').change();
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                createMsgBox('error', error.responseText,'#'+varReportstop.widgetName+' .panel-body');                 
                varReportstop.widgetObj.find(select).html(error.responseText);
                varReportstop.widgetObj.find(select).removeClass('select-loader');
            }
        });     
    });
    
    varReportstop.widgetObj.find('select#subgroup').on('change', function() {
        var select = 'select#unit';

        var html = '';      
        if($(this).val() == '0') {
            html += '<option value="0">Todos</option>';
        }
        var data = 
        {
            subGroupIds: $(this).val()
        };
        varReportstop.numEvents++;
        
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'unit/listTrackedUnit/', data:JSON.parse(data) },
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                varReportstop.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                try {
                    var unities = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReportstop.widgetName+' .panel-body');              
                    varReportstop.widgetObj.find(select).html('');
                    varReportstop.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                html = '<option value="0" selected>Todos</option>';
                $(unities).each(function(i, val) {
                    if(val.label != undefined){
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    }   
                });
                
                varReportstop.numEvents--;
                if(varReportstop.numEvents == 0) {
                    varReportstop.widgetObj.find(select).removeClass('select-loader');
                    varReportstop.widgetObj.find(select).html(html);
                }                   
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstop) != 'object'){ return false; }
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varReportstop.widgetName+' .panel-body');                 
                varReportstop.widgetObj.find(select).html(error.responseText);
                varReportstop.widgetObj.find(select).removeClass('select-loader');
            }
        });
    });

    varReportstop.widgetObj.find('input#searchUnits').on('keyup', function(e) {
        e.preventDefault();
        if(varReportstop.backupElem == '') {
            varReportstop.backupElem = varReportstop.widgetObj.find('select#unit').html();
        }
        if($(this).val().length > 2) {
            var select = 'select#unit';
            var html = '';
            var data = 
            {
                unit: $(this).val()
            };
            data = JSON.stringify(data);
            varReportstop.numSearchs++;
             $.ajax({
                async : true,
                type : "POST",
                data: { url: 'unit/searchUnit', data: JSON.parse(data) },
                url : 'widgets/routing.php',
                dataType : "json",
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstop) != 'object'){ return false; }
                    varReportstop.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                },
                success : function(json) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstop) != 'object'){ return false; }
                    varReportstop.numSearchs--;
                    if(json.length <= 0) {
                        if(varReportstop.numSearchs == 0) {
                            varReportstop.widgetObj.find(select).html('');
                            varReportstop.widgetObj.find(select).removeClass('select-loader');
                            return false;
                        }
                    }
                    html+='<option value="0" selected>Todos</option>';
                    $(json).each(function(i, val) {
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    });
                    if(varReportstop.numSearchs == 0) {
                        varReportstop.widgetObj.find(select).removeClass('select-loader');
                        varReportstop.widgetObj.find(select).html(html);
                    }
                },
                error : function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstop) != 'object'){ return false; }
                    varReportstop.numSearchs--;
                    if(varReportstop.numSearchs == 0) {
                        createMsgBox('error', error.responseText, "#"+varReportstop.widgetName+" .panel-body");
                        varReportstop.widgetObj.find(select).removeClass('select-loader');
                    }
                }
            });
        } else if($(this).val().length <= 2) {
            if(varReportstop.backupElem) {
                varReportstop.widgetObj.find('select#unit').html(varReportstop.backupElem);
                varReportstop.backupElem = '';
            }
        } else {
            varReportstop.backupElem = '';
            return false;       
        }
    });

    varReportstop.widgetObj.find('#unit').die('click').live('click', function (){
        var id = $(this).val();
        var label = $(this).find('option:selected').text();        
        var labelArr = label.match(/.{1,8}/g); //divide a string a cada 8 caracteres
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';


        if(id.length > 1){
            for (i = 0; i < id.length; i++) { 
                var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id[i]+'" title="Click para remover esse unidade">'+labelArr[i]+'</div>';
                var content = varReportstop.widgetObj.find('.selectedUnitList').html();
                if(divTodos == div){
                    varReportstop.widgetObj.find('.selectedUnitList').html(divTodos);
                    return false;
                }
                if((content.indexOf(div) < 0) && (div != divTodos)){
                    if(content == divTodos){
                        varReportstop.widgetObj.find('.selectedUnitList').html(div);
                    }else{
                        varReportstop.widgetObj.find('.selectedUnitList').append(div);
                    }
                }
            }
        }else{  
            var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id+'" title="Click para remover esse unidade">'+label+'</div>';
            var content = varReportstop.widgetObj.find('.selectedUnitList').html();
            if(divTodos == div){
                varReportstop.widgetObj.find('.selectedUnitList').html(divTodos);
                return false;
            }
            if((content.indexOf(div) < 0) && (div != divTodos)){
                if(content == divTodos){
                    varReportstop.widgetObj.find('.selectedUnitList').html(div);
                }else{
                    varReportstop.widgetObj.find('.selectedUnitList').append(div);
                }  
            }        
        }
    }); 

    varReportstop.widgetObj.find('.selectedUnit').die('click').live('click', function (){
        $(this).remove();
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';
        var content = varReportstop.widgetObj.find('.selectedUnitList').html();

        if(content == ''){
            varReportstop.widgetObj.find('.selectedUnitList').html(divTodos);
        }
    }); 

    varReportstop.widgetObj.find('.closeReportTab').die('click').live('click', function (){
        var id = $(this).attr('id');
        varReportstop.widgetObj.find('#tab-result-stop-'+id).empty().remove();
        varReportstop.widgetObj.find('#result-stop-'+id).empty().remove();
        varReportstop.widgetObj.find('#search-stop').trigger('click');
        window.setTimeout(function() {
            varReportstop.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled'); 
        }, 50);  
        varReportstop.dataFilterArr[id] = null;
        varReportstop.table[id]         = null; 
    });  

    varReportstop.widgetObj.find('#search').die('click').live('click', function (){
        varReportstop.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');     
    }); 

    // FORM TIPO DE CONTA
    varReportstop.widgetObj.find('form').submit(function(e) {
        e.preventDefault();
        if ( $(this).parsley().isValid() ) {       
            
            var unitIds = [];

            varReportstop.widgetObj.find(".selectedUnitList > div").each( function(index, value) {
                unitIds.push($(this).attr('id'));
            });
            
            var dataFilter = 
                {
                    start_day: $(this).find('#start_day').val(),
                    end_day: $(this).find('#end_day').val(),
                    start_hour: $(this).find('#start_hour').val(),
                    end_hour: $(this).find('#end_hour').val(),
                    group:  $(this).find('select#group').val(),
                    subgroup:  $(this).find('select#subgroup').val(),
                    units: unitIds,
                    limit: 0
                };
            dataFilter = JSON.stringify(dataFilter);
            
            $.ajax({
                async: true,
                type: 'POST',
                url: 'widgets/routing.php',
                data: { url: 'reportstop/search/', data: JSON.parse(dataFilter) },
                dataType: 'json',
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstop) != 'object'){ return false; }
                    loadingWidget('#'+varReportstop.widgetName);
                },
                success: function(data) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstop) != 'object'){ return false; }
                    varReportstop.dataFilterArr.push(dataFilter);
                    loadingWidget('#'+varReportstop.widgetName, true); 

                    varReportstop.widgetObj.find('.tab-content').append('<div class="col-md-12 tab-pane" id="tab-result-stop-'+varReportstop.tabResults+'"></div>');
                    varReportstop.widgetObj.find('#widgetsreportstop-nav').append('<li class=""><a href="#tab-result-stop-'+varReportstop.tabResults+'"  id="result-stop-'+varReportstop.tabResults+'" data-toggle="tab">Resultado <i class="fa fa-times closeReportTab" id="'+varReportstop.tabResults+'"></i></a></li>');

                    varReportstop.widgetObj.find('#result-stop-'+varReportstop.tabResults).die('click').live('click', function (){
                        var id = $(this).attr('id');
                        id = id.replace('result-stop-', '');

                        var dropDownMenu = '<li><a href="javascript:;" onClick="widgetsReportStop.print(\'print\', '+id+')">Imprimir</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportStop.print(\'PDF\', '+id+')">Exportar PDF</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportStop.print(\'CSV\', '+id+')">Exportar CSV</a></li>';

                        varReportstop.widgetObj.find('#widgetsreportstop-dropdown').html(dropDownMenu);   
                        varReportstop.widgetObj.find('.dropdown-toggle').removeAttr("disabled");                 
                    }); 
                    var numRows = varReportstop.rowSizeMin;
                    var panelWidget = varReportstop.widgetObj.closest('.panel');
                    if(panelWidget.hasClass('full-Scr')){
                        numRows = Math.round(panelWidget.height() / varReportstop.rowSizeMax) - varReportstop.rowAdjust;
                    }
                    varReportstop.widgetObj.find('#tab-result-stop-'+varReportstop.tabResults).html( '<table class="table table-striped table-bordered" data-page-length="'+numRows+'" id="table-list-result-stop-'+varReportstop.tabResults+'"></table>' );
                    varReportstop.table.push(null);
                    varReportstop.table[varReportstop.tabResults] = varReportstop.widgetObj.find('#table-list-result-stop-'+varReportstop.tabResults).DataTable({
                        "data": data,
                        paginate: true,
                        bLengthChange: false,
                        bFilter: false,
                        bInfo: true,
                        "ordering": true,
                        "pagingType": "full",
                        "fnDrawCallback": function( oSettings ) {
                            window.setTimeout(function() {
                                buildRevGeoFromTable(varReportstop.widgetName);
                            }, 500);
                        },
                        "columns": 
                            [
                                { "title": "Placa", "data": "label", "className": "larguraPlaca" },
                                { "title": "Hora&nbsp;Inicial", "data": "data_inicio", "className": "10_p" },
                                { "title": "Hora&nbsp;Final", "data": "data_fim", "className": "10_p" },
                                { "title": "Tempo", "data": "total_time", "className": "10_p" },
                                { "title": "Local", "data": "initial_full_address", "className": "30_p" },
                                { "title": "POI", "data": "initial_poi_name", "className": "15_p" },
                                { "title": "Cerca", "data": "initial_area_name", "className": "15_p" },
                                { "title": "Date Order", "data": "full_date_begin", "visible": false }
                            ],
                        "columnDefs": 
                            [
                                {
                                    "targets": [0],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" long="'+full.initial_longitude+'" lat="'+full.initial_latitude+'">' + data + '</div>';
                                    }
                                },
                                {
                                    "targets": [1],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + full.data_inicio + ' às ' + full.hora_min_inicio + '">' + full.data_inicio + ' às ' + full.hora_min_inicio + '</div>';
                                    }
                                },
                                {
                                    "targets": [2],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        if(full.data_fim == null){
                                            return '<div class="nowrap ellipsis"> -- </div>'; 
                                        }else{
                                            return '<div class="nowrap ellipsis" title="' + full.data_fim + ' às ' + full.hora_min_fim + '">' + full.data_fim + ' às ' + full.hora_min_fim + '</div>';                                            
                                        }
                                    }
                                },
                                {
                                    "targets": [3],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        if(data == 0){
                                            return '<div class="nowrap ellipsis"> -- </div>'; 
                                        }else{
                                            return '<div class="nowrap ellipsis" title="' + minutesToHMS(data * 60) + '">' + minutesToHMS(data * 60) + '</div>';
                                        }
                                    }
                                },
                                {
                                    "targets": [4],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        //verifica se este endereco ja existe no banco
                                        if(data != '') {
                                            //se sim ja retorna o endereco
                                            return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                        } else {
                                            //se nao faz o reversegeo para capturar o endereco e salvar no banco
                                            return "<div class='mapAddress nowrap ellipsis' hist='"+full.devst_id+"' long='"+full.initial_longitude+"' lat='"+full.initial_latitude+"'>Carregando...</div>" ;
                                        }
                                    }
                                },
                                {
                                    "targets": [5],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                    }
                                },
                                {
                                    "targets": [6],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                    }
                                }
                            ],
                        "language" : {
                            "emptyTable":     "Ainda não há registros",
                            "info" : "Total de _TOTAL_ registros",
                            "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
                            "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
                            "infoPostFix":    "",
                            "thousands":      ".",
                            "lengthMenu":     "Mostre _MENU_ registros",
                            "loadingRecords": "Carregando...",
                            "processing":     "Processando...",
                            "search":         "",
                            "searchPlaceholder": "Procure por unidade...",
                            "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
                            "paginate": {
                                "first":      "<<",
                                "last":       ">>",
                                "next":       ">",
                                "previous":   "<"
                            }
                        },
                        "order": [[ 7, 'desc' ], [ 0, 'asc' ], [ 1, 'desc' ]],
                        "initComplete": function(settings) {
                            if( data.length > 0){
                                varReportstop.api.push(this.api());

                                varReportstop.widgetObj.find('.larguraData').css('width', '120px');
                                varReportstop.widgetObj.find('.larguraPlaca').css('width', '80px');
                                varReportstop.widgetObj.find('.10_p').css('width', '10%');
                                varReportstop.widgetObj.find('.15_p').css('width', '15%');
                                varReportstop.widgetObj.find('.30_p').css('width', '30%');

                                dataFilter = JSON.parse(dataFilter);
                                dataFilter.limit += varReportstop.processingSearchLimit;
                                if( data.length >= varReportstop.processingSearchLimit ){
                                    widgetsReportStop.processingSearch(varReportstop.tabResults, dataFilter);
                                } 
                                
                                varReportstop.widgetObj.find('#table-list-result-stop-'+varReportstop.tabResults+' tbody tr').die("click").live("click", function(){          
                                    var label = $(this).children('td').html();
                                    var lat = $(label).attr('lat');
                                    var lng = $(label).attr('long');
                                    label = $(label).html();

                                    var drawMark = [];
                                        drawMark.size = varGlobal.marker.Size;
                                        drawMark.symbol = varGlobal.marker.carSymbol;
                                        drawMark.color = varGlobal.marker.color;

                                    widgetsReportStop.openMap(lat, lng, drawMark);
                                }); 
                            }
                        }
                    });              
                    //reseta o form   
                    varReportstop.widgetObj.find('#result-stop-'+varReportstop.tabResults).trigger('click');  
                    varReportstop.tabResults++;
                    //resetReportAction(); 
                },
                error: function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstop) != 'object'){ return false; }
                    loadingWidget('#'+varReportstop.widgetName, true);
                    //Criando mensagem de retorno
                    createMsgBox('error', error.responseText, '#'+varReportstop.widgetName+' .panel-body'); 
                }
            });
            return false;
        }
    }); 
});

