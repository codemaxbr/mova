var varReporthidro = {};
    varReporthidro.widgetName   = 'widgetsreporthidro';
    varReporthidro.widgetObj    = $('#'+varReporthidro.widgetName);
    varReporthidro.numEventsG   = 0;
    varReporthidro.numEvents    = 0;
    varReporthidro.numSearchs   = 0;
    varReporthidro.backupElem   = '';
    varReporthidro.tabResults   = 0;
    varReporthidro.table        = [];
    varReporthidro.api           = [];
    varReporthidro.dataFilterArr = [];
    varReporthidro.processingSearchLimit = 1000;
    varReporthidro.callMap;

    varReporthidro.rowSizeMax = 28;
    varReporthidro.rowSizeMin = 10;
    varReporthidro.rowAdjust = 5;

var widgetsReportHidro = {    
    buildGroup: function (data){
        varReporthidro.widgetObj.find('#group').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varReporthidro.widgetObj.find('#group').append(select);
    },
    buildSubGroup: function (data){
        varReporthidro.widgetObj.find('#subgroup').html('<option value="0" selected>Todos</option>');
        var template =  '{{#subgroupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/subgroupinfo}}';
        var select = Mustache.to_html(template, data);
        varReporthidro.widgetObj.find('#subgroup').append(select);
    },
    processingSearch: function (idTab, dataFilter){
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reporthidro/search/', data: dataFilter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }

                varReporthidro.widgetObj.find('#table-list-result-hidro-'+idTab+'_info').append('<span class="sp-load-'+idTab+'" title="Carregando consulta..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                //para de processar caso a tab seja fechada
                if(varReporthidro.table[idTab] == null ){ return false; }
                
                $(json).each(function(i, rows) {                
                    varReporthidro.table[idTab].row.add({
                        "label": rows.label, 
                        "data_inicio": rows.data_inicio,  
                        "data_report_inicio": rows.data_report_inicio,
                        "hora_min_inicio": rows.hora_min_inicio, 
                        "data_fim": rows.data_fim,  
                        "data_report_fim": rows.data_report_fim,
                        "hora_min_fim": rows.hora_min_fim,  
                        "longitude": rows.longitude, 
                        "latitude": rows.latitude, 
                        "full_address": rows.full_address, 
                        "devst_id": rows.id, 
                        "poi_name": rows.poi_name, 
                        "qtd": rows.qtd, 
                        "total_time": rows.total_time, 
                        "full_date_begin": rows.full_date_begin
                    });
                }); 
                varReporthidro.table[idTab].draw(false);
                //reinicia o geocode caso haja algum registro para ser carregado
                buildRevGeoFromTable(varReporthidro.widgetName);
                //verifica se ainda existem registros para serem buscados
                if( json.length >= varReporthidro.processingSearchLimit ){
                    dataFilter.limit += varReporthidro.processingSearchLimit;

                    window.setTimeout(function() {
                        widgetsReportHidro.processingSearch(idTab, dataFilter); 
                    }, 50);
                }    
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReporthidro.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                varReporthidro.widgetObj.find('.sp-load-'+idTab).remove();
            }
        });
    },
    print: function (type, id){
        var filter = JSON.parse(varReporthidro.dataFilterArr[id]);
        //removendo limit para retornar todos os dados quando for´exportação
        filter.limit = null;

        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reporthidro/search/', data: filter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                varReporthidro.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
                varReporthidro.widgetObj.find('.actionMenu .fristAction').append('<span class="sp-load" title="Carregando exportação..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                switch (type) { 
                    case 'print': 
                        var content = '<style>';
                        content += '    table { border: 1px solid #999; border-bottom: none; width: 100%; }';
                        content += '    thead { border: 5px solid #000; }';
                        content += '    tbody td { border: 1px solid #ccc; }';
                        content += '</style>';        
                        content += '<table><thead"><th>Placa</th><th>Data</th><th>Início</th><th>Fim</th><th>Tempo Total</th><th>Local</th><th>Quantidade</th></thead><tbody>';
                        
                        $(json).each(function(i, row) {
                            content += '<tr><td>'+row.label+'</td><td>'+row.data_report_inicio+'</td><td>'+row.hora_min_inicio+'</td><td>'+row.hora_min_fim+'</td><td>'+row.total_time+' min.</td>';
                            content += '<td>'+row.full_address+'</td><td>'+row.qtd+' L</td></tr>';
                        });            
                        content += '</tbody></table>';

                        var w = window.open("");

                        w.document.write(content);
                        w.print();
                        w.close();

                        break;
                    case 'PDF': 
                        var columns = ["Placa", "Data", "Início", "Fim", "Tempo", "Local", "Qtd."];
                        var rows = [];
                        $(json).each(function(i, row) {
                            rows.push([row.label, row.data_report_inicio, row.hora_min_inicio, row.hora_min_fim, row.total_time+' min.', row.full_address, row.qtd+' L']);
                        });   
                        var doc = new jsPDF('l', 'pt');
                        var dataCurrente = new Date();
                        var year = dataCurrente.getFullYear();
                        var month = dataCurrente.getMonth() + 1;
                        var day = dataCurrente.getDate();

                        doc.autoTable(columns, rows, {
                            theme: 'striped', // 'striped', 'grid' or 'plain'
                            headerStyles: {
                                fillColor: [0, 147, 147],
                                color: [255, 255, 255]
                            },
                            bodyStyles: {
                                overflow: 'linebreak', // visible, hidden, ellipsize or linebreak
                            },
                            margin: {
                                top: 100
                            },
                            beforePageContent: function(data) {
                                var imgData = varGlobal.imgRelLogo;
                                doc.addImage(imgData, 'PNG', 40, 30);
                                doc.text("Relatório de Descargas", 40, 90);
                            }
                        });
                        doc.save("Relatorio_de_Descargas"+year+"-"+month+"-"+day+".pdf");

                        break;
                    case 'CSV': 
                        var content = 'Placa;Data;Início;Fim;Tempo Total;Local;Quantidade\r\n';
                        
                        $(json).each(function(i, row) {
                            content += row.label+';'+row.data_report_inicio+';'+row.hora_min_inicio+';'+row.hora_min_fim+';'+row.total_time+' min.;'+row.full_address+';'+row.qtd+' L\r\n';
                        });           

                        var a         = document.createElement('a');
                        a.href        = 'data:attachment/csv,' +  escape(content);
                        a.target      = '_blank';
                        a.download    = 'Relatório_de_Descarga.csv';

                        document.body.appendChild(a);
                        a.click();

                        break;     
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReporthidro.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                varReporthidro.widgetObj.find('.dropdown-toggle').removeAttr("disabled"); 
                varReporthidro.widgetObj.find('.sp-load').remove(); 
            }
        }); 
    },
    openMap: function (lat, lng, drawMark){
        if(typeof(varMapBox) != 'object'){
            handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 4);

            clearTimeout(varReporthidro.callMap);
            varReporthidro.callMap = window.setTimeout(function() {
                widgetsReportHidro.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }
        if(typeof(varMapBox.mapInstance) != 'object'){
            clearTimeout(varReporthidro.callMap);
            varReporthidro.callMap = window.setTimeout(function() {
                widgetsReportHidro.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }

        MapBox.centerUnit(lat, lng, false, null, drawMark);
    }
};

function load_widgetsreporthidro_Page(){
    $.ajax({
        type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'reporthidro/index/'},
        dataType: 'json',
        beforeSend: function () {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReporthidro) != 'object'){ return false; }
            varReporthidro.widgetObj.find('.btn').attr('disabled', 'disabled');
            varReporthidro.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReporthidro) != 'object'){ return false; }

            widgetsReportHidro.buildGroup(JSON.parse(data.groupinfo))
            widgetsReportHidro.buildSubGroup(JSON.parse(data.subgroupinfo))

            varReporthidro.widgetObj.find('form').find('#start_day').val(data.date);
            varReporthidro.widgetObj.find('form').find('#end_day').val(data.date);
            varReporthidro.widgetObj.find('form').find('#end_hour').val(data.hour);

            varReporthidro.widgetObj.find('form').removeClass('hidden');
            varReporthidro.widgetObj.find('.load-report').addClass('hidden');
            //liberando menu
            varReporthidro.widgetObj.find('.btn').removeAttr("disabled");
            loadingWidget('#'+varReporthidro.widgetName, true); 
            varReporthidro.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');

        },
        error: function(error) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReporthidro) != 'object'){ return false; }
            createMsgBox('error', error.responseText, '#'+varReporthidro.widgetName+' .panel-body');
        },
        complete: function() {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReporthidro) != 'object'){ return false; }
            varReporthidro.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
    load_widgetsreporthidro_Page();

    //Plugin Clockpicker
    varReporthidro.widgetObj.find('.clockpicker').clockpicker({
        autoclose: true,
        'default': 'now'
    });    
    //Datepicker
    varReporthidro.widgetObj.find('.pluginDate').datepicker({ 
        autoclose: true, 
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        todayHighlight: true,
    });

    varReporthidro.widgetObj.find('select#group').on('change', function() {
        var select = 'select#subgroup';
        var data = 
        {
            groupIds: $(this).val()
        };
        varReporthidro.numEventsG++;
        data = JSON.stringify(data);

        var html = '';
        var selected = 'selected';
        if($(this).val() == 0) {
            html += '<option value="0" selected>Todos</option>';
            selected  = '';
        }
        
        $.ajax({
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'client/listSubGroups/', data:JSON.parse(data) },
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                varReporthidro.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                varReporthidro.widgetObj.find('select#unit').addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                try {
                    var subgroups = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReporthidro.widgetName+' .panel-body');              
                    varReporthidro.widgetObj.find(select).html('');
                    varReporthidro.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                
                $(subgroups).each(function(i, val) {
                    html+='<option value="'+val.id+'" '+selected+'>'+val.name+'</option>';
                });

                varReporthidro.numEventsG--;
                if(varReporthidro.numEventsG == 0) {
                    varReporthidro.widgetObj.find(select).removeClass('select-loader');
                    varReporthidro.widgetObj.find(select).html(html);
                    varReporthidro.widgetObj.find('select#subgroup').change();
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                createMsgBox('error', error.responseText,'#'+varReporthidro.widgetName+' .panel-body');                 
                varReporthidro.widgetObj.find(select).html(error.responseText);
                varReporthidro.widgetObj.find(select).removeClass('select-loader');
            }
        });     
    });
    
    varReporthidro.widgetObj.find('select#subgroup').on('change', function() {
        var select = 'select#unit';

        var html = '';      
        if($(this).val() == '0') {
            html += '<option value="0">Todos</option>';
        }
        var data = 
        {
            subGroupIds: $(this).val()
        };
        varReporthidro.numEvents++;
        
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'unit/listTrackedUnit/', data:JSON.parse(data) },
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                varReporthidro.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                try {
                    var unities = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReporthidro.widgetName+' .panel-body');              
                    varReporthidro.widgetObj.find(select).html('');
                    varReporthidro.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                html = '<option value="0" selected>Todos</option>';
                $(unities).each(function(i, val) {
                    if(val.label != undefined){
                        html += '<option value="'+val.id+'">'+val.label+'</option>';
                    }
                });
                
                varReporthidro.numEvents--;
                if(varReporthidro.numEvents == 0) {
                    varReporthidro.widgetObj.find(select).removeClass('select-loader');
                    varReporthidro.widgetObj.find(select).html(html);
                }                   
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReporthidro) != 'object'){ return false; }
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varReporthidro.widgetName+' .panel-body');                 
                varReporthidro.widgetObj.find(select).html(error.responseText);
                varReporthidro.widgetObj.find(select).removeClass('select-loader');
            }
        });
    });

    varReporthidro.widgetObj.find('input#searchUnits').on('keyup', function(e) {
        e.preventDefault();
        if(varReporthidro.backupElem == '') {
            varReporthidro.backupElem = varReporthidro.widgetObj.find('select#unit').html();
        }
        if($(this).val().length > 2) {
            var select = 'select#unit';
            var html = '';
            var data = 
            {
                unit: $(this).val()
            };
            data = JSON.stringify(data);
            varReporthidro.numSearchs++;
             $.ajax({
                async : true,
                type : "POST",
                data: { url: 'unit/searchUnit', data: JSON.parse(data) },
                url : 'widgets/routing.php',
                dataType : "json",
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReporthidro) != 'object'){ return false; }
                    varReporthidro.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                },
                success : function(json) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReporthidro) != 'object'){ return false; }
                    varReporthidro.numSearchs--;
                    if(json.length <= 0) {
                        if(varReporthidro.numSearchs == 0) {
                            varReporthidro.widgetObj.find(select).html('');
                            varReporthidro.widgetObj.find(select).removeClass('select-loader');
                            return false;
                        }
                    }
                    html+='<option value="0" selected>Todos</option>';
                    $(json).each(function(i, val) {
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    });
                    if(varReporthidro.numSearchs == 0) {
                        varReporthidro.widgetObj.find(select).removeClass('select-loader');
                        varReporthidro.widgetObj.find(select).html(html);
                    }
                },
                error : function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReporthidro) != 'object'){ return false; }
                    varReporthidro.numSearchs--;
                    if(varReporthidro.numSearchs == 0) {
                        createMsgBox('error', error.responseText, "#"+varReporthidro.widgetName+" .panel-body");
                        varReporthidro.widgetObj.find(select).removeClass('select-loader');
                    }
                }
            });
        } else if($(this).val().length <= 2) {
            if(varReporthidro.backupElem) {
                varReporthidro.widgetObj.find('select#unit').html(varReporthidro.backupElem);
                varReporthidro.backupElem = '';
            }
        } else {
            varReporthidro.backupElem = '';
            return false;       
        }
    });

    varReporthidro.widgetObj.find('#unit').die('click').live('click', function (){
        var id = $(this).val();
        var label = $(this).find('option:selected').text();        
        var labelArr = label.match(/.{1,8}/g); //divide a string a cada 8 caracteres
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';

        if(id.length > 1){
            for (i = 0; i < id.length; i++) { 
                var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id[i]+'" title="Click para remover esse unidade">'+labelArr[i]+'</div>';
                var content = varReporthidro.widgetObj.find('.selectedUnitList').html();
                if(divTodos == div){
                    varReporthidro.widgetObj.find('.selectedUnitList').html(divTodos);
                    return false;
                }
                if((content.indexOf(div) < 0) && (div != divTodos)){
                    if(content == divTodos){
                        varReporthidro.widgetObj.find('.selectedUnitList').html(div);
                    }else{
                        varReporthidro.widgetObj.find('.selectedUnitList').append(div);
                    }
                }
            }
        }else{  
            var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id+'" title="Click para remover esse unidade">'+label+'</div>';
            var content = varReporthidro.widgetObj.find('.selectedUnitList').html();
            if(divTodos == div){
                varReporthidro.widgetObj.find('.selectedUnitList').html(divTodos);
                return false;
            }
            if((content.indexOf(div) < 0) && (div != divTodos)){
                if(content == divTodos){
                    varReporthidro.widgetObj.find('.selectedUnitList').html(div);
                }else{
                    varReporthidro.widgetObj.find('.selectedUnitList').append(div);
                }  
            }        
        }

    }); 

    varReporthidro.widgetObj.find('.selectedUnit').die('click').live('click', function (){
        $(this).remove();
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';
        var content = varReporthidro.widgetObj.find('.selectedUnitList').html();

        if(content == ''){
            varReporthidro.widgetObj.find('.selectedUnitList').html(divTodos);
        }
    }); 

    varReporthidro.widgetObj.find('.closeReportTab').die('click').live('click', function (){
        var id = $(this).attr('id');
        varReporthidro.widgetObj.find('#tab-result-hidro-'+id).empty().remove();
        varReporthidro.widgetObj.find('#result-hidro-'+id).empty().remove();
        varReporthidro.widgetObj.find('#search-hidro').trigger('click');
        window.setTimeout(function() {
            varReporthidro.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled'); 
        }, 50);  
        varReporthidro.dataFilterArr[id] = null;
        varReporthidro.table[id]         = null; 
    });  

    varReporthidro.widgetObj.find('#search').die('click').live('click', function (){
        varReporthidro.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');     
    }); 

    // FORM TIPO DE CONTA
    varReporthidro.widgetObj.find('form').submit(function(e) {
        e.preventDefault();
        if ( $(this).parsley().isValid() ) {       
            
            var unitIds = [];

            varReporthidro.widgetObj.find(".selectedUnitList > div").each( function(index, value) {
                unitIds.push($(this).attr('id'));
            });
            
            var dataFilter = 
                {
                    start_day: $(this).find('#start_day').val(),
                    end_day: $(this).find('#end_day').val(),
                    start_hour: $(this).find('#start_hour').val(),
                    end_hour: $(this).find('#end_hour').val(),
                    group:  $(this).find('select#group').val(),
                    subgroup:  $(this).find('select#subgroup').val(),
                    units: unitIds,
                    limit: 0
                };
            dataFilter = JSON.stringify(dataFilter);
            
            $.ajax({
                async: true,
                type: 'POST',
                url: 'widgets/routing.php',
                data: { url: 'reporthidro/search/', data: JSON.parse(dataFilter) },
                dataType: 'json',
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReporthidro) != 'object'){ return false; }
                    loadingWidget('#'+varReporthidro.widgetName);
                },
                success: function(data) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReporthidro) != 'object'){ return false; }
                    varReporthidro.dataFilterArr.push(dataFilter);
                    loadingWidget('#'+varReporthidro.widgetName, true); 

                    varReporthidro.widgetObj.find('.tab-content').append('<div class="col-md-12 tab-pane" id="tab-result-hidro-'+varReporthidro.tabResults+'"></div>');
                    varReporthidro.widgetObj.find('#widgetsreporthidro-nav').append('<li class=""><a href="#tab-result-hidro-'+varReporthidro.tabResults+'"  id="result-hidro-'+varReporthidro.tabResults+'" data-toggle="tab">Resultado <i class="fa fa-times closeReportTab" id="'+varReporthidro.tabResults+'"></i></a></li>');

                    varReporthidro.widgetObj.find('#result-hidro-'+varReporthidro.tabResults).die('click').live('click', function (){
                        var id = $(this).attr('id');
                        id = id.replace('result-hidro-', '');

                        var dropDownMenu = '<li><a href="javascript:;" onClick="widgetsReportHidro.print(\'print\', '+id+')">Imprimir</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportHidro.print(\'PDF\', '+id+')">Exportar PDF</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportHidro.print(\'CSV\', '+id+')">Exportar CSV</a></li>';

                        varReporthidro.widgetObj.find('#widgetsreporthidro-dropdown').html(dropDownMenu);   
                        varReporthidro.widgetObj.find('.dropdown-toggle').removeAttr("disabled");                 
                    }); 
                    var numRows = varReporthidro.rowSizeMin;
                    var panelWidget = varReporthidro.widgetObj.closest('.panel');
                    if(panelWidget.hasClass('full-Scr')){
                        numRows = Math.round(panelWidget.height() / varReporthidro.rowSizeMax) - varReporthidro.rowAdjust;
                    }
                    varReporthidro.widgetObj.find('#tab-result-hidro-'+varReporthidro.tabResults).html( '<table class="table table-striped table-bordered" data-page-length="'+numRows+'" id="table-list-result-hidro-'+varReporthidro.tabResults+'"></table>' );
                    varReporthidro.table.push(null);
                    varReporthidro.table[varReporthidro.tabResults] = varReporthidro.widgetObj.find('#table-list-result-hidro-'+varReporthidro.tabResults).DataTable({
                        "data": data,
                        paginate: true,
                        bLengthChange: false,
                        bFilter: false,
                        bInfo: true,
                        "ordering": true,
                        "pagingType": "full",
                        "fnDrawCallback": function( oSettings ) {
                            window.setTimeout(function() {
                                buildRevGeoFromTable(varReporthidro.widgetName);
                            }, 500);
                        },
                        "columns": 
                            [
                                { "title": "<div class='labelHeader'>Placa</div>", "data": "label", "className": "10_p" },
                                { "title": "<div class='labelHeader'>Data</div>", "data": "data_inicio", "className": "10_p" },
                                { "title": "<div class='labelHeader'>Início</div>", "data": "data_inicio", "className": "10_p" },
                                { "title": "<div class='labelHeader'>Fim</div>", "data": "data_fim", "className": "10_p" },
                                { "title": "<div class='labelHeader'>Tempo</div>", "data": "total_time", "className": "10_p" },
                                { "title": "<div class='labelHeader'>Local</div>", "data": "full_address", "className": "40_p" },
                                { "title": "<div class='labelHeader'>Qtd.</div>", "data": "qtd", "className": "10_p" },
                                { "title": "Date Order", "data": "full_date_begin", "visible": false }
                            ],
                        "columnDefs": 
                            [
                                {
                                    "targets": [0],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" long="'+full.longitude+'" lat="'+full.latitude+'">' + data + '</div>';
                                    }
                                },
                                {
                                    "targets": [1],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + full.data_inicio + '">' + full.data_inicio + '</div>';
                                    }
                                },
                                {
                                    "targets": [2],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + full.hora_min_inicio + '">' + full.hora_min_inicio + '</div>';
                                    }
                                },
                                {
                                    "targets": [3],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        if(full.hora_min_fim == null){
                                            return '<div class="nowrap ellipsis"> -- </div>'; 
                                        }else{
                                            return '<div class="nowrap ellipsis" title="' + full.hora_min_fim + '">' + full.hora_min_fim + '</div>';
                                        }
                                    }
                                },
                                {
                                    "targets": [4],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        if(data == 0){
                                            return '<div class="nowrap ellipsis"> -- </div>'; 
                                        }else{
                                            return '<div class="nowrap ellipsis" title="' + minutesToHMS(data * 60) + '">' + minutesToHMS(data * 60) + '</div>';
                                        }
                                    }
                                },
                                {
                                    "targets": [5],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        //verifica se este endereco ja existe no banco
                                        if(data != '') {
                                            //se sim ja retorna o endereco
                                            return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                        } else {
                                            //se nao faz o reversegeo para capturar o endereco e salvar no banco
                                            return "<div class='mapAddress nowrap ellipsis' hist='"+full.devst_id+"' long='"+full.longitude+"' lat='"+full.latitude+"'>Carregando...</div>" ;
                                        }
                                    }
                                },
                                {
                                    "targets": [6],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + ' L</div>';
                                    }
                                }
                            ],
                        "language" : {
                            "emptyTable":     "Ainda não há registros",
                            "info" : "Total de _TOTAL_ registros",
                            "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
                            "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
                            "infoPostFix":    "",
                            "thousands":      ".",
                            "lengthMenu":     "Mostre _MENU_ registros",
                            "loadingRecords": "Carregando...",
                            "processing":     "Processando...",
                            "search":         "",
                            "searchPlaceholder": "Procure por unidade...",
                            "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
                            "paginate": {
                                "first":      "<<",
                                "last":       ">>",
                                "next":       ">",
                                "previous":   "<"
                            }
                        },
                        "order": [[ 7, 'desc' ], [ 0, 'asc' ], [ 1, 'desc' ]],
                        "initComplete": function(settings) {
                            if( data.length > 0){
                                varReporthidro.api.push(this.api());

                                varReporthidro.widgetObj.find('.10_p').css('width', '10%');
                                varReporthidro.widgetObj.find('.40_p').css('width', '40%');


                                dataFilter = JSON.parse(dataFilter);
                                dataFilter.limit += varReporthidro.processingSearchLimit;
                                if( data.length >= varReporthidro.processingSearchLimit ){
                                    widgetsReportHidro.processingSearch(varReporthidro.tabResults, dataFilter);
                                } 
                                
                                varReporthidro.widgetObj.find('#table-list-result-hidro-'+varReporthidro.tabResults+' tbody tr').die("click").live("click", function(){        
                                    var label = $(this).children('td').html();
                                    var lat = $(label).attr('lat');
                                    var lng = $(label).attr('long');
                                    label = $(label).html();

                                    var drawMark = [];
                                        drawMark.size = varGlobal.marker.Size;
                                        drawMark.symbol = varGlobal.marker.carSymbol;
                                        drawMark.color = varGlobal.marker.color;

                                    widgetsReportHidro.openMap(lat, lng, drawMark);
                                }); 
                            }
                        }
                    });              
                    //reseta o form   
                    varReporthidro.widgetObj.find('#result-hidro-'+varReporthidro.tabResults).trigger('click');  
                    varReporthidro.tabResults++;
                    //resetReportAction(); 
                },
                error: function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReporthidro) != 'object'){ return false; }
                    loadingWidget('#'+varReporthidro.widgetName, true);
                    //Criando mensagem de retorno
                    createMsgBox('error', error.responseText, '#'+varReporthidro.widgetName+' .panel-body'); 
                }
            });
            return false;
        }
    }); 

});


