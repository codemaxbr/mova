var varChecklistvelleda = {};
	varChecklistvelleda.widgetName 	= 'widgetschecklistvelleda';
	varChecklistvelleda.varName 	= 'varChecklistvelleda';
	varChecklistvelleda.widgetObj 	= $('#'+varChecklistvelleda.widgetName);
	varChecklistvelleda.action 		= 'edit';


var widgetschecklistvelleda = {
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	loadEditForm: function (data) {
    	if(typeof(varChecklistvelleda) != 'object'){return false;}
		var checklistvelleda = data.data;
		var form = varChecklistvelleda.widgetObj.find('form');
		//limpa formulário
		widgetschecklistvelleda.reset();
		console.log(checklistvelleda);

		form.find('#dataatual').val(dateToBr(checklistvelleda.dataatual));
		form.find('#placacavalo').val(checklistvelleda.placacavalo);
		form.find('#origem').val(checklistvelleda.origem);
		form.find('#destino').val(checklistvelleda.destino);
		form.find('#motorista').val(checklistvelleda.motorista);
		form.find('#nlacre').val(checklistvelleda.nlacre);
		form.find('#nnf').val(checklistvelleda.nnf);
		form.find('#hsaida').val(checklistvelleda.hsaida);
		form.find('#local').val(checklistvelleda.local);
		form.find('#placacarreta').val(checklistvelleda.placacarreta);
		form.find('#transp').val(checklistvelleda.transp);
		form.find('#tipoveiculo').val(checklistvelleda.tipoveiculo);
		form.find('#ncnh').val(checklistvelleda.ncnh);
		form.find('#datavalidade').val(dateToBr(checklistvelleda.datavalidade));
		form.find('#preventrega').val(dateToBr(checklistvelleda.preventrega));
		form.find('#tempotermo').val(checklistvelleda.tempotermo);
		form.find('#observacoes').val(checklistvelleda.observacoes);
		form.find('#datatransm').val(dateToBr(checklistvelleda.datatransm));
		form.find('#datasaida').val(dateToBr(checklistvelleda.datasaida));
		form.find('#horachegada').val(checklistvelleda.horachegada);

		form.find('i.fa').removeClass('fa-toggle-on');
		form.find('i.fa').removeClass('fa-toggle-off');

		checklistvelleda.camboio ? form.find('.camboio i.fa').addClass('fa-toggle-on') : form.find('.camboio i.fa').addClass('fa-toggle-off');
		checklistvelleda.pneus ? form.find('.pneus i.fa').addClass('fa-toggle-on') : form.find('.pneus i.fa').addClass('fa-toggle-off');
		checklistvelleda.eletrica ? form.find('.eletrica i.fa').addClass('fa-toggle-on') : form.find('.eletrica i.fa').addClass('fa-toggle-off');
		checklistvelleda.condgerais ? form.find('.condgerais i.fa').addClass('fa-toggle-on') : form.find('.condgerais i.fa').addClass('fa-toggle-off');
		checklistvelleda.estepes ? form.find('.estepes i.fa').addClass('fa-toggle-on') : form.find('.estepes i.fa').addClass('fa-toggle-off');
		checklistvelleda.estintorvalido ? form.find('.estintorvalido i.fa').addClass('fa-toggle-on') : form.find('.estintorvalido i.fa').addClass('fa-toggle-off');
		checklistvelleda.cadeadobau ? form.find('.cadeadobau i.fa').addClass('fa-toggle-on') : form.find('.cadeadobau i.fa').addClass('fa-toggle-off');
		checklistvelleda.termoking ? form.find('.termoking i.fa').addClass('fa-toggle-on') : form.find('.termoking i.fa').addClass('fa-toggle-off');
		checklistvelleda.abastecimento ? form.find('.abastecimento i.fa').addClass('fa-toggle-on') : form.find('.abastecimento i.fa').addClass('fa-toggle-off');
		checklistvelleda.sensoresportas ? form.find('.sensoresportas i.fa').addClass('fa-toggle-on') : form.find('.sensoresportas i.fa').addClass('fa-toggle-off');
		checklistvelleda.sensorbau ? form.find('.sensorbau i.fa').addClass('fa-toggle-on') : form.find('.sensorbau i.fa').addClass('fa-toggle-off');
		checklistvelleda.bloqueio ? form.find('.bloqueio i.fa').addClass('fa-toggle-on') : form.find('.bloqueio i.fa').addClass('fa-toggle-off');
		checklistvelleda.sensordesengate ? form.find('.sensordesengate i.fa').addClass('fa-toggle-on') : form.find('.sensordesengate i.fa').addClass('fa-toggle-off');
		checklistvelleda.sirene ? form.find('.sirene i.fa').addClass('fa-toggle-on') : form.find('.sirene i.fa').addClass('fa-toggle-off');
		checklistvelleda.travabau ? form.find('.travabau i.fa').addClass('fa-toggle-on') : form.find('.travabau i.fa').addClass('fa-toggle-off');
		checklistvelleda.procedimentojbs ? form.find('.procedimentojbs i.fa').addClass('fa-toggle-on') : form.find('.procedimentojbs i.fa').addClass('fa-toggle-off');
		checklistvelleda.conhecimentoroda ? form.find('.conhecimentoroda i.fa').addClass('fa-toggle-on') : form.find('.conhecimentoroda i.fa').addClass('fa-toggle-off');
		checklistvelleda.manuseiorastreador ? form.find('.manuseiorastreador i.fa').addClass('fa-toggle-on') : form.find('.manuseiorastreador i.fa').addClass('fa-toggle-off');

    	//liberando menu
    	varChecklistvelleda.widgetObj.find('.btn').removeAttr("disabled");
    	loadingWidget('#'+varChecklistvelleda.widgetName, true);
	},
	reset: function (){
    	if(typeof(varChecklistvelleda) != 'object'){return false;}
		var form = varChecklistvelleda.widgetObj.find('form');
		
		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');

		form[0].reset();

		form.find('.camboio i.fa').removeClass('fa-toggle-on');
		form.find('.camboio i.fa').addClass('fa-toggle-off');
	},
	buildTipoVeiculo: function (data){
		var dataSelect = {
			tipoveiculoinfo : data.data
		};
		varChecklistvelleda.widgetObj.find('#tipoveiculo').html('<option value="">Selecione um Tipo de Veículo</option>');
		var template = 	'{{#tipoveiculoinfo}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/tipoveiculoinfo}}';
		var select = Mustache.to_html(template, dataSelect);
		varChecklistvelleda.widgetObj.find('#tipoveiculo').append(select);
	},
	buildTransportador: function (data){
		var dataSelect = {
			shippingCompanyList : data.data
		};
		varChecklistvelleda.widgetObj.find('#transp').html('<option value="">Selecione uma Transportadora</option>');
		var template = 	'{{#shippingCompanyList}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/shippingCompanyList}}';
		var select = Mustache.to_html(template, dataSelect);
		varChecklistvelleda.widgetObj.find('#transp').append(select);
	},
	executeOrder: function (form, status) {
    	if(typeof(varChecklistvelleda) != 'object'){return false;}
	    
	    var data = 
			{
				status : status,
    			id: form.find('#id').val(),
    			user_id: form.find('#user_id').val(),
    			account_id: form.find('#account_id').val(),
				dataatual : dateToEn(form.find('#dataatual').val()),
				placacavalo : form.find('#placacavalo').val(),
				origem : form.find('#origem').val(),
				destino : form.find('#destino').val(),
				motorista : form.find('#motorista').val(),
				nlacre : form.find('#nlacre').val(),
				nnf : form.find('#nnf').val(),
				hsaida : form.find('#hsaida').val(),
				local : form.find('#local').val(),
				placacarreta : form.find('#placacarreta').val(),
				transp : form.find('#transp').val(),
				tipoveiculo : form.find('#tipoveiculo').val(),
				ncnh : form.find('#ncnh').val(),
				datavalidade : dateToEn(form.find('#datavalidade').val()),
				preventrega : form.find('#preventrega').val(),
				tempotermo : form.find('#tempotermo').val(),
				observacoes : form.find('#observacoes').val(),
				datatransm : dateToEn(form.find('#datatransm').val()),
				datasaida : dateToEn(form.find('#datasaida').val()),
				horachegada : form.find('#horachegada').val(),

				camboio : form.find('.camboio i.fa').hasClass('fa-toggle-on') ? true : false,
				pneus : form.find('.pneus i.fa').hasClass('fa-toggle-on') ? true : false,
				eletrica : form.find('.eletrica i.fa').hasClass('fa-toggle-on') ? true : false,
				condgerais : form.find('.condgerais i.fa').hasClass('fa-toggle-on') ? true : false,
				estepes : form.find('.estepes i.fa').hasClass('fa-toggle-on') ? true : false,
				estintorvalido : form.find('.estintorvalido i.fa').hasClass('fa-toggle-on') ? true : false,
				cadeadobau : form.find('.cadeadobau i.fa').hasClass('fa-toggle-on') ? true : false,
				termoking : form.find('.termoking i.fa').hasClass('fa-toggle-on') ? true : false,
				abastecimento : form.find('.abastecimento i.fa').hasClass('fa-toggle-on') ? true : false,
				sensoresportas : form.find('.sensoresportas i.fa').hasClass('fa-toggle-on') ? true : false,
				sensorbau : form.find('.sensorbau i.fa').hasClass('fa-toggle-on') ? true : false,
				bloqueio : form.find('.bloqueio i.fa').hasClass('fa-toggle-on') ? true : false,
				sensordesengate : form.find('.sensordesengate i.fa').hasClass('fa-toggle-on') ? true : false,
				sirene : form.find('.sirene i.fa').hasClass('fa-toggle-on') ? true : false,
				travabau : form.find('.travabau i.fa').hasClass('fa-toggle-on') ? true : false,
				procedimentojbs : form.find('.procedimentojbs i.fa').hasClass('fa-toggle-on') ? true : false,
				conhecimentoroda : form.find('.conhecimentoroda i.fa').hasClass('fa-toggle-on') ? true : false,
				manuseiorastreador : form.find('.manuseiorastreador i.fa').hasClass('fa-toggle-on') ? true : false
			};
		console.log(data);
	    data = JSON.stringify(data);
	    
        $.ajax({
        	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'checklist/edit_VL/',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				if(typeof(varChecklistvelleda) != 'object'){return false;}
	        	loadingWidget('#'+varChecklistvelleda.widgetName);
			},
			success : function(data) {
    			if(typeof(varChecklistvelleda) != 'object'){return false;}  
				var id = data.id;
				var msg = data.message;

	        	loadingWidget('#'+varChecklistvelleda.widgetName, true);

				if(id == false){
		        	loadingWidget('#'+varChecklistvelleda.widgetName, true);
		        	createMsgBox('error', msg.responseText, '#'+varChecklistvelleda.widgetName+' .panel-body');
		        	return false;
				}
 	        
    	        //Criando mensagem de retorno
    	        createMsgBox('success', msg, '#'+varChecklistvelleda.widgetName+' .panel-body');
    	        // atualiza tabela que lista as contas
    	        if(typeof(varReportchecklistvelleda) == 'object'){
    	        	//'#table-list-result-checklistvelleda-2'
    	        	//varReportchecklistvelleda.widgetObj.find('.dataTable').DataTable().ajax.reload(null, false); 
    	        }
			},
			error : function(error) {
				if(typeof(varChecklistvelleda) != 'object'){return false;}
	        	loadingWidget('#'+varChecklistvelleda.widgetName, true);
	        	createMsgBox('error', error.responseText, '#'+varChecklistvelleda.widgetName+' .panel-body');		
			}
		});
		return false;
	},
	getById : function (data) {
	    $.ajax({
	    	async: true,
	    	type: 'POST',
	        url: 'widgets/routing.php',
	        data: { 
	        	url: 'checklist/getById_VL/', 
	        	data: JSON.parse(data) 
	        },
	        dataType: 'json',
	        success: function(data) {
		        widgetschecklistvelleda.loadEditForm(data);
	        },
	        error: function(error) {
	        	createMsgBox('error', error.responseText, '#'+varChecklistvelleda.widgetName+' .panel-body');
	        	loadingWidget('#'+varChecklistvelleda.widgetName, true);
	        }
	    });
	}
};

function load_widgetschecklistvelleda_Page(){
	if(typeof(varChecklistvelleda) != 'object'){return false;}

	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'checklist/index/'},
        dataType: 'json',
        beforeSend: function () {
    		if(typeof(varChecklistvelleda) != 'object'){return false;}
    		//Se não houver um ID de checklist, o cadastro se fecha
    		if(typeof(varReportchecklistvelleda) != 'object'){ varChecklistvelleda.widgetObj.find('[data-click=panel-remove]').trigger('click'); return false; }    		
    		if(varReportchecklistvelleda.id == 0){ varChecklistvelleda.widgetObj.find('[data-click=panel-remove]').trigger('click'); return false; }
			widgetschecklistvelleda.reset();

			varChecklistvelleda.widgetObj.find('.btn').attr('disabled', 'disabled');
            varChecklistvelleda.widgetObj.find('.btn-danger').removeAttr("disabled");

        	loadingWidget('#'+varChecklistvelleda.widgetName);
        },
        success: function(data) {
        	if(typeof(varChecklistvelleda) != 'object'){return false;}
        	widgetschecklistvelleda.buildTipoVeiculo(JSON.parse(data.unitTypeList));
        	widgetschecklistvelleda.buildTransportador(JSON.parse(data.shippingCompanyList));
        	//SET ID account user
        	varChecklistvelleda.widgetObj.find('form').find('#id').val(varReportchecklistvelleda.id);
        	varChecklistvelleda.widgetObj.find('form').find('#user_id').val(varReportchecklistvelleda.user_id);
        	varChecklistvelleda.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);

        	widgetschecklistvelleda.getById(varReportchecklistvelleda.id);
        },
        error: function(error) {
    		if(typeof(varChecklistvelleda) != 'object'){return false;}
        	loadingWidget('#'+varChecklistvelleda.widgetName, true);
			createMsgBox('error', error.responseText, '#'+varChecklistvelleda.widgetName+' .panel-body');
        },
        complete: function() {
    		if(typeof(varChecklistvelleda) != 'object'){return false;}
            varChecklistvelleda.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
	load_widgetschecklistvelleda_Page();
    if(typeof(varChecklistvelleda) != 'object'){return false;}

    //Datepicker
    varChecklistvelleda.widgetObj.find('.pluginDate').datepicker({ 
        autoclose: true, 
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        todayHighlight: true,
    });

    varChecklistvelleda.widgetObj.find('.clockpicker').clockpicker({
        autoclose: true,
        'default': 'now'
    }); 

	varChecklistvelleda.widgetObj.find(".label-mask").mask("aaa-9999");
	//CHAMADA PARA EXCLUSÃO DE checklistvelleda 
	varChecklistvelleda.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		openConfirm(varChecklistvelleda.widgetName, "<i class='fa fa-2x fa-times' style='vertical-align: middle;'></i> Reprovar Checklist", "Deseja reprovar esse checklist?", "Sim", "Não");
	});
	varChecklistvelleda.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		var form = varChecklistvelleda.widgetObj.find('form'); 

		widgetschecklistvelleda.executeOrder(form, 2);
		closeConfirm(varChecklistvelleda.widgetName);
	});
	varChecklistvelleda.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varChecklistvelleda.widgetName);
	});

	varChecklistvelleda.widgetObj.find('form').find('.cancel').die('click').live('click', function() {
		varChecklistvelleda.widgetObj.find('[data-click=panel-remove]').trigger('click');
	});	

	varChecklistvelleda.widgetObj.find('form').submit(function(e) { 
		e.preventDefault();
	    if ( $(this).parsley().isValid() ) {
			var form = $(this); 

			widgetschecklistvelleda.executeOrder(form, 3);
	    }
	});
});



