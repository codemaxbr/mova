<?php
require_once('../../autoloader.php');
require_once('../../dashboard-api/class/core/config.php');
require_once('../../dashboard-api/class/core/library.php');
require_once('../../dashboard-api/class/error/error-map.php');

require_once('../../dashboard-api/class/driver/dynamo-api/autoload.php');

date_default_timezone_set("America/Sao_Paulo");
session_start();
session_write_close();
//Removendo cache a cada request
header("Pragma: no-cache");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, cachehack=".time());
header("Cache-Control: no-store, must-revalidate");
header("Cache-Control: post-check=-1, pre-check=-1", false);
clearstatcache();

$url = explode('/', $_POST['url']);

$param = null;
if(!empty($_POST['data'])){
	$param = json_decode(json_encode($_POST['data']), false);
}

$pagesnoSession = array(0 => 'user/forgotPassword', 1 => 'user/updatePassword');

if(!isset($_SESSION['user']) && (array_search($_POST['url'], $pagesnoSession) >= 0 )){
	echo E_SESSIONEXPIRED;
	exit;
}	

$controller = "$url[0]Controller";
$controller = new $controller();

$action = "$url[1]Action";
$return = $controller->$action($param);

if(!is_array($return)) {
	echo $return;
} else {
	echo json_encode($return);
}
