var varAccount = {};
	varAccount.widgetName 	= 'widgetsaccount';
	varAccount.widgetObj 	= $('#'+varAccount.widgetName);
	varAccount.action 		= 'add';
	varAccount.tree 		= varAccount.widgetObj.find('.groupTree');
	varAccount.client 		= varAccount.widgetObj.find('.groupTree').jstree('get_json');

var WidgetsAccount = {
	delete: function () {
	    var data = { id: $("#id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'account/deleteaccount',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varAccount.widgetName);
			},
			success : function(data) {
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varAccount.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varAccount.widgetObj.find('#table-list-account').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varAccount.widgetName);
				WidgetsAccount.reset();
				loadingWidget('#'+varAccount.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varAccount.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varAccount.widgetName+' .panel-body');			
			}
		});
		return false;
	},
	reset: function (){
		var form = varAccount.widgetObj.find('form');

		varAccount.action = 'add';
	    WidgetsAccount.loadDefaultGroupTree(null);  

		form[0].reset();  
		form.find('#type').prop('selectedIndex', 0);
		form.find('#driver_manager').prop('selectedIndex', 0);
	    form.find('#permissions').jstree('uncheck_all');    
		form.find('#permissions').jstree('close_all');	
	    form.find('#event').jstree('uncheck_all');    
		form.find('#event').jstree('close_all');	
		
		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');
		//form.find('.accordion').accordion({ collapsible: true, active: false });
	},
	// Árvore de permissões
	createWidgetTrees: function (json){
		varAccount.widgetObj.find('.permTree').jstree({
			"checkbox" : {
				"keep_selected_style" : false
		    },
		    "core": {
		        'check_callback' : true,
		    	"data" : json,
		        "themes":{
		        	// Retirando os ícones
		            "icons":true
		        }
		    },
		    // Adicionando checkboxes para cada permissão
		    "plugins" : [ "checkbox"]
	    });
	},
	// Árvore de grupos e subgrupos
	loadDefaultGroupTree: function (data){	
		
		varAccount.widgetObj.find('form').find('.groupTree').jstree('destroy');

		if(data != null){
			varAccount.widgetObj.find('form').find('.groupTree').jstree({
				'core': {
					'check_callback': true,
					'data' : data,
				    "themes": {
				        "icons": false
				    }
				},
				"plugins": [ "contextmenu"],
				"contextmenu": {items: customMenu}
			});
		}else{
			varAccount.widgetObj.find('form').find('.groupTree').jstree({
				'core': {
					'check_callback': true,
					'data': [
						{ "id": "ajson1", "parent": "#", "text": "Grupo Geral", "state": {"opened": true}, "data" : { "file" : false } },
						{ "id": "ajson2", "parent": "ajson1", "text": "Veículos", "data" : { "file" : true } },
					],
			        "themes": {
			            "icons": false
			        }
				},
				"plugins": [ "contextmenu"],
				"contextmenu": {items: customMenu}
			});		
		}
	},
	// combo de tipos de contas
	buildAccountType: function (data){
		varAccount.widgetObj.find('#type').html('<option selected disabled>Tipo de Conta</option>');
		var template = 	'{{#data}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/data}}';
		var select = Mustache.to_html(template, data);
		varAccount.widgetObj.find('#type').append(select);
	},
    createEventTrees: function(json){
        varAccount.widgetObj.find('.eventTree').jstree({
            "checkbox" : {
                "keep_selected_style" : false
            },
            "core": {
                "data" : json,
                "themes" : { "icons": false }
            },
            "plugins" : [ "checkbox" ]
        });
    },
	// Tabela com a lista de contas cadastradas
	buildTable: function (){
		varAccount.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-account" width="100%"></table>' );
		//Pega os dados da tabela
		var table_account = varAccount.widgetObj.find('#table-list-account').dataTable({
		    ajax: {
		        "async": true,
		        "url": 'widgets/routing.php',
		        "type": "POST",
		        "data": { url: 'account/list' },
		  		error: function (error) {  
		        	loadingWidget('#'+varAccount.widgetName, true);
					createMsgBox('error', error.responseText, '#'+varAccount.widgetName+' .panel-body');
		  		}
		      },
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "columns": 
		    	[	    	 	
		            { 
		            	"title": "ID", 
		            	"data": "id" ,
		            	"className": "10_p"
		            },
		            { 
		            	"title": "Nome", 
		            	"data": "name" ,
		            	"className": "90_p",
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
		            }
		          ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por contas...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
                varAccount.widgetObj.find('.10_p').css('width', '10%');
                varAccount.widgetObj.find('.90_p').css('width', '90%');
		    	if(json.data.length > 0) {

			    	//CLICANDO NA CONTA PARA EDITÁ-LA
			    	varAccount.widgetObj.find('#table-list-account tbody').delegate('tr', 'click', function() {
			    		//Pegando id da conta
			    		var data = $(this).children('td').html();
			    	    loadingWidget('#'+varAccount.widgetName);
			    	    $.ajax({
			    	    	async: true,
				        	type: 'POST',
			    	        url: 'widgets/routing.php',
			    	        data: { url: 'account/createEditForm/', data: JSON.parse(data) },
			    	        dataType: 'json',
			    	        success: function(data) {
			    		        WidgetsAccount.loadEditForm(data, varAccount.widgetName);
			    	        	loadingWidget('#'+varAccount.widgetName, true);
			    	        },
			    	        error: function(error) {
			    	        	createMsgBox('error', error.responseText, '#'+varAccount.widgetName+' .panel-body');
			    	        	loadingWidget('#'+varAccount.widgetName, true);
			    	        }
			    	    });
			    	    return false;
			    	});
		    	}
		    }
		});
	},
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	loadEditForm: function (data){	
		var form = varAccount.widgetObj.find('form');
		//limpa formulário
		WidgetsAccount.reset();
		// muda o form para edição
		varAccount.action = 'edit';

		form.find('#permissions').jstree('open_all');
		form.find('#event').jstree('open_all');	
		form.find('input#name').focus();	
		form.find('input#id').val(data.account.id);
		form.find('input#name').val(data.account.name);
		form.find('input#name_user_master').val(data.account.users_name);
		form.find('input#email_user_master').val(data.account.users_login);

		//Carregando todos grupos/subgrupos da conta		
		WidgetsAccount.loadDefaultGroupTree(data.groups);
		
		if(data.perms) {
	    	form.find('.accordion.perm').accordion( "option", "active", 0 );
		}

		if(data.event) {
	    	form.find('.accordion.event').accordion( "option", "active", 0 );
		}
		
		if(data.groups) {
			form.find('.accordion.group').accordion( "option", "active", 0 );
		}
		
		form.find('select#type option').each(function(index, value){
		      var value = $(this).attr('value');
		      if(value == data.account.type)
			      $(this).attr('selected', true);
		});
		
		form.find('select#driver_manager option').each(function(index, value){
		      var value = $(this).attr('value');
		      if(value == data.account.driver_manager)
			      $(this).attr('selected', true);
		});

		form.find('select#status option').each(function(index, value){
		      var value = $(this).attr('value');
		      if(value == data.account.status)
			      $(this).attr('selected', true);
		});

		form.find('#permissions li').each(function(index, value){
			var check = $(this);
			var id = check.attr('id');
			$.each(data.perms, function(i, val){
				if(id == val)
					form.find('#permissions').jstree('check_node', check);
			});
		});
		form.find('#permissions').jstree('close_all');

		form.find('#event li').each(function(index, value){
			var check = $(this);
			var id = check.attr('id');
			$.each(data.event, function(i, val){
				if(id == val)
					form.find('#event').jstree('check_node', check);
			});
		});
		form.find('#event').jstree('close_all');

		openForm(varAccount.widgetName, varAccount.action);
	}
}

function load_widgetsaccount_Page(){
	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'account/index/'},
        dataType: 'json',
        beforeSend: function () {
        	closeForm(varAccount.widgetName);
			WidgetsAccount.reset();	
			varAccount.widgetObj.find('.btn').attr('disabled', 'disabled');
            varAccount.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	WidgetsAccount.buildTable();
        	WidgetsAccount.buildAccountType(JSON.parse(data.accountTypes));
        	WidgetsAccount.createWidgetTrees(JSON.parse(data.widgetTree));
            WidgetsAccount.createEventTrees(JSON.parse(data.events));
        	//liberando menu
        	varAccount.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varAccount.widgetName, true); 

        	//Carregando comportamento do widget
			//Accordion
			$(function() {
				varAccount.widgetObj.find('.accordion').accordion({
					active: false,
					header: 'h6',
				    collapsible: true,
				    heightStyle: "content"
			      });
			});
			//Árvore de grupos do form adicionar
			WidgetsAccount.loadDefaultGroupTree(null);
        },
        error: function(error) {
        	loadingWidget('#'+varAccount.widgetName, true);
            varAccount.widgetObj.find('.btn-danger').removeAttr("disabled");
			createMsgBox('error', error.responseText, '#'+varAccount.widgetName+' .panel-body');
        }
    });
}

$(document).ready( function () {
	load_widgetsaccount_Page();

	varAccount.widgetObj.find('.openForm').die("click").live("click", function(){
		WidgetsAccount.reset();
		varAccount.action = 'add';
		openForm(varAccount.widgetName, 'add');
	});
	//CHAMADA PARA EXCLUSÃO DE ACCOUNT 
	varAccount.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		openConfirm(varAccount.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir Conta", "Deseja remover a conta selecionado?", "Sim", "Não");
	});
	varAccount.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		WidgetsAccount.delete();
		closeConfirm(varAccount.widgetName);
	});
	varAccount.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varAccount.widgetName);
	});
	//Resetando árvores ao clicar em cancelar/fechar
	varAccount.widgetObj.find('.cancel').die('click').live('click', function() {
		closeForm(varAccount.widgetName);
		WidgetsAccount.reset();		
	});
	// CRIANDO CONTA
    varAccount.widgetObj.find('form').submit(function(e) { 
        e.preventDefault();
        if ( $(this).parsley().isValid() ) {
        	var selectedElmsIds = [];
        	var selectedElms = $(this).find('.permTree').jstree("get_selected", true);
        	
        	if($(selectedElms).length > 0){
        		$.each(selectedElms, function() {
            		if(this.children.length == 0)
            		{
            	    	selectedElmsIds.push(this.id);
            		}
    			});
        	}else{
    	        createMsgBox('error', 'Deve selecionar no mínimo 1 permissão.', '#'+varAccount.widgetName+' .panel-body');
            	$(this).find('.permTree').addClass('permError');
            	varAccount.widgetObj.find('#parsley-perm-tree').html("Campo Obrigatório");
            	return false;
        	}

        	var eventIds = [];
        	var eventElms = $(this).find('.eventTree').jstree("get_selected", true);
        	
        	if($(eventElms).length > 0){
        		var lastParent = '';
        		$.each(eventElms, function() {
            		if((this.children.length == 0) && (lastParent != this.parent)) 
            		{
            	    	eventIds.push(this.id);
            	    	lastParent = this.parent;
            		}
    			});
        	}else{
    	        createMsgBox('error', 'Deve selecionar no mínimo 1 typo de evento.', '#'+varAccount.widgetName+' .panel-body');
            	$(this).find('.eventTree').addClass('permError');
            	varAccount.widgetObj.find('#parsley-event-tree').html("Campo Obrigatório");
            	return false;
        	}
        	
			varAccount.tree	= varAccount.widgetObj.find('.groupTree');
        	varAccount.client = $(this).find('.groupTree').jstree('get_json');
        	
			var data = 
				{
					account_id: $(this).find('#id').val(),
					ac_name: $(this).find('#name').val(),
					type: $(this).find('#type').val(),
					status: $(this).find('#status').val(),
					driver_manager: $(this).find('#driver_manager').val(),
					client: varAccount.client,
					name: $(this).find('#name_user_master').val(),
					login: $(this).find('#email_user_master').val(),
					perms: selectedElmsIds,
					event: eventIds
				};
				
			data = JSON.stringify(data);
			
            $.ajax({
            	async: true,
    	        type: 'POST',
    	        url: 'widgets/routing.php',
                data: { url: 'account/'+varAccount.action+'/', data:JSON.parse(data) },
    	        dataType: 'json',
    	        beforeSend: function () {
    	        	loadingWidget('#'+varAccount.widgetName);
    	        },
    	        success: function(data) {
    	        	loadingWidget('#'+varAccount.widgetName, true);       	        
	    	        //Criando mensagem de retorno
	    	        createMsgBox('success', data, '#'+varAccount.widgetName+' .panel-body');
	    	        // atualiza tabela que lista as contas
	    	        varAccount.widgetObj.find('#table-list-account').DataTable().ajax.reload(null, false);	    	            	
	    	        //reseta o form	    
					closeForm(varAccount.widgetName);
	    	        WidgetsAccount.reset();   		        
    	        },
    	        error: function(error) {
    	        	loadingWidget('#'+varAccount.widgetName, true);
    	        	//Criando mensagem de retorno
    	        	createMsgBox('error', error.responseText, '#'+varAccount.widgetName+' .panel-body');
	            }
    	    });
    		return false;
        }
    });
});

