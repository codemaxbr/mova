var varPOI = {};
	varPOI.widgetName 	= 'widgetspoi';
	varPOI.widgetObj 	= $('#'+varPOI.widgetName);
	varPOI.action 		= 'add';
	varPOI.callMap;
	varPOI.color;
	varPOI.accessLevel 	= 1;

var widgetspoi = {
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	loadEditForm: function (data) {
		//verifica se o widget ainda está aberto
    	if(typeof(varPOI) != 'object'){return false;}

    	if(varPOI.accessLevel == 1){
			var divContent = varPOI.widgetObj.find('.visualize-box');

			divContent.find('.lbl_local').html(data.name);
			divContent.find('.lbl_lat').html(data.latitude);
			divContent.find('.lbl_lng').html(data.longitude);
			divContent.find('.mapAddress').attr('long', data.longitude);
			divContent.find('.mapAddress').attr('lat', data.latitude);
			divContent.find('.lbl_icon').html("<i class='fa "+data.icon+"' style='font-size: 15px;'></i>");
			divContent.find('.lbl_color').html("<i class='fa fa-circle' style='color: "+data.color+"; font-size: 15px;'></i>");
			divContent.find('.lbl_group').html(data.group_name);
			divContent.find('.lbl_description').html(data.description);
			//dispara reveGeo
			getAddressByCoord(divContent.find('.mapAddress'));

			openVisualize(varPOI.widgetName);
			return false;
    	}

		var form = varPOI.widgetObj.find('form');
		//limpa formulário
		widgetspoi.reset();
		// muda o form para edição
		varPOI.action = 'edit';
		form.find('.call_map').removeClass('color-stat');

		form.find('#id').val(data.id);
		form.find('#local').val(data.name);
		form.find('#lat').val(data.latitude);
		form.find('#long').val(data.longitude);
		form.find('.selected-icon').children().attr('class', data.icon);
		form.find('.color').val(data.color);
		form.find('.color').css('background-color', data.color);
		if(typeof(varPOI.color) == 'object'){
			$(varPOI.color).colorPicker({
				customBG: data.color
			});
		}
		form.find('#description').val(data.description);
		form.find('#group option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == data.group_id) {
				$(this).prop('selected', true);
			}
		});
		//guarda a posição original do POI caso as alterações não sejam salvas
		if(typeof(varMapBox) == 'object'){
			//caso mapa esteja aberto, junto a edição do POI executar um "ir para" o POI no mapa. 
			MapBox.centerUnit(data.latitude, data.longitude);
			MapBox.getMarkerPOI(data.id, 0, '');
		}

		openForm(varPOI.widgetName, varPOI.action);
	},
	buildGroup: function (data){
    	if(typeof(varPOI) != 'object'){return false;}
		varPOI.widgetObj.find('#group').html('<option value="">Selecione um grupo</option>');
		var template = 	'{{#groupinfo}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/groupinfo}}';
		var select = Mustache.to_html(template, data);
		varPOI.widgetObj.find('#group').append(select);
	},
	reset: function (){
    	if(typeof(varPOI) != 'object'){return false;}
		var form = varPOI.widgetObj.find('form');

		varPOI.action = 'add'; 

		form[0].reset();  
		form.find('#group').prop('selectedIndex', 0);
		form.find('#description').val('');
		form.find('.selected-icon').children().attr('class', 'fa fa-1x fa-close');
		form.find('.color').val('#808080');
		form.find('.color').css('background-color', '#808080');
		if(typeof(varPOI.color) == 'object'){
			$(varPOI.color).colorPicker({
				customBG: '#808080'
			});
		}	
		
		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');
	},
	delete: function () {
    	if(typeof(varPOI) != 'object'){return false;}
	    var data = { id: varPOI.widgetObj.find('form').find("#id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'poi/delete',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varPOI.widgetName);
			},
			success : function(data) {
    			if(typeof(varPOI) != 'object'){return false;}
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varPOI.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varPOI.widgetObj.find('#table-list-poi').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varPOI.widgetName);
				widgetspoi.reset();
				if(typeof(varMapBox) == 'object'){
					//liberar o marker sobre o mapa
					MapBox.disabledMarker();
					MapBox.loadPOILayer();
				}					
				loadingWidget('#'+varPOI.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varPOI.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varPOI.widgetName+' .panel-body');		
			}
		});
		return false;
	},
	alterLatLongMarker: function (obj){
    	if(typeof(varPOI) != 'object'){return false;}
		var form = varPOI.widgetObj.find('form');

		//verifica de existe uma instancia de mapa e se o widgets do mapa está aberto e se o widget de POI tambem existe
		if(typeof(varMapBox) == 'object'){
			//altera cor do botão para indicar que estáativo
			if(typeof(varMapBox.POILayer) != 'object'){
				clearTimeout(varPOI.callMap);
				varPOI.callMap = window.setTimeout(function() {
					obj.removeClass('color-stat');
			        widgetspoi.alterLatLongMarker(obj, form);
			    }, 1000);
			    return false;
			}
			obj.addClass('color-stat');
			
			var id = form.find('#id').val();
			var lat = form.find('#lat').val();
			var lng = form.find('#long').val();
			
			MapBox.centerUnit(lat, lng);
			//adiciona marcador no mapa
			MapBox.addMarkerPOI(lat, lng);
			MapBox.getMarkerPOI(id, 0, 'none');			
		}else{
			//caso o mapa não esteja aberto, chama o mapa
			handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 8);
			//para dar tempo do mapa abrir, espera alguns segundos para chamar a função novamente
			clearTimeout(varPOI.callMap);
			varPOI.callMap = window.setTimeout(function() {
				obj.removeClass('color-stat');
		        widgetspoi.alterLatLongMarker(obj, form);
		    }, 1000); 		
		}	
	},
	getLatLongMap: function (obj){
    	if(typeof(varPOI) != 'object'){return false;}
		var form = varPOI.widgetObj.find('form');
		//verifica de existe uma instancia de mapa e se o widgets do mapa está aberto e se o widget de POI tambem existe
		if(typeof(varMapBox) == 'object'){
			//altera cor do botão para indicar que estáativo
			if(typeof(varMapBox.mapInstance) != 'object'){
				clearTimeout(varPOI.callMap);
				varPOI.callMap = window.setTimeout(function() {
			        widgetspoi.getLatLongMap(obj, form);
			    }, 1000); 
				return false;
			}
			obj.addClass('color-stat');
			//altera icone do curosr no mapa 
			varMapBox.widgetObj.find('.leaflet-container').css('cursor', 'crosshair');

			//remove posteriores eventos de click no mapa
			varMapBox.mapInstance.off('click');		
			varMapBox.mapInstance.on('click', function(e){
				//pega coordenadas lat e long do click no mapa
			    var latlng = e.latlng.toString();
			    latlng = latlng.replace(' ', '');
			    latlng = latlng.replace('LatLng(', '');
			    latlng = latlng.replace(')', '');

			    var lat = latlng.split(',')[0];
			    var lng = latlng.split(',')[1];
			    //carrega lat e long nos campos de cadastro do POI
			    form.find('#lat').val(lat);
			    form.find('#long').val(lng);
			    //remove evento de click no mapa, voltar cursor ao normal, remove cor do botão
			    varMapBox.mapInstance.off('click');
				varMapBox.widgetObj.find('.leaflet-container').css('cursor', '');
				obj.removeClass('color-stat');
				//adiciona marcador no mapa
				MapBox.addMarkerPOI(lat, lng);
			});
			//cria evento no fechar do mapa.
			//se o mapa for fechado sem que uma coordenada seja selecionada, ele remove o cursor e a cor do botão.
			varMapBox.widgetObj.find('[data-click=panel-remove]').on("click", function(e){
				clearTimeout(varPOI.callMap);	
				varMapBox.widgetObj.find('.leaflet-container').css('cursor', '');
				obj.removeClass('color-stat');	
				MapBox.disabledMarker();       
		    });
		}else{
			//caso o mapa não esteja aberto, chama o mapa
			handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 8);
			//para dar tempo do mapa abrir, espera alguns segundos para chamar a função novamente
			clearTimeout(varPOI.callMap);
			varPOI.callMap = window.setTimeout(function() {
		        widgetspoi.getLatLongMap(obj, form);
		    }, 1000); 
		}
	},
	fitBoundsPOI: function (id, lat, lng){
		if(typeof(varPOI) != 'object'){return false;}
		//verifica de existe uma instancia de mapa e se o widgets do mapa está aberto e se o widget de POI tambem existe
		if(typeof(varMapBox) == 'object'){
			clearTimeout(varPOI.callMap);
			if((typeof(varMapBox.POILayer) != 'object') || (typeof(varMapBox.mapInstance) != 'object')){
				clearTimeout(varPOI.callMap);
				varPOI.callMap = window.setTimeout(function() {
			        widgetspoi.fitBoundsPOI(id, lat, lng);
			    }, 1000);
			    return false;
			}
			MapBox.centerUnit(lat, lng);
			MapBox.getMarkerPOI(id, 0, '');	
		}else{
			//caso o mapa não esteja aberto, chama o mapa
			handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 8);
			//para dar tempo do mapa abrir, espera alguns segundos para chamar a função novamente
			clearTimeout(varPOI.callMap);
			varPOI.callMap = window.setTimeout(function() {
		        widgetspoi.fitBoundsPOI(id, lat, lng);
		    }, 2000); 
		}
	},
	buildTable: function (poi){
    	if(typeof(varPOI) != 'object'){return false;}
		varPOI.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-poi" width="100%"></table>' );
		//Pega os dados da tabela
		var table_pois = varPOI.widgetObj.find('#table-list-poi').dataTable({
		    ajax: {
		        "url": 'widgets/routing.php',
		        "type": "POST",
		        "data": { url: 'poi/list/'},
		  		error: function (error) {  
    				if(typeof(varPOI) != 'object'){return false;}
		        	loadingWidget('#'+varPOI.widgetName, true);
					createMsgBox('error', error.responseText, '#'+varPOI.widgetName+' .panel-body');
		  		}
	      	},
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "columns": 
		    	[
		            { "title": "#", "data": "poi_id", "class": 'hidden' },
		            { "title": "Local", "data": "poi_name", "className": "goToEdit 35_p" },
		            { "title": "Grupo", "data": "group_name", "className": "goToEdit 35_p" },
		            { "title": "Icone", "data": "iconhtml", "class" : "text-center", "sortable": false, "className": "goToEdit 10_p" },
		            { "title": "Cor", "data": "color", "className": "text-center goToEdit no-select 10_p" },
		            { "title": "&nbsp;", "data": null, "className": "text-center no-select goToMap 10_p" }
	          	],
          	"columnDefs": 
                [
                    {
                        "targets": [1],
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
                    },
                    {
                        "targets": [2], 
		            	"render": function ( data, type, full, meta ) {
							return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
						}
                    },
                    {
                        "targets": [3],
                        "orderable": false,
                        "render": function (data, type, full, meta) {
                            return data;
                        }
                    },
                    {
                        "targets": [4],
                        "orderable": false,
		            	"render": function ( data, type, full, meta ) {
							return "<i class='fa fa-circle' style='color: "+data+"; font-size: 15px;'></i>";
						}
                    },
                    {
                        "targets": [5],
                        "orderable": false,
		            	"render": function ( data, type, full, meta ) {
							return "<i class='fa fa-globe primary-color-font' style='font-size: 15px;' latitude='"+full.latitude+"' longitude='"+full.longitude+"' title='Ver ponto no mapa'></i>";
						}
                    }
                ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por dispositivos...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
    			if(typeof(varPOI) != 'object'){return false;}
                varPOI.widgetObj.find('.10_p').css('width', '10%');
                varPOI.widgetObj.find('.35_p').css('width', '35%');
		    	//CLICANDO NA CONTA PARA EDITÃ�-LA
		    	if(json.data.length > 0){

		    		varPOI.widgetObj.find('#table-list-poi tbody tr').find('td.goToEdit').die("click").live("click", function(){
			    		//Pegando id
			    		var parent = $(this).parents('tr');
	    				var data = parent.children('td').html();
			    	    loadingWidget('#'+varPOI.widgetName);		
		    		    $.ajax({
		    		    	async: true,
		    		    	type: 'POST',
		    		        url: 'widgets/routing.php',
		    		        data: { 
		    		        	url: 'poi/getById/', 
		    		        	data: JSON.parse(data) 
		    		        },
		    		        dataType: 'json',
		    		        success: function(data) {
			    		        widgetspoi.loadEditForm(data);
			    	        	loadingWidget('#'+varPOI.widgetName, true);
		    		        },
		    		        error: function(error) {
			    	        	createMsgBox('error', error.responseText, '#'+varPOI.widgetName+' .panel-body');
			    	        	loadingWidget('#'+varPOI.widgetName, true);
		    		        }
		    		    });
	    		    	return false;
	    		    });

		    		varPOI.widgetObj.find('#table-list-poi tbody tr').find('td.goToMap').die("click").live("click", function(){
						var parent = $(this).parents('tr');
		    			var data = parent.children('td').html();
		    			var html = $(this).context.innerHTML;
		    			var latitude = $(html).attr('latitude');
		    			var longitude = $(html).attr('longitude');
		    			
		    			widgetspoi.fitBoundsPOI(data, latitude, longitude);
		    		    return false;
		    		});
				}  	
		  	}
		});
	}
};

function load_widgetspoi_Page(){
	if(typeof(varPOI) != 'object'){return false;}
	varPOI.accessLevel = handleAccessLevel(varPOI.widgetName);
	if(varPOI.accessLevel == 1){
		varPOI.widgetObj.find('.openForm').addClass('hidden');
	}

	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'poi/index/'},
        dataType: 'json',
        beforeSend: function () {
    		if(typeof(varPOI) != 'object'){return false;}
        	closeForm(varPOI.widgetName);
			widgetspoi.reset();	
			varPOI.widgetObj.find('.btn').attr('disabled', 'disabled');
            varPOI.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	if(typeof(varPOI) != 'object'){return false;}
        	widgetspoi.buildTable();
        	widgetspoi.buildGroup(JSON.parse(data.groupinfo));        	
        	selectIcons('#'+varPOI.widgetName+' form', data.listIcons, 9);

			if(typeof(MapBox) == 'object'){
				MapBox.activePOIControls();
			}
        	//SET ID account user
        	varPOI.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	//liberando menu
        	varPOI.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varPOI.widgetName, true); 
        },
        error: function(error) {
    		if(typeof(varPOI) != 'object'){return false;}
        	loadingWidget('#'+varPOI.widgetName, true);
			createMsgBox('error', error.responseText, '#'+varPOI.widgetName+' .panel-body');
        },
        complete: function() {
    		if(typeof(varPOI) != 'object'){return false;}
            varPOI.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
	load_widgetspoi_Page();

    if(typeof(varPOI) != 'object'){return false;}
	varPOI.color = varPOI.widgetObj.find("form").find('.color').colorPicker({
		opacity: false,
		doRender: true,
		customBG: '#808080',
		renderCallback: function($elm, toggled) {
			if((typeof(MapBox) == 'object') && (typeof(varPOI) == 'object')){
				varPOI.widgetObj.find("form").find('.color').val('#'+this.color.colors.HEX);
				MapBox.reloadMarkerActive(varPOI.widgetObj.find("form"));				
			}
		}
	});
			
	varPOI.widgetObj.find('.openForm').die("click").live("click", function(){
		widgetspoi.reset();
		varPOI.action = 'add';
		openForm(varPOI.widgetName, 'add');
	});
	//CHAMADA PARA EXCLUSÃO DE pois 
	varPOI.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		openConfirm(varPOI.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir POI", "Deseja remover o ponto selecionado?", "Sim", "Não");
	});
	varPOI.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		widgetspoi.delete();
		closeConfirm(varPOI.widgetName);
	});
	varPOI.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varPOI.widgetName);
	});

	varPOI.widgetObj.find('form').find('.cancel').die('click').live('click', function() {
		clearTimeout(varPOI.callMap);	
		if(typeof(varMapBox) == 'object'){
			varMapBox.widgetObj.find('.leaflet-container').css('cursor', '');
			varMapBox.mapInstance.off('click');
			MapBox.disabledMarker();
		}

		closeForm(varPOI.widgetName);
		widgetspoi.reset();		
	});	

	varPOI.widgetObj.find('.visualize-box').find('.cancel').die('click').live('click', function() {
		closeVisualize(varPOI.widgetName);
	});	

	varPOI.widgetObj.find('.call_map').die("click").live("click", function(){	
		if(varPOI.action == 'add'){
			widgetspoi.getLatLongMap($(this));
		}else{
			widgetspoi.alterLatLongMarker($(this));
		}		
	});

	varPOI.widgetObj.find('form').find('#icon').on('change', function(){
		if(typeof(MapBox) == 'object'){
			MapBox.reloadMarkerActive(varPOI.widgetObj.find("form"));				
		}
	});

	varPOI.widgetObj.find('form').submit(function(e) { 
		e.preventDefault();
	    if ( $(this).parsley().isValid() ) {
			var form = $(this);        
		    var data = 
				{
					poi_id: $(this).find("#id").val(),
					name: $(this).find("#local").val(),
					group_id: $(this).find("#group").val(),
					account_id: $(this).find("#account_id").val(),
					icon: $(this).find('.selected-icon').children('i').attr('class'),
					color: $(this).find(".color").val(),
					latitude: $(this).find("#lat").val(),
					longitude: $(this).find("#long").val(),
					description: $(this).find("#description").val()
				};
		    
		    data = JSON.stringify(data);
		    
	        $.ajax({
	        	async: true,
				type : 'POST',
				url : 'widgets/routing.php',
				data : {
					url : 'poi/'+varPOI.action+'/',
					data : JSON.parse(data)
				},
				dataType : 'json',
				beforeSend : function() {
    				if(typeof(varPOI) != 'object'){return false;}
    	        	loadingWidget('#'+varPOI.widgetName);
				},
				success : function(data) {
        			if(typeof(varPOI) != 'object'){return false;}
    	        	loadingWidget('#'+varPOI.widgetName, true);       	        
	    	        //Criando mensagem de retorno
	    	        createMsgBox('success', data, '#'+varPOI.widgetName+' .panel-body');
	    	        // atualiza tabela que lista as contas
	    	        varPOI.widgetObj.find('#table-list-poi').DataTable().ajax.reload(null, false);	    	            	
	    	        //reseta o form	    
					closeForm(varPOI.widgetName);
	    	        widgetspoi.reset(); 
					if(typeof(varMapBox) == 'object'){
						//liberar o marker sobre o mapa
						MapBox.disabledMarker();
						MapBox.loadPOILayer();
					}
				},
				error : function(error) {
    				if(typeof(varPOI) != 'object'){return false;}
    	        	loadingWidget('#'+varPOI.widgetName, true);
    	        	createMsgBox('error', error.responseText, '#'+varPOI.widgetName+' .panel-body');		
				}
			});
			return false;
	    }
	});
});



