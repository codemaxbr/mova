var varReportstoppoi = {};
    varReportstoppoi.widgetName    = 'widgetsreportstoppoi';
    varReportstoppoi.widgetObj     = $('#'+varReportstoppoi.widgetName);
    varReportstoppoi.numEventsG    = 0;
    varReportstoppoi.numEvents     = 0;
    varReportstoppoi.numSearchs    = 0;
    varReportstoppoi.backupElem    = '';
    varReportstoppoi.tabResults    = 0;
    varReportstoppoi.table         = [];
    varReportstoppoi.api           = [];
    varReportstoppoi.dataFilterArr = [];
    varReportstoppoi.processingSearchLimit = 1000;

    varReportstoppoi.rowSizeMax = 28;
    varReportstoppoi.rowSizeMin = 10;
    varReportstoppoi.rowAdjust = 5;

var widgetsReportStopPoi = {
    buildGroup: function (data){
        varReportstoppoi.widgetObj.find('#group').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varReportstoppoi.widgetObj.find('#group').append(select);
    },
    buildSubGroup: function (data){
        varReportstoppoi.widgetObj.find('#subgroup').html('<option value="0" selected>Todos</option>');
        var template =  '{{#subgroupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/subgroupinfo}}';
        var select = Mustache.to_html(template, data);
        varReportstoppoi.widgetObj.find('#subgroup').append(select);
    },
    buildGroup_poi: function (data){
        varReportstoppoi.widgetObj.find('#group_poi').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varReportstoppoi.widgetObj.find('#group_poi').append(select);
    },
    processingSearch: function (idTab, dataFilter){
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportstoppoi/search/', data: dataFilter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                varReportstoppoi.widgetObj.find('#table-list-result-stoppoi-'+idTab+'_info').append('<span class="sp-load-'+idTab+'" title="Carregando consulta..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                //para de processar caso a tab seja fechada
                if(varReportstoppoi.table[idTab] == null ){ return false; }
                
                $(json).each(function(i, rows) {                
                    varReportstoppoi.table[idTab].row.add({
                        "label": rows.label, 
                        "data_inicio": rows.data_inicio,  
                        "data_report_inicio": rows.data_report_inicio,
                        "hora_min_inicio": rows.hora_min_inicio, 
                        "data_fim": rows.data_fim,  
                        "data_report_fim": rows.data_report_fim,
                        "hora_min_fim": rows.hora_min_fim,  
                        "initial_longitude": rows.initial_longitude, 
                        "initial_latitude": rows.initial_latitude, 
                        "initial_full_address": rows.initial_full_address, 
                        "devst_id": rows.devst_id, 
                        "final_poi": rows.final_poi, 
                        "initial_area_name": rows.initial_area_name, 
                        "total_time": rows.total_time, 
                        "full_date_begin": rows.full_date_begin
                    });
                }); 
                varReportstoppoi.table[idTab].draw(false);
                //reinicia o geocode caso haja algum registro para ser carregado
                buildRevGeoFromTable(varReportstoppoi.widgetName);
                //verifica se ainda existem registros para serem buscados
                if( json.length >= varReportstoppoi.processingSearchLimit ){
                    dataFilter.last_id = json[json.length - 1].id;

                    window.setTimeout(function() {
                        varReportstoppoi.processingSearch(idTab, dataFilter); 
                    }, 50);
                }    
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReportstoppoi.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                varReportstoppoi.widgetObj.find('.sp-load-'+idTab).remove();
            }
        });
    },
    print: function (type, id){
        var filter = JSON.parse(varReportstoppoi.dataFilterArr[id]);
        //removendo limit para retornar todos os dados quando for´exportação
        filter.limit = 0;
        filter.last_id = 0;

        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'reportstoppoi/search/', data: filter },
            dataType: 'json',
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                varReportstoppoi.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');
                varReportstoppoi.widgetObj.find('.actionMenu .fristAction').append('<span class="sp-load" title="Carregando exportação..."><div class="spinner_dots"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span>');
            },
            success: function(json) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                var horaFim;
                var speedFim;

                switch (type) { 
                    case 'print': 
                        var content = '<style>';
                        content += '    table { border: 1px solid #999; border-bottom: none; width: 100%; }';
                        content += '    thead { border: 5px solid #000; }';
                        content += '    tbody td { border: 1px solid #ccc; }';
                        content += '</style>';             
                        content += '<table><thead"><th>Placa</th><th>Hora Inicial</th><th>Hora Final</th><th>Tempo Total</th><th>Local</th><th>POI</th></thead><tbody>';
                        
                        $(json).each(function(i, row) {
                            horaFim = row.data_report_fim+' às '+row.hora_min_fim;
                            if(row.data_report_fim == null){
                                horaFim = '--';
                            }
                            speedFim = minutesToHMS(row.total_time * 60);
                            if(row.total_time == 0){
                                speedFim = '--';
                            }
                            content += '<tr><td>'+row.label+'</td><td>'+row.data_report_inicio+' às '+row.hora_min_inicio+'</td><td>'+horaFim+'</td><td>'+speedFim+'</td>';
                            content += '<td>'+row.initial_full_address+'</td><td>'+row.initial_poi_name+'</td></tr>';
                        });            
                        content += '</tbody></table>';

                        var w = window.open("");

                        w.document.write(content);
                        w.print();
                        w.close();

                        break;
                    case 'PDF': 
                        var columns = ["Placa", "Hora Inicial", "Hora Final", "Tempo", "Local", "POI"];
                        var rows = [];
                        $(json).each(function(i, row) {
                            horaFim = row.data_report_fim+' às '+row.hora_min_fim;
                            if(row.data_report_fim == null){
                                horaFim = '--';
                            }
                            speedFim = minutesToHMS(row.total_time * 60);
                            if(row.total_time == 0){
                                speedFim = '--';
                            }
                            rows.push([row.label, row.data_report_inicio+' às '+row.hora_min_inicio, horaFim, speedFim, row.initial_full_address, row.initial_poi_name]);
                        });   
                        var doc = new jsPDF('l', 'pt');
                        var dataCurrente = new Date();
                        var year = dataCurrente.getFullYear();
                        var month = dataCurrente.getMonth() + 1;
                        var day = dataCurrente.getDate();

                        doc.autoTable(columns, rows, {
                            theme: 'striped', // 'striped', 'grid' or 'plain'
                            headerStyles: {
                                fillColor: [0, 147, 147],
                                color: [255, 255, 255]
                            },
                            bodyStyles: {
                                overflow: 'linebreak', // visible, hidden, ellipsize or linebreak
                            },
                            margin: {
                                top: 100
                            },
                            beforePageContent: function(data) {
                                var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKwAAAAyCAYAAADBXTDrAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADfVJREFUeNrsXQtwFdUZPnsTQDtTUUR8gER5KyQBdXhIiKTysJCLrQqDEGxxQEQRtQMFQTqlKka0OrQBSuOUKSTUEp1BQuwYQGoSKE9jLqAE5ZGAILYCsWN5JTk937nZZXfvObtn770hybj/zM7k7r275z97vvOd7//PORuNUkribcX7P6dVp/9Lqk5/Zzmf2rE9aXt1GzKi9x0a8c23KCwxHjfJLd1JS788Tkq++CoCpBKjKR1vIOndO5L0bh3J2Hv6+gD2Tcm0aBl255HDNOefn5LVOz6P2YmkdteQrAF3kJcfHOYD17f4Arby66/oE/kbGZsej7szkAszhvZlwB3uA9e32AH74rqN9JV/7FAGX2qnG4zPZ89dIKHj/1a+NjdruC8VfIsOsDsPH6ZTGas6AS6FgTOY3IXp0k6OQRUYGvcpDB0m60OHSA0DsswmDbiTrHr8IR+0vqkDFhH/uNwNUmABVPNHDSA9b+oYFbByS3fQPKaDZRIDHaFiwTQftL65AxZgeiKv2AGoA6MGqqhjoCxRliEM2id90PomB6wMrNCXa6cGGy2X+sw7G2jOlnIftL6pAxaadcSSdyNkAAfNbxofNLklO+js9z6OKB/6+OPZk33Q/oAtIDo5Lnc9A8t59hc1jpRO7a8IWGFT0wdoxc8+wti8tcWHki+OkRfXFVO/2XzAGjYmJ59WffudgFmnX1Fm69+lCwPt2Ijzr3ywnY8AftP5koAHPiOXFFh+kHT9NeToq8812TCcW7I9QkvHowNdKv8b1U7sJYHDZcY52q4zoTcnk7remaTNjb2F979waj9N2L+BaCf3Eu10tXG+vksaobckk1b9Ho35Wem+8TLO1UT4h7Ja3zbYsZxL2/5EE/as4X/X3T2BtLo3fqNjbdE8iudGr25L6obNdfXl4tGt/Pf2Z4breX26svr0GhVxj9r3nqYB9hx4OWnT+W8sgE1duIzac60fPjeWBVh3NqluBOsXVnxpOffnSSOZdBioRQOGxMK5RDtT7fi7unsmktphL1iAW5efRRN25zteR6/rzK/zChDeEcqWkYQ9+RaQRlMOAJW46VXrOfbbxNGLYm7H+j+mM/CVWgH5bJkQtLUfvcbr5PasdfDWDX7K8FFWjgFYMNm01R9afjAj4y7yhwnBZhHkXDczm5qDMDD/keznPflW93aQs6Oq4SHWZmZzZmu1aoISkC4z7hASeKZEU2WshK3LPN3fDNxL45ZbGIr+LonaQYK6aIvOxtSWFw98QFuvGB3pfxoD2sNLNa+kIAXu3RNJIgN6RDms0xkaNn/7ZxHpq+YCVtjiR4ZaPkNnF+wqF2pZDCXkeY0faDwMSfWvp3oCK9dLDECtCthQxBrJK5jADihThbHAhtGAlfvIQAH/wGbGyfM1wrqgY8TSBokbs8U+mMrDs+edOwqw6n6KwGoJuipPfkVLDrICwLYNx4yMfs1KbGP4T2r3Y4uPedv3C7WbucK8QZekkcCJ0JWPaFmZYHUpWBmg7cNe1GBijAbJwu/LNLjIwOKx6GqZr/W3pxmSyQlsccsS5G3fF/HFSz8fydl15ppCGpjyW3r7nDcpgN2UoM0a2Nvy2a5reYU+K2pWHQ2sjqFUqAXj3Imgr9FhMXTK2CtalkWHkEkfrvcha1z0fdwAG274y/nOYGrXBl37L5rz0W5+rurbGrJ0yx7S9ICllgM+2jWdF5156TEWSb9FNRyXxi7nGkpJ2zJQXJi7j1+LYKBOwmqioTQcZZc635/pwovTigzfuH/MV4DDEVgb5hq6Uvg9kx8I8DyxK+sEsiG+dvhcLn/sQZ4oiDU/a/7cWP3gp8oztwA2dOwbCw4yU7rxL5du/sRynv+uCa3nzZ20lI4dLD6VHbQumqlTfAB4gAiKzGkoRNwITJyux3eXniji0ayeQUCEnDClUJMxGxpUBwnPBjgMzfALPiCIsad64GvCxDzeQepvSZHr7rXTSR3zRVYPN3Cp/h4dHodM2/LfMD/RseG3PeWH+qGeqK9Th7c8y+J9n1E7a6X3uJV/GTp+ynIegVhTW5j9L/tUceyU5XuACIByBB1jYTxAadSeKW8AfCfLOwLEMobXh3+e5pEEWLXBbEe/jIZm5QdmV2gy0KKDaGeqeJpIJh1UWVaFXWWjBUAIP2U5bYtPrMO7jR68bqHj30RkB8Bkoh9npnZrcsCm3NrB8tnuv96gTqB1681OOVS3/CpSMkLmO7E33DB78qU+Jf5kjqesDMAgq2PC1uWOo40qy7qxa0LZcjkpMBB60uCss+KejnWu+d95C8GmdupgYrPuxvkUdn7qfYOaPM2V1K6tfUCQspCMYchVbRvNP8x2OeUxRewKUHltXLfRIMCCPT0ZHy3LurGrHlQKrx23PLpshMt1DXlYceu/P3OSNj/zXrJ4bAb5dOGMZpGT7d+1q2ZHbPG+/c1mbYGTFAkcKhOzlaJ+kzG+qEx0DICNy5QoWVYP4ETsCv0pkwJgV9FUq4pBPjiNgAG3Ifelhx7QZv00w1/SFwfTzorZqv7O0THdVzaMQsdyueHAsphUETJo0Twq09o6C8q+r4uhA3LAd0lzAKwpEW8czcQqTx6nv8j9O52Zt87qVDP11xWwp6s9s7JSA9+c7MyUDsFgYuELEg28TJrF0IMoXZfHW3LVO8iqCIY1p67Q+5xmahrbBr68kqzetpfkbN5NAFyfo6M3p5SbfWLDiV1l97lSJtCw1FSZMulMTWMbWJUHhLb0VVivWv0d0ad3i5AssTKpV6kRoXVlLGvLo6qwq65VhXa+Jrb6nJbXJ9D5emvUXWFiWJ2aZcFCY1oIABX0I3tWg7Qg3pUN3YH9sU0nBw6VKg3NKizrhV2xik18v9jwEjgivz6QaguyAAhox3APSoqLA/Ew3c/Sg8dsQeKNLQewEm0my80qpYGw2FuQegKb2yc4VFhWlV1hskwAJkliGZUDDqvqAv27dotIExWWHzRSDKhgvFYUebHwrNplnyYOCjd2SWWV5by9wzVnk+1GaJRFKZJI3YllsSBHlieWXSfLCMiCOTdDzOS01JJr2GDfHpaTpQerIxyKdS2lV5sfTCNJTK7gWPHL0WREcm8NzB+yTcWmNUwjtxSTTT8iJ4pEvad75WdJE/vYFuNVy0qnWAc/Jd0yJEvJ8aWVDcsdlUcLbOtxWbPMATuENbpGqXFsKK80ZEF9Q8VjWUsZjYH5j/x+Dj+mDg0PbXlbQxY/cejftfRonYOpYLp1IbZT6oexoWw5n57Yl/oQzPYUKDptrXHqAPBPNcsEQkT9lbIEs0bdH+HQsk27wjqF6SA8ANB0U6a4YEs377J8Dvbr0eLSS2Aq2dI/fYjHLgkR2yLNiIal8651XJ6oT5s6SRO3OXszu7qCzaEDgDHhL/wWTQWjnqiv6toGIw+bda915c/qbSGDZc3zxl6HraiDCVYOAgr984J3P2hIc122zL4tD7AclA8vla604pqWDfOcbbDFhzU22BR/Y+eE23YaMLjKtKgbqFXY1dwBnFZa8W0vzO822X14nVAf1At/o55ettOYAJtsmT2q+f4cmfPORiMa1FkBBTQ2aHF/lBNo2KbM2XXTTot/2IQ4pYXJAUsdH1ujlJdFY6sGvQCN6s5YtKkby6qwq8GkWGnl0AntWjnaPWwGYIcn99GG9EyyfFnItGzBjt3UzgqNCVodrPqed9jP3loZwa7zgumkJZu+ble1kVXAqrKWVpVlVdnVAqbZFUprWmMxy9TsismZxJ7imraykFSeOEbtrABQxVvTmoU3ykJjvlG0mRaWH7D4hAU6UzLc2VU6E6OyDUZwbbxnqvSF2LE0Mt8BMXa5Z7C6sawXdrUzLTRtLM/KifktgO15y60aZy4TZmu+P0/GLy0QsoIuqGNlWySZsYMUOkdvADidt7WCyZLiiJmtFZPHKN1XtPSOM7fCaiLRQmzZ4mw7CIRlOoCSb3uZVkRUAyHzPbG9JJa3uohYFp01lpduYCH6xWe3Eq8dEe2C7T/YuiQiDLwhRvj2wn7z36Shamu+M6XzjaT8lV9dfguK7aUU+gsQkAZze3WNJe/GdKqu0fhLIRqYFWCd9pfCiGteGz+CzBp9v/LDRGSNJDZPjLP71gZfVV6rqb/gQger+WURbmUmbMrmHdprmcarkPBqH5N+Db/WJ4VPh2JbdTxfPYTgFv7y3CkDjcrrh7zKPEy3Yo2AdjJk6Fd0UF6vLmkRr4fCc0DGhJMi3nLDWBvBnRCwkACDFuYSu25M6XwTY7cg6d+tu6Y7wqNWwVtG8HDrG9Y18inJhgXFWBgR4O+0sgYSCOr0jXNhsK6P8CtrcCr565MT/bW5P2CTvtB455df0AdeXx0B2rY/uoosZiw3JSNNM/cgEXBVgwWkYtCLUBaAimDPbugsZob3zQesMmhhyCgAuDrb6kMhVh7xN9WdqRICmA8DgmHt7S1l9NdMr4rK8sHqmxJgDdAuXiUEEix4Vy/y9LD+ZHhKclSAeqNoE122cQep+s9Z4fdcBkzP8sHqmxpgdU07PmctCVV/Lf1NUvtrOesO6XUbX/JnZl6zbQztpViMXVZZRQo/OeBY7muPjmQBlv/fEX3zCFjdFhRsoIve/9hTAQBwNWNPGYOKjAd3j4+Rgt43H7DKBrYFaBHJx9vA0vMevM8S0PnmW0yAtQMXs1AyfeuFhaFVp2QM8YHqW+MA1mwF23dRaNKSA0cdda5uSI2lM5CmMc2LoA0zbH5T+HbFACvKLMhYN9psgm++wf4vwACwaw/iLA1MawAAAABJRU5ErkJggg==';
                                doc.addImage(imgData, 'PNG', 40, 30);
                                doc.text("Passsagem por POI", 40, 90);
                            }
                        });
                        doc.save("Histórico_Paradas_POI_"+year+"-"+month+"-"+day+".pdf");

                        break;
                    case 'CSV': 
                        var content = '<style>';
                        content += '    table { border: 1px solid #999; border-bottom: none; width: 100%; }';
                        content += '    thead { border: 5px solid #000; }';
                        content += '    tbody td { border: 1px solid #ccc; }';
                        content += '</style>';              
                        content += '<table><thead"><th>Placa</th><th>Hora Inicial</th><th>Hora Final</th><th>Tempo Total</th><th>Local</th><th>POI</th></thead><tbody>';
                        
                        $(json).each(function(i, row) {
                            horaFim = row.data_report_fim+' às '+row.hora_min_fim;
                            if(row.data_report_fim == null){
                                horaFim = '--';
                            }
                            speedFim = minutesToHMS(row.total_time * 60);
                            if(row.total_time == 0){
                                speedFim = '--';
                            }
                            content += '<tr><td>'+row.label+'</td><td>'+row.data_report_inicio+' às '+row.hora_min_inicio+'</td><td>'+horaFim+'</td><td>'+speedFim+'</td>';
                            content += '<td>'+row.initial_full_address+'</td><td>'+row.initial_poi_name+'</td></tr>';
                        });           
                        content += '</tbody></table>';

                        window.open('data:application/vnd.ms-excel,' + content);

                        break;     
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                createMsgBox('error', error.responseText, '#'+varReportstoppoi.widgetName+' .panel-body'); 
            },
            complete: function() {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                varReportstoppoi.widgetObj.find('.dropdown-toggle').removeAttr("disabled"); 
                varReportstoppoi.widgetObj.find('.sp-load').remove(); 
            }
        }); 
    },
    openMap: function (lat, lng, drawMark){
        if(typeof(varMapBox) != 'object'){
            handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 4);

            clearTimeout(varReportstoppoi.callMap);
            varReportstoppoi.callMap = window.setTimeout(function() {
                widgetsReportStopPoi.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }
        if(typeof(varMapBox.mapInstance) != 'object'){
            clearTimeout(varReportstoppoi.callMap);
            varReportstoppoi.callMap = window.setTimeout(function() {
                widgetsReportStopPoi.openMap(lat, lng, drawMark);
            }, 2000); 

            return false;
        }

        MapBox.centerUnit(lat, lng, false, null, drawMark);
    }
};

function load_widgetsreportstoppoi_Page(){
    $.ajax({
        type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'reportstoppoi/index/'},
        dataType: 'json',
        beforeSend: function () {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportstoppoi) != 'object'){ return false; }
            varReportstoppoi.widgetObj.find('.btn').attr('disabled', 'disabled');
            varReportstoppoi.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportstoppoi) != 'object'){ return false; }

            widgetsReportStopPoi.buildGroup(JSON.parse(data.groupinfo));
            widgetsReportStopPoi.buildGroup_poi(JSON.parse(data.groupinfo));
            widgetsReportStopPoi.buildSubGroup(JSON.parse(data.subgroupinfo));

            varReportstoppoi.widgetObj.find('form').find('#start_day').val(data.date);
            varReportstoppoi.widgetObj.find('form').find('#end_day').val(data.date);
            varReportstoppoi.widgetObj.find('form').find('#end_hour').val(data.hour);

            varReportstoppoi.widgetObj.find('form').removeClass('hidden');
            varReportstoppoi.widgetObj.find('.load-report').addClass('hidden');
            //liberando menu
            varReportstoppoi.widgetObj.find('.btn').removeAttr("disabled");
            loadingWidget('#'+varReportstoppoi.widgetName, true); 
            varReportstoppoi.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');

        },
        error: function(error) {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportstoppoi) != 'object'){ return false; }
            createMsgBox('error', error.responseText, '#'+varReportstoppoi.widgetName+' .panel-body');
        },
        complete: function() {
            //para de processar caso não exista widget seja fechado.
            if(typeof(varReportstoppoi) != 'object'){ return false; }
            varReportstoppoi.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
    load_widgetsreportstoppoi_Page();

    //Plugin Clockpicker
    varReportstoppoi.widgetObj.find('.clockpicker').clockpicker({
        autoclose: true,
        'default': 'now'
    });    
    //Datepicker
    varReportstoppoi.widgetObj.find('.pluginDate').datepicker({ 
        autoclose: true, 
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        todayHighlight: true,
    });

    varReportstoppoi.widgetObj.find('select#group').on('change', function() {
        var select = 'select#subgroup';
        var data = 
        {
            groupIds: $(this).val()
        };
        varReportstoppoi.numEventsG++;
        data = JSON.stringify(data);

        var html = '';
        var selected = 'selected';
        if($(this).val() == 0) {
            html += '<option value="0" selected>Todos</option>';
            selected  = '';
        }
        
        $.ajax({
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'client/listSubGroups/', data:JSON.parse(data) },
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                varReportstoppoi.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                varReportstoppoi.widgetObj.find('select#unit').addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                try {
                    var subgroups = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReportstoppoi.widgetName+' .panel-body');              
                    varReportstoppoi.widgetObj.find(select).html('');
                    varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                
                $(subgroups).each(function(i, val) {
                    html+='<option value="'+val.id+'" '+selected+'>'+val.name+'</option>';
                });

                varReportstoppoi.numEventsG--;
                if(varReportstoppoi.numEventsG == 0) {
                    varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                    varReportstoppoi.widgetObj.find(select).html(html);
                    varReportstoppoi.widgetObj.find('select#subgroup').change();
                }
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                createMsgBox('error', error.responseText,'#'+varReportstoppoi.widgetName+' .panel-body');                 
                varReportstoppoi.widgetObj.find(select).html(error.responseText);
                varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
            }
        });     
    });

    varReportstoppoi.widgetObj.find('select#group_poi').on('change', function() {
        var select = 'select#pois';
        var data = 
        {
            group_id: $(this).val()
        };
        varReportstoppoi.numEventsG++;
        data = JSON.stringify(data);

        var html = '';
        var selected = 'selected';
        if($(this).val() == 0) {
            html += '<option value="0" selected>Todos</option>';
            selected  = '';
        }
        
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'poi/getByGroup/', data:JSON.parse(data) },
            beforeSend: function () {
                varReportstoppoi.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                //varReportstoppoi.widgetObj.find('select#unit').addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }

                try {
                    var poi = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReportstoppoi.widgetName+' .panel-body');              
                    varReportstoppoi.widgetObj.find(select).html('');
                    varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                html = '';
                html+='<option value="0" selected>Todos</option>';

                $.each($(poi.data), function(i, val) {
                    html+='<option value="'+val.poi_id+'" '+selected+'>'+val.poi_name+'</option>';
                });

                varReportstoppoi.numEventsG--;
                if(varReportstoppoi.numEventsG == 0) {
                    varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                    varReportstoppoi.widgetObj.find(select).html(html);
                    varReportstoppoi.widgetObj.find('select#grupos_poi').change();
                }
            },
            error: function(error) {
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varReportstoppoi.widgetName+' .panel-body');                 
                varReportstoppoi.widgetObj.find(select).html(error.responseText);
                varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
            }
        });     
    });
    
    varReportstoppoi.widgetObj.find('select#subgroup').on('change', function() {
        var select = 'select#unit';

        var html = '';      
        if($(this).val() == '0') {
            html += '<option value="0">Todos</option>';
        }
        var data = 
        {
            subGroupIds: $(this).val()
        };
        varReportstoppoi.numEvents++;
        
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'unit/listTrackedUnit/', data:JSON.parse(data) },
            beforeSend: function () {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                varReportstoppoi.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                try {
                    var unities = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varReportstoppoi.widgetName+' .panel-body');              
                    varReportstoppoi.widgetObj.find(select).html('');
                    varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                html = '<option value="0" selected>Todos</option>';
                $(unities).each(function(i, val) {
                    if(val.label != undefined){
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    }   
                });
                
                varReportstoppoi.numEvents--;
                if(varReportstoppoi.numEvents == 0) {
                    varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                    varReportstoppoi.widgetObj.find(select).html(html);
                }                   
            },
            error: function(error) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varReportstoppoi) != 'object'){ return false; }
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varReportstoppoi.widgetName+' .panel-body');                 
                varReportstoppoi.widgetObj.find(select).html(error.responseText);
                varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
            }
        });
    });

    varReportstoppoi.widgetObj.find('input#searchUnits').on('keyup', function(e) {
        e.preventDefault();
        if(varReportstoppoi.backupElem == '') {
            varReportstoppoi.backupElem = varReportstoppoi.widgetObj.find('select#unit').html();
        }
        if($(this).val().length > 2) {
            var select = 'select#unit';
            var html = '';
            var data = 
            {
                unit: $(this).val()
            };
            data = JSON.stringify(data);
            varReportstoppoi.numSearchs++;
             $.ajax({
                async : true,
                type : "POST",
                data: { url: 'unit/searchUnit', data: JSON.parse(data) },
                url : 'widgets/routing.php',
                dataType : "json",
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstoppoi) != 'object'){ return false; }
                    varReportstoppoi.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small"></option>');
                },
                success : function(json) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstoppoi) != 'object'){ return false; }
                    varReportstoppoi.numSearchs--;
                    if(json.length <= 0) {
                        if(varReportstoppoi.numSearchs == 0) {
                            varReportstoppoi.widgetObj.find(select).html('');
                            varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                            return false;
                        }
                    }
                    html+='<option value="0" selected>Todos</option>';
                    $(json).each(function(i, val) {
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    });
                    if(varReportstoppoi.numSearchs == 0) {
                        varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                        varReportstoppoi.widgetObj.find(select).html(html);
                    }
                },
                error : function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstoppoi) != 'object'){ return false; }
                    varReportstoppoi.numSearchs--;
                    if(varReportstoppoi.numSearchs == 0) {
                        createMsgBox('error', error.responseText, "#"+varReportstoppoi.widgetName+" .panel-body");
                        varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                    }
                }
            });
        } else if($(this).val().length <= 2) {
            if(varReportstoppoi.backupElem) {
                varReportstoppoi.widgetObj.find('select#unit').html(varReportstoppoi.backupElem);
                varReportstoppoi.backupElem = '';
            }
        } else {
            varReportstoppoi.backupElem = '';
            return false;       
        }
    });

    varReportstoppoi.widgetObj.find('input#searchPoi').on('keyup', function(e) {
        e.preventDefault();
        if($(this).val().length > 2) {
            var select = 'select#pois';
            var html = '';
            var data = 
            {
                name: $(this).val()
            };
            data = JSON.stringify(data);
            varReportstoppoi.numSearchs++;
            $.ajax({
                async : true,
                type : "POST",
                data: { url: 'poi/searchPOI', data: JSON.parse(data) },
                url : 'widgets/routing.php',
                dataType : "json",
                beforeSend: function () {
                    varReportstoppoi.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
                },
                success : function(json) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstoppoi) != 'object'){ return false; }
                
                    varReportstoppoi.numSearchs--;
                    if(json.length <= 0) {
                        if(varReportstoppoi.numSearchs == 0) {
                            varReportstoppoi.widgetObj.find(select).html('');
                            varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                            return false;
                        }
                    }
                    html+='<option value="0" selected>Todos</option>';
                    $(json).each(function(i, val) {
                        html+='<option value="'+val.id+'">'+val.name+'</option>';
                    });
                    if(varReportstoppoi.numSearchs == 0) {
                        varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                        varReportstoppoi.widgetObj.find(select).html(html);
                    }
                },
                error : function(error) {
                    varReportstoppoi.numSearchs--;
                    if(varReportstoppoi.numSearchs == 0) {
                        createMsgBox('error', error.responseText, "#"+varReportstoppoi.widgetName+" .panel-body");
                        varReportstoppoi.widgetObj.find(select).removeClass('select-loader');
                    }
                }
            });
        } 
    });

    varReportstoppoi.widgetObj.find('#pois').die('click').live('click', function (){
        var id = $(this).val();
        var label = $(this).find('option:selected').text();        
        var labelArr = label.match(/.{1,8}/g); //divide a string a cada 8 caracteres
        var divTodos = '<div class="selectedPOI bg-color-primary no-select" id="0" title="Click para remover esse POI">Todos</div>';


        if(id.length > 1){
            for (i = 0; i < id.length; i++) { 
                var div = '<div class="selectedPOI bg-color-primary no-select" id="'+id[i]+'" title="Click para remover esse POI">'+labelArr[i]+'</div>';
                var content = varReportstoppoi.widgetObj.find('.selectedPOIList').html();
                if(divTodos == div){
                    varReportstoppoi.widgetObj.find('.selectedPOIList').html(divTodos);
                    return false;
                }
                if((content.indexOf(div) < 0) && (div != divTodos)){
                    if(content == divTodos){
                        varReportstoppoi.widgetObj.find('.selectedPOIList').html(div);
                    }else{
                        varReportstoppoi.widgetObj.find('.selectedPOIList').append(div);
                    }
                }
            }
        }else{  
            var div = '<div class="selectedPOI bg-color-primary no-select" id="'+id+'" title="Click para remover esse POI">'+label+'</div>';
            var content = varReportstoppoi.widgetObj.find('.selectedPOIList').html();

            if(divTodos == div){
                varReportstoppoi.widgetObj.find('.selectedPOIList').html(divTodos);
                return false;
            }
            if((content.indexOf(div) < 0) && (div != divTodos)){
                if(content == divTodos){
                    varReportstoppoi.widgetObj.find('.selectedPOIList').html(div);
                }else{
                    varReportstoppoi.widgetObj.find('.selectedPOIList').append(div);
                }  
            }        
        }
    });

    varReportstoppoi.widgetObj.find('#unit').die('click').live('click', function (){
        var id = $(this).val();
        var label = $(this).find('option:selected').text();        
        var labelArr = label.match(/.{1,8}/g); //divide a string a cada 8 caracteres
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';

        if(id.length > 1){
            for (i = 0; i < id.length; i++) { 
                var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id[i]+'" title="Click para remover esse unidade">'+labelArr[i]+'</div>';
                var content = varReportstoppoi.widgetObj.find('.selectedUnitList').html();
                if(divTodos == div){
                    varReportstoppoi.widgetObj.find('.selectedUnitList').html(divTodos);
                    return false;
                }
                if((content.indexOf(div) < 0) && (div != divTodos)){
                    if(content == divTodos){
                        varReportstoppoi.widgetObj.find('.selectedUnitList').html(div);
                    }else{
                        varReportstoppoi.widgetObj.find('.selectedUnitList').append(div);
                    }
                }
            }
        }else{  
            var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id+'" title="Click para remover esse unidade">'+label+'</div>';
            var content = varReportstoppoi.widgetObj.find('.selectedUnitList').html();
            if(divTodos == div){
                varReportstoppoi.widgetObj.find('.selectedUnitList').html(divTodos);
                return false;
            }
            if((content.indexOf(div) < 0) && (div != divTodos)){
                if(content == divTodos){
                    varReportstoppoi.widgetObj.find('.selectedUnitList').html(div);
                }else{
                    varReportstoppoi.widgetObj.find('.selectedUnitList').append(div);
                }  
            }        
        }
    }); 

    varReportstoppoi.widgetObj.find('.selectedPOI').die('click').live('click', function (){
        $(this).remove();
        var divTodos = '<div class="selectedPOI bg-color-primary no-select" id="0" title="Click para remover esse POI">Todos</div>';
        var content = varReportstoppoi.widgetObj.find('.selectedPOIList').html();

        if(content == ''){
            varReportstoppoi.widgetObj.find('.selectedPOIList').html(divTodos);
        }
    }); 

    varReportstoppoi.widgetObj.find('.selectedUnit').die('click').live('click', function (){
        $(this).remove();
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';
        var content = varReportstoppoi.widgetObj.find('.selectedUnitList').html();

        if(content == ''){
            varReportstoppoi.widgetObj.find('.selectedUnitList').html(divTodos);
        }
    }); 

    varReportstoppoi.widgetObj.find('.closeReportTab').die('click').live('click', function (){
        var id = $(this).attr('id');
        varReportstoppoi.widgetObj.find('#tab-result-stoppoi-'+id).empty().remove();
        varReportstoppoi.widgetObj.find('#result-stoppoi-'+id).empty().remove();
        varReportstoppoi.widgetObj.find('#search-stoppoi').trigger('click');
        window.setTimeout(function() {
            varReportstoppoi.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled'); 
        }, 50);  
        varReportstoppoi.dataFilterArr[id] = null;
        varReportstoppoi.table[id]         = null; 
    });  

    varReportstoppoi.widgetObj.find('#search').die('click').live('click', function (){
        varReportstoppoi.widgetObj.find('.dropdown-toggle').attr('disabled', 'disabled');     
    }); 

    // FORM TIPO DE CONTA
    varReportstoppoi.widgetObj.find('form').submit(function(e) {
        e.preventDefault();
        if ( $(this).parsley().isValid() ) {       
            
            var unitIds = [];

            varReportstoppoi.widgetObj.find(".selectedUnitList > div").each( function(index, value) {
                unitIds.push($(this).attr('id'));
            });
            
            var dataFilter = 
                {
                    last_id: 0,
                    start_day: $(this).find('#start_day').val(),
                    end_day: $(this).find('#end_day').val(),
                    start_hour: $(this).find('#start_hour').val(),
                    end_hour: $(this).find('#end_hour').val(),
                    group:  $(this).find('select#group').val(),
                    subgroup:  $(this).find('select#subgroup').val(),
                    units: unitIds,
                    limit: varReportstoppoi.processingSearchLimit
                };
            dataFilter = JSON.stringify(dataFilter);
            
            $.ajax({
                async: true,
                type: 'POST',
                url: 'widgets/routing.php',
                data: { url: 'reportstoppoi/search/', data: JSON.parse(dataFilter) },
                dataType: 'json',
                beforeSend: function () {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstoppoi) != 'object'){ return false; }
                    loadingWidget('#'+varReportstoppoi.widgetName);
                },
                success: function(data) {
                    //para de processar caso não exista widget seja fechado.
                    console.log(data);
                    if(typeof(varReportstoppoi) != 'object'){ return false; }
                    varReportstoppoi.dataFilterArr.push(dataFilter);
                    loadingWidget('#'+varReportstoppoi.widgetName, true); 

                    varReportstoppoi.widgetObj.find('.tab-content').append('<div class="col-md-12 tab-pane" id="tab-result-stoppoi-'+varReportstoppoi.tabResults+'"></div>');
                    varReportstoppoi.widgetObj.find('#widgetsreportstoppoi-nav').append('<li class=""><a href="#tab-result-stoppoi-'+varReportstoppoi.tabResults+'"  id="result-stoppoi-'+varReportstoppoi.tabResults+'" data-toggle="tab">Resultado <i class="fa fa-times closeReportTab" id="'+varReportstoppoi.tabResults+'"></i></a></li>');

                    varReportstoppoi.widgetObj.find('#result-stoppoi-'+varReportstoppoi.tabResults).die('click').live('click', function (){
                        var id = $(this).attr('id');
                        id = id.replace('result-stoppoi-', '');

                        var dropDownMenu = '<li><a href="javascript:;" onClick="widgetsReportStopPoi.print(\'print\', '+id+')">Imprimir</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportStopPoi.print(\'PDF\', '+id+')">Exportar PDF</a></li>';
                        dropDownMenu += '<li><a href="javascript:;" onClick="widgetsReportStopPoi.print(\'CSV\', '+id+')">Exportar CSV</a></li>';

                        varReportstoppoi.widgetObj.find('#widgetsreportstoppoi-dropdown').html(dropDownMenu);   
                        varReportstoppoi.widgetObj.find('.dropdown-toggle').removeAttr("disabled");                 
                    }); 
                    var numRows = varReportstoppoi.rowSizeMin;
                    var panelWidget = varReportstoppoi.widgetObj.closest('.panel');
                    if(panelWidget.hasClass('full-Scr')){
                        numRows = Math.round(panelWidget.height() / varReportstoppoi.rowSizeMax) - varReportstoppoi.rowAdjust;
                    }
                    varReportstoppoi.widgetObj.find('#tab-result-stoppoi-'+varReportstoppoi.tabResults).html( '<table class="table table-striped table-bordered" data-page-length="'+numRows+'" id="table-list-result-stoppoi-'+varReportstoppoi.tabResults+'"></table>' );
                    varReportstoppoi.table.push(null);
                    varReportstoppoi.table[varReportstoppoi.tabResults] = varReportstoppoi.widgetObj.find('#table-list-result-stoppoi-'+varReportstoppoi.tabResults).DataTable({
                        "data": data,
                        paginate: true,
                        bLengthChange: false,
                        bFilter: false,
                        bInfo: true,
                        "ordering": true,
                        "pagingType": "full",
                        "fnDrawCallback": function( oSettings ) {
                            window.setTimeout(function() {
                                buildRevGeoFromTable(varReportstoppoi.widgetName);
                            }, 500);
                        },
                        "columns": 
                            [
                                { "title": "Placa", "data": "label", "className": "larguraPlaca" },
                                { "title": "Hora&nbsp;Inicial", "data": "data_inicio", "className": "larguraData" },
                                { "title": "Hora&nbsp;Final", "data": "data_fim", "className": "larguraData" },
                                { "title": "Tempo", "data": "total_parado_poi", "className": "10_p" },
                                { "title": "Local", "data": "initial_full_address", "className": "30_p" },
                                { "title": "POI", "data": "final_poi", "className": "15_p" },
                                { "title": "Date Order", "data": "full_date_begin", "visible": false }
                            ],
                        "columnDefs": 
                            [
                                {
                                    "targets": [0],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" long="'+full.initial_longitude+'" lat="'+full.initial_latitude+'">' + data + '</div>';
                                    }
                                },
                                {
                                    "targets": [1],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + full.data_inicio + ' às ' + full.hora_min_inicio + '">' + full.data_inicio + ' às ' + full.hora_min_inicio + '</div>';
                                    }
                                },
                                {
                                    "targets": [2],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        if(full.data_fim == null){
                                            return '<div class="nowrap ellipsis"> -- </div>'; 
                                        }else{
                                            return '<div class="nowrap ellipsis" title="' + full.data_fim + ' às ' + full.hora_min_fim + '">' + full.data_fim + ' às ' + full.hora_min_fim + '</div>';                                            
                                        }
                                    }
                                },
                                {
                                    "targets": [3],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        if(full.total_parado_poi == null){
                                            return '<div class="nowrap ellipsis"> -- </div>'; 
                                        }else{
                                            var string = full.total_parado_poi;
                                            var retorno = string.split(":");

                                            var hour = retorno[0];
                                            var min = retorno[1];

                                            if(hour < 1){
                                                return '<div class="nowrap ellipsis" title="'+ min +" minutos"+ '">'+min +"min"+ '</div>';
                                            }else{
                                                return '<div class="nowrap ellipsis" title="' + hour+" Horas e "+min +" minutos"+ '">' + hour+"H e "+min +"min"+ '</div>';
                                            }
                                            
                                        }
                                    }
                                },
                                {
                                    "targets": [4],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        //verifica se este endereco ja existe no banco
                                        if(data != '') {
                                            //se sim ja retorna o endereco
                                            return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                        } else {
                                            //se nao faz o reversegeo para capturar o endereco e salvar no banco
                                            return "<div class='mapAddress nowrap ellipsis' hist='"+full.devst_id+"' long='"+full.initial_longitude+"' lat='"+full.initial_latitude+"'>Carregando...</div>" ;
                                        }
                                    }
                                },
                                {
                                    "targets": [5],
                                    "orderable": false,
                                    "render": function (data, type, full, meta) {
                                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                                    }
                                }
                            ],
                        "language" : {
                            "emptyTable":     "Ainda não há registros",
                            "info" : "Total de _TOTAL_ registros",
                            "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
                            "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
                            "infoPostFix":    "",
                            "thousands":      ".",
                            "lengthMenu":     "Mostre _MENU_ registros",
                            "loadingRecords": "Carregando...",
                            "processing":     "Processando...",
                            "search":         "",
                            "searchPlaceholder": "Procure por unidade...",
                            "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
                            "paginate": {
                                "first":      "<<",
                                "last":       ">>",
                                "next":       ">",
                                "previous":   "<"
                            }
                        },
                        "order": [[ 0, 'desc' ], [ 0, 'asc' ], [ 1, 'desc' ]],
                        "initComplete": function(settings) {
                            if( data.length > 0){
                                varReportstoppoi.api.push(this.api());

                                varReportstoppoi.widgetObj.find('.larguraData').css('width', '120px');
                                varReportstoppoi.widgetObj.find('.larguraPlaca').css('width', '80px');
                                varReportstoppoi.widgetObj.find('.10_p').css('width', '10%');
                                varReportstoppoi.widgetObj.find('.15_p').css('width', '15%');
                                varReportstoppoi.widgetObj.find('.30_p').css('width', '30%');

                                dataFilter = JSON.parse(dataFilter);
                                dataFilter.last_id = data[data.length - 1].id;
                                if( data.length >= varReportstoppoi.processingSearchLimit ){
                                    widgetsReportStopPoi.processingSearch(varReportstoppoi.tabResults, dataFilter);
                                } 
                                
                                varReportstoppoi.widgetObj.find('#table-list-result-stoppoi-'+varReportstoppoi.tabResults+' tbody tr').die("click").live("click", function(){          
                                    var label = $(this).children('td').html();
                                    var lat = $(label).attr('lat');
                                    var lng = $(label).attr('long');
                                    label = $(label).html();

                                    var drawMark = [];
                                        drawMark.size = varGlobal.marker.Size;
                                        drawMark.symbol = varGlobal.marker.carSymbol;
                                        drawMark.color = varGlobal.marker.color;

                                    widgetsReportStopPoi.openMap(lat, lng, drawMark);
                                }); 
                            }
                        }
                    });              
                    //reseta o form   
                    varReportstoppoi.widgetObj.find('#result-stoppoi-'+varReportstoppoi.tabResults).trigger('click');  
                    varReportstoppoi.tabResults++;
                    //resetReportAction(); 
                },
                error: function(error) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varReportstoppoi) != 'object'){ return false; }
                    loadingWidget('#'+varReportstoppoi.widgetName, true);
                    //Criando mensagem de retorno
                    createMsgBox('error', error.responseText, '#'+varReportstoppoi.widgetName+' .panel-body'); 
                }
            });
            return false;
        }
    }); 
});

