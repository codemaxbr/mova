var varFreecommand = {};
	varFreecommand.widgetName 	= 'widgetsfreecommand';
	varFreecommand.varName 		= 'varFreecommand';
	varFreecommand.widgetObj 	= $('#'+varFreecommand.widgetName);
	varFreecommand.action 		= 'add';
	varFreecommand.method;
    varFreecommand.numEventsG 	= 0;
    varFreecommand.numEvents 	= 0;
    varFreecommand.numSearchs 	= 0;
    varFreecommand.backupElem 	= '';

var widgetsfreecommand = {
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	reset: function (){
    	if(typeof(varFreecommand) != 'object'){return false;}
		var form = varFreecommand.widgetObj.find('form');
		varFreecommand.action = 'add'; 

		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');

		form[0].reset();
	},
    buildCommandType: function (data){
        varFreecommand.widgetObj.find('#command_type').html('<option value="" selected>Selecionar um comando</option>');
        var template =  '{{#commandTypeinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/commandTypeinfo}}';
        var select = Mustache.to_html(template, data);
        varFreecommand.widgetObj.find('#command_type').append(select);
    },
    buildGroup: function (data){
        //console.log(data);
        varFreecommand.widgetObj.find('#group').html('<option value="0" selected>Todos</option>');
        var template =  '{{#groupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/groupinfo}}';
        var select = Mustache.to_html(template, data);
        varFreecommand.widgetObj.find('#group').append(select);
    },
    buildSubGroup: function (data){
        varFreecommand.widgetObj.find('#subgroup').html('<option value="0" selected>Todos</option>');
        var template =  '{{#subgroupinfo}}'+
                        '   <option value="{{id}}">{{name}}</option>'+
                        '{{/subgroupinfo}}';
        var select = Mustache.to_html(template, data);
        varFreecommand.widgetObj.find('#subgroup').append(select);
    },
	buildTable: function (){
    	if(typeof(varFreecommand) != 'object'){return false;}
		varFreecommand.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-freecommand" width="100%"></table>' );
		//Pega os dados da tabela
		var table_groups = varFreecommand.widgetObj.find('#table-list-freecommand').dataTable({
		    ajax: {
		        "url": 'widgets/routing.php',
		        "type": "POST",
		        "data": { url: 'freecommand/list/'},
		  		error: function (error) {  
    				if(typeof(varFreecommand) != 'object'){return false;}
		        	loadingWidget('#'+varFreecommand.widgetName, true);
					createMsgBox('error', error.responseText, '#'+varFreecommand.widgetName+' .panel-body');
		  		}
	      	},
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "fnDrawCallback": function( oSettings ) {
	    		varFreecommand.widgetObj.find('.10_p').css('width', '10%');
	    		varFreecommand.widgetObj.find('.25_p').css('width', '25%');
	    		varFreecommand.widgetObj.find('.20_p').css('width', '20%');
		    },
		    "columns": 
		    	[
		            { "title": "ID", "data": "id", 'class': 'hidden'},
		            { "title": "<div class='labelHeader nowrap ellipsis' title='Unidade'>Unidade</div>", "data": "unit_name", "className": "goToEdit 10_p"},
		            { "title": "<div class='labelHeader nowrap ellipsis' title='Status'>Status</div>", "data": "status_name", "className": "goToEdit 25_p"},
		            { "title": "<div class='labelHeader nowrap ellipsis' title='Tipo'>Tipo</div>", "data": "type_name", "className": "goToEdit 25_p"},
		            { "title": "<div class='labelHeader nowrap ellipsis' title='Execução'>Execução</div>", "data": "date_exec", "className": "goToEdit 20_p"},
		            { "title": "<div class='labelHeader nowrap ellipsis' title='Criado Em'>Criado Em</div>", "data": "date_insert", "className": "goToEdit 20_p"}
	          	],
            "columnDefs": 
            [
                {
                    "targets": [ 1 ], 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                    }
                },
                {
                    "targets": [ 2 ], 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                    }
                },
                {
                    "targets": [ 3 ], 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                    }
                },
                {
                    "targets": [ 4 ], 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                    }
                },
                {
                    "targets": [ 5 ], 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                    }
                }
            ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por comandos enviados...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
	    		varFreecommand.widgetObj.find('.10_p').css('width', '10%');
	    		varFreecommand.widgetObj.find('.25_p').css('width', '25%');
	    		varFreecommand.widgetObj.find('.20_p').css('width', '20%');

    			if(typeof(varFreecommand) != 'object'){return false;}
		    	//CLICANDO NA CONTA PARA EDITÃ�-LA
		    	if(json.data.length > 0){

		    		varFreecommand.widgetObj.find('#table-list-freecommand tbody tr').die("click").live("click", function(){
			    		//Pegando id da conta
	    				var data = $(this).children('td').html();
			    	    //loadingWidget('#'+varFreecommand.widgetName);		
	    		    	return false;
	    		    });
				}  	
		  	}
		});
	},
};

function load_widgetsfreecommand_Page(){
	if(typeof(varFreecommand) != 'object'){return false;}

	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'freecommand/index/'},
        dataType: 'json',
        beforeSend: function () {
    		if(typeof(varFreecommand) != 'object'){return false;}
			widgetsfreecommand.reset();	
			varFreecommand.widgetObj.find('.btn').attr('disabled', 'disabled');
            varFreecommand.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
            console.log(data);
        	if(typeof(varFreecommand) != 'object'){return false;}
        	widgetsfreecommand.buildTable();    
        	widgetsfreecommand.buildCommandType(JSON.parse(data.commandTypeinfo));    
            widgetsfreecommand.buildGroup(JSON.parse(data.groupinfo));
            widgetsfreecommand.buildSubGroup(JSON.parse(data.subgroupinfo));
        	//SET ID account user
        	varFreecommand.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	//liberando menu
        	varFreecommand.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varFreecommand.widgetName, true); 
        },
        error: function(error) {
    		if(typeof(varFreecommand) != 'object'){return false;}
        	loadingWidget('#'+varFreecommand.widgetName, true);
			createMsgBox('error', error.responseText, '#'+varFreecommand.widgetName+' .panel-body');
        },
        complete: function() {
    		if(typeof(varFreecommand) != 'object'){return false;}
            varFreecommand.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
	load_widgetsfreecommand_Page();

    if(typeof(varFreecommand) != 'object'){return false;}

	varFreecommand.widgetObj.find('.openForm').die("click").live("click", function(){
		widgetsfreecommand.reset();
		varFreecommand.action = 'add';
		openForm(varFreecommand.widgetName, 'add');
	});

	varFreecommand.widgetObj.find('form').find('.cancel').die('click').live('click', function() {
		closeForm(varFreecommand.widgetName);
		widgetsfreecommand.reset();		
	});	

    varFreecommand.widgetObj.find('select#command_type').on('change', function() {
    	var content = 	'<label>Comando Livre</label>'+
						'<input type="text" class="form-control input-sm" name="command" id="command" placeholder="Digite seu comando" data-parsley-length="[2, 100]" required="required" />';

   		varFreecommand.widgetObj.find('input-command').html(content);
    });

    varFreecommand.widgetObj.find('select#group').on('change', function() {
        var select = 'select#subgroup';
        var data = 
        {
            groupIds: $(this).val()
        };
        varFreecommand.numEventsG++;
        data = JSON.stringify(data);

        var html = '';
        var selected = 'selected';
        if($(this).val() == 0) {
            html += '<option value="0" selected>Todos</option>';
            selected  = '';
        }
        
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'client/listSubGroups/', data:JSON.parse(data) },
            beforeSend: function () {
                varFreecommand.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
                varFreecommand.widgetObj.find('select#unit').addClass('select-loader').html('<option class="spinner-small select"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varFreecommand) != 'object'){ return false; }

                try {
                    var subgroups = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varFreecommand.widgetName+' .panel-body');              
                    varFreecommand.widgetObj.find(select).html('');
                    varFreecommand.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                
                $(subgroups).each(function(i, val) {
                    html+='<option value="'+val.id+'" '+selected+'>'+val.name+'</option>';
                });

                varFreecommand.numEventsG--;
                if(varFreecommand.numEventsG == 0) {
                    varFreecommand.widgetObj.find(select).removeClass('select-loader');
                    varFreecommand.widgetObj.find(select).html(html);
                    varFreecommand.widgetObj.find('select#subgroup').change();
                }
            },
            error: function(error) {
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varFreecommand.widgetName+' .panel-body');                 
                varFreecommand.widgetObj.find(select).html(error.responseText);
                varFreecommand.widgetObj.find(select).removeClass('select-loader');
            }
        });     
    });
    
    varFreecommand.widgetObj.find('select#subgroup').on('change', function() {
        var select = 'select#unit';

        var html = '';      
        if($(this).val() == '0') {
            html += '<option value="0">Todos</option>';
        }
        var data = 
        {
            subGroupIds: $(this).val()
        };
        varFreecommand.numEvents++;
        
        data = JSON.stringify(data);
        $.ajax({
            async: true,
            type: 'POST',
            url: 'widgets/routing.php',
            data: { url: 'unit/listTrackedUnit/', data:JSON.parse(data) },
            beforeSend: function () {
                varFreecommand.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
            },
            success: function(data) {
                //para de processar caso não exista widget seja fechado.
                if(typeof(varFreecommand) != 'object'){ return false; }
                
                try {
                    var unities = JSON.parse(data);
                } catch(e) {
                    //Criando mensagem de retorno
                    createMsgBox('error', data, '#'+varFreecommand.widgetName+' .panel-body');              
                    varFreecommand.widgetObj.find(select).html('');
                    varFreecommand.widgetObj.find(select).removeClass('select-loader');
                    return false;
                }
                html = '<option value="0" selected>Todos</option>';
                $(unities).each(function(i, val) {
                    if(val.label != undefined){
                        html += '<option value="'+val.id+'">'+val.label+'</option>';                            
                    }
                });
                
                varFreecommand.numEvents--;
                if(varFreecommand.numEvents == 0) {
                    varFreecommand.widgetObj.find(select).removeClass('select-loader');
                    varFreecommand.widgetObj.find(select).html(html);
                }                   
            },
            error: function(error) {
                //Criando mensagem de retorno
                createMsgBox('error', error.responseText,'#'+varFreecommand.widgetName+' .panel-body');                 
                varFreecommand.widgetObj.find(select).html(error.responseText);
                varFreecommand.widgetObj.find(select).removeClass('select-loader');
            }
        });
    });

    varFreecommand.widgetObj.find('input#searchUnits').on('keyup', function(e) {
        e.preventDefault();
        if(varFreecommand.backupElem == '') {
            varFreecommand.backupElem = varFreecommand.widgetObj.find('select#unit').html();
        }
        if($(this).val().length > 2) {
            var select = 'select#unit';
            var html = '';
            var data = 
            {
                unit: $(this).val()
            };
            data = JSON.stringify(data);
            varFreecommand.numSearchs++;
             $.ajax({
                async : true,
                type : "POST",
                data: { url: 'unit/searchUnit', data: JSON.parse(data) },
                url : 'widgets/routing.php',
                dataType : "json",
                beforeSend: function () {
                    varFreecommand.widgetObj.find(select).addClass('select-loader').html('<option class="spinner-small select"></option>');
                },
                success : function(json) {
                    //para de processar caso não exista widget seja fechado.
                    if(typeof(varFreecommand) != 'object'){ return false; }
                
                    varFreecommand.numSearchs--;
                    if(json.length <= 0) {
                        if(varFreecommand.numSearchs == 0) {
                            varFreecommand.widgetObj.find(select).html('');
                            varFreecommand.widgetObj.find(select).removeClass('select-loader');
                            return false;
                        }
                    }
                    html+='<option value="0" selected>Todos</option>';
                    $(json).each(function(i, val) {
                        html+='<option value="'+val.id+'">'+val.label+'</option>';
                    });
                    if(varFreecommand.numSearchs == 0) {
                        varFreecommand.widgetObj.find(select).removeClass('select-loader');
                        varFreecommand.widgetObj.find(select).html(html);
                    }
                },
                error : function(error) {
                    varFreecommand.numSearchs--;
                    if(varFreecommand.numSearchs == 0) {
                        createMsgBox('error', error.responseText, "#"+varFreecommand.widgetName+" .panel-body");
                        varFreecommand.widgetObj.find(select).removeClass('select-loader');
                    }
                }
            });
        } else if($(this).val().length <= 2) {
            if(varFreecommand.backupElem) {
                varFreecommand.widgetObj.find('select#unit').html(varFreecommand.backupElem);
                varFreecommand.backupElem = '';
            }
        } else {
            varFreecommand.backupElem = '';
            return false;       
        }
    });

    varFreecommand.widgetObj.find('#unit').die('click').live('click', function (){
        var id = $(this).val();
        var label = $(this).find('option:selected').text();        
        var labelArr = label.match(/.{1,8}/g); //divide a string a cada 8 caracteres
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';

        if(id.length > 1){
            for (i = 0; i < id.length; i++) { 
                var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id[i]+'" title="Click para remover esse unidade">'+labelArr[i]+'</div>';
                var content = varFreecommand.widgetObj.find('.selectedUnitList').html();
                if(divTodos == div){
                	varFreecommand.widgetObj.find('.selectedUnitList').html(divTodos);
                	return false;
                }
                if((content.indexOf(div) < 0) && (div != divTodos)){
                    if(content == divTodos){
                        varFreecommand.widgetObj.find('.selectedUnitList').html(div);
                    }else{
                        varFreecommand.widgetObj.find('.selectedUnitList').append(div);
                    }
                }
            }
        }else{  
            var div = '<div class="selectedUnit bg-color-primary no-select" id="'+id+'" title="Click para remover esse unidade">'+label+'</div>';
            var content = varFreecommand.widgetObj.find('.selectedUnitList').html();
            if(divTodos == div){
            	varFreecommand.widgetObj.find('.selectedUnitList').html(divTodos);
            	return false;
            }
            if((content.indexOf(div) < 0) && (div != divTodos)){
                if(content == divTodos){
                    varFreecommand.widgetObj.find('.selectedUnitList').html(div);
                }else{
                    varFreecommand.widgetObj.find('.selectedUnitList').append(div);
                }  
            }        
        }
    }); 

    varFreecommand.widgetObj.find('.selectedUnit').die('click').live('click', function (){
        $(this).remove();
        var divTodos = '<div class="selectedUnit bg-color-primary no-select" id="0" title="Click para remover esse unidade">Todos</div>';
        var content = varFreecommand.widgetObj.find('.selectedUnitList').html();

        if(content == ''){
            varFreecommand.widgetObj.find('.selectedUnitList').html(divTodos);
        }
    }); 

	varFreecommand.widgetObj.find('form').submit(function(e) { 
		e.preventDefault();
	    if ( $(this).parsley().isValid() ) {
			var form = $(this); 
			var unitIds = [];

            varFreecommand.widgetObj.find(".selectedUnitList > div").each( function(index, value) {
                unitIds.push($(this).attr('id'));
            });

		    var data = 
				{
	    			id: form.find('#id').val(),
	    			command_type: form.find('#command_type').val(),
	    			command: form.find('#command').val(),
	    			units: unitIds
				};
			console.log(JSON.stringify(data));
			
		    data = JSON.stringify(data);
		    
	        $.ajax({
	        	async: true,
				type : 'POST',
				url : 'widgets/routing.php',
				data : {
					url : 'freecommand/'+varFreecommand.action+'/',
					data : JSON.parse(data)
				},
				dataType : 'json',
				beforeSend : function() {
    				if(typeof(varFreecommand) != 'object'){return false;}
    	        	loadingWidget('#'+varFreecommand.widgetName);
				},
				success : function(d) {
                                    console.log(d);
					var data = d;
					var msg = d.msg;

        			if(typeof(varFreecommand) != 'object'){return false;}    	        
	    	        //Criando mensagem de retorno
	    	        createMsgBox('success', msg, '#'+varFreecommand.widgetName+' .panel-body');
	    	        // atualiza tabela que lista as contas
	    	        varFreecommand.widgetObj.find('#table-list-freecommand').DataTable().ajax.reload(null, false);	

	    	        widgetsfreecommand.reset();
        			loadingWidget('#'+varFreecommand.widgetName, true);   	            	
				},
				error : function(error) {
    				if(typeof(varFreecommand) != 'object'){return false;}
    	        	loadingWidget('#'+varFreecommand.widgetName, true);
    	        	createMsgBox('error', error.responseText, '#'+varFreecommand.widgetName+' .panel-body');		
				}
			});
			return false;
	    }
	});
});



