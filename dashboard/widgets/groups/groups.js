var varGroups = {};
	varGroups.widgetName 	= 'widgetsgroups';
	varGroups.varName 		= 'varGroups';
	varGroups.widgetObj 	= $('#'+varGroups.widgetName);
	varGroups.action 		= 'add';
	varGroups.method;
	varGroups.color 		= [];
	varGroups.colorCount 	= 0;

var widgetsgroups = {
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	loadEditForm: function (data) {
		var groups = data.group[0];
		var subgroups = data.subgroup;

    	if(typeof(varGroups) != 'object'){return false;}

		var form = varGroups.widgetObj.find('form');
		//limpa formulário
		widgetsgroups.reset();
		// muda o form para edição
		varGroups.action = 'edit';

		form.find('#id').val(groups.id);
		form.find('#name').val(groups.name);

		$.each(subgroups, function(index, val) {
			if(index == 0){			
				form.find('.subgroup_id').last().val(val.id);
				form.find('.subgroup_name').last().val(val.name);
				form.find('.color_value').last().val(val.color);
				form.find('.color_value').last().css('background-color', val.color);
			}else{
				widgetsgroups.handleAddSubgroup(false);

				form.find('.subgroup_id').last().val(val.id);
				form.find('.subgroup_name').last().val(val.name);
				form.find('.color_value').last().val(val.color);
				form.find('.color_value').last().css('background-color', val.color);
			}
		});

		openForm(varGroups.widgetName, varGroups.action);
	},
	reset: function (){
    	if(typeof(varGroups) != 'object'){return false;}
		var form = varGroups.widgetObj.find('form');
		varGroups.action = 'add'; 

		varGroups.widgetObj.find('.list-subgroup').html('');
		widgetsgroups.handleAddSubgroup(true);

		form.find('.color').val('#808080');
		form.find('.color').css('background-color', '#808080');
		if(typeof(varGroups.color) == 'object'){
			$.each(varGroups.color, function(index, val) {
				$(val).colorPicker({
					customBG: '#808080'
				});
			});
		}

		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');

		form[0].reset();
	},
	delete: function () {
    	if(typeof(varGroups) != 'object'){return false;}
	    var data = { id: varGroups.widgetObj.find('form').find("#id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'groups/delete',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varGroups.widgetName);
			},
			success : function(data) {
    			if(typeof(varGroups) != 'object'){return false;}
				// cria mensagem de sucesso no widget
				createMsgBox('success', data.responseText, '#'+varGroups.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varGroups.widgetObj.find('#table-list-groups').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varGroups.widgetName);
				widgetsgroups.reset();
				loadingWidget('#'+varGroups.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varGroups.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varGroups.widgetName+' .panel-body');		
			}
		});
		return false;
	},
	handleAddSubgroup : function (primary) {
		var lastColor = varGroups.widgetObj.find('.color_value').last().val();
		varGroups.colorCount++;

		var basicContent = '<div class="col-md-12 col-sm-12 item-subgroup">';
		basicContent += '	<div class="col-md-7 col-sm-7 item-subgroup">';
		basicContent += '		<input type="hidden" class="subgroup_id" name="subgroup_id" id="subgroup_id" value="" />';
		basicContent += '		<input type="text" class="form-control input-sm subgroup_name" name="subgroup_name" id="subgroup_name" placeholder="Nome do Subgrupo" data-parsley-length="[2, 20]" required="required" />';
		basicContent += '	</div>';	
		basicContent += '	<div class="col-md-4 col-sm-4 item-subgroup input-toggles">';	
		basicContent += '		<input class="color_'+varGroups.colorCount+' no-alpha form-control input-sm color_value" value="'+lastColor+'" required="required"/>';	
		basicContent += '	</div>';	
		basicContent += '	<div class="col-md-1 col-sm-1 item-subgroup minus">';
		if(primary){
			basicContent += '		<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" style="line-height: 0px;"><i class="fa fa-minus"></i></a>';
		}else{
			basicContent += '		<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger minus-subgroup" style="line-height: 0px;" title="Remover Subgrupo"><i class="fa fa-minus"></i></a>';
		}
		basicContent += '	</div>';
		basicContent += '</div>';

		varGroups.widgetObj.find('.list-subgroup').append(basicContent);

		var colorAux = varGroups.widgetObj.find("form").find('.color_'+varGroups.colorCount).colorPicker({
				opacity: false,
				doRender: true,
				customBG: lastColor
			});
		varGroups.color.push(colorAux);
	},
	getById : function (data) {
	    $.ajax({
	    	async: true,
	    	type: 'POST',
	        url: 'widgets/routing.php',
	        data: { 
	        	url: 'groups/getById/', 
	        	data: JSON.parse(data) 
	        },
	        dataType: 'json',
	        success: function(data) {
		        widgetsgroups.loadEditForm(data);
	        	loadingWidget('#'+varGroups.widgetName, true);
	        },
	        error: function(error) {
	        	createMsgBox('error', error.responseText, '#'+varGroups.widgetName+' .panel-body');
	        	loadingWidget('#'+varGroups.widgetName, true);
	        }
	    });
	},
	buildTable: function (){
    	if(typeof(varGroups) != 'object'){return false;}
		varGroups.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-groups" width="100%"></table>' );
		//Pega os dados da tabela
		var table_groups = varGroups.widgetObj.find('#table-list-groups').dataTable({
		    ajax: {
		        "url": 'widgets/routing.php',
		        "type": "POST",
		        "data": { url: 'groups/list/'},
		  		error: function (error) {  
    				if(typeof(varGroups) != 'object'){return false;}
		        	loadingWidget('#'+varGroups.widgetName, true);
					createMsgBox('error', error.responseText, '#'+varGroups.widgetName+' .panel-body');
		  		}
	      	},
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "fnDrawCallback": function( oSettings ) {
	    		varGroups.widgetObj.find('.100_p').css('width', '100%');
		    },
		    "columns": 
		    	[
		            { "title": "ID", "data": "id", 'class': 'hidden' },
		            { "title": "Nome", "data": "name", "className": "goToEdit 100_p"  }
	          	],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por Grupo...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
	    		varGroups.widgetObj.find('.100_p').css('width', '100%');
    			if(typeof(varGroups) != 'object'){return false;}
		    	//CLICANDO NA CONTA PARA EDITÃ�-LA
		    	if(json.data.length > 0){

		    		varGroups.widgetObj.find('#table-list-groups tbody tr').die("click").live("click", function(){
			    		//Pegando id da conta
	    				var data = $(this).children('td').html();
			    	    loadingWidget('#'+varGroups.widgetName);	
			    	    widgetsgroups.getById(data);
		
	    		    	return false;
	    		    });
				}  	
		  	}
		});
	},
};

function load_widgetsgroups_Page(){
	if(typeof(varGroups) != 'object'){return false;}

	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'groups/index/'},
        dataType: 'json',
        beforeSend: function () {
    		if(typeof(varGroups) != 'object'){return false;}
        	closeForm(varGroups.widgetName);
			widgetsgroups.reset();	
			varGroups.widgetObj.find('.btn').attr('disabled', 'disabled');
            varGroups.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	if(typeof(varGroups) != 'object'){return false;}
        	widgetsgroups.buildTable();    
        	//SET ID account user
        	varGroups.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	//liberando menu
        	varGroups.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varGroups.widgetName, true); 
        },
        error: function(error) {
    		if(typeof(varGroups) != 'object'){return false;}
        	loadingWidget('#'+varGroups.widgetName, true);
			createMsgBox('error', error.responseText, '#'+varGroups.widgetName+' .panel-body');
        },
        complete: function() {
    		if(typeof(varGroups) != 'object'){return false;}
            varGroups.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
	load_widgetsgroups_Page();

    if(typeof(varGroups) != 'object'){return false;}

    varGroups.color.push(varGroups.widgetObj.find("form").find('.color').colorPicker({
			opacity: false,
			doRender: true,
			customBG: '#808080'
		})
    );

	varGroups.widgetObj.find('.openForm').die("click").live("click", function(){
		widgetsgroups.reset();
		varGroups.action = 'add';
		openForm(varGroups.widgetName, 'add');
	});
	//CHAMADA PARA EXCLUSÃO DE GRUPOS 
	varGroups.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		openConfirm(varGroups.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir Grupo", "Deseja remover o Grupo selecionado?", "Sim", "Não");
	});
	varGroups.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		widgetsgroups.delete();
		closeConfirm(varGroups.widgetName);
	});
	varGroups.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varGroups.widgetName);
	});

	varGroups.widgetObj.find('form').find('.cancel').die('click').live('click', function() {
		closeForm(varGroups.widgetName);
		widgetsgroups.reset();		
	});	

	varGroups.widgetObj.find('.plus-subgroup').die("click").live("click", function(){	
		widgetsgroups.handleAddSubgroup(false);
	});

	varGroups.widgetObj.find('.minus-subgroup').die("click").live("click", function(){	
		$(this).parent().parent().remove();
		var id = $(this).parent().parent().find('#subgroup_id').val();

		$.ajax({
	    	type: 'POST',
	        url: 'widgets/routing.php',
	        data : {
					url : 'groups/deleteSubgroup/',
					data : id
				},
	        dataType: 'json',
	        beforeSend: function () {

	        },
	        success: function(data) {
	        	if(typeof(varGroups) != 'object'){return false;}
				createMsgBox('success', data.msg, '#'+varGroups.widgetName+' .panel-body');
	        },
	        error: function(error) {
	    		if(typeof(varGroups) != 'object'){return false;}
				createMsgBox('error', error.responseText, '#'+varGroups.widgetName+' .panel-body');
	        }
	    });
	});

	varGroups.widgetObj.find('form').submit(function(e) { 
		e.preventDefault();
	    if ( $(this).parsley().isValid() ) {
			var form = $(this); 

			var subgroupNameOption = form.find('.subgroup_name');
			var subgroupName = [];
			subgroupNameOption.each(function() {
				subgroupName.push($(this).val());
			});

			var subgroupIDOption = form.find('.subgroup_id');
			var subgroupID = [];
			subgroupIDOption.each(function() {
				subgroupID.push($(this).val());
			});

			var colorOption = form.find('.color_value');
			var color = [];
			colorOption.each(function() {
				color.push($(this).val());
			});

		    var data = 
				{
	    			id: form.find('#id').val(),
	    			name: form.find('#name').val(),
	    			color: color,
	    			subgroupName: subgroupName,
	    			subgroupID: subgroupID
				};
				
                console.log(data);
		    data = JSON.stringify(data);
		    
	        $.ajax({
	        	async: true,
				type : 'POST',
				url : 'widgets/routing.php',
				data : {
					url : 'groups/'+varGroups.action+'/',
					data : JSON.parse(data)
				},
				dataType : 'json',
				beforeSend : function() {
    				if(typeof(varGroups) != 'object'){return false;}
    	        	loadingWidget('#'+varGroups.widgetName);
				},
				success : function(data) {
					var data = data.data;
					var msg = data.msg;

        			if(typeof(varGroups) != 'object'){return false;}    	        
	    	        //Criando mensagem de retorno
	    	        createMsgBox('success', data, '#'+varGroups.widgetName+' .panel-body');
	    	        // atualiza tabela que lista as contas
	    	        varGroups.widgetObj.find('#table-list-groups').DataTable().ajax.reload(null, false);	

					closeForm(varGroups.widgetName);
	    	        widgetsgroups.reset();
        			loadingWidget('#'+varGroups.widgetName, true);   	            	
				},
				error : function(error) {
    				if(typeof(varGroups) != 'object'){return false;}
    	        	loadingWidget('#'+varGroups.widgetName, true);
    	        	createMsgBox('error', error.responseText, '#'+varGroups.widgetName+' .panel-body');		
				}
			});
			return false;
	    }
	});
});



