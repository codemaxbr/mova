var varUnitMonitor = {};
	varUnitMonitor.widgetName 	= 'widgetsunitmonitor';
	varUnitMonitor.widgetObj 	= $('#'+varUnitMonitor.widgetName);
	varUnitMonitor.dataTable 	= [];
	varUnitMonitor.table;
	varUnitMonitor.api;
	varUnitMonitor.selectedRow;
	varUnitMonitor.callMap;

var widgetsUnitMonitor = {
	// Tabela com a lista de contas cadastradas
	buildTable: function (data){
		varUnitMonitor.dataTable = [];
		$(data.features).each(function(i, row) {
            varUnitMonitor.dataTable.push({
        		'id': row.properties.id, 
        		'history_id': row.properties.history_id, 
        		'placa': row.properties.label, 
        		'description': row.properties.label2, 
        		'date': row.properties.data_hora, 
        		'ign': row.properties.ignition, 
        		'full_address': row.properties.full_address, 
        		'city': row.properties.city, 
        		'speed': row.properties.speed, 
        		'POI': row.properties.point, 
        		'cerca': row.properties.area,
        		'type': row.properties.type_unit,
        		'category': row.properties.category,
        		'group_name': row.properties.group_name,
        		'subgroup': row.properties.subgroup_name, 
        		'identity': row.properties.device_id, 
        		'lasttransmition': row.properties.last_transm, 
        		'timewrite': row.properties.time_write, 
        		'last_save': row.properties.last_save,
        		'coords': row.properties.latitude+'/'+row.properties.longitude,  
        		'lat': row.properties.latitude,
        		'lng': row.properties.longitude,  
        		'GPS': row.properties.gps,   
        		'event': row.properties.event,   
        		'direction': row.properties.icondirect, 
        		'voltage': row.properties.voltage, 
        		'battery': row.properties.battery,  
        		'in1': row.properties.in1,  
        		'in2': row.properties.in2,  
        		'in3': row.properties.in3,  
        		'out1': row.properties.out1,  
        		'out2': row.properties.out2,  
        		'out3': row.properties.out3,  
        		'OS': row.properties.os,  
        		'product': row.properties.intra_prod_id,  
        		'device_model': row.properties.device_model,  
        		'driver_name': row.properties.driver_name
        	});
        }); 

        var numRows = 10;
        var panelWidget = varUnitMonitor.widgetObj.closest('.panel');
        if(panelWidget.hasClass('full-Scr')){
            numRows = Math.round(panelWidget.height() / 28) - 5;
        }

		varUnitMonitor.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" data-page-length="'+numRows+'" id="table-list-unitmonitor" width="100%"></table>' );
		varUnitMonitor.table = varUnitMonitor.widgetObj.find('#table-list-unitmonitor').DataTable({
		    "data": varUnitMonitor.dataTable,
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,		    
	        //stateSave: true,
            "ordering": false,
		    "pagingType": "full",
		    "scrollX": true,
	        "dom": 'C<"clear">lfrtip',
		    "fnDrawCallback": function( oSettings ) {
				varUnitMonitor.widgetObj.find('.divTimeWrite.bg-color-on').parent().addClass('bg-color-on');
				varUnitMonitor.widgetObj.find('.divTimeWrite.bg-color-stat').parent().addClass('bg-color-stat');
				varUnitMonitor.widgetObj.find('.divTimeWrite.bg-color-off').parent().addClass('bg-color-off'); 

				widgetsUnitMonitor.adjustTableLayout();
            	window.setTimeout(function() {
					widgetsUnitMonitor.replaceLabelToInput();
					buildRevGeoFromTable(varUnitMonitor.widgetName);
			    }, 500);
		    },
		    "columns": 
	    	[
	            { "title": "ID", "data": "id" },
	            { "title": "<div class='labelHeader'>Placa</div>", "data": "placa" },
	            { "title": "<div class='labelHeader'>Descrição</div>", "data": "description" },
	            { "title": "<div class='labelHeader'>Data</div>", "data": "date" },
	            { "title": "<div class='labelHeader'>ign</div>", "data": "ign", "class": 'text-center' },
	            { "title": "<div class='labelHeader'>Endereço</div>", "data": "full_address" },
	            { "title": "<div class='labelHeader'>Cidade</div>", "data": "city" },
	            { "title": "<div class='labelHeader'>Vel.</div>", "data": "speed" },
	            { "title": "<div class='labelHeader'>POI</div>", "data": "POI" },
	            { "title": "<div class='labelHeader'>Área</div>", "data": "cerca" },
	            { "title": "<div class='labelHeader'>Tipo</div>", "data": "type" },
	            { "title": "<div class='labelHeader'>Categoria</div>", "data": "category" },
	            { "title": "<div class='labelHeader'>Grupo</div>", "data": "group_name" },
	            { "title": "<div class='labelHeader'>Subgrupo</div>", "data": "subgroup" },
	            { "title": "<div class='labelHeader'>Identificador</div>", "data": "identity" },
	            { "title": "<div class='labelHeader'>Última transmissão</div>", "data": "lasttransmition" },
	            { "title": "<div class='labelHeader'>Última Gravação</div>", "data": "timewrite" },
	            { "title": "<div class='labelHeader'>Lat/Long</div>", "data": "coords" },
	            { "title": "<div class='labelHeader'>GPS</div>", "data": "GPS", "class": 'text-center' },
	            { "title": "<div class='labelHeader'>Evento</div>", "data": "event" },
	            { "title": "<div class='labelHeader'>Direção</div>", "data": "direction", "class": 'text-center' },
	            { "title": "<div class='labelHeader'>Alimentação</div>", "data": "voltage" },
	            { "title": "<div class='labelHeader'>Bateria</div>", "data": "battery" },
	            { "title": "<div class='labelHeader'>Entrada 1</div>", "data": "in1", "class": 'text-center' },
	            { "title": "<div class='labelHeader'>Entrada 2</div>", "data": "in2", "class": 'text-center' },
	            { "title": "<div class='labelHeader'>Entrada 3</div>", "data": "in3", "class": 'text-center' },
	            { "title": "<div class='labelHeader'>Saida 1</div>", "data": "out1", "class": 'text-center' },
	            { "title": "<div class='labelHeader'>Saida 2</div>", "data": "out2", "class": 'text-center' },
	            { "title": "<div class='labelHeader'>Saida 3</div>", "data": "out3", "class": 'text-center' },
	            { "title": "<div class='labelHeader'>OS</div>", "data": "OS" },
	            { "title": "<div class='labelHeader'>Produto</div>", "data": "product" },
	            { "title": "<div class='labelHeader'>Modelo</div>", "data": "device_model" },
	            { "title": "<div class='labelHeader'>Motorista</div>", "data": "driver_name" }
          	],
          	"columnDefs": 
          	[
          	 	//ID
	            {
	                "targets": [ 0 ],
	                "visible": false,
	                "searchable": false
	            },
	            //PLACA
	            {
	                "targets": [ 1 ], 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap" long="'+full.lng+'" lat="'+full.lat+'">' + data + '</div>';
                    }
	            },
	            //DESCRIÇÃO
	            {
	                "targets": [ 2 ],
	                "visible": false, 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //DATA
	            {
	                "targets": [ 3 ], 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //IGN
	            {
	                "targets": [ 4 ], 
                    "render": function (data, type, full, meta) {
                    	var color = 'color-off-2';
                    	var title = 'desligada';
                    	if(data){
                    		color = 'color-on';
                    		title = 'ligada';
                    	}
                        return '<div class="nowrap"><i class="fm fm-key_2 fm-rotate-315 '+color+'" style="font-size: 20px;" title="Ignição '+title+'"></i><span style="display: none;">'+data+'</span></div>';
                    }
	            },
	            //ENDEREÇO
	            {
	                "targets": [ 5 ], 
                    "render": function (data, type, full, meta) {
                        //verifica se este endereco ja existe no banco
                        if(data != '') {
                            //se sim ja retorna o endereco
                            return '<div class="nowrap ellipsis" title="' + data + '">' + data + '</div>';
                        } else {
                            //se nao faz o reversegeo para capturar o endereco e salvar no banco
                            return "<div class='mapAddress nowrap ellipsis' hist='"+full.history_id+"' long='"+full.lng+"' lat='"+full.lat+"'>Carregando...</div>" ;
                        }
                    }
	            },
	            //CIDADE
	            {
	                "targets": [ 6 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //VELOCIDADE
	            {
	                "targets": [ 7 ], 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + ' km/h</div>';
                    }
	            },
	            //POI
	            {
	                "targets": [ 8 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //AREA
	            {
	                "targets": [ 9 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //TIPO
	            {
	                "targets": [ 10 ],
	                "visible": false, 	                
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //CATEGORIA
	            {
	                "targets": [ 11 ],	
	                "visible": false,                
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //GRUPO
	            {
	                "targets": [ 12 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //SUBGRUPO
	            {
	                "targets": [ 13 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //IDENTIFICADOR
	            {
	                "targets": [ 14 ], 
	                "visible": false, 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //ULTIMA TRANSMISSÃO
	            {
	                "targets": [ 15 ],
	                "visible": false, 
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //TIME WRITE
	            {
	                "targets": [ 16 ], 
                    "render": function (data, type, full, meta) {
                    	var today = new Date();
						var bdDate = new Date(data);
						var diff = new Date(today - bdDate);
						var days = diff/1000/60/60/24;
						var color;
						var data_days;

						if(days < 2) {
                		  	data_days = 'me2';
							color = 'bg-color-on';   	                			  
						} else if(days >= 2 && days <= 4) {
                		  	data_days = '2a4';
							color = 'bg-color-stat'; 
						} else if(days > 4) {
                		  	data_days = 'ma2';
							color = 'bg-color-off'; 
						}					
                        return '<div class="nowrap '+color+' divTimeWrite">' + full.last_save + '<span style="display: none;">'+data_days+'</span></div>';
                    }
	            },
	            //LAT LNG
	            {
	                "targets": [ 17 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //GPS
	            {
	                "targets": [ 18 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                    	var color = 'color-off-2';
                    	if(data){
                    		color = 'color-on';
                    	}
                        return '<div class="nowrap"><i class="fa fa-circle fa-1x '+color+'" style="margin-left: 4px;" title="GPS"></i><span style="display: none;">'+data+'</span></div>';
                    }
	            },
	            //EVENTO
	            {
	                "targets": [ 19 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //DIREÇÃO
	            {
	                "targets": [ 20 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                    	if(data == '0' || data == 0 || data == null) {
							return '<strong title="Parado" style="color: red; font-size: 14px">P</strong>';
						} else {
							return '<i class="fa fa-location-arrow fa-1x" style="color: #00acac; -ms-transform: rotate('+(parseInt(data)-45)+'deg); -webkit-transform: rotate('+(parseInt(data)-45)+'deg); transform: rotate('+(parseInt(data)-45)+'deg);"></i>';
						}
                    }
	            },
	            //ALIMENTOÇÃO
	            {
	                "targets": [ 21 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //BATERIA
	            {
	                "targets": [ 22 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + ' v.</div>';
                    }
	            },
	            //ENTREDA 1
	            {
	                "targets": [ 23 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                    	var color = 'color-off-2';
                    	var title = 'off';
                    	if(data){
                    		color = 'color-on';
                    		title = 'on';
                    	}
                        return '<div class="nowrap"><i class="fa fa-circle fa-1x '+color+'" style="margin-left: 4px;" title="Entrada '+title+'"></i><span style="display: none;">'+data+'</span></div>';
                    }
	            },
	            //ENTRADA 2
	            {
	                "targets": [ 24 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                    	var color = 'color-off-2';
                    	var title = 'off';
                    	if(data){
                    		color = 'color-on';
                    		title = 'on';
                    	}
                        return '<div class="nowrap"><i class="fa fa-circle fa-1x '+color+'" style="margin-left: 4px;" title="Entrada '+title+'"></i><span style="display: none;">'+data+'</span></div>';
                    }
	            },
	            //ENTREDA 3
	            {
	                "targets": [ 25 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                    	var color = 'color-off-2';
                    	var title = 'off';
                    	if(data){
                    		color = 'color-on';
                    		title = 'on';
                    	}
                        return '<div class="nowrap"><i class="fa fa-circle fa-1x '+color+'" style="margin-left: 4px;" title="Entrada '+title+'"></i><span style="display: none;">'+data+'</span></div>';
                    }
	            },
	            //SAIDA 1
	            {
	                "targets": [ 26 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                    	var color = 'color-off-2';
                    	var title = 'off';
                    	if(data){
                    		color = 'color-on';
                    		title = 'on';
                    	}
                        return '<div class="nowrap"><i class="fa fa-circle fa-1x '+color+'" style="margin-left: 4px;" title="Saida '+title+'"></i><span style="display: none;">'+data+'</span></div>';
                    }
	            },
	            //SAIDA 2
	            {
	                "targets": [ 27 ],
	                "visible": false, 
                    "render": function (data, type, full, meta) {
                    	var color = 'color-off-2';
                    	var title = 'off';
                    	if(data){
                    		color = 'color-on';
                    		title = 'on';
                    	}
                        return '<div class="nowrap"><i class="fa fa-circle fa-1x '+color+'" style="margin-left: 4px;" title="Saida '+title+'"></i><span style="display: none;">'+data+'</span></div>';
                    }
	            },
	            //SAIDA 3
	            {
	                "targets": [ 28 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                    	var color = 'color-off-2';
                    	var title = 'off';
                    	if(data){
                    		color = 'color-on';
                    		title = 'on';
                    	}
                        return '<div class="nowrap"><i class="fa fa-circle fa-1x '+color+'" style="margin-left: 4px;" title="Saida '+title+'"></i><span style="display: none;">'+data+'</span></div>';
                    }
	            },
	            //OS
	            {
	                "targets": [ 29 ],
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //PRODUTO
	            {
	                "targets": [ 30 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //MODELO DO DISPOSITIVO
	            {
	                "targets": [ 31 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            },
	            //NOME DO MOTORISTA
	            {
	                "targets": [ 32 ], 
	                "visible": false,
                    "render": function (data, type, full, meta) {
                        return '<div class="nowrap">' + data + '</div>';
                    }
	            }
	        ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por contas...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    //colReorder: {
		    //    realtime: false,
		    //    "reorderCallback": function () {
	        //        widgetsUnitMonitor.adjustTableLayout();
	        //    }
		    //},
		    colVis: {
	            exclude: [ 0, 1, 3, 4, 5, 7, 16 ], //tirando das listas as colunas que sempre serao fixas
	            "buttonText": "", //alterando nome do menu
	            showNone: "<i class='fa fa-trash-o fa-1x'></i> <label style='margin-left: -6px;'>Limpar</label>",
	            showAll: "<i class='fa fa-check fa-1x'></i> <label style='margin-left: -9px;'>Marcar todas</label>",          
	        },
	        //"order": [[ 1, 'asc' ]],
		    "initComplete": function(settings, json) {
		    	if(data.features == null){ return false; }
		    	if(data.features.length > 0) {
			    	varUnitMonitor.api = this.api();
                    
    	    		if(typeof(MapBox) == 'object'){
    	    			MapBox.highlightMapBox(1, data.label, true);
    	    		}

			    	varUnitMonitor.widgetObj.find('table[id]').on('click', 'tbody tr', function(e) {
	    			    $(this).addClass('highlight').siblings().removeClass('highlight');
	    			    varUnitMonitor.selectedRow = $(this).index();

	    			    var label = $(this).children('td').html();
	    			    var lat = $(label).attr('lat');
	    			    var lng = $(label).attr('long');
	    			    label = $(label).html();

						widgetsUnitMonitor.openMap(lat, lng, label);
	    			});	

			    	varUnitMonitor.widgetObj.find('.listColVisButtons').on('click', function (){
			    		varUnitMonitor.widgetObj.find('button.ColVis_Button.ColVis_MasterButton').trigger('click');
			    	});
		    	}
		    }
		});
		widgetsUnitMonitor.replaceLabelToInput();
	},
    openMap: function (lat, lng, label){
        if(typeof(varMapBox) != 'object'){
            handleLoadWidget('#widgets/mapbox/mapbox.html', 'widgetsmapbox', 8);

            clearTimeout(varUnitMonitor.callMap);
            varUnitMonitor.callMap = window.setTimeout(function() {
                widgetsUnitMonitor.openMap(lat, lng, label);
            }, 2000); 

            return false;
        }
        if(typeof(varMapBox.mapInstance) != 'object'){
            clearTimeout(varUnitMonitor.callMap);
            varUnitMonitor.callMap = window.setTimeout(function() {
                widgetsUnitMonitor.openMap(lat, lng, label);
            }, 2000); 

            return false;
        }

        MapBox.centerUnit(lat, lng, true, label);
    },
	adjustTableLayout: function () {
		window.setTimeout(function() {
			//remove os icones de ordenação do header lixo
			varUnitMonitor.widgetObj.find('.dataTables_scrollBody th').removeClass('sorting');
			varUnitMonitor.widgetObj.find('.dataTables_scrollBody th').removeClass('sorting_desc');	
	    }, 500); 	
	},
	replaceLabelToInput: function () {
		var columnsNotIn = ['Data', 'ign', 'Vel.', 'GPS', 'Direção', 'Entrada 1', 'Entrada 2', 'Entrada 3', 'Saida 1', 'Saida 2', 'Saida 3'];
		var newField = false;
		//Adicionando inputs search aos devidos campos
		varUnitMonitor.widgetObj.find('.dataTables_scrollHeadInner thead th').each( function () {
			var title = varUnitMonitor.widgetObj.find('.dataTables_scrollHeadInner thead th').eq( $(this).index() ).text();
			if( $.inArray($(this).text(), columnsNotIn) < 0 ) {
				var aux = $(this).html();
				if((aux.indexOf("select") < 0) && (aux.indexOf("input") < 0)){
					newField = true;
					if($(this).text() == 'Última Gravação'){
						$(this).html( "<select style='width: 100%;'><option value=''>Todos</option><option value='me2' class='bg-color-on'>- 2 dias</option><option value='2a4' class='bg-color-stat'>2 a 4 dias</option><option value='ma2' class='bg-color-off'>+ 4 dias</option></select>" );
					} else {
						$(this).html( '<input type="text" placeholder="'+title+'" style="width: 100%; min-width: 70px;"/>' );
					}
				}
			}
		});
		if(newField){
			widgetsUnitMonitor.createSearchEvent();		
			window.setTimeout(function() {
				//redesenha datatable para alinhar colunas
				if(varUnitMonitor.api == null){ return false; }
				varUnitMonitor.api.draw(false);
		    }, 500);			
		}
	},
	createSearchEvent: function () {
		if(varUnitMonitor.api == null){ return false; }
		varUnitMonitor.api.columns().eq(0).each(function(id) {	
			var that = this;
			$('input', varUnitMonitor.api.column(id).header()).on('click mousedown', function(e) {
				e.stopPropagation();
			});

			$('input', varUnitMonitor.api.column(id).header()).on('keyup change', function() {
				if ( that.search() !== this.value ) {
	                that
	                    .search( this.value )
	                    .draw();
	            }
			});
			
			$('select', varUnitMonitor.api.column(id).header()).on('click mousedown', function(e) {
				e.stopPropagation();
			});

			$('select', varUnitMonitor.api.column(id).header()).on('change', function() {
				if ( that.search() !== this.value ) {
	                that
	                    .search( this.value )
	                    .draw();
	            }
			});
		});
	},
    reloadUnitMonitor: function (data){
        varUnitMonitor.dataTable = [];
		$(data.features).each(function(i, row) {
            varUnitMonitor.dataTable.push({
        		'id': row.properties.id, 
        		'history_id': row.properties.history_id, 
        		'placa': row.properties.label, 
        		'description': row.properties.label2, 
        		'date': row.properties.data_hora, 
        		'ign': row.properties.ignition, 
        		'full_address': row.properties.full_address, 
        		'city': row.properties.city, 
        		'speed': row.properties.speed, 
        		'POI': row.properties.point, 
        		'cerca': row.properties.area,
        		'type': row.properties.type_unit,
        		'category': row.properties.category,
        		'group_name': row.properties.group_name,
        		'subgroup': row.properties.subgroup_name, 
        		'identity': row.properties.device_id, 
        		'lasttransmition': row.properties.last_transm, 
        		'timewrite': row.properties.time_write, 
        		'last_save': row.properties.last_save,
        		'coords': row.properties.latitude+'/'+row.properties.longitude,  
        		'lat': row.properties.latitude,
        		'lng': row.properties.longitude,  
        		'GPS': row.properties.gps,   
        		'event': row.properties.event,   
        		'direction': row.properties.icondirect, 
        		'voltage': row.properties.voltage, 
        		'battery': row.properties.battery,  
        		'in1': row.properties.in1,  
        		'in2': row.properties.in2,  
        		'in3': row.properties.in3,  
        		'out1': row.properties.out1,  
        		'out2': row.properties.out2,  
        		'out3': row.properties.out3,  
        		'OS': row.properties.os,  
        		'product': row.properties.intra_prod_id,  
        		'device_model': row.properties.device_model,  
        		'driver_name': row.properties.driver_name
        	});
        });
		//removendo rows
		if(typeof(varUnitMonitor.table) == 'object'){
        	varUnitMonitor.table.clear();
        }
		$(data.features).each(function(i, row) {
            varUnitMonitor.table.row.add({
        		'id': row.properties.id, 
        		'history_id': row.properties.history_id, 
        		'placa': row.properties.label, 
        		'description': row.properties.label2, 
        		'date': row.properties.data_hora, 
        		'ign': row.properties.ignition, 
        		'full_address': row.properties.full_address, 
        		'city': row.properties.city, 
        		'speed': row.properties.speed, 
        		'POI': row.properties.point, 
        		'cerca': row.properties.area,
        		'type': row.properties.type_unit,
        		'category': row.properties.category,
        		'group_name': row.properties.group_name,
        		'subgroup': row.properties.subgroup_name, 
        		'identity': row.properties.device_id, 
        		'lasttransmition': row.properties.last_transm, 
        		'timewrite': row.properties.time_write, 
        		'last_save': row.properties.last_save,
        		'coords': row.properties.latitude+'/'+row.properties.longitude,  
        		'lat': row.properties.latitude,
        		'lng': row.properties.longitude,  
        		'GPS': row.properties.gps,   
        		'event': row.properties.event,   
        		'direction': row.properties.icondirect, 
        		'voltage': row.properties.voltage, 
        		'battery': row.properties.battery,  
        		'in1': row.properties.in1,  
        		'in2': row.properties.in2,  
        		'in3': row.properties.in3,  
        		'out1': row.properties.out1,  
        		'out2': row.properties.out2,  
        		'out3': row.properties.out3,  
        		'OS': row.properties.os,  
        		'product': row.properties.intra_prod_id,  
        		'device_model': row.properties.device_model,  
        		'driver_name': row.properties.driver_name
        	});
        });  
        varUnitMonitor.table.draw(false);
        //reinicia o geocode caso haja algum registro para ser carregado
        //buildRevGeoFromTable(varUnitMonitor.widgetName);
    },
    print: function (type){

        switch (type) { 
            case 'CSV': 
                var content = 'Placa;Descrição;Data;ign;Endereço;Cidade;Vel.;POI;Área;Tipo;Categoria;Grupo;Subgrupo;Identificador;Última transmissão;Última Gravação;Lat/Long;GPS;Evento;Direção';
	            content += ';Alimentação;Bateria;Entrada 1;Entrada 2;Entrada 3;Saida 1;Saida 2;Saida 3;OS;Produto;Modelo;Motorista\r\n';
                
                $(varUnitMonitor.dataTable).each(function(i, row) {
                    content += row.placa+';'+row.description+';'+row.date+';'+row.ign+';'+row.full_address+';'+row.city+';'+row.speed+';'+row.POI+';'+row.cerca+';'+row.type+';'+row.category+';';
                    content += row.group_name+';'+row.subgroup+';'+row.identity+';'+row.lasttransmition+';'+row.timewrite+';'+row.coords+';'+row.GPS+';'+row.event+';';
                    content += row.direction+';'+row.voltage+';'+row.battery+';'+row.in1+';'+row.in2+';'+row.in3+';'+row.out1+';'+row.out2+';'+row.out3+';'+row.OS+';'+row.product+';';
                    content += row.device_model+';'+row.driver_name+'\r\n';
                });            

                var a         = document.createElement('a');
                a.href        = 'data:attachment/csv,' +  escape(content);
                a.target      = '_blank';
                a.download    = 'Monitor_de_Unidades.csv';

                document.body.appendChild(a);
                a.click();

                break;     
        }
    }
}

function load_widgetsunitmonitor_Page(){
    varUnitMonitor.widgetObj.find('.btn-danger').removeAttr("disabled");
	window.setTimeout(function() {
		widgetsUnitMonitor.buildTable(varGlobal.unitData);
		//liberando menu
		varUnitMonitor.widgetObj.find('.btn').removeAttr("disabled");
		loadingWidget('#'+varUnitMonitor.widgetName, true); 

		widgetsUnitMonitor.createSearchEvent();
			
	}, 2000);
}

$(document).ready( function () {
	load_widgetsunitmonitor_Page();
});

