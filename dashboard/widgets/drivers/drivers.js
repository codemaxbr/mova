var varDrivers = {};
	varDrivers.widgetName 	= 'widgetsdrivers';
	varDrivers.varName 		= 'varDrivers';
	varDrivers.widgetObj 	= $('#'+varDrivers.widgetName);
	varDrivers.action 		= 'add';
	varDrivers.accessLevel 	= 1;	
	varDrivers.method;	
	varDrivers.valeClient 	= false;
	varDrivers.valeClientIDs = [274, 411, 412];
	varDrivers.driverManager = 1;

	//VARIAVEIS PARA O COMPONENTE DE ASSOCIAÇÃO DE UNIDADES
	//tabelas do componente de associação de unidades
	varDrivers.tableUnit;
	varDrivers.tableGroup;
	varDrivers.tableUnitAssoc;
	varDrivers.tableUnitAssocCad;
	//listas do componente de associação de unidades
	varDrivers.Groups = [];
	varDrivers.Units = [];
	varDrivers.UnitsAssocOri = [];
	varDrivers.UnitsAssoc = [];
	//lista do componente de associação de unidades
	varDrivers.UnitsSelected = [];
	//FIM DA VARIAVEIS DE ASOCIÇÃO


var widgetsdrivers = {
	// SETANDO CAMPOS NOS INPUTS DO FORM EDITAR
	loadEditForm: function (data) {
		var driver = data.driver;
		var driverUnit = JSON.parse(data.driverUnit);

    	if(typeof(varDrivers) != 'object'){return false;}

    	if(varDrivers.accessLevel == 1){
			var divContent = varDrivers.widgetObj.find('.visualize-box');

			divContent.find('.lbl_name').html(driver.name);
			divContent.find('.lbl_email').html(driver.email);
			divContent.find('.lbl_phone').html(driver.phone);
			divContent.find('.lbl_matricula').html(driver.login);
			divContent.find('.lbl_cnh').html(driver.cnh);
			divContent.find('.lbl_cnh_category').html(driver.cnh_category);
			divContent.find('.lbl_cnh_validate').html(driver.cnh_validate_format);
			if(driver.auth == 1){
				divContent.find('.lbl_auth').html('Matrícula');	
			}else{
				divContent.find('.lbl_auth').html('Login/Senha');				
			}
			divContent.find('.lbl_group').html(driver.group_name);

			//DADOS MOTORISTA VALE
			divContent.find('.lbl_passport').html(driver.passport);
			divContent.find('.lbl_function').html(driver.function_name);	
			divContent.find('.lbl_area').html(driver.area);
			divContent.find('.lbl_empresa').html(driver.empresa);
			divContent.find('.lbl_gestor').html(driver.gestor);
			divContent.find('.lbl_tel_gestor').html(driver.gestor_tel);
			divContent.find('.lbl_local').html(driver.local);
			divContent.find('.lbl_rac_validade').html(driver.rac_validate_format);
			divContent.find('.lbl_aos_validade').html(driver.aso_validate_format);
			//FIM DADOS MOTORISTA VALE
			if(varDrivers.driverManager != 1){
				widgetsdrivers.buildTableUnitAssoc(driverUnit.data, varDrivers.widgetObj.find('#create-table-drivers-unit-view'));
			}

			openVisualize(varDrivers.widgetName);
			return false;
    	}

		var form = varDrivers.widgetObj.find('form');
		//limpa formulário
		widgetsdrivers.reset();
		// muda o form para edição
		varDrivers.action = 'edit';

		form.find('#id').val(driver.id);
		form.find('#name').val(driver.name);
		form.find('#email').val(driver.email);
		form.find('#phone').val(driver.phone);
		form.find('#login').val(driver.login);
		form.find('#password').val(driver.password);
		form.find('#cnh').val(driver.cnh);
		form.find('#cnh_category').val(driver.cnh_category);
		form.find('#cnh_validate').val(driver.cnh_validate_format);

		//DADOS CLIENTE VALE
		form.find('#passport').val(driver.passport);
		form.find('#function option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == driver.driver_function_id) {
				$(this).prop('selected', true);
			}
		});
		form.find('#area').val(driver.area);
		form.find('#empresa').val(driver.empresa);
		form.find('#gestor').val(driver.gestor);
		form.find('#gestor_tel').val(driver.gestor_tel);
		form.find('#local').val(driver.local);
		form.find('#rac_validate').val(driver.rac_validate_format);
		form.find('#aso_validate').val(driver.aso_validate_format);
		form.find('#group').val(driver.group_id);	
		// FIM DADOS CLIENTE VALE

		form.find('#description').val(driver.description);
		form.find('#group option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == driver.group_id) {
				$(this).prop('selected', true);
			}
		});
		form.find('#auth option').each(function(i, obj) {
			var value = $(this).attr('value');
			if (value == driver.auth) {
				$(this).prop('selected', true);
			}
		});
		if(driver.auth == 1){
			form.find('.reset_password').css('display', 'none');
			form.find('#reset_password').removeAttr('checked');
		}else{
			form.find('.reset_password').css('display', 'block');
		}

		if((driverUnit.data.length > 0) && (varDrivers.driverManager == 2)){
			varDrivers.widgetObj.find('#assoc-driver-manager').attr('checked', 'checked');
    		varDrivers.widgetObj.find('.assoc-validation').css('display', 'block');
		}

		if(varDrivers.driverManager != 1){
			varDrivers.UnitsAssocOri = [];
			$.each(driverUnit.data, function(index, val) {
				varDrivers.UnitsAssocOri.push({
					id: val.id,
					name: val.name,
					description: val.description,
					group_id: val.group_id,
					group_name: val.group_name,
					memory_position: val.memory_position
				});
			});
			widgetsdrivers.buildTableUnitAssoc(driverUnit.data, varDrivers.widgetObj.find('#create-table-drivers-unit'));
		}

		openForm(varDrivers.widgetName, varDrivers.action);
	},
	buildGroup: function (data){
    	if(typeof(varDrivers) != 'object'){return false;}
		varDrivers.widgetObj.find('#group').html('<option value="">Selecione um grupo</option>');
		var template = 	'{{#groupinfo}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/groupinfo}}';
		var select = Mustache.to_html(template, data);
		varDrivers.widgetObj.find('#group').append(select);
	},
	buildFunction: function (data){
    	if(typeof(varDrivers) != 'object'){return false;}
		varDrivers.widgetObj.find('#function').html('<option value="">Selecione uma função</option>');
		var template = 	'{{#functioninfo}}'+
						'	<option value="{{id}}">{{name}}</option>'+
						'{{/functioninfo}}';
		var select = Mustache.to_html(template, data);
		varDrivers.widgetObj.find('#function').append(select);
	},
	reset: function (){
    	if(typeof(varDrivers) != 'object'){return false;}
		var form = varDrivers.widgetObj.find('form');
		varDrivers.action = 'add'; 
		
		form.find('#assoc-driver-manager').removeAttr('checked');
		form.find('input').removeClass('parsley-success');
		form.find('select').removeClass('parsley-success');

		form[0].reset();
	},
	delete: function () {
    	if(typeof(varDrivers) != 'object'){return false;}
	    var data = { id: varDrivers.widgetObj.find('form').find("#id").val(), status: 3 };    
	    data = JSON.stringify(data);     
	    
	    $.ajax({
	    	async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'drivers/delete',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
				loadingWidget('#'+varDrivers.widgetName);
			},
			success : function(data) {
    			if(typeof(varDrivers) != 'object'){return false;}
				// cria mensagem de sucesso no widget
				createMsgBox('success',data.responseText, '#'+varDrivers.widgetName+' .panel-body');
				// atualiza tabela que lista as contas
				varDrivers.widgetObj.find('#table-list-drivers').DataTable().ajax.reload(null,false);
				// Excluindo botão loading e voltando buttons ('salvar' e 'cancelar') após sucesso
				closeForm(varDrivers.widgetName);
				widgetsdrivers.reset();
				loadingWidget('#'+varDrivers.widgetName, true);
			},
			error : function(error) {
	        	loadingWidget('#'+varDrivers.widgetName, true);
				//cria mensagem de erro no widget
				createMsgBox('error',error.responseText, '#'+varDrivers.widgetName+' .panel-body');		
			}
		});
		return false;
	},
	getById : function (data) {
	    $.ajax({
	    	async: true,
	    	type: 'POST',
	        url: 'widgets/routing.php',
	        data: { 
	        	url: 'drivers/getById/', 
	        	data: JSON.parse(data) 
	        },
	        dataType: 'json',
	        success: function(data) {
		        widgetsdrivers.loadEditForm(data);
	        	loadingWidget('#'+varDrivers.widgetName, true);
	        },
	        error: function(error) {
	        	createMsgBox('error', error.responseText, '#'+varDrivers.widgetName+' .panel-body');
	        	loadingWidget('#'+varDrivers.widgetName, true);
	        }
	    });
	},
	buildTable: function (){
    	if(typeof(varDrivers) != 'object'){return false;}
		varDrivers.widgetObj.find('#create-table').html( '<table class="table table-striped table-bordered" id="table-list-drivers" width="100%"></table>' );
		//Pega os dados da tabela
		var table_drivers = varDrivers.widgetObj.find('#table-list-drivers').dataTable({
		    ajax: {
		        "url": 'widgets/routing.php',
		        "type": "POST",
		        "data": { url: 'drivers/list/'},
		  		error: function (error) {  
    				if(typeof(varDrivers) != 'object'){return false;}
		        	loadingWidget('#'+varDrivers.widgetName, true);
					createMsgBox('error', error.responseText, '#'+varDrivers.widgetName+' .panel-body');
		  		}
	      	},
		    paginate: true,
		    bLengthChange: false,
		    bFilter: true,
		    bInfo: true,
		    "pagingType": "full",
		    "fnDrawCallback": function( oSettings ) {
	    		varDrivers.widgetObj.find('.60_p').css('width', '60%');
	    		varDrivers.widgetObj.find('.40_p').css('width', '40%');
		    },
		    "columns": 
		    	[
		            { "title": "ID", "data": "id", 'class': 'hidden' },
		            { "title": "Nome", "data": "name", "className": "goToEdit 60_p"  },
		            { "title": "Grupo", "data": "group_name", "className": "goToEdit 40_p" }
	          	],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por Motoristas...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
	    		varDrivers.widgetObj.find('.60_p').css('width', '60%');
	    		varDrivers.widgetObj.find('.40_p').css('width', '40%');
    			if(typeof(varDrivers) != 'object'){return false;}
		    	//CLICANDO NA CONTA PARA EDITÃ�-LA
		    	if(json.data.length > 0){

		    		varDrivers.widgetObj.find('#table-list-drivers tbody tr').die("click").live("click", function(){
			    		//Pegando id da conta
	    				var data = $(this).children('td').html();
			    	    loadingWidget('#'+varDrivers.widgetName);	
			    	    widgetsdrivers.getById(data);
		
	    		    	return false;
	    		    });
				}  	
		  	}
		});
	},
	buildTableUnitAssoc : function (data, tableObj){
		if(typeof(varDrivers) != 'object'){return false;}
		tableObj.html( '<table class="table table-striped table-bordered" id="table-list-drivers-unit" width="100%"></table>' );
		//Pega os dados da tabela
		varDrivers.tableUnitAssocCad = varDrivers.widgetObj.find('#table-list-drivers-unit').DataTable({
		    "data": data,
		    paginate: true,
		    bLengthChange: false,
		    bFilter: false,
		    bInfo: true,
		    "pagingType": "full",
		    "fnDrawCallback": function( oSettings ) {
    			if(typeof(varDrivers) != 'object'){return false;}
                varDrivers.widgetObj.find('.30_p').css('width', '30%');
                varDrivers.widgetObj.find('.60_p').css('width', '60%');
                varDrivers.widgetObj.find('.10_p').css('width', '10%');
		    },
		    "columns": 
		    	[
		            { "title": "#", "data": "id", "class": 'hidden' },
		            { "title": "Unidade", "data": "name", "orderable": false, "className": "30_p" },
		            { "title": "Grupo", "data": "group_name", "orderable": false, "className": "60_p" },
		            { "title": "", "data": "status", "orderable": false, "className": "10_p text-center"}
	          	],
          	"columnDefs": 
                [
                    {
                        "targets": [1],
		            	"render": function ( data, type, full, meta ) {
		            		return '<div class="nowrap ellipsis" title="'+data+'">'+data+'</div>';
						}
                    },
                    {
                        "targets": [2],
		            	"render": function ( data, type, full, meta ) {
		            		if((data == null) || (data == '')){
		            			return '';
		            		}else{
		            			return '<div class="nowrap ellipsis" title="'+data+'">'+data+'</div>';
		            		}
						}
                    },
                    {
                        "targets": [3],
		            	"render": function ( data, type, full, meta ) {
		            		if(data == 1){
								return '<div title="Unidade pendente de envio"><i class="fa fa-warning color-stat" style="font-size: 14px;"></i></div>';
		            		}else if(data == 2){
								return '<div title="Unidade enviado"><i class="fa fa-check-circle color-on" style="font-size: 15px;"></div>';
		            		}else{
		            			return '<div title="Não foi possivel enviar a unidade"><i class="fa fa-minus-circle color-off" style="font-size: 15px;"></div>';
		            		}
						}
                    }
                ],
		    "language" : {
		    	"emptyTable":     "Ainda não há registros",
				"info" : "Total de _TOTAL_ registros",
		        "infoEmpty":      "Mostrando 0 de 0 de um total de 0 registros",
		        "infoFiltered":   "(filtrado de um total de  _MAX_ registros)",
		        "infoPostFix":    "",
		        "thousands":      ".",
		        "lengthMenu":     "Mostre _MENU_ registros",
		        "loadingRecords": "Carregando...",        
		        "processing":     "Processando...",
		        "search":         "",
		    	"searchPlaceholder": "Procure por Unidade...",
		        "zeroRecords":    "Nenhum registro encontrado com esta pesquisa",
		        "paginate": {
		            "first":      "<<",
		            "last":       ">>",
		            "next":       ">",
		            "previous":   "<"
		        }
		    },
		    "initComplete": function(settings, json) {
    			if(typeof(varDrivers) != 'object'){return false;}
                varDrivers.widgetObj.find('.30_p').css('width', '30%');
                varDrivers.widgetObj.find('.60_p').css('width', '60%');
                varDrivers.widgetObj.find('.10_p').css('width', '10%');
		  	}
		});
		return false; 	
	},
	sendUnit : function () {	
		var data = [];
		var id = varDrivers.widgetObj.find("#id").val();

		$.each(varDrivers.UnitsAssocOri, function(index, val) {
			data.push({
				type: 'units',
				id: id,
				unit_id: val.id,
				driver_id: id,
				memory_position: val.memory_position
			});
		});
		
		data = JSON.stringify(data);

		$.ajax({
			async: true,
			type : 'POST',
			url : 'widgets/routing.php',
			data : {
				url : 'drivers/resendDriverUnit/',
				data : JSON.parse(data)
			},
			dataType : 'json',
			beforeSend : function() {
	        	loadingWidget('#'+varDrivers.widgetName);
			},
			success : function(data) {
	        	loadingWidget('#'+varDrivers.widgetName, true);       	        
    	        //Criando mensagem de retorno
    	        createMsgBox('success', data.msg, '#'+varDrivers.widgetName+' .panel-body');

			    var info = {
			    	widgetName: varDrivers.widgetName,
			    	widgetVar: varDrivers.varName
			    }
			    handleAssocSelect.reloadTableUnitAssoc(info, JSON.parse(data.data));
			},
			error : function(error) {
	        	loadingWidget('#'+varDrivers.widgetName, true);
	        	//Criando mensagem de retorno
	        	createMsgBox('error', error.responseText, '#'+varDrivers.widgetName+' .panel-body');	

			}
		});
		return false;
	}
};

function load_widgetsdrivers_Page(){
	if(typeof(varDrivers) != 'object'){return false;}
	varDrivers.accessLevel = handleAccessLevel(varDrivers.widgetName);
	if(varDrivers.accessLevel == 1){
		varDrivers.widgetObj.find('.openForm').addClass('hidden');
	}

	$.ajax({
    	type: 'POST',
        url: 'widgets/routing.php',
        data: { url: 'drivers/index/'},
        dataType: 'json',
        beforeSend: function () {
    		if(typeof(varDrivers) != 'object'){return false;}
        	closeForm(varDrivers.widgetName);
			widgetsdrivers.reset();	
			varDrivers.widgetObj.find('.btn').attr('disabled', 'disabled');
            varDrivers.widgetObj.find('.btn-danger').removeAttr("disabled");
        },
        success: function(data) {
        	if(typeof(varDrivers) != 'object'){return false;}
        	widgetsdrivers.buildTable();
        	widgetsdrivers.buildGroup(JSON.parse(data.groupinfo));   
        	widgetsdrivers.buildFunction(JSON.parse(data.functioninfo));      
        	//SET ID account user
        	varDrivers.widgetObj.find('form').find('#account_id').val(data.userinfo.user.account_id);
        	if(varDrivers.valeClientIDs.indexOf(data.userinfo.user.account_id) >=0 ){
        		varDrivers.valeClient = true;
        		varDrivers.widgetObj.find('.valeFields').removeClass('hidden');
        		varDrivers.widgetObj.find('.valeFields').find('input').attr('required', 'required');
        		varDrivers.widgetObj.find('.valeFields').find('select').attr('required', 'required');
        	}

        	varDrivers.driverManager = data.userinfo.user.driver_manager;
        	if(varDrivers.driverManager == 2){
        		varDrivers.widgetObj.find('.assoc-validation').css('display', 'none');
        		varDrivers.widgetObj.find('.assoc-validation-check').css('display', 'block');
        	}else if(varDrivers.driverManager == 3){
        		varDrivers.widgetObj.find('.assoc-validation').css('display', 'block');
        		varDrivers.widgetObj.find('.assoc-validation-check').css('display', 'none');
        	}else{
        		varDrivers.widgetObj.find('.assoc-validation').css('display', 'none');
        		varDrivers.widgetObj.find('.assoc-validation-check').css('display', 'none');
        	}
        	//liberando menu
        	varDrivers.widgetObj.find('.btn').removeAttr("disabled");
        	loadingWidget('#'+varDrivers.widgetName, true); 
        },
        error: function(error) {
    		if(typeof(varDrivers) != 'object'){return false;}
        	loadingWidget('#'+varDrivers.widgetName, true);
			createMsgBox('error', error.responseText, '#'+varDrivers.widgetName+' .panel-body');
        },
        complete: function() {
    		if(typeof(varDrivers) != 'object'){return false;}
            varDrivers.widgetObj.find('.btn-danger').removeAttr("disabled");
        }
    });
}

$(document).ready(function() {
	load_widgetsdrivers_Page();

    if(typeof(varDrivers) != 'object'){return false;}

    //INICIALIZANDO COMPONENTE DE ASSOCIAÇÃO DE UNIDADES
    //inicializando componente de associação de unidades
    var info = {
    	widgetName: varDrivers.widgetName,
    	btnSaveName: 'Associar e Enviar',
    	widgetVar: 'varDrivers',
    	title: 'Associação de unidades',
    	titletab: 'Unidades',
    	type: 'units',
    	assocUrl: 'drivers/assocDriverUnit/'
    }
    handleAssocSelect.init(info);
    //FIM DO COMPONETE DE ASSOCIAÇÃO DE UNIDADES

	//mask nos campos de telefone
	varDrivers.widgetObj.find('form').find('.tel').mask('(99) 9999-9999?9');
	//mask nos campos de login
	varDrivers.widgetObj.find('form').find('#login').mask('999?9999999');	

    varDrivers.widgetObj.find('.pluginDate').datepicker({ 
        autoclose: true, 
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
    });

	//escondendo campo senha caso seja necessario
	varDrivers.widgetObj.find('form').find('#auth').on('change', function() {
		var val = $(this).val();
		if(varDrivers.action == 'edit'){
			if(val == "1"){
				varDrivers.widgetObj.find('.reset_password').css('display', 'none');
				varDrivers.widgetObj.find('#reset_password').removeAttr('checked');
			}else{
				varDrivers.widgetObj.find('.reset_password').css('display', 'block');
			}			
		}
	});

	varDrivers.widgetObj.find('.openForm').die("click").live("click", function(){
		widgetsdrivers.reset();
		varDrivers.action = 'add';
		openForm(varDrivers.widgetName, 'add');
	});
	//CHAMADA PARA EXCLUSÃO DE drivers 
	varDrivers.widgetObj.find('.confirm_delete').die("click").live("click", function(){
		openConfirm(varDrivers.widgetName, "<i class='fa fa-2x fa-trash-o' style='vertical-align: middle;'></i> Excluir Motorista", "Deseja remover o Motorista selecionado?", "Sim", "Não");
	});
	varDrivers.widgetObj.find('.confirm_yes').die("click").live("click", function(){
		widgetsdrivers.delete();
		closeConfirm(varDrivers.widgetName);
	});
	varDrivers.widgetObj.find('.confirm_no').die("click").live("click", function(){
		closeConfirm(varDrivers.widgetName);
	});
	//CHAMADA PARA REENVIAR MOTORISTAS PARA UNIDADE
	varDrivers.widgetObj.find('.resend').die("click").live("click", function(){
		openConfirm(varDrivers.widgetName, "<i class='fa fa-2x fa-rss' style='vertical-align: middle;'></i> Reenviar unidades", "Deseja reenviar todas as unidades para esse motorista?", "Sim", "Não", "resend_yes", "resend_no");
	});
	varDrivers.widgetObj.find('.resend_yes').die("click").live("click", function(){
		widgetsdrivers.sendUnit();
		closeConfirm(varDrivers.widgetName);
	});
	varDrivers.widgetObj.find('.resend_no').die("click").live("click", function(){
		closeConfirm(varDrivers.widgetName);
	});

	varDrivers.widgetObj.find('form').find('.cancel').die('click').live('click', function() {
		closeForm(varDrivers.widgetName);
		widgetsdrivers.reset();		
	});	

	varDrivers.widgetObj.find('.visualize-box').find('.cancel').die('click').live('click', function() {
		closeVisualize(varDrivers.widgetName);
	});	

	varDrivers.widgetObj.find('#assoc-driver-manager').die("click").live("click", function(){
		if($(this).attr('checked') == 'checked'){
			$(this).attr('checked', 'checked');
    		varDrivers.widgetObj.find('.assoc-validation').css('display', 'block');
    	}else{
			$(this).removeAttr('checked');
    		varDrivers.widgetObj.find('.assoc-validation').css('display', 'none');
    	}
	});

	varDrivers.widgetObj.find('form').submit(function(e) { 
		e.preventDefault();
	    if ( $(this).parsley().isValid() ) {
			var form = $(this); 

			//DADOS EXPLUSIVOS VALE
			var dataVale = null;
			if(varDrivers.valeClient){
				dataVale = {
    				passport: form.find('#passport').val(),
	    			function: form.find('#function').val(),
	    			area: form.find('#area').val(),
	    			empresa: form.find('#empresa').val(),
	    			gestor: form.find('#gestor').val(),
	    			gestor_tel: form.find('#gestor_tel').val(),
	    			local: form.find('#local').val(),
	    			rac_validate: form.find('#rac_validate').val(),
	    			aso_validate: form.find('#aso_validate').val()
				};				
			}

		    var data = 
				{
	    			id: form.find('#id').val(),
	    			account_id: form.find('#account_id').val(),
	    			name: form.find('#name').val(),
	    			email: form.find('#email').val(),
	    			phone: form.find('#phone').val(),
	    			auth: form.find('#auth').val(),
	    			login: form.find('#login').val(),
	    			reset_password: form.find('#reset_password').attr('checked') == 'checked' ? true : false,
	    			cnh: form.find('#cnh').val(),
	    			cnh_category: form.find('#cnh_category').val(),
	    			cnh_validate: form.find('#cnh_validate').val(),
	    			group_id: form.find('#group').val(),
	    			dataVale: dataVale
				};
				
		    data = JSON.stringify(data);
		    
	        $.ajax({
	        	async: true,
				type : 'POST',
				url : 'widgets/routing.php',
				data : {
					url : 'drivers/'+varDrivers.action+'/',
					data : JSON.parse(data)
				},
				dataType : 'json',
				beforeSend : function() {
    				if(typeof(varDrivers) != 'object'){return false;}
    	        	loadingWidget('#'+varDrivers.widgetName);
				},
				success : function(data) {
					var id = data.id;
					var msg = data.msg;

        			if(typeof(varDrivers) != 'object'){return false;}    	        
	    	        //Criando mensagem de retorno
	    	        createMsgBox('success', data, '#'+varDrivers.widgetName+' .panel-body');
	    	        // atualiza tabela que lista as contas
	    	        varDrivers.widgetObj.find('#table-list-drivers').DataTable().ajax.reload(null, false);	

	    	        if(varDrivers.action == 'edit'){ 
		    	        //reseta o form	    
						closeForm(varDrivers.widgetName);
		    	        widgetsdrivers.reset();
	        			loadingWidget('#'+varDrivers.widgetName, true);   
	    	        }else{
	    	        	if(varDrivers.driverManager == 3){
		    	        	widgetsdrivers.getById(id);
		    	        	varDrivers.widgetObj.find('.edit').removeClass('hidden')
		    	        	varDrivers.widgetObj.find('.add').addClass('hidden');
	    	        	}else{	 
							closeForm(varDrivers.widgetName);
			    	        widgetsdrivers.reset();
		        			loadingWidget('#'+varDrivers.widgetName, true);     	        		
	    	        	}
	    	        }    	            	
				},
				error : function(error) {
    				if(typeof(varDrivers) != 'object'){return false;}
    	        	loadingWidget('#'+varDrivers.widgetName, true);
    	        	createMsgBox('error', error.responseText, '#'+varDrivers.widgetName+' .panel-body');		
				}
			});
			return false;
	    }
	});
});



