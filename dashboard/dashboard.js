$.ajax({
	type: 'POST',
    url: 'widgets/routing.php',
    data: { url: 'widget/dashboard/'},
    dataType: 'json',
    success: function(data) {
    	//Carrega dados do usuário
    	var userData = data.userinfo.user;
    	var userMenu = '<div class="image">'+
		'	<img src="assets/img/'+userData.account_id+'/logo_small.png" alt="" />'+
		'</div>'+
		userData.name+' <b class="caret"></b>'+
		'<small>'+userData.name_account+'</small>';
		$('#sidebar').find('.nav .nav-profile .info').html(userMenu);

		//Carrega menu do usuário
    	var category_name = '';
		var leftMenu = '<li class="has-sub active">';
		leftMenu += '	<a href="javascript:;">';
		leftMenu += '	    <b class="caret pull-right"></b>';
		leftMenu += '	    <i class="fa fa-laptop"></i>';
		leftMenu += '	    <span>Dashboard</span>';
		leftMenu += '   </a>';
		leftMenu += '	<ul class="sub-menu">';
		leftMenu += '	    <li class="active"><a href="#grid/index.html" data-toggle="ajax">Principal</a></li>';
		leftMenu += '	</ul>';
		leftMenu += '</li>';
		leftMenu += '<li class="has-sub">';
		leftMenu += '	<a href="javascript:;">';
		leftMenu += '	    <b class="caret pull-right"></b>';
		leftMenu += '	    <i class="fa fa-align-left"></i> ';
		leftMenu += '	    <span>Widgets</span>';
		leftMenu += '	</a>';
		leftMenu += '	<ul class="sub-menu">';

		$($(data.infos.widgetsInfo)).each(function(i, row){
            if(category_name != row.category_name){            	
	            if(category_name != ''){
	            	leftMenu += '			</ul>';
	            	leftMenu += '		</li>';
	            }
            	category_name = row.category_name;
				leftMenu += '		<li class="has-sub">';
				leftMenu += '			<a href="javascript:;">';
				leftMenu += '	            <b class="caret pull-right"></b>';
				leftMenu += '	            '+row.category_name;
				leftMenu += '	        </a>';
				leftMenu += '			<ul class="sub-menu">';	
			}
											
			leftMenu += '				<li><a href="javascript:;" onClick="handleLoadWidget($(this).attr(\'id\'), \'widgets'+row.template+'\', \'small\');" id="#widgets/'+row.template+'/'+row.template+'.html">'+row.name+'</a></li>';			
        });

        leftMenu += '			</ul>';
		leftMenu += '		</li>';
		leftMenu += '	</ul>';
		leftMenu += '</li>';
		leftMenu += '<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>';

		$('#sidebar').find('.nav.left-menu').html(leftMenu);


    },
    error: function(error) {
		//cria mensagem de erro no widget
		createMsgBox('error', error.responseText);		
    }
});
