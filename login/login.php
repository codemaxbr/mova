<?php
/**
 * login
 * PHP Version 5.x
 *
 * @Author Rafael Silva
 * @category Validacao
 * @package login
 * Valida o acesso do usuario atraves do login e senha passados como parametros por post
 */
#print(phpinfo());
try {
	//print phpinfo();
	session_start();
	
	require_once("../autoloader.php");
	require_once('../dashboard-api/class/core/config.php');
	require_once('../dashboard-api/class/core/library.php');
	require_once("../dashboard-api/class/error/error-map.php");

	if(Validate::login($_POST['formUserLogin']) && Validate::password($_POST['formPasswordLogin'])){
		
		// Verifica se o usuario selecionou a opcao "Lembrar"
		$rememberMe = isset($_POST['formLembrar']) ? $_POST['formLembrar'] : 0;

		$connection  = new Connection();

		$connection -> open();
		
		$authenticate = new Authentication($_POST['formUserLogin'],$_POST['formPasswordLogin']);

		$validateAccess = $authenticate -> validateAccess();

		if(is_object($validateAccess)){
				
			$isValid = $authenticate -> createSession($validateAccess,$rememberMe);	
		
			if($isValid === true){
				header("location: ../dashboard/dashboard.html");
				
			}else{
				$_SESSION['errorMessage'] = E_DENIED;
				header("location: ../");
				
			}
								
		}else{
			if(is_string($validateAccess)){
				$_SESSION['errorMessage'] = $validateAccess;
				header("location: ../");	
			}else{
				$_SESSION['errorMessage'] = E_DENIED;
				header("location: ../");				
			}
		}

	}else{
		$_SESSION['errorMessage'] = E_INVALID;
		header("location: ../");
		// Definir mensagem de retorno
	}

} catch (Exception $e) {
	$_SESSION['errorMessage'] = E_UNABLETOLOGIN;
	header("location: ../");
}