<?php 
class stopreportController extends defaultController
{
	public function indexAction($infos=null) {
		date_default_timezone_set("America/Sao_Paulo");
		$widget = new widgetController();
		$group = new clientController();
		$unit = new unitController();
		$return = array();
		$return['permissionType'] = $this -> haveWriteAction($infos);
		$return['infos'] = $widget->getWidgetInfoAction($infos);
		$return['width_slider'] = ($return['infos']->width - 80) / 2; //Fazendo tamanho(largura) do plugin ionslider(range)
		$return['height'] = $return['infos']->height - 60;
		$return['groups'] = $group->listAction();
		$return['subgroups'] = $group->listSubGroupsAction();
		$return['default_width'] = $return['infos']->width;
		$return['unities'] = $unit->listAction();
		$return['date'] = date('d/m/Y');
		$return['hour'] = date('H:i');
		
		return json_encode($return);	
	}
	
	public function listEventsAction() {
		 try {
			$parents = $this->getCategoriesAction();
			$result = array();				
			if(is_array($parents)){
				foreach ($parents as $parent){
					$parent->children = $this->getEventsAction($parent->parent_id);
					if($parent->children !== false) {
						$result[] = $parent;
					}
				}
			
				$tree = array("text" => "Eventos");
				$tree["children"] = $result;
				$json = json_encode($tree);
				//Trocando nome dos campos de arrays para funcionar no plugin jsTree
				$search = array("name");
				$replace = array("text");
				$return = str_replace($search, $replace, $json);
				$return = json_encode($return);
				return json_decode($return);
			} else {
				return E_UNABLETOLISTWIDGETS; //TODO: REGISTRAR ERRO
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	
	public function buildStopReportAction($obj) {
		try {
			if(!isset($_SESSION)) {
				session_start();
			}
			$account_id = $_SESSION['user'] -> getAccount();
			
			$table = 'con_stop';
			$primaryKey = 'tu.id';
			
			$columns = array(
					array( 'db' => 'tu.label as placa', 'dt' => 0, 'field' => 'placa' ),
					array( 'db' => "CASE WHEN (to_char(cs.initial_time, 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje' WHEN (to_char(cs.initial_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' ELSE to_char(cs.initial_time, 'DD/MM/YY') END AS data_ini",  'dt' => 1, 'field' => 'data_ini'),
					array( 'db' => "CASE WHEN (to_char(cs.final_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje' WHEN (to_char(cs.final_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' ELSE to_char(cs.final_time, 'DD/MM/YY') END AS data_final",  'dt' => 2, 'field' => 'data_final'),
					array( 'db' => "cs.total_time as total_time",  'dt' => 3, 'field' => 'total_time'),
					array( 'db' => "cs.initial_history_id||';'||cs.initial_lon||';'||cs.initial_lat as span_address",  'dt' => 4, 'field' => 'span_address'),
					array( 'db' => "cs.initial_poi_name as poi",  'dt' => 5, 'field' => 'poi'),
					array( 'db' => "cs.initial_area_name as cerca",  'dt' => 6, 'field' => 'cerca'),
					array( 'db' => 'cs.initial_address as address',  'dt' => 7, 'field' => 'address'),
					array( 'db' => "to_char(cs.initial_time, 'HH24:MI:SS') as hora_ini",  'dt' => 8, 'field' => 'hora_ini' ),					
					array( 'db' => "to_char(cs.final_time, 'HH24:MI:SS') as hora_final",  'dt' => 9, 'field' => 'hora_final'),
					array( 'db' => "to_char(cs.initial_time, 'DD/MM/YYYY') as data_ini",  'dt' => 10, 'field' => 'data_ini'),
					array( 'db' => "to_char(cs.final_time , 'DD/MM/YYYY') as data_final",  'dt' => 11, 'field' => 'data_final'),
					array( 'db' => "cs.initial_lat as latitude",  'dt' => 12, 'field' => 'latitude'),
					array( 'db' => "cs.initial_lon as longitude",  'dt' => 13, 'field' => 'longitude')
			);			
			
			$join = 'FROM con_stop as cs INNER JOIN tracked_unit tu on tu.id = cs.unit_id INNER JOIN "group" grp on grp.id = cs.group_id INNER JOIN subgroup sgrp ON sgrp.id = cs.subgroup_id INNER JOIN unit_category ucat ON tu.unit_category_id = ucat.id';
			
			$ands = array();
			$numWhere = array();

			//pegando filtro de conta logada
			$numWhere[] = 'tu.account_id = '.$account_id;

			//pegando filtro de data/hora
			$start_date = $this->formatDateTimeAction($obj->start_day, $obj->start_hour, '/'); 
			$end_date = $this->formatDateTimeAction($obj->end_day, $obj->end_hour, '/');
			
			$ands[] 	= 'AND';
			$numWhere[] = "(cs.initial_time BETWEEN '$start_date' AND '$end_date')";	
			$ands[] 	= 'OR';
			$numWhere[] = "(cs.final_time BETWEEN '$start_date' AND '$end_date')";	
				
			//pegando filtro de unidades selecionadas
			if($obj->units[0] != '0') {
				$ands[] = 'AND';
				$ids='';
				$last = count($obj->units);
				$i = 0;
				$and = ', ';
				$in = '(';
			
				foreach ($obj->units as $id) {
					$i++;
					if($i == $last) {
						$and = ')';
					}
					$ids = $in.$ids."'".$id."'".$and;
					$in = '';
				}
			
				$numWhere[] = 'cs.unit_id in '.$ids;
			}
	
			//concatenando wheres
			$ind = 0;
			$whereSSP = '';
			$e = ' AND ';
			$ultimo = count($numWhere);
			foreach ($numWhere as $where) {
				$ind++;
				if($ind == $ultimo) {
					$e = '';
				}
				$whereSSP.=$where.$e;
			}

			$orderBy = ' ORDER BY cs.initial_time DESC ';
			
			echo json_encode(
					SSP::buildQuerySimple( $_POST, $table, $primaryKey, $columns, $join, $whereSSP, $orderBy  )
			);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function printPDFSearchAction($query) {
		// SQL server connection information
		$result = SSP::expert( $query );

		$data = array();

		foreach ($result as $row) {
			if(empty($row[0])){ $row[0] = ''; } //placa
			if(empty($row[10])){ $row[10] = ''; } else { if(!empty($row[8])) { $row[10] = $row[10].' às '.$row[8]; } }//data_ini
			if(empty($row[11])){ $row[11] = '-'; } else { $row[11] = $row[11].' às '.$row[9]; } //data_final
			if(empty($row[3])){ $row[3] = ''; } else { $row[3] = gmdate("H:i", ($row[3])); } //total_time
			if(empty($row[7])){ $row[7] = ''; } //endereco
			if(empty($row[5])){ $row[5] = ''; } //poi
			if(empty($row[6])){ $row[6] = ''; } //cerca

		 	array_push($data, [$row[0], $row[10], $row[11], $row[3], $row[7], $row[5], $row[6]]);
		}

		return json_encode($data);
			
	}

	public function printExcelSearchAction($query) {
		// SQL server connection information
		$result = SSP::expert( $query );
		
		$content = '<style>
						table { border: 1px solid #999; border-bottom: none; width: 100%; }
						thead { border: 5px solid #000; }
						tbody td { border: 1px solid #ccc; }
					</style>';
		
		$content = $content.'<table><thead><th>Placa</th><th>Data Inicial</th><th>Data Final</th><th>Tempo Parado<p style="font-size: 9px">(hr:min)</p></th><th>Endereço</th><th>POI</th><th>Cerca</th></thead><tbody>';
		
		foreach ($result as $row) {
			if(empty($row[0])){ $row[0] = ''; } //placa
			if(empty($row[10])){ $row[10] = ''; } else { if(!empty($row[8])) { $row[10] = $row[10].' às '.$row[8]; } }//data_ini
			if(empty($row[11])){ $row[11] = '-'; } else { $row[11] = $row[11].' às '.$row[9]; } //data_final
			if(empty($row[3])){ $row[3] = ''; } else { $row[3] = gmdate("H:i", ($row[3])); } //total_time
			if(empty($row[7])){ $row[7] = ''; } //endereco
			if(empty($row[5])){ $row[5] = ''; } //poi
			if(empty($row[6])){ $row[6] = ''; } //cerca
			$content = $content."<tr><td>$row[0]</td><td>$row[10]</td><td>$row[11]</td><td>$row[3]</td><td>$row[7]</td><td>$row[5]</td><td>$row[6]</td></tr>";
		}
		
		$content = $content.'</tbody></table>';
		
		return $content;
			
	}
}