<?php

class clientController extends defaultController
{
	public function addAction($obj)	{
	 
		 try{	
			$db = new DatabaseHandler();
		
			$db->addOption("table", '"group"');
			$db->addOption("fields", array("name", "account_id"));
			$db->addOption("param", array(":name" => $obj->text, ":account_id" => $obj->account_id));
			$insert = $db->insert();
			$db->close();
			
			if(is_bool($insert) && $insert === true) {
				
				$result = $db->lastInsertId('client_id_seq');
				if($result !== false)
				{
					return $result;
				} else {
					return E_UNABLETOADDCLIENT;
				}
				
			} else {
				return E_UNABLETOADDCLIENT;
			}
			
		 }catch(Exception $e){
		 	return E_INTERNAL;
		 }
	}
	
	public function listAction() {
		try {
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;
			
			$db = new DatabaseHandler();
			$db->addOption("fields", array("g.*"));
			$db->addOption("table", '"group" g');
			$db->addOption("where", array('g.account_id = :account_id'));
			$db->addOption("param", array(':account_id' => $account_id));
			$resultAccount = $db->select(true, 1);
			$db->close();
			
			if(is_object($resultAccount)) {
				$result = array();
				while ($contas=$resultAccount->fetch()) {
					$result[] = $contas;
				}
				
				return $result;
			} else {
				return E_UNABLETOLISTCLIENT;
			}
			
		}catch(Exception $e){
			return E_INTERNAL;
		}
	}
	
	public function listSubGroupsAction($group_id = null) {
		
		try{
			if(!isset($_SESSION)){
				
			}
			$account_id = $_SESSION['user']->getAccount();
			
			$db = new DatabaseHandler();
			$db->addOption("fields", array("sg.id", "sg.name"));
			$db->addOption("table", "subgroup sg");
			$db->addOption("joinTable", array('"group" g'));
			$db->addOption("joinTableOn", array("g.id = sg.group_id"));
			$db->addOption("joinTableType", array("INNER JOIN"));
			if($group_id === null || $group_id->groupIds[0] == '0') {
				$db->addOption("where", array("g.account_id = :account_id"));
				$db->addOption("param", array(":account_id" => $account_id));
			} else {
				$gids='';
				$last = count($group_id->groupIds);
				$i = 0;
				$and = ', ';
				$in = '(';
				
				foreach ($group_id->groupIds as $gid) {
					$i++;
					if($i == $last) {
						$and = ')';
					}
					$gids = $in.$gids."'".$gid."'".$and;
					$in = '';
				}				
				$db->addOption("where", array("g.account_id = :account_id", "sg.group_id in $gids"));
				$db->addOption("logicalOperator", array("AND"));
				$db->addOption("param", array(":account_id" => $account_id));
			}
			$resultSubGroups = $db->select(true, 1);
			$db->close();
			
			if(is_object($resultSubGroups)) {
				$result = array();
				while ($subgroup=$resultSubGroups->fetch()) {
					$result[] = $subgroup;
				}
				
				if($group_id !== null) {
					$result = json_encode($result);
				}				
				return $result;
			} else {
				return E_UNABLETOLISTCLIENT;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addSubGroupAction($obj) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("table", 'subgroup');
			$db->addOption("fields", array("name", "group_id"));
			$db->addOption("param", array(":name" => $obj->text, ":group_id" => $obj->group_id));
			$insert = $db->insert();
			$result = $db->lastInsertId('subgroup_id_seq');
			$db->close();
			
			
			if(is_bool($insert) && $insert === true){
					
				if($result !== false) {
					return $result;
				} else {
					return E_UNABLETOCREATESUBGROUP;
				}	
				
			} else {
					return E_UNABLETOCREATESUBGROUP;
			}
			
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function listTreeAction() {
		
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("c.name", "c.id"));
			$db->addOption("table", '"group" c');
			$db->addOption("joinTable", array("account ac"));
			$db->addOption("joinTableOn", array("ac.id = c.account_id"));
			$db->addOption("joinTableType", array("INNER JOIN"));
			$db->addOption("orderBy", array("c.name"));
			$resultAccount = $db->select(false, 1);
			$db->close();
	
			if(is_object($resultAccount)) {
				$result = array();
				while($conta=$resultAccount->fetch()) {
					$conta->children = $this->getSubGroupsAction($conta->id);
					$conta->parent_id = $conta->id;
					$conta->data = array('file' => false);
					$result[] = $conta;
				}
		
				$json = json_encode($result);
				//Trocando nome dos campos de arrays para funcionar no plugin jsTree
				$return = str_replace("name", "text", $json);
				//Passando para json
				$return = json_encode($return);
				//Imprimindo para o plugin jsTree		
				return json_decode($return);
			} else {
				return E_UNABLETOLISGROUPTREE;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function listTreeAccLogAction($id=null) {
		try {
			
			$accountId = $_SESSION['user']->getAccount();
			$joinGroupAccess = "";
			if( isset($id) ){
				$joinGroupAccess = " INNER JOIN user_group_access uga ON uga.group_id = gp.id AND uga.user_id = $id ";
			}
			
			$db = new DatabaseHandler();
			$db->addOption("table", "SELECT DISTINCT
										gp.name, 
										gp.id  
									FROM  
										\"group\" gp  
										INNER JOIN account ac ON ac.id = gp.account_id   
										$joinGroupAccess
									WHERE  
										gp.account_id = $accountId 
									ORDER BY gp.name");

			$resultAccount = $db -> select(false, 0, true);
			$db->close();
			/* 
			TODO:
			Lógica parecida com fetchTree (defaultController);
			 *
			 *
			 */		
			if(is_object($resultAccount)){
				$result = array();
				while($conta=$resultAccount->fetch())
				{
					$conta->children = $this->getSubGroupsAction($conta->id);
					$conta->parent_id = $conta->id;
					$conta->data = array('file' => false);
					//$conta->state = array('opened' => true);
					$result[] = $conta;
				}
		
				$tree = array("text" => "Grupos", "state" => array("opened" => true));
				$tree["children"] = $result;
				$json = json_encode($tree);
				//Trocando nome dos campos de arrays para funcionar no plugin jsTree
				$return = str_replace("name", "text", $json);
				//Passando para json
				$return = json_encode($return);
				//Imprimindo para o plugin jsTree		
				return json_decode($return);
			}else{
				return E_UNABLETOLISTACCOUNTTREE;
			}
			
		}catch(Exception $e){
			return E_INTERNAL;
		}		
	}
	
	public function listTreeFormEditAction($account_id) {
			
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("c.name", "c.id"));
			$db->addOption("table", '"group" c');
			$db->addOption("joinTable", array("account ac"));
			$db->addOption("joinTableOn", array("ac.id = c.account_id"));
			$db->addOption("joinTableType", array("INNER JOIN"));
			$db->addOption("where", array("c.account_id = :acc_id"));
			$db->addOption("param", array(":acc_id" => $account_id));
			$db->addOption("orderBy", array("c.name"));		
			$resultAccount = $db->select(true, 1);
			$db->close();
			
			if($resultAccount == false) {
				return '{ "id": "ajson1", "parent": "#", "text": "Grupo Geral", "state": {"opened": true}, "data" : { "file" : false } },
						{ "id": "ajson2", "parent": "ajson1", "text": "Veículos", "data" : { "file" : true } },';
			}
			
			$result = array();
			while($conta=$resultAccount->fetch()) {
				$conta->children = $this->getSubGroupsAction($conta->id);
				$conta->parent_id = $conta->id;
				$conta->data = array('file' => false);
				$conta->state = array('opened' => true);
				$result[] = $conta;
			}
			
			$json = json_encode($result);
			//Trocando nome dos campos de arrays para funcionar no plugin jsTree
			$return = str_replace("name", "text", $json);
			//Passando para json
			$return = json_encode($return);
			//Imprimindo para o plugin jsTree
			return json_decode($return);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getSubGroupsAction($group_id) {
		try{
			$db = new DatabaseHandler();	
			$db->addOption("fields", array("sub.name as text", "sub.id as id", "sub.color"));
			$db->addOption("table", 'subgroup sub');
			$db->addOption("where", array("sub.group_id = :group_id"));
			$db->addOption("param", array(":group_id" => $group_id));	
			
			$resultAccount = $db->select(true, 1);
			$db->close();
			if(!is_object($resultAccount)) {
				return false;
			}
			
			while($sub=$resultAccount->fetch()) {
				$sub->data = array('file' => true);
				$result[] = $sub;
			}
			
			return $result;
		}catch(Exception $e){
			return E_INTERNAL;
		}
		/*
		try{
			$user_id = $_SESSION['user'] -> getUserId();

			$db = new DatabaseHandler();	
			$db->addOption("table", "SELECT DISTINCT sg.name as text, sg.id as id
									FROM 
										subgroup sg
										INNER JOIN user_group_access usgrac ON usgrac.group_id = sg.group_id AND usgrac.user_id = $user_id
									WHERE 
										sg.group_id = $group_id
									ORDER BY sg.name");		
			
			$resultAccount = $db -> select(false, 0, true);
			$db->close();
			if(!is_object($resultAccount)) {
				return false;
			}
			
			while($sub=$resultAccount->fetch()) {
				$sub->data = array('file' => true);
				$result[] = $sub;
			}
			
			return $result;
		}catch(Exception $e){
			return E_INTERNAL;
		}
		*/
	}
	
	public function editGroupAction($obj) {
		try{
			$db = new DatabaseHandler();
			$db->addOption("fields", array("name = :name"));
			$db->addOption("table", '"group"');
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(":id" => $obj->id, ":name" => $obj->text));
			$resultUpdate = $db->update();
			$db->close();
			
			if(!is_bool($resultUpdate) || $resultUpdate !== true) {
				return E_UNABLETOUPDATEGROUP;
			}
		}catch(Exception $e){
			return E_INTERNAL;
		}
	}
	
	public function editSubGroupAction($obj) {
		
		try{
			$db = new DatabaseHandler();
			$db->addOption("fields", array("name = :name"));
			$db->addOption("table", '"subgroup"');
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(":id" => $obj->id, ":name" => $obj->text));
			$resultUpdate = $db->update();
			$db->close();
			
			if(!is_bool($resultUpdate) || $resultUpdate !== true) {
				return E_UNABLETOUPDATESUBGROUP;
			}		
		}catch(Exception $e){
				return E_INTERNAL;
		}
	}
	
	public function checkDepAction($group_id) {
		$db = new DatabaseHandler();
		$db->addOption("fields", array("*"));
		$db->addOption("table", 'tracked_unit');
		$db->addOption("where", array("group_id = :group_id", "subgroup_id = :group_id"));
		$db->addOption("logicalOperator", array("OR"));
		$db->addOption("param", array(":group_id" => $group_id));		
		$resultDep = $db->select(true, 1);
		$db->close();
		
		if($resultDep) {
			return '0';
		} else {
			return '1';
		}
	}
	
	public function deleteAction($obj) {
		$db = new DatabaseHandler();
		
		//É SUBGROUP
		if($obj->group !== 'group') {
			
			$db->addOption("table", '"subgroup"');
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(":id" => $obj->id));
			$deleteSubgroup = $db->delete();
			
			if(is_bool($deleteSubgroup) &&  $deleteSubgroup === true){
				
				$db->addOption("table", '"user_group_access"');
				$db->addOption("where", array("subgroup_id = :id"));
				$db->addOption("param", array(":id" => $obj->id));
				$deleteGroupAccess = $db->delete();
				
				if(is_bool($deleteGroupAccess) && $deleteGroupAccess === true){
						
					$db->addOption("table", '"tracked_unit"');
					$db->addOption("where", array("subgroup_id = :id"));
					$db->addOption("param", array(":id" => $obj->id));
					$deleteUnit = $db->delete();
					
					if(!is_bool($deleteUnit) || $deleteUnit !== true){
						return E_FAILEDTOREMOVESUBGROUPUNIT;
					}
					
				}else{

					return E_FAILEDTOREMOVESUBGROUPACCESS;
				}
				
			}else{
				return E_FAILEDTOREMOVESUBGROUP;
			}


			return json_encode('Subgrupo removido.');
			
		//É GROUP
		} else {
			
			
			$db->addOption("table", '"group"');
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(":id" => $obj->id));
			$deleteGroup = $db->delete();
			
			
			if(is_bool($deleteGroup) && $deleteGroup === true){
				$db->addOption("table", '"subgroup"');
				$db->addOption("where", array("group_id = :id"));
				$db->addOption("param", array(":id" => $obj->id));
				$deleteSubgroup = $db->delete();
				
				if(is_bool($deleteSubgroup) && $deleteSubgroup === true){
					$db->addOption("table", '"user_group_access"');
					$db->addOption("where", array("group_id = :id"));
					$db->addOption("param", array(":id" => $obj->id));
					$deleteGroupAccess = $db->delete();
					
					if(is_bool($deleteGroupAccess) && $deleteGroupAccess === true){
						$db->addOption("table", '"tracked_unit"');
						$db->addOption("where", array("group_id = :id"));
						$db->addOption("param", array(":id" => $obj->id));
						$deleteUnit = $db->delete();
						
						
						if(!is_bool($deleteUnit) && $deleteUnit !== true){
							return E_FAILEDTOREMOVEGROUPTRACKEDUNIT;
						}
						
					}else{
						return E_FAILEDTOREMOVEGROUPACCESS;
					}	
				}else{
					return E_FAILEDTOREMOVEGROUPSUBGROUPS;
				}	
			}else{
				return E_FAILEDTOREMOVEGROUP;
			}

			return json_encode('Grupo removido.');
		}
		$db->close();
	}
}