<?php
  /**
   * Class checklistController
   * 
   * classe de controle para tabelas de checklist de ordens VELLEDA
   * 
   * @author Paulo Silva
   * @category controladora 
   * @package mobile / web controler
   * @uses ...
   */

class checklistController extends defaultController
{
	public function indexAction($infos = null) {
		$unit = new UnitController();
		$return = array();
		$user_id = $_SESSION['user'] -> getUserId();

		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());
		$return['unitTypeList'] = $this -> listUnitTypeAction($user_id);
		$return['shippingCompanyList'] = $this -> listShippingCompanyAction($user_id);

		return json_encode($return);
	}

	public function GetDataAction($user_id) {
		$return = array();	

		$return['unitTypeList'] = $this -> listUnitTypeAction($user_id);
		$return['shippingCompanyList'] = $this -> listShippingCompanyAction($user_id);

		return json_encode($return);
	}
	
	public function list_VLAction($user_id) {
		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT
											*
										FROM  
											velleda.checklist_order
										WHERE  
											user_id = $user_id");
			
			$select = $db -> select(false, 0, true);
			$db->close();

			$result = array();
			if (is_object($select)) {	
				while ($entry = $select -> fetch()) {
					$result[] = $entry;
				}	
			}
			return json_encode(array('data' => $result, 'message' => null));
		} catch(Exception $e) {
			return json_encode(array('data' => false, 'message' => $e->getMessage()));
		}
	}

	public function getById_VLAction($id) {
		try {

			$db = new DatabaseHandler();			
			$db->addOption("table", "SELECT
										*
									FROM  
										velleda.checklist_order
									WHERE  
										id = $id");

			$select = $db -> select(false, 0, true);
			$db->close();

			$order = $select->fetch();
			return json_encode(array('data' => $order, 'message' => null));
		} catch(Exception $e) {
			return json_encode(array('data' => false, 'message' => $e->getMessage()));
		}
	}
	
	public function add_VLAction($obj) {
		
		$user_id = $obj->user_id;
		$account_id = $obj->account_id;
		$dataatual = $obj->dataatual;
		$placacavalo = $obj->placacavalo;
		$origem = $obj->origem;
		$destino = $obj->destino;
		$motorista = $obj->motorista;
		$nlacre = $obj->nlacre;
		$nnf = $obj->nnf;
		$hsaida = $obj->hsaida;
		$local = $obj->local;
		$placacarreta = $obj->placacarreta;
		$transp = $obj->transp;
		$tipoveiculo = $obj->tipoveiculo;
		$ncnh = $obj->ncnh;
		$datavalidade = $obj->datavalidade;
		$preventrega = $obj->preventrega;
		$tempotermo = $obj->tempotermo;
		$observacoes = $obj->observacoes;
		$datatransm = $obj->datatransm;
		$datasaida = $obj->datasaida;
		$horachegada = $obj->horachegada;
		$group_id = $obj->group_id;
		$status = $obj->status;

		$camboio = ($obj->camboio) ? 't' : 'f';
		$pneus = ($obj->pneus) ? 't' : 'f';
		$eletrica = ($obj->eletrica) ? 't' : 'f';
		$condgerais = ($obj->condgerais) ? 't' : 'f';
		$estepes = ($obj->estepes) ? 't' : 'f';
		$estintorvalido = ($obj->estintorvalido) ? 't' : 'f';
		$cadeadobau = ($obj->cadeadobau) ? 't' : 'f';
		$termoking = ($obj->termoking) ? 't' : 'f';
		$abastecimento = ($obj->abastecimento) ? 't' : 'f';
		$sensoresportas = ($obj->sensoresportas) ? 't' : 'f';
		$sensorbau = ($obj->sensorbau) ? 't' : 'f';
		$bloqueio = ($obj->bloqueio) ? 't' : 'f';
		$sensordesengate = ($obj->sensordesengate) ? 't' : 'f';
		$sirene = ($obj->sirene) ? 't' : 'f';
		$travabau = ($obj->travabau) ? 't' : 'f';
		$procedimentojbs = ($obj->procedimentojbs) ? 't' : 'f';
		$conhecimentoroda = ($obj->conhecimentoroda) ? 't' : 'f';
		$manuseiorastreador = ($obj->manuseiorastreador) ? 't' : 'f';

		try {
			$db = new DatabaseHandler();
			$SQL = "INSERT INTO velleda.checklist_order (
						user_id,
						account_id,
						dataatual,
						placacavalo,
						origem,
						destino,
						motorista,
						nlacre,
						nnf,
						hsaida,
						local,
						placacarreta,
						transp,
						tipoveiculo,
						ncnh,
						datavalidade,
						preventrega,
						tempotermo,
						camboio,
						pneus,
						eletrica,
						condgerais,
						estepes,
						estintorvalido,
						cadeadobau,
						termoking,
						abastecimento,
						sensoresportas,
						sensorbau,
						bloqueio,
						sensordesengate,
						sirene,
						travabau,
						procedimentojbs,
						conhecimentoroda,
						manuseiorastreador,
						observacoes,
						datatransm,
						datasaida,
						horachegada,
						group_id,
						status
					) 
				    VALUES 
					(
						$user_id,
						$account_id,
						'$dataatual',
						'$placacavalo',
						'$origem',
						'$destino',
						'$motorista',
						'$nlacre',
						'$nnf',
						'$hsaida',
						'$local',
						'$placacarreta',
						$transp,
						$tipoveiculo,
						'$ncnh',
						'$datavalidade',
						'$preventrega',
						'$tempotermo',
						'$camboio',
						'$pneus',
						'$eletrica',
						'$condgerais',
						'$estepes',
						'$estintorvalido',
						'$cadeadobau',
						'$termoking',
						'$abastecimento',
						'$sensoresportas',
						'$sensorbau',
						'$bloqueio',
						'$sensordesengate',
						'$sirene',
						'$travabau',
						'$procedimentojbs',
						'$conhecimentoroda',
						'$manuseiorastreador',
						'$observacoes',
						'$datatransm',
						'$datasaida',
						'$horachegada',
						$group_id,
						$status
					)";

			$db -> addOption("table", $SQL);
			$insert = $db -> insert(false, 0, 0, true);
			
			if(is_bool($insert) && $insert === true) {
				$lastInsert = $db->lastInsertId('velleda.checklist_order_id_seq');
				$return = json_encode(array('data' => $lastInsert, 'message' => "Ordem cadastrado com sucesso"));
			} else {
				$return = json_encode(array('data' => false, 'message' => $insert));
			}

			$db->close();
			return $return;	

		} catch(Exception $e) {
			return json_encode(array('data' => false, 'message' => $e->getMessage()));
		}
	}
	
	public function edit_VLAction($obj) {

		$id = $obj->id;
		$user_id = $obj->user_id;
		$account_id = $obj->account_id;
		$status = $obj->status;
		$dataatual = $obj->dataatual;
		$placacavalo = $obj->placacavalo;
		$origem = $obj->origem;
		$destino = $obj->destino;
		$motorista = $obj->motorista;
		$nlacre = $obj->nlacre;
		$nnf = $obj->nnf;
		$hsaida = $obj->hsaida;
		$local = $obj->local;
		$placacarreta = $obj->placacarreta;
		$transp = $obj->transp;
		$tipoveiculo = $obj->tipoveiculo;
		$ncnh = $obj->ncnh;
		$datavalidade = $obj->datavalidade;
		$preventrega = $obj->preventrega;
		$tempotermo = $obj->tempotermo;
		$observacoes = $obj->observacoes;
		$datatransm = $obj->datatransm;
		$datasaida = $obj->datasaida;
		$horachegada = $obj->horachegada;

		$camboio = (filter_var($obj->camboio, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$pneus = (filter_var($obj->pneus, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$eletrica = (filter_var($obj->eletrica, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$condgerais = (filter_var($obj->condgerais, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$estepes = (filter_var($obj->estepes, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$estintorvalido = (filter_var($obj->estintorvalido, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$cadeadobau = (filter_var($obj->cadeadobau, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$termoking = (filter_var($obj->termoking, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$abastecimento = (filter_var($obj->abastecimento, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$sensoresportas = (filter_var($obj->sensoresportas, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$sensorbau = (filter_var($obj->sensorbau, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$bloqueio = (filter_var($obj->bloqueio, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$sensordesengate = (filter_var($obj->sensordesengate, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$sirene = (filter_var($obj->sirene, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$travabau = (filter_var($obj->travabau, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$procedimentojbs = (filter_var($obj->procedimentojbs, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$conhecimentoroda = (filter_var($obj->conhecimentoroda, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';
		$manuseiorastreador = (filter_var($obj->manuseiorastreador, FILTER_VALIDATE_BOOLEAN)) ? 't' : 'f';

		try {
			$db = new DatabaseHandler();
			$SQL = "UPDATE velleda.checklist_order SET 
						user_id = $user_id,
						dataatual = '$dataatual',
						placacavalo = '$placacavalo',
						origem = '$origem',
						destino = '$destino',
						motorista = '$motorista',
						nlacre = '$nlacre',
						nnf = '$nnf',
						hsaida = '$hsaida',
						local = '$local',
						placacarreta = '$placacarreta',
						transp = $transp,
						tipoveiculo = $tipoveiculo,
						ncnh = '$ncnh',
						datavalidade = '$datavalidade',
						preventrega = '$preventrega',
						tempotermo = '$tempotermo',
						camboio = '$camboio',
						pneus = '$pneus',
						eletrica = '$eletrica',
						condgerais = '$condgerais',
						estepes = '$estepes',
						estintorvalido = '$estintorvalido',
						cadeadobau = '$cadeadobau',
						termoking = '$termoking',
						abastecimento = '$abastecimento',
						sensoresportas = '$sensoresportas',
						sensorbau = '$sensorbau',
						bloqueio = '$bloqueio',
						sensordesengate = '$sensordesengate',
						sirene = '$sirene',
						travabau = '$travabau',
						procedimentojbs = '$procedimentojbs',
						conhecimentoroda = '$conhecimentoroda',
						manuseiorastreador = '$manuseiorastreador',
						observacoes = '$observacoes',
						datatransm = '$datatransm',
						datasaida = '$datasaida',
						horachegada = '$horachegada',
						status = $status
				    WHERE 
						id = $id";

			$db -> addOption("table", $SQL);
			$update = $db -> update(false, true);
			
			if(is_bool($update) && $update === true) {
				$return = json_encode(array('data' => $id, 'message' => "Ordem alterada com sucesso"));
			} else {
				$return = json_encode(array('data' => false, 'message' => $update));
			}	

			$db->close();
			return $return;	

		} catch(Exception $e) {
			return json_encode(array('data' => false, 'message' => $e->getMessage()));
		}
	}

	public function delete_VLAction($obj) {

		//
	}
	/**
	 * listUnitTypeAction : Retorna os tipos de unidades de um cliente
	 * @param $user_id : id do usuário
	 */
	public function listUnitTypeAction($user_id) {
		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											v_ut.*
										FROM  
											velleda.unit_type v_ut
											INNER JOIN mova.user_group_access usgrac
											   	ON usgrac.group_id = v_ut.group_id 
											   	AND usgrac.user_id = $user_id 
										WHERE  
											v_ut.status = 1");
			
			$select = $db -> select(false, 0, true);
			$db->close();

			$result = array();
			if (is_object($select)) {	
				while ($entry = $select -> fetch()) {
					$result[] = $entry;
				}	
			}
			return json_encode(array('data' => $result, 'message' => null));
		} catch(Exception $e) {
			return json_encode(array('data' => false, 'message' => $e->getMessage()));
		}
	}
	/**
	 * listShippingCompanyAction : Retorna a lista de transportadores de um cliente
	 * @param $user_id : id do usuário
	 */
	public function listShippingCompanyAction($user_id) {
		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											v_sc.*
										FROM  
											velleda.shipping_company v_sc
											INNER JOIN mova.user_group_access usgrac
											   	ON usgrac.group_id = v_sc.group_id 
											   	AND usgrac.user_id = $user_id 
										WHERE  
											v_sc.status = 1");
			
			$select = $db -> select(false, 0, true);
			$db->close();

			$result = array();
			if (is_object($select)) {	
				while ($entry = $select -> fetch()) {
					$result[] = $entry;
				}	
			}
			return json_encode(array('data' => $result, 'message' => null));
		} catch(Exception $e) {
			return json_encode(array('data' => false, 'message' => $e->getMessage()));
		}
	}
}
