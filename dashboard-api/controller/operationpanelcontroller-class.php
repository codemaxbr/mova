<?php
Class OperationpanelController extends defaultController {
	
	public function indexAction($infos = null) {
		$widget = new widgetController();
		$return = array();
		$return['permissionType'] = $this -> haveWriteAction($infos);
		$return['infos'] = $widget -> getWidgetInfoAction($infos);
		$sessionInfo = json_decode($_SESSION['user'] -> getUserInfo());
		$return['userinfo'] = $sessionInfo;

		return json_encode($return);
	}
}