<?php

class driversController extends defaultController
{
	public function indexAction($infos=null) {
		$return = array();
		$unit = new UnitController();
		
		$widget = new widgetController();
		$return['infos'] = $widget->getWidgetInfoAction($infos);
		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());
		$return['groupinfo'] = $unit -> listGroupAction();
		$return['functioninfo'] = $this -> listFunctionAction();
		
		return json_encode($return);
	}
	
	public function listAction($param = null) {
		$user_id = $_SESSION['user'] -> getUserId();
		$accountId 	= $_SESSION['user']	-> getAccount();
		$typeData = true;
		//group
		$sqlGroupWhere = '';
		if($param != null){
			$groups = '0';
			foreach ($param->group_id as $group) {
				$groups .= ','.$group;
			}
			if( ($groups != '0,0') && ($groups != '0') ){
				$sqlGroupWhere = " AND gp.id IN ($groups) ";
			}

			if(isset($param->typeData)){
				if(!$param->typeData){
					$typeData = false;
				}
			}
		}

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											d.*, 
											gp.name as group_name
										FROM 
											driver d
											INNER JOIN \"group\" gp ON d.group_id = gp.id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = gp.id AND usgrac.user_id = $user_id
										WHERE 
											d.status = 1
											AND d.account_id = $accountId
											$sqlGroupWhere
										ORDER BY d.name");
			
			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	
			}
			if($typeData){
				$return = array("data" => $result);
			}else{
				$return = $result;
			}
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function getByIdAction($id) {
		try {

			$db = new DatabaseHandler();			
			$db->addOption("table", "SELECT
										d.*
										, to_char(d.cnh_validate, 'DD/MM/YYYY') as cnh_validate_format
										, to_char(d.rac_validate, 'DD/MM/YYYY') as rac_validate_format
										, to_char(d.aso_validate, 'DD/MM/YYYY') as aso_validate_format
										, gp.name as group_name
										, df.name as function_name
									FROM  
										driver d
										INNER JOIN \"group\" gp ON d.group_id = gp.id
										LEFT JOIN driver_function df ON d.driver_function_id = df.id
									WHERE  
										d.id = $id");

			$sqlResult = $db -> select(false, 0, true);
			$db->close();

			$unities = $this -> getUnitiesAssociatesAction($id);

			$driver = $sqlResult->fetch();
			return json_encode(array("driver" => $driver, "driverUnit" => $unities));

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addAction($obj) {
		$user = new userController();

		if($this->hasDriverAction($obj->login, $obj->cnh)) {
			return E_DRIVEREXISTS;
		}

		$post_account_id = $obj->account_id;
		$post_name = $obj->name;
		$post_email = $obj->email;
		$post_phone = $obj->phone;
		$post_login = $obj->login;
		$post_auth = $obj->auth;
		//A SENHA DEPENDE DO CAMPO AUTH
		$post_cnh = $obj->cnh;
		$post_cnh_category = $obj->cnh_category;
		$post_cnh_validate = $this->formatDateUS($obj->cnh_validate);
		$post_group_id = $obj->group_id;

		$sqlPasswordField = '';
		$sqlPasswordValue = '';
		if($obj->auth == 2){
			$post_password = str_pad(mt_rand(0, 99999), 5, '0', STR_PAD_LEFT);
			$sqlPasswordField = " password, ";			
			$sqlPasswordValue = " '$post_password', ";		
		}

		$sqlValeField = '';
		$sqlValeValue = '';
		if($obj->dataVale != null){
			$post_passport = $obj->dataVale->passport;
			$post_function = $obj->dataVale->function;
			$post_area = $obj->dataVale->area;
			$post_empresa = $obj->dataVale->empresa;
			$post_gestor = $obj->dataVale->gestor;
			$post_gestor_tel = $obj->dataVale->gestor_tel;
			$post_local = $obj->dataVale->local;
			$post_rac_validate = $this->formatDateUS($obj->dataVale->rac_validate);
			$post_aso_validate = $this->formatDateUS($obj->dataVale->aso_validate);

			$sqlValeField = " , passport, driver_function_id, area, empresa, gestor, gestor_tel, local, rac_validate, aso_validate ";			
			$sqlValeValue = " , '$post_passport', $post_function, '$post_area', '$post_empresa', '$post_gestor', '$post_gestor_tel', '$post_local', '$post_rac_validate', '$post_aso_validate' ";			
		}

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "INSERT INTO driver (
											account_id, 
											name, 
											email, 
											phone, 
											auth, 
											login, 
											$sqlPasswordField
											cnh, 
											cnh_category, 
											cnh_validate, 
											group_id
											$sqlValeField
											) 
									   VALUES 
										(
											'$post_account_id',
											'$post_name',
											'$post_email',
											'$post_phone',
											'$post_auth',
											'$post_login',
											$sqlPasswordValue
											'$post_cnh',
											'$post_cnh_category',
											'$post_cnh_validate',
											'$post_group_id'
											$sqlValeValue
										)");

			$insert = $db -> insert(false, 0, 0, true);

			$lastInsert = $db->lastInsertId('driver_id_seq');
			$db->close();
			
			if(is_bool($insert) && $insert === true) {
				//Envio de e-mail
				if($obj->auth == 2){
					$this->sendEmailAction("Conta Mova - Motorista", "Seu usuário foi criada com sucesso!<br />Abaixo segue seu login e sua senha:<br /><br />Login: ".$post_login."<br />Senha: ".$post_password, $post_email);
				}
				return json_encode(array('id' => $lastInsert, 'message' => "Motorista cadastrado com sucesso"));
			} else {
				return E_DRIVERCREATEFAILED;
			}			
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function editAction($obj) {
		//$user = new userController();

		if($this->hasDriverAction($obj->login, $obj->cnh, $obj->id)) {
			return E_DRIVEREXISTS;
		}

		$id = $obj->id;
		$post_account_id = $obj->account_id;
		$post_name = $obj->name;
		$post_email = $obj->email;
		$post_phone = $obj->phone;
		$post_login = $obj->login;
		$post_auth = $obj->auth;
		//A SENHA DEPENDE DO CAMPO AUTH
		$post_cnh = $obj->cnh;
		$post_cnh_category = $obj->cnh_category;
		$post_cnh_validate = $this->formatDateUS($obj->cnh_validate);
		$post_group_id = $obj->group_id;
		$post_reset_password = $obj->reset_password;

		$sqlPassword = '';
		if(($obj->auth == 2) && ($post_reset_password == "true")){
			$post_password = str_pad(mt_rand(0, 99999), 5, '0', STR_PAD_LEFT);
			$sqlPassword = " password = '$post_password', ";	
		}

		$sqlVale = '';
		if($obj->dataVale != null){
			$post_passport = $obj->dataVale->passport;
			$post_function = $obj->dataVale->function;
			$post_area = $obj->dataVale->area;
			$post_empresa = $obj->dataVale->empresa;
			$post_gestor = $obj->dataVale->gestor;
			$post_gestor_tel = $obj->dataVale->gestor_tel;
			$post_local = $obj->dataVale->local;
			$post_rac_validate = $this->formatDateUS($obj->dataVale->rac_validate);
			$post_aso_validate = $this->formatDateUS($obj->dataVale->aso_validate);

			$sqlVale  = " , passport = '$post_passport', ";
			$sqlVale .= " driver_function_id = $post_function, ";
			$sqlVale .= " area = '$post_area', ";
			$sqlVale .= " empresa = '$post_empresa', ";
			$sqlVale .= " gestor = '$post_gestor', ";
			$sqlVale .= " gestor_tel = '$post_gestor_tel', ";
			$sqlVale .= " local = '$post_local', ";
			$sqlVale .= " rac_validate = '$post_rac_validate', ";
			$sqlVale .= " aso_validate ='$post_aso_validate' ";					
		}

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "UPDATE driver SET 
											account_id = '$post_account_id', 
											name = '$post_name', 
											email = '$post_email', 
											phone = '$post_phone', 
											auth = '$post_auth', 
											login = '$post_login', 
											$sqlPassword
											cnh = '$post_cnh', 
											cnh_category = '$post_cnh_category', 
											cnh_validate = '$post_cnh_validate', 
											group_id = '$post_group_id'
											$sqlVale 
									   WHERE 
										id = $id");

			$result = $db -> update(false, true);
			$db->close();
			
			if(is_bool($result) && $result === true) {
				//Envio de e-mail
				if(($obj->auth == 2) && ($post_reset_password == "true")){	
					$this->sendEmailAction("Conta Mova - Motorista", "Seu usuário foi alterado com sucesso!<br />Abaixo segue seu login e sua senha:<br /><br />Login: ".$post_login."<br />Senha: ".$post_password, $post_email);
				}
				return json_encode(array('id' => $id, 'message' => "Motorista alterado com sucesso"));
			} else {
				return E_DRIVERUPDATEFAILED;
			}			
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	private function hasDriverAction($matr, $cnh, $id=null) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("*"));
			$db->addOption("table", "driver");
			if($id === null) {
				$db->addOption("where", array("(login = :matricula", "cnh = :cnh)", "status = 1"));
				$db->addOption("logicalOperator", array("OR", "AND"));
				$db->addOption("param", array(":matricula" => $matr, ":cnh" => $cnh));
			} else {
				$db->addOption("where", array("(login = :matricula", "cnh = :cnh)", "id != :id", "status = 1"));
				$db->addOption("logicalOperator", array("OR", "AND", "AND"));
				$db->addOption("param", array(":matricula" => $matr, ":cnh" => $cnh, ":id" => $id));
			}
			$resultAccount = $db->select(true, 1);
			$db->close();
			
			if(is_object($resultAccount)) {
				return true;
			} else {
				return false;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}		
	}

	public function getUnitiesAssociatesAction($id) {		
		try {
			$db = new DatabaseHandler();			
			$db->addOption("table", "SELECT
										tu.id,
										dtu.status,
										tu.label as name,
										tu.label2 as description,
										tu.group_id,
										gp.name as group_name,
										dtu.memory_position
									FROM  
										driver_tracked_unit dtu
										INNER JOIN tracked_unit tu ON tu.id = dtu.unit_id
										INNER JOIN \"group\" gp ON tu.group_id = gp.id
									WHERE  
										dtu.driver_id = $id
										AND dtu.status > 0");

			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	
			}

			$return = array("data" => $result);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	/**
	 * METODO PARA REENVIAR A LISTA DE MOTORISTAS DE UMA DETERMINADA UNIDADE.
	 * OU A LISTA DE UNIDADES DE UM DETERMINADO MOTORISTA.
	 * @param entreda: $obj contendo o unit_id, id da unidade; driver_id, id do motorista; memory_position, posição da memoria em que o motorista se encontra; type, define o tipo de associação
	 */
	public function resendDriverUnitAction($obj=null) {
		try {
			if(sizeof($obj) <= 0){
				return E_DRIVERASSOCFAILED;
			}
			$db = new DatabaseHandler();
			$user_id = $_SESSION['user'] -> getUserId();

			$arrUnitId = array();
			$arrDriverId = array();
			$arrMemory = array();

			$input = array();
			foreach ($obj as &$value) {
				$type = $value -> type;
				$id = $value -> id;
				$arrUnitId[] = $value -> unit_id;
				$arrDriverId[] = $value -> driver_id;
				//$arrMemory[] = $value -> memory_position;
				$input[] = array('unit_id' => $value -> unit_id, 'driver_id' => $value -> driver_id, 'memory' => $value -> memory_position);
			}
			$unitId = join(',', $arrUnitId);
			$driverId = join(',', $arrDriverId);
			/**
			 * ATUALIZAR REGISTROS REMOVIDOS NA driver_tracked_unit PARA STATUS ZERO
			 * O RETORNO DESTA ATUALIZAÇÃO SERÁ A LISTA DE MOTORISTAS QUE DEVE SER GERADO COMANDDO PARA SERE RETIRADOS DOS DEVICES
			 */
			$SQL = " UPDATE driver_tracked_unit SET status = 1 WHERE driver_id IN ($driverId) AND unit_id IN ($unitId) AND status > 0 ";

			$db -> addOption("table", $SQL);
			$result = $db -> update(false, true, 0);
			$db->close();

			if($type == "drivers"){
				$unit = new UnitController();
				$unitAssoc = $unit -> getDriverAssociatesAction($id);
			}else{
				$unitAssoc = $this -> getUnitiesAssociatesAction($id);				
			}

			$db->close();

			if(is_bool($result) && $result === true) {
				//CHAMA AQUI O METODO PARA ADICIONAR MOTORISTAS
				$sendCommandReturn = Command::sendCommandDrivers($input, 'add');
				if($sendCommandReturn){
					return json_encode(array("data" => $unitAssoc, "msg" => "Motoristas enviados com sucesso."));
				}else{
					return E_DATACOMMANDASSOCFAILED;
				}
			} else {
				if($type == "drivers"){
					return E_DRIVERASSOCFAILED;
				}else{
					return E_UNITASSOCFAILED;			
				}
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	/**
	 * METODO QUE GRAVA A ASSOCIAÇÃO DE MOTORISTAS COM UNIDADES E INSTANCIA A CLASSE DE ENVIO DE COMANDOS PARA GRAVAR O MOTORISTA NAS UNIDADES
	 * @param entreda: $obj contendo o ID, que pode ser da unidade ou do motorista, dependendo de quem requisitou o metodo
	 * @param entreda: $obj contendo o type, defini se o ide é o do motorista ou da unidade
	 * @param entreda: $obj contendo o unitAssoc & unitRemove, listas dos itens que serão adicionados ou removidos 
	 */
	public function assocDriverUnitAction($obj) {
		try {
			$db = new DatabaseHandler();
			$user_id = $_SESSION['user'] -> getUserId();

			$id = $obj -> id;
			$type = $obj -> type;
			$unitAssoc = '';
			$unitRemove = '';

			if(isset($obj -> unitAssoc)){
				$unitAssocArr = $obj -> unitAssoc;
				$unitAssoc = join(',', $unitAssocArr);
			}
			if(isset($obj -> unitRemove)){
				$unitRemoveArr = $obj -> unitRemove;
				$unitRemove = join(',', $unitRemoveArr);
			}
			/**
			 * ATUALIZAR REGISTROS REMOVIDOS NA driver_tracked_unit PARA STATUS ZERO
			 * O RETORNO DESTA ATUALIZAÇÃO SERÁ A LISTA DE MOTORISTAS QUE DEVE SER GERADO COMANDDO PARA SERE RETIRADOS DOS DEVICES
			 */
			$sqlRemoveData  = " UPDATE driver_tracked_unit SET status = 0 "; // STATUS ZERO INFORMANDO QUE UM COMANDO FOI ENVIADO E QUE ESTÁ PENDENTE
			if($type == "drivers"){
				$sqlRemoveData .= " WHERE driver_id IN ($unitRemove) AND unit_id IN ($id) AND status > 0 ";
			}else{
				$sqlRemoveData .= " WHERE driver_id IN ($id) AND unit_id IN ($unitRemove) AND status > 0 ";
			}
			$sqlRemoveData .= " RETURNING unit_id, driver_id, memory_position; ";
			$db -> addOption("table", $sqlRemoveData);
			$dataToRemove = $db -> update(false, true, 1);

			$inputRemove = array();
			if (is_object($dataToRemove)) {	
				while ($value = $dataToRemove -> fetch()) {
					$inputRemove[] = array('unit_id' => $value -> unit_id, 'driver_id' => $value -> driver_id, 'memory' => $value -> memory_position);
				}	
			}
			if(sizeof($inputRemove) > 0){
				//CHAMA AQUI O METODO PARA REMOVER MOTORISTAS
				$sendCommandReturn = Command::sendCommandDrivers($inputRemove, 'delete');
				if(!$sendCommandReturn){
					return E_DATACOMMANDASSOCFAILED;
				}
			}
			// FIM			
			$result = true;
			if($unitAssoc != ''){
				$inputAdd = array();
				for ($i = 0; $i < sizeof($unitAssocArr); $i++) {
					$unit = $unitAssocArr[$i];
					if($type == "drivers"){
						$sqlAddData = " UPDATE driver_tracked_unit SET status = 1, driver_id = $unit 
										WHERE 
											memory_position = (SELECT MIN(memory_position) FROM driver_tracked_unit WHERE unit_id = $id AND driver_id = 0) 
											AND unit_id = $id 
											AND (SELECT count(driver_id) FROM driver_tracked_unit WHERE driver_id = $unit AND unit_id = $id AND status > 0) = 0
										RETURNING unit_id, driver_id, memory_position; ";
					}else{
						$sqlAddData = " UPDATE driver_tracked_unit SET status = 1, driver_id = $id
										WHERE 
											memory_position = (SELECT MIN(memory_position) FROM driver_tracked_unit WHERE unit_id = $unit AND driver_id = 0) 
											AND unit_id = $unit 
											AND (SELECT count(driver_id) FROM driver_tracked_unit WHERE driver_id = $id AND unit_id = $unit AND status > 0) = 0
										RETURNING unit_id, driver_id, memory_position; ";
					}		

					$db -> addOption("table", $sqlAddData);
					$dataToAdd = $db -> update(false, true, 1);

					if (is_object($dataToAdd)) {	
						while ($value = $dataToAdd -> fetch()) {
							$inputAdd[] = array('unit_id' => $value -> unit_id, 'driver_id' => $value -> driver_id, 'memory' => $value -> memory_position);
						}	
					}
				}
				if(sizeof($inputAdd) > 0){
					//CHAMA AQUI O METODO PARA ADICIONAR MOTORISTAS
					$sendCommandReturn = Command::sendCommandDrivers($inputAdd, 'add');
					if(!$sendCommandReturn){
						return E_DATACOMMANDASSOCFAILED;
					}
				}
			}
			
			if($type == "drivers"){
				$unit = new UnitController();
				$unitAssoc = $unit -> getDriverAssociatesAction($id);
			}else{
				$unitAssoc = $this -> getUnitiesAssociatesAction($id);				
			}

			$db->close();

			if(is_bool($result) && $result === true) {
				return json_encode($unitAssoc);
			} else {
				return E_DRIVERASSOCFAILED;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function deleteAction($obj) {
		try {

			$db = new DatabaseHandler();

			$db -> addOption("table", "driver_tracked_unit");
			$db -> addOption("where", array("driver_id = :id"));
			$db -> addOption("param", array(":id" => $obj -> id));
			$deleteResult = $db -> delete();

			$db -> addOption("table", "driver");
			$db -> addOption("fields", array("status = :status"));
			$db -> addOption("where", array("id = :id"));
			$db -> addOption("param", array(":id" => $obj -> id,
											":status" => $obj -> status));
			$result = $db -> update();

			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Motorista removido com sucesso.');
			} else {
				return E_DRIVERDELETEFAILED;
			}

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function listFunctionAction() {
		$account_id = $_SESSION['user'] -> getAccount();

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT 
											id, name
										FROM 
											driver_function
										WHERE 
											status = 1
											AND account_id = $account_id 
										ORDER BY name");
			
			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	
			}

			$return = array("functioninfo" => $result);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function searchDriverAction($obj) {
		try {		
			$user_id = $_SESSION['user'] -> getUserId();
			$accountId 	= $_SESSION['user']	-> getAccount();
			$name = $obj -> name;

			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											d.*, 
											gp.name as group_name
										FROM 
											driver d
											INNER JOIN \"group\" gp ON d.group_id = gp.id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = gp.id AND usgrac.user_id = $user_id
										WHERE 
											d.status = 1
											AND d.account_id = $accountId
											AND d.name ilike '%$name%'
										ORDER BY d.name");
			
			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			
			if(is_object($sqlResult)) {
				$result = array();
				while ($row = $sqlResult->fetch()) {
					$result[] = $row;
				}
			} else if($sqlResult === false) {
				$result = [];
			}
			
			return json_encode($result);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
}
