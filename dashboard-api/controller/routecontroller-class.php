<?php
Class RouteController extends defaultController {
	
	public function indexAction($infos = null) {
		$widget = new widgetController();
		$unit = new UnitController();
		$return = array();

		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());
		$return['groupinfo'] = $unit -> listGroupAction();

		return json_encode($return);
	}

	public function GetDataSMSAction($dataToken = null) {
		$poi = new PoiController();
		$map = new MapController();
		$return = array();

		$return['listPOI'] = $poi -> listAction($dataToken);
		$return['listUnit'] = $map -> loadDataAction($dataToken);

		return json_encode($return);
	}

	public function getByIdAction($obj=null) {
		$user_id = $obj->user;
		$route_id = $obj->route_id;
		$poi_id = $obj->poi_id;

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT 
											rt.id,
											rt.name,
											rt.color,
											rt.description,
											rt.speed,
											poi.name as poi_name,
											poi.latitude,
											poi.longitude
										FROM route rt
											INNER JOIN route_poi rtp ON rt.id = rtp.route_id
											INNER JOIN poi ON rtp.poi_id = poi.id AND poi.status = 1
										WHERE 
											rt.status = 1
											AND rt.id = $route_id
										ORDER BY
											rtp.ordem");
			
			$resultRoute = $db -> select(false, 0, true);
			$db->close();
			$result = array();
			if (is_object($resultRoute)) {	
				while ($route = $resultRoute -> fetch()) {
					$result[] = $route;
				}	
				return json_encode(array('data' => $result, 'message' => null));
			} else {
				return json_encode(array('data' => false, 'message' => 'Nenhum registro encontrado.'));
			}
		} catch(Exception $e) {
			return json_encode(array('data' => false, 'message' => $e->getMessage()));
		}
	}

	public function getFirstUnitRouteAction($obj=null) {
		$user_id = $obj->user;
		$route_id = $obj->route_id;
		$poi_id = $obj->poi_id;

		try {
			$db = new DatabaseHandler();
			$sql = "SELECT 
						rp.poi_id, rp.ordem, p.name, p.latitude, p.longitude, coalesce(rlp.unit_id, -1) as unit_id, rlp.date_time
					FROM 
						route_poi rp
						LEFT JOIN (	
							SELECT * FROM route_last_poi WHERE route_id = $route_id
							UNION 
							SELECT $route_id, 0, $poi_id, now(), 0 ) rlp 
						ON rp.poi_id = rlp.poi_id AND rp.route_id = rlp.route_id
						INNER JOIN poi p ON p.id = rp.poi_id AND p.status = 1
					WHERE 
						rp.route_id = $route_id
					ORDER BY 
						rp.ordem, rlp.unit_id DESC";

			$db -> addOption("table", $sql);
			
			$resultRoute = $db -> select(false, 0, true);
			$db->close();
			$result = array();
			if (is_object($resultRoute)) {	
				while ($route = $resultRoute -> fetch()) {
					$result[] = $route;
				}						
				return json_encode(array('data' => $result, 'message' => null));
			} else {
				return json_encode(array('data' => false, 'message' => 'Nenhum registro encontrado'));
			}
		} catch(Exception $e) {
			return json_encode(array('data' => false, 'message' => $e->getMessage()));
		}
	}

}
