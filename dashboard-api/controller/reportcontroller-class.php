<?php 
class reportController extends defaultController
{
	public function indexAction($infos=null) {
		date_default_timezone_set("America/Sao_Paulo");
		$widget = new widgetController();
		$group = new clientController();
		$unit = new unitController();
		$return = array();

		$return['groupinfo'] = $unit -> listGroupAction();
		$return['subgroupinfo'] = $unit -> listSubGroupAction();		
		$return['events'] = $this -> listEventsAction();
		$return['date'] = date('d/m/Y');
		$return['hour'] = date('H:i');
		
		return json_encode($return);	
	}
	
	public function searchAction($obj) {		
		try {			
			if(!isset($_SESSION)) {
				session_start();
			}
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;
			$user_id = $_SESSION['user'] -> getUserId();
			
			//controle de consulta
			$limit = '';
			$orderby = ' devst.local_time DESC, tu.label ';
			if( $obj->limit != null && $obj->limit != '' ){
				$limit = " LIMIT 1000 OFFSET ".$obj->limit;
				$orderby = ' devst.local_time DESC, tu.label ';
			}

			//pegando filtro de data/hora
			$start_date = $this->formatDateTimeAction($obj->start_day, $obj->start_hour, '/'); 
			$end_date = $this->formatDateTimeAction($obj->end_day, $obj->end_hour, '/');
			$andDate = " AND (devst.local_time BETWEEN '$start_date' AND '$end_date') ";
			//Velocidade
			$andSpeed = " AND devst.speed >= $obj->start_km ";

			//unidades
			$andUnits = '';
			$units = '0';
			foreach ($obj->units as $unit) {
				$units .= ','.$unit;
			}
			if( ($units != '0,0') && ($units != '0') ){
				$andUnits = " AND tu.id IN ($units) ";
			}
			//Eventos
			$andEvents = '';
			$events = '0';
			foreach ($obj->events as $event) {
				$events .= ','.$event;
			}
			if( ($events != '0,0') && ($events != '0') ){
				$andEvents = " AND eve.id IN ($events) ";
			}

			$db = new DatabaseHandler();			
			$db -> addOption("table", 	"SELECT 
											tu.id, 
											devst.id as devst_id, 
											tu.label, 
											CASE 
												WHEN (to_char(devst.local_time, 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(devst.local_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(devst.local_time, 'DD/MM/YY') 
											END	AS data, 
											to_char(devst.local_time, 'DD/MM/YY HH24:MI') as full_date, 
											to_char(devst.local_time, 'DD/MM/YY') as data_report, 
											to_char(devst.local_time, 'HH24:MI') as hora_min, 
											devst.local_time,
											devst.ignition,
											devst.longitude as longitude, 
											devst.latitude as latitude, 
											devst.speed as velocidade,
											coalesce(devst.address,'') as full_address,
											eve.name as \"event\"
										FROM
											tracked_unit as tu
											INNER JOIN dev_status_30 as devst ON tu.id = devst.unit_id
											INNER JOIN tracker_event as eve ON eve.id = devst.tracker_event_id 
											INNER JOIN subgroup as sbgr ON tu.subgroup_id = sbgr.id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = sbgr.group_id AND sbgr.id = usgrac.subgroup_id AND usgrac.user_id = $user_id
										WHERE 
											tu.account_id = $account_id
											$andUnits
											$andDate
											$andSpeed
											$andEvents
										ORDER BY 
											$orderby
										$limit");

			$resultSearch = $db -> select(false, 0, true);
			$db->close();
			
			if(is_object($resultSearch)) {
				$result = array();
				while($search = $resultSearch->fetch()) {
					$result[] = $search;
				}				
				return json_encode($result);
			} else {
				if($resultSearch === false) {
					return json_encode(array('text' => 'Não foram encontrados registro para este filtro.'));
				} else {
					return E_UNABLETOGETPARENTWIDGETS;
				}
			}
		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function listEventsAction() {
	 	try {
			$parents = $this->getCategoriesAction();
			$result = array();				
			if(is_array($parents)){
				foreach ($parents as $parent){
					$parent->children = $this->getEventsAction($parent->parent_id);
					if($parent->children !== false) {
						$result[] = $parent;
					}
				}
			
				$tree = array("text" => "Eventos");
				$tree["children"] = $result;
				$json = json_encode($tree);
				//Trocando nome dos campos de arrays para funcionar no plugin jsTree
				$search = array("name");
				$replace = array("text");
				$return = str_replace($search, $replace, $json);
				$return = json_encode($return);
				return json_decode($return);
			} else {
				return E_UNABLETOLISTWIDGETS; //TODO: REGISTRAR ERRO
			}
		
		} catch(Exception $e) {
			return E_INTERNAL;
		}	
	}

	public function getSpeedAction($obj) {		
		try {			
			if(!isset($_SESSION)) {
				session_start();
			}
			
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;
			
			$db = new DatabaseHandler();
			$db->addOption("fields", array("DISTINCT tu.label as placa", 
											"devst.speed as velocidade",
											"to_char(devst.local_time , 'DD/MM/YYYY') as data", 
											"to_char(devst.local_time, 'HH24:MI:SS') as hora", 
											"to_char(devst.local_time, 'HH24:MI') as hora_min",
											"devst.local_time",));
			$db->addOption("table", "tracked_unit as tu");
			$db->addOption("joinTable", array(
												"account as acc", 
												"dev_status_30 as devst")
											);
			
			$db->addOption("joinTableOn", array(
												"acc.id = tu.account_id",
												"tu.id = devst.unit_id"
			)
												);
			$db->addOption("joinTableType", array(
													"INNER JOIN",
													"INNER JOIN")
												);
			$db->addOption("where", array("tu.account_id = :account_id", "tu.id = :unit_id", "devst.local_time::text like '%'||now()::date||'%'"));
			$db->addOption("logicalOperator", array("AND", "AND"));
			$db->addOption("param", array(":account_id" => $account_id, ":unit_id" => $obj->units));
			$db->addOption("orderBy", array("devst.local_time DESC"));
			$resultSearch = $db->select(true, 1);
			$db->close();
			
			if(is_object($resultSearch)) {
				$result = array();
				while($search = $resultSearch->fetch()) {
					$result[] = $search;
				}				
				return json_encode($result);
			} else {
				if($resultSearch === false) {
					return json_encode(array('text' => 'Não há registro para este filtro.'));
				} else {
					return E_UNABLETOGETPARENTWIDGETS; //TODO: REGISTRAR ERRO
				}
			}
		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function buildReportAction($obj) {
		try {
			if(!isset($_SESSION)) {
				session_start();
			}
			$account_id = $_SESSION['user'] -> getAccount();
			
			// DB table to use
			$table = 'tracked_unit';
			
			// Table's primary key
			$primaryKey = 'devst.id';
			
			// Array of database columns which should be read and sent back to DataTables.
			// The `db` parameter represents the column name in the database, while the `dt`
			// parameter represents the DataTables column identifier. In this case simple
			// indexes
			$columns = array(
					array( 'db' => 'tu.label as placa', 'dt' => 0, 'field' => 'placa' ),
					array( 'db' => "CASE WHEN (to_char(devst.local_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje' WHEN (to_char(devst.local_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' ELSE to_char(devst.local_time, 'DD/MM/YY') END as data",  'dt' => 1, 'field' => 'data'),
					array( 'db' => "to_char(devst.local_time, 'HH24:MI') as hora",  'dt' => 2, 'field' => 'hora' ),
					array( 'db' => "devst.id||';'||devst.longitude||';'||devst.latitude as span_address",  'dt' => 3, 'field' => 'span_address'),
					array( 'db' => 'devst.speed',  'dt' => 4, 'field' => 'speed'),
					array( 'db' => 'devst.address',  'dt' => 5, 'field' => 'address'),
					array( 'db' => 'eve.name as event',  'dt' => 6, 'field' => 'event'),
					array( 'db' => 'devst.latitude',  'dt' => 7, 'field' => 'latitude'),
					array( 'db' => 'devst.longitude',  'dt' => 8, 'field' => 'longitude'),
					array( 'db' => "to_char(devst.local_time, 'DD/MM/YYYY') as local_time",  'dt' => 9, 'field' => 'local_time')
			);
			
			// SQL server connection information
			
			$join = 'FROM tracked_unit as tu '.
					'INNER JOIN account as acc ON(acc.id = tu.account_id) '.
					'INNER JOIN dev_status_30 as devst ON(tu.id = devst.unit_id) '.
					'INNER JOIN unit_category as ucat ON(tu.unit_category_id = ucat.id) '.
					'INNER JOIN unit_type as utype ON(ucat.unit_type_id = utype.id) '.
					'INNER JOIN tracked_unit_device as tud ON(tu.id = tud.tracked_unit_id) '.
					'INNER JOIN tracker_event as eve ON(eve.id = devst.tracker_event_id) '.
					'INNER JOIN subgroup as sbgr ON(tu.subgroup_id = sbgr.id)';
			
			$ands = array();
			$numWhere = array();

			//pegando filtro de conta logada
			$numWhere[] = 'tu.account_id = '.$account_id;

			//pegando filtro de data/hora
			$start_date = $this->formatDateTimeAction($obj->start_day, $obj->start_hour, '/'); 
			$end_date = $this->formatDateTimeAction($obj->end_day, $obj->end_hour, '/');
			
			$ands[] 	= 'AND';
			$numWhere[] = "(devst.local_time BETWEEN '$start_date' AND '$end_date')";

			//pegando filtro de velocidade
			if($obj->start_km != '' && $obj->end_km == '') {
				$ands[] = 'AND';
				$numWhere[] = "devst.speed >= $obj->start_km";
			} else if($obj->start_km == '' && $obj->end_km != '') {
				$ands[] = 'AND';
				$numWhere[] = "devst.speed <= $obj->end_km";
			} else if($obj->start_km != '' && $obj->end_km != '') {
				$ands[] = 'AND';
				$numWhere[] = "(devst.speed >= $obj->start_km AND devst.speed <= $obj->end_km)";
			}
				
			//pegando filtro de eventos selecionados
			if(!empty($obj->events)) {
				$ands[] = 'AND';
				$ids='';
				$last = count($obj->events);
				$i = 0;
				$and = ', ';
				$in = '(';
			
				foreach ($obj->events as $id) {
					$i++;
					if($i == $last) {
						$and = ')';
					}
					$ids = $in.$ids."'".$id."'".$and;
					$in = '';
				}
			
				$numWhere[] = 'eve.id in '.$ids;
			}
				
			//pegando filtro de unidades selecionadas
			if($obj->units[0] != '0') {
				$ands[] = 'AND';
				$ids='';
				$last = count($obj->units);
				$i = 0;
				$and = ', ';
				$in = '(';
			
				foreach ($obj->units as $id) {
					$i++;
					if($i == $last) {
						$and = ')';
					}
					$ids = $in.$ids."'".$id."'".$and;
					$in = '';
				}
			
				$numWhere[] = 'tu.id in '.$ids;
			}
	
			//concatenando wheres
			$ind = 0;
			$whereSSP = '';
			$e = ' AND ';
			$ultimo = count($numWhere);
			foreach ($numWhere as $where) {
				$ind++;
				if($ind == $ultimo) {
					$e = '';
				}
				$whereSSP.=$where.$e;
			}

			$orderBy = ' ORDER BY devst.unit_id DESC, devst.local_time DESC ';
			
			echo json_encode(
				SSP::buildQuerySimple( $_POST, $table, $primaryKey, $columns, $join, $whereSSP, $orderBy  )
			);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getConDayAction($obj=null) {
		try {
			$param = false;
			$db = new DatabaseHandler();
			$db->addOption("fields", array(	"cd.unit_id as id",
											"cd.id as con_day_id",
											"cd.time_moving as time_move", 
											"cd.time_stop_ign as time_stop_ign", 
											"cd.time_stop as time_stop"));
			$db->addOption("table", "con_day as cd");
			if($obj === null) {
				$db->addOption("where", array("cd.cday::text = now()::date::text"));		
			} else {
				$db->addOption("where", array(	"cd.unit_id = :unit_id", 
												"cd.cday::text = now()::date::text"));	
				$db->addOption("logicalOperator", array("AND"));		
				$db->addOption("param", array(":unit_id" => $obj->unit_id));
				$param = true;
			}

			$resultCds = $db->select($param);
			$db->close();
			if(is_object($resultCds)) {
				$result = array();
				while($days = $resultCds->fetch()) {
					$result[] = $days;
				}
				return json_encode($result);
			} else {
				return 'N�o existem dados no per�odo';
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getConStopAction($obj) {
		try {
			
			$db = new DatabaseHandler();

			$db->addOption("fields", array(	"cs.id as con_stop_id",
											"cs.unit_id",
											"tu.label as placa",
											"CASE 	WHEN (to_char(cs.initial_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje' 
												WHEN (to_char(cs.initial_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cs.initial_time, 'DD/MM/YY') END	AS data_ini",
											"to_char(cs.initial_time, 'HH24:MI:SS') as hora_ini", 
											"to_char(cs.initial_time, 'HH24:MI') as hora_ini_min", 
											"cs.initial_address",  
											"cs.initial_lat",  
											"cs.initial_lon", 
											"CASE 	WHEN (to_char(cs.final_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje' 
												WHEN (to_char(cs.final_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cs.final_time, 'DD/MM/YY') END	AS data_final",
											"to_char(cs.final_time, 'HH24:MI:SS') as hora_final",
											"to_char(cs.final_time, 'HH24:MI') as hora_final_min",
											"cs.final_address", 
											"cs.final_lat", 
											"cs.final_lon", 
											"cs.total_time", 
											"func_icon_draw(ucat.icon, sgrp.color, 0, 0, 0, 0) as iconHtml"));
			$db->addOption("table", "con_stop as cs");
			$db->addOption("joinTable", array("tracked_unit tu", '"group" grp', 'subgroup sgrp', 'unit_category ucat'));
			$db->addOption("joinTableOn", array("tu.id = cs.unit_id", "grp.id = cs.group_id", "sgrp.id = cs.subgroup_id" , "tu.unit_category_id = ucat.id" ));
			$db->addOption("joinTableType", array("INNER JOIN", "INNER JOIN", "INNER JOIN", "INNER JOIN"));
			$db->addOption("where", array(	"cs.move_stop = 0", 
											"cs.unit_id = :unit_id", 
											"to_char(cs.final_time, 'DD/MM/YYYY') = to_char(to_date('$obj->cday','YYYY-MM-DD'),'DD/MM/YYYY')"));	
			$db->addOption("logicalOperator", array("AND", "AND"));		
			$db->addOption("param", array(":unit_id" => $obj->unit_id));
			$db->addOption("orderBy", array("cs.initial_time"));
			$db->addOption("orderBySort", array("desc"));	
			$resultCds = $db->select(true);
			$db->close();
			
			if(is_object($resultCds)) {
				$result = array();
				while($days = $resultCds->fetch()) {
					$result[] = $days;
				}
				return json_encode($result);
			} else {
				return json_encode('N�o existem dados no per�odo');
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	private function getCategoriesAction() {
		try {		
			$accountId 	= $_SESSION['user']	-> getAccount();
			
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT cat.id as parent_id, initcap(cat.name) as name 
									FROM event_type as cat 
										INNER JOIN account_event_type aet ON aet.event_type_id = cat.id
									WHERE aet.account_id = $accountId
									ORDER BY cat.name");

			$resultCats = $db -> select(false, 0, true);
			$db->close();
				
			if(is_object($resultCats)) {
				$result = array();
				while($cats = $resultCats->fetch()) {
					$result[] = $cats;
				}
				return $result;
			} else {
				return E_UNABLETOGETPARENTWIDGETS; //TODO: REGISTRAR ERRO
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	private function getEventsAction($parent) {
		try {
			if($parent) {	
				$db = new DatabaseHandler();
				$db->addOption("fields", array("e.id", "initcap(e.name) as name"));
				$db->addOption("table", "tracker_event as e");
				$db->addOption("where", array("e.event_type_id = :cat"));
				$db->addOption("param", array(":cat" => $parent));
				$db->addOption("orderBy",array("e.name"));
				$resultEvents = $db->select(true, 1);
				$db->close();
				
				if($resultEvents == false) {
					return false;
				}
	
				if(!is_object($resultEvents)) {
					return E_WIDGETSNOTFOUND; //TODO: REGISTRAR ERRO
				}
	
				$result = array();
				while($widget = $resultEvents->fetch()) {
					$result[] = $widget;
				}
	
				return $result;
			} else {
				return false;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

}