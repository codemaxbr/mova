<?php
Class DeviceController extends defaultController {
	
	public function indexAction($infos = null) {
		$widget = new widgetController();
		$return = array();
		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());
		$return['deviceList'] = $this -> listDevicesAction();
		$return['manufacturerinfo'] = $this -> listManufacturerAction();

		return json_encode($return);
	}

	public function getDeviceBySerial($deviceSerial) {
		try {

			$db = new DatabaseHandler();
			$db -> addOption("table", "device");
			$db -> addOption("fields", array("*"));
			$db -> addOption("where", array("identifier = :identifier "));
			$db -> addOption("param", array(":identifier" => $deviceSerial));
			$device = $db -> select();
			$db->close();

			if(is_object($device)) {
				$device = $device -> fetch();
				return $device -> id;
			} else {
				return false;
			}
		} catch(Exception $e) {
			return false;
		}
	}

	public function listDevicesAction($device_id=null) {
		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "device dev");
			$db -> addOption("fields", array(
												"distinct dev.id as device_id", 
												"dm.id as model_id", 
												"dmanu.id as manufacturer_id", 
												"dm.name as model_name", 
												"ac.name as account_name", 
												"i_as.tag as tag", 
												"dev.*"
											));
			$db -> addOption("joinTable", array("device_model dm", 
												"device_manufacturer dmanu", 
												"account ac", 
												"intranet.asset i_as"
											));
			$db -> addOption("joinTableOn", array(
													"dm.id = dev.device_model_id", 
													"dmanu.id = dm.device_manufacturer_id", 
													"dev.account_id = ac.id", 
													"dev.asset = i_as.tag AND i_as.tag is not null"));	
			$db -> addOption("joinTableType", array("INNER JOIN", "INNER JOIN", "INNER JOIN", "LEFT JOIN"));			
				
			if(isset($device_id)) {
				$db -> addOption("where", array("dev.id = :id", "dev.status = 1","dev.account_id = :account_id"));
				$db -> addOption("logicalOperator", array("AND","AND"));		
				$db -> addOption("param", array(":id" => $device_id,":account_id" => $_SESSION['user'] -> getAccount()));
			} else {
				$db -> addOption("where", array("dev.status = 1","account_id = :account_id"));
				$db -> addOption("logicalOperator", array("AND"));	
				$db -> addOption("param", array(":account_id" => $_SESSION['user'] -> getAccount()));
			}
			$resultDevice = $db -> select(true);
			$db->close();
		
			$result = array();
			
			if(is_string($resultDevice)) {
				return E_UNABLETOLOADDEVICES;
			}
			
			if (is_object($resultDevice)) {	
				while ($device = $resultDevice -> fetch()) {
					$result[] = $device;
				}						
				if(isset($device_id)) {				
					$return = $result;										
				} else {
					$return = array("data" => $result);						
				}
				return json_encode($return);
			} else {
				$return = array("data" => $result);	
				return json_encode($return);
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function createEditFormDeviceAction($data) {
		try {
			$device = $this -> listDevicesAction($data);	
			if($device !== false) {
				return $device;
			} else {
				return E_DEVICENOTFOUND;
			}
		} catch(Exception $ex) {
			//TODO Log de erros	
			return E_INTERNAL;
		}
	}
	
	public function loadDeviceModelAction() {
		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "device_model");
			$db -> addOption("fields", array("*"));

			$resultDevice = $db -> select(false);
			$db->close();

			if (is_object($resultDevice)) {

				$result = array();
				while ($device = $resultDevice -> fetch()) {
					$result[] = $device;
				}
				$return = array("data" => $result);
				return json_encode($return);

			} else {
				return false;
			}
			
		} catch(Exception $e) {
			//TODO Log de erros
			return E_UNABLETOLOADDEVICEMODEL;
		}
	}
	
	public function addAction($obj) {
		try {	
			if($obj->asset == '') {
				$obj -> asset = null;
			}
			
			//verificando se ja nao existe este dispositivo com mesmo identificador para este modelo
			if($this->checkDeviceExistInModelAction($obj->identifier, $obj->model) !== false) {
				return E_DEVICEINMODELEXITS;
			}

			$db = new DatabaseHandler();
			$db -> addOption("table", "device");
			$db -> addOption("fields", array("identifier","asset","internal_id","device_model_id","account_id"));
			$db -> addOption("param", array(":identifier" => $obj -> identifier, 
											":asset" => $obj -> asset, 
											":internal_id" => $obj -> internal_id, 
											":device_model_id" => $obj -> model, 
											":account_id" => $obj -> account_id));
			$result = $db -> insert();
			$db->close();

			if(is_bool($result) && $result === true) {
				return json_encode('Dispositivo cadastrado com sucesso.');
			} else {
				return E_DEVICEEXISTS;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function editAction($obj) {
		try {
			if($obj->asset == '') {
				$obj -> asset = null;
			}
			
			//verificando se ja nao existe este dispositivo com mesmo identificador para este modelo			
			if($this->checkDeviceExistInModelAction($obj->identifier, $obj->model, $obj->device_id) !== false) {
				return E_DEVICEINMODELEXITS;
			}

			$db = new DatabaseHandler();
			$db -> addOption("table", "device");
			$db -> addOption("fields", array("device_model_id = :device_model_id",
											"identifier = :identifier",
											"asset = :asset",
											"internal_id = :internal_id",
											"account_id = :account_id"));
			$db -> addOption("where", array("id = :id"));
			$db -> addOption("param", array(":id" => $obj -> device_id,
											":identifier" => $obj -> identifier, 
											":asset" => $obj -> asset, 
											":internal_id" => $obj -> internal_id, 
											":device_model_id" => $obj -> model, 
											":account_id" => $obj -> account_id));
			$result = $db -> update();
			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Dispositivo alterado com sucesso.');
			} else {
				return E_DEVICEEXISTS;
			}
			
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function deletedeviceAction($obj) {
		try {

			$db = new DatabaseHandler();

			$db -> addOption("table", "tracked_unit_device tud");
			$db -> addOption("fields", array("tud.id, tu.label"));
			$db -> addOption("joinTable", array("tracked_unit tu"));	
			$db -> addOption("joinTableType", array("INNER JOIN"));
			$db -> addOption("joinTableOn", array("tud.tracked_unit_id = tu.id"));
			$db -> addOption("where", array("tud.device_id = :id", "tud.status = 1"));
			$db -> addOption("logicalOperator", array("AND"));		
			$db -> addOption("param", array(":id" => $obj -> id));

			$select = $db -> select(true);

			if ($select) {
				$trackedUnit = $select -> fetch();		
				return E_UNABLETODELETEDEVICEBYUNIT . strtoupper($trackedUnit -> label);
			} else {
				$db -> addOption("table", "device");
				$db -> addOption("fields", array("status = :status"));
				$db -> addOption("where", array("id = :id"));
				$db -> addOption("param", array(":id" => $obj -> id,
												":status" => $obj -> status));
				$result = $db -> update();
				
				if(is_bool($result) && $result === true) {
					return json_encode('Dispositivo removido com sucesso.');
				} else {
					return E_UNABLETODELETEDEVICE;
				}	
			}
			$db->close();
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function listManufacturerAction() {
		try {
			$db = new DatabaseHandler();
			$db -> addOption("fields", array("id", "name"));
			$db -> addOption("table", 'device_manufacturer');
			$db -> addOption("where", array('status = 1'));
			$resultUnit = $db -> select(false);
			$db->close();
		
			if(is_object($resultUnit)) {
				$result = array();
				while ($units = $resultUnit -> fetch()) {
					$result[] = $units;
				}

				$return = array("manufactures" => $result);		
				return json_encode($return);
			} else {
						
				return E_UNABLETOLOADDEVICES; 		
				
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function listDataTableManufacturerAction() {
		try {
			$db = new DatabaseHandler();
			$db -> addOption("fields", array("id", "name"));
			$db -> addOption("table", 'device_manufacturer');
			$db -> addOption("where", array('status = 1'));
			$resultUnit = $db -> select(false, 1);
			$db->close();
		
			$result = array();
			while ($units = $resultUnit -> fetch()) {
				$result[] = $units;
			}
	
			return json_encode(array("data" => $result));
		} catch(Exception $e) {
			return E_UNABLETOLISTMANUFACTURER;
		}
	}
	
	public function getManufacturerAction($id) {
		$db = new DatabaseHandler();
		$db -> addOption("fields", array("id", "name"));
		$db -> addOption("table", 'device_manufacturer');
		$db -> addOption("where", array('id = :id'));
		$db -> addOption("param", array(':id' => $id));
		$resultUnit = $db -> select(true, 1);
		$db->close();
		
		return json_encode($resultUnit->fetch());
	}
	
	public function addManufacturerAction($obj) {
		try {
			$hasManuf = $this->hasManufactureAction($obj->name);
			if(!$hasManuf) {
				$db = new DatabaseHandler();
				$db->addOption("table", "device_manufacturer");
				$db->addOption("fields", array("name"));
				$db->addOption("param", array(":name" => $obj->name));
				$insert = $db->insert();
				$db->close();
				
				if(is_bool($insert) && $insert === true) {
					return json_encode('Fabricante criado com sucesso.');
				} else {
					return E_UNABLETOADDTYPE;
				}
			} else {
			//TODO REGISTRAR ERRO
				return $hasManuf;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function editManufacturerAction($obj) {
		try {
			$hasManuf = $this->hasManufactureAction($obj->name, $obj->id);
			if(!$hasManuf) {
				$db = new DatabaseHandler();
				$db->addOption("fields", array("name = :name"));
				$db->addOption("table", "device_manufacturer");
				$db->addOption("where", array("id = :id"));
				$db->addOption("param", array(":id" => $obj->id, ":name" => $obj->name));
				$resultUpdate = $db->update();
				$db->close();
	
				if($resultUpdate === true) {
					return json_encode('Fabricante alterado com sucesso.');
				} else {
					return E_UNABLETOUPDATETYPE;
				}
			} else {
			//TODO REGISTRAR ERRO
				return $hasManuf;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	private function hasManufactureAction($name, $id = null) {
		$db = new DatabaseHandler();
		$db -> addOption("fields", array("id", "name"));
		$db -> addOption("table", 'device_manufacturer');
		if($id === null) {
			$db -> addOption("where", array('LOWER(name) = LOWER(:name)', 'status = 1'));
			$db -> addOption("logicalOperator", array('AND'));
			$db -> addOption("param", array(':name' => $name));
		} else {
			$db -> addOption("where", array('id != :id', 'LOWER(name) = LOWER(:name)', 'status = 1'));
			$db -> addOption("logicalOperator", array('AND', "AND"));
			$db -> addOption("param", array(':id' => $id, ':name' => $name));
		}
		$resultUnit = $db -> select(true, 1);
		$db->close();
		
		if($resultUnit !== false) {
			//TODO REGISTRAR ERRO
			return 'Este fabricante já está cadastrado';
		} else {
			return $resultUnit;
		}
	}

	public function listModelAction($manufacturerId) {
		$db = new DatabaseHandler();
		$db -> addOption("fields", array("id", "name"));
		$db -> addOption("table", 'device_model');
		$db -> addOption("where", array("device_manufacturer_id = :manufacturerId","status = 1"));
		$db -> addOption("logicalOperator", array("AND"));
		$db -> addOption("param", array(":manufacturerId" => $manufacturerId));
		$resultUnit = $db -> select(true, 1);
		$db->close();
		
		if(is_object($resultUnit)) {
			$result = array();
			while ($units = $resultUnit->fetch()) {
				$result[] = $units;
			}
				
			return json_encode($result);
		} else {
			echo E_UNABLETOLOADMODELOS;			
		}
	}

	public function listDataTableModelAction() {
		$db = new DatabaseHandler();
		$db -> addOption("fields", array("mod.id", "mod.name", "man.name AS manuf_name", "mod.device_manufacturer_id AS manuf_id"));
		$db -> addOption("table", 'device_model AS mod');
		$db -> addOption("joinTable", array('device_manufacturer AS man'));	
		$db -> addOption("joinTableType", array("INNER JOIN"));
		$db -> addOption("joinTableOn", array("mod.device_manufacturer_id = man.id"));
		$db -> addOption("where", array('mod.status = 1'));
		$resultModel = $db -> select(false, 1);
		$db->close();
		
		if(is_object($resultModel)) {
			$result = array();
			while ($units = $resultModel->fetch()) {
				$result[] = $units;
			}
		
			return json_encode(array("data" => $result));
		} else {
			//TODO REGISTRAR ERRO
			echo E_UNABLETOLOADMODELOS;
		}
	}
	
	public function getModelAction($id) {
		$db = new DatabaseHandler();
		$db -> addOption("fields", array("mod.id", "mod.name", "mod.device_manufacturer_id AS manuf_id", "mod.qtd_driver"));
		$db -> addOption("table", 'device_model AS mod');
		$db -> addOption("where", array('id = :id'));
		$db -> addOption("param", array(':id' => $id));
		$resultModel = $db -> select(true, 1);
		$db->close();
		
		return json_encode($resultModel->fetch());
	}

	public function addModelAction($obj) {
		try {
			$hasModel = $this->hasModelAction($obj->name, $obj->manuf);
			if(!$hasModel) {
				$db = new DatabaseHandler();
				$db->addOption("table", "device_model");
				$db->addOption("fields", array("name", "device_manufacturer_id", "qtd_driver"));
				$db->addOption("param", array(":name" => $obj->name, ":device_manufacturer_id" => $obj->manuf, ":qtd_driver" => $obj->qtd_driver));
				$insert = $db->insert();
				$db->close();
	
				if(is_bool($insert) && $insert === true) {
					return json_encode('Modelo criado com sucesso.');
				} else {
					return 'Não foi possivel criar o modelo';
				}
			} else {
				//TODO REGISTRAR ERRO
				return $hasModel;
			}
	
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function editModelAction($obj) {
		try {
			$hasModel= $this->hasModelAction($obj->name, $obj->manuf, $obj->id);
			if(!$hasModel) {
				$db = new DatabaseHandler();
				$db->addOption("fields", array("name = :name", "qtd_driver = :qtd_driver"));
				$db->addOption("table", "device_model");
				$db->addOption("where", array("id = :id"));
				$db->addOption("param", array(":id" => $obj->id, ":name" => $obj->name, ":qtd_driver" => $obj->qtd_driver));
				$resultUpdate = $db->update();
				$db->close();
	
				if($resultUpdate === true) {
					return json_encode('Modelo alterado com sucesso.');
				} else {
					return E_UNABLETOUPDATETYPE;
				}
			} else {
			//TODO REGISTRAR ERRO
				return $hasModel;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	private function hasModelAction($name, $manuf, $id = null) {
		$db = new DatabaseHandler();
		$db -> addOption("fields", array("id", "name", "device_manufacturer_id"));
		$db -> addOption("table", 'device_model');
		if($id === null) {
			$db -> addOption("where", array('status = 1', 'LOWER(name) = LOWER(:name)', 'device_manufacturer_id = :manuf'));
			$db -> addOption("logicalOperator", array('AND', 'AND'));
			$db -> addOption("param", array(':name' => $name, ':manuf' => $manuf));
		} else {
			$db -> addOption("where", array('status = 1', 'id != :id', 'device_manufacturer_id = :manuf', 'LOWER(name) = LOWER(:name)'));
			$db -> addOption("logicalOperator", array('AND', 'AND', 'AND'));
			$db -> addOption("param", array(':id' => $id, ':manuf' => $manuf, ':name' => $name));
		}
		$resultUnit = $db -> select(true, 1);
		$db->close();
		
		if($resultUnit !== false) {
			//TODO REGISTRAR ERRO
			return 'Este modelo já está cadastrado para este fabricante.';
		} else {
			return $resultUnit;
		}
	
	}

	public function listAssetByTagAction($assetTag) {

		$db = new DatabaseHandler();
		$db -> addOption("fields", array("id", "tag as asset", "identifier", "manufacturer_id", "model_id"));
		$db -> addOption("table", 'intranet.asset');
		$db -> addOption("where", array("tag = :tag"));
		$db -> addOption("param", array(":tag" => $assetTag));
		$resultUnit = $db -> select(true, 1);
		$db->close();
		
		if(is_object($resultUnit)) {
			$result = array();
			while ($units = $resultUnit->fetch()) {
				$result[] = $units;
			}
				
			return json_encode($result);
		} else {
			echo E_UNABLETOLOADASSET;			
		}
	}
	
	public function deleteManufacturerAction($obj) {
		try {
			
			//Verificando se neste fabricante há dependentes (modelos)
			if($this->checkManufDependencyAction($obj->id) !== false) {
				return E_UNABLEDELFABUSE;
			}
			
			
			$modifyDate = date("Y-m-d H:i:s");

			$db = new DatabaseHandler();
			$db->addOption("fields", array(	"status = :status", 
											"modify_date = :modify_date"));
			$db->addOption("table", "device_manufacturer");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(	":id" => $obj->id, 
											":status" => $obj->status, 
											":modify_date" => $modifyDate));
			$resultUpdate = $db->update();
			
			$db->addOption("fields", array(	"status = :status", 
											"modify_date = :modify_date"));
			$db->addOption("table", "device_model");
			$db->addOption("where", array("device_manufacturer_id = :id"));
			$db->addOption("param", array(	":id" => $obj->id, 
											":status" => $obj->status, 
											":modify_date" => $modifyDate));
			$resultModelUpdate = $db->update();
			
			if($resultUpdate === true) {
				return json_encode('Fabricante removido com sucesso.');	
			} else {
				//TODO REGISTRAR ERRO
				return E_UNABLETOUPDATETYPE;
			}	
			$db->close();
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function deleteModelAction($obj) {
		try {			
			//Validando se este modelo tem algum dispositivo como dependentes
			if($this->checkModelDepAction($obj->id) !== false) {
				return E_MODELDEVDEPDEL;
			}
			
			$modifyDate = date("Y-m-d H:i:s");

			$db = new DatabaseHandler();
			$db->addOption("fields", array("status = :status", "modify_date = :modify_date"));
			$db->addOption("table", "device_model");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(":id" => $obj->id, ":status" => $obj->status, ":modify_date" => $modifyDate));
			$resultUpdate = $db->update();
			$db->close();
			
			if($resultUpdate === true) {
				return json_encode('Modelo removido com sucesso.');	
			} else {
				return E_MODELDEVERRORDEL;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	private function checkManufDependencyAction($id) {
		$db = new DatabaseHandler();
		$db->addOption("fields", array("id", "name"));
		$db->addOption("table", 'device_model');
		$db->addOption("where", array("device_manufacturer_id = :id","status = 1"));
		$db->addOption("logicalOperator", array("AND"));
		$db->addOption("param", array(":id" => $id));
		$resultCheck = $db -> select(true, 1);
		$db->close();

		return $resultCheck;
	}
	
	private function checkDeviceExistInModelAction($identifier, $model, $id=false) {
		$db = new DatabaseHandler();
		$db->addOption("table", "device");
		$db->addOption("fields", array("*"));
		if($id === false){
			$db->addOption("where", array("status = 1", "LOWER(identifier) = LOWER(:device)", "device_model_id = :model"));
			$db->addOption("logicalOperator", array("AND", "AND"));
			$db->addOption("param", array(":device" => $identifier, "model" => $model));
		} else {
			$db->addOption("where", array("status = 1", "LOWER(identifier) = LOWER(:device)", "device_model_id = :model", "id != :id" ));
			$db->addOption("logicalOperator", array("AND", "AND", "AND"));
			$db->addOption("param", array(":device" => $identifier, ":model" => $model, ":id" => $id));
		}
		$exist = $db -> select(true);
		$db->close();

		return $exist;
	}
	
	public function printDeviceAction($obj) {
		$content = '<style>
						table { border: 1px solid #999; border-bottom: none; width: 100%; }
						thead { border: 5px solid #000; }
						tbody td { border: 1px solid #ccc; }
					</style>';
		
		$content = $content.'<table><thead"><th>Identificador</th><th>Modelo</th></thead><tbody>';
		
		foreach ($obj as $row) {
			if(empty($row->identifier)){ $row->identifier = ''; }
			if(empty($row->model_name)){ $row->model_name = ''; }
			$content = $content."<tr><td>".$row->identifier."</td><td>".$row->model_name."</td></tr>";
		}
		
		$content = $content.'</tbody></table>';
		
		return $content;
	}

	private function checkModelDepAction($id) {
		$db = new DatabaseHandler();
		$db->addOption("fields", array("*"));
		$db->addOption("table", "device");
		$db->addOption("where", array("status = 1", "device_model_id = :id"));
		$db->addOption("logicalOperator", array("AND"));
		$db->addOption("param", array(":id" => $id));
		$resultModelDep = $db->select(true, 1);
		$db->close();
		
		return $resultModelDep;
	}
}
