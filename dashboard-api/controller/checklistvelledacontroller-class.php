<?php

class checklistvelledaController extends defaultController
{
	public function indexAction($infos=null) {
		$return = array();
		
		return json_encode($return);
	}
	
	public function listAction($param = null) {
		$unit = new UnitController();

		$user_id = $_SESSION['user'] -> getUserId();
		$accountId 	= $_SESSION['user']	-> getAccount();
		
		$typeData = true;
		//group
		$sqlGroupWhere = '';
		if($param != null){
			$groups = '0';
			foreach ($param->group_id as $group) {
				$groups .= ','.$group;
			}
			if( ($groups != '0,0') && ($groups != '0') ){
				$sqlGroupWhere = " AND gp.id IN ($groups) ";
			}

			if(isset($param->typeData)){
				if(!$param->typeData){
					$typeData = false;
				}
			}
		}

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											d.*, 
											gp.name as group_name
										FROM 
											driver d
											INNER JOIN \"group\" gp ON d.group_id = gp.id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = gp.id AND usgrac.user_id = $user_id
										WHERE 
											d.status = 1
											AND d.account_id = $accountId
											$sqlGroupWhere
										ORDER BY d.name");
			
			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	
			}
			if($typeData){
				$return = array("data" => $result);
			}else{
				$return = $result;
			}
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function getByIdAction($id) {
		try {

			$db = new DatabaseHandler();			
			$db->addOption("table", "SELECT
										d.*
										, to_char(d.cnh_validate, 'DD/MM/YYYY') as cnh_validate_format
										, to_char(d.rac_validate, 'DD/MM/YYYY') as rac_validate_format
										, to_char(d.aso_validate, 'DD/MM/YYYY') as aso_validate_format
										, gp.name as group_name
										, df.name as function_name
									FROM  
										driver d
										INNER JOIN \"group\" gp ON d.group_id = gp.id
										LEFT JOIN driver_function df ON d.driver_function_id = df.id
									WHERE  
										d.id = $id");

			$sqlResult = $db -> select(false, 0, true);
			$db->close();

			$unities = $this -> getUnitiesAssociatesAction($id);

			$driver = $sqlResult->fetch();
			return json_encode(array("driver" => $driver, "driverUnit" => $unities));

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addAction($obj) {
		$user = new userController();

		if($this->hasDriverAction($obj->login, $obj->cnh)) {
			return E_DRIVEREXISTS;
		}

		$post_account_id = $obj->account_id;
		$post_name = $obj->name;
		$post_email = $obj->email;
		$post_phone = $obj->phone;
		$post_login = $obj->login;
		$post_auth = $obj->auth;
		//A SENHA DEPENDE DO CAMPO AUTH
		$post_cnh = $obj->cnh;
		$post_cnh_category = $obj->cnh_category;
		$post_cnh_validate = $this->formatDateUS($obj->cnh_validate);
		$post_group_id = $obj->group_id;

		$sqlPasswordField = '';
		$sqlPasswordValue = '';
		if($obj->auth == 2){
			$post_password = str_pad(mt_rand(0, 99999), 5, '0', STR_PAD_LEFT);
			$sqlPasswordField = " password, ";			
			$sqlPasswordValue = " '$post_password', ";		
		}

		$sqlValeField = '';
		$sqlValeValue = '';
		if($obj->dataVale != null){
			$post_passport = $obj->dataVale->passport;
			$post_function = $obj->dataVale->function;
			$post_area = $obj->dataVale->area;
			$post_empresa = $obj->dataVale->empresa;
			$post_gestor = $obj->dataVale->gestor;
			$post_gestor_tel = $obj->dataVale->gestor_tel;
			$post_local = $obj->dataVale->local;
			$post_rac_validate = $this->formatDateUS($obj->dataVale->rac_validate);
			$post_aso_validate = $this->formatDateUS($obj->dataVale->aso_validate);

			$sqlValeField = " , passport, driver_function_id, area, empresa, gestor, gestor_tel, local, rac_validate, aso_validate ";			
			$sqlValeValue = " , '$post_passport', $post_function, '$post_area', '$post_empresa', '$post_gestor', '$post_gestor_tel', '$post_local', '$post_rac_validate', '$post_aso_validate' ";			
		}

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "INSERT INTO driver (
											account_id, 
											name, 
											email, 
											phone, 
											auth, 
											login, 
											$sqlPasswordField
											cnh, 
											cnh_category, 
											cnh_validate, 
											group_id
											$sqlValeField
											) 
									   VALUES 
										(
											'$post_account_id',
											'$post_name',
											'$post_email',
											'$post_phone',
											'$post_auth',
											'$post_login',
											$sqlPasswordValue
											'$post_cnh',
											'$post_cnh_category',
											'$post_cnh_validate',
											'$post_group_id'
											$sqlValeValue
										)");

			$insert = $db -> insert(false, 0, 0, true);

			$lastInsert = $db->lastInsertId('driver_id_seq');
			$db->close();
			
			if(is_bool($insert) && $insert === true) {
				//Envio de e-mail
				if($obj->auth == 2){
					$this->sendEmailAction("Conta Mova - Motorista", "Seu usuário foi criada com sucesso!<br />Abaixo segue seu login e sua senha:<br /><br />Login: ".$post_login."<br />Senha: ".$post_password, $post_email);
				}
				return json_encode(array('id' => $lastInsert, 'message' => "Motorista cadastrado com sucesso"));
			} else {
				return E_DRIVERCREATEFAILED;
			}			
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function editAction($obj) {
		//$user = new userController();

		if($this->hasDriverAction($obj->login, $obj->cnh, $obj->id)) {
			return E_DRIVEREXISTS;
		}

		$id = $obj->id;
		$post_account_id = $obj->account_id;
		$post_name = $obj->name;
		$post_email = $obj->email;
		$post_phone = $obj->phone;
		$post_login = $obj->login;
		$post_auth = $obj->auth;
		//A SENHA DEPENDE DO CAMPO AUTH
		$post_cnh = $obj->cnh;
		$post_cnh_category = $obj->cnh_category;
		$post_cnh_validate = $this->formatDateUS($obj->cnh_validate);
		$post_group_id = $obj->group_id;
		$post_reset_password = $obj->reset_password;

		$sqlPassword = '';
		if(($obj->auth == 2) && ($post_reset_password == "true")){
			$post_password = str_pad(mt_rand(0, 99999), 5, '0', STR_PAD_LEFT);
			$sqlPassword = " password = '$post_password', ";	
		}

		$sqlVale = '';
		if($obj->dataVale != null){
			$post_passport = $obj->dataVale->passport;
			$post_function = $obj->dataVale->function;
			$post_area = $obj->dataVale->area;
			$post_empresa = $obj->dataVale->empresa;
			$post_gestor = $obj->dataVale->gestor;
			$post_gestor_tel = $obj->dataVale->gestor_tel;
			$post_local = $obj->dataVale->local;
			$post_rac_validate = $this->formatDateUS($obj->dataVale->rac_validate);
			$post_aso_validate = $this->formatDateUS($obj->dataVale->aso_validate);

			$sqlVale  = " , passport = '$post_passport', ";
			$sqlVale .= " driver_function_id = $post_function, ";
			$sqlVale .= " area = '$post_area', ";
			$sqlVale .= " empresa = '$post_empresa', ";
			$sqlVale .= " gestor = '$post_gestor', ";
			$sqlVale .= " gestor_tel = '$post_gestor_tel', ";
			$sqlVale .= " local = '$post_local', ";
			$sqlVale .= " rac_validate = '$post_rac_validate', ";
			$sqlVale .= " aso_validate ='$post_aso_validate' ";					
		}

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "UPDATE driver SET 
											account_id = '$post_account_id', 
											name = '$post_name', 
											email = '$post_email', 
											phone = '$post_phone', 
											auth = '$post_auth', 
											login = '$post_login', 
											$sqlPassword
											cnh = '$post_cnh', 
											cnh_category = '$post_cnh_category', 
											cnh_validate = '$post_cnh_validate', 
											group_id = '$post_group_id'
											$sqlVale 
									   WHERE 
										id = $id");

			$result = $db -> update(false, true);
			$db->close();
			
			if(is_bool($result) && $result === true) {
				//Envio de e-mail
				if(($obj->auth == 2) && ($post_reset_password == "true")){	
					$this->sendEmailAction("Conta Mova - Motorista", "Seu usuário foi alterado com sucesso!<br />Abaixo segue seu login e sua senha:<br /><br />Login: ".$post_login."<br />Senha: ".$post_password, $post_email);
				}
				return json_encode(array('id' => $id, 'message' => "Motorista alterado com sucesso"));
			} else {
				return E_DRIVERUPDATEFAILED;
			}			
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function deleteAction($obj) {
		try {

			$db = new DatabaseHandler();

			$db -> addOption("table", "driver_tracked_unit");
			$db -> addOption("where", array("driver_id = :id"));
			$db -> addOption("param", array(":id" => $obj -> id));
			$deleteResult = $db -> delete();

			$db -> addOption("table", "driver");
			$db -> addOption("fields", array("status = :status"));
			$db -> addOption("where", array("id = :id"));
			$db -> addOption("param", array(":id" => $obj -> id,
											":status" => $obj -> status));
			$result = $db -> update();

			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Motorista removido com sucesso.');
			} else {
				return E_DRIVERDELETEFAILED;
			}

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
}
