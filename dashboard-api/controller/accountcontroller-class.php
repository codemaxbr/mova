<?php

class accountController extends defaultController
{
	public function indexAction($infos=null) {
		$widget = new widgetController();
		$report = new reportController();
		$return = array();
		$return['accountTypes'] = $this -> listAccountTypesAction(true);
		$return['widgetTree'] = $widget -> listWidgetsAccountAction();	
		$return['events'] = $report -> listEventsAction();
		return json_encode($return);
	}
	
	public function addAction($obj) {
		try {
			//Validando permissões
			$permsLen = count($obj->perms);
			if($permsLen <= 0 ) {
				return E_INVALIDPERMISSIONLEVEL;
			}
			//Validando eventos
			$eventLen = count($obj->event);
			if($eventLen <= 0 ) {
				return 'Selecione os typos de eventos associados a conta.';
			}
		
			//Validando nome existente
			if($this->checkAccountExistsAction($obj->ac_name, $obj->type) !== false) {
				return E_WIDGETACCOUNTEXISTS;
			}
			
			$db = new DatabaseHandler();
			//Criando nova conta
			$db->addOption("table", "account");
			$db->addOption("fields", array("name", "account_type_id", "driver_manager"));
			$db->addOption("param", array(":name" => $obj->ac_name, ":account_type_id" => $obj->type, ":driver_manager" => $obj->driver_manager));
			$insert = $db->insert();

			if(is_bool($insert) && $insert === true) {
				//Pegando o id da conta criada
				$obj->account_id =  $db->lastInsertId('account_id_seq');
				
				//Adicionando todas novas permissões da conta
				foreach ($obj->perms as $perm) {
					$dados[] = array($obj->account_id, $perm);
				}			
		
				$db->addOption("table", "account_widget");
				$db->addOption("fields", array("account_id", "widget_id"));
				$db->addOption("param", $dados);
				$insert = $db->insert(true,0,1);
				
				//Adicionando todos novos typos de evento da conta
				$account_id = $obj->account_id;
				foreach ($obj->event as $event) {
					$db -> addOption("table", "INSERT INTO account_event_type (account_id, event_type_id) VALUES ($account_id, (SELECT event_type_id FROM tracker_event WHERE id IN ($event)))");
					$insertEvent = $db -> insert(false, 0, 0, true);
				}			
				
				if(is_bool($insert) && $insert === true) {
					$arrayGroups = $obj->client;
					$client = new clientController();
					$gs = array();
					foreach ($arrayGroups as $group) {
						$group->account_id = $obj->account_id;
						$obj->group_id = $client->addAction($group);
						if($obj->group_id === false) {
							return E_UNABLETOCREATEGROUP;
						}
						if(isset($group->children)) {
							$subGroups = $group->children;
							foreach($subGroups as $subGroup) {
								$subGroup->group_id = $obj->group_id;
								$obj->subgroup = $client->addSubGroupAction($subGroup);
								//Verifica se deu erro ao salvar subgroup
								if($obj->subgroup === false) {
									return E_UNABLETOCREATESUBGROUP;
								}
							}
						}
						$gs[] = array('group' => $obj->group_id, 'subgroup' => $obj->subgroup);
					}
					
					$obj->groups = json_decode(json_encode($gs));
			
					//Setando usuário como master
					$obj->master = 1;
					$obj->user_web = 1;
					$obj->user_mobile = 0;
					//Recebe id do user e password criado para o envio do e-mail
					$user = new userController();
					$verifyUser = $user->addAction($obj);
					$delId = array('id' => $obj->account_id, 'status' => 3);
					if(!$this->isJsonAction($verifyUser)) {
						$this->deleteaccountAction($delId);
						return $verifyUser;
					}
					
					$msg = json_encode("A nova conta para acesso ao sistema foi criada e os dados de acesso foram enviados para o e-mail informado.");
				
					return $msg;
				} else {
					$this->deleteaccountAction($delId);
					return E_UNABLETOADDWIDGETS;
				}
				
			}else{
				return E_UNABLETOCREATEACCOUNT;
			}
			
		}	catch (Exception $e){
			return E_INTERNAL;
		}		
	}
	
	public function editAction($obj) {
		try {
			$modifyDate = date("Y-m-d H:i:s");
			$db = new DatabaseHandler();
			//EDITANDO CONTA
			$db->addOption("fields", array(	"name = :name", 
											"account_type_id = :type", 
											"driver_manager = :driver_manager", 
											"status = :status",
											"modify_date = :modify_date"));
			$db->addOption("table", "account");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(	":id" => $obj->account_id, 
											":type" => $obj->type, 
											":driver_manager" => $obj->driver_manager, 
											":name" => $obj->ac_name, 
											":status" => $obj->status,
											":modify_date" => $modifyDate));
			$update = $resultUpdate = $db->update();
			
			//Validando nome existente
			if($this->checkAccountExistsAction($obj->name, $obj->type, $obj->account_id) !== false) {
				return E_WIDGETACCOUNTEXISTS;
			}
			
			if(is_bool($update) && $update === true) {
				//EDITANDO PERMISSÕES DA CONTA
				//Deletando todas as permissões da conta
				$db->addOption("table", "account_widget");
				$db->addOption("where", array("account_id = :id"));
				$db->addOption("param", array(":id" => $obj->account_id));
				$delete = $db->delete();
				
				if(is_bool($delete) && $delete === true){
					$permsLen = count($obj->perms);
					if($permsLen > 0 )
					{
						//Adicionando todas novas permissões da conta
						foreach ($obj->perms as $perm)
						{
							$dados[] = array($obj->account_id, $perm);
						}
			
						$db->addOption("table", "account_widget");
						$db->addOption("fields", array("account_id", "widget_id"));
						$db->addOption("param", $dados);
						$insert = $db->insert(true,0,1);
						
						if(!is_bool($insert) && $insert !== true){
							return E_UNABLETOUPDATEPERMISSIONS;
						}
						
					} else {
						return E_INVALIDPERMISSIONLEVEL;
					}
				
				}else{
					return E_UNABLETODELETEPERMISSIONS;
				}
				
				//EDITANDO TIPOS DE EVENTOS DA CONTA
				//Deletando todos os tipos de eventos da conta
				$db->addOption("table", "account_event_type");
				$db->addOption("where", array("account_id = :id"));
				$db->addOption("param", array(":id" => $obj->account_id));
				$deleteEvent = $db->delete();

				if(is_bool($deleteEvent) && $deleteEvent === true){
					$eventLen = count($obj->event);
					if($eventLen > 0 )
					{
						$account_id = $obj->account_id;
						//Adicionando todas novas permissões da conta
						foreach ($obj->event as $event)
						{
							$db -> addOption("table", "INSERT INTO account_event_type (account_id, event_type_id) VALUES ($account_id, (SELECT event_type_id FROM tracker_event WHERE id IN ($event)))");
							$insertEvent = $db -> insert(false, 0, 0, true);
						}
						
						if(!is_bool($insertEvent) && $insertEvent !== true){
							return 'Erro e tentar alterar os tipos de eventos da conta';
						}
						
					} else {
						return 'Erro e tentar alterar os tipos de eventos da conta';
					}
				
				}else{
					return 'Erro ao tentar remover os tipos de eventos associados a conta.';
				}
					
				//Editando grupos da conta
				if(isset($obj -> client) && count($obj -> client) > 0){
					$arrayGroups = $obj->client;
					$client = new clientController();
					foreach ($arrayGroups as $group) {
						$group->account_id = $obj->account_id;
						$newGroup = intval($group->id);
						//Verifica se está editando ou criando um novo grupo
						if($newGroup == 0) {
							$obj->group_id = $client->addAction($group);
							if($obj->group_id === false) {
								return E_UNABLETOCREATEGROUP;
							}
						} else {
							$obj->group_id = $client->editGroupAction($group);
							if($obj->group_id === false) {
								return E_UNABLETOCREATEGROUP;
							}
						}
						//Verifica se este grupo tem subgrupo
						if(isset($group->children)) {
							$subGroups = $group->children;
							foreach($subGroups as $subGroup) {
								$newSub = intval($subGroup->id);

								$subGroup->group_id = $group->id;
								if($newGroup == 0) {
									$subGroup->group_id = $obj->group_id;
								}
								
								//Verifica se está editando ou criando um novo subgrupo
								if($newSub == 0) {
									$obj->subgroup = $client->addSubGroupAction($subGroup);
									//Verifica se deu erro ao salvar subgroup
									if($obj->subgroup === false) {
										return E_UNABLETOCREATESUBGROUP;
									}
								} else {
									$obj->subgroup = $client->editSubGroupAction($subGroup);
									//Verifica se deu erro ao salvar subgroup
									if($obj->subgroup === false) {
										return E_UNABLETOEDITSUBGROUP;
									}
								}
							}
						}
					}
				}else{
					return E_NOGROUPSELECTED;
				}				
			}else{				
				return E_UNABLETOUPDATEACCOUNT;				
			}			
		 	return json_encode("Conta alterada com sucesso");
			
		} catch(Exception $e) {
			return E_INTERNAL;
		}		
	}
	
	public function listAction() {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("ac.id", "ac.name", "act.name as type", "ac.status", "ac.driver_manager"));
			$db->addOption("table", "account ac");
			$db->addOption("joinTable", array("account_type as act"));
			$db->addOption("joinTableOn", array("ac.account_type_id = act.id"));
			$db->addOption("joinTableType", array("INNER JOIN"));
			$db->addOption("where", array("ac.status = 1"));
			$resultAccount = $db->select(false, 1);
			
			if(is_object($resultAccount)) {
				$result = array();
				while ($contas=$resultAccount->fetch()) {
					$result[] = $contas;
				}

				$db->close();
		
				$return = array("data" => $result);		
				return json_encode($return);
			} else {
				return $return = array("data" => '');
			}
			
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getAccountsAction($id=null) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("*"));
			$db->addOption("table", "account");
			if($id===null) {
				$resultAccount = $db->select(false, 1);
			} else {
				$db->addOption("where", array("id = :id"));
				$db->addOption("param", array(":id" => $id));				
				$resultAccount = $db->select(true, 1);
			}
			
			if(is_object($resultAccount)) {
				$result = array();
				while ($contas=$resultAccount->fetch()) {
					$result[] = $contas;
				}
				$db->close();
				return $result;
			} else {
				return E_UNABLETOLISTACCOUNT;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function createEditFormAction($id) {
	  try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("ac.id", "ac.name", "act.id as type", "ac.status", "u.name as users_name", "u.login as users_login", "ac.driver_manager"));
			$db->addOption("table", "account ac");
			$db->addOption("joinTable", array("account_type as act", "users as u"));
			$db->addOption("joinTableOn", array("ac.account_type_id = act.id", "ac.id = u.account_id AND u.master = 1"));
			$db->addOption("joinTableType", array("INNER JOIN", "INNER JOIN"));
			$db->addOption("where", array("ac.id = :id"));
			$db->addOption("param", array(":id" => $id));
			$resultAccount = $db->select(true, 1);
			
			if(!is_object($resultAccount)) {
				return E_UNABLETOSELECTACCOUNT;	
			}
			$account = $resultAccount->fetch();
			$db->addOption("fields", array("account_id", "widget_id"));
			$db->addOption("table", "account_widget");
			$db->addOption("where", array("account_id = :id"));
			$db->addOption("param", array(":id" => $id));
			$resultSelectPerm = $db->select(true, 1);

			$db->addOption("fields", array("aet.account_id", "te.id"));
			$db->addOption("table", "tracker_event te");
			$db->addOption("joinTable", array("account_event_type aet"));
			$db->addOption("joinTableOn", array("te.event_type_id = aet.event_type_id"));
			$db->addOption("joinTableType", array("INNER JOIN"));
			$db->addOption("where", array("aet.account_id = :id"));
			$db->addOption("param", array(":id" => $id));
			$resultSelectEvent= $db->select(true, 1);
			
			$group = new clientController();
			$myGroups = $group->listTreeFormEditAction($id);
			
			if($myGroups){
				$myGroups = json_decode($myGroups);
			}else{
				return E_UNABLETOLISTACCOUNTGROUP;
			}
					
			$permsAcc = array();
			if(is_object($resultSelectPerm)) {
				while($perms = $resultSelectPerm->fetch()) {
					$permsAcc[] = $perms->widget_id;
				}
			} else {
				return E_UNABLETOLISTACCOUNTPERMISSION;
			}
					
			$eventAcc = array();
			if(is_object($resultSelectEvent)) {
				while($perms = $resultSelectEvent->fetch()) {
					$eventAcc[] = $perms->id;
				}
			} else {
				return E_UNABLETOLISTACCOUNTPERMISSION;
			}

			$db->close();
			$result = array("account" => $account, "perms" => $permsAcc, "event" => $eventAcc, "groups" => $myGroups);
			
			return json_encode($result);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	//Parametro tabela para saber se está sendo requisatado para
	//carregamento da tabela com plugin datatables
	public function listAccountTypesAction($table=false) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("id", "name"));
			$db->addOption("table", "account_type");
			$db->addOption("where", array("status = 1"));
			$resultTypeAcc = $db->select(false, 1);
	
			$result = array();
			
			if(is_object($resultTypeAcc)) {
				while ($types=$resultTypeAcc->fetch()) {
					$result[] = $types;
				}
				
				$return = array('data' => $result);
				$db->close();
				if($table!=false) {
					return json_encode($return);
				} else {
					return $result;
				}
				
			} else {
				return $return = array('data' => '');
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function createEditFormAccountTypeAction($obj) {
		try {	
			$db = new DatabaseHandler();
			$db->addOption("fields", array("id", "name"));
			$db->addOption("table", "account_type");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(":id" => $obj));
			$resultType = $db->select(true, 1);
			$db->close();
			if(is_object($resultType)) {
				return json_encode($resultType->fetch());
			} else {
				return E_UNABLETOSELECTACCOUNTTYPE;				
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function editTypeAction($obj) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("name = :name"));
			$db->addOption("table", "account_type");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(":id" => $obj->id, ":name" => $obj->name));
			$resultUpdate = $db->update();
			$db->close();
			
			if($resultUpdate === true) {
				return json_encode('Tipo de conta alterado com sucesso.');	
			} else {
				return E_UNABLETOUPDATETYPE;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	
	public function deleteaccountTypeAction($obj) {
		try {
			$modifyDate = date("Y-m-d H:i:s");
			
			//Verificando se o tipo de conta tem alguma conta associada a este tipo
			if($this->checkTypeAccDepAction($obj->id) !== false) {
				return E_WIDGETTYPEACCDEP;
			}

			$db = new DatabaseHandler();
			$db->addOption("fields", array(	"status = :status", "modify_date = :modify_date"));
			$db->addOption("table", "account_type");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(	":id" => $obj->id, ":status" => $obj->status, ":modify_date" => $modifyDate));
			$resultUpdate = $db->update();
			$db->close();
			
			if($resultUpdate === true) {
				return json_encode('Tipo de conta removido com sucesso.');	
			} else {
				return E_UNABLETOUPDATETYPE;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addTypeAction($obj) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("table", "account_type");
			$db->addOption("fields", array("name"));
			$db->addOption("param", array(":name" => $obj->name));
			$insert = $db->insert();
			$db->close();
			
			if(is_bool($insert) && $insert === true) {
				return json_encode('Tipo de conta criado com sucesso.');	
			} else {
				return E_UNABLETOADDTYPE;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	// ATUALIZANDO STATUS DA CONTA EXCLUIDO
	public function deleteaccountAction($obj) {
		try {
			if(!is_object($obj)) {
				$obj = json_decode(json_encode($obj), false); //transforma em objeto caso não for
			}
			
			$modifyDate = date("Y-m-d H:i:s");
		
			$db = new DatabaseHandler();
			$db->addOption("fields", array(	"status = :status", "modify_date = :modify_date"));
			$db->addOption("table", "account");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(	":id" => $obj->id, ":status" => $obj->status, ":modify_date" => $modifyDate));
			$resultDelAccount = $db->update();

			$db->addOption("fields", array("status = :status", "modify_date = :modify_date"));
			$db->addOption("table", "users");
			$db->addOption("where", array("account_id = :id"));
			$db->addOption("param", array(":id" => $obj->id, ":status" => $obj->status, ":modify_date" => $modifyDate));
			$resultDelAccountUsers = $db->update();
			$db->close();
			
			//se nao tiver erros
			if($resultDelAccount === true) {
				return json_encode('Conta excluída com sucesso');
			} else {
				return E_UNABLETOUPDATETYPE;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	private function checkAccountExistsAction($name, $typeAcc, $id=false) {
		$db = new DatabaseHandler();
		$db->addOption("fields", array("*"));
		$db->addOption("table", "account");
		if($id === false) {
			$db->addOption("where", array("status = 1", "LOWER(name) = LOWER(:name)", "account_type_id = :type_acc"));
			$db->addOption("logicalOperator", array("AND", "AND"));
			$db->addOption("param", array(":name" => $name, ":type_acc" => $typeAcc));
		} else {
			$db->addOption("where", array("status = 1", "LOWER(name) = LOWER(:name)", "account_type_id = :type_acc", "id != :id"));
			$db->addOption("logicalOperator", array("AND", "AND", "AND"));
			$db->addOption("param", array(":name" => $name, ":type_acc" => $typeAcc, ":id" => $id));
		}
		$resultAccountExists = $db->select(true, 1);
		$db->close();
		
		return $resultAccountExists;
	}

	private function checkTypeAccDepAction($id) {
		$db = new DatabaseHandler();
		$db->addOption("fields", array("*"));
		$db->addOption("table", "account");
		$db->addOption("where", array("status = 1", "account_type_id = :id"));
		$db->addOption("logicalOperator", array("AND"));
		$db->addOption("param", array(":id" => $id));
		$resultAccountDep = $db->select(true, 1);
		$db->close();
		
		return $resultAccountDep;
	}
}
