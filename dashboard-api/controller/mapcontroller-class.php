<?php
class MapController extends defaultController 
{		
	public function loadUnitDataAction() {
		$return = array();
		$return['data'] = json_decode($this -> loadDataAction());
		return json_encode($return);
	}
	
	public function loadGroupsAction($infos = null) {
		
		$client = new clientController();
		$return = array();
		$user_id = $_SESSION['user'] -> getUserId();
		$return['groupinfo'] = $client -> listTreeAccLogAction($user_id);

		return json_encode($return);
	}
	/**
	 * Carrega os dados de todas as unidades do mapa de acordo com as permissoes do usuario
	 */
	public function loadDataAction($id=null) {
		try {
			$joinComplement = false;

			if(isset($id)) {
				if( (isset($id->user)) && ($id->token == "12342612648763598123791829371462835413")) {
					$user_id = $id->user;
				} else if(isset($id)) {
					$joinComplement = " AND devst.unit_id = :unit_id ";
				}
			}else{				
				$user_id = $_SESSION['user'] -> getUserId();
			}
			
			$db = new DatabaseHandler();
			$db->addOption("fields", array("replace(replace(row_to_json(featureCollection)::varchar,'\"[', '[') ,']\"',']') as geoJSON"));
			$db->addOption("table", " (SELECT 'FeatureCollection' As type, array_to_json(array_agg(feature)) As features
										 FROM ( SELECT 'Feature' As type
											  , ST_AsGeoJSON(point(st.longitude, st.latitude)::geometry)::json As geometry
											  , row_to_json(unit) As properties 
											FROM dev_status As st 
										        INNER JOIN 
											(SELECT  devst.id 
													,devst.unit_id
													,label 
													,coalesce(label2,'') as label2
													,devst.latitude
													,devst.longitude
													,devst.speed
													,devst.time_write
													,devst.local_time
													,CONCAT(CASE 
														WHEN (to_char(devst.local_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
														WHEN (to_char(devst.local_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
														ELSE to_char(devst.local_time, 'DD/MM/YY') 
													END	 
													  || ' às '  ||  to_char(devst.local_time, 'HH24:MI:SS') ) as data_hora
													,CONCAT(CASE 
														WHEN (to_char(devst.local_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
														WHEN (to_char(devst.local_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
														ELSE to_char(devst.local_time, 'DD/MM/YY') 
													END	 
													  || ' às '  ||  to_char(devst.local_time, 'HH24:MI:SS') ) as last_transm
													,CONCAT(CASE 
														WHEN (to_char(devst.time_write , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
														WHEN (to_char(devst.time_write, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
														ELSE to_char(devst.time_write, 'DD/MM/YY') 
													END	 
													  || ' às '  ||  to_char(devst.time_write, 'HH24:MI:SS') ) as last_save
													,local_time > now() - interval '3 hours 3 minutes' as online
													,devst.odom as pedometer
													,devst.ignition
													,devst.dev_status_30_id as history_id
													,devst.address as full_address
													,devst.street
													,devst.city
													,CASE devst.satelite WHEN 'TRUE' THEN 'Satelite' ELSE 'GPRS' END AS signal_type
													,devst.gps as gps
													,gr.name as group_name
													,trun.group_id
													,coalesce(devst.driver_name, '') as driver_name
													,unc.name as category
													,evt.name as event
													,dev.identifier as device_id 
													,coalesce(poi.name,'') as point
													,coalesce(ar.name,'') as area
													,coalesce(cond.km, 0) as km
													,coalesce(cond.avg_spd, 0) as avg_spd
													,coalesce(cond.max_spd, 0) as max_spd
													,coalesce(cond.time_moving, 0) as time_moving
													,coalesce(cond.time_moving, 0) as time_move
													,coalesce(cond.time_stop_ign, 0) as time_stop_ign
													,coalesce(cond.time_stop, 0) as time_stop
													,coalesce(cond.time_stop_ign, 0) + coalesce(cond.time_moving, 0) as time_on
													,unc.icon  as iconUnitCSS
													,func_icon_draw(unc.icon, sbgr.color, devst.speed, devst.direction, coalesce(uav.unit_id, 0), 1) as iconHtml
													,devst.direction as iconDirect
													,CASE WHEN devst.altitude ISNULL THEN 0 ELSE devst.altitude END as altitude
													,devst.in1 as in1
													,devst.in2 as in2
													,devst.in3 as in3
													,devst.out1 as out1
													,devst.out2 as out2
													,devst.out3 as out3
													,devst.voltage as voltage 
													,coalesce(devst.poi_distance, 0) as poi_distance 
													,CASE WHEN devst.battery ISNULL THEN 0 ELSE devst.battery END as battery
													,sbgr.name as subgroup_name
													,sbgr.id as subgroup_id
													,utype.name as type_unit
													--,CASE WHEN trun.os_num ISNULL THEN 0 ELSE trun.os_num END as os_num
													,CASE WHEN intra_os.num_os ISNULL THEN 0 ELSE intra_os.num_os END as os
													,CASE WHEN intra_prod.name ISNULL THEN ' ' ELSE intra_prod.name END as intra_prod_id
													,coalesce(devm.name, '') as device_model
									           FROM dev_status devst
											    INNER JOIN tracked_unit trun 
											    	ON devst.unit_id = trun.id 
											    	AND trun.status = 1 
											    	$joinComplement
											    INNER JOIN unit_type utype
											    	ON trun.unit_type_id = utype.id 
											    INNER JOIN \"group\" gr
											    	ON trun.group_id = gr.id 
											    INNER JOIN subgroup sbgr
											    	ON trun.subgroup_id = sbgr.id 
											    INNER JOIN user_group_access usgrac
											    	ON usgrac.group_id = gr.id 
											    	AND usgrac.user_id = :user_id
											    	AND trun.group_id = usgrac.group_id  
											    	AND trun.subgroup_id = usgrac.subgroup_id
											    INNER JOIN tracked_unit_device trudv
											    	ON trun.id = trudv.tracked_unit_id 
											    	AND devst.device_id = trudv.device_id 
											    	AND trudv.status = 1
											    INNER JOIN device dev
											    	ON dev.id = trudv.device_id
											    INNER JOIN device_model devm
											    	ON dev.device_model_id = devm.id
											    INNER JOIN unit_category unc
											    	ON unc.id = trun.unit_category_id
											    LEFT JOIN driver drv
											    	ON devst.driver_id = drv.id 
											    LEFT JOIN cerca ar
											    	ON devst.area_id = ar.id
											    LEFT JOIN poi poi
											    	ON devst.poi_id = poi.id
											    INNER JOIN tracker_event evt
											    	ON evt.id = devst.tracker_event_id 
											    LEFT JOIN con_day cond
											    	ON cond.unit_id = devst.unit_id
											    	AND to_char(cond.cday , 'DD/MM/YYYY') = to_char(devst.local_time , 'DD/MM/YYYY')
											    LEFT JOIN intranet.product intra_prod
											    	ON trun.intranet_product_id = intra_prod.id
											    LEFT JOIN intranet.os intra_os
											    	ON trun.os_id = intra_os.id
											    LEFT JOIN (select distinct unit_id from alarm_violation where status = 1 ) as uav ON uav.unit_id = trun.id ORDER BY devst.local_time DESC
										      ) As unit
										        ON st.unit_id = unit.unit_id AND st.id = unit.id) As feature 
										     )  As featureCollection
										");
			$param = array();
			if($joinComplement) {
				$param[":user_id"] = $user_id;
				$param[":unit_id"] = $id;
			} else {
				$param[":user_id"] = $user_id;
			}						
							
			$db->addOption("param", $param);				
			$data = $db->select(true);
			$db->close();
			
			if(is_object($data)) {
				$json = $data -> fetch();
				return $json -> geojson;
			} else {
				return false;
			}

		} catch(Exception $e) {
			return $e -> getMessage();
		}
	}

	public function loadGroupDataAction() {
		try {		
			$db = new DatabaseHandler();
			$db->addOption("fields", array("group_id","g.name as group_name"));
			$db->addOption("table", "user_group_access uga");
			$db->addOption("joinTableType", array("INNER JOIN"));
			$db->addOption("joinTable", array("\"group\" as g"));
			$db->addOption("joinTableOn", array("g.id = uga.group_id"));
			$db->addOption("where", array("user_id = :user_id"));
			$db->addOption("param", array(":user_id" => $_SESSION['user'] -> getUserId()));
			$select = $db -> select(true);
			$db->close();
			
			if(is_object($select)) {
				while($obj = $select -> fetch()) {
					$result[] = $obj;
				}
				return json_encode($result);
			} else {
				return false;
			}
			
		} catch(Exception $e) {
			return $e -> getMessage();
		}
	}
	
	public function listAction() {
		$ar = array();
		return json_encode($ar['data']);
	}
	
	public function getAddressAction($long, $lat, $map = 'mapbox', $key=null) {
		if($map == 'mapbox') {
			$key = 'pk.eyJ1IjoibW92YW1hcHMiLCJhIjoiZTIwM2MzNTBkMzBlMDI3YThmNzE4MmViMDhmZjgwZGUifQ.7cJw2Mx5rHo2sx2jFPDQhQ';
			$local = json_decode(file_get_contents("http://api.mapbox.com/v4/geocode/mapbox.places/$long,$lat.json?access_token=$key"));
			$return = $local->features[0];
		} else {
			$return = 'error';
		}
		return $return;
	}
	
	public function getMapPathAction($obj) {		
		try {			
			if(!isset($_SESSION)) {
				session_start();
			}
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;

			//pegando filtro de data/hora
			$start_date = $this->formatDateTimeAction($obj->start_day, $obj->start_hour, '/'); 
			$end_date = $this->formatDateTimeAction($obj->end_day, $obj->end_hour, '/');
			$andDate = " AND (devst.local_time >= '$start_date' AND devst.local_time <= '$end_date') ";

			//unidades
			$unit = $obj->unit_id;
			$andUnit = " AND tu.id = $unit ";

			$db = new DatabaseHandler();			
			$db -> addOption("table", 	"SELECT 
											tu.id, 
											devst.id as devst_id, 
											tu.label, 
											CASE 
												WHEN (to_char(devst.local_time, 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(devst.local_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(devst.local_time, 'DD/MM/YY') 
											END	AS data, 
											devst.local_time,
											to_char(devst.local_time, 'DD/MM/YY HH24:MI') as full_date, 
											to_char(devst.local_time, 'DD/MM/YY') as d_date, 
											to_char(devst.local_time, 'HH24:MI') as hour, 
											devst.longitude as longitude, 
											devst.latitude as latitude, 
											devst.speed as velocidade,
											devst.direction as direction,
											coalesce(devst.address,'') as full_address,
											CASE 
												WHEN (to_char(cs.initial_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(cs.initial_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cs.initial_time, 'DD/MM/YY') 
											END	AS data_inicio, 
											to_char(cs.initial_time, 'HH24:MI') as hora_min_inicio, 
											CASE 
												WHEN (to_char(cs.final_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(cs.final_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cs.final_time, 'DD/MM/YY') 
											END	AS data_fim, 
											to_char(cs.final_time, 'HH24:MI') as hora_min_fim,
											coalesce(cs.total_time,0) as total_time
										FROM
											tracked_unit as tu
											INNER JOIN dev_status_30 as devst ON tu.id = devst.unit_id
											LEFT JOIN con_stop cs ON devst.id = cs.initial_history_id AND cs.move_stop = 0
										WHERE 
											tu.account_id = $account_id
											$andUnit
											$andDate
										ORDER BY 
											devst.local_time");

			$resultSearch = $db -> select(false, 0, true);
			$db->close();
			
			if(is_object($resultSearch)) {
				$result = array();
				while($search = $resultSearch->fetch()) {
					$result[] = $search;
				}				
				return json_encode($result);
			} else {
				if($resultSearch === false) {
					return 'Não foram encontrados registro para este filtro.';
				} else {
					return E_UNABLETOGETPARENTWIDGETS;
				}
			}
		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}
}