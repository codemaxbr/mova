<?php
Class AlarmController extends defaultController {
	
	public function indexAction($infos = null) {
		$widget = new widgetController();
		$unit = new UnitController();
		$cerca = new CercaController();
		$event = new EventController();
		$return = array();

		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());
		$return['groupinfo'] = $unit -> listGroupAction();
		$return['parameterInfo'] = $this -> getParameterAction();
		$return['cercaInfo'] = $cerca -> listAction();
		$return['eventInfo'] = $event -> listAction();

		return json_encode($return);
	}

	public function indexViolationAction($infos = null) {
		$unit = new UnitController();
		$return = array();

		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());
		$return['groupinfo'] = $unit -> listGroupAction();

		return json_encode($return);
	}

	public function listAction($param=null) {

		if(isset($param)) {
			if( (isset($param->user)) && ($param->token == "12342612648763598123791829371462835413")) {
				$user_id = $param->user;
			}
		}else{				
			$user_id = $_SESSION['user'] -> getUserId();
		}

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											al.id as alarm_id, 
											al.name as alarm_name, 
											al.description, 
											al.email_notification, 
											al.level, 
											al.group_id, 
											al.name as group_name
										FROM 
											alarm al
											INNER JOIN \"group\" gp ON al.group_id = gp.id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = gp.id AND usgrac.user_id = $user_id
										WHERE 
											al.status = 1
										ORDER BY al.name");
			
			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	
			}

			$return = array("data" => $result);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function getByIdAction($id) {
		try {

			$db = new DatabaseHandler();			
			$db->addOption("table", "SELECT
										al.*
										, gp.name as group_name
										, ac.parameter_id, ac.operator, ac.value
										, p.input, p.name as parameter_name, p.id as parameter_id, p.option_id, p.option_name
										, pt.operators_name, pt.operators
										, ce.name as cerca_name
										, te.name as event_name
									FROM  
										alarm al
										INNER JOIN \"group\" gp ON al.group_id = gp.id
										LEFT JOIN alarm_config ac ON ac.alarm_id = al.id 
										INNER JOIN alarm_parameter p ON p.id = ac.parameter_id
										LEFT JOIN alarm_parameter_type pt ON p.type = pt.id
										LEFT JOIN cerca ce ON ac.value = CAST(ce.id AS varchar)
										LEFT JOIN tracker_event te ON ac.value = CAST(te.id AS varchar)
									WHERE  
										al.id = $id");

			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	

				$AlrmUnitAssoc = $this -> getUnitAssocByIdAction($id);
			}

			$return = array("data" => [$result, $AlrmUnitAssoc]);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function getUnitAssocByIdAction($id) {
		try {

			$db = new DatabaseHandler();			
			$db->addOption("table", "SELECT
										tu.id,
										atu.status,
										tu.label as name,
										tu.label2 as description,
										tu.group_id
									FROM  
										alarm_tracked_unit atu
										INNER JOIN tracked_unit tu ON tu.id = atu.unit_id
									WHERE  
										atu.alarm_id = $id");

			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	
			}

			$return = array("data" => $result);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function getParameterAction() {
		try {

			$db = new DatabaseHandler();			
			$db->addOption("table", "SELECT
										p.id as parameter_id, p.name as parameter_name, p.input, p.placeholder, p.option_id, p.option_name, pt.operators, pt.operators_name
									FROM  
										alarm_parameter p
										INNER JOIN alarm_parameter_type pt ON p.type = pt.id
									ORDER BY p.id");

			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	
			}

			$return = array("data" => $result);	
			return json_encode($return);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addAction($obj) {
		try {
			$user_id = $_SESSION['user'] -> getUserId();

			$name = $obj -> name;
			$account_id = $obj -> account_id;
			$group_id = $obj -> group_id;
			$level = $obj -> level;
			$description = $obj -> description;
			$notif_email = $obj -> notif_email;
			$notif_monitor = $obj -> notif_monitor;
			$time_send_mail = $obj -> time_send_mail;
			$email_notification = $obj -> email_notification;

			$alarm_parameter = $obj -> alarm_parameter_val;
			$alarm_parameter_type = $obj -> alarm_parameter_type_val;
			$alarm_option = $obj -> alarm_option_val;

			$db = new DatabaseHandler();
			$db -> addOption("table", "INSERT INTO alarm (name, account_id, group_id, level, description, time_send_mail, email_notification, user_created, date_created, status, notif_email, notif_monitor) VALUES 
										(
											'$name',
											$account_id,
											$group_id,
											$level,
											'$description',
											$time_send_mail,
											'$email_notification',
											$user_id,
											NOW(),
											1,
											$notif_email,
											$notif_monitor
										)");

			$result = $db -> insert(false, 0, 0, true);			
			//Pegando o id do usuário criado
			$alarmId =  $db->lastInsertId('alarm_id_seq');

			//$sql = "DELETE FROM alarm_config WHERE id = $alarmId;";
			for ($i = 0; $i < sizeof($alarm_parameter); $i++) {
				$p = $alarm_parameter[$i];
				$t = $alarm_parameter_type[$i];
				$o = $alarm_option[$i];
				$db -> addOption("table", "INSERT INTO alarm_config (alarm_id, parameter_id, operator, value) VALUES ($alarmId, $p, '$t', '$o')");
				$resultConfig = $db -> insert(false, 0, 0, true);
			}

			$db->close();

			if(is_bool($result) && $result === true) {
				$return = array("data" => [$alarmId, 'Alarme cadastrado com sucesso.']);	
				// return json_encode('Alarme cadastrado com sucesso.');
				return json_encode($return);
			} else {
				return 'Erro ao tentar cadastrar o Alarme.';
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function assocAlarmUnitAction($obj) {
		try {
			$user_id = $_SESSION['user'] -> getUserId();

			$id = $obj -> id;
			$units = '';
			if(isset($obj -> unitAssoc)){
				$units = $obj -> unitAssoc;
			}
			$db = new DatabaseHandler();

			$db -> addOption("table", "alarm_tracked_unit");
			$db -> addOption("where", array("alarm_id = :id"));
			$db -> addOption("param", array(":id" => $id));
			$deleteResult = $db -> delete();

			$result = true;
			if($units != ''){
				for ($i = 0; $i < sizeof($units); $i++) {
					$unit = $units[$i];
					$db -> addOption("table", "INSERT INTO alarm_tracked_unit (alarm_id, unit_id, status) VALUES ($id, $unit, 1)");
					$result = $db -> insert(false, 0, 0, true);
				}
			}
			
			$AlarmUnitAssoc = $this -> getUnitAssocByIdAction($id);

			$db->close();

			if(is_bool($result) && $result === true) {
				//return json_encode('Unidades associadas com sucesso.');
				return json_encode($AlarmUnitAssoc);
			} else {
				return 'Erro ao tentar associas unidades.';
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function editAction($obj) {
		try {
			$user_id = $_SESSION['user'] -> getUserId();

			$id = $obj -> id;
			$name = $obj -> name; 
			$account_id = $obj -> account_id;
			$group_id = $obj -> group_id;
			$level = $obj -> level;
			$description = $obj -> description;
			$notif_email = $obj -> notif_email;
			$notif_monitor = $obj -> notif_monitor;
			$time_send_mail = $obj -> time_send_mail;
			$email_notification = $obj -> email_notification;

			$alarm_parameter = $obj -> alarm_parameter_val;
			$alarm_parameter_type = $obj -> alarm_parameter_type_val;
			$alarm_option = $obj -> alarm_option_val;

			$db = new DatabaseHandler();
			$db -> addOption("table", "UPDATE alarm SET 
											name = '$name', 
											account_id = $account_id, 
											group_id = $group_id, 
											level = $level, 
											description = '$description', 
											email_notification = '$email_notification', 
											time_send_mail = $time_send_mail,
											user_modified = $user_id, 
											date_modified = NOW(), 
											notif_email = $notif_email, 
											notif_monitor = $notif_monitor
										WHERE id = $id"
									);
			$result = $db -> update(false, true);

			$db -> addOption("table", "alarm_config");
			$db -> addOption("where", array("alarm_id = :id"));
			$db -> addOption("param", array(":id" => $id));
			$deleteResult = $db -> delete();
			
			if(is_bool($deleteResult) &&  $deleteResult === true){
				for ($i = 0; $i < sizeof($alarm_parameter); $i++) {
					$p = $alarm_parameter[$i];
					$t = $alarm_parameter_type[$i];
					$o = $alarm_option[$i];
					$db -> addOption("table", "INSERT INTO alarm_config (alarm_id, parameter_id, operator, value) VALUES ($id, $p, '$t', '$o')");
					$resultConfig = $db -> insert(false, 0, 0, true);
				}
			}else{
				$db->close();
				return 'Erro ao tentar alterar as configurações do Alarme.';
			}
			$db->close();
			
			if(is_bool($result) && $result === true) {
				$return = array("data" => [$id, 'Alarme alterado com sucesso.']);	
				// return json_encode('Alarme alterado com sucesso.');
				return json_encode($return);
			} else {
				return 'Erro ao tentar alterar o Alarme.';
			}
			
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function deleteAction($obj) {
		try {

			$db = new DatabaseHandler();

			$db -> addOption("table", "alarm_tracked_unit");
			$db -> addOption("where", array("alarm_id = :id"));
			$db -> addOption("param", array(":id" => $obj -> id));
			$deleteResult = $db -> delete();

			$db -> addOption("table", "alarm");
			$db -> addOption("fields", array("status = :status"));
			$db -> addOption("where", array("id = :id"));
			$db -> addOption("param", array(":id" => $obj -> id,
											":status" => $obj -> status));
			$result = $db -> update();

			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Alarme removido com sucesso.');
			} else {
				return 'Erro ao tentar deletar o Alarme.';
			}

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	//ALARMES VIOLADOS ##################

	public function listViolationAction($param=null) {

		if(isset($param)) {
			if( (isset($param->user)) && ($param->token == "12342612648763598123791829371462835413")) {
				$user_id = $param->user;
			}else{
				$user_id = $_SESSION['user'] -> getUserId();
			}
		}else{				
			$user_id = $_SESSION['user'] -> getUserId();
		}

		//group
		$andGroup = ' ';
		$groups = '0';
		if( $param->group_id != null ){
			foreach ($param->group_id as $group) {
				$groups .= ','.$group;
			}
			if( ($groups != '0,0') && ($groups != '0') ){
				$andGroup = " AND al.group_id IN ($groups) ";
			}
		}
		$andLevel = ' ';
		if( $param->level != null && $param->level != '' && $param->level != '0' ){
			$andLevel = " AND al.level IN (".$param->level.") ";
		}

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											al.id as alarm_id, 
											al.name as alarm_name, 
											al.description, 
											al.email_notification, 
											al.level, 
											al.group_id, 
											gp.name as group_name,
											tu.label,
											av.id as violation_id,
											av.initial_time,
											av.user_view,
											av.status_30_id as hist_id,
											CASE 
												WHEN (to_char(av.initial_time, 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(av.initial_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(av.initial_time, 'DD/MM/YY') 
											END	AS data, 
											to_char(av.initial_time, 'DD/MM/YY HH24:MI') as full_date,
											to_char(av.initial_time, 'HH24:MI') as hora,
											dst.latitude as lat,
											dst.longitude as lng,
											coalesce(dst.address, '') as full_address
										FROM 
											alarm_violation av
											INNER JOIN alarm al ON av.alarm_id = al.id AND al.status = 1
											INNER JOIN \"group\" gp ON al.group_id = gp.id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = gp.id AND usgrac.user_id = $user_id
											INNER JOIN tracked_unit tu ON av.unit_id = tu.id AND tu.status = 1
											INNER JOIN dev_status_30 dst ON dst.id = av.status_30_id
										WHERE 
											av.status = 1
											$andGroup
											$andLevel
										ORDER BY av.initial_time DESC");
			
			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	
			}

			$return = array("data" => $result);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function setViewUserAction($obj) {
		try {

			$db = new DatabaseHandler();

			$db -> addOption("table", "alarm_violation");
			$db -> addOption("fields", array("user_view = 1"));
			$db -> addOption("where", array("id = :id"));
			$db -> addOption("param", array(":id" => $obj -> id));
			$result = $db -> update();
			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Alarme visto.');
			} else {
				return 'Erro ao tentar marcar o Alarme.';
			}

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function deleteAlarmViolatedAction($obj) {
		try {

			$db = new DatabaseHandler();

			$db -> addOption("table", "alarm_violation");
			$db -> addOption("fields", array("status = 3"));
			$db -> addOption("where", array("id = :id"));
			$db -> addOption("param", array(":id" => $obj -> id));
			$result = $db -> update();
			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Violação de alarme removida com sucesso.');
			} else {
				return 'Erro ao tentar remover a violação do Alarme.';
			}

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
}
