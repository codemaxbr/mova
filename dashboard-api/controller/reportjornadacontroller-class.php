<?php 
class reportjornadaController extends defaultController
{
	public function indexAction($infos=null) {
		date_default_timezone_set("America/Sao_Paulo");
		$unit = new unitController();
		$return = array();
		$return['groupinfo'] = $unit -> listGroupAction();
		$return['subgroupinfo'] = $unit -> listSubGroupAction();
		$return['unities'] = $unit -> listAction();
		$return['date'] = date('d/m/Y');
		$return['hour'] = date('H:i');
		
		return json_encode($return);	
	}

	function object2array($o){
	 $a = (array) $o;
	 
	 foreach($a as $k => $v)
	   if( is_object($v))
	     $a[$k] = object2array($v);

	 reset($a);
	 return $a;
	}
	
	public function searchAction($obj) {

		try {			
			if(!isset($_SESSION)) {
				session_start();
			}
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;
			$user_id = $_SESSION['user'] -> getUserId();
			
			//controle de consulta
			//sempre retornando de 100 em 100 a partir do ultimo registro encontrado

			

			$limit = '';
			$orderby = ' tu.label DESC, cd.first_activity_time DESC';
			if( $obj->limit != null && $obj->limit != '' && $obj->limit != '0' ){
				$limit = " LIMIT 1000 OFFSET ".$obj->limit;
				$orderby = ' tu.label DESC, cd.first_activity_time DESC, cd.id DESC ';
			}

			//pegando filtro de data/hora
			$start_date = $this->formatDateTimeAction($obj->start_day, $obj->start_hour, '/'); 
			$end_date = $this->formatDateTimeAction($obj->end_day, $obj->end_hour, '/');
			$andDate = " AND (cd.first_activity_time >= '$start_date' AND cd.first_activity_time <= '$end_date') ";

			//unidades
			$andUnits = '';
			$units = '0';
			foreach ($obj->units as $unit) {
				$units .= ','.$unit;
			}
			if( ($units != '0,0') && ($units != '0') ){
				$andUnits = " AND cd.unit_id IN ($units) ";
			}
			//group
			$andGroups = '';
			$groups = '0';
			foreach ($obj->group as $group) {
				$groups .= ','.$group;
			}
			if( ($groups != '0,0') && ($groups != '0') ){
				$andGroups = " AND cd.group_id IN ($groups) ";
			}
			//Subgroup
			$andSubGroups = '';
			$subgroups = '0';
			foreach ($obj->subgroup as $subgroup) {
				$subgroups .= ','.$subgroup;
			}
			if( ($subgroups != '0,0') && ($subgroups != '0') ){
				$andSubGroups = " AND cd.subgroup_id IN ($subgroups) ";
			}

			$db = new DatabaseHandler();			
			$db -> addOption("table", 	"SELECT DISTINCT
											cd.id, 
											cd.unit_id as unit_id,
											cd.first_activity_time as first_time, 
											cd.last_activity_time as last_time,
											tu.label, 
											CASE 
												WHEN (to_char(cd.first_activity_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(cd.first_activity_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cd.first_activity_time, 'DD/MM/YY') 
											END	AS data_inicio, 
											to_char(cd.first_activity_time, 'DD/MM/YY HH24:MI') as full_date_begin, 
											to_char(cd.first_activity_time, 'DD/MM/YY') as data_report_inicio, 
											to_char(cd.first_activity_time, 'HH24:MI') as hora_min_inicio,
											cd.first_activity_time,
											CASE 
												WHEN (to_char(cd.last_activity_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(cd.last_activity_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cd.last_activity_time, 'DD/MM/YY') 
											END	AS data_fim, 
											to_char(cd.last_activity_time, 'DD/MM/YY HH24:MI') as full_date_end, 
											to_char(cd.last_activity_time, 'DD/MM/YY') as data_report_fim, 
											to_char(cd.last_activity_time, 'HH24:MI') as hora_min_fim, 
											cd.last_activity_time,
											cd.last_activity_time - cd.first_activity_time as total_jornada,
											cd.first_activity_lon as initial_longitude, 
											cd.first_activity_lat as initial_latitude,
											cd.last_activity_lon as last_longitude, 
											cd.last_activity_lat as last_latitude,
											cd.first_activity_history_id as initial_history_id,
											cd.last_activity_history_id as last_history_id,
											cd.first_activity_poi_name as initial_poi_name,
											cd.last_activity_poi_name as last_poi_name,

											coalesce(cd.first_activity_address,'') as initial_address,
											coalesce(cd.last_activity_address,'') as last_address

											
										FROM
											con_day as cd
											INNER JOIN tracked_unit as tu ON tu.id = cd.unit_id

										WHERE 
											cd.account_id = $account_id
											$andUnits
											$andDate
											/*
											$andGroups
											$andSubGroups*/
											

										ORDER BY
											$orderby
										$limit");

			$resultSearch = $db -> select(false, 0, true);
			//echo $resultSearch;
			//echo $db -> query();
			$db->close();

			
				$result = array();
				$content = null;

				$unit = null;
				$retorno = array();
				$i = 0;
				
				foreach ($resultSearch->fetchAll() as $key => $result) {

					if($unit == $result->unit_id){
						$itens = array(
										'data_relatorio' => $result->data_report_inicio,
										'hora_inicio' => $result->hora_min_inicio,
										'hora_fim' => $result->hora_min_fim,
										'POI_inicio' => $result->initial_poi_name,
										'POI_fim' => $result->last_poi_name,
										'total_jornada' => $result->total_jornada,
										'inicio_long' => $result->initial_longitude,
										'inicio_lat' => $result->initial_latitude,
										'fim_long' => $result->last_longitude,
										'fim_lat' => $result->last_latitude,
										'initial_history_id' => $result->initial_history_id,
										'last_history_id' => $result->last_history_id,
										'initial_address' => $result->initial_address,
										'last_address' => $result->last_address
									);
						array_push($placa[$result->unit_id]['itens'], $itens);
					}else{
						$itens = array(
									'data_relatorio' => $result->data_report_inicio,
									'hora_inicio' => $result->hora_min_inicio,
									'hora_fim' => $result->hora_min_fim,
									'POI_inicio' => $result->initial_poi_name,
									'POI_fim' => $result->last_poi_name,
									'total_jornada' => $result->total_jornada,
									'inicio_long' => $result->initial_longitude,
									'inicio_lat' => $result->initial_latitude,
									'fim_long' => $result->last_longitude,
									'fim_lat' => $result->last_latitude,
									'initial_history_id' => $result->initial_history_id,
									'last_history_id' => $result->last_history_id,
									'initial_address' => $result->initial_address,
									'last_address' => $result->last_address
								);

						$placa[$result->unit_id] = array(
							'label' => $result->label,
							'account_id' => json_decode($_SESSION['user']->getUserInfo()),
							//'itens'	=> array($result->unit_id." linha")
							'itens'	=> array($itens)
							);
					}

					$i + 1;
					$unit = $result->unit_id;
				}

				$placa = array_values($placa);
				return json_encode($placa);
			
				if($resultSearch === false) {
					return json_encode(array('text' => 'Não foram encontrados registro para este filtro.'));
				} else {
					return E_UNABLETOGETPARENTWIDGETS;
				}
			
			
			
		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getConStopByHistIdAction($hist_id) {		
		try {			
			if(!isset($_SESSION)) {
				session_start();
			}
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;

			$db = new DatabaseHandler();			
			$db -> addOption("table", 	"SELECT 
											cs.id, 
											cs.unit_id as unit_id,
											cs.initial_history_id as devst_id, 
											tu.label, 

											CASE 
												WHEN (to_char(cs.initial_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(cs.initial_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cs.initial_time, 'DD/MM/YY') 
											END	AS data_inicio, 
											to_char(cs.initial_time, 'HH24:MI') as hora_min_inicio, 
											cs.initial_time,

											CASE 
												WHEN (to_char(cs.final_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(cs.final_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cs.final_time, 'DD/MM/YY') 
											END	AS data_fim, 
											to_char(cs.final_time, 'HH24:MI') as hora_min_fim, 
											cs.final_time,

											coalesce(cs.initial_poi_name,'') as initial_poi_name,
											coalesce(cs.initial_area_name,'') as initial_area_name,

											coalesce(cs.total_time,0) as total_time
										FROM
											con_stop as cs
											INNER JOIN tracked_unit as tu ON tu.id = cs.unit_id
										WHERE 
											cs.account_id = $account_id
											AND cs.initial_history_id = $hist_id");

			$resultSearch = $db -> select(false, 0, true);
			//echo $resultSearch;
			//echo $db -> query();
			$db->close();
			
			if(is_object($resultSearch)) {
				$result = array();
				while($search = $resultSearch->fetch()) {
					$result[] = $search;
				}				
				return json_encode($result);
			} else {
				if($resultSearch === false) {
					return json_encode(array('text' => 'Não foram encontrados registro da parada.'));
				} else {
					return E_UNABLETOGETPARENTWIDGETS;
				}
			}
		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}

}