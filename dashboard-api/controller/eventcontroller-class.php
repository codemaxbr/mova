<?php
Class EventController extends defaultController {
	
	public function indexAction($infos = null) {
		$widget = new widgetController();
		$return = array();

		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());

		return json_encode($return);
	}

	public function listAction() {

		$account_id = $_SESSION['user'] -> getAccount();

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT 
											te.id as event_id,
											te.name as event_name
										FROM 
											tracker_event te
											INNER JOIN event_type et ON te.event_type_id = et.id
											INNER JOIN account_event_type aet ON aet.event_type_id = et.id AND aet.account_id = $account_id
										ORDER BY 
											te.name");
			
			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	
			}

			$return = array("data" => $result);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

}
