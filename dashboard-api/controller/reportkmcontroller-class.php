<?php 
class reportkmController extends defaultController
{
	public function indexAction($infos=null) {
		date_default_timezone_set("America/Sao_Paulo");
		$unit = new unitController();
		$return = array();
		$return['groupinfo'] = $unit -> listGroupAction();
		$return['subgroupinfo'] = $unit -> listSubGroupAction();
		$return['unities'] = $unit -> listAction();
		$return['date'] = date('d/m/Y');
		$return['hour'] = date('H:i');
		
		return json_encode($return);	
	}
	
	public function searchAction($obj) {		
		try {			
			if(!isset($_SESSION)) {
				session_start();
			}
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;
			$user_id = $_SESSION['user'] -> getUserId();
			
			//controle de consulta
			$limit = '';
			$orderby = ' tu.label, cd.cday ';
			if( $obj->limit != null && $obj->limit != '' ){
				$limit = " LIMIT 1000 OFFSET ".$obj->limit;
				$orderby = ' tu.label, cd.cday ';
			}

			//pegando filtro de data/hora
			$start_date = $this->formatDateTimeAction($obj->start_day, '00:00', '/'); 
			$end_date = $this->formatDateTimeAction($obj->end_day, '23:59', '/');
			$andDate = " AND (cd.cday >= '$start_date' AND cd.cday <= '$end_date') ";

			//unidades
			$andUnits = '';
			$units = '0';
			foreach ($obj->units as $unit) {
				$units .= ','.$unit;
			}
			if( ($units != '0,0') && ($units != '0') ){
				$andUnits = " AND cd.unit_id IN ($units) ";
			}

			$db = new DatabaseHandler();		
			$db -> addOption("table", 	"SELECT 
											cd.unit_id,
											tu.label as label,
											cd.cday,
											to_char(cd.cday, 'D') as weekday,
											coalesce(cd.km, 0) as km
										FROM
											con_day as cd
											INNER JOIN tracked_unit as tu ON tu.id = cd.unit_id
											INNER JOIN user_group_access usgrac ON cd.group_id = usgrac.group_id AND cd.subgroup_id = usgrac.subgroup_id AND usgrac.user_id = $user_id
										WHERE 
											tu.account_id = $account_id
											$andUnits
											$andDate
										ORDER BY 
											$orderby
										$limit");
			$resultSearch = $db -> select(false, 0, true);
			//ECHO $db -> query();
			$db->close();
			
			if(is_object($resultSearch)) {
				$result = array();
				while($search = $resultSearch->fetch()) {
					$result[] = $search;
				}				
				return json_encode($result);
			} else {
				if($resultSearch === false) {
					return json_encode(array('text' => 'Não foram encontrados registro para este filtro.'));
				} else {
					return E_UNABLETOGETPARENTWIDGETS;
				}
			}
		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}

}