<?php 
class reporthidroController extends defaultController
{
	public function indexAction($infos=null) {
		date_default_timezone_set("America/Sao_Paulo");
		$unit = new unitController();
		$return = array();
		$return['groupinfo'] = $unit -> listGroupAction();
		$return['subgroupinfo'] = $unit -> listSubGroupAction();
		$return['unities'] = $unit -> listAction();
		$return['date'] = date('d/m/Y');
		$return['hour'] = date('H:i');
		
		return json_encode($return);	
	}
	
	public function searchAction($obj) {		
		try {			
			if(!isset($_SESSION)) {
				session_start();
			}
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;
			$user_id = $_SESSION['user'] -> getUserId();
			
			//controle de consulta
			$limit = '';
			$orderby = ' rh.start_time DESC, rh.unit_label ';
			if( $obj->limit != null && $obj->limit != '' ){
				$limit = " LIMIT 1000 OFFSET ".$obj->limit;
				$orderby = ' rh.start_time DESC, rh.unit_label ';
			}

			//pegando filtro de data/hora
			$start_date = $this->formatDateTimeAction($obj->start_day, $obj->start_hour, '/'); 
			$end_date = $this->formatDateTimeAction($obj->end_day, $obj->end_hour, '/');
			$andDate = " AND (rh.start_time >= '$start_date' AND rh.start_time <= '$end_date') ";

			//unidades
			$andUnits = '';
			$units = '0';
			foreach ($obj->units as $unit) {
				$units .= ','.$unit;
			}
			if( ($units != '0,0') && ($units != '0') ){
				$andUnits = " AND rh.unit_id IN ($units) ";
			}

			$db = new DatabaseHandler();			
			$db -> addOption("table", 	"SELECT 
											rh.id, 
											rh.unit_id,
											rh.unit_label as label,
											rh.dev_status_30_ini_id as devst_id, 

											CASE 
												WHEN (to_char(rh.start_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(rh.start_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(rh.start_time, 'DD/MM/YY') 
											END	AS data_inicio, 
											to_char(rh.start_time, 'DD/MM/YY HH24:MI') as full_date_begin, 
											to_char(rh.start_time, 'DD/MM/YY') as data_report_inicio, 
											to_char(rh.start_time, 'HH24:MI') as hora_min_inicio, 
											rh.start_time, 

											CASE 
												WHEN (to_char(rh.end_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(rh.end_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(rh.end_time, 'DD/MM/YY') 
											END	AS data_fim, 
											to_char(rh.start_time, 'DD/MM/YY HH24:MI') as full_date_end, 
											to_char(rh.end_time, 'DD/MM/YY') as data_report_fim, 
											to_char(rh.end_time, 'HH24:MI') as hora_min_fim, 
											rh.end_time,

											rh.longitude as longitude, 
											rh.latitude as latitude, 

											coalesce(rh.address, '') as full_address,

											coalesce(rh.poi_name, '') as poi_name,
											coalesce(rh.qtd, 0) as qtd,
											coalesce(rh.total_time, 0) as total_time
										FROM
											report_hidro as rh
											INNER JOIN tracked_unit as tu ON tu.id = rh.unit_id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = tu.group_id AND tu.subgroup_id = usgrac.subgroup_id AND usgrac.user_id = $user_id
										WHERE 
											tu.account_id = $account_id
											$andUnits
											$andDate
										ORDER BY 
											$orderby
										$limit");

			$resultSearch = $db -> select(false, 0, true);
			//echo $resultSearch;
			//echo $db -> query();
			$db->close();
			
			if(is_object($resultSearch)) {
				$result = array();
				while($search = $resultSearch->fetch()) {
					$result[] = $search;
				}				
				return json_encode($result);
			} else {
				if($resultSearch === false) {
					return json_encode(array('text' => 'Não foram encontrados registro para este filtro.'));
				} else {
					return E_UNABLETOGETPARENTWIDGETS;
				}
			}
		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}

}