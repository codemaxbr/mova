<?php 
class reportchecklistvelledaController extends defaultController
{
	public function indexAction($infos=null) {
		date_default_timezone_set("America/Sao_Paulo");
		$unit = new unitController();
		$return = array();
		$return['groupinfo'] = $unit -> listGroupAction();
		$return['unities'] = $unit -> listAction();
		$return['date'] = date('d/m/Y');
		
		return json_encode($return);	
	}
	
	public function searchAction($obj) {		
		try {			
			if(!isset($_SESSION)) {
				session_start();
			}

			$user_id = $_SESSION['user'] -> getUserId();

			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;
			$user_id = $_SESSION['user'] -> getUserId();
			
			//controle de consulta
			$limit = '';
			$orderby = ' co.id DESC ';
			if( $obj->limit != null && $obj->limit != '' ){
				$limit = " LIMIT 1000 OFFSET ".$obj->limit;
			}
			//id do posto
			$andGroup = '';
			if( $obj->group_id != null && $obj->group_id != '' && $obj->group_id != '0' ){
				$andGroup = " AND co.group_id = " . $obj->group_id . " ";
			}

			//pegando filtro de data/hora
			$start_date = $this->formatDateTimeAction($obj->start_dataatual, '00:00', '/'); 
			$end_date = $this->formatDateTimeAction($obj->end_dataatual, '00:00', '/');
			$andDate = " AND (co.dataatual >= '$start_date' AND co.dataatual <= '$end_date') ";

			$db = new DatabaseHandler();			
			$db -> addOption("table", 	"SELECT 
											co.*
											, CASE 
												WHEN (to_char(co.dataatual, 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(co.dataatual, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(co.dataatual, 'DD/MM/YY') 
											END	AS dataatual_data
											, to_char(co.dataatual, 'DD/MM/YY HH24:MI') as dataatual_full
											, to_char(co.dataatual, 'HH24:MI') as dataatual_time
											, g.name as posto
										FROM
											velleda.checklist_order co
											INNER JOIN \"group\" g ON g.id = co.group_id
										WHERE 
											co.account_id = $account_id
											$andDate
											$andGroup
										ORDER BY 
											$orderby
										$limit");

			$resultSearch = $db -> select(false, 0, true);
			//ECHO $db -> query();
			$db->close();
			
			if(is_object($resultSearch)) {
				$result = array();
				while($search = $resultSearch->fetch()) {
					$result[] = $search;
				}				
				return json_encode($result);
			} else {
				if($resultSearch === false) {
					return json_encode(array('text' => 'Não foram encontrados registro para este filtro.'));
				} else {
					return E_UNABLETOGETPARENTWIDGETS;
				}
			}
		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}

}