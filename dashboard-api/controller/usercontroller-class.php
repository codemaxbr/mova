<?php
class userController extends defaultController
{	
	public function indexAction($infos=null) {
		$widgets = new widgetController();
		$account = new accountController();
		$client = new clientController();
		$return = array();
		//$return['permissionType'] = $this -> haveWriteAction($infos);
		//$return['accounts'] = $account->getAccountsAction();
		$return['days'] = $this->daysAction();
		$return['userinfo'] = json_decode($_SESSION['user']->getUserInfo()); //Transofrmando em objeto
		$return['widgetList'] = $widgets -> listWidgetsAction();
		$return['groupList'] = $client -> listTreeAccLogAction();
		$return['userList'] = $this -> listAction();
		
		return json_encode($return);
	}
	
	public function addAction($obj) {
		try{			
			//Validando e-mail
			if(Validate::email($obj->login) === false){
				return E_INVALIDMAIL;
			}
			
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());

			//verifica o tipo de conta. caso seja conta MOVA, o usuário eh do tipo mova
			$account_type = $my_account_id->user->account_type;
			$user_mova = 0;
			if($account_type == 1) {
				$user_mova = 1;
			}
			/* TODO
			 * GAMBIARRA ATÉ ACHAR SOLUÇÃO
			 * Quando não selecionados, estes campos a seguir
			 * vem como string vazia (''), sendo que o datatype
			 * do banco deles não é string 
			 * */
			if((!isset($obj->hour_start)) || ($obj->hour_start == '')) {
				$obj->hour_start = null;
			}
			
			if((!isset($obj->hour_end)) || ($obj->hour_end == '')) {
				$obj->hour_end = null;
			}
			
			if((!isset($obj->day_end)) || ($obj->day_end == '')) {
				$obj->day_end = null;
			}
			
			if((!isset($obj->day_start)) || ($obj->day_start == '') ) {
				$obj->day_start = null;
			}
			
			if((!isset($obj->end_access)) || ($obj->end_access == '')) {
				$obj->end_access = null;
			} else {
				//Formatando data para postgree
				$obj->end_access = $this->formatDateAction($obj->end_access, '/');
			}
	
			//Verificando se usuário está sendo criado do widget conta ou do próprio usuário
			if(!isset($obj->master)) {
				//Se não tiver master setado ele está passando pelo widget usuários
				$obj->master = 0;
	
				//Validando criação usuário para que ele não mude conta "inspecionando elemento"
				$validateId = $my_account_id->user->account_id;
				if($obj->account_id != $validateId) {
					return E_USERACCESSDENIED;
				}
			}
	
			//Verificando se e-mail já esta cadastrado
			$hasLogin = $this->getLoginUserAction($obj->login);
			
			//Se já houver o e-mail cadastrado ele não cadastra usuário
			//Se a pesquisa retornar falso (não encontra nenhum usuário com este e-mail cadastrado), inicia o cadastro
			if(!$hasLogin)
			{
				//Criando senha para usuário automaticamente
				$password = $this->random_password(4);
				$my_account_info = json_decode($_SESSION['user']->getUserInfo());
				$auth = new Authentication($obj->login, $password);
				$passwordHash = $auth->passwordHash();

				$db = new DatabaseHandler();
				//Criando usuário
				$db->addOption("table", "users");
				$db->addOption("fields", array(	"name", 
												"login", 
												"password", 
												"account_id", 
												"master", 
												"hour_start", 
												"hour_end", 
												"day_start", 
												"day_end",  
												"user_web",  
												"user_mobile", 
												"end_access", 
												"user_mova"));
				$db->addOption("param", array(	":name" => $obj->name, 
												":login" => $obj->login, 
												":password" => $passwordHash, 
												":account_id" => $obj->account_id, 
												":master" => $obj->master, 
												":hour_start" => $obj->hour_start, 
												":hour_end" => $obj->hour_end, 
												":day_start" => $obj->day_start, 
												":day_end" => $obj->day_end, 
												":user_web" => $obj->user_web, 
												":user_mobile" => $obj->user_mobile, 
												":end_access" => $obj->end_access, 
												":user_mova" => $user_mova));
				$createUser = $db->insert();
				
				//Validando insert
				if($createUser !== true) {
					//return $createUser;
					return E_UNABLETOCREATEUSER;
					
				}
				
				//Pegando o id do usuário criado
				$userId =  $db->lastInsertId('users_id_seq');
				$db->close();
				
				//Adicionando mesmas permissões ao usuário master
				$permsAcess = $this->addAccessAction($obj->perms, $userId);
				if($permsAcess != true) {
					return E_UNABLETOCREATEACCESS;
				}
				
				//Adicionando mesmas permissões ao usuário master
				$groups = $this->addAcessGroupsAction($obj->groups, $userId);
				if($groups != true) {
					return E_UNABLETOADDGROUPACCESS;
				}
				
				//Envio de e-mail
				$this->sendEmailAction("Conta Mova", "Sua conta foi criada com sucesso!<br />Abaixo seguem seus dados para acesso:<br /><br />Link: http://mova2.movaomundo.com<br />Login: ".$obj->login."<br />Senha: ".$password, $obj->login);
				
				return json_encode('Usuário cadastrado com sucesso');
	
			} else {
				//Se já houver e-mail/login cadastrado no sistema
				return E_USEREXISTS;
			}
		}catch(Exception $e){
			return E_INTERNAL;
		}
	}
	
	public function editAction($obj) {
		try{
			
			$modifyDate = date("Y-m-d H:i:s");
			$my_account_info = json_decode($_SESSION['user']->getUserInfo());

			//verifica o tipo de conta. caso seja conta MOVA, o usuário eh do tipo mova
			$account_type = $my_account_info->user->account_type;
			$user_mova = 0;
			if($account_type == 1) {
				$user_mova = 1;
			}
			/* 
			 * Comentado até habilitar edição de e-mail
			 * 
			 * //Validando e-mail
			if(Validate::email($obj->login) === false){
				return 'Este não é um e-mail válido.';
			}
			
			$accEmail = $my_account_id->user->login;
			if($accEmail != $obj->login)
			{
				//Verificando se e-mail já esta cadastrado
				if($this->getLoginUserAction($obj->login)) {
					return 'Este e-mail já existe cadastrado em nosso sistema.';
				}
			} */
	
			/* TODO
			 * GAMBIARRA ATÉ ACHAR SOLUÇÃO
			 * Quando não selecionados, estes campos a seguir
			 * vem como string vazia (''), sendo que o datatype
			 * do banco deles não é string
			 * */
			if($obj->hour_start == '') {
				$obj->hour_start = null;
			}
			
			if($obj->hour_end == '') {
				$obj->hour_end = null;
			}
			
			if($obj->day_end == '') {
				$obj->day_end = null;
			}
			
			if($obj->day_start == '') {
				$obj->day_start = null;
			}
			
			if($obj->end_access == '') {
				$obj->end_access = null;
			} else {
				//Formatando data para postgree
				$obj->end_access = $this->formatDateAction($obj->end_access, '/');
			}
			
			//Validando se há permissões selecionadas
			$countPerms = count($obj->perms);
			if($countPerms <= 0)
			{
				return E_INVALIDPERMISSIONLEVEL;
			}
	
	
			$db = new DatabaseHandler();
			//EDITANDO ACESSOS DE GRUPOS
			//Deletando todas os acessos do usuário
			$db->addOption("table", "user_group_access");
			$db->addOption("where", array("user_id = :id"));
			$db->addOption("param", array(":id" => $obj->id));
			$db->delete();
			$this->addAcessGroupsAction($obj->groups, $obj->id);
			
			//EDITANDO ACESSOS DA CONTA
			//Deletando todas os acessos do usuário
			$db->addOption("table", "user_access");
			$db->addOption("where", array("user_id = :id"));
			$db->addOption("param", array(":id" => $obj->id));
			$db->delete();
			$this->addAccessAction($obj->perms, $obj->id);
			
			if($obj->reset_password == '1') {
				$password = $this->random_password(4);
				//Criando hash do password
				$auth = new Authentication($obj->login, $password);
				$hashPassword = $auth->passwordHash();
				
				//EDITANDO CONTA
				$db->addOption("fields", array(	"name = :name",
												"login = :login", 
												"password = :password", 
												"hour_start = :hour_start", 
												"hour_end = :hour_end", 
												"day_start = :day_start", 
												"day_end = :day_end", 
												"user_web = :user_web", 
												"user_mobile = :user_mobile", 
												"end_access = :end_access", 
												"modify_date = :modify_date", 
												"user_mova = :user_mova"));
				$db->addOption("table", "users");
				$db->addOption("where", array("id = :id"));
				$db->addOption("param", array(	":id" => $obj->id, 
												":name" => $obj->name, 
												":login" => $obj->login, 
												":password" => $hashPassword, 
												":hour_start" => $obj->hour_start, 
												":hour_end" => $obj->hour_end, 
												":day_start" => $obj->day_start, 
												":day_end" => $obj->day_end, 
												":user_web" => $obj->user_web, 
												":user_mobile" => $obj->user_mobile, 
												":end_access" => $obj->end_access, 
												":modify_date" => $modifyDate, 
												":user_mova" => $user_mova));
				$updateUser = $db->update();
				if($updateUser !== true) {
					return E_UNABLETOUPDATEUSER;
				}
	
				//Envio de e-mail
				$this->sendEmailAction("Conta Mova", "Sua senha foi alterada!<br />Abaixo seguem seus dados para acesso:<br /><br />Link: http://mova2.movaomundo.com<br />Nova Senha: ".$password, $obj->login);
				
			} else { //Caso não deseje alterar a senha
				$db = new DatabaseHandler();
				//EDITANDO CONTA
				$db->addOption("fields", array(	"name = :name",
												"login = :login", 
												"status = 1", 
												"hour_start = :hour_start", 
												"hour_end = :hour_end", 
												"day_start = :day_start", 
												"user_web = :user_web", 
												"user_mobile = :user_mobile", 
												"day_end = :day_end", 
												"end_access = :end_access", 
												"user_mova = :user_mova"));
				$db->addOption("table", "users");
				$db->addOption("where", array("id = :id"));
				$db->addOption("param", array(	":id" => $obj->id, 
												":name" => $obj->name, 
												":login" => $obj->login, 
												":hour_start" => $obj->hour_start, 
												":hour_end" => $obj->hour_end, 
												":day_start" => $obj->day_start, 
												":day_end" => $obj->day_end, 
												":user_web" => $obj->user_web, 
												":user_mobile" => $obj->user_mobile, 
												":end_access" => $obj->end_access, 
												":user_mova" => $user_mova));
				$updateUser = $db->update();
				$db->close();
				
				if($updateUser !== true) {
					return E_UNABLETOUPDATEUSER;
				}			
			}			
			return json_encode('Usuário alterado com sucesso');
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getUserAction($id) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("u.*", "ac.id as acc_id", "ac.name as name_account", "ac.account_type_id as account_type", "ac.driver_manager", "ac.skin", "ac.imglogo"));
			$db->addOption("table", "users u");
			$db->addOption("joinTable", array("account as ac"));
			$db->addOption("joinTableOn", array("u.account_id = ac.id"));
			$db->addOption("joinTableType", array("INNER JOIN"));
			$db->addOption("where", array("u.id = :id"));
			// $db->addOption("where", array("u.id = :id", "u.user_web = 1")); //somente usuários web podem acessar o sistema
			// $db->addOption("logicalOperator", array("AND"));
			$db->addOption("param", array(":id" => $id));
			$resultAccount = $db->select(true, 1);
			$user = $resultAccount->fetch();
			
			if(!is_object($user)) {
				return E_UNABLETOSELECTUSER;
			}			
			
			//Pegando permissoes do user
			$db->addOption("fields", array("user_id", "widget_id", "access_level"));
			$db->addOption("table", "user_access");
			$db->addOption("where", array("user_id = :id"));
			$db->addOption("param", array(":id" => $id));
			$resultSelectPerm = $db->select(true, 1);
		
			//Pegando grupos do user
			if(is_object($resultSelectPerm))
			{
				$permsAcc = array();
				while($perms = $resultSelectPerm->fetch()){
					$permsAcc[] = array('widget_id' => $perms->widget_id, 'access_level' => $perms->access_level);
				}
			}else{
				return E_UNABLETOSELECTUSERPERMISSION;
			}
	
			$db->addOption("fields", array("subgroup_id", "group_id"));
			$db->addOption("table", "user_group_access");
			$db->addOption("where", array("user_id = :id"));
			$db->addOption("param", array(":id" => $id));
			$resultSelectGroups = $db->select(true, 1);	
			$db->close();
	
			//Pegando grupos do user
			if($resultSelectGroups)
			{
				$groups = array();
				while($g = $resultSelectGroups->fetch()){
					$groups[] = array('subgroup_id' => $g->subgroup_id, 'group_id' => $g->group_id);
				}
			} else {
				$groups = '';
			}
		
			$result = array("user" => $user,"perms" => $permsAcc, "groups" => $groups);
		
			return json_encode($result);
		}catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getLoginUserAction($login) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("*"));
			$db->addOption("table", "users");
			$db->addOption("where", array("login = :email"));
			$db->addOption("param", array(":email" => $login));
			$resultUserAction = $db->select(true, 1);
			$db->close();
		
			return $resultUserAction;
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function listAction() {
		try {
			$db = new DatabaseHandler();

			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account = $my_account_id->user->account_id;
			
			$db->addOption("fields", array("id", "name", "login"));;
			$db->addOption("table", "users");
			$db->addOption("where", array("account_id = :account_id", "status = 1"));
			$db->addOption("logicalOperator", array("AND"));	
			$db->addOption("param", array(":account_id" => $account));
			$hasParam = true;
			$resultUser = $db->select($hasParam);
			$db->close();
			
			if(is_object($resultUser)) {
				$result = array();
				while ($contas=$resultUser->fetch()) {
					$result[] = $contas;
				}
			} else {
				$return = array("data" => '');	
			}
	
			$return = array("data" => $result);		
			return json_encode($return);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function listMobileAction() {
		try {
			$db = new DatabaseHandler();

			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account = $my_account_id->user->account_id;
			
			$db->addOption("fields", array("id", "name", "login", "login", "password"));;
			$db->addOption("table", "users");
			$db->addOption("where", array("account_id = :account_id", "status = 1", "user_mobile = 1"));
			$db->addOption("logicalOperator", array("AND", "AND"));	
			$db->addOption("param", array(":account_id" => $account));
			$hasParam = true;
			$resultUser = $db->select($hasParam);
			$db->close();
			
			if(is_object($resultUser)) {
				$result = array();
				while ($contas=$resultUser->fetch()) {
					$result[] = $contas;
				}
			} else {
				$return = array("data" => '');	
			}
	
			$return = array("data" => $result);		
			return json_encode($return);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addAccessAction($perms, $userId, $master=2)	{
		try {
			foreach ($perms as $perm) {
				if((!isset($perm->access_level))) {
					$dados[] = array($userId, $perm, $master);
				} else {
					$dados[] = array($userId, $perm->widget_id, $perm->access_level);
				}
			}
			$db = new DatabaseHandler();		
			$db->addOption("table", "user_access");
			$db->addOption("fields", array("user_id", "widget_id", "access_level"));
			$db->addOption("param", $dados);
			$validateAccess = $db->insert(true,0,1);
			$db->close();
			
			if(!is_bool($validateAccess) || $validateAccess !== true) {
				return E_UNABLETOSAVEACCESS;
			}
			return true;
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
		
	public function editMyProfileAction($obj) {
		try {
			//Validando criação usuário para que ele não mude conta "inspecionando elemento"
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$validateId = $my_account_id->user->id;
			
			//Validando nova senha com a confirmação da nova senha
			if(($obj->password !== $obj->confirm) && ($obj->password == '' && $obj->confirm == '')) {
				return E_PASSWORDNOTEQUAL;
			}
			
			if($obj->id != $validateId) {
				return E_USERACCESSDENIED;
			}
	
			//Validando e-mail
			if(Validate::email($obj->login) === false){
				return E_INVALIDMAIL;
			}
			
			$accEmail = $my_account_id->user->login;
			//Verifica se o usuário está alterando o e-mail/login
			if($accEmail != $obj->login)
			{
				//Verificando se e-mail já esta cadastrado
				if($this->getLoginUserAction($obj->login)) {
					return E_USEREXISTS;
				}
			}
			
			if($obj->old_password == '') {
				return E_USERINFORMPASSWORD;
			}
			
			//Confirmando senha antiga
			$auth = new Authentication($my_account_id->user->login, $obj->old_password);		
			if($auth->passwordHash() != $my_account_id->user->password)
			{
				return E_USERINVALIDPASSWORD;
			}
			
			$modifyDate = date("Y-m-d H:i:s");
			
			if(($obj->old_password != '') && ($obj->password != '')) {
				//Confirmando senha antiga
				$auth = new Authentication($my_account_id->user->login, $obj->old_password);			
				if($auth->passwordHash() == $my_account_id->user->password) {
					$new_auth = new Authentication($obj->login, $obj->password);
					
					//Validando quantidade de caracteres da senha
					$passwordCount = strlen($obj->password);
					if($passwordCount < 6) {
						return E_USERPASSWORDLENGTH;
					} else {
						
						//Altera senha
						$password = $new_auth->passwordHash();
	
						$db = new DatabaseHandler();
						//EDITANDO CONTA
						$db->addOption("fields", array("name = :name", "password = :password", "login = :login", "modify_date = :modify_date"));
						$db->addOption("table", "users");
						$db->addOption("where", array("id = :id"));
						$db->addOption("param", array(":id" => $obj->id, ":name" => $obj->name, ":password" => $password, ":login" => $obj->login, ":modify_date" => $modifyDate,));
						$updateUser = $db->update();
						if(!is_bool($updateUser) || $updateUser !== true) {
							return E_UNABLETOUPDATEUSER;
						}
						
						//Envio de e-mail
						$this->sendEmailAction("Conta Mova", "Olá ".$obj->name."!<br />Sua conta foi alterada com sucesso!<br />Abaixo seguem seus novos dados:<br /><br />Login: ".$obj->login."<br />Senha: ".$obj->password, $obj->login);
					}
				} else {
					return E_USERINVALIDPASSWORD;	
				}
			} else if($accEmail != $obj->login) {
				$new_auth = new Authentication($obj->login, $obj->old_password);
				//Altera senha
				$password = $new_auth->passwordHash();		
				
				$db = new DatabaseHandler();
				//EDITANDO CONTA
				$db->addOption("fields", array("name = :name", "password = :password", "login = :login"));
				$db->addOption("table", "users");
				$db->addOption("where", array("id = :id"));
				$db->addOption("param", array(":id" => $obj->id, ":name" => $obj->name, ":password" => $password,":login" => $obj->login));
				$updateUser = $db->update();
				if(!is_bool($updateUser) || $updateUser !== true) {
					return E_UNABLETOUPDATEUSER;
				}
				
				//Envio de e-mail
				$this->sendEmailAction("Conta Mova", "Olá ".$obj->name."!<br />Sua conta foi alterada com sucesso!<br />Abaixo seguem seus novos dados:<br /><br />Login: ".$obj->login, $obj->login);
			} else  {
				$db = new DatabaseHandler();
				//EDITANDO CONTA
				$db->addOption("fields", array("name = :name", /*"password = :password", */"login = :login"));
				$db->addOption("table", "users");
				$db->addOption("where", array("id = :id"));
				$db->addOption("param", array(":id" => $obj->id, ":name" => $obj->name, /*":password" => $password, */":login" => $obj->login));
				$updateUser = $db->update();
				
				if(!is_bool($updateUser) || $updateUser !== true) {
					return E_UNABLETOUPDATEUSER;
				}
				
				//Envio de e-mail
				$this->sendEmailAction("Conta Mova", "Olá ".$obj->name."!<br />Sua conta foi alterada com sucesso!", $obj->login);
			}
			$db->close();
			return true;
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addAcessGroupsAction($groups, $userId) {
		try {
			foreach ($groups as $group) {
				$sg = $group->subgroup;
				$g = $group->group;
				if (!is_numeric($group->subgroup)) {
					$sg = 0;
				}
				if (!is_numeric($group->group)) {
					$g = 0;
				}
				$dados[] = array($sg, $g, $userId);
			}
	
			$db = new DatabaseHandler();
			$db->addOption("table", "user_group_access");
			$db->addOption("fields", array("subgroup_id", "group_id", "user_id"));
			$db->addOption("param", $dados);
			$validateAccess = $db->insert(true,0,1);
			$db->close();
			
			if(!is_bool($validateAccess) || $validateAccess !== true) {
				return E_UNABLETOADDGROUPACCESS;
			}
			
			return true;
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function deleteAction($id) {
		try {
			$db = new DatabaseHandler();
		
			//Excluindo seus acessos
			$db->addOption("table", "session_log");
			$db->addOption("where", array("users_id = :id"));
			$db->addOption("param", array(":id" => $id));
			$deleteSession = $db->delete();
		
			if(is_bool($deleteSession)) {
				//Excluindo seus acessos
				$db->addOption("table", "session");
				$db->addOption("where", array("users_id = :id"));
				$db->addOption("param", array(":id" => $id));
				$deleteLog = $db->delete();
				
				if(is_bool($deleteLog)) {
					//Excluindo seus acessos
					$db->addOption("table", "user_access");
					$db->addOption("where", array("user_id = :id"));
					$db->addOption("param", array(":id" => $id));
					$deleteAccess = $db->delete();
					
					if(is_bool($deleteAccess) && $deleteAccess === true) {
						//Excluindo o usuario
						$db->addOption("table", "users");
						$db->addOption("where", array("id = :id"));
						$db->addOption("param", array(":id" => $id));
						$deleteUser = $db->delete();
					} else {
						return E_FAILEDTOREMOVEUSER;
					}
				} else {
					return E_FAILEDTOREMOVEUSERACCESS;
				}
			} else {
				return E_FAILEDTOREMOVESESSIONLOG;
			}
			$db->close();
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function resetPasswordAction($userLogin) {
			try {
				$password = $this->random_password(4);
				//Criando hash do password
				$auth = new Authentication($userLogin, $password);
				$hashPassword = $auth->passwordHash();
	
				$db = new DatabaseHandler();
				$db->addOption("fields", array("password = :password"));
				$db->addOption("table", "users");
				$db->addOption("where", array("login = :login"));
				$db->addOption("param", array(":login" => $userLogin,":password" => $hashPassword));
				$updated = $db -> update();
				
				if(!$updated) {
					return false;
				}
				
				$this->sendEmailAction("Conta Mova", "Olá ".$userLogin."!<br />Você solicitou uma alteração de senha.<br/> Sua nova senha é:".$password, $userLogin);
				return true;
								
			} catch(Exception $e) {
				return false;
			}
	}
	
	public function forgotPasswordAction($userLogin) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("id, login, name"));
			$db->addOption("table", "users");
			$db->addOption("where", array("login = :login"));
			$db->addOption("param", array(":login" => $userLogin));
			$user = $db -> select(true);
			$user = $user->fetch();
			$db->close();
	
			if(!$user) {
				return false;
			}
	
			$link = $this->generateHashLinkAction($user->id, $user->login);
	
			$this->sendEmailAction("Esqueceu a senha?", "Olá ".$user->name."!<br />Você solicitou uma alteração de senha.<br/> Clique no link abaixo para alterar sua senha.<br />".$link."<br/><br/>Se você não solicitou alteração de senha, ignore o e-mail.", $userLogin);
			return true;
	
		} catch(Exception $e) {
			return false;
		}		
	}
	
	public function updatePasswordAction($obj) {
		try {
			
			//Validando se senhas são realmente iguais
			if( $obj->password !== $obj->confirm ) {
				return false;
			}

			//Validando quantidade de caracteres da senha
			$passwordCount = strlen($obj->password);
			if($passwordCount < 4) {
				return false;
			}
			
			$id = explode('&', $obj->param);
			$id = explode('!', $id[1]);
			$id = $id[0];
			
			//Pegando informações do usuário
			$db = new DatabaseHandler();
			$db->addOption("fields", array("*"));
			$db->addOption("table", "users");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(":id" => $id));
			$user = $db->select(true);
			$user = $user->fetch();
			//Hash da senha
			$new_auth = new Authentication($user->login, $obj->password);
			$password = $new_auth->passwordHash();
			//Alterando senha
			$db->addOption("fields", array("password = :password"));
			$db->addOption("table", "users");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(":id" => $id, ":password" => $password));
			$updatePassword = $db->update();
			$db->close();
			//Verificando se retorno foi true
			if($updatePassword !== true) {
				return false;
			}

			return true;
		} catch(Exception $e) {
			return false;
		}
	}	
	// ATUALIZANDO STATUS DO USUARIO EXCLUIDO
	public function deleteuserAction($obj) {
		try {
			$modifyDate = date("Y-m-d H:i:s");
					
			$db = new DatabaseHandler();
			$db -> addOption("table", "users");
			$db -> addOption("fields", array("status = :status","modify_date = :modify_date"));
			$db -> addOption("where", array("id = :id", "status = 1"));
			$db -> addOption("logicalOperator", array("AND"));	
			$db -> addOption("param", array(":modify_date" => $modifyDate,
											":id" => $obj -> id, 
											":status" => $obj -> status));	
			
			$update = $db->update();
			$db->close();
			
			if (is_bool($update) && $update === true) {
				return json_encode('Usuário removida com sucesso.');
			}else{
				return E_FAILEDTOREMOVEUSER;
			}

		}catch(Exception $e){
			return E_INTERNAL;
		}
	}
	
	public function printUserAction($obj) {
		$content = '<style>
						table { border: 1px solid #999; border-bottom: none; width: 100%; }
						thead { border: 5px solid #000; }
						tbody td { border: 1px solid #ccc; }
					</style>';
		
		$content = $content.'<table><thead"><th>ID</th><th>Nome</th><th>Login</th></thead><tbody>';
		
		
		foreach ($obj as $row) {
			if(empty($row->id)){ $row->id = ''; }
			if(empty($row->name)){ $row->name = ''; }
			if(empty($row->login)){ $row->login = ''; }
			$content = $content."<tr><td>".$row->id."</td><td>".$row->name."</td><td>".$row->login."</td></tr>";
		}
		
		$content = $content.'</tbody></table>';
		
		return $content;
	}
}
