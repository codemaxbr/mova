<?php 
class reportstoppoiController extends defaultController
{
	public function indexAction($infos=null) {
		date_default_timezone_set("America/Sao_Paulo");
		$unit = new unitController();
		$return = array();
		$return['groupinfo'] = $unit -> listGroupAction();
		$return['subgroupinfo'] = $unit -> listSubGroupAction();
		$return['unities'] = $unit -> listAction();
		$return['date'] = date('d/m/Y');
		$return['hour'] = date('H:i');
		
		return json_encode($return);	
	}
	
	public function searchAction($obj) {		
		try {			
			if(!isset($_SESSION)) {
				session_start();
			}
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;
			$user_id = $_SESSION['user'] -> getUserId();
			
			//controle de consulta
			//sempre retornando de 100 em 100 a partir do ultimo registro encontrado
			$andlastId = '';
			if( $obj->last_id != null && $obj->last_id != ''  && $obj->last_id != '0' ){
				$andlastId = " AND cs.id < ".$obj->last_id;
			}
			$limit = '';
			$orderby = ' cs.initial_time ASC, tu.label';
			if( $obj->limit != null && $obj->limit != '' && $obj->limit != '0' ){
				$limit = " LIMIT ".$obj->limit;
				$orderby = ' cs.initial_time ASC, tu.label';
			}

			//pegando filtro de data/hora
			$start_date = $this->formatDateTimeAction($obj->start_day, $obj->start_hour, '/'); 
			$end_date = $this->formatDateTimeAction($obj->end_day, $obj->end_hour, '/');
			$andDate = " AND (cs.initial_time >= '$start_date' AND cs.initial_time <= '$end_date') ";

			//unidades
			$andUnits = '';
			$units = '0';
			foreach ($obj->units as $unit) {
				$units .= ','.$unit;
			}
			if( ($units != '0,0') && ($units != '0') ){
				$andUnits = " AND cs.unit_id IN ($units) ";
			}
			//group
			$andGroups = '';
			$groups = '0';
			foreach ($obj->group as $group) {
				$groups .= ','.$group;
			}
			if( ($groups != '0,0') && ($groups != '0') ){
				$andGroups = " AND cs.group_id IN ($groups) ";
			}
			//Subgroup
			$andSubGroups = '';
			$subgroups = '0';
			foreach ($obj->subgroup as $subgroup) {
				$subgroups .= ','.$subgroup;
			}
			if( ($subgroups != '0,0') && ($subgroups != '0') ){
				$andSubGroups = " AND cs.subgroup_id IN ($subgroups) ";
			}

			$db = new DatabaseHandler();			
			$db -> addOption("table", 	"SELECT 
											cs.id, 
											cs.unit_id as unit_id,
											cs.initial_history_id as devst_id, 
											tu.label, 
											CASE 
												WHEN (to_char(cs.initial_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(cs.initial_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cs.initial_time, 'DD/MM/YY') 
											END	AS data_inicio, 
											to_char(cs.initial_time, 'DD/MM/YY HH24:MI') as full_date_begin, 
											to_char(cs.initial_time, 'DD/MM/YY') as data_report_inicio, 
											to_char(cs.initial_time, 'HH24:MI') as hora_min_inicio, 
											cs.initial_time,
											CASE 
												WHEN (to_char(cs.final_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(cs.final_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cs.final_time, 'DD/MM/YY') 
											END	AS data_fim, 
											to_char(cs.final_time, 'DD/MM/YY HH24:MI') as full_date_end, 
											to_char(cs.final_time, 'DD/MM/YY') as data_report_fim, 
											to_char(cs.final_time, 'HH24:MI') as hora_min_fim, 
											cs.final_time,
											cs.final_time - cs.initial_time as total_parado_poi,
											cs.initial_lon as initial_longitude, 
											cs.initial_lat as initial_latitude, 
											coalesce(cs.initial_address,'') as initial_full_address,
											coalesce(cs.initial_poi_name,'') as initial_poi_name,
											coalesce(cs.initial_area_name,'') as initial_area_name,
											coalesce(cs.total_time,0) as total_time,
											poi.name as final_poi
										FROM
											con_stop as cs
											INNER JOIN tracked_unit as tu ON tu.id = cs.unit_id
											INNER JOIN poi ON poi.id = cs.final_poi_id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = tu.group_id AND cs.subgroup_id = usgrac.subgroup_id AND usgrac.user_id = $user_id
										WHERE 
											
											cs.account_id = $account_id
											AND cs.move_stop = 0
											AND initial_poi_name IS NOT NULL
											$andUnits
											$andDate
											$andGroups
											$andSubGroups
											$andlastId
										ORDER BY 
											$orderby
										$limit");

			$resultSearch = $db -> select(false, 0, true);
			//echo $resultSearch;
			//echo $db -> query();
			$db->close();
			
			if(is_object($resultSearch)) {
				$result = array();
				while($search = $resultSearch->fetch()) {
					$result[] = $search;
				}				
				return json_encode($result);
			} else {
				if($resultSearch === false) {
					return json_encode(array('text' => 'Não foram encontrados registro para este filtro.'));
				} else {
					return E_UNABLETOGETPARENTWIDGETS;
				}
			}
		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getConStopByHistIdAction($hist_id) {		
		try {			
			if(!isset($_SESSION)) {
				session_start();
			}
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;

			$db = new DatabaseHandler();			
			$db -> addOption("table", 	"SELECT 
											cs.id, 
											cs.unit_id as unit_id,
											cs.initial_history_id as devst_id, 
											tu.label, 

											CASE 
												WHEN (to_char(cs.initial_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(cs.initial_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cs.initial_time, 'DD/MM/YY') 
											END	AS data_inicio, 
											to_char(cs.initial_time, 'HH24:MI') as hora_min_inicio, 
											cs.initial_time,

											CASE 
												WHEN (to_char(cs.final_time , 'DD/MM/YYYY') = to_char(now(), 'DD/MM/YYYY')) THEN 'Hoje'  
												WHEN (to_char(cs.final_time, 'DD/MM/YYYY') = to_char((now()::date - interval '1 day'), 'DD/MM/YYYY')) THEN 'Ontem' 
												ELSE to_char(cs.final_time, 'DD/MM/YY') 
											END	AS data_fim, 
											to_char(cs.final_time, 'HH24:MI') as hora_min_fim, 
											cs.final_time,

											coalesce(cs.initial_poi_name,'') as initial_poi_name,
											coalesce(cs.initial_area_name,'') as initial_area_name,

											coalesce(cs.total_time,0) as total_time
										FROM
											con_stop as cs
											INNER JOIN tracked_unit as tu ON tu.id = cs.unit_id
										WHERE 
											cs.account_id = $account_id
											AND cs.initial_history_id = $hist_id");

			$resultSearch = $db -> select(false, 0, true);
			//echo $resultSearch;
			//echo $db -> query();
			$db->close();
			
			if(is_object($resultSearch)) {
				$result = array();
				while($search = $resultSearch->fetch()) {
					$result[] = $search;
				}				
				return json_encode($result);
			} else {
				if($resultSearch === false) {
					return json_encode(array('text' => 'Não foram encontrados registro da parada.'));
				} else {
					return E_UNABLETOGETPARENTWIDGETS;
				}
			}
		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}

}