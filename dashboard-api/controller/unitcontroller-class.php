<?php

Class UnitController extends defaultController {

    public function getUnitById($unit_id) {
        return json_decode($this->createEditFormAction($unit_id));
    }

	/*
	   UNIDADE --> DEVICE

	   Recebe uma UNIDADE e retorna um DEVICE a que a UNIDADE é correspondente.

	   Exemplos:  
	   getDeviceByUnit("2381")
	     onde 2381 é o tracked_unit_id da tabela tracked_unit_device.
	*/
	public function getDeviceByUnit($unit){
		if (!strlen($unit) || !is_integer(intval($unit)))
			throw new Exception("unit_id tem que ser número inteiro.");

		$tracked_unit_id = intval($unit);

		$db = new DatabaseHandler();
		$db->select_v0("SELECT TUD.id as TUD_ID, DEV.id as DEV_ID, DM.id as DM_ID
     					, DM.name as DM_NAME
     					, DEV.identifier as DEV_IDENTIFIER
    					FROM tracked_unit_device TUD 
      				JOIN device DEV on DEV.id = TUD.device_id
      				JOIN device_model DM on DM.id = DEV.device_model_id
   					WHERE TUD.status = 1 
   						AND TUD.device_primary IN (1)  
          				AND TUD.tracked_unit_id = {$tracked_unit_id}");

		return $db->fetch_all_v0();
	}

	public function indexAction($infos = null) {
		
		$widget = new widgetController();
		$return = array();

		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());
		$return['groupinfo'] = $this -> listGroupAction();
		$return['typeinfo'] = $this -> listTypeAction();
		$return['productinfo'] = $this -> listProductsAction();

		return json_encode($return);
	}

	public function listTrackedUnitAction($subgroup = null) {
		try {
			$user_id = $_SESSION['user'] -> getUserId();

			if(!is_null($subgroup)) {
				if($subgroup->subGroupIds[0] == '0' || $subgroup->subGroupIds[0] == '') {
					$subgroup->subGroupIds = '';
				}
			}

			$db = new DatabaseHandler();
			$db->addOption("fields", array("tru.id as id", "tru.label", "coalesce(tru.label2, '') as label2", "gr.name as group_name", "coalesce(d.identifier, '') as identifier"));
			$db->addOption("table", "tracked_unit tru");
			$db->addOption("joinTable", array("account as ac", '"group" as gr', "tracked_unit_device tud", "device d", "user_group_access usgrac"));
			$db->addOption("joinTableOn", array(
												"tru.account_id = ac.id", 
												"tru.group_id = gr.id", 
												"tud.tracked_unit_id = tru.id AND tud.status = 1 AND tud.device_primary = 1", 
												"tud.device_id = d.id", 
												"usgrac.group_id = tru.group_id AND usgrac.subgroup_id = tru.subgroup_id AND usgrac.user_id = $user_id"
												));
			$db->addOption("joinTableType", array("INNER JOIN", "INNER JOIN", "LEFT JOIN", "LEFT JOIN", "INNER JOIN"));
			if($subgroup == null || $subgroup->subGroupIds == '') {
				$db->addOption("where", array("tru.account_id = :id", "tru.status = 1"));
				$db->addOption("logicalOperator", array("AND"));			
			} else {
				$sgids='';
				$last = count($subgroup->subGroupIds);
				$i = 0;
				$and = ', ';
				$in = '(';
				
				foreach ($subgroup->subGroupIds as $sgid) {
					$i++;
					if($i == $last) {
						$and = ')';
					}
					$sgids = $in.$sgids."'".$sgid."'".$and;
					$in = '';
				}
				
				$db->addOption("where", array("tru.account_id = :id", "tru.subgroup_id in $sgids", "tru.status = 1"));
				$db->addOption("logicalOperator", array("AND", "AND"));			
			}
			$db->addOption("param", array(":id" => $_SESSION['user'] -> getAccount()));
			$db->addOption("orderBy", array("tru.id"));
			$db->addOption("orderBySort", array("desc"));		

			$resultUnit = $db -> select(true, 1);
			$db->close();

			if(is_object($resultUnit)) {
				$result = array();
				while ($units = $resultUnit -> fetch()) {
					$result[] = $units;
				}
				if($subgroup == null) {
					$return = array("data" => $result);
				} else {
					$return = $result;
				}
			} else {
				//if($subgroup == null) {
					$return = array("data" => "");
				//} else {
				//	return E_UNABLETOLOADUNITLIST;
				//}
			}
			
			return json_encode($return);

		} catch(Exception $e) {

			return E_INTERNAL;

		}
	}
	
	public function listAction($param = null) {	
		try {
			$user_id = $_SESSION['user'] -> getUserId();
			$accountId 	= $_SESSION['user']	-> getAccount();

			$db = new DatabaseHandler();
			$db->addOption("fields", array("t.*", "t.label as label", "t.label as name"));
			$db->addOption("table", 'tracked_unit t');
			if($param === null) {
				$db->addOption("where", array('t.account_id = :account_id', "t.status = 1"));
				$db->addOption("logicalOperator", array('AND'));
				$db->addOption("param", array(':account_id' => $_SESSION['user']->getAccount()));
			} else {
				$db->addOption("where", array('t.account_id = :account_id', "t.status = 1", "t.group_id = :group_id"));
				$db->addOption("logicalOperator", array('AND', 'AND'));
				$db->addOption("param", array(':account_id' => $_SESSION['user']->getAccount(), ':group_id' =>  $param));
			}
			$select = $db->select(true, 1);
			// $db -> addOption("table", "SELECT
			// 								t.*, 
			// 								t.label as label,
			// 								t.label as name,
			// 								gp.name as group_name
			// 							FROM 
			// 								tracked_unit t
			// 								INNER JOIN \"group\" gp ON t.group_id = gp.id
			// 								INNER JOIN user_group_access usgrac ON usgrac.group_id = t.group_id AND usgrac.subgroup_id = t.subgroup_id AND usgrac.user_id = $user_id
			// 							WHERE 
			// 								t.status = 1
			// 								AND t.account_id = $accountId
			// 							ORDER BY t.label");
			
			// $select = $db -> select(false, 0, true);
			$db->close();

			if(is_object($select)) {
				$result = array();
				while ($contas=$select->fetch()) {
					$result[] = $contas;
				}
				
				return $result;
			} else {
				return E_UNABLETOLISTCLIENT;
			}	
		} catch(Exception $e) {
			return E_INTERNAL;
		}	
	}
	
	public function listWithMemoryDriverAction($param = null) {	
		try {
			$account_id = $_SESSION['user']	-> getAccount();
			$user_id 	= $_SESSION['user']	-> getUserId();
			$group_id 	= $param;

			$db = new DatabaseHandler();
			$db -> addOption("table", "	SELECT DISTINCT t.*, t.label as label, t.label as name 
										FROM tracked_unit t
											INNER JOIN driver_tracked_unit dtu ON dtu.unit_id = t.id AND dtu.driver_id = 0 
										WHERE 
											t.account_id = $account_id
											AND t.group_id = $group_id
										ORDER BY t.label");

			$select = $db -> select(false, 0, true);
			$db->close();

			$result = array();
			if(is_object($select)) {
				while ($contas = $select -> fetch()) {
					$result[] = $contas;
				}				
			}
			return $result;

		} catch(Exception $e) {
			return E_INTERNAL;
		}	
	}

	public function listByGroupAction($type=null) {
		$driver = new driversController();

		try {
			$accountId 	= $_SESSION['user']	-> getAccount();
			$user_id 	= $_SESSION['user']	-> getUserId();
			
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT gp.id as g_id, gp.name 
									FROM \"group\" gp 
										INNER JOIN user_group_access uga ON uga.group_id = gp.id AND uga.user_id = $user_id 
									WHERE gp.account_id = $accountId
									ORDER BY gp.name");

			$select = $db -> select(false, 0, true);
			$db->close();
			
			if(is_object($select)) {
				$result = array();
				while($conta=$select->fetch()) {
					if($type == "drivers"){
						$group = array($conta->g_id);
						$conta->children = json_decode( $driver->listAction( (object) array("group_id" => $group, "typeData" => false) ));
					}else{
						$conta->children = $this->listWithMemoryDriverAction($conta->g_id);
					}
					
					$conta->parent_id = $conta->g_id;
					$conta->data = array('file' => false);
					$result[] = $conta;
				}
		
				$tree = array("text" => "Unidades", "state" => array("opened" => true));
				$tree["children"] = $result;
				$json = json_encode($tree);
				//Trocando nome dos campos de arrays para funcionar no plugin jsTree
				$return = str_replace("name", "text", $json);
				//Passando para json
				$return = json_encode($return);
				//Imprimindo para o plugin jsTree
				return json_decode($return);
			}else{
				return E_UNABLETOLISTACCOUNTTREE;
			}
				
		}catch(Exception $e){
			return E_INTERNAL;
		}
	}

	public function createEditFormAction($object) {
		$unit_id = $object;
		try {
			$db = new DatabaseHandler();
			$db->addOption("table", "tracked_unit");
			$db->addOption("fields", array("*"));
			$db->addOption("where", array("id = :id", "status = 1"));
			$db->addOption("logicalOperator", array('AND'));
			$db->addOption("param", array(":id" => $object));
			$select = $db -> select(true);

			if (is_object($select)) {
				$trackedUnit =  $select -> fetch();
				
				$result[] = $trackedUnit;
				
				// Criando atributo vazio para devices
				$result[0]->devices  = "";
				
				// Recuperando os dispositivos associados a unidade
				$db -> addOption("table", "tracked_unit_device tud");				
				$db -> addOption("fields", array("dev.id","dev.identifier"));	
				$db -> addOption("joinTable", array("device dev"));	
				$db -> addOption("joinTableType", array("INNER JOIN"));
				$db -> addOption("joinTableOn", array("tud.device_id = dev.id"));		
				$db -> addOption("where", array("tud.tracked_unit_id = :tracked_unit","tud.status = 1"));
				$db -> addOption("logicalOperator", array("AND"));		
				$db -> addOption("param", array(":tracked_unit" => $trackedUnit -> id));	
				$db -> addOption("orderBy", array("tud.id"));	
				
				$select = $db -> select(true);
				$db->close();
				//Verifica se existem dispositivos cadastrados
				if(is_object($select)){
					
					$unitDevices = $select -> fetchAll();
					// Adicionando os dispositivos no atributo criado, mantendo o mesmo nivel e criando um JSON correto
					$result[0]->devices = array($unitDevices);					
				}
				//ADICIONA LISTA DE MOTORISTAS ASSOCIADOS A ESSA UNIDADE
				$driver = $this -> getDriverAssociatesAction($unit_id);
				$result[0]->driver = array($driver);
				
				return json_encode($result);
				
			} else {
				return E_UNABLETOLOADUNIT;
			}

		} catch(Exception $e) {
			return $e -> getMessage();
		}
	}

	public function addAction($obj) {
		try {
			
			$returnMessage = "";
			$returnedDevices = array();
			$unitCheck = $this -> getUnitByLabelAction($obj -> label, $obj -> account);
			
			//Verificando se a unidade com a label informada ja existes
			if ($unitCheck === FALSE) {

				$db = new DatabaseHandler();

				$db -> addOption("table", "tracked_unit");
				$db -> addOption("fields", array("label",
													"label2", 
													"account_id", 
													"unit_category_id", 
													"subgroup_id", 
													"group_id", 
													"intranet_product_id", 
													"os_num", 
													"os_id", 
													"cli_fat_id",
													"timezone",
													"status",
													"unit_type_id",
													"dst"));
				$db -> addOption("param", array(":label" => $obj -> label, 
												":label2" => $obj -> label2 ,
												":account_id" => $obj -> account, 
												":unit_category_id" => $obj -> category, 
												":subgroup_id" => $obj -> subgroup, 
												":group_id" => $obj -> group, 
												":intranet_product_id" => $obj -> product, 
												":os_num" => $obj -> osnum, 
												":os_id" => $obj -> osid, 
												":cli_fat_id" => $obj -> clifatid, 
												":timezone" => $obj -> timezone,
												":status" => "1",
												":dst" => $obj -> dst,
												":unit_type_id" => $obj -> unit_type));
				$insert = $db -> insert();
				$trackedUnitId = $db->lastInsertId('tracked_unit_id_seq');
				$db->close();

				if (is_bool($insert) && $insert === true) {
					if(isset($obj->device) && sizeof($obj->device) > 0) {
						$primary = true;
						foreach ($obj->device as $deviceId) {
							$addDevice = $this -> addTrackedUnitDevice($trackedUnitId, $deviceId, $primary);
							//Adicionando em um array os devices que tiveram problemas no cadastro
							if (!is_bool($addDevice)) {
								$returnedDevices[] = $addDevice;
							}
							$primary = false;
						}
	
						if (count($returnedDevices) > 0) {
							$returnMessage = E_DEVICEEXISTS;
							
							foreach ($returnedDevices as $device) {
								$returnMessage .= " [" . $device . "]";
							}
	
							return $returnMessage;
						} else {
							return json_encode('Unidade cadastrada com sucesso.');
						}
					}	
					return json_encode('Unidade cadastrada com sucesso.');
				}
			} else {
				return E_UNITEXISTS;
			}
		} catch(Exception $e) {
			//TODO Adicionar log de erros
			return E_INTERNAL;
		}
	}

	public function editAction($obj) {
		try {
			date_default_timezone_set("America/Sao_Paulo");

			$returnMessage = "";
			$returnedDevices = array();
			$returnedEditDevices = array();
			$unitCheck = $this -> getUnitByLabelAction($obj -> label, $obj -> account);
			
			// Verifica se ja existe uma unidade cadastrada com a placa informada
			// e caso exista se a unidade existente nao e a mesma que esta sendo atualizada
			if($unitCheck !== FALSE) {
				if($unitCheck -> id != $obj -> tracked_unit){
					return E_UNITEXISTS;
				}
			}
			
			if($obj -> osid == '') {
				$obj -> osid = null;
				$obj -> osnum = null;
				$obj -> clifatid = null;
			}
	
			$db = new DatabaseHandler();
			$db -> addOption("table", "tracked_unit");
			$db -> addOption("fields", array(	"label = :label",
												"label2 = :label2", 
												"account_id = :account_id", 
												"unit_category_id = :unit_category_id", 
												"subgroup_id = :subgroup_id", 
												"group_id = :group_id", 
											 	"intranet_product_id = :intranet_product_id", 
											 	"os_num = :os_num", 
											 	"os_id = :os_id", 
											 	"cli_fat_id = :cli_fat_id",
											 	"timezone = :timezone",
											 	"status = :status",
											 	"unit_type_id = :unit_type", 
											 	"dst = :dst"));
			$db -> addOption("where", array("id = :tracked_unit"));
			$db -> addOption("param", array(":label" => $obj -> label, 
											":label2" => $obj -> label2 ,
											":account_id" => $obj -> account, 
											":unit_category_id" => $obj -> category, 
											":subgroup_id" => $obj -> subgroup, 
											":group_id" => $obj -> group, 
											":intranet_product_id" => $obj -> product, 
											":os_num" => $obj -> osnum, 
											":os_id" => $obj -> osid, 
											":cli_fat_id" => $obj -> clifatid, 
											":timezone" => $obj -> timezone,
											":status" => $obj -> status,
											":tracked_unit" => $obj -> tracked_unit,
											":unit_type" => $obj -> unit_type,
											":dst" => $obj->dst));
			
			$update = $db -> update();
			$db->close();
			
			if (is_bool($update) && $update == true) {
					
				$trackedUnitId = $obj -> tracked_unit;
			
			  	if(isset($obj -> device) && count($obj -> device) > 0){
				  	$primary = true;
					foreach ($obj -> device as $deviceId) {
						$addDevice = $this -> addTrackedUnitDevice($trackedUnitId, $deviceId, $primary);
						//Adicionando em um array os devices que tiveram problemas no cadastro
						if (!is_bool($addDevice)) {
							$returnedDevices[] = $addDevice;
						}
						$primary = false;			
				  	}
					// Tratando retorno com erro
					if (count($returnedEditDevices) > 0) {
						$returnMessage .= E_DEVICEEXISTS;

						foreach ($returnedEditDevices as $device) {		
							$returnMessage .= " [" . $device . "]";
						}							
					}
				}
				
				if(strlen($returnMessage) > 0) {
					return $returnMessage;	
				} else {
					return json_encode('Unidade alterada com sucesso.');
				}
			} else {
				//TODO Adicionar log de erros
				return E_UNABLETOUPDATE;
			}
		} catch(Exception $e) {
			//TODO Adicionar log de erros
			echo $e -> getMessage();
			return E_INTERNAL;
		}	
	}
	
	public function getTrackedUnitDevice($unitId, $deviceId) {
		try {

			$db = new DatabaseHandler();
			$db -> addOption("table", "tracked_unit_device");
			$db -> addOption("fields", array("device_id"));
			$db -> addOption("where", array("tracked_unit_id = :unit_id", 
											"device_id = :device_id", 
											"status = 1"));
			$db -> addOption("logicalOperator", array("AND", "AND"));	
			$db -> addOption("param", array(":unit_id" => $unitId, 
											":device_id" => $deviceId));
			$resultDevice = $db -> select(true, 1);
			$db->close();
			
			$result = array();
			if(is_object($resultDevice)) {				
				return true;
			} else {
				return false;
			}

		} catch(Exception $e) {
			return false;
		}
	}

	public function addTrackedUnitDevice($unitId, $deviceId, $primary=false) {

		try {
			date_default_timezone_set("America/Sao_Paulo");
			//$deviceController = new DeviceController();

			//Verificando se o dispositivo ja foi cadastrado
			$deviceByUnit = $this -> getTrackedUnitDevice($unitId, $deviceId);
			
		    if (!$deviceByUnit) {
				$primaryValue = 0;
				if($primary){
					$primaryValue = 1;
				}
				//Equivalente a NOW() no sql
				$associationDate = date("Y-m-d H:i:s");
				$db = new DatabaseHandler();
				$db -> addOption("table", "tracked_unit_device");
				$db -> addOption("fields", array("tracked_unit_id", 
													"device_id", 
													"association_date", 
													"status", 
													"user_id", 
													"device_primary"
												));
				$db -> addOption("param", array(":tracked_unit_id" => $unitId, 
												":device_id" => $deviceId, 
												":association_date" => $associationDate, 
												":status" => 1, 
												":user_id" => $_SESSION['user'] -> getUserId(), 
												":device_primary" => $primaryValue
												));
				$insert = $db -> insert();
				$db->close();
				if (is_bool($insert) && $insert === true) {
					if($primary){
						$loadMemory = $this -> handleAllocateMemory($unitId);
					}
					return true;
				} else {
					return $deviceId;
				}

			} else {
				return false;
			}

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	/**
	 * Recupera o id da unidade através da label passada por parametro
	 * @param Mixed Object Unit, boolean false
	 */
	public function getUnitByLabelAction($label, $account_id) {

		$db = new DatabaseHandler();
		$db -> addOption("table", "tracked_unit");
		$db -> addOption("fields", array("id", "account_id"));
		$db -> addOption("where", array("label = :label", "account_id = :account_id", "status = 1"));
		$db -> addOption("logicalOperator", array("AND", "AND"));	
		$db -> addOption("param", array(":label" => $label, ":account_id" => $account_id));
		$resultUnit = $db -> select(true, 1);
		$db->close();

		if (is_object($resultUnit)) {
			return $resultUnit -> fetch();
		} else {
			return false;
		}
	}
	
	public function listGroupAction() {

		$accountId = $_SESSION['user'] -> getAccount(); //pegando id da conta que esta logada
		$user_id = $_SESSION['user'] -> getUserId();

		$db = new DatabaseHandler();
		$db -> addOption("table", "SELECT DISTINCT gp.id, gp.name 
									FROM \"group\" gp 
										INNER JOIN user_group_access uga ON uga.group_id = gp.id AND uga.user_id = $user_id 
									WHERE gp.account_id = $accountId
									ORDER BY gp.name");

		$resultUnit = $db -> select(false, 0, true);
		$db->close();

		$result = array();
		if (is_object($resultUnit)) {
			while ($units = $resultUnit -> fetch()) {
				$result[] = $units;
			}
		}
		$return = array("groupinfo" => $result);		
		return json_encode($return);
	}

	public function listSubGroupAction($groupId = null) {
		$where = "";
		if( isset($groupId) ){
			$where = " AND gp.id = $groupId ";
		}

		$accountId = $_SESSION['user'] -> getAccount(); //pegando id da conta que esta logada
		$user_id = $_SESSION['user'] -> getUserId();

		$db = new DatabaseHandler();

		$db -> addOption("table", "SELECT DISTINCT sg.id, sg.name 
									FROM 
										\"group\" gp 
										INNER JOIN subgroup sg ON sg.group_id = gp.id
										INNER JOIN user_group_access usgrac ON usgrac.group_id = gp.id AND usgrac.user_id = $user_id
									WHERE 
										gp.account_id = $accountId
										$where
									ORDER BY sg.name");

		$resultGroup = $db -> select(false, 0, true);
		$result = array();
		if (is_object($resultGroup)) {
			while ($group = $resultGroup -> fetch()) {
				$result[] = $group;
			}
		}
		$db->close();

		$return = array("subgroupinfo" => $result);		
		return json_encode($return);
	}

	public function checkDeviceAction($deviceIdentifier) {
		try {
			$account_id = $_SESSION['user'] -> getAccount();

			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT 
											dev.id as device_id,
											dev.identifier, 
											tud.id as tracked_id
										FROM 
											device dev
											LEFT JOIN tracked_unit_device tud ON dev.id = tud.device_id AND tud.status = 1
										WHERE
											dev.status = 1
											AND dev.account_id = $account_id
											AND dev.identifier = '$deviceIdentifier'");

			$resultDevice = $db -> select(false, 0, true);
			
			$db->close();
			
			if(is_object($resultDevice)) {
				$result = array();
				while ($units = $resultDevice->fetch()) {
					$result[] = $units;
				}
					
				return json_encode($result);
			} else {
				echo json_encode(false);			
			}	
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function listCategoryAction($typeId) {
		$db = new DatabaseHandler();
		$db -> addOption("fields", array("id", "name"));
		$db -> addOption("table", "unit_category");
		$db -> addOption("where", array("unit_type_id = :id"));
		$db -> addOption("param", array(":id" => $typeId));
		$resultCategory = $db -> select(true, 1);
		$db->close();
		
		$result = array();
		
		if(is_object($resultCategory)) {
			while ($categories = $resultCategory -> fetch()) {
				$result[] = $categories;
			}	
		}

		return json_encode($result);
	}

	public function listTypeAction() {
		$db = new DatabaseHandler();
		$db -> addOption("fields", array("id", "name"));
		$db -> addOption("table", "unit_type");
		$resultType = $db -> select(false, 1);
		$db->close();

		$result = array();
		while ($types = $resultType -> fetch()) {
			$result[] = $types;
		}

		$return = array("typeinfo" => $result);		
		return json_encode($return);
	}

	public function listProductsAction() {

		$db = new DatabaseHandler();
		$db -> addOption("fields", array("id", "name"));
		$db -> addOption("table", "intranet.product");
		$resultType = $db -> select(false, 1);

		$result = array();
		if(is_object($resultType)) {
			while ($types = $resultType -> fetch()) {
			$result[] = $types;
			}	
		}

		$db -> close();

		$return = array("productinfo" => $result);		
		return json_encode($return);
	}

	public function loadCategoriesAction() {
		$db = new DatabaseHandler();
		$db -> addOption("fields", array("id", "name"));
		$db -> addOption("table", "unit_category");
		$db -> addOption("where", array("unit_type_id = :id"));
		$db -> addOption("param", array(":id" => $typeId));

		$resultCategory = $db -> select(true, 1);
		$result = array();

		if(is_object($resultCategory)){
			while ($categories = $resultCategory -> fetch()) {
				$result[] = $categories;
			}
		}
		
		return $result;
	}
	
	public function loadTypeByCategoryAction($categoryId) {
		try {
			
			$db = new DatabaseHandler();
			$db -> addOption("table", "unit_category uc");
			$db -> addOption("fields", array("ut.id"));
			$db -> addOption("joinTable", array("unit_type ut"));
			$db -> addOption("joinTableType", array("INNER JOIN"));
			$db -> addOption("joinTableOn", array("ut.id = uc.unit_type_id"));
			$db -> addOption("where", array("uc.id = :id"));
			$db -> addOption("param", array(":id" => $categoryId));	
			$type = $db -> select(true);
			$db->close();
			
			if(is_object($type)) {
				$result[] = $type -> fetch();
			}
			
			return json_encode($result);
		} catch(Exception $e) {
			//FIXME mensagem de erro
			return $e -> getMessage();
		}	
	}
	
	public function loadOSAction($osNumber) {
		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "intranet.os as os");
			$db -> addOption("fields", array("os.id","os.num_os","os.placa","os.product","cl.id as clifatid","cl.name"));
			$db -> addOption("joinTable", array("intranet.client as cl"));
			$db -> addOption("joinTableType", array("INNER JOIN"));
			$db -> addOption("joinTableOn", array("os.cliente = cl.id"));
			$db -> addOption("where", array("num_os = :os"));
			$db -> addOption("param", array(":os" => $osNumber));
			$resultOS = $db -> select(true, 1);
			$db->close();
			
			if(is_object($resultOS)) {
				$result = array();
				while ($os = $resultOS -> fetch()) {
					$result[] = $os;
				}	
			} else {
				return false;
			}
			
			return json_encode($result);
			
		} catch(Exception $e) {
			//FIXME Mensagem de erro
			return $e -> getMessage();
		}
	}
	/**
	 * QUANDO UM DISPOSITIVO FOR ASSOCIADO A UMA UNIDADE, DEVE SER CRIADO UM REGISTRO PARA CADA ESPAÇO DE MEMORIA DISPONIVEL NO MODULO.
	 * A QUANTIDADE DE ESPAÇO ESTÁ DEFINIDA NO MODELO DO MODULO
	 * @param $unitId - ID DA UNIDADE
	 */
	public function handleAllocateMemory($unitId){
		try{	
			$return = true;
			$db = new DatabaseHandler();

			$db->addOption("table", "	SELECT 
											dm.qtd_driver 
										FROM 
											device_model dm
											INNER JOIN device d ON d.device_model_id = dm.id 
											INNER JOIN tracked_unit_device tud ON tud.device_id = d.id AND tud.device_primary = 1 AND tud.status = 1
										WHERE 
											tud.tracked_unit_id = $unitId
											AND (SELECT count(id) FROM driver_tracked_unit WHERE unit_id = $unitId) = 0");

			$resultSelect = $db -> select(false, 0, true);

			if (is_object($resultSelect)) {
				$row = $resultSelect -> fetch();
				$qtdMemory = $row -> qtd_driver;

				if($qtdMemory > 0){
					$sqlInsert = "INSERT INTO driver_tracked_unit (driver_id, unit_id, status, memory_position) VALUES (0, $unitId, 0, 1)";

					for ($i = 2; $i <= $qtdMemory; $i++) {
						$sqlInsert .= ", (0, $unitId, 0, $i)";
					}	

					$db -> addOption("table", $sqlInsert);
					$resultInsert = $db -> insert(false, 0, 0, true);
				}
			}else{
				$db -> addOption("table", "UPDATE driver_tracked_unit SET status = 0 WHERE unit_id = $unitId" );
				$resultUpdate = $db -> update(false, true);
			}

			$db -> close();
			return $return;
						
		}catch(Exception $e){
			return false;
		}
	}
	/**
	 * O MODULO MAIS ANTIGO CADASTRADO DEVE SER SEMPRE O MODULO PRINCIPAL DA UNIDADE
	 * QUANDO UM MODULO PRINCIPAL FOR REMOVIDO, O MAIS ANTIGO DEPOIS DELE DEVE TOMAR SEU LUGAR COMO PRINCIPAL
	 * @param $unitId - ID DA UNIDADE; $deviceId - ID DO DISPOSITIVO(MODULO)
	 */
	public function handleUpdatePrimary($unitId, $deviceId){
		try{	
			$return = true;		
			$db = new DatabaseHandler();

			$db -> addOption("table", "	SELECT 
											id
										FROM
											tracked_unit_device
										WHERE
											tracked_unit_id = $unitId
											AND device_primary = 1
											AND status = 1");
			
			$sqlResult = $db -> select(false, 0, true);

			if (!is_object($sqlResult)) {	
				$db -> addOption("table", "	UPDATE tracked_unit_device SET 
												device_primary = 1 
										   	WHERE 
												id = (SELECT MIN(id) FROM tracked_unit_device WHERE tracked_unit_id = $unitId AND status = 1)");

				$result = $db -> update(false, true);
				//ATUALIZA ALOCAÇÃO DE MEMORIA
				$loadMemory = $this -> handleAllocateMemory($unitId);
			}else{
				$return = false;
			}	

			$db -> close();
			return $return;		
						
		}catch(Exception $e){
			return false;
		}
	}

	public function releaseTrackedUnitDeviceAction($obj){
		try{		

			$relaseDate = date("Y-m-d H:i:s");
					
			$db = new DatabaseHandler();
			$db -> addOption("table", "tracked_unit_device");
			$db -> addOption("fields", array("status = 3","release_date = :release_date"));
			$db -> addOption("where", array("device_id = :device_id", "tracked_unit_id = :unit_id"));
			$db -> addOption("logicalOperator", array("AND"));	
			$db -> addOption("param", array(":release_date" => $relaseDate,
											":device_id" => $obj -> device_id, 
											":unit_id" => $obj -> unit_id));	
			
			$update = $db -> update();
			$db->close();
			
			if (is_bool($update) && $update === TRUE) {

				$validation = $this -> handleUpdatePrimary($obj -> unit_id, $obj -> device_id);
				
				return json_encode('Dispositivo removido com sucesso.');
			} else {
				return E_UNABLETORELEASE;				
			}			
						
		}catch(Exception $e){
			return E_INTERNAL;
		}
	}	

	public function deleteunitAction($obj) {
		try {
			$relaseDate = date("Y-m-d H:i:s");
					
			$db = new DatabaseHandler();
			$db -> addOption("table", "tracked_unit_device");
			$db -> addOption("fields", array("status = :unit_status","release_date = :release_date"));
			$db -> addOption("where", array("tracked_unit_id = :unit_id", "status = 1"));
			$db -> addOption("logicalOperator", array("AND"));	
			$db -> addOption("param", array(":release_date" => $relaseDate,
											":unit_id" => $obj -> unit_id, 
											":unit_status" => $obj -> unit_status));	
			$updateStatus = $db -> update();
			$db->close();
			
			if (is_bool($updateStatus)) {

				$db = new DatabaseHandler();

				$db -> addOption("table", "tracked_unit");
				$db -> addOption("fields", array("status = :status"));
				$db -> addOption("where", array("id = :id"));
				$db -> addOption("param", array(":id" => $obj -> unit_id,
												":status" => $obj -> unit_status));
				$updateUnit = $db -> update();
				$db->close();
				
				if(is_bool($updateUnit) && $updateUnit === true) {
					return json_encode('Unidade removida com sucesso.');
				} else {
					return E_UNABLETODELETEUNIT;
				}

			} else {
				return E_UNABLETORELEASE;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	/**
	 * Recupera as informações de uma unidade dado o id da conta em que se encontra
	 * @param Integer $id
	 * @return Mixed String $errorMessage, json $result 
	 */
	public function getUnitAction($id=null) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("tru.id as id", 
											"tru.label as label",
											"tru.label2 as label2",
											"tru.dst as dst",
											"grp.name as nome_grupo",
											"sgrp.name as nome_subgrupo",
											"ucat.name as nome_categoria"));
			$db->addOption("table", "tracked_unit tru");
			$db->addOption("joinTable", array(	"account as ac", 
												'"group" as grp', 
												'subgroup sgrp', 
												'unit_category ucat', 
												'user_group_access usgrac'));
			$db->addOption("joinTableOn", array("tru.account_id = ac.id", 
												"tru.group_id = grp.id", 
												"tru.subgroup_id = sgrp.id", 
												"tru.unit_category_id = ucat.id", 
												"usgrac.group_id = grp.id AND usgrac.user_id = :user_id AND usgrac.group_id = tru.group_id AND tru.subgroup_id = usgrac.subgroup_id"));
			$db->addOption("joinTableType", array(	"INNER JOIN", 
													"INNER JOIN", 
													"INNER JOIN", 
													"INNER JOIN", 
													"INNER JOIN"));
			if(isset($id)){
			
				$db -> addOption("where", array("tru.account_id = :account_id", 
												"tru.id = :id", 
												"tru.status = 1"));
				$db -> addOption("logicalOperator", array("AND", "AND"));

				$db -> addOption("param", array(":id" => $id, 
												":account_id" => $_SESSION['user']->getAccount(), 
												":user_id" => $_SESSION['user']->getUserId()));
			
			}else{

				$db -> addOption("where", array("tru.account_id = :account_id", 
												"tru.status = 1"));
				$db -> addOption("logicalOperator", array("AND"));
				$db -> addOption("param", array(":account_id" => $_SESSION['user']->getAccount(), 
												":user_id" => $_SESSION['user']->getUserId()));


			}
			$db->addOption("orderBy", array("tru.label"));

			$resultUnit = $db -> select(true);
			$db->close();
			$db->close();
			if(is_object($resultUnit)) {
				$result = array();
				while ($unit = $resultUnit -> fetch()) {
					$result[] = $unit;
				}						
				if(isset($id)){					
					$return = $result;										
				} else {			
					$return = array("data" => $result);						
				}

				return json_encode($return);
			} else {
				echo E_UNABLETOLOADUNITLIST;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function searchUnitAction($obj) {
		try {		
			$db = new DatabaseHandler();
			$db->addOption("fields", array("tru.id as id", "tru.label"));
			$db->addOption("table", "tracked_unit tru");
			$db->addOption("where", array("tru.label ilike '%".$obj->unit."%'", "tru.account_id = :id", "tru.status = 1"));
			$db->addOption("logicalOperator", array("AND", "AND"));			
			$db->addOption("param", array(":id" => $_SESSION['user']->getAccount()));
			$db->addOption("orderBy", array("tru.id"));
			$db->addOption("orderBySort", array("desc"));
			$resultUnit = $db -> select(true, 1);
			$db->close();
			
			if(is_object($resultUnit)) {
				$result = array();
				while ($units = $resultUnit->fetch()) {
					$result[] = $units;
				}
			} else if($resultUnit === false) {
				$result = [];
			}
			
			return json_encode($result);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function printUnitAction($obj) {
		$content = '<style>
						table { border: 1px solid #999; border-bottom: none; width: 100%; }
						thead { border: 5px solid #000; }
						tbody td { border: 1px solid #ccc; }
					</style>';
		
		$content = $content.'<table><thead"><th>Placa</th><th>Descrição</th></thead><tbody>';
		
		
		foreach ($obj as $row) {
			if(empty($row->label)){ $row->label = ''; }
			if(empty($row->label2)){ $row->label2 = ''; }
			$content = $content."<tr><td>".$row->label."</td><td>".$row->label2."</td></tr>";
		}
		
		$content = $content.'</tbody></table>';
		
		return $content;
	}
	
	public function getDriverAssociatesAction($id) {
		try {
			$db = new DatabaseHandler();			
			$db->addOption("table", "SELECT
										d.id,
										dtu.status,
										d.name as name,
										d.cnh as description,
										d.group_id,
										gp.name as group_name,
										dtu.memory_position
									FROM  
										driver_tracked_unit dtu
										INNER JOIN driver d ON d.id = dtu.driver_id
										INNER JOIN \"group\" gp ON d.group_id = gp.id
									WHERE  
										dtu.unit_id = $id
										AND dtu.status > 0");

			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			$result = array();

			if (is_object($sqlResult)) {	
				while ($entry = $sqlResult -> fetch()) {
					$result[] = $entry;
				}	
			}

			$return = array("data" => $result);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
}
