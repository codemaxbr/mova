<?php
Class CercaController extends defaultController {
	
	public function indexAction($infos = null) {
		$unit = new UnitController();
		$return = array();

		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());
		$return['groupinfo'] = $unit -> listGroupAction();

		return json_encode($return);
	}

	public function listAction($id=null) {
		try {
			$user_id = $_SESSION['user'] -> getUserId();

			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											cerca.id as cerca_id, 
											cerca.name as cerca_name, 
											cerca.color,  
											cerca.polygroup_type,  
											CASE when cerca.polygroup_type = 1 then 'Circulo' when cerca.polygroup_type = 2 then 'Quadrado' else 'Polígono' end as polygroup_description,  
											replace(SUBSTRING(replace(REPLACE(ST_AsText(polygon), ',' , '|'), ' ',','),10, length(ST_AsText(polygon))),')','') as polygon, 
											cerca.latitude, 
											cerca.longitude, 
											cerca.radius, 
											cerca.speed, 
											cerca.description, 
											cerca.group_id, 
											gp.name as group_name
										FROM 
											cerca
											INNER JOIN \"group\" gp ON cerca.group_id = gp.id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = gp.id AND usgrac.user_id = $user_id
										WHERE 
											cerca.status = 1
										ORDER BY
											cerca.name");
			
			$resultCerca = $db -> select(false, 0, true);
			$db->close();
			
			$result = array();
			if (is_object($resultCerca)) {	
				while ($cerca = $resultCerca -> fetch()) {
					$result[] = $cerca;
				}						
				if(isset($id)) {				
					$return = $result;										
				} else {
					$return = array("data" => $result);						
				}
				return json_encode($return);
			} else {
				$return = array("data" => $result);	
				return json_encode($return);
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function getByIdAction($id) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("c.id as cerca_id, c.name as cerca_name, c.*", "gp.name as group_name"));
			$db->addOption("table", "cerca c");
			$db->addOption("joinTable", array( "\"group\" gp"));
			$db->addOption("joinTableType", array("INNER JOIN"));	
			$db->addOption("joinTableOn", array( "c.group_id = gp.id"));	
			$db->addOption("where", array("c.id = :id"));
			$db->addOption("param", array(":id" => $id));
			$result = $db->select(true);

			$db->close();
			
			$cerca = $result->fetch();
			return json_encode($cerca);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addAction($obj) {
		try {
			$db = new DatabaseHandler();
			$name = $obj -> name;
			$account_id = $obj -> account_id;
			$group_id = $obj -> group_id;
			$polygroup_type = $obj -> polygroup_type;
			$color = $obj -> color;
			$speed = $obj -> speed;
			$description = $obj -> description;
    		$radius = $obj -> cerca_radius;

			if($obj -> polygroup_type == 1) {
		    	foreach ($obj -> cerca_coords as $coord) {
					$lat = $coord[0];
					$lng = $coord[1];
				}
	    		$polygon = 'NULL';
			} else {
				$polygon = "(SELECT ST_GeomFromText('SRID=4326;POLYGON((";
		    	foreach ($obj -> cerca_coords as $coord) {
			    	foreach ($coord as $a) {
					    $lat = $a[0];
					    $lng = $a[1];
			    		$polygon .= "$lat $lng,"; 
			    	}
				}
				$polygon = substr($polygon, 0, strlen($polygon) - 1);
				$polygon .= "))'))";
				$lat = 0;
				$lng = 0;
			}

			$db -> addOption("table", 	"INSERT INTO cerca (name, account_id, group_id, polygroup_type, polygon, latitude, longitude, radius, color, speed, description)
										VALUES (
											'$name',
											$account_id,
											$group_id,
											$polygroup_type,
											$polygon,
											$lat,
											$lng,
											$radius,
											'$color',
											$speed,
											'$description')");

			$result = $db -> insert(false, 0, 0, true);
			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Cerca cadastrada com sucesso.');
			} else {
				return 'Erro ocorrido no cadastro da cerca.';
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function editAction($obj) {
		try {
			$db = new DatabaseHandler();
			$id = $obj -> id;
			$name = $obj -> name;
			$account_id = $obj -> account_id;
			$group_id = $obj -> group_id;
			$polygroup_type = $obj -> polygroup_type;
			$color = $obj -> color;
			$speed = $obj -> speed;
			$description = $obj -> description;
    		$radius = $obj -> cerca_radius;

			if($obj -> polygroup_type == 1) {
		    	foreach ($obj -> cerca_coords as $coord) {
					$lat = $coord[0];
					$lng = $coord[1];
				}
	    		$polygon = 'NULL';
			} else {
				$polygon = "(SELECT ST_GeomFromText('SRID=4326;POLYGON((";
		    	foreach ($obj -> cerca_coords as $coord) {
			    	foreach ($coord as $a) {
					    $lat = $a[0];
					    $lng = $a[1];
			    		$polygon .= "$lat $lng,"; 
			    	}
				}
				$polygon = substr($polygon, 0, strlen($polygon) - 1);
				$polygon .= "))'))";
				$lat = 0;
				$lng = 0;
			}

			$db -> addOption("table", 	"UPDATE cerca SET 
											name = '$name', 
											account_id = $account_id, 
											group_id = $group_id, 
											polygroup_type = $polygroup_type, 
											polygon = $polygon, 
											latitude = $lat, 
											longitude = $lng, 
											radius = $radius, 
											speed = $speed, 
											color = '$color', 
											description = '$description'
										WHERE id = $id");

			$result = $db -> update(false, true);

			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Cerca alterada com sucesso.');
			} else {
				return 'Erro ocorrido na alteração da cerca.';
			}
			
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function deleteAction($obj) {
		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "cerca");
			$db -> addOption("fields", array("status = :status"));
			$db -> addOption("where", array("id = :id"));
			$db -> addOption("param", array(":id" => $obj -> id,
											":status" => $obj -> status));
			$result = $db -> update();
			
			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Cerca removida com sucesso.');
			} else {
				return 'TODO';
			}

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

}
