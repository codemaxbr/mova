<?php
Class groupsController extends defaultController {
	
	public function indexAction($infos = null) {
		$return = array();
		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());

		return json_encode($return);
	}

	public function listAction() {
		try {
            $user_id = $_SESSION['user'] -> getUserId();
			$result = array();

            $sql = "select distinct g.id, g.name from \"group\" g
                         join user_group_access uga on uga.group_id = g.id AND uga.user_id = $user_id
                         where g.account_id = {$_SESSION['user']->getAccount()}
                         order by g.name";

            $db = new DatabaseHandler();
            $result = $db->select_fetch_v1($sql);
			$return = array("data" => $result);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function getByIdAction($id) {
		try {
			$resultGroup = array();
			$resultSubgroup = array();

            $sql = "select * from \"group\" g where g.id = {$id}";
            $db = new DatabaseHandler();
            $resultGroup = $db->select_fetch_v1($sql);
            
            $sql = "select * from subgroup s where s.group_id = {$id}";
            $db = new DatabaseHandler();
            $resultSubgroup = $db->select_fetch_v1($sql);

			$return = array("group" => $resultGroup, 
                            "subgroup" => $resultSubgroup);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addAction($obj) {
		try {
			$result = array();

            if (!strlen($obj->name))
                return "Nome de grupo vazio.";

            for ($i = 0; $i < count($obj->subgroupName); ++$i) {
                if (!strlen($obj->subgroupName[$i]))
                    return "Nome de subgrupo vazio.";

                if (!strlen($obj->color[$i]))
                    return "Nome de cor de subgrupo vazio.";
            }

            $a = sql_escape($obj);
            $o = array2object($a);

            $sql = "INSERT INTO \"group\" (name, account_id) 
                      VALUES ({$o->name}, {$_SESSION['user']->getAccount()}) ";

            $db = new DatabaseHandler();
            $result = $db->exec_v1($sql);

            $gid = intval($db->lastInsertId('client_id_seq'));
            if ($gid <= 0) {
                return "Não foi possível obter o ID do grupo.";
            }

            for ($i = 0; $i < count($a['subgroupName']); $i++) {
                $sql1 = "INSERT INTO subgroup (name, group_id, color)
                           VALUES ({$a['subgroupName'][$i]}, {$gid}, {$a['color'][$i]}) ";
                $db->exec_v1($sql1);

                $subid = intval($db->lastInsertId('subgroup_id_seq'));
                if ($subid <= 0) {
                    return "Não foi possível obter o ID do subgrupo.";
                }

                $sql2 = "INSERT INTO user_group_access (subgroup_id, group_id, user_id)
                           VALUES ({$subid}, {$gid}, {$_SESSION['user']->getUserId()}) ";
                $db->exec_v1($sql2);
            }

			$return = array("data" => "", "msg" => "Sucesso.");
			return json_encode($return);
		} catch(Exception $e) {
            return E_INTERNAL;
		}
	}
	
	public function editAction($obj) {
		try {
            $gid = intval($obj->id);

            $a = sql_escape($obj);
            $x = object2array($obj);
            
            $db = new DatabaseHandler();
            $sql = "UPDATE \"group\" SET name = {$a['name']} WHERE id = {$gid} ";
            $db->exec_v1($sql);

            for ($i = 0; $i < count($a['subgroupName']); $i++) {
                $subid = intval($x['subgroupID'][$i]);

                if ($subid > 0) {
                    // grupo já existe; fazemos apenas um UPDATE
                    $sql = "UPDATE subgroup SET 
                               name = {$a['subgroupName'][$i]} 
                              ,color = {$a['color'][$i]} 
                              WHERE id = {$subid} ";
                    $db->exec_v1($sql);
                    continue;
                }

                // grupo é novo. precisamos adicioná-lo.
                $sql = "INSERT INTO subgroup (name, group_id, color) VALUES 
                              ({$a['subgroupName'][$i]}, {$gid}, {$a['color'][$i]}) ";
                //d($sql);
                $db->exec_v1($sql);
                    
                $subid = intval($db->lastInsertId('subgroup_id_seq'));
                if ($subid <= 0) {
                    return "Não foi possível obter o ID do subgrupo.";
                }
                
                $sql = "INSERT INTO user_group_access (subgroup_id, group_id, user_id)
                                VALUES ({$subid}, {$gid}, {$_SESSION['user']->getUserId()}) ";
                //d($sql);
                $db->exec_v1($sql);
            }

			$result = array();

			$return = array("data" => $result, "msg" => "Sucesso.");
			return json_encode($return);
		} catch(Exception $e) {
			return $e;
		}
	}

    public function deleteSubgroupAction($id) {
        $subid = intval($id);
        
        $del = "DELETE FROM subgroup WHERE id = {$subid} ";
        $db = new DatabaseHandler();
        $db->exec_v1($del);

        $return = array("data" => true, "msg" => "Sucesso.");
        return json_encode($return);
    }
	
	public function deleteAction($obj) {

        /*
          Não removeremos um grupo caso haja qualquer coisa que faça
          referência ao grupo.  Por ``qualquer coisa'' queremos dizer
          qualquer registro das seguintes tabelas: alarm, area, cerca,
          driver, poi, route, tracked_unit.  Falta alguma?
         */

        $gid = $obj->id;
        
        if (intval($gid) <= 0)
            return "Grupo inválido.";

        $sql[] = array('sql' => "SELECT COUNT(*) AS N FROM alarm WHERE group_id = {$gid} ",
                       'msg' => "Há %s alarmes associados a este grupo.");

        $sql[] = array('sql' => "SELECT COUNT(*) AS N FROM area WHERE group_id = {$gid} ",
                       'msg' => "Há %s áreas associadas a este grupo.");

        $sql[] = array('sql' => "SELECT COUNT(*) AS N FROM cerca WHERE group_id = {$gid} ",
                       'msg' => "Há %s cercas associadas a este grupo.");

        $sql[] = array('sql' => "SELECT COUNT(*) AS N FROM driver WHERE group_id = {$gid} ",
                       'msg' => "Há %s motoristas associados a este grupo.");

        $sql[] = array('sql' => "SELECT COUNT(*) AS N FROM poi WHERE group_id = {$gid} ",
                       'msg' => "Há %s POI associados a este grupo.");

        $sql[] = array('sql' => "SELECT COUNT(*) AS N FROM route WHERE group_id = {$gid} ",
                       'msg' => "Há %s rotas associados a este grupo.");

        $sql[] = array('sql' => "SELECT COUNT(*) AS N FROM tracked_unit WHERE group_id = {$gid} ",
                       'msg' => "Há %s unidades associadas a este grupo.");

		try {
			$result = array();

            $db = new DatabaseHandler();
            foreach ($sql as $a) {
                $r = $db->select_fetch_v1($a['sql'])[0];
                if ($r->n > 0)
                    return sprintf($a['msg'], $r->n);
            }
            
            /* Agora deletemos. */
            $dels[] = "DELETE FROM \"group\" WHERE id = {$gid} ";
            $dels[] = "DELETE FROM subgroup WHERE group_id = {$gid} ";
            $dels[] = "DELETE FROM user_group_access WHERE group_id = {$gid} ";

            foreach($dels as $s)
                $r = $db->exec_v1($s);

			$return = array("data" => $result, "msg" => "Sucesso.");
			return json_encode($return);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
}
