<?php
class UnitMonitorController extends defaultController {
	public function indexAction($infos=null) {
		$map = new MapController();
		$return['unities'] = $map->loadDataAction();		
		return json_encode($return);
	}
}