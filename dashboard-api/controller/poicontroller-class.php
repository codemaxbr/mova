<?php
Class PoiController extends defaultController {
	
	public function indexAction($infos = null) {
		$widget = new widgetController();
		$unit = new UnitController();
		$return = array();

		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());
		$return['listIcons'] = $widget -> listIconsAction('poi');
		$return['groupinfo'] = $unit -> listGroupAction();

		return json_encode($return);
	}

	public function listAction($id=null) {

		$testId = false;
		$distance = "";
		$orderBy = "";
		if(isset($id)) {
			if( (isset($id->user)) && ($id->token == "12342612648763598123791829371462835413")) {
				$user_id = $id->user;
				if(isset($id->lat)){
					$distance = " , round(CAST(ST_Distance_Sphere(poi.gpoint, ST_GeomFromText('POINT(".$id->lat." ".$id->lng.")', 4326)) As numeric), 2) AS distance ";
					$orderBy = " ORDER BY round(CAST(ST_Distance_Sphere(poi.gpoint, ST_GeomFromText('POINT(".$id->lat." ".$id->lng.")', 4326)) As numeric), 2) ";
					// $distance = " , ST_Distance_Sphere(poi.gpoint, ST_GeomFromText('POINT(".$id->lat." ".$id->lng.")', 4326)) AS distance ";
					// $orderBy = " ORDER BY ST_Distance_Sphere(poi.gpoint, ST_GeomFromText('POINT(".$id->lat." ".$id->lng.")', 4326)) ";
				}
			} else if(isset($id)) {
				$testId = true;
			}
		}else{				
			$user_id = $_SESSION['user'] -> getUserId();
		}

		try {
			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											poi.id as poi_id, 
											poi.name as poi_name, 
											poi.icon as icon, 
											'<i class=\"' || poi.icon || '\"></i>' as iconhtml, 
											'<span class=\"fa-stack fa-lg\"><i class=\"fa fa-circle fa-stack-2x\" style=\"color: ' || poi.color || ';\"></i><i class=\"fa '|| poi.icon ||' fa-stack-1x\"></i></span>' as iconhtmlstack, 
											poi.color, 
											poi.latitude, 
											poi.longitude, 
											poi.description, 
											poi.group_id, 
											gp.name as group_name
											$distance 
										FROM 
											poi
											INNER JOIN \"group\" gp ON poi.group_id = gp.id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = gp.id AND usgrac.user_id = $user_id
										WHERE 
											poi.status = 1
											$orderBy");
			
			$resultPoi = $db -> select(false, 0, true);
			$db->close();
			$result = array();
			//echo $db -> query();
			if (is_object($resultPoi)) {	
				while ($poi = $resultPoi -> fetch()) {
					$result[] = $poi;
				}						
				if(isset($id)) {				
					$return = $result;										
				} else {
					$return = array("data" => $result);						
				}
				return json_encode($return);
			} else {
				$return = array("data" => $result);	
				return json_encode($return);
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function getByGroupAction($obj = ''){

		try {
			if(!isset($_SESSION)) {
				session_start();
			}
			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$account_id = $my_account_id->user->account_id;
			$user_id = $_SESSION['user'] -> getUserId();

			//group
			$andGroups_poi = '';
			$groups_poi = '0';
			foreach ($obj->group_id as $group_poi) {
				$groups_poi .= ','.$group_poi;
			}
			if( ($groups_poi != '0,0') && ($groups_poi != '0') ){
				$andGroups_poi = " AND poi.group_id IN ($groups_poi) ";
			}

			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											poi.id as poi_id, 
											poi.name as poi_name, 
											poi.group_id,
											poi.account_id,
											gp.name as group_name
										FROM 
											poi
											INNER JOIN \"group\" gp ON poi.group_id = gp.id
										WHERE 
											poi.status = 1
											AND poi.account_id = $account_id
											$andGroups_poi
										");
			
			$resultPoi = $db -> select(false, 0, true);
			$db->close();
			$result = array();
			//echo $db -> query();
			if (is_object($resultPoi)) {	
				while ($poi = $resultPoi -> fetch()) {
					$result[] = $poi;
				}						
				if(isset($id)) {				
					$return = $result;										
				} else {
					$return = array("data" => $result);						
				}
				return json_encode($return);
			} else {
				$return = array("data" => $result);	
				return json_encode($return);
			}

		} catch (Exception $e) {
			return E_INTERNAL;
		}
	}

	public function getByIdAction($id) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("p.*", "gp.name as group_name"));
			$db->addOption("table", "poi p");
			$db->addOption("joinTable", array( "\"group\" gp"));
			$db->addOption("joinTableType", array("INNER JOIN"));	
			$db->addOption("joinTableOn", array( "p.group_id = gp.id"));	
			$db->addOption("where", array("p.id = :id"));
			$db->addOption("param", array(":id" => $id));
			$result = $db->select(true);
			$db->close();
			$poi = $result->fetch();
			
			return json_encode($poi);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addAction($obj) {
		try {

			$name = $obj -> name;
			$account_id = $obj -> account_id;
			$group_id = $obj -> group_id;
			$icon = $obj -> icon;
			$color = $obj -> color;
			$latitude = $obj -> latitude;
			$longitude = $obj -> longitude;
			$description = $obj -> description;

			$db = new DatabaseHandler();
			$db -> addOption("table", "INSERT INTO poi (name, account_id, group_id, icon, color, latitude, longitude, gpoint, description) VALUES 
										(
											'$name',
											$account_id,
											$group_id,
											'$icon',
											'$color',
											$latitude,
											$longitude,
											ST_GeomFromText('POINT($latitude $longitude)', 4326),
											'$description'
										)");

			$result = $db -> insert(false, 0, 0, true);
			
			$db->close();

			if(is_bool($result) && $result === true) {
				return json_encode('Ponto cadastrado com sucesso.');
			} else {
				return 'TODO';
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function editAction($obj) {
		try {
			$id = $obj -> poi_id;
			$name = $obj -> name; 
			$account_id = $obj -> account_id; 
			$group_id = $obj -> group_id; 
			$icon = $obj -> icon; 
			$color = $obj -> color; 
			$latitude = $obj -> latitude; 
			$longitude = $obj -> longitude; 
			$description = $obj -> description;

			$db = new DatabaseHandler();
			$db -> addOption("table", "UPDATE poi SET 
											name = '$name', 
											account_id = $account_id, 
											group_id = $group_id, 
											icon = '$icon', 
											color = '$color', 
											latitude = $latitude, 
											longitude = $longitude, 
											gpoint = ST_GeomFromText('POINT($latitude $longitude)', 4326),
											description = '$description'
										WHERE id = $id"
											);
			$result = $db -> update(false, true);
			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Ponto cadastrado com sucesso.');
			} else {
				return 'TODO';
			}
			
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function deleteAction($obj) {
		try {

			$db = new DatabaseHandler();

			$db -> addOption("table", "poi");
			$db -> addOption("fields", array("status = :status"));
			$db -> addOption("where", array("id = :id"));
			$db -> addOption("param", array(":id" => $obj -> id,
											":status" => $obj -> status));
			$result = $db -> update();
			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Ponto removido com sucesso.');
			} else {
				return 'TODO';
			}

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function searchPOIAction($obj) {
		try {		
			$user_id = $_SESSION['user'] -> getUserId();
			$accountId 	= $_SESSION['user']	-> getAccount();
			$name = $obj -> name;

			$db = new DatabaseHandler();
			$db -> addOption("table", "SELECT DISTINCT
											p.*, 
											gp.name as group_name
										FROM 
											poi p
											INNER JOIN \"group\" gp ON p.group_id = gp.id
											INNER JOIN user_group_access usgrac ON usgrac.group_id = gp.id AND usgrac.user_id = $user_id
										WHERE 
											p.status = 1
											AND p.account_id = $accountId
											AND p.name ilike '%$name%'
										ORDER BY p.name");
			
			$sqlResult = $db -> select(false, 0, true);
			$db->close();
			
			if(is_object($sqlResult)) {
				$result = array();
				while ($row = $sqlResult->fetch()) {
					$result[] = $row;
				}
			} else if($sqlResult === false) {
				$result = [];
			}
			
			return json_encode($result);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

}
