<?php

class widgetController extends defaultController
{
	public function indexAction($infos=null) {
		$device = new DeviceController(); 
		$account = new accountController();
		$return = array();
		//$return['permissionType'] = $this -> haveWriteAction($infos);
		//$return['infos'] = $this ->getWidgetInfoAction($infos);
		$return['widgetsCategory'] = $this -> getParentsWidgets();
		$return['manuf'] = $device ->listManufacturerAction();
		$return['accountTypeList'] = $account -> listAccountTypesAction(true);
		$return['widgetList'] = $this -> listAction();
		$return['listModel'] = $device -> listDataTableModelAction();
		$return['listIcons'] = $this -> listIconsAction();
		return json_encode($return);
	}

	public function dashboardAction($infos=null) {
		$return = array();
		$return['userinfo'] = json_decode($_SESSION['user']->getUserInfo());
		$return['infos'] = json_decode($this ->getWidgetInfoAction($infos));
		return json_encode($return);
	}
	
	public function addAction($obj) {
		try {
			$checkTemplate = $this -> checkTemplateAction($obj->template);
			
			if($checkTemplate !== E_WIDGETNOTFOUND) {
				return $checkTemplate;
			}
					
			$db = new DatabaseHandler();
			$db->addOption("table", "widget");
			$db->addOption("fields", array(	"name",  
											"size", 
											"template", 
											"icon",  
											"widget_category_id"));
			$db->addOption("param", array(	":name" => $obj->name, 
											":widget_category_id" => $obj->category, 
											":size" => $obj->size,  
											":icon" => $obj->icon,  
											":template" => $obj->template));
			$result = $db->insert();
			$db->close();
			
			if(is_bool($result) && $result === true) {
				return json_encode('Widget cadastrado com sucesso.');
			} else {
				return E_UNABLETOCREATEWIDGET;
			}
		} catch( Exception $e ) {
			return E_INTERNAL;
		}
	}
	
	public function editAction($obj) {		
		try {			
			$checkTemplate = $this->checkTemplateAction($obj->template, $obj->id);
			
			if($checkTemplate !== E_WIDGETNOTFOUND){
				return $checkTemplate;
			}

			//Setando valores padrões
			$db = new DatabaseHandler();
			$db->addOption("fields", array(	"name = :name", 
											"size = :size", 
											"widget_category_id = :category", 
											"icon = :icon", 
											"template = :template"));
			$db->addOption("table", "widget");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(	":id" => $obj->id, 
											":name" => $obj->name, 
											":size" => $obj->size, 
											":category" => $obj->category, 
											":icon" => $obj->icon, 
											":template" => $obj->template));
			$resultUpdate = $db->update();
			$db->close();
			
			if(is_bool($resultUpdate) && $resultUpdate === true)
			{
				return json_encode('Widget alterado com sucesso.');
			} else {
				return E_UNABLETOUPDATEWIDGET;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function listAction() {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("w.id", "w.name", "wc.name as cat", "w.icon", "w.template", "w.size"));
			$db->addOption("table", "widget as w");
			$db->addOption("joinTable", array("widget_category as wc"));
			$db->addOption("joinTableOn", array("w.widget_category_id = wc.id"));
			$db->addOption("joinTableType", array("INNER JOIN"));
			$db->addOption("where", array("w.status = 1"));
			$resultAccount = $db->select(false, 1);
			$db->close();
			
			$result = array();
			
			if(is_object($resultAccount)) {
				while ($contas=$resultAccount->fetch()) {
					$result[] = $contas;
				}
			
				$return = array("data" => $result);
				return json_encode($return);
			} else {
				return $return = array("data" => '');
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getParentsWidgets() {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("wc.id as parent_id", "wc.name"));
			$db->addOption("table", "widget_category as wc");
			$db->addOption("orderBy",array("wc.name"));
			$resultWidgetsC = $db->select(false, 1);
			$db->close();
			
			if(is_object($resultWidgetsC)) {
				$result = array();
				while($widgetC = $resultWidgetsC->fetch()) {
					$widgetC->icon = 'fa fa-1x fa-laptop';
					$result[] = $widgetC;
				}

				$return = array("widgetsCategory" => $result);
				return json_encode($return);
			} else {
				return E_UNABLETOGETPARENTWIDGETS;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
		
	public function listWidgetsAction()	{
		try {
			$widgetList = $this->getWidgetList($_SESSION['user'] -> getAccount());
			//TODO Tratamento de erros
			
			if($widgetList == false){
				echo $widgetList;
				return;
			}
			
			$fullTree = $this -> fetchTree($widgetList,true);

			if(is_array($fullTree)){	
				$tree = array("text" => "Acessos", "icon" => 'fa fa-1x fa-laptop');
				$tree["children"] = $fullTree;		
				$json = json_encode($tree);
				//Trocando nome dos campos de arrays para funcionar no plugin jsTree
				$search = array("name", "childrens");
				$replace = array("text", "children");
				$return = str_replace($search, $replace, $json);
				//Passando para json
				$return = json_encode($return);
				//Imprimindo para o plugin jsTree
				return json_decode($return);
			} else {
				return E_UNABLETOLISTWIDGETS;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function listWidgetsAccountAction()	{
		try {	
			$widgetList = $this->getWidgetList();
						
			$fullTree = $this -> fetchTree($widgetList);
			if(is_array($fullTree)) {
				$tree = array("text" => "Acessos", "icon" => 'fa fa-1x fa-laptop');
				$tree["children"] = $fullTree;		
				$json = json_encode($tree);
				//Trocando nome dos campos de arrays para funcionar no plugin jsTree
				$search = array("name", "childrens");
				$replace = array("text", "children");
				$return = str_replace($search, $replace, $json);
				//Passando para json
				$return = json_encode($return);
				//Imprimindo para o plugin jsTree
				return json_decode($return);
			} else {
				return E_UNABLETOLISTWIDGETS;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getChildrensWidgets($parent){
		try {
			if($parent) {
				if(!isset($_SESSION)){
					
				}
				$my_account_id = json_decode($_SESSION['user']->getUserInfo());
				$account_id = $my_account_id->user->account_id;
				
				$db = new DatabaseHandler();
				$db->addOption("fields", array("w.id", "w.name", "w.icon as icon"));
				$db->addOption("table", "widget as w");
				$db->addOption("joinTable", array("account_widget as aw"));
				$db->addOption("joinTableOn", array("w.id = aw.widget_id"));
				$db->addOption("joinTableType", array("LEFT JOIN"));
				$db->addOption("where", array("w.widget_category_id = :idc", "aw.account_id = :account_id"));
				$db->addOption("logicalOperator", array("AND"));
				$db->addOption("param", array(":idc" => $parent, ":account_id" => $account_id));
				$db->addOption("orderBy",array("w.name"));
				$resultWidgets = $db->select(true, 1);
				$db->close();
						
				if($resultWidgets == false) {
					return false;
				}
				
				if(!is_object($resultWidgets)) {
					return E_WIDGETSNOTFOUND;
				}
	
				$result = array();
				$i = 0;
				while($widget = $resultWidgets->fetch()) {
					$write = array('parent_id' => $widget->id, 'id' => $widget->id.$i, 'text' => 'Escrita', 'icon' => 'fa fa-1x fa-pencil');
					$widget->children = array($write);
					$result[] = $widget;
					$i++;
				}
				
				return $result;			
			} else {
				return false;
			}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getChildrensWidgetsAccountAction($parent) {
		try {
			if($parent) {
				$db = new DatabaseHandler();
				$db->addOption("fields", array("w.id", "w.name", "w.icon as icon"));
				$db->addOption("table", "widget as w");
				$db->addOption("where", array("w.widget_category_id = :idc", "w.status = 1"));
				$db->addOption("logicalOperator", array("AND"));
				$db->addOption("param", array(":idc" => $parent));
				$db->addOption("orderBy",array("w.name"));
				$resultWidgets = $db->select(true, 1);
		
				if(!$resultWidgets) {
					return false;
				}
		
				$result = array();
				while($widget = $resultWidgets->fetch()){
					$result[] = $widget;
				}
					
				return $result;
			} else {
				return false;
			}
		} catch(Exception $e){
			return E_INTERNAL;
		}
	}

	public function createEditFormAction($id) {
		try {
			$db = new DatabaseHandler();
			$db->addOption("fields", array("*"));
			$db->addOption("table", "widget");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(":id" => $id));
			$result = $db->select(true);
			$db->close();
			
			$widget = $result->fetch();
			return json_encode($widget);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function getWidgetInfoAction($widgetTemplate=null) {
		try {

			$my_account_id = json_decode($_SESSION['user']->getUserInfo());
			$user_id = $my_account_id->user->id;

			$db = new DatabaseHandler();

			$db->addOption("fields", array("wc.name as category_name", "wc.icon as category_icon", "w.*"));
			$db->addOption("table", "user_access ua");
			$db->addOption("joinTable", array("widget w", "widget_category wc"));
			$db->addOption("joinTableOn", array("ua.widget_id = w.id", "w.widget_category_id = wc.id"));
			$db->addOption("joinTableType", array("INNER JOIN", "INNER JOIN"));
			$db->addOption("where", array("w.status = 1", "ua.user_id = :user_id"));
			$db->addOption("logicalOperator", array("AND"));
			$db->addOption("param", array(":user_id" => $user_id));
			$db->addOption("orderBy", array("wc.name", "w.name"));

			$result = $db->select(true);
			
			$array = array();
			while ($widget=$result->fetch()) {
				$array[] = $widget;
			}
			
			$return = array("widgetsInfo" => $array);
			return json_encode($return);		

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function checkTemplateAction($widgetTemplate, $widgetId=null) {
		try {
				$db = new DatabaseHandler();
				$db->addOption("fields", array("*"));
				$db->addOption("table", "widget");				
				if($widgetId != null) {
					$db->addOption("where", array("template = :template","id != :id", "status = 1"));
					$db-> addOption("logicalOperator", array("AND", "AND"));
					$db->addOption("param", array(":template" => $widgetTemplate,":id" => $widgetId));
				} else {
					$db->addOption("where", array("template = :template", "status = 1"));
					$db-> addOption("logicalOperator", array("AND"));
					$db->addOption("param", array(":template" => $widgetTemplate));
				}				
				$result = $db->select(true);
				$db->close();
				
				if(is_object($result)) {					
					while($widget = $result->fetch()) {
						if($widget->template == $widgetTemplate && $widget->id == $widgetId) {
							return false;
						}
					}
						return E_WIDGETTEMPLATEEXISTS;
				} else {
					return E_WIDGETNOTFOUND;
				}
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function listIconsAction($widget=null) {
		$db = new DatabaseHandler();
		$db->addOption("fields", array("prefix || ' ' || size || ' ' || class as icon"));
		if($widget != null) {
			$db->addOption("table", "(select * from icons where $widget = 1) as icons");
		} else {
			$db->addOption("table", "icons");			
		}
		$result = $db->select(false, 1);
		$db->close();
		
		$list = array();
		while($icon = $result->fetch()) {
			$list[] = $icon;
		}
		
		if($result) {
			return json_encode($list);
		} else {
			echo 'Houve algum erro';
		}
	}
	
	public function addIconsAction() {
		$db = new DatabaseHandler();
		$db->addOption("table", "icons");
		$db->addOption("fields", array("prefix", "class", "size"));
		$db->addOption("param", array(":prefix" => 'fa', ":class" => 'fa-ban', ":size" => 'fa-1x'));
		$result = $db->insert();
		$db->close();
	
		if($result) {
			return $result;
		} else {
			echo 'Houve algum erro';
		}
	}
	
	public function deleteWidgetsAction($obj) {
		try {
			$modifyDate = date("Y-m-d H:i:s");

			$db = new DatabaseHandler();
			$db->addOption("fields", array(	"status = :status", 
											"modify_date = :modify_date"));
			$db->addOption("table", "widget");
			$db->addOption("where", array("id = :id"));
			$db->addOption("param", array(	":id" => $obj->id, 
											":status" => $obj->status, 
											":modify_date" => $modifyDate));
			$resultUpdate = $db->update();
			$db->close();
			
			
			if($resultUpdate === true) {
				return json_encode('Widget removido com sucesso.');	
			} else {
				//TODO REGISTRAR ERRO
				return E_UNABLETOUPDATETYPE;
			}			
			
		} catch(Exception $e) {
			return E_INTERNAL;
		}		
	}
	
	/**
	 * Recupera os widgets associados a uma conta
	 * @param  Int $account_id
	 */
	private function getWidgetList($account_id = null) {
		$db = new DatabaseHandler();
		$db->addOption("fields", array("w.id", "w.name as widget"," w.icon as icon","wc.name as parent","wc.id as parent_id"));
		$db->addOption("table", "widget as w");
		if($account_id === null) {
			$bool = false;
			$db->addOption("joinTable", array("widget_category wc"));
			$db->addOption("joinTableOn", array("w.widget_category_id = wc.id"));
			$db->addOption("joinTableType", array("INNER JOIN"));
			$db->addOption("where", array("w.status = 1"));
		} else {
			$bool = true;
			$db->addOption("joinTable", array("account_widget as aw","widget_category wc"));
			$db->addOption("joinTableOn", array("w.id = aw.widget_id","w.widget_category_id = wc.id"));
			$db->addOption("joinTableType", array("INNER JOIN","INNER JOIN"));
			$db->addOption("where", array("aw.account_id = :account_id", "w.status = 1"));
			$db->addOption("logicalOperator", array("AND"));
			$db->addOption("param", array(":account_id" => $account_id));
		}
		$db->addOption("orderBy",array("parent, widget"));
		$resultWidgets = $db->select($bool);
		$db->close();
		
		$result = array();
		
		if(is_object($resultWidgets)) {
			while($widgetC = $resultWidgets->fetch()) {
					$widgetC->parent_icon = 'fa fa-1x fa-laptop';
					$result[] = $widgetC;
			}
			return $result;
		} else {
			return false;
		}
	}
}