<?php
class defaultController
{
	public function indexAction($infos=null) {
		
	}
	
	public function editAction($obj) {
				
	}
	
	public function listAction() {
		
	}
	
	public function addAction($obj) {
		
	}
	
	public function haveWriteAction($widget) {		
		return $_SESSION['user']->getAccessLevel($widget);
	}

	public function sendEmailAction($subject, $msg, $email, $html=true) {
		$mail = new PHPMailer();
		
		$mail->IsSMTP();
		$mail->SMTPAuth = true; // authentication enabled
		$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
		$mail->Host = "smtp.gmail.com";
		$mail->Port = 465; // or 587
		$mail->Username = "mova@movamaps.com";
		$mail->Password = "mova123";
		
		$mail->From = "no-reply@movamaps.com";
		$mail->FromName = "MovaMaps";
		$mail->AddAddress($email);
		/*$mail->AddAddress("name@email.com", "name"); //example
		$mail->AddReplyTo("info@example.com", "Information");*/
		
		$mail->WordWrap = 50;
		$mail->IsHTML($html); // seta para ver se e-mail usa html '<b>,<i>,<p>'
		
		$mail->Subject = utf8_decode($subject); //Título do assunto
		$mail->Body    = utf8_decode($msg); //Mensagem com tags html
		$mail->AltBody = utf8_decode($msg); //Mensagem sem tags html
		
		//Se não conseguir enviar o e-mail
		if(!$mail->Send())
		{
			//Mensagem de erro
			echo E_UNABLETOSENDEMAIL;
		//	echo "Tipo de erro: ".$mail->ErrorInfo;
			exit;
		}
		
		//echo "Message has been sent"; //Mensagem de sucesso
		return true; //Retornando sucesso
	}
	
	public function random_password($length=4) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$password = substr( str_shuffle( $chars ), 0, $length );
		return $password;
	}
	
	public function daysAction() {
		$days = array();
		$days[] = array('id' => 0, 'name' => 'Domingo');
		$days[] = array('id' => 1, 'name' => 'Segunda');
		$days[] = array('id' => 2, 'name' => 'Terça');
		$days[] = array('id' => 3, 'name' => 'Quarta');
		$days[] = array('id' => 4, 'name' => 'Quinta');
		$days[] = array('id' => 5, 'name' => 'Sexta');
		$days[] = array('id' => 6, 'name' => 'Sábado');

		$return = array("days" => $days);
		return $return;
	}
	
	public function formatDateAction($date, $separator) {
		$arrayDate = explode($separator, $date);		
		$buildDate = $arrayDate[1].'-'.$arrayDate[0].'-'.$arrayDate[2];
		
		return $buildDate;
	}
	
	public function formatDateUS($date, $separator = "/"){
		$arrayDate = explode($separator, $date);		
		$buildDate = $arrayDate[2].'-'.$arrayDate[1].'-'.$arrayDate[0];
		
		return $buildDate;
	}
	
	public function formatDateTimeAction($date, $time, $separator) {
		$arrayDate = explode($separator, $date);		
		$buildDate = $arrayDate[2].'-'.$arrayDate[1].'-'.$arrayDate[0].' '.$time;
		
		return $buildDate;
	}
	
	public function getDayAction($date) {
		$day = $this->daysAction();		
		return $day[$date];		
	}
	
	public function isJsonAction($string, $return_data=false) {
		$data = json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : true) : false;
	}
	
	public function generateHashLinkAction($id, $login) {
		$host = str_replace('forgot.html', 'password.html', $_SERVER['HTTP_REFERER']);
		$hashId = md5($id);
		$hashEmail = md5($login);
		$link = $host.'?p='.$hashId.'&'.$id.'!'.$hashEmail;
		
		return $link;
	}
	
	public function saveAddressAction($obj) {
		try {
			$id = $obj -> id;
			$fulladdress = $obj -> fulladdress;
			$street = $obj -> street;
			$streetnumber = $obj -> streetnumber;
			$featLen = count($obj -> features);
			$country = ''; 
			$state = '';
			$city = '';
			$region = '';
			$postalcode = '';
			
			for($i = 0; $i < $featLen; $i++) {
				if(isset($obj -> features[$i])){
					switch(($obj -> features[$i] -> id)) {
					
					case strpos($obj -> features[$i] -> id,"place"):
						$city = $obj -> features[$i] -> text;
						$region = $city;
						break;
					case strpos($obj -> features[$i] -> id,"postcode"):
						$postalcode = $obj -> features[$i] -> text;
						break;
					
					case strpos($obj -> features[$i] -> id,"region"):
						$state = $obj -> features[$i] -> text;
						break;
						
					case strpos($obj -> features[$i] -> id,"country"):
						$country = $obj -> features[$i] -> text;
						break;	
					}
				}
			}
			
			$db = new DatabaseHandler();
			//$db->addOption("table", "save_revgeo");
			//$db->addOption("param", array(array($obj -> id,$obj -> fulladdress,$country,$state,$city,$region,$obj -> street,$postalcode,$obj -> streetnumber)));
			//$db->addOption("paramType", array(array(PDO::PARAM_INT,PDO::PARAM_STR,PDO::PARAM_STR,PDO::PARAM_STR,PDO::PARAM_STR,PDO::PARAM_STR,PDO::PARAM_STR,PDO::PARAM_STR,PDO::PARAM_STR)));
			//$add = $db -> call(true, 0, 1);
			$db->addOption("table", "SELECT save_revgeo ($id, '$fulladdress', '$country', '$state', '$city', '$region', '$street', '$postalcode', '$streetnumber')");
			$add = $db -> call(true, 0, true);
			$db->close();
			return json_encode($add);

		} catch(Exception $e) {
			return $e -> getMessage();
		}
	}

	public function fetchTree($widgetList,$treeHasWrite = false) {
		$tam = count($widgetList);
			//Container dos widgets aninhados
			$nresult = array();
			$aux = '';
			$k = 0;
			for($i = 0; $i < $tam ; $i++) {
				//Inicializa indice
				$index = $widgetList[$i] -> parent;
				
				if($aux != $index) {
					//Inicializa o nó pai
					$parentWid = new stdClass();
					$parentWid -> parent_id = $widgetList[$i] -> parent_id;
					$parentWid -> name = $widgetList[$i] -> parent;
					$parentWid -> icon = $widgetList[$i] -> parent_icon;
					$parentWid -> children = array();
					
					//Adicionando a categoria pai ao array
					if(!isset($nresult[$k])) {
						$nresult[$k] = $parentWid;
						//guarda o valor do nó anterior
						$ant = $k;
						//avança um nó
						$k++;	
					}
					$x = 0;
				    $iaux = $index;
				  
					for($j = 0; $j < $tam; $j++) {
						// Inicializa o nó filho
						$childWid = new stdClass();
						$childWid -> id = $widgetList[$j]  -> id;	
						$childWid -> name = $widgetList[$j]  -> widget;
						$childWid -> icon = $widgetList[$j]  -> icon;
						$childWid -> children = array();
						 //Verifica se o elemento atual é igual ao indice (elemento pai obtido no laço mais externo)
						  if($iaux == $widgetList[$j] -> parent) {
								//Adicionando permissões
								if($treeHasWrite) {
									$write = array('parent_id' => $widgetList[$j]  -> id, 'id' => $widgetList[$j]  -> id.$x, 'text' => 'Escrita', 'icon' => 'fa fa-1x fa-pencil');
									$childWid -> children[] = $write;	
								}
								
								//Adicionando nó filho
							    $nresult[$ant] -> children[] = $childWid;
								$x++;	
						  }
						  $childWid = null;
					}
				}
				$aux = $widgetList[$i]  -> parent;
				$parentWid = null;
			}
			return $nresult;
	}
	
	public function dataParammedAction($obj, $acc_id=true) {
		if($acc_id) {
			$obj->account_id = $_SESSION['user']->getAccount(); //add id da conta logada
		}
		
		$needles = array('date', 'validate', 'data', 'dt');
		
		$fields = array();
		$data = array();
		foreach ($obj as $key=>$item) {
			if(!empty($obj->id)) {
				$fields[] = $key.' = :'.$key;
			} else {
				$fields[] = $key;
			}
			$thisKey = ":$key";
			if($this->strposa($key, $needles) !== false) {
				$item = $this->formatDateAction($item, '/');
			}
			$data[$thisKey] = $item;
		}
		
		return array($fields, $data);
	}
	
	private function strposa($haystack, $needles=array(), $offset=0) {
		$chr = array();
		foreach($needles as $needle) {
			$res = strpos($haystack, $needle, $offset);
			if ($res !== false) $chr[$needle] = $res;
		}
		if(empty($chr)) return false;
		return min($chr);
	}
}