<?php
Class freecommandController extends defaultController {
	
	public function indexAction($infos = null) {
		$unit = new unitController();
		$return = array();

        $c = new Command();
        $commandtypes['commandTypeinfo'] = $c->getCommandTypes();

		$return['commandTypeinfo'] = json_encode($commandtypes);

		$return['groupinfo'] = $unit -> listGroupAction();
		$return['subgroupinfo'] = $unit -> listSubGroupAction();
		$return['userinfo'] = json_decode($_SESSION['user'] -> getUserInfo());

        

		return json_encode($return);
	}
	
	public function listAction() {
		try {
			$result = array();

            $c = new Command();
            $result = $c->getCommandsLast(100);

			$return = array("data" => $result);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}

	public function getByIdAction($id) {
		try {
			$result = array();
			$return = array("data" => $result);	
			return json_encode($return);

		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function addAction($obj) {
		try {
            $okay = "yes";

            if ($obj->units[0] == "0") {
                // usuário pediu que o comando seja enviado a /todas/
                // as unidades de todos os grupos.
                $sql = "select tu.id as unit_id from tracked_unit tu
                           join tracked_unit_device tud on tud.tracked_unit_id = tu.id 
                                                                and tud.status = 1
                           join \"group\" g on g.id = tu.group_id
                           join user_group_access uga ON g.id = uga.group_id
                                   AND tu.group_id = uga.group_id
                                   AND tu.subgroup_id = uga.subgroup_id
                         WHERE uga.user_id = {$_SESSION['user']->getUserId()} 
                             AND tu.status = 1 
                             AND tud.device_primary = 1 ";

                $db = new DatabaseHandler();
                $rows = $db->select_fetch_v1($sql);

                $all_units = array();
                foreach ($rows as $o)
                    $all_units[] = $o->unit_id;

                $obj->units = $all_units;
            }

            // pra cada tracked_unit, construa um objeto pra função saveIt
            $c = 0; // informamos ao usuário quantos inserts fizemos
            foreach ($obj->units as $unit_id) {

                $cmd = sprintf(">%s#%s;", $obj->command, Command::getMessageId(">{$obj->command}"));
                $sum = Command::virlockChecksum($cmd);
                $cmd .= "{$sum}<";

                $x = explode(";", $obj->command);

                if (count($x) < 2)
                    return "Comando inválido.";

                $u = explode(",", $x[0]);
                
                $driver_login = "";
                $driver_pass = "";

                if (count($u) >= 2) {
                    /* o usuário pode ter digitado comandos tão inválidos
                     * que nem login/pass eu vá encontrar. nesse caso, $u
                     * pode não ser um array. daí o if acima. */
                    $driver_login = substr($u[1],0,10);
                    $driver_pass = substr($u[1],10);
                }            

                $a = array();
                $a['unit_id'] = $unit_id;
                $a['driver_login'] = $driver_login;
                $a['driver_pass'] = $driver_pass;

                $device = Command::getDevice($unit_id);

                $a['device_id'] = $device->dev_id;
                $a['device_name'] = $device->dm_name;
                $a['device_identifier'] = $device->dev_identifier;
                $a['command'] = $cmd;

                $r = Command::saveIt($a); // command_queue

                if ($r >= 1) $c++;

                if ($r <= 0) {
                    // não foi possível salvar, por alguma razão
                    // obtenha os labels das unidades pra avisar ao usuário
                    $okay = "no";
                    $uc = new UnitController();
                    $o = $uc->getUnitById($unit_id);
                    $labels[] = $o->label;
                }
            }
            
            $msg = "Commando inserido com sucesso: %s unidade%s afetada%s.";
            $plural = $c > 1 ? "s" : "";
            $msg = sprintf($msg, $c, $plural, $plural);

            if ($okay == "no") {
                $msg = sprintf("Comandos para as seguintes unidades não foram cadastrados: %s."
                               , join(", ", $labels));
            }

			$return = array("data" => NULL, "msg" => $msg);
			return json_encode($return);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
	public function editAction($obj) {
		try {
			$result = array();

			$return = array("data" => $result, "msg" => "");
			return json_encode($return);
		} catch(Exception $e) {
			return E_INTERNAL;
		}
	}
	
}
