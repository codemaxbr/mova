<?php
use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Marshaler;
use Aws\Credentials\CredentialProvider;

ini_set("memory_limit",	-1);

/*
  Implementa o acesso à tabela history_archive armazenada no DynamoDB.
*/

class HistoryArchive {

    public static function queryUnitsFromJson($o)
    {
        /* 
           Na verdade não recebemos um JSON.  Recebemos um PHP-object,
           que era um JSON-string.  A convenção aqui é que já passar o
           dado convertido.  
        */ /* $o = json_decode($json); */

        if (!is_array($o->units))
            throw new Exception("JSON tem que conter um array de nome ``units''.");

        return self::queryUnits($o->units, json_encode($o)); 
    }

    public static function queryUnits(array $units, $json)
    {
        $ret = [];
        foreach ($units as $u)
            $ret[] = self::backend_to_frontend(self::queryUnit($u, $json));

        return $ret;
    }

    public static function queryUnit($unit_id, $json)
    {
        if (intval($unit_id) <= 0)
            throw new Exception("UNIT_ID tem que ser inteiro positivo.");

        if (NULL == json_decode($json))
            throw new Exception("JSON inválido.");

        $path = "/etc/aws/credentials";
        $provider = CredentialProvider::ini("system", $path);
        $provider = CredentialProvider::memoize($provider);

        $c = DynamoDbClient::factory([
            'region' => 'us-west-2',
            'version' => 'latest',
            'credentials' => $provider
        ]);
  
        $data = $c->query(self::params($unit_id, $json));
  
        $ret = array();

        for ($i = 0; $i < $data['Count']; $i++) {
            $e = $data['Items'][$i];
            foreach ($e as $k => $v) {
                $ret[$i][$k] = $v['S'];
            }
        }

        return $ret;
    }

    public static function date_iso_format($date,$time) {
        $xs = explode("/", $date);
        return "{$xs[2]}-{$xs[1]}-{$xs[0]} {$time}:00";
    }

    public static function events(array $events) {
        /*
          Consome um array de tracker_event_id numérico.  Esse array
          vem do JSON do frontend.  São os eventos aos quais o usuário
          está interessado.  Vamos pedir ao DynamoDB que filtre a
          consulta por esses eventos.  Pra isso é preciso construir um
          placeholder pra cada evento.  O DynamoDB não permite um
          filtro literal.  Somos obrigados a construir esses
          placeholders.
         */

        $ret = [];
        foreach ($events as $k => $v) {
            $ret[sprintf(":e%s", $k)] = $v;
        }

        /* return array( ":e1" => 71, ":e2" => 2, ":e3" => 82, ":e4" => 1 ); */
        return $ret;
    }

    public static function events_list_placeholders($a) {
        foreach (self::events($a) as $var => $val)
            $ret[] = $var;

        return join(",", $ret);
    }

    public static function attribute_values($a) {
        foreach ($a as $var => $v)
            $ret[$var] = array("S" => "{$v}");
    
        return $ret;
    }

    public static function params($unit_id, $json)
    {
        /*
          Consome o ID de uma unidade e um JSON com os parâmetros do
          relatório desejado, retornando o array $params que a API DynamoDB
          precisa.  

          Note que a interface do usuário nos envia todas as unidades em que
          o usuário está interessado, mas precisamos consultar unidade a
          unidade ao DynamoDB.  Portanto, esta função constrói o array
          $params pra uma unidade só.  Ela ignora o array units do JSON.

          A unidade que será consultada é o argumento $unit_id.
        */

        $j = json_decode($json); 

        if (isset($j->events[0]) && $j->events[0] == 0)
            unset($j->events[0]);

        $a = [];
        $a[":u"] = $unit_id;
        $a[":d1"] = self::date_iso_format($j->start_day, $j->start_hour);
        $a[":d2"] = self::date_iso_format($j->end_day, $j->end_hour);
        $a = array_merge($a, self::events($j->events));

        $ret = array();
        $ret["TableName"] = "history_archive";
        $ret["KeyConditionExpression"] =
                                       "unit_id = :u AND local_time BETWEEN :d1 AND :d2";

        if (count($j->events))
            $ret["FilterExpression"] =
                                     sprintf("tracker_event_id IN (%s) ", 
                                             self::events_list_placeholders($j->events));

        $ret["ExpressionAttributeValues"] = self::attribute_values($a);
        return $ret;
    }

    public static function get_a_name(&$names, $id, $sql) {
        /*
          Abstrai o código em comum entre

            1. get_label_by_unit_id()
            2. get_event_by_tracker_event_id()

          Note que $names é um array estático, que precisa ser
          declarado como estático em suas funções de origem.  Não
          poderia ser declarado estático aqui, obviamente.

          Pra que esta função faça seu trabalho, é preciso que sua
          consulta SQL retorne pelo menos uma coluna id e uma coluna
          name.  Repare em get_label_by_unit_id() como traduzimos a
          coluna ``label'' para ``name''.

         */

        if (!is_array($names))
            throw new Exception(sprintf("Esperava por array, obtive %s.", gettype($names)));

        if (count($names) > 0)
            return array_get($names, $id);

        $db = new DatabaseHandler();
        $a = $db->select_fetch_v1($sql);

        foreach ($a as $e) {
            $names[$e->id] = $e->name;
        }

        return array_get($names, $id);
    }

    public static function get_event_by_tracker_event_id($id)
    {
        static $names = array();
        return self::get_a_name($names, $id, "SELECT * FROM tracker_event");
    }

    public static function get_label_by_unit_id($id) {
        static $labels = array();
        return self::get_a_name($labels, $id, "SELECT id, label as name FROM tracked_unit");
    }

    public static function backend_to_frontend($a)  {

        if(!is_array($a))
            throw new Exception(sprintf("Oops.  Esperava um array, obtive %s.", gettype($a)));

        $ret = array();

        foreach ($a as $v) {
            $o = new stdClass();
            $o->data = strftime("%d/%m/%Y",strtotime(array_get($v,"local_time")));
            $o->data_report = strftime("%d/%m/%Y",time());
            $o->devst_id = 0;
            $o->event = self::get_event_by_tracker_event_id(array_get($v,"tracker_event_id"));
            $o->full_address = array_get($v,"address");
            $o->full_date = array_get($v,"local_time");
            $o->hora_min = strftime("%H:%M",strtotime(array_get($v,"local_time")));
            $o->id = array_get($v, "unit_id");
            $o->label = self::get_label_by_unit_id($o->id);
            $o->latitude = array_get($v,"latitude");
            $o->longitude = array_get($v,"longitude");
            $o->local_time = array_get($v,"local_time");
            $o->velocidade = array_get($v,"speed");
            
            $ret[] = $o;
        }

        return $ret;
    }

}
?>
