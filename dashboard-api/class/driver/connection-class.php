<?php
/**
 * 
 * Class Connection
 * PHP version 5.x
 * 
 * Classe de conexao com o banco de dados, define uma variavel global $linkConexao para ser utilizada em todo sistema 
 * 
 * @author Rafael Silva
 * @category Connection
 * @package Driver
 * @uses connection-settings.ini
 */
Class Connection{

	private $driver;
	private $host;
	private $port;
	private $database;
	private $user;
	private $pass;
    private $server = PRODSERVER;

	public function setHost($host){

		$this->host = $host;
	}
	public function getHost(){
		return $this->host;
	}

	public function setUser($user){
			
		$this->user =  $user;
	}

	public function getUser(){
		return $this->user;
	}
	public function setPass($pass){
			
		$this->pass = $pass;
	}

	public function getPass(){
		return $this->pass;
	}

	public function setDriver($driver){
		$this -> driver = $driver;
	}
	public function getDriver(){
		return $this -> driver;
	}

	public function setPort($port){
		$this -> port = $port;
	}
	public function getPort(){
		return $this -> port;
	}

	public function setDatabase($database){
		$this -> database = $database;
	}

	public function getDatabase(){
		return $this -> database;
	}
	
	public function getServer() {
		if(getenv("MOVA")) {
			return DEVSERVER;
		}
		return $this -> server;
	}
	public function __construct(){

		try {

			if($config = parse_ini_file("connection-settings.ini",true)){
				
					$server = $this -> getServer();
				
					$this -> setDriver($config[$server]['driver']);
					$this -> setHost($config[$server]['host']);
					$this -> setPort($config[$server]['port']);
					$this -> setDatabase($config[$server]['dbname']);
					$this -> setUser($config[$server]['user']);
					$this -> setPass($config[$server]['password']);
		
			}else{
				//TODO Alterar mensagem de erro
				throw new PDOException("Falha ao se comunicar com o servidor, tente novamente.");
			}
		} catch (PDOException $e) {
			return $e -> getMessage();
		}

	}
	
	public function open(){
		$this -> newConnection();
	}

	private function newConnection(){
		/**
		 * Link de conexão, associado a uma variável global.
		 */
		global $linkConexao;
		
		
		try{
			
			$linkConexao = new PDO($this -> getDriver().$this -> getHost().$this -> getPort().$this -> getDatabase(),$this -> getUser(), $this -> getPass());

			if(!isset($linkConexao) or $linkConexao == false){
				//TODO Alterar mensagem de erro
				throw new Exception("Falha ao se comunicar com servidor, tente novamente.");
			}

		}catch(Exception $e){

			echo $e->getMessage();
			exit();
		}

		$linkConexao -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$linkConexao -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
			
	}
	
	/**
	 * Faz a chamada aos metodos necessarios para se conectar diretamente a intranet 
	 */	
	public function openIntranet(){

		$this -> loadOptions();
		
		$this -> connectToIntranet();
	}
	
	/**
	 * Carrega opcoes de driver para conexoes adicionais
	 * OBS: alterar de acordo com o necessario
	 */
	private function loadOptions(){
		$this -> setDatabase("dbname = intranet;");
	}
	
	/**
	 * Realiza a conexao com o servidor da intranet
	 */
	private function connectToIntranet(){
		
		/**
		 * Link de conexão, associado a uma variável global.
		 */
		global $linkConexao;
		
		try{
			
			$linkConexao = new PDO($this -> getDriver().$this -> getHost().$this -> getPort().$this -> getDatabase(),$this -> getUser(), $this -> getPass());

			if(!isset($linkConexao) or $linkConexao == false){
				//TODO Alterar mensagem de erro
				throw new Exception("Falha ao se comunicar com servidor, tente novamente.");
			}

		}catch(Exception $e){

			echo $e->getMessage();
			exit();
		}

		$linkConexao -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$linkConexao -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	}
	
	
}