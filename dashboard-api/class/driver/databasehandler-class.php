<?php
/**
 * Class DatabaseHandler
 *
 * Classe que abstrai o controle de consultas, inserts, updates e deletes no banco de dados
 * monta as queries dinamicamente atraves dos parametros passados, o retorno depende do tipo de query executada
 *
 */

Class DatabaseHandler {
	/**
	 * @var String $statement
	 * String da query montada dinamicamente pelos parametros passados
	 */
	private $statement = null;
	/**
	 * @var Array $fields
	 * Array contendo os campos a serem utilizados na query, os campos podem ser passados com alias
	 * EX: "widget.user_id as widgetuser"
	 */
	private $fields;
	/**
	 * @var Array $param
	 * Array com os parametros vinculados a seus respectivos valores
	 * Ex: ":login" => "teste"
	 */
	private $param;
	/**
	 * @var Array $paramType
	 * Array com os tipos dos parametros, necessario para stored procedures
	 */
	private $paramType;
	/**
	 * @var Array $joinTable
	 * Array com as tabelas a serem associadas por join, as tabelas podem ser passadas com alias
	 * EX: "widget_category as wdc"
	 */
	private $joinTable;
	/**
	 * @var Array $joinTableType
	 * Indica o tipo de join a ser utilizado, deve possuir um tipo para cada join utilizado
	 * EX: "INNER JOIN","LEFT JOIN","RIGHT JOIN"
	 */
	private $joinTableType;
	/**
	 * @var Array $joinTableOn
	 * Array com as igualdades utlizadas para os joins, podem conter alias
	 * EX: "wdc.id = wd.widget_category_id"
	 */
	private $joinTableOn;
	/**
	 * @var Array $logicalOperator
	 * Operadores logicos da clausula WHERE
	 * EX: "AND","OR"
	 */
	private $logicalOperator;

	/**
	 * @var Array $where
	 * Igualdades da clausula where, podem receber alias
	 * Ex: "wdc.id = wd.widget_category_id"
	 */
	private $where;
	/**
	 * @var Array $orderBy
	 * Campos utilizados para ordenar o retorno de uma consulta
	 */
	private $orderBy;
	/**
	 * @var Array $orderBySort
	 * Ordem (ASC ou DESC) das colunas a serem retornadas por uma consulta
	 * a ordem é passada individualmente para cada campo da clausula ORDER BY
	 * EX: Se $orderBy contem os campos "a" e "b", $orderBySort pode conter apenas DESC montando a seguinte clausula ORDER BY a DESC, b
	 * ou orderBySort pode conter DESC,ASC ficando ORDER BY a DESC,b ASC
	 */
	private $orderBySort;

	/**
	 * @var String $orderByLimit
	 * Define o limite de linhas retornadas no formato "min,max"
	 */
	private $orderByLimit;

	/**
	 * @var Array $having
	 * Clausula Having de uma consulta, utiliza o mesmo formato das igualdades
	 * Ex: "xpto > 1000"
	 */
	private $having;
	/**
	 * @var Array $groupBy
	 * Define qual(is) campo(s) ira(ao) agrupar o resultado da consulta
	 * Ex: "widget_category_id","widget_name"
	 */
	private $groupBy;

	/**
	 * @var String $table
	 * Define a tabela a ser utilizada na clausula FROM, a tabela pode conter um alias
	 * Ex "users usr"
	 */
	private $table;
	/**
	 * @var PDO $dbh
	 * Recebe o link de conexao atraves da chamada a variavel global $linkConexao
	 */
	private $dbh;
	/**
	 * @var PDO
	 * Utilizado para manipular as queries, recebe um prepared statement passado por $dbh
	 */
	private $stmt;

	/**
	 * public __construct
	 * Construtor da classe
	 *
	 * Atribui o valor do link de conexao ativo para $dbh
	 *
	 */
	public function __construct() {

		global $linkConexao;

		// Verifica se a conexao ja foi estabelecida, instancia a classe Connection caso nao tenha sido
		if (!isset($linkConexao)) {

			$connection = new Connection();

			$connection -> open();
		}

		$this -> dbh = $linkConexao;

	}

	/*
	  VOID --> PDO

	  Retorna o objeto PDO.
	*/
	public function getPDO()
	{
		return $this->dbh;
	}

	/*
	NON-SELECT-STATEMENT --> ...
	Recebe um NON-SELECT-STATEMENT, executa o comando no banco. 
	*/

	public function exec_v1($sql)
	{
		try {
			return $this->dbh->exec($sql);
		}
		catch (PDOException $e) {
			throw $e;
		}
	}

	/* v1: bypassing this class completely */
	public function select_fetch_v1($sql)
	{
		$r = FALSE;

		try {
			$r = $this->dbh->query($sql);
		}
		catch (PDOException $e) {
			throw $e;
		}

		return $r->fetchAll();
	}

	/*
	SELECT-STATEMENT --> ...

	Recebe um SELECT-STATEMENT, executa o comando no banco. Estou 
	fazendo um hack aqui pra desburocratizar o uso da databasehandler-class.php.

	Exemplos:
	$o1 = select_v0("select current_database() as db") == is_object()
	$o2 = fetch_all_v0($o) == $o2->db == "mova2"

	*/
	public function select_v0($sql)
	{
		$this->addOption("table", $sql);
		return $this->select(FALSE, 0, TRUE);
	} 

	/*
	VOID --> ArrayOf OBJECT

	Mero wrapper para PDOStatement::fetchAll
	*/
	public function fetch_all_v0()
	{
		return $this->stmt->fetchAll();
	}


	/**
	 * public select
	 *
	 * Monta a consulta dinamicamente atraves dos parametros passados
	 * @param Boolean $hasParams, indica se a consulta possui parametros (default:true)
	 * @return Mixed, Object, Boolean
	 */
	public function select($hasParams = true, $num = 0, $sql = false) {
		//o parametro $SQL está disponivel para coleta de comendas de insert direto 
		if($sql){
			$this -> statement = $this -> table;
		}else{	
			// Inicia a consulta
			$this -> statement = "SELECT ";
			// Atribui os campos a consulta, utilizando a funcao implode para gerar a string com os campos
			$this -> statement .= implode(",", $this -> fields) . " ";
			// Determina a tabela a ser utilizada
			$this -> statement .= "FROM " . $this -> table;

			// Verifica se algum join vai ser realizado
			if (sizeof($this -> joinTable) > 0) {

				// Recupera o tamanho do array $joinTable
				$sizeJoinTable = sizeof($this -> joinTable);

				// Monta os joins [TIPO][TABELA][ON][IGUALDADE]
				for ($i = 0; $i < $sizeJoinTable; $i++) {

					$this -> statement .= " " . $this -> joinTableType[$i] . " " . $this -> joinTable[$i];
					$this -> statement .= " ON " . $this -> joinTableOn[$i];

				}
			}
			// Verifica se algum campo foi adicionado a clausula where
			if (sizeof($this -> where) > 0) {
				// Adiciona a clausula where
				$this -> statement .= " WHERE ";
				// Conta todos os campos
				$sizeWhere = count($this -> where);
				// Monta a clausula where
				for ($i = 0; $i < $sizeWhere; $i++) {

					$this -> statement .= " " . $this -> where[$i];

					// Adiciona os operadores logicos caso existam
					if (isset($this -> logicalOperator[$i])) {
						$this -> statement .= " " . $this -> logicalOperator[$i];
					}

				}

			}

			// Recupera o total de campos em $orderBy
			$sizeOrderBy = count($this -> orderBy);
			// Recupera o total de sorts
			$sizeOrderBySort = count($this -> orderBySort);

			// Verifica se $orderBy e $orderBySort foram setados
			if ($sizeOrderBy > 0 && $sizeOrderBySort > 0) {
				// Adiciona a clausula ORDER BY
				$this -> statement .= " ORDER BY ";
				// Monta a clausula ORDER BY
				for ($i = 0; $i < $sizeOrderBy; $i++) {
					$this -> statement .= $this -> orderBy[$i];

					//Verifica se existe sort para o campo
					if (isset($this -> orderBySort[$i])) {
						$this -> statement .= " " . $this -> orderBySort[$i] . " ";
					}
					// Verifica se existe um proximo campo e caso haja adiciona virgula apos o campo atual
					$j = $i;
					if (isset($this -> orderBy[$i]) && isset($this -> orderBy[++$j])) {
						$this -> statement .= ",";
					}

				}
				// Caso nao exista sort definido utiliza implode para montar a clausula order by
			} else {

				if ($sizeOrderBy > 0) {
					$this -> statement .= " ORDER BY " . implode(",", $this -> orderBy);
				}

			}
		}

		return $this -> execute($hasParams, 1);

	}

	/**
	 * public update
	 *
	 * Monta uma query para atualizar um registro no banco
	 *
	 * @param Boolean $hasParams, indica se a consulta possui parametros (default:true)
	 * @return Mixed
	 *
	 */
	public function update($hasParams = true, $sql = false, $typeReturn = 0) {
		//o parametro $SQL está disponivel para coleta de comendas de insert direto 
		if($sql){
			$this -> statement = $this -> table;
		}else{	
			$this -> statement = "UPDATE " . $this -> table . " ";
			$this -> statement .= "SET ";

			$this -> statement .= implode(",", $this -> fields);

			if (sizeof($this -> where) > 0) {
				$this -> statement .= " WHERE ";
				$sizeWhere = count($this -> where);
				for ($i = 0; $i < $sizeWhere; $i++) {

					$this -> statement .= " " . $this -> where[$i];

					if (isset($this -> logicalOperator[$i])) {
						$this -> statement .= " " . $this -> logicalOperator[$i];
					}

				}
			}
		}

		return $this -> execute($hasParams, $typeReturn);
	}

	/**
	 *
	 * public delete
	 *
	 * Monta uma query dinamicamente para remover regisros da base de dados
	 *
	 * @return Mixed, object, boolean
	 */
	public function delete($hasParams = true) {

		$this -> statement = "DELETE ";
		$this -> statement .= "FROM " . $this -> table;

		if (sizeof($this -> where) > 0) {
			$this -> statement .= " WHERE ";
			$sizeWhere = count($this -> where);
			for ($i = 0; $i < $sizeWhere; $i++) {

				$this -> statement .= " " . $this -> where[$i];

				if (isset($this -> logicalOperator[$i])) {
					$this -> statement .= " " . $this -> logicalOperator[$i];
				}

			}

		}

		return $this -> execute();
	}

	/**
	 * Monta uma query para inserir um registro em uma tabela no banco
	 * @param Boolean $hasParams, indica se a consulta possui parametros (default:true)
	 * @param Int $returnType, indica se a inserção deve possuir retorno como object ou booleando (Default:0). Alterar para 1 caso deseje manipular o retorno de um insert
	 * @param Int $bindType, indica o tipo de substituicao a ser utilizado no binding dos valores da query. 0: bind nomeado (ex: ':value,:value2'); 1: bind não nomeado (ex: '?,?,?,?'); (Default: 0)
	 * @return Mixed
	 */
	public function insert($hasParams = true, $returnType = 0, $bindType = 0, $sql = false) {
		//o parametro $SQL está disponivel para coleta de comendas de insert direto 
		if($sql){
			$this -> statement = $this -> table;
		}else{	
			$this -> statement = "INSERT INTO ";

			$this -> statement .= $this -> table;
			$this -> statement .= "(" . implode(",", $this -> fields) . ") ";
			
			// Verifica o tipo de bind, associado por nome ou ?
			if ($bindType == 0) {
				
				// Preenche a lista de valores com bind por nome, utiliza o nome dos campos
				$this -> statement .= "VALUES(:" . implode(",:", $this -> fields) . ")";

			} else {
				
				// Concatena separadamente
				$this -> statement .= "VALUES ";
									
				$pArrayLen = count($this -> param);
				// Inicia percorrendo o array mais externo (array de arrays)
				for($i = 0; $i < $pArrayLen; $i++){
					
					// Abre a listagem de valores	
					$this -> statement .= "(";
					
					$pElemLen = count($this -> param[$i]);
					$x = $i;
					$k = 0;
					// Adicionando as ? que serao substituidas pelos valores na execucao
					for($j = 0; $j < $pElemLen; $j++){
						$this -> statement .= "?";
						$k = $j;
						// Verifica se o proximo valor e nulo para adicionar virgula
						if(isset($this -> param[$i][++$k])){
							$this -> statement .= ",";
						}
					}
					
					// Fecha a listagem de valores
					$this -> statement .= ")";
					
					// Verifica se o proximo valor e nulo para adicionar virgula 
					if(isset($this -> param[++$x])){
						$this -> statement .= ",";
					}
				}

			}
		}

		return $this -> execute($hasParams,$returnType,$bindType);
	}
	
	
	public function call($hasParams = true, $returnType = 0, $sql = false){
		//o parametro $SQL está disponivel para coleta de comendas de insert direto 
		if($sql){
			$this -> statement = $this -> table;
		}else{	
			// Inicia a consulta
			$this -> statement = "SELECT ";
			// Atribui os campos a consulta, utilizando a funcao implode para gerar a string com os campos
			$this -> statement .= $this -> table;//implode(",", $this -> fields) . " ";
			// Determina a tabela a ser utilizada
			
			$this -> statement .= "(";
			$pArrayLen = count($this -> param);
				// Inicia percorrendo o array mais externo (array de arrays)
				for($i = 0; $i < $pArrayLen; $i++){
					
					// Abre a listagem de valores	
					
					$pElemLen = count($this -> param[$i]);
					$x = $i;
					$k = 0;
					// Adicionando as ? que serao substituidas pelos valores na execucao
					for($j = 0; $j < $pElemLen; $j++){
						$this -> statement .= "?";
						$k = $j;
						// Verifica se o proximo valor e nulo para adicionar virgula
						if(isset($this -> param[$i][++$k])){
							$this -> statement .= ",";
						}
					}
				}
			// Fecha a listagem de valores
			$this -> statement .= ")";
		}
		return $this -> executeProcedure();
	}
	
	
	public function query() {

		return $this -> statement;
	}

	/**
	 * public addOption
	 *
	 * Adiciona as opcoes da query de acordo com os parametros
	 *
	 * Opcoes de campos:
	 *
	 * fields (Tipo: Array) Ex: $dbh -> addOption("fields", array("users_id","id"));
	 * table (Tipo: string) Ex: $dbh -> addOption("table","session");
	 * joinTable (Tipo: Array) Ex: $dbh -> addOption("joinTable","widgets wd");
	 * joinTableOn (Tipo: Array) Ex: $dbh -> addOption ("joinTableOn",array("wdc.id = wd.category_id"));
	 * joinTableType (Tipo: Array) Ex: $dbh -> addOption("joinTableType",Array("INNER JOIN","INNER JOIN","LEFT JOIN"));
	 * where (Tipo: Array) Ex: $dbh -> addOption("where","wd.id = :id");
	 * logicalOperator (Tipo: Array) Ex: $dbh -> addOption("logicalOperator",array("AND","OR"));
	 * orderBy (Tipo: Array) Ex: $dbh -> addOption "("orderBy",array("s.users_id"));
	 * orderBySort (Tipo: Array) Ex: $dbh -> addOption("orderBySort",array("desc"));
	 * groupBy (Tipo: Array) Ex: $dbh -> addOption("groupBy",array("users_id"));
	 * having (Não Implementado);
	 * param (Tipo: Array ou Array(Array) Ex: $dbh -> addOption("param", array(":users_id" => $users_id)) Ex2: $dbh -> addOption ("param", array(array(1,2),array(1,4)))
	 *
	 * Os parametros passados devem ser string ou array de acordo com o tipo de campo
	 *
	 * Ex1: $this -> addOption("table","users usr");
	 * Ex2: $this -> addOptions("fields",array("user_id","login"));
	 *
	 * Os parametros do tipo array aceitam um ou mais valores de acordo com os exemplos acima.
	 *
	 * @param String $attribute, determina qual atributo da classe vai receber os parametros
	 * @param Mixed $param, parametros a serem atribuidos, pode ser um array ou uma string dependendo do tipo esperado por $attribute
	 */
	public function addOption($attribute, $param) {
		// Zera o campo antes de atribuir outros valores (remover lixo)
		$this -> clearField($attribute);
		// Verifica se o atributo da classe e um array
		if (is_array($this -> $attribute)) {
			// Adiciona o novo elemento no final do array
			array_push($this -> $attribute, $param);
		} else {
			// Atribui o valor a $field como uma string
			$this -> $attribute = $param;
		}

		//}
	}

	/**
	 * private execute
	 *
	 * Realiza o bind dos parametros em uma query previamente montada e a executa
	 *
	 * @param Boolean $hasParams, indica se a query possuim parametros a serem vinculados (default: false)
	 * @param String $returnType, indica se o tipo de retorno é 1: object ou 0: boolean (default: 0 boolean)
	 * @param Int $bindType, indica o tipo de substituicao a ser utilizado no binding dos valores da query. 0: bind nomeado (ex: ':value,:value2'); 1: bind não nomeado (ex: '?,?,?,?'); (Default: 0)
	 *
	 * @return Mixed, Object, Boolean
	 */
	private function execute($hasParams = true, $returnType = 0, $bindType = 0) {

		try {
			$this -> stmt = $this -> dbh -> prepare($this -> statement);

			if ($hasParams) {
				
				if($bindType == 0){
					// Vincula os atributos a seus valores (EX: :login = $login)
					foreach ($this -> param as $param => $value) {
	
						$this -> stmt -> bindValue($param, $value);
	
					}	
				}else{
				
					$arraySize = count($this -> param);
					$k = 0;
					for($i = 0; $i < $arraySize; $i++){
						
						$elemSize = count($this -> param [$i]);
						
						
						for ($j = 0; $j < $elemSize ; $j ++){
							
							$this -> stmt -> bindValue(++$k, $this -> param [$i][$j]);
						}
						
					}
				}				
			}
			
			$this -> clearAll();
			// Executa a query
			$this -> stmt -> execute();
			// Verifica o tipo de retorno e se houve retorno pela quantidade de linhas afetadas
			if ($returnType == 0) {
				if ($this -> stmt -> rowCount() > 0) {
					return true;
				} else {
					return false;
				}

			} else {
				if ($this -> stmt -> rowCount() > 0) {
					return $this -> stmt;
				} else {
					return false;
				}

			}
		} catch(Exception $e) {
			return $e -> getMessage();
		}

	}
	
	private function executeProcedure(){
		
		try {
			$this -> stmt = $this -> dbh -> prepare($this -> statement);

			$arraySize = count($this -> param);
			$k = 0;
			
			for($i = 0; $i < $arraySize; $i++){
						
				$elemSize = count($this -> param [$i]);
						
						
				for ($j = 0; $j < $elemSize ; $j ++){
							
					$this -> stmt -> bindValue(++$k, $this -> param [$i][$j],$this -> paramType[$i][$j]);
				}
						
			}
			
			$this -> clearAll();
			// Executa a query
			$this -> stmt -> execute();
			// Verifica o tipo de retorno e se houve retorno pela quantidade de linhas afetadas
			if ($this -> stmt -> rowCount() > 0) {
					return true;
			} else {
					return false;
			}
		} catch(Exception $e) {
			return $e -> getMessage();
		}
	}
	
	
	/**
	 * private clearAll
	 *
	 * Seta os valores dos atributos como nulo (evita junção de valores no caso de uma mesma instância da classe ser reutilizada)
	 *
	 */
	private function clearAll() {
		//unset();
		$this -> fields = null;
		$this -> logicalOperator = null;
		$this -> groupBy = null;
		$this -> joinTable = null;
		$this -> having = null;
		$this -> joinTableOn = null;
		$this -> joinTableType = null;
		$this -> orderBy = null;
		$this -> orderByLimit = null;
		$this -> orderBySort = null;
		$this -> param = null;
		$this -> paramType = null;
		$this -> table = null;

	}

	/**
	 * private clearField
	 *
	 * Seta o valor de um campo $field como nulo (evita junção de valores no caso de uma mesma instância da classe ser reutilizada)
	 * @param String $field, campo que vai receber null
	 */
	private function clearField($field) {
		$this -> $field = null;
	}

	/**
	 * public lastInsertId
	 *
	 * @param String $sequence, sequence da tabela
	 * @return String Id, ultimo id inserido na tabela
	 */
	public function lastInsertId($sequence) {
		return $this -> dbh -> lastInsertId($sequence);
	}
	
	/**
	 * public toIntranet
	 * Inicia uma conexao com o servidor da intranet sobrescrevendo a conexao anterior
	 */
	public function toIntranet(){
		
		global $linkConexao;

		// Verifica se a conexao ja foi estabelecida, instancia a classe Connection caso nao tenha sido
    	$connection = new Connection();
		$connection -> openIntranet();

		$this -> dbh = $linkConexao;
	}
	
	public function close(){
		global $linkConexao;
		$linkConexao = null;
	}

}
