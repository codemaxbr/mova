<?php
  /**
   * Comandos e coisas que lhe são relativas.
   */

Class Command {
    public $db; /* handler para o banco */
        
    public function __construct() {
        if (is_null($this->db))
            $this->db = new DatabaseHandler();
    }

    public function getCommandTypes() {
        $h = new DatabaseHandler();
        $sql = "SELECT * FROM command_type ORDER BY name ASC";
        $rows = $h->select_fetch_v1($sql);
        
        return $rows;
    }

    public function getCommandsLast($n) {
        if (intval($n) <= 0)
            throw new Exception(E_COMMANDDRIVER_ARGUMENT_ILLEGAL);

        $h = new DatabaseHandler();

        $sql = "select 
                   cq.id
                  ,tu.label as unit_name
                  ,cs.name as status_name
                  ,ct.name as type_name
                  ,cq.date_exec as date_exec
                  ,cq.date_add as date_insert
                      from command_queue cq
                        join command_status cs on cs.id = cq.command_status_id
                        join command_type ct on ct.id = cq.command_type_id
                        join tracked_unit tu on tu.id = cq.tracked_unit_id
                 ORDER BY cq.id DESC LIMIT {$n} "; 

        $rows = $h->select_fetch_v1($sql);
        return $rows;
    }
        
    public static function getDriver($driver_id) {
        $d = new driversController();
        $a = json_decode($d->getByIdAction($driver_id));
        $r = array();
        $r['login'] = $a->driver->login;
        $r['pass'] = $a->driver->password;
        return $r;
    }

    public static function getDevice($unit_id) {
        $u = new unitController();
        $ret = $u->getDeviceByUnit($unit_id);
        if (count($ret) == 0) 
            throw new Exception("Não foram encontrados dispositivos para a unidade {$unit_id}");
        return $ret[0];
    }

    public static function getMessageId() {
        $h = new DatabaseHandler();
        $n = $h->select_fetch_v1("SELECT nextval('command_seq') as n")[0]->n;
        
        return sprintf("%X",$n);
    }

  /* 
     STRING -> STRING

     Recebe uma string $message e retorna uma hex-string
     representando o checksum computado de acordo com o algoritmo
     escrito aqui.  Essa função computa um checksum específico do
     dispositivo VIRLOCK.

     Exemplos:
     virlockChecksum("") == 0
     virlockChecksum("a") == 0 ^ 97 = "97"
     virlockChecksum("a*xxx") == virlockChecksum("a")
     virlockChecksum("aa") == 97 ^ 97 = "0"
     virlockChecksum("hello") == ord('h') ^ ord('e') ^ ... 
  */

  public static function virlockChecksum($message) 
  {
    $checksum = 0;

    foreach (str_split($message) as $c) {
      if ($c == '*')
      break;

      $checksum = $checksum ^ ord($c);
    }

    return sprintf("%02X", $checksum);
  }

  /*
    ArrayOf STRING --> COMMAND
    
    Recebe um array de strings e retorna um comando.
  */
  public static function buildOneCommand($data) {
    if (!is_array($data))
      throw new Exception(E_COMMANDDRIVER_ARGUMENT_ILLEGAL);
    
    if (count($data) == 0)
      throw new Exception(E_COMMANDDRIVER_EMPTYCOMMAND);
 
    $o = array2object($data);

    $ret = sprintf(">{$o->instruction}{$o->memory},%s%s;ID={$o->device_identifier};#%s;*",
		   substr(str_pad($o->driver_login,10),0,10), 
		   substr(str_pad($o->driver_pass, 5),0,5), Command::getMessageId());

    $sum = Command::virlockChecksum($ret);
    return $ret . "{$sum}<";
  }

  /*
    ArrayOf OBJECT --> ArrayOf COMMAND
    
    Recebe um array de arrays de objetos contendo os dados do comando
    e retorna um array de comandos prontos.  É um mero map de 
    buildOneCommand()
  */
  public static function buildCommands($a) {
    if (!is_array($a))
      throw new Exception(E_COMMANDDRIVER_ARGUMENT_ILLEGAL);

    return array_map( array("Command","buildOneCommand"), $a);
  }
  
  public static function extractMessageIdFromCommand($s) {
    $a = explode(";", $s);
    return hexdec(substr($a[2],1));
  }

  public static function saveIt($c) {
    if (!is_array($c))
      throw new Exception(E_COMMANDDRIVER_ARGUMENT_ILLEGAL);

    $uid = $_SESSION['user']->getUserId();

    if (!is_integer($uid))
      throw new Exception(E_COMMANDDRIVER_INVALID_USER_ID);

    $c['message_id'] = Command::extractMessageIdFromCommand($c['command']);

    $a = sql_escape($c);
    $o = array2object($a); // easier syntax, less single quotes?

    $h = new DatabaseHandler();
    $sql = "INSERT INTO command_queue (
              tracked_unit_id
             ,identifier
             ,date_add
             ,attempts
             ,command_type_id
             ,command_status_id
             ,command
             ,user_id
             ,command_msg_id) VALUES (
              {$o->unit_id}
             ,{$o->device_identifier}
             ,now()
             ,0
             ,1
             ,1
             ,{$o->command}
             ,{$uid}
             ,{$o->message_id}) ";

    try {
        $ret = $h->exec_v1($sql);
    }
    catch (Exception $e) {
      throw $e;
    }

    return $ret;
  }

  public static function markFailure($c) {
    if (!is_array($c))
      throw new Exception(E_COMMANDDRIVER_ARGUMENT_ILLEGAL);

    $o = array2object($c); // easier syntax, less single quotes

    if (!is_integer(intval($o->memory)))
      throw new Exception(E_COMMANDDRIVER_ARGUMENT_ILLEGAL);

    $h = new DatabaseHandler();
    $sql = "UPDATE driver_tracked_unit SET status=2 WHERE unit_id = {$o->unit_id} AND memory_position = {$o->memory}";

    try { $ret = $h->exec_v1($sql); }
    catch (PDOException $e) {
      throw $e;
    }

    return $ret;
  }

  /*
    ArrayOf ARRAY --> ArrayOf ARRAY
    
    Recebe o input --- veja exemplos abaixo --- e processa o input um
    a um.  Se o device não for suportado, ele é ignorado.  Se não for
    possível salvar o comando em command_queue, marcamos status=2 em
    driver_tracked_unit.

    Eis um exemplo de input:

    $input = array(array('unit_id' => 4240, 'driver_id' => 18, 'memory' => 7),
                   array('unit_id' => 4311, 'driver_id' => 19, 'memory' => 11),
                   array('unit_id' => 4309, 'driver_id' => 22, 'memory' => 23));

    Eis um exemplo de output:

    array(3) {
     [0]=>array(10) {
            ["unit_id"]=>int(4340)
            ["driver_id"]=>int(18)
	    ["memory"]=>int(7)
	    ...
	    ["command"]=>string(41) ">VSRT7,012345678911379;ID=4316;#ffff;*37<"
          }

     ...
    }
  */

  public static function sendCommandDrivers($input, $flag)
  {
    if (!is_array($input)) //Input não pode ser vazio.
      throw new Exception(E_COMMANDDRIVER_ARGUMENT_ILLEGAL);

    if (!in_array($flag, array('add', 'delete'))) //'Argumento $flag tem que ser "add" ou "delete".'
      throw new Exception(E_COMMANDDRIVER_ARGUMENT_ILLEGAL);

    $commands = array();

    foreach($input as $a) {
      $driver = Command::getDriver($a['driver_id']);

      $a['instruction'] = "VSRT"; // VIRLOCK instrução (única que suportamos no momento)

      switch ($flag) {
      case 'add':
          $a['driver_login'] = $driver['login'];
          $a['driver_pass'] = $driver['pass'];
          break;
      case 'delete':
          $a['driver_login'] = "----------";
          $a['driver_pass'] = "-----";
          break;
      default:
          throw new Exception('Argumento $flag tem que ser "add" ou "delete".');
      }

      $device = Command::getDevice($a['unit_id']);
      //XXX: nome exato ou aproximado?
      if (!preg_match("/VIRLOC/i", $device->dm_name)) {
          //$a['error'] = "Ignorado. Device não suportado no momento.";
          continue;
      }
      
      $a['device_id'] = $device->dev_id;
      $a['device_name'] = $device->dm_name;
      $a['device_identifier'] = $device->dev_identifier;
      
      $a['command'] = Command::buildOneCommand($a);
      
      $commands[] = $a;
    }
    
    $ret = array();
    
    foreach($commands as $c) {
        $r = Command::saveIt($c); // command_queue
        
        if ($r == 0) {
            $c['saveIt_error'] = "Nenhuma exception.  Entretanto, 0 affected rows.";
            Command::markFailure($c, $c['saveIt_error']); // driver_tracked_unit
        }
        
        $ret[] = $c;
    }
    
    return $ret;
  }
}
