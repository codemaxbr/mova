<?php
$json = json_decode($_POST['dados']);

header('Content-Type: application/excel');
header('Content-Disposition: attachment; filename="Relatorio_Telemetria_'.date('Y-m-d').'.csv"');

foreach ($json as $dados){
	$string[] = array(
		$dados->id,
		$dados->devst_id,
		$dados->driver_id,
		$dados->label,
		$dados->data,
		$dados->full_date,
		$dados->data_report,
		$dados->hora_min,
		$dados->local_time,
		$dados->longitude,
		$dados->latitude,
		$dados->velocidade,
		$dados->odometro,
		$dados->motorista,
		$dados->rpm,
		$dados->sensor1,
		$dados->sensor2,
		$dados->full_address,
		$dados->event

	);
}

$fp = fopen('php://output', 'w');
foreach ($string as $line) {
    fputcsv($fp, $line, ',');
}

fclose($fp);

?>