<?php
$json = json_decode($_POST['dados']);

header('Content-Type: application/excel');
header('Content-Disposition: attachment; filename="Relatorio_Jornada_'.date('Y-m-d').'.csv"');

foreach ($json as $dados){
	foreach ($dados->itens as $item){
		$string[] = array(
			$dados->label,
			$item->data_relatorio,
			$item->hora_inicio,
			$item->hora_fim,
			$item->POI_inicio,
			$item->POI_fim,
			$item->total_jornada,
			$item->inicio_long,
			$item->inicio_lat,
			$item->fim_long,
			$item->fim_lat,
			$item->initial_history_id,
			$item->last_history_id,
			$item->initial_address,
			$item->last_address

		);
	}
}

$fp = fopen('php://output', 'w');
foreach ($string as $line) {
    fputcsv($fp, $line, ',');
}

fclose($fp);

?>