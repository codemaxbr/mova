<?php
/*
  Ferramentas.
*/

function dq($x) { var_dump($x); exit; }
function d($x) { var_dump($x); }

/*
  Recebe um array, um índice.  Se o índice existir no array, retorna o
  valor dele.  Senão, retorna um valor padrão pré-definido.
*/

function array_get($a, $index) {
    if (isset($a[$index]))
        return $a[$index];
    
    return ""; // empty string como valor padrão.
}

/*
  ARRAY OR STRING -> ARRAY OR STRING

  Recebe um array x e pra cada elemento desse array, chama
  PDO::quote() pra que o conteúdo seja protegido. Assim ficamos livres
  pra usar o dado em consultas SQL. Protege contra erros de sintaxe
  provocados por input do usuário ou mesmo contra SQL-injection.

  Exemplos:
  sql_escape(array("John's Bar", "no problem")) == array("'John''s Bar'", "'no problem'")
  sql_escape("John's Bar") == "'John''s Bar'"
  sql_escape("") = "''"
*/

function sql_escape($a) 
{
  /* só sei trabalhar com strings ou arrays (ou stdClass) */
  $h = new DatabaseHandler();

  switch ( gettype($a) ) {
  case 'object':
    return sql_escape(object2array($a));;
    break;

  case 'array':
    foreach($a as $k => $e) {
      if (is_array($e)) {
        $a[$k] = sql_escape($e);
        continue;
      }
      $a[$k] = $h->getPDO()->quote($e);
    }
	
    return $a;
    break;
	
  case 'string':
    $a = $h->getPDO()->quote($a);
    return $a;
    break;

  default: // datatype not supported: therefore no changes
    return $a;
  } // end of switch
}

function object2array($o)
{
  $a = (array) $o;
  
  foreach($a as $k => $v)
    if( is_object($v))
      $a[$k] = object2array($v);

  reset($a);
  return $a;
}

function array2object($a)
{
  foreach($a as $k => $v)
    if( is_array($v))
      $a[$k] = array2object($v);

  return (object) $a;
}

function map($f, $a)
{
  if (!count($a)) return array();
  
  foreach ($a as $k => $v) {
    $ret[] = call_user_func($f, $v);
  }
  
  return $ret;
}

function fold($op, $init, $a) {
  if (!count($a)) return $init;
  return call_user_func($op, array_shift($a), fold($op, $init, $a));
}
?>