<?php
/**
 * 
 * @author ADM
 *
 */
Class User{
	
	/**
	 * 
	 * @var String $name
	 * Nome do usuario
	 */
	private $name;
	
	/**
	 * 
	 * @var Int $account
	 * Identificador da conta do usuario
	 * 
	 */
	private $account;
	
	/**
	 * 
	 * @var Array $widgets
	 * Widgets associados a conta do usuario
	 * 
	 */
	private $widgetsAccess = array();
	
	/**
	 * 
	 * @var Array $accessLevel
	 * Niveis de acesso dos widgets associados a conta do usuario
	 * 
	 */
	private $accessLevel;
	
	/**
	 * 
	 * @var String $location
	 * IP de acesso do usuario
	 * 
	 */
	private $location;
	
	/**
	 * 
	 * @var String $webBrowser
	 * Navegador utilizado pelo usuario
	 * 
	 */
	private $webBrowser;
	
	/**
	 * 
	 * @var String $userId
	 * Id do usuario, utilizado para gravar os cookies de sessao
	 * 
	 */
	private $userId;
	/**
	 * 
	 * @var String $token
	 * Token de sessao, utilizado para validacao do usuario em cookies de sessao
	 */
	private $token;
	/**
	 * @var String $seriesIdentifier
	 * Identifica multiplas sessoes geradas a partir de um unico login
	 */
	private $seriesIdentifier;
	
	public function __construct(stdClass $usuario,  array $widgetsMenu, array $accesLevel){
			
		// Nome do usuario
		$this -> name = $usuario -> name;
		// Account do usuario
		$this -> account = $usuario -> account_id;
		// Recebe a string JSON formatada do menu
		$this -> widgetsAccess = $this -> createMenu($widgetsMenu);
		// Recupera o endereco IP do  usuario
		$this -> location = $_SERVER['REMOTE_ADDR'];
		// Recupera o navegador do usuario
		$this -> webBrowser = $_SERVER['HTTP_USER_AGENT'];
		// Recebe o array de niveis de acesso
		$this -> accessLevel = $accesLevel;
		// Login do usuario
		$this -> userId = $usuario -> userId;
		// Gera um hash unico a partir da string abaixo para a sessao do usario (token da sessao)
		$this -> token = str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
		// Gera um hash unico a partir da combinacao abaixo que sera utilizado para identificar 
		// todas as sessoes realizadas apos o primeiro login (renovacao de token a partir de cookies)
		$this -> seriesIdentifier = uniqid(rand(time(),time()+999999));
		
	}

	public function setUserId($i) {
		// hack pra me ajudar no desenvolvimento
		// assim consigo simular um usuário rapidamente
		// sem ter que me logar no sistema, sem ter que ir à web
		$this->userId = $i;
	}

	/**
	 * getName
	 * 
	 * Retorna o nome do usuario
	 * 
	 * @return String $name
	 * 
	 */
	public function getName(){
		return $this -> name;
	}
	
	/**
	 * getAccount
	 * 
	 * Retorna o id da account do usuario
	 * 
	 * @return Int $account
	 * 
	 */
	public function getAccount(){
		return $this -> account;	
	}


	/**
	 * getUserInfo
	 *
	 * Retorna informacoes em JSON relacionadas ao usuario, cliente e conta
	 * @return String $userInfo
	 */
	public function getUserInfo() {
		$user = new userController();
		$userInfo = $user->getUserAction($this->getUserId());
		return $userInfo;	
	}
	
	/**
	 * getWidgetAccess
	 * 
	 * Retorna os elementos que compoe o menu (widgets) devidamente formatados em JSON e com correcoes de caracteres especiais
	 * 
	 * @return String $widgetAccess
	 * 
	 */
	public function getWidgetAccess(){
		return html_entity_decode(json_encode($this -> widgetsAccess));
	}
	
	/**
	 * 
	 * getAccessLevel
	 * 
	 * Recebe o ID do widget como parametro para o array de niveis de acesso $accessLevel
	 * e retorna o nivel de acesso associado
	 * 
	 * @param Int $widgetId
	 * @return Int $accessLevel
	 * 
	 */
	public function getAccessLevel($widgetId){
		return $this -> accessLevel[$widgetId];
	}
	/**
	 * getLocation
	 * 
	 * Retorna o local de acesso (IP) do usuario
	 * 
	 * @return String $location
	 * 
	 */
	public function getLocation(){
		return $this -> location;
	}
	/**
	 * getWebBrowser
	 * 
	 * Retorna o navegador do usuario
	 * 
	 * @return String $webBrowser
	 * 
	 */
	public function getWebBrowser(){
		return $this -> webBrowser;
	}
	/**
	 * 
	 * getUserId
	 * 
	 * Retorna o id do usuario
	 * 
	 * @return String $userId
	 * 
	 */
	public function getUserId(){
		return $this -> userId;
	}
	/**
	 * getToken
	 * 
	 * Retorna o token de autenticacao da sessao do usuario
	 * 
	 * @return String $token
	 * 
	 */
	public function getToken(){
		return $this -> token;
	}
	/**
	 * 
	 * getSeriesIdentifier
	 * 
	 * Retorna o identificador de sessoes do usuario
	 *  
	 * @return String $seriesIdentifier
	 * 
	 */
	public function getSeriesIdentifier(){
		return $this -> seriesIdentifier;
	}
	
	/**
	 * 
	 * @param Array $widgetsMenu
	 * Finaliza a criacao do menu, recebe um array contendo os widgets organizados por categorias
	 * converte para o formato JSON e elimina os elementos desnecessarios para o mustache
	 * @return string $encodedMenu, string no formato JSON
	 */
	private function createMenu($widgetsMenu){
		
		// Remove os elementos numerados da string JSON que compoe o menu
		function replaceNumbers($matches){
			// Padrao de busca (numeros de 0 a 9 seguidos por :{
			$pattern = "/\"[0-9]{1,2}\":{/";
			// Busca os elementos de acordo com o padrao e substitui por {
			return preg_replace($pattern, "{", $matches[0]);
		}
		
		// Codifica o array $widgetsMenu para json
		$encodedMenu =  json_encode($widgetsMenu);
		
		// Remove um nivel do JSON, a "{" faz com widget seja um objeto, contendo outros objetos, removendo
		// a "{" widget se torna um array de objetos (e o que queremos)
		$encodedMenu = preg_replace("/\"widget\":\[{\".\":/", "\"widget\":[", $encodedMenu);
		
		// Padrao de busca (numeros de 00 a 99 seguidos por :{
		// preg_replace_callback nao funciona se um padrao nao for passado
		$pattern = "/\"[0-9]{1,2}\":{/";
		
		// Utiliza a funcao preg_replace_callbal para remover os indices numericos recursivamente
		// para permitir que o mustache acesse diretamente os objetos
		$encodedMenu = preg_replace_callback($pattern, "replaceNumbers", $encodedMenu);
		
		// Remove "}" extras para normalizar a string
		// Retorna uma string JSON valida que vai ser parseada pelo JS
		return str_replace("]}}]}}","]}]}}",$encodedMenu);
	
	}
	
}