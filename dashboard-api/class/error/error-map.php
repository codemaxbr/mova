<?php
/**
 * Mapa de erros do sistema
 * Todas as constante de erros do sistema serão definidas aqui
 * 
 */
 
//ERROS GERAIS

DEFINE("E_INTERNAL","Houve uma falha ao processar sua solicitação, tente novamente. Código do erro: EDSBINT -0901"); 
DEFINE("E_UNABLETOLOADWIDGET","Não foi possível carregar o widget, tente novamente. Código do erro: EDSBUNLDWDG -0902");
DEFINE("E_FAILEDTOLOADPERMISSION","Não foi possível carregar todos os recursos associados. Código do erro: EDSBFLDTLDPRM -0903");
//DEFINE("E_SESSIONEXPIRED","infelizmente a sessão expirou. Código do erro: -0904");
DEFINE("E_SESSIONEXPIRED","-0904");
 
// LOGIN
DEFINE("E_INVALID","Usuário e/ou senha de acesso inválidos. Tente novamente. Código do erro: EDSBLGN -1001");
DEFINE("E_ACCINVALID","Essa conta não existe.");
DEFINE("E_DENIED","Este usuário não possui os privilégios necessários para acessar o sistema, em caso de dúvidas entre em contato com o suporte.Código do erro: EDSBLGND -1002");
DEFINE("E_INVALIDSESSION","Acesso inválido. Por favor autentique-se.Código do erro: EDSBLGNINSE -1003");
DEFINE("E_UNABLETOCREATE","Ocorreu um erro ao acessar o sistema, por favor tente novamente. Código do erro: EDSBLGNUTC -1004");
DEFINE("E_UNABLETOLOGIN","Ocorreu um erro ao tentar efetuar login, por favor tente novamente. Código do erro: EDSBLGNUTL -1005");
DEFINE("E_INVALIDTIME","Você não possui permissões para acessar o sistema neste horário. Código do erro: EDSBLGNINVTM -1006");
DEFINE("E_INVALIDWEEKDAY","Você não possui permissões para acessar o sistema neste dia da semana. Código do erro: EDSBLGNINVWKD -1007");
DEFINE("E_USEREXPIRED","O acesso deste usuário foi encerrado. Código do erro: EDSBLGNUSREXP -1008");

// WIDGET ACCOUNT
DEFINE("E_INVALIDPERMISSIONLEVEL","A conta deve ter ao menos 1 permissão. Código do erro: EDSBACIPRM -2003");
DEFINE("E_UNABLETOCREATEGROUP","Não foi possível adicionar um grupo a conta. Código do erro: EDSACUAGR -2004");
DEFINE("E_UNABLETOCREATESUBGROUP","Não foi possível adicionar um subgrupo a conta. Código do erro: EDSACUAGR -2005");
DEFINE("E_UNABLETOEDITGROUP","Não foi possível editar o grupo. Código do erro: EDSACUEGR -2006");
DEFINE("E_UNABLETOEDITSUBGROUP","Não foi possível editar o subgrupo. Código do erro: EDSACUEGR -2007");
DEFINE("E_UNABLETOUPDATETYPE","Não foi possível atualizar o tipo de conta. Código do erro: EDSACUPTP -2008");
DEFINE("E_UNABLETOCREATEACCOUNT","Não foi possível adicionar a conta. Código do erro: EDSAUCRACC - 2009");
DEFINE("E_UNABLETOADDWIDGETS","Não foi possível adicionar os widgets a conta. Código do erro: EDSAUCADWDG- 2010");
DEFINE("E_UNABLETOUPDATEACCOUNT","Não foi possível atualizar a conta. Código de erro: EDSAUUPACC -2011");
DEFINE("E_UNABLETODELETEPERMISSIONS","Houve um erro ao tentar alterar as permissões da conta.Código do erro: EDSAUDLPRM -2012");
DEFINE("E_UNABLETOUPDATEPERMISSIONS","Não foi possível alterar as permissões da conta.Código do erro: EDSAUUPPRM -2013");
DEFINE("E_UNABLETOSELECTACCOUNT","Não foi possível recuperar a conta selecionada. Código do erro: EDSAUNLSAC -2014");
DEFINE("E_UNABLETOLISTACCOUNTGROUP","Não foi possível recuperar o(s) grupo(s) associado(s) a esta conta. Código do erro: EDSBUNLSACGR -2015");
DEFINE("E_UNABLETOLISTACCOUNTPERMISSION","Não foi possível recuperar uma ou mais permissões associadas a esta conta. Código do erro: EDSNUNLSACPRM -2016");
DEFINE("E_UNABLETOLISTACCOUNTTYPE","Não foi possível listar os tipos de contas cadastradas. Código do erro: EDSAUNLSACT -2017");
DEFINE("E_UNABLETOSELECTACCOUNTTYPE","Não foi possível recuperar o tipo de conta selecionado. Código do erro: EDSAUNSLACT -2018");
DEFINE("E_NOGROUPSELECTED","Nenhum grupo foi editado. Código do erro: EDSBNGSL -2019");
DEFINE("E_UNABLETODELETEACCOUNT","Não foi possível remover a conta selecionada. Código do erro: EDSBUNATODELACO -2020");
DEFINE("E_WIDGETACCOUNTEXISTS","Nome da conta já existente com este Tipo de Conta. Código do erro: EDSBWDEXIST -2021");
DEFINE("E_WIDGETTYPEACCDEP","Há contas relacionadas à este tipo de conta. Código do erro: EDSBWDETACDEP -2022");
//DEFINE("E_UNABLETOADDSUBGROUP","Não foi possível adicionar o subgrupo.Código do erro: EDSAUNSLACT -2019");
//DEFINE("","Não foi possível recuperar o tipo de conta selecionado. Código do erro: EDSAUNLSAC -2019");

// WIDGET ACCOUNT > CLIENT
DEFINE("E_UNABLETOUPDATEGROUP","Não foi possível alterar o nome de um grupo. Código do erro: EDSACLUNUPGR -3001");
DEFINE("E_UNABLETOUPDATESUBGROUP","Não foi possível alterar o nome de um subgrupo. Código do erro: EDSACLUNUPSBGR -3002");
DEFINE("E_UNABLETOADDCLIENT","Não foi possível cadastrar o widget. Código do erro: EDSBADCL -3003");
DEFINE("E_UNABLETOLISTCLIENT","Não foi possível listar os clientes . Código do erro: EDSBUNLSCL -3004");
DEFINE("E_UNABLETOCREATECLIENTSUBGROUP","Não foi possível adicionar um subgrupo a conta. Código do erro: EDSBUNLSCL -3005");
DEFINE("E_UNABLETOLISGROUPTREE","Não foi possível listar os grupos.Código do erro: EDSBUNLSGRTR -3006");
DEFINE("E_UNABLETOLISTACCOUNTTREE","Não foi possível listar os grupos associados a conta.Código do erro: EDSBUNLSACTR -3007");
DEFINE("E_UNABLETOREMOVESUBGROUPACCOUNT","Não foi possível remover os subgrupos da conta. Código do erro: EDSBUNRMSBGACC -3008");
DEFINE("E_UNABLETOREMOVEGROUPACCESS","Não foi possível remover os acessos dos grupos. Código do erro: EDSBUNRMGRAC -3009");
DEFINE("E_UNABLETOREMOVEGROUP","Não foi possível remover os grupos. Código do erro: EDSBUNRMGR -3011");
DEFINE("E_FAILEDTOREMOVESUBGROUP","Não foi possível remover o subgrupo. Código do erro: EDSBUNRMSBGP -3012");
DEFINE("E_FAILEDTOREMOVESUBGROUPACCESS","Não foi possível remover o subgrupo. Código do erro: EDSBUNRMSBGPACS -3013");
DEFINE("E_FAILEDTOREMOVESUBGROUPUNIT","Não foi possível remover o subgrupo. Código do erro: EDSBUNRMSBGPUNT -3014");
DEFINE("E_FAILEDTOREMOVEGROUP","Não foi possível remover o grupo. Código do erro> EDSBFLRMGRP -3015");
DEFINE("E_FAILEDTOREMOVEGROUPSUBGROUPS","Não foi possível remover o grupo. Código do erro> EDSBFLRMGRPSBG -3016");
DEFINE("E_FAILEDTOREMOVEGROUPACCESS","Não foi possível remover o grupo. Código do erro> EDSBFLRMGRPACS -3017");
DEFINE("E_FAILEDTOREMOVEGROUPTRACKEDUNIT","Não foi possível remover o grupo. Código do erro> EDSBFLRMGRPTRUNT -3018");

//DEFINE("E_UNABLETOREMOVEGROUP","");

// WIDGET TABLE > WIDGET
DEFINE("E_UNABLETOCREATEWIDGET","Não foi possível cadastrar o widget. Código do erro: EDSTUNCRWD -4001");
DEFINE("E_UNABLETOUPDATEWIDGET","Não foi possível cadastrar o widget. Código do erro: EDSTUNUPWD -4002");
DEFINE("E_UNABLETOLISTWIDGETS","Não foi possível listar os widgets. Código do erro: EDSBUNTLSWD -4003");
DEFINE("E_UNABLETOGETPARENTWIDGETS","Não foi possível listar todos os widgets. Código do erro: EDSBUNGTPRWD -4004");
DEFINE("E_WIDGETSNOTFOUND","Nenhum widget encontrado. Código do erro: EDSBNTFWD -4005");
DEFINE("E_WIDGETTEMPLATEEXISTS","O template escolhido já existe. Código do erro: EDSBWDTMPEX -4006");
DEFINE("E_WIDGETNOTFOUND","Não foi possível encontrar o widget. Código do erro: EDSBWDNTFND -4007");
DEFINE("E_UNABLETODELETEUNIT","Não foi possível remover os dados da unidade selecionada. Código do erro: EDSBUNADELUNI - 4008");
//DEFINE("E_","Não foi possível cadastrar o widget. Código do erro: EDSTUCRW -4003");

//WIDGET UNIT
DEFINE("E_INVALIDUNIT","Preencha todos os campos corretamente. Código do erro: EDSBUNINV -5001");
DEFINE("E_UNITEXISTS","Já existe uma unidade cadastrada com a identificação informada. Código do erro: EDSBUNEXT -5002");
DEFINE("E_UNABLETOADDTYPE","Não foi possível atualizar a unidade selecionada. Código do erro: EDSBUNAD -5003");
DEFINE("E_UNABLETOUPDATE","Não foi possível atualizar a unidade selecionada. Código do erro: EDSBUNUP -5004");
DEFINE("E_UNABLETOLOADUNIT","Não foi possível carregar a unidade selecionada. Código do erro: EDSBUNLD -5005");
DEFINE("E_UNABLETOLOADUNITLIST","Não foi possível carregar a lista de unidades. Código do erro: EDSBUNALOAUNILIS -5006");

//WIDGET USER
DEFINE("E_INVALIDMAIL","O e-mail informado é inválido.Código do erro: - 6001");
DEFINE("E_USERACCESSDENIED","Você não tem os privilégios necessários para realizar esta operação. Código do erro: - 6002");
DEFINE("E_USEREXISTS","Este e-mail já está associado a um usuário no sistema. Código do erro: - 6003");
DEFINE("E_UNABLETOSAVEACCESS","Não foi possível adicionar os acessos ao usuário. Código do erro: - 6004");
DEFINE("E_USERINFORMPASSWORD","Você deve informar sua senha para prosseguir com a operação solicitada. Código do erro: -6005");
DEFINE("E_USERINVALIDPASSWORD","A senha informada é inválida. Código do erro: -6006");
DEFINE("E_USERPASSWORDLENGTH","A senha deve possuir ao menos 6 caracteres. Código do erro: -6007");
DEFINE("E_UNABLETOADDGROUPACCESS","Não foi possível adicionar os acessos ao grupo. Código do erro: -6008");
DEFINE("E_UNABLETOCREATEUSER","Não foi possível criar o usuário. Código do erro: Código do erro: -6009");
DEFINE("E_UNABLETOCREATEACCESS","Não foi possível adicionar os acessos ao usuário. Código do erro: -6010");
DEFINE("E_UNABLETOUPDATEUSER","Não foi possível atualizar o usuário. Código do erro: EDSBUNUPDUSR -6011");
DEFINE("E_UNABLETOSELECTUSER","Não foi possível selecionar o usuário. Código do erro: EDSBUNSLUSR -6012");
DEFINE("E_UNABLETOSELECTUSERPERMISSION","Não foi possível recuperar os acessos da conta. Código do erro: -6013");
DEFINE("E_UNABLETOLISTUSERS","Não foi possível recuperar um ou mais usuários associados a conta. Código do erro: -6014");
DEFINE("E_FAILEDTOREMOVESESSIONLOG","Não foi possível remover o usuário. Código do erro: -6015");
DEFINE("E_FAILEDTOREMOVEUSERACCESS","Não foi possível remover o usuário. Código do erro: -6016");
DEFINE("E_FAILEDTOREMOVEUSER","Não foi possível remover o usuário. Código do erro: -6017");
DEFINE("E_PASSWORDNOTEQUAL","As senhas não estão iguais. Código do erro: -6018");


//WIDGET UNIT > TRACKED_UNIT_DEVICE
DEFINE("E_UNABLETORELEASE","Não foi possível liberar o(s) dispositivo(s) associado(s) a esta unidade. Código do erro: -6001");

//WIDGET DEVICE
DEFINE("E_INVALIDDEVICE","O dispositivo não possui todos os campos necessários para cadastro. Código do erro: -7001");
DEFINE("E_DEVICEEXISTS","Não foi possível cadastrar um ou mais dispositivos. Código do erro: - 7002");
DEFINE("E_DEVICENOTFOUND","Não foi possível localizar o dispositivo. Código do erro: - 7003");
DEFINE("E_UNABLETOLOADDEVICEMODEL","Não foi possível carregar os modelos. Código do erro: - 7004");
DEFINE("E_UNABLETOLOADDEVICES","Não foi possível carregar os dispositivos. Código do erro: - 7005");
DEFINE("E_UNABLETOLOADMODELOS","Não foi possível carregar os modelos. Código do erro: - 7006");
DEFINE("E_UNABLETOLOADASSET","Não foi possível carregar os dados do patrimônio. Código do erro: - 7007");
DEFINE("E_UNABLETODELETEDEVICE","Não foi possível remover os dados do dispositivo selecionado. Código do erro: - 7008");
DEFINE("E_UNABLETODELETEDEVICEBYUNIT","Não foi possível remover o dispositivo selecionado. O mesmo se encontra associado a unidade: ");
DEFINE("E_UNABLETOLISTMANUFACTURER","Não foi possível listar o(s) fabricante(s) do(s) dispositivo(s). Código do erro: -7010");
DEFINE("E_UNABLEDELFABUSE","O fabricante não pode ser excluído pois possui modelos dependentes. Código do erro: -7011");
DEFINE("E_DEVICEINMODELEXITS","Este dispositivo com este Identificador já existe cadastrado neste Modelo. Código do erro: -7012");
DEFINE("E_MODELDEVERRORDEL","Não foi possível deletar este modelo. Código do erro: -7013");
DEFINE("E_MODELDEVDEPDEL","Há dispositivos dependentes deste modelo. Código do erro: -7014");


//WIDGET MAP
DEFINE("E_FAILEDTOLOADUNITS","Não foi possível carregar as unidades, tente novamente. Código do erro: EDSFLDTLDUNT -8005");
DEFINE("E_FAILEDTORELOADUNITS","Não foi possível recarregar as unidades, tente novamente. Código do erro: EDSFLDTLDUNT -8006");
DEFINE("E_FAILEDTOLOADGROUPS","Não foi possível carregar os grupos, tente novamente. Código do erro: EDSFLDTLDGRP -8007");
DEFINE("E_FAILEDTOLOADFOOTER","Houve uma falha ao processar a requisição, tente novamente. Código do erro: EDSFLDTLDFTR -8010");
DEFINE("E_FAILEDTORELOADFOOTER","Houve uma falha ao processar a requisição, tente novamente. Código do erro: EDSFLDTRLDFTR -8011");
DEFINE("E_UNABLETOREVERSEGEOCODE","Houve um erro ao recuperar os recursos associados, tente novamente. Código do erro: EDSUNTRVRSGCD -8012");
DEFINE("E_UNABLETORECOVERUNITHISTORY","Não foi possível recuperar o histórico da unidade, tente novamente. Código do erro: EDSBUNTRCVRHUNSTR -8013");

/* Classe CommandDriver */
DEFINE("E_COMMANDDRIVER_EMPTYCOMMAND", "Você não pode construir um comando vazio. Código do erro: 9001");
DEFINE("E_COMMANDDRIVER_ARGUMENT_ILLEGAL", "Argumento illegal. Código do erro: 9002");
DEFINE("E_COMMANDDRIVER_INVALID_USER_ID", "Não encontrei ID do usuário. Usuário não logado? Código do erro: 9003");

//WIDGET DRIVERS
DEFINE("E_DRIVEREXISTS","Já existe um motorista com mesmo CNH ou matrícula cadastrado no sistema. Código do erro: -9000");
DEFINE("E_DRIVERCREATEFAILED","Ocorreu um erro ao adicionar motorista. Código do erro: -9001");
DEFINE("E_DRIVERUPDATEFAILED","Ocorreu um erro ao alterar o motorista. Código do erro: -9002");
DEFINE("E_DRIVERASSOCFAILED","Não existem motoristas associados a essa unidade. Código do erro: -9003");
DEFINE("E_DRIVERDELETEFAILED","Erro ao tentar deletar o Alarme. Código do erro: -9004");
DEFINE("E_UNITASSOCFAILED","Não existem unidades associadas a esse motorista. Código do erro: -9005");
DEFINE("E_DATACOMMANDASSOCFAILED","Ocorreu um erro na construção do comando. Código do erro: -9006");

//ALL WIDGETS
DEFINE("E_UNABLETOSENDEMAIL","Não foi possível enviar o e-mail. Código do erro: EDSBDNF - 100001");

