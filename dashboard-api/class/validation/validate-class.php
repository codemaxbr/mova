<?php
/**
 * 
 * @author ADM
 *
 */
Class Validate{
	
	
	/**
	 * Valida um parametro de acordo com os parametros opcionais passados pela
	 * chamada ao metodo
	 * @param Mixed $param, parametro a ser validado
	 * @param String $tipo, identifica o tipo de parametro
	 * @param Int $min, determina o menor valor que $param pode ter. default = 1
	 * @param Int $max, determina o maior valor que $param pode ter, default = 100
	 * @param String $regex, expressão regular a ser utilizada para o parametro, default=null
	 * @param Int $cast, determina se deve haver cast explicito da variavel, default=false
	 * @param String $replace, expressao regular para remover caracteres indesejados, default = false
	 * @access public
	 * @return Mixed, false caso $param nao esteja dentro do determinado, $param caso a flag $cast esteja habilitada, true em todos os outros casos
	 */
	public static function isValid($param, $tipo, $min = 1, $max = 100, $cast = false, $regex = null, $pattern = null) {
		/**
		 * @var $ftipo
		 * Utilizada para fazer a chamada a funcao que ira comparar o tipo da variavel
		 * Ex: is_numeric
		 */
		$ftipo = "is_";
		/**
		 * Finaliza a funcao concatenando com o tipo passado por parametro
		 * Ex: $ftipo = "is_";
		 * $tipo = "numeric";
		 * $ftipo .= $tipo
		 * Resulta na funcao is_numeric
		 */
		$ftipo .= $tipo;
	
		if ($ftipo($param)/*  === $tipo */) {
	
			if (strlen($param) >= $min && strlen($param) <= $max) {
	
				if ($pattern !== null) {
	
					//Caso o replace a ser aplicado seja para validacao de CPF chama o metodo validaCpf
					if ($pattern == "cpf") {
						return self::validaCpf($param);
					} else {
						$param = preg_replace($pattern, "", $param);
					}
				}
	
				/**
				 * Verifica se cast e explicitamente diferente de false
				 */
				if ($cast !== false) {
	
					//Utiliza eval para forçar o cast de $param, $cast recebido por parametro
					eval("\$param = {$cast} \$param;");
				}
				/**
				 * Verifica se algum regex foi setado
				 */
				if ($regex !== null) {
						
					//Utiliza a funcao interna de validacao para e-mail caso a versao seja maior que a 5.2
					if ($regex == "email" && PHP_VERSION > "5.2.0") {
	
						return filter_var($param, FILTER_VALIDATE_EMAIL);
					}
					if (preg_match($regex, $param)) {
						return $param;
					} else {
						return false;
					}
				}
	
				return $param;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Verifica se o tamanho da variavel passada esta dentro do min e max 
	 * @param String $param, o parametro a ser verificado
	 * @param Int $min, o menor valor possivel para a variavel
	 * @param Int $max, o maior valor possivel para a variavel
	 * @return Mixed
	 */
	public static function length($param,$min=1,$max=100){
		if(strlen($param) >= $min && strlen($param) <= $max){
			return $param;
		}else{
			return false;
		}
	}
	/**
	 * Verifica se um e-mail e valido ou nao, utiliza um regex passado por parametro ou 
	 * a validacao interna do PHP, caso a versao seja maior que a 5.2
	 * @param String $param, o e-mail a ser validado
	 * @return Mixed 
	 */
	public static function email($param){
		// Padrao utilizado pela funcao filter_var
		$pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
		if (PHP_VERSION > "5.2.0") {
	
			return filter_var($param, FILTER_VALIDATE_EMAIL);
	
		}else{
	
			if (preg_match($regex, $param)) {
	
					return $param;
	
			} else {
	
					return false;
	
			}
		}
		
	}
	/**
	 * Verifica se a variavel e de um determinado tipo
	 * @param $param, o parametro a ser testado
	 * @param $type, o tipo a ser validado
	 * @return Mixed
	 */
	public static function type($param,$type){
		/**
		 * @var $ftipo
		 * Utilizada para fazer a chamada a funcao que ira comparar o tipo da variavel
		 * Ex: is_numeric
		 */
		$ftipo = "is_";
		/**
		 * Finaliza a funcao concatenando com o tipo passado por parametro
		 * Ex: $ftipo = "is_";
		 * $tipo = "numeric";
		 * $ftipo .= $tipo
		 * Resulta na funcao is_numeric
		 */
		$ftipo .= $type;
	
		if ($ftipo($param)){
			return $param;
		}else{
			return false;
		}
	}
	/**
	 * Forca o cast de uma variavel para um tipo estabelecido
	 * @param String $param
	 * @param String $cast, o tipo para converter a variavel
	 * @return Mixed
	 */
	public static function castTo($param,$cast){
		return eval("\$param = {$cast} \$param;");
	}
	/**
	 * Utiliza uma expressao regular para substituir valores
	 * @param String $param
	 * @param String $replacement, novos valores
	 * @param String $pattern, padrao de busca (regex)
	 * @return String $param
	 */
	public static function replace($param,$replacement,$pattern){
		$param = preg_replace($pattern, $replacement, $param);
		return $param;
	}
	
	
	public static function login($param){
		return self::type(self::length($param), "string");
	}
	
	public static function password($param){
		return self::type(self::length($param), "string");
	}
}
