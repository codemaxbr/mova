<?php
/**
 * 
 * Authentication class
 * 
 * Responsavel por criar, validar, atualizar e destruir sessoes de usuarios
 * 
 */
Class Authentication{
	/**
	 * @var String $user
	 * Login do usuario
	 */
	private $user;
    private $user_id;
	/**
	 * @var String $password
	 * Senha do usuario
	 */
	private $password;
    private $password_clean;
	/**
	 * @var String $seriesIdentifier
	 * Identificador de multiplas sessoes do usuario
	 */
	public static $seriesIdentifier;
	/**
	 * @var String $token
	 * Identificador da sessao atual do usuario
	 */
	public static $token;
	/**
	 * @var DatabaseHandler $dbh
	 * Recebe uma instancia da classe DatabaseHandler 
	 */
	private static $dbh;
	
	/**
	 * Construtor da classe
	 * 
	 * @param String $user
	 * @param String $password
	 */		
	public function __construct($user,$password) {

	
		/**
		 * @var String User
		 * Login do usuario
		 */
		$this -> user = $user;
		
		// TODO Gerar hash da senha
		$this -> password = $password;
		$this->password_clean = $password;

		//require_once("../dashboard-api/driver/database-handler-class.php");
		
		self::$dbh = new DatabaseHandler();
		date_default_timezone_set("America/Sao_Paulo");
	}
	/**
	 * setSID
	 * Define o valor do atributo seriesIdentifier da sessao como o valor da sessao anterior
	 * utilizado para que seja possivel renovar o token de uma mesma sessao
	 * @param String $seriesIdentifier
	 */
	private static function setSID($seriesIdentifier){
		self::$seriesIdentifier = $seriesIdentifier;
	}
	/**
	 * getSID
	 * Retorna o valor do seriesIdentifier da sessao anterior
	 * @return String $seriesIdentifier
	 */
	private static function getSID(){
		return self::$seriesIdentifier;
	}
	/**
	 * setToken
	 * Define o valor do atributo token, como o valor do token gerado na sessao anterior
	 * @param String $token
	 */
	private static function setToken($token){
		self::$token = $token;
	}
	/**
	 * getToken
	 * Recupera o valor do token da sessao anterior
	 * @return String $token
	 */	
	private static function getToken(){
		return self::$token;
	}
	/**
	 * verifyAccess
	 * 
	 * Verifica se o usuario possui credenciais validas de acesso na sessao atual
	 * utilizada nos acessos diretos ao dashboard para verificar se o usuario possui um cookie de sessao valido
	 * e renovar sua sessao ou forcar seu login caso seja necessario
	 * 
	 * @return String $errorMessage, retorna uma mensagem de erro caso seja detectada uma tentativa de roubar a sessao do usuario
	 * 
	 */
	public static function verifyAccess(){
		try {
			
			if(isset($_COOKIE['MovaSessionAuth']) && !isset($_SESSION['user'])){
				
				//require_once("../dashboard-api/authentication/database-handler.php");
		
				self::$dbh = new DatabaseHandler();
				
				$checkCookies = self::checkCookies($_COOKIE['MovaSessionAuth']);
			
				if(is_object($checkCookies)){
						self::sessionRenew($checkCookies);
				}else{
						if(is_string($checkCookies)){
							throw new Exception($checkCookies);		
						
						}else{
							self::disableSessions($checkCookies);
							exit(header("location: ../"));
						}
					}	
			}else{

					if(!isset($_COOKIE['MovaSessionAuth']) && !isset($_SESSION['user'])){
						exit(header("location: ../"));
					}
				}
		} catch (Exception $e) {
			//FIXME Tratamento de erros
			$_SESSION['errorMessage'] = $e -> getMessage();
			exit(header("location: ../"));
		}		
	}
	/**
	 * validateAccess
	 * 
	 * Verifica a validade de um acesso de usuario atraves do formulario de login, buscando seu usuario e senha na tabela de usuarios
	 * @return Mixed, Object se o usuario for encontrado, false se nao for encontrado ou uma mensagem de erro caso haja algum erro de execucao
	 */
	public function validateAccess() {
		try {
			$account_name = '';
			//bypass para usuário admin de outras contas
			if(strrpos($this -> user, "::")){
				list($userDefalt, $account_name) = split('::', $this -> user);
				$this -> user = $userDefalt;
				$this -> password = $this -> passwordHash();

				self::$dbh -> addOption("table", "users usr");
				self::$dbh -> addOption("fields", array("usr.name as user_name","usr.login", "usr.password", "usr.id as user_id","usr.account_id","usracs.widget_id","usracs.access_level","wd.name as widget_name","wd.icon","wd.template","wdc.name as widget_category_name","wdc.id as widget_category_id", "ac.name as account_name"/* , "cli.name as client_name" */));
				self::$dbh -> addOption("joinTable", array("user_access usracs","widget wd","widget_category wdc","account ac","account_widget acw", "users usrDefalt"));
				self::$dbh -> addOption("joinTableType", array("INNER JOIN","INNER JOIN","INNER JOIN","INNER JOIN","INNER JOIN","INNER JOIN"));
				self::$dbh -> addOption("joinTableOn", array(
																"usr.id = usracs.user_id",
																"wd.id = usracs.widget_id AND wd.status = 1",
																"wd.widget_category_id = wdc.id", 
																"ac.id = usr.account_id", 
																"acw.account_id = ac.id AND acw.widget_id = wd.id", 
																"usrDefalt.user_mova = 1"
															));					
				self::$dbh -> addOption("orderBy", array("wdc.name", "wd.name"));

				self::$dbh -> addOption("where", array(
														"upper(ac.name) = upper(:account_name)", 
														"usrDefalt.login = :login",
														"usrDefalt.password = :password",
														"usr.master = 1", 
														"usr.status = 1"
													));
				self::$dbh -> addOption("logicalOperator", array("AND", "AND", "AND", "AND"));
				self::$dbh -> addOption("param", array(
														":account_name" => $account_name, 
														":login" => $userDefalt, 
														":password" => $this -> password
													));	

				$bypassResult = self::$dbh -> select();
				
				if(is_object($bypassResult)) {
					$data = $bypassResult -> fetch();

					$this -> user = $data -> login;
					$this -> password = $data -> password;
                    $this->user_id = $data->user_id;
				} else {
					return E_ACCINVALID;
				}
			}else{
				$this -> password = $this -> passwordHash();
			}

            //ini: verifica se login/pass casam
			self::$dbh -> addOption("table", "users usr");
			self::$dbh -> addOption("fields", array("usr.name as user_name","usr.login", "usr.id as user_id","usr.account_id","usracs.widget_id","usracs.access_level","wd.name as widget_name","wd.icon","wd.template","wdc.name as widget_category_name","wdc.id as widget_category_id", "ac.name as account_name"/* , "cli.name as client_name" */));
			self::$dbh -> addOption("joinTable", array("user_access usracs","widget wd","widget_category wdc","account ac","account_widget acw"));
			self::$dbh -> addOption("joinTableType", array("INNER JOIN","INNER JOIN","INNER JOIN","INNER JOIN","INNER JOIN"));
			self::$dbh -> addOption("joinTableOn", array(
															"usr.id = usracs.user_id",
															"wd.id = usracs.widget_id AND wd.status = 1",
															"wd.widget_category_id = wdc.id", 
															"ac.id = usr.account_id", 
															"acw.account_id = ac.id AND acw.widget_id = wd.id"
														));					
			self::$dbh -> addOption("orderBy", array("wdc.name", "wd.name"));

			self::$dbh -> addOption("where", array(
													"usr.login = :login", 
													"usr.password = :password", 
													"usr.status = 1"
												));
			self::$dbh -> addOption("logicalOperator",array("AND", "AND"));
			self::$dbh -> addOption("param", array(
													":login" => $this -> user, 
													":password" => $this -> password
												));					
			
			$result = self::$dbh -> select();
            //fim: verifica se login/pass casam
            if(!is_object($result)) {
                return E_INVALID;
            }

            $xxxdata = $result->fetch();
            $this->user_id = $xxxdata->user_id;

            $data = $result;
            $user = $this -> getUserAccess(null, $this -> user, $this -> password);

            $checkAccess = 	$this -> verifyUserAccess($user);
				
            if($checkAccess === true) {
                /* Login bem sucedido.  Aproveitamos o momento aqui para
                 * migrar o hash do usuário.  Se ele estiver usando md5(),
                 * o alteramos pra sha1().
                 */
                if ( strlen($this->passwordHash()) == 32 ) {
                    //$hash = sql_escape(sha1($this->password_clean));
                    $hash = sha1($this->password_clean);
                    $id = intval($this->user_id);

                    if ($id <= 0)
                        throw new Exception("ID [{$id}] do usuário não é válido.");
                    
                    //$sql = "UPDATE users SET modify_date = now(), password = {$hash} WHERE id = {$id} ";
                    $sql = "UPDATE users SET modify_date = now(), password = '$hash' WHERE id = {$id} ";
                    $db = new DatabaseHandler();
                    $db->exec_v1($sql);
                }

                return $result;
            }  else { 
                return $checkAccess;
            }
		} catch(Exception $e) {
			return $e -> getMessage();
		}
	}
	/**
	 * Metodo que valida o acesso do usuario atraves de seu status e do periodo de acesso
	 * @uses getUserAcess()
	 * @return Mixed, String, Boolean
	 */
	public static function verifyUserAccess($user) {
		try {
			if(is_string($user)) {
				return $user;
			}
			
			$user = $user -> fetch();
			
			$day_week = date('w'); //Pegando dia da semana
			$hour = date('H:i'); //Pegando a hora
			
			//Verificando se usuário está ativo
			if($user->usr_status != 1) {
				return E_DENIED;
			}
	
			//Verificando se usuário está com data limite expiradaç
			if(!is_null($user->end_access)) {
				if($user->end_access < date('Y-m-d')) {
					return E_USEREXPIRED;						
				}
			}
	
			//Verificando se usuário está entrando no horário permitido
			if(!is_null($user->hour_start) && !is_null($user->hour_end)) {
				if(($hour < date('H:i', strtotime($user->hour_start))) || ($hour > date('H:i', strtotime($user->hour_end)))) {
					return E_INVALIDTIME;
				}
			} else if(is_null($user->hour_start) && !is_null($user->hour_end)) {
				if(($hour < date('H:i', strtotime('00:00'))) && ($hour > date('H:i', strtotime($user->hour_end)))) {
					return E_INVALIDTIME;
				}
			} else if(!is_null($user->hour_start) && is_null($user->hour_end)) {
				if(($hour < date('H:i', strtotime($user->hour_start))) && ($hour > date('H:i', strtotime('00:00')))) {
					return E_INVALIDTIME;
				}
			}
			
			//Verificando se usuário está entrando no dia da semana permitido
			if(!is_null($user->day_start) && !is_null($user->day_end)) {
				if(($day_week < $user->day_start) || ($day_week > $user->day_end)) {
					return E_INVALIDWEEKDAY;
				}
			}
			return true;
			
		} catch(Exception $e) {
			return $e -> getMessage();
		}
	}
	/**
	 * Metodo que recupera o periodo de acesso e o status do usuario
	 * @return Mixed, Object, String $result
	 */
	private static function getUserAccess($sid=null, $user=null, $password=null) {
		try {
			self::$dbh -> addOption("table", "users usr");
			self::$dbh -> addOption("fields", array("usr.status usr_status", "usr.hour_start", "usr.hour_end", "usr.day_start", "usr.day_end", "usr.end_access"));

			if($sid === null) {
				self::$dbh -> addOption("where", array("usr.login = :login","usr.password = :password"));
				self::$dbh -> addOption("logicalOperator",array("AND"));
				self::$dbh -> addOption("param",array(":login" => $user, ":password" => $password));
			} else {
				self::$dbh -> addOption("where", array("usr.id = :id"));
				self::$dbh -> addOption("param",array(":id" => $sid));
			}

			$result = self::$dbh -> select();
			
			if(is_object($result)) {
				return $result;
			} else {
				return E_INVALID;				
			}
			
		} catch(Exception $e) {
			return $e -> getMessage();
		}
	}
	/**
	 * Gera o hash da senha do usuario utilizando o login como salt e a funcao md5 para o obter o hash
	 * @param String $login
	 * @param String $password
	 * @return String hash
	 */
	public function passwordHash() {
        /*
          Estamos num plano de migração de hashes.
        */
        
        // $login = sql_escape($this->user);
        // $sql = "SELECT LENGTH(password) AS N FROM users WHERE login = {$login}";
        $login = $this->user;
        $sql = "SELECT LENGTH(password) AS N FROM users WHERE login = '$login'";
        $r = static::$dbh->select_fetch_v1($sql);
        if (!count($r)){
            //throw new Exception("Login do usuário não encontrado.");
            return sha1($this->password);
		}else{
        	$o = $r[0];

			if ($o->n == 32) {
	            return md5(md5($this -> user.$this -> password));
	        }else if ($o->n == 40){
	            return sha1($this->password);
	        }
	    }
        
        throw new Exception("Hash do usuário não identificado.");
    }
	/**
	 * validateToken
	 * 
	 * Valida a sessao do usuario em seu acesso atraves de cookies de sessao
	 * utilizado para verificar se o usuario tentando acessar o dashboard vem de uma conexao legitima, ou seja, que nao
	 * existe risco do cookie ser forjado ou que alguem esta tentando se passar pelo usuario
	 * @param Int $userId
	 * @param String $seriesIdentifier
	 * @param String $token
	 * @return Mixed, boolean false em caso de falha ao encontrar o usuario, ou PDO $stmt caso os dados da sessao sejam validos, ou String $errorMessage em caso de erro 
	 */	
	private static function validateToken($userId, $seriesIdentifier, $token) {
		try {
			// Dados um user_id, um seriesIdentifier e um token busca na tabela de sessoes verificando tambem,
			// se o seriesIdentifier e o token pertencem ao usuario informado e se representam a ultima sessao valida
			self::$dbh -> addOption("table", "session");
			self::$dbh -> addOption("fields",array("users_id","series_identifier","token"));
			self::$dbh -> addOption("where", array("users_id = :uid","series_identifier = :sid","series_identifier IN(SELECT series_identifier FROM session ORDER BY time DESC LIMIT 1)","token = :token","token IN(SELECT token FROM session ORDER BY time DESC LIMIT 1)"));
			self::$dbh -> addOption("logicalOperator",array("AND","AND","AND","AND"));
			self::$dbh -> addOption("param", array(":uid" => $userId, ":sid" => $seriesIdentifier,":token" => md5($token)));
			$lookUpResult = self::$dbh -> select();

			if(is_object($lookUpResult)) {
				// Recupera as permissoes associadas ao ultimo SID e token validos
				self::$dbh -> addOption("table", "users usr");
				self::$dbh -> addOption("fields", array("usr.name as user_name","usr.login","usr.id as user_id","ses.series_identifier","ses.token","usr.account_id","usracs.widget_id","usracs.access_level","wd.name as widget_name","wd.icon","wd.template","wdc.name as widget_category_name","wdc.id as widget_category_id", "ac.name as account_name"));
				self::$dbh -> addOption("joinTable", array("user_access usracs","widget wd","widget_category wdc","session  ses","account ac","account_widget acw"));
				self::$dbh -> addOption("joinTableType", array("INNER JOIN","INNER JOIN","INNER JOIN","INNER JOIN","INNER JOIN","INNER JOIN"));
				self::$dbh -> addOption("joinTableOn", array("usr.id = usracs.user_id","wd.id = usracs.widget_id","wd.widget_category_id = wdc.id","ses.users_id = usr.id", "ac.id = usr.account_id", "acw.account_id = ac.id AND acw.widget_id = wd.id"));
				self::$dbh -> addOption("where", array("usr.id = :uid"," ses.series_identifier = :sid ","ses.token = :token"));
				self::$dbh -> addOption("logicalOperator", array("AND","AND"));
				self::$dbh -> addOption("orderBy", array("wdc.name","wd.name"));
				self::$dbh -> addOption("param",array(":uid" => $userId, ":sid" => $seriesIdentifier, ":token" => md5($token)));
				
				$getUserAccess = self::$dbh -> select();
				if(is_object($getUserAccess)){
					
					self::setSID($seriesIdentifier);
					
					self::setToken($token);
					
					return $getUserAccess;
					
				} else {
					return false;
				}
			} else {	
				return false;
			}			
		} catch(Exception $e) {
			return $e -> getMessage();
		}
	}
	/**
	 * sessionRenew
	 * Renova a sessao de um usuario acessando o dashboard diretamente atraves de cookies de sessao
	 * @param stdClass $sessionInfo
	 * @return String $errorMessage em caso de erro
	 */	
	private static function sessionRenew($sessionInfo) {
		try {
			self::menuInit($sessionInfo);	
			if(is_object($_SESSION['user'])) {
				// Atualiza a sessao
				$newSession = self::updateSession($_SESSION['user'] -> getUserId(),$_SESSION['user'] -> getToken());
				
				//TODO REVER LOCATION
				if($newSession === true) {
					// Sessao atualizada com sucesso, prossegue para o dashboard
					header("location: ../dashboard/");
				} else {
					// Sessao nao foi atualizada, destroi dados de sessao e forca o usuario a realizar o login
					self::destroySession();
					exit(header("location: ../"));	
				}
			} else {
				//FIXME Tratamento de erros
				throw new Exception(E_UNABLETOLOGIN);
			}
		} catch(Exception $e) {
			return $e -> errorMessage();			
		}
	}
	/**
	 * createSession
	 * Inicia uma sessao de usuario quando este e autenticado pelo formulario de login
	 * @param $validaAcesso
	 * @param Int $cookieInit, flag que indica se o usuario selecionou a opcao "Lembrar" ao se logar
	 * @return Mixed, true se a sessao tiver sido iniciada corretamente ou uma mensagem de erro caso a sessao nao tenha sido iniciada corretamente
	 */
	public function createSession($validateAccess, $cookieInit) {	
		try {			
			//Array de tres niveis responsavel por armazenar os valores dos widgets retornados pela consulta de acesso
			//Cada nivel deste array é um novo array de arrays. Ex:
			//1 => array (size=1)
			//    'elem' => array (size=3) (contido no primeiro nivel)
			//              0 => array (size=7) (contido em elem)
			//              1 =>array (size=7)  (contido em elem)
			//              2 => array (size=7) (contido em elem)
			//Nivel 1 - Agrupa os widgets por categoria
			//Nivel 2 - Agrupa os widgets de cada categoria dentro de um novo array de elementos "elem"
			//Nivel 3 - Array de arrays - Recebe os arrays dos wigets
			//Essa formatacao inicial (junto com aninhamentos que ocorrem antes da codificacao para JSON) permite criar um elemento JSON valido
			$widgetAccess = array();
				
			// Armazena o nivel de acesso para cada widget individualmente
			// Formato: widget => acesso
			$accessLevel = array();
			$widgetCategory = array();
			
			// Monta o menu do usuario, instancia a classe User e inicializa a sessao do usuario 
			$setMenu = self::menuInit($validateAccess);
			
			// Verifica se a sessao foi criada corretamente
			if(is_object($_SESSION['user']) && $setMenu == true) {
				// Verifica se o usuario selecionou a opcao para "Lembrar"
				if($cookieInit === 'on') {
					// Inicia o cookie de sessao
					$this -> cookieInit($_SESSION['user'] -> getUserId(),$_SESSION['user'] -> getToken(), $_SESSION['user'] -> getSeriesIdentifier());
				}
				// Salva os dados da sessao no banco
				$this -> saveSession($_SESSION['user'] -> getUserId(), $_SESSION['user'] -> getSeriesIdentifier(), $_SESSION['user'] -> getToken());
				return true;
			} else {
				throw new Exception(E_UNABLETOLOGIN);
			}
		} catch(Exception $e) {
			return $e -> getMessage();
		}
	}
	/**
	 * 
	 * disableSessions
	 * 
	 * Remove todas as entradas de sessao do usuario por seguranca
	 * forcando o usuario a se logar
	 * 
	 * @param Int $userId
	 * @return String erroMessage, retorna uma mensagem de erro caso o metodo nao seja executado
	 * 
	 */
	private static function disableSessions($userId) {
		try {
			$selectLast = self::lastActive($userId);
			
			if(is_object($selectLast)) {
				self::logAccess($selectLast);	
			}
			
			self::$dbh -> addOption("table", "session");
			self::$dbh -> addOption("where",array("users_id = :userId"));
			self::$dbh -> addOption("param", array(":userId" => $userId));
			self::$dbh -> delete();
									
			self::destroySession();
			
		}catch(Exception $e){
			//TODO Tratamento de erros
			return $e -> getMessage();
		}	
	}
	/**
	 * private static lastActive
	 * 
	 * Recupera o ultimo registro ativo de sessao do usuario
	 * @param Int $userId, identificador do usuario
	 * @return Mixed
	 */
	private static function lastActive($userId){
		
		try{
			
			/*
			$selectLast = "SELECT * FROM session WHERE users_id = :userId ORDER BY time DESC LIMIT 1";
			
			$stmt = $GLOBALS['linkConexao'] -> prepare($selectLast);
			
			$stmt -> bindValue(":userId",$userId);
			
			$stmt -> execute();
			*/
			self::$dbh -> addOption("table", "session");
			self::$dbh -> addOption("fields", array("*"));
			self::$dbh -> addOption("where", array("users_id = :userId"));
			self::$dbh -> addOption("orderBy", array("time"));
			self::$dbh -> addOption("orderBySort", array("DESC"));
			self::$dbh -> addOption("orderByLimit", "1");
			self::$dbh -> addOption("param", array(":userId" => $userId));
			$selectLast = self::$dbh -> select();

			if(is_object($selectLast)){
					
				//$userSession = $stmt -> fetch();
				
				return $selectLast -> fetch();	
			}else{
				return false;
			}
			
			
		}catch(Exception $e){
			return $e -> getMessage();
		}	
	}
	/**
	 * logAccess
	 * 
	 * Grava as sessoes do usuario no BD
	 * @param stdClass $userSession, classe container com as informacoes da sessao do usuario
	 */
	private static function logAccess(stdClass $userSession){
		try{
			/*
			$log = "INSERT INTO session_log (users_id,old_series_identifier,old_token,ip,browser,time) VALUES(:users_id,:old_series_identifier,:old_token,:ip,:browser,:time)";
			
			$stmt = $GLOBALS['linkConexao'] -> prepare($log);
			
			$stmt -> bindValue(":users_id",$userSession -> users_id );
			$stmt -> bindValue(":old_series_identifier",$userSession -> series_identifier);
			$stmt -> bindValue(":old_token",$userSession -> token);
			$stmt -> bindValue(":ip",$userSession -> ip);
			$stmt -> bindValue(":browser",$userSession -> browser );ç
			$stmt -> bindValue(":time",$userSession -> time);
			
			$stmt -> execute();
			*/
			
			self::$dbh -> addOption("table", "session_log");
			self::$dbh -> addOption("fields", array("users_id","old_series_identifier","old_token","ip","browser","time"));
			self::$dbh -> addOption("param", array(":users_id" => $userSession -> users_id ,":old_series_identifier" => $userSession -> series_identifier,":old_token" => $userSession -> token,":ip" => $userSession -> ip,":browser" => $userSession -> browser,":time" => $userSession -> time));
			self::$dbh -> insert();
			
		}catch(Exception $e){
			
			return $e -> getMessage();
			
		}
	}
	/**
	 * cookieInit
	 * 
	 * Grava o cookie de sessao do usuario caso ele tenha selecionado a opcao "Lembrar" no login.
	 * os cookies possuem duracao de 30 dias e sao validos para todo o dominio
	 * @param String $login, login do usuario utilizado para verificar a sessao atraves do cookie
	 * @param String $token, id da sessao do usuario, renovado a cada acesso
	 * 
	 */
	private static function cookieInit($userId,$token,$seriesIdentifier){
		
		//ID do usuario
		$param = "sid=".$userId.";";
		//Series Identifier
		$param .= "ssid=".$seriesIdentifier.";";
		//Token
		$param .= "token=".$token.";";
		
		setcookie("MovaSessionAuth", $param, strtotime("+30 days"),"/");
	}
	/**
	 * checkCookies
	 * Recebe e manipula a string de um cookie para recuperar os valores de acesso da sessao do usuario
	 * @return  Mixed, Object $checkSession caso a sessao seja valida, Int $sid id do usuario, String $errorMessage em caso de erro
	 */
	public static function checkCookies($cookie){
		
		
		try{
			$cookie = str_replace(";","&",$cookie);
			
			parse_str($cookie);
			
			$user = self::getUserAccess($sid);
			$verifyAcess = self::verifyUserAccess($user);		
			
			if(is_string($verifyAcess) || $verifyAcess !== true){
				return $verifyAcess;
			}	
			
			$checkSession = self::validateToken($sid,$ssid, $token);
			
			if(is_object($checkSession)){
				
				return $checkSession;
			}else{
				//Forca o cast para inteiro
				//FIXME Revisar
				return (int) $sid;
			}	
		}catch(Exception $e){
			return $e -> getMessage();
		}
	}
	/**
	 * saveSession
	 * Grava os dados da sessao do usuario no banco de dados e gera o log da sessao ativa
	 * @param Int $userId
	 * @param String $seriesIdentifier
	 * @param String $token
	 * @return Mixed, boolean para a sessao salva no bd, String $errorMessage em caso de erro
	 */
	private function saveSession($userId,$seriesIdentifier,$token){
		
		try{
			/*
			$query = "INSERT INTO session(users_id,series_identifier,token,time,ip,browser) VALUES(:users_id,:series_identifier,:token,:time,:ip,:browser)";
			//$query = "INSERT INTO session(users_id,series_identifier,token,time,ip,browser) VALUES(:users_id,:series_identifier,:token,now(),:ip,:browser)";
			
		
			$stmt = $GLOBALS['linkConexao'] -> prepare($query);
			
			$stmt -> bindValue(":users_id",$userId);
			$stmt -> bindValue(":series_identifier",$seriesIdentifier);
			$stmt -> bindValue(":token",md5($token));
			$stmt -> bindValue(":time",$time );
			$stmt -> bindValue(":ip",$_SERVER['REMOTE_ADDR']);
			$stmt -> bindValue(":browser",$_SERVER['HTTP_USER_AGENT']);
			
			$stmt -> execute();
			*/
			
			$time =  date("Y-m-d H:i:s");
			
			self::$dbh -> addOption("table", "session");
			self::$dbh -> addOption("fields", array("users_id","series_identifier","token","time","ip","browser"));
			self::$dbh -> addOption("param", array(":users_id" => $userId ,":series_identifier" => $seriesIdentifier,":token" => md5($token),":time" => $time,":ip" => $_SERVER['REMOTE_ADDR'],":browser" => $_SERVER['HTTP_USER_AGENT']));
			$sessionStarted = self::$dbh -> insert();
			
			$userSession  = new stdClass();
			$userSession -> users_id = $userId;
			$userSession -> series_identifier = $seriesIdentifier;
			$userSession -> token = md5($token);
			$userSession -> ip = $_SERVER['REMOTE_ADDR'];
			$userSession -> browser = $_SERVER['HTTP_USER_AGENT'];
			$userSession -> time = $time;
			
			if($sessionStarted === true){
					
				// Busca a ultima sessao ativa do usuario
				$selectLast = $this -> lastActive($userId);

				// Log da ultima sessao ativa
				$this -> logAccess($selectLast);
				
				// Log da sessao atual
				$this -> logAccess($userSession);
				
				return true;
				
			}else{
				return false;
			}
			
		}catch(Exception $e){
			//FIXME Retorno de erros
			return $e -> getMessage();
		}
	}
	/**
	 * updateSession
	 * Atualiza uma sessao existente, alterando o valor do token e do time de acordo com os da nova sessao a ser gravada
	 * @param Int $userId
	 * @param String $token, novo token de sessao do usuario
	 * @return Mixed, boolean para a sessao atualizada, String $errorMessage em caso de erro
	 */
	private static function updateSession($userId,$token){
			
		try{
			
			$oldSeriesIdentifier = self::getSID();
			$oldToken = self::getToken();
			
			
			
			$getLastSession = "SELECT * FROM session WHERE users_id = :uid AND series_identifier = :sid AND token = :oldToken";
			
			$session = $GLOBALS['linkConexao'] -> prepare($getLastSession);
						
			$session -> bindValue(":uid",$userId);
			$session -> bindValue(":sid",$oldSeriesIdentifier);
			$session -> bindValue(":oldToken",md5($oldToken));
			
			$session -> execute();

			$sessionInfo = $session -> fetch();
			
			self::logAccess($sessionInfo);
			
			self::cookieInit($userId, $token, $oldSeriesIdentifier);
			
			$updateToken = "UPDATE session SET token = :token, time = :time WHERE users_id = :uid AND series_identifier = :sid AND token = :oldToken";
			
			$time = date("Y-m-d H:i:s");
			
			
			
			$stmt = $GLOBALS['linkConexao'] -> prepare($updateToken);
			
			$stmt -> bindValue(":token",md5($token));
			$stmt -> bindValue(":uid",$userId);
			$stmt -> bindValue(":sid",$oldSeriesIdentifier);
			$stmt -> bindValue(":oldToken",md5($oldToken));
			$stmt -> bindValue(":time",$time);
			
			$stmt -> execute();
			
			if($stmt -> rowCount() > 0){
				return true;				
			}else{
				return false;
			}
			
		}catch(Exception $e){
			return $e -> getMessage();			
		}
	}
	/**
	 * destroySession
	 * 
	 * Desabilita a sessao e os cookie caso ja tenham sido iniciados, forcando o usuario a logar atraves do formulario
	 */
	public static function destroySession(){
		//Verifica se existe uma sessao ativa e destroi, caso nao haja inicia uma nova sessao para destruir todos os dados de sessao
	
		if(is_null(session_id())){
			session_start();
		}

		session_destroy();
		//Remove o cookie de sessao
		setcookie("MovaSessionAuth", "", strtotime("-1 year"),"/");
	}
	/**
	 * menuInit
	 * 
	 * Inicializa a sessao do usuario, instanciando a classe User.
	 * Carrega os widgets do menu e os niveis de acesso do usuario
	 * 
	 * @param Object $userInfo
	 * @return Mixed, boolean true se nao houverem erros de execucao, String $errorMessage em caso de erro
	 */
	private static function menuInit($userInfo){
		
		try{
			
			while($objUser = $userInfo -> fetch()){
				//Verifica se a variavel $usuario ja foi criada e inicializada
				//Caso nao tenha sido criada, cria uma classe stdClass() como container dos atributos basicos do usuario
				if(!isset($usuario)){
						
					$user = new stdClass();
					//utf8_decode para tratamento de caracteres especiais
					$user -> name = utf8_decode($objUser -> user_name);
					$user -> account_id = $objUser -> account_id;
					$user -> userId = $objUser -> user_id;
				}
				
				// usr.name as user_name,usr.account_id,usracs.widget_id,usracs.access_level,wd.name as widget_name,wd.icon,wd.template,wdc.name as widget_category_name
			
				// Verifica se a categoria do widget ja foi criada
				// Caso a categoria ainda nao tenha sido criada, inicializa a categoria atribuindo um array com o nome da categoria (widget_category_name => nome_categoria)
				// Utiliza htmlentities para tratar de caracteres especiais
				if(!isset($widgetCategory[$objUser -> widget_category_id])){
					//Inicializa a categoria
					$widgetCategory[$objUser -> widget_category_id] = array("widget_category_name" => htmlentities($objUser -> widget_category_name));
					//Atribui a categoria ao nivel correto do array de widgets
					$widgetAccess[$objUser -> widget_category_id] = $widgetCategory[$objUser -> widget_category_id];
				}
				//Insere os arrays de widgets dentro do array elem
				$widgetAccess[$objUser -> widget_category_id]["elem"][] = array("widget_id" => $objUser -> widget_id, "access_level" => $objUser -> access_level,"widget_name" => htmlentities($objUser -> widget_name),
						"icon" => $objUser -> icon,"template" => $objUser -> template,"widget_category_name" => htmlentities($objUser -> widget_category_name), "widget_category_id" => $objUser -> widget_category_id);
			
				// Niveis de acesso do usuario por widget
				$accessLevel[$objUser -> template] = $objUser -> access_level;
			}
			
			//Cria um novo nivel no array (widget) e atribui o array de widgets a esse novo nivel
			$widgetLevel["widget"] = array($widgetAccess);
				
			//Cria mais um nivel no array e atribui o nivel anterior a este nivel
			//OBSERVACAO: Os niveis sao criados para que seja possivel formatar o JSON corretamente, caso os niveis nao sejam criados
			//ainda teremos um JSON corretamente formatado mas o mustache nao vai conseguir ler/parsear este JSON
			$fullMenu["menu"] = $widgetLevel;
				
			// Inicializa a sessao do usuario com a instancia da classe User
			$_SESSION['user'] = new User($user,$fullMenu,$accessLevel);
			
			return true;
			
		}catch(Exception $e){
			return $e -> getMessage();
		}
	}
}
