<?php
  /**
   * Class Ws_default
   * 
   * metodos default para aplicativos mobile   
   * 
   * @author Paulo Silva
   * @category mobile
   * @package Ws_default
   * @uses ...
   */

Class Ws_default {
	public $token = "12342612648763598123791829371462835413";

	public static function __testWsDefault() {
		ECHO "testWsDefault OK";
	}
	/**
	 * Login: Metodo que autentica o usuário e retorna seus dados, os dados de todos os usuários da mesma conta e um token de acesso
	 * @param $request->login: login do usuário mobile
	 * @param $request->password: senha do usuário mobile
	 */
	public function Login($request) {
		try {
			session_start();

			@$Login = $request->login;
			@$password = $request->password;

			if(Validate::login($Login) && Validate::password($password)){

				$rememberMe = 0;
				$connection  = new Connection();
				$connection -> open();		
				$authenticate = new Authentication($Login,$password);

				$validateAccess = $authenticate -> validateAccess();
				if(is_object($validateAccess)){
						
					$isValid = $authenticate -> createSession($validateAccess, $rememberMe);				

					if($isValid === true){
						$userController = new userController();

						$userId = $_SESSION['user'] -> getUserId();	
						$accountId = $_SESSION['user'] -> getAccount();

						$userData = $userController->getUserAction($userId);
						$userList = $userController->listMobileAction();

						echo json_encode(array("data" => $userData, "token" => $this -> token, "list" => $userList));			
					}else{
						echo json_encode(array("data" => false, "message" => "Usuário invalido"));
					}
										
				}else{
					echo json_encode(array("data" => false, "message" => "Usuário invalido"));
				}
			}else{
				echo json_encode(array("data" => false, "message" => "Usuário invalido"));
			}

		} catch (Exception $e) {
			echo json_encode(array("data" => false, "message" => $e->getMessage()));
		}
	}
	/**
	 * CheckToken: Verifica sé o token de acesso é valido
	 * @param $token: tokend e acesso do usuário
	 */
	public function CheckToken($token) {
		if($token == $this -> token){
			return true;
		}else{
			return false;
		}
	}
}

?>