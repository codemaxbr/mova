<?php
  /**
   * Class Ws_SMS
   * 
   * Interface de comunicação de aplicativos mobile com o dashboard mova.   
   * 
   * @author Paulo Silva
   * @category mobile
   * @package ws_mobile
   * @uses ...
   */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Authorization, Content-Type, Accept');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 1728000');

require_once('../autoloader.php');
require_once('../dashboard-api/class/core/config.php');
require_once('../dashboard-api/class/error/error-map.php');
require_once("ws_default.php");

Class Ws_SMS extends Ws_default {

	public static function __testWsSMS($teste) {
		ECHO "testWsVelleda OK";
	}
	/**
	 * GetCheckList: Retorna a lista de ordens de um determinado usuário
	 * @param $request->user_id: id do usuário
	 * @param $request->token: token de aesso do sistema
	 */
	public function GetDataSMS($request) {
		try {
			// Verifica se o token está correto
			if($this -> CheckToken($request -> token)){
				$routeController = new RouteController();

				echo $routeController->GetDataSMSAction($request);
			}else{
				echo json_encode(array("data" => false, 'message' => "Token invalido"));
			}

		} catch (Exception $e) {
			echo json_encode(array("data" => false, 'message' => $e->getMessage()));
		}
	}
	/**
	 * GetCheckList: Retorna a lista de ordens de um determinado usuário
	 * @param $request->user_id: id do usuário
	 * @param $request->token: token de aesso do sistema
	 */
	public function GetNextUnitSMS($request) {
		try {
			// Verifica se o token está correto
			if($this -> CheckToken($request -> token)){
				$routeController = new RouteController();

				echo $routeController->getFirstUnitRouteAction($request);
			}else{
				echo json_encode(array("data" => false, 'message' => "Token invalido"));
			}

		} catch (Exception $e) {
			echo json_encode(array("data" => false, 'message' => $e->getMessage()));
		}
	}
}

/**
 * Recupera parametros da requisição HTTP jquery.
 */ 
if(!empty($_POST['data'])){
	$request = json_decode(json_encode($_POST['data']), false);
	@$class = $request->class;
	@$method = $request->method;
}

// @$request = json_decode(json_encode(array("token" => "12342612648763598123791829371462835413"), false));
// @$class = "Ws_SMS";
// @$method = "__testWsSMS";

/**
 * Instanciando classe da requisição mobile
 * @param $class = nome da classe; 
 * @param $method = nome do metodo; 
 */ 
$ws = new $class;
$execute = $ws -> $method($request);

?>