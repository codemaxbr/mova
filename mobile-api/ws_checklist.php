<?php
  /**
   * Class ws_velleda
   * 
   * Interface de comunicação de aplicativos mobile com o dashboard mova.   
   * 
   * @author Paulo Silva
   * @category mobile
   * @package ws_mobile
   * @uses ...
   */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Authorization, Content-Type, Accept');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 1728000');

require_once('../autoloader.php');
require_once('../dashboard-api/class/core/config.php');
require_once('../dashboard-api/class/error/error-map.php');
require_once("ws_default.php");

Class Ws_velleda extends Ws_default {

	public static function __testWsVelleda($teste) {
		ECHO "testWsVelleda OK";
	}
	/**
	 * GetCheckList: Retorna a lista de ordens de um determinado usuário
	 * @param $request->user_id: id do usuário
	 * @param $request->token: token de aesso do sistema
	 */
	public function GetCheckList($request) {
		try {
			// Verifica se o token está correto
			if($this -> CheckToken($request -> token)){
				$checklistController = new checklistController();

				echo $checklistController->list_VLAction($request->user_id);
			}else{
				echo json_encode(array("data" => false, 'message' => "Token invalido"));
			}

		} catch (Exception $e) {
			echo json_encode(array("data" => false, 'message' => $e->getMessage()));
		}
	}	
	/**
	 * SetCheckListData: returna a lista de ordens de um determinado usuário
	 * @param $request->user_id: id do usuário
	 * @param $request->token: token de aesso do sistema
	 */
	public function SetCheckListData($request) {
		try {
			// Verifica se o token está correto
			if($this -> CheckToken($request -> token)){
				$checklistController = new checklistController();
				if($request->action == 'add'){
					echo $checklistController->add_VLAction($request);
				}else{
					echo $checklistController->edit_VLAction($request);
				}
			}else{
				echo json_encode(array("data" => false, 'message' => "Token invalido"));
			}

		} catch (Exception $e) {
			echo json_encode(array("data" => false, 'message' => $e->getMessage()));
		}
	}
	/**
	 * GetCheckListById: Retorna uma unica linha da tabela de ordens cadastradas
	 * @param $request->id: id da ordem salva no banco de dados web
	 * @param $request->token: token de aesso do sistema
	 */
	public function GetCheckListById($request) {
		try {
			// Verifica se o token está correto
			if($this -> CheckToken($request -> token)){
				$checklistController = new checklistController();

				echo $checklistController->getById_VLAction($request->id);
			}else{
				echo json_encode(array("data" => false, 'message' => "Token invalido"));
			}

		} catch (Exception $e) {
			echo json_encode(array("data" => false, 'message' => $e->getMessage()));
		}
	}
	/**
	 * GetCheckListById: Retorna uma unica linha da tabela de ordens cadastradas
	 * @param $request->id: id da ordem salva no banco de dados web
	 * @param $request->token: token de aesso do sistema
	 */
	public function GetData($request) {
		try {
			// Verifica se o token está correto
			if($this -> CheckToken($request -> token)){
				$checklistController = new checklistController();

				echo $checklistController->GetDataAction($request -> user_id);
			}else{
				echo json_encode(array("data" => false, 'message' => "Token invalido"));
			}

		} catch (Exception $e) {
			echo json_encode(array("data" => false, 'message' => $e->getMessage()));
		}
	}
}

/**
 * Recupera parametros da requisição HTTP Angular.
 */ 
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
@$class = $request->class;
@$method = $request->method;

// @$request = json_decode(json_encode(array("token" => "12342612648763598123791829371462835413"), false));
// @$class = "Ws_velleda";
// @$method = "__testWsVelleda";

/**
 * Instanciando classe da requisição mobile
 * @param $class = nome da classe; 
 * @param $method = nome do metodo; 
 */ 
$ws = new $class;
$execute = $ws -> $method($request);

?>